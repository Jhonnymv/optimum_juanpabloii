<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluacionCategoria extends Model
{
    //
    protected $table='evaluacion_categorias';

    public $fillable = ['categoria','peso','estado','min_prod','siges_id'];
}
