<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PostulacionAnexo extends Model
{
    protected $table='postulacion_anexos';

    public function postulacion()
    {
        return $this->belongsTo('App\Postulacion','postulacion_id','id');
    }

    public function getUrlPathAttribute()
    {
        return Storage::url($this->url);
    }
}
