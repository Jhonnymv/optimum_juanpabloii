<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas_academica extends Model
{
    protected $table='areas_academica_docente';

    public $fillable = ['id','nombre'];

    public function carreras()
    {
        return $this->hasMany('App\Carerra','areas_academica_id','id');
    }

    public function docentes()
    {
        return $this->belongsToMany('App\Docente')->withPivot('fecha_inicio', 'fecha_fin','estado_gestion')->withTimestamps();
    }
    public function responsablesArea()
    {
        return $this->belongsToMany('App\Docente','areas_academica_docente','areas_academica_id','docente_id')->withPivot('fecha_inicio', 'fecha_fin','estado_gestion','docente_id')->withTimestamps();
    }


}
