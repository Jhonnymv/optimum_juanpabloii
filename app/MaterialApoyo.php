<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialApoyo extends Model
{
    protected $table='materiales_apoyo';

    public function tema()
    {
        return $this->belongsTo(Tema::class,'tema_id');
    }
}
