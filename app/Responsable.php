<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsable extends Model
{
    public $fillable = ['nombre','estado'];
    
    public function carreras()
    {
        return $this->hasMany('App\Carrera');
    }
}
