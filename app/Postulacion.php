<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulacion extends Model
{

    protected $table = 'postulaciones';

    public function postulante()
    {
        return $this->belongsTo('App\Postulante');
    }

    public function periodo()
    {
        return $this->belongsTo('App\Periodo');
    }

    public function pe_carrera()
    {
        return $this->belongsTo('App\PeCarrera','pe_carrera_id','id');
    }

    public function postulacion_anexos()
    {
        return $this->hasMany('App\PostulacionAnexo','postulacion_id','id');
    }

    public function vochers()
    {
        return $this->hasMany('App\Voucher','postulacion_id','id');
    }

    public function getUrlPathAttribute()
    {
        return \Storage::url($this->urlimg);
    }
    public function url_img_tests(){
        return asset($this->urlimg);
    }

}
