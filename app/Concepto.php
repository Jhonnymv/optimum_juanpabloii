<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $table='conceptos';
    public $fillable = ['nombre', 'periodo_id','estado','observaciones',];
    public $timestamps = false;
    public function c()
    {
        return $this->hasMany('App\Cronogramas','concepto_id','id');
    }

}
