<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{
    protected $table='dimensiones';

    public function criterio_dimension()
    {
        return $this->hasMany('App\CriterioDimension','dimension_id','id');
    }
}
