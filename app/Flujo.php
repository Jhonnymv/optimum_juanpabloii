<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flujo extends Model
{
    protected $fillable=['estado','observacion','fecha','tarea_id','solcitud_id'];

    public function solicitud()
    {
        return $this->belongsTo('App\Solicitud','solicitud_id','id');
    }
    public function usuario(){
        return $this->belongsTo('App\Usuario','revisado_por','id');
    }
}
