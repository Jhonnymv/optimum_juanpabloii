<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatriculaDetalle extends Model
{
    protected $table='matricula_detalles';
    protected $fillable=['matricula_id','seccion_id'];

    public function matricula()
    {
        return $this->belongsTo('App\Matricula','matricula_id','id');
    }
    public function seccion()
    {
        return $this->belongsTo('App\Seccion','seccion_id','id');
    }
}
