<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntrevistaPostulante extends Model
{
    protected $table='entrevista_postulante';

    public function postulante()
    {
        return $this->belongsTo('App\Postulante','postulante_id','id');
    }
}
