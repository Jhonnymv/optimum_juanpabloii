<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitudes';
    protected $fillable = ['estado', 'descripcion', 'alumno_id', 'tramite_id', 'estado'];

    public function tramite()
    {
        return $this->belongsTo('App\Tramite', 'tramite_id', 'id');
    }

    public function anexoSolicitudes()
    {
        return $this->hasMany('App\AnexoSolicitud', 'solicitud_id', 'id');
    }

    public function flujos()
    {
        return $this->hasMany('App\Flujo', 'solicitud_id', 'id');
    }

    public function last_flujo()
    {
        return $this->hasMany('App\Flujo')->latest('id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuario');
    }
}
