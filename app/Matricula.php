<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $table='matriculas';
    protected $fillable=['alumno_carrera_id','periodo_id','estado','monto','observaciones'];


    public function alumnocarrera()
    {
        return $this->belongsTo('App\AlumnoCarrera','alumno_carrera_id','id');
    }
    public function periodo()
    {
        return $this->belongsTo('App\Periodo','periodo_id','id');
    }

    public function matriculadetalles()
    {
        return $this->hasMany('App\MatriculaDetalle','matricula_id','id');
    }
    public function licencia()
    {
        return $this->belongsto('App\Licencia','matricula_id','id');
    }

    public function categoria()
    {
        return $this->belongsto('App\Caregoria','categoria_id','id');
    }

    public function seccions()
    {
        return $this->belongsToMany(Seccion::class,'matricula_detalles')->withPivot('tipo')->withTimestamps();
    }
}
