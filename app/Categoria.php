<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $table='categorias';
    public $fillable = ['nombre','observacion', 'monto','periodo_id','carrera_id','estado'];

    public function matriculas()
    {
        return $this->hasMany('App\Matriculas','matricula_id','id');
    }

    public function periodo()
    {
        return $this->belongsTo('App\Periodo','periodo_id','id');
    }

    public function carrera()
    {
        return $this->belongsTo('App\Carrera','carrera_id','id');
    }

    public function alumnos()
    {
        return $this->belongsToMany('App\Alumno', 'alumno_categoria')->withPivot('id', 'estado')->withTimestamps();
    }
}
