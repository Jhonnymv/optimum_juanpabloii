<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    //

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario')->withTimestamps();
//        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
