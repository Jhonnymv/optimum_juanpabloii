<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    protected $table='carreras';

    public $fillable = ['nombre','resolucion','responsable_id','estado'];

//    public function responsable()
//    {
//        return $this->belongsTo('App\Responsable');
//    }

    public function cursos()
    {
        return $this->hasMany('App\Curso','carrera_id','id');
    }

    public function carreramatriculas()
    {
      return  $this->hasManyThrough('App\Matricula','App\AlumnoCarrera','carrera_id','alumno_carrera_id','id','id');
    }


    public function responsablesCarrera()
    {
        return $this->belongsToMany('App\Docente','carrera_docente','carrera_id','docente_id')->withPivot('fecha_inicio', 'fecha_fin','estado_gestion','docente_id')->withTimestamps();
    }


    public function area_academica()
    {
        return $this->belongsTo('App\Areas_academica','areas_academica_id','id');
    }


    public function pe_carrera()
    {
        return $this->hasMany('App\PeCarrera','carrera_id','id');
    }

    public function planes_estudio()
    {
        return $this->belongsToMany('App\PlanEstudio','pe_carreras','carrera_id','plan_estudio_id','id','id');
    }

    public function examenes()
    {
        return $this->hasMany('App\Examen','carrera_id','id');
    }

    public function cdocente(){
        return $this->belongsToMany('App\Docente','carrera_docente','carrera_id','docente_id')->withTimestamps();
    }

    public function categorias()
    {
        return $this->hasMany('App\Categoria','categoria_id','id');
    }
}
