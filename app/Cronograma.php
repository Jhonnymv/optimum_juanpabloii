<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model
{
    //
    protected $table='cronogramas';
    protected $fillable=['num_couta','fech_vencimiento','periodo_id','estado','concepto_id'];
    protected $dates=['fech_vencimiento'];

    public function pago()
    {
        return $this->belongsToMany('App\Pago')->withTimestamps();
    }

    public function concepto()
    {
        return $this->belongsTo('App\Concepto','concepto_id','id');
    }

    public function alumno()
    {
        return $this->belongsToMany('App\Alumno','alumno_pago')->withPivot('id','estado','pago_id')->withTimestamps();
    }

    public function alumnoPago()
    {
        return $this->hasMany(Alumnopago::class);
    }

    public function periodo()
    {
        return $this->belongsTo(Periodo::class);
    }
}
