<?php


namespace App\Traits;

use App\Pago;
use App\Sequence;
use Greenter\Model\Client\Client;
use Greenter\Model\Company\Address;
use Greenter\Model\Company\Company;
use Greenter\Model\Response\BillResult;
use Greenter\Model\Sale\Document;
use Greenter\Model\Sale\Note;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;
use Greenter\Ws\Services\SunatEndpoints;

use App\Traits\GreenterTrait;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;

trait SendNoteCredTrait
{
    use GreenterTrait;

    public function sendNoteCred($pagoId)
    {
        $pago = Pago::find($pagoId);

        $sequence = Sequence::where([['status', 1], ['doc_type', '07'], ['serie', 'like', 'B%']])->first();
        $final_result = [];
        $see = $this->getGreenter();
        $client = new Client();

//        $client->setTipoDoc($rental->document_type == '03' ? '1' : '6')
        $client->setTipoDoc($pago->tipo_doc == '03' ? '1' : '6')
            ->setNumDoc($pago->persona->DNI)
            ->setRznSocial(strtoupper($pago->persona->paterno . ' ' . $pago->persona->materno . ' ' . $pago->persona->nombres));

        $address = (new Address())
            ->setUbigueo('060101')
            ->setDepartamento('CAJAMARCA')
            ->setProvincia('CAJAMARCA')
            ->setDistrito('CAJAMARCA')
            ->setUrbanizacion('BAR. LA COLMENA')
            ->setDireccion('Jr. El Inca 524')
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 de lo contrario.
        $company = (new Company())
            ->setRuc('20607116114')
            ->setRazonSocial('INSTITUCION EDUCATIVA PARTICULAR BEATO JUAN PABLO II SOCIEDAD COMERCIAL DE RESPONSABILIDAD LIMITADA')
            ->setNombreComercial('COLEGIO JUAN PABLO II')
            ->setAddress($address);

        $note = new Note();
        $note
            ->setUblVersion('2.1')
            ->setTipoDoc('07') // Tipo Doc: Nota de Credito
            ->setSerie($sequence->serie) // Serie NCR
            ->setCorrelativo(str_pad(($sequence->current + 1), 6, "0", STR_PAD_LEFT)) // Correlativo NCR
            ->setFechaEmision(new \DateTime(date('Y-m-d H:i:s', strtotime($pago->created_at))))
            ->setTipDocAfectado('03') // Tipo Doc: Boleta
            ->setNumDocfectado($pago->serie . '-' . $pago->numero_doc) // Boleta: Serie-Correlativo
            ->setCodMotivo('01') // Catalogo. 09
            ->setDesMotivo('ANULACION DE LA OPERACION')
            ->setTipoMoneda('PEN')
            ->setCompany($company)
            ->setClient($client)
            ->setMtoOperInafectas($pago->fe_total_price)
            ->setMtoIGV(0)
            ->setTotalImpuestos(0)
            ->setMtoImpVenta($pago->fe_total_price);

        $details = [];
        foreach ($pago->alumnos as $alumno) {
            $item = (new SaleDetail())
                ->setCodProducto($alumno->persona->DNI)
                ->setUnidad('ZZ')
                ->setDescripcion($alumno->pivot->fe_descripcion)
                ->setCantidad(1)
                ->setMtoValorUnitario($alumno->pivot->fe_valor_venta)
                ->setMtoValorVenta($alumno->pivot->fe_valor_venta)
                ->setMtoBaseIgv($alumno->pivot->fe_valor_venta)
                ->setPorcentajeIgv(0)
                ->setIgv(0)
                ->setTipAfeIgv('30')
                ->setTotalImpuestos(0)
                ->setMtoPrecioUnitario($alumno->pivot->fe_valor_venta);
            array_push($details, $item);
        }
        $legend = new Legend();
        $legend->setCode('1000')
            ->setValue('SON DOSCIENTOS TREINTA Y SEIS CON 00/100 SOLES');

        $note->setDetails($details)
            ->setLegends([$legend]);

// Envio a SUNAT.
        $res = $see->send($note);

        Storage::disk('local')->put('fe_sunat/xml/' . $note->getName() . '.xml', $see->getFactory()->getLastXml());

        if (!$res->isSuccess()) {
            $final_result['status'] = 'error';
            $final_result['code'] = $res->getError()->getCode();
            $final_result['description'] = $res->getError()->getMessage();

            return $final_result;
        }

        /**@var $res \Greenter\Model\Response\BillResult */
        $cdr = $res->getCdrResponse();

        Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $note->getName() . '.zip', $res->getCdrZip());

        $code = (int)$cdr->getCode();

        $final_result['code'] = $code;

        if ($code === 0) {
            $final_result['status'] = 'accepted';
            if (count($cdr->getNotes()) > 0) {
                $final_result['observations'] = $cdr->getNotes();
            }
        } else if ($code >= 2000 && $code <= 3999) {
            $final_result['status'] = 'denied';
        } else {
            $final_result['status'] = 'exception';
        }

        $final_result['description'] = $cdr->getDescription();
        return $final_result;
    }
}

?>
