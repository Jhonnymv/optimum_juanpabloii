<?php
namespace App\Traits;

use App\Alumno;

//facturación
use App\Traits\GreenterTrait;


use App\Pago;
use Greenter\Model\Client\Client;
use Greenter\Model\Company\Company;
use Greenter\Model\Company\Address;
use Greenter\Model\Sale\Invoice;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;

trait SendDocSunatTrait
{
    use GreenterTrait;

    public function sendDocument($pagoId,$tipoConcepto=null)
    {
        $pago = Pago::find($pagoId);
        $final_result = [];
        $see = $this->getGreenter();
        // Cliente

        $client = new Client();
        if ($pago->tipo_doc == '03') {
            $client->setTipoDoc('1')
                ->setNumDoc($pago->persona->DNI)
                ->setRznSocial(strtoupper($pago->persona->paretno.' '.$pago->persona->materno.' '.$pago->persona->nombres));
        }
        if ($pago->tipo_doc == '01') {
            $client->setTipoDoc('6')
                ->setNumDoc($pago->ruc)
                ->setRznSocial(strtoupper($pago->social_reason));
        }
        // Emisor
        $address = new Address();
        $address->setUbigueo('060101')
            ->setDepartamento('CAJAMARCA')
            ->setProvincia('CAJAMARCA')
            ->setDistrito('CAJAMARCA')
            ->setUrbanizacion('-')
            ->setDireccion('EL INCA 524')
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 de lo contrario.

        $company = new Company();
        $company->setRuc('20607116114')
            ->setRazonSocial('INSTITUCION EDUCATIVA PARTICULAR BEATO JUAN PABLO II S.R.L')
            ->setNombreComercial('COLEGIO JUAN PABLO II')
            ->setAddress($address);

//        if ($checMat !== false || $checPen !== false) {
            $invoice = (new Invoice())
                ->setUblVersion('2.1')
                ->setTipoOperacion('0101')
                ->setTipoDoc($pago->tipo_doc)
                ->setSerie($pago->serie)
                ->setCorrelativo($pago->numero_doc)
                ->setFechaEmision(new \DateTime(date('Y-m-d H:i:s',strtotime($pago->created_at))))
                ->setTipoMoneda('PEN')
                ->setCompany($company)
                ->setClient($client)
                ->setMtoOperInafectas($pago->fe_total_price)
                ->setMtoIGV(0)
                ->setTotalImpuestos(0)
                ->setValorVenta($pago->fe_total_price)
                ->setSubTotal($pago->fe_total_price)
                ->setMtoImpVenta($pago->fe_total_price);
            $details = [];
            foreach ($pago->alumnos as $alumno) {
                $item = (new SaleDetail())
                    ->setCodProducto($alumno->persona->DNI)
                    ->setUnidad('ZZ')
                    ->setDescripcion($alumno->pivot->fe_descripcion)
                    ->setCantidad(1)
                    ->setMtoValorUnitario($alumno->pivot->fe_valor_venta)
                    ->setMtoValorVenta($alumno->pivot->fe_valor_venta)
                    ->setMtoBaseIgv($alumno->pivot->fe_valor_venta)
                    ->setPorcentajeIgv(0)
                    ->setIgv(0)
                    ->setTipAfeIgv('30')
                    ->setTotalImpuestos(0)
                    ->setMtoPrecioUnitario($alumno->pivot->fe_valor_venta);
                array_push($details, $item);
            }
//        } else {
//            $invoice = (new Invoice())
//                ->setUblVersion('2.1')
//                ->setTipoOperacion('0101') // Venta - Catalog. 51
//                ->setTipoDoc($request->tipo_doc) // Factura - Catalog. 01
//                ->setSerie($request->serie)
//                ->setCorrelativo($request->numero)
//                ->setFechaEmision(new \DateTime(date('Y-m-d H:i:s')))
//                ->setTipoMoneda('PEN') // Sol - Catalog. 02
//                ->setCompany($company)
//                ->setClient($client)
//                ->setMtoOperGravadas(number_format($request->total / 1.18, 2))
//                ->setMtoIGV(number_format($request->total - ($request->total / 1.18), 2))
//                ->setTotalImpuestos(number_format($request->total - ($request->total / 1.18), 2))
//                ->setValorVenta(number_format($request->total / 1.18, 2))
//                ->setSubTotal(number_format($request->total, 2))
//                ->setMtoImpVenta(number_format($request->total, 2));
//            $details = [];
//            for ($i = 0; $i < count($request->service_name); $i++) {
//                $item = (new SaleDetail())
//                    ->setCodProducto('P001')
//                    ->setUnidad('ZZ') // Unidad - Catalog. 03
//                    ->setCantidad($request->quantity[$i])
//                    ->setMtoValorUnitario(number_format($request->price[$i] / 1.18, 2))
//                    ->setDescripcion($request->service_name[$i])
//                    ->setMtoBaseIgv(number_format(($request->price[$i] / 1.18) * $request->quantity[$i], 2))
//                    ->setPorcentajeIgv(18.00) // 18%
//                    ->setIgv(number_format(($request->price[$i] - ($request->price[$i] / 1.18)) * $request->quantity[$i], 2))
//                    ->setTipAfeIgv('10') // Gravado Op. Onerosa - Catalog. 07
//                    ->setTotalImpuestos(number_format(($request->price[$i] - ($request->price[$i] / 1.18)) * $request->quantity[$i], 2)) // Suma de impuestos en el detalle
//                    ->setMtoValorVenta(number_format(($request->price[$i] / 1.18) * $request->quantity[$i], 2))
//                    ->setMtoPrecioUnitario(number_format($request->price[$i]));
//
//                array_push($details, $item);
//            }
//        }

        $valor = new  NumeroALetras();

        $legend = (new Legend())
            ->setCode('1000') // Monto en letras - Catalog. 52
            ->setValue($valor->toInvoice($pago->total, 2, 'soles'));
        $invoice->setDetails($details)
            ->setLegends([$legend]);

        $result = $see->send($invoice);
//        return $result;

        Storage::disk('local')->put('fe_sunat/xml/' . $invoice->getName() . '.xml', $see->getFactory()->getLastXml());

        if (!$result->isSuccess()) {
            $final_result['status'] = 'error';
            $final_result['code'] = $result->getError()->getCode();
            $final_result['description'] = $result->getError()->getMessage();

            return $final_result;
        }

        Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $invoice->getName() . '.zip', $result->getCdrZip());

        $cdr = $result->getCdrResponse();

        $code = (int)$cdr->getCode();

        $final_result['code'] = $code;

        if ($code === 0) {
            $final_result['status'] = 'accepted';
            if (count($cdr->getNotes()) > 0) {
                $final_result['observations'] = $cdr->getNotes();
            }
        } else if ($code >= 2000 && $code <= 3999) {
            $final_result['status'] = 'denied';
        } else {
            $final_result['status'] = 'exception';
        }

        $final_result['description'] = $cdr->getDescription();

        return $final_result;
    }

}

?>
