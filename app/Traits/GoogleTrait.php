<?php


namespace App\Traits;

trait GoogleTrait {
    
    public function processGoogleToken($request,$gClient){
        if ($request->get('code')){
            

            $accessToken = $gClient->fetchAccessTokenWithAuthCode($request->get('code'));            
            

            $request->session()->forget('token');
            // $request->session()->put('token', $gClient->getAccessToken());
            $request->session()->put('token', $accessToken);
            $gClient->setAccessToken($accessToken);

        }else{

            if ($request->session()->get('token'))
            {
                $gClient->setAccessToken($request->session()->get('token'));

                if ($gClient->isAccessTokenExpired()) {
                    if ($gClient->getRefreshToken()) {
                        $gClient->fetchAccessTokenWithRefreshToken($gClient->getRefreshToken());
                    }else{
                        $authUrl = $gClient->createAuthUrl();
                        return redirect()->to($authUrl);
                    } 
                }
            }else{
                $authUrl = $gClient->createAuthUrl();
                return redirect()->to($authUrl);
            }
        }

    }

    public function getGoogleClient($routeRedirect){
        $google_redirect_url = route($routeRedirect);
            
        $gClient = new \Google_Client();
        $gClient->setApplicationName(config('services.google.app_name'));
        $gClient->setClientId(config('services.google.client_id'));
        $gClient->setClientSecret(config('services.google.client_secret'));
        $gClient->setRedirectUri($google_redirect_url);
        $gClient->setDeveloperKey(config('services.google.api_key'));
        $gClient->setScopes(array(
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/classroom.courses',
            'https://www.googleapis.com/auth/classroom.topics'
        ));
        return $gClient;
    }

}

?>