<?php


namespace App\Traits;

use App\Pago;
use App\PostulacionAnexo;
use App\Sequence;
use Greenter\Model\Client\Client;
use Greenter\Model\Company\Address;
use Greenter\Model\Company\Company;
use Greenter\Model\Sale\Note;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;
use Greenter\Ws\Services\SunatEndpoints;

use App\Traits\GreenterTrait;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;

trait SendNoteDebTrait
{
    use GreenterTrait;

    public function sendNoteDeb($pagoId)
    {
//        $pago=Pago::find($pagoId);
//        $sequence = Sequence::where([['status', 1], ['doc_type', '08'], ['serie', 'like', 'F%']])->first();
//        $final_result = [];
//        $see = $this->getGreenter();
//        $client = new Client();
////        if ($rental->document_type == '03') {
//        $client->setTipoDoc('1')
//            ->setNumDoc($pago->persona->DNI)
//            ->setRznSocial(strtoupper($rental->entity->name));
////        }
//        $address = (new Address())
//            ->setUbigueo('150101')
//            ->setDepartamento('LIMA')
//            ->setProvincia('LIMA')
//            ->setDistrito('LIMA')
//            ->setUrbanizacion('-')
//            ->setDireccion('Av. Villa Nueva 221')
//            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 de lo contrario.
//        $company = (new Company())
//            ->setRuc('20123456789')
//            ->setRazonSocial('GREEN SAC')
//            ->setNombreComercial('GREEN')
//            ->setAddress($address);
//
//
//        $note = new Note();
//        $note
//            ->setUblVersion('2.1')
//            ->setTipDocAfectado('01')
//            ->setNumDocfectado($rental->serie . '-' . $rental->number)
//            ->setCodMotivo($rental->motive->code)
//            ->setDesMotivo($rental->motive->description)
//            ->setTipoDoc('08')
//            ->setSerie($sequence->serie)
//            ->setFechaEmision(new \DateTime(date('Y-m-d H:i:s', strtotime($rental->created_at))))
//            ->setCorrelativo(str_pad(($sequence->current + 1), 6, "0", STR_PAD_LEFT))
//            ->setTipoMoneda('PEN')
//            ->setCompany($company)
//            ->setClient($client)
//            ->setMtoOperGravadas($rental->subtotal)
//            ->setMtoIGV($rental->igv)
//            ->setTotalImpuestos($rental->igv)
//            ->setMtoImpVenta($rental->total);
//
//        $details = [];
//        foreach ($rental->equipments as $equipment) {
//            $item = (new SaleDetail())
//                ->setCodProducto($equipment->plate_number)
//                ->setUnidad('NIU')
//                ->setCantidad($equipment->pivot->quantity)
//                ->setDescripcion($equipment->pivot->description)
//                ->setMtoBaseIgv($equipment->pivot->subtotal)
//                ->setPorcentajeIgv(18.00)
//                ->setIgv($equipment->pivot->igv)
//                ->setTipAfeIgv('10')
//                ->setTotalImpuestos($equipment->pivot->igv)
//                ->setMtoValorVenta($equipment->pivot->subtotal)
//                ->setMtoValorUnitario($equipment->pivot->price)
//                ->setMtoPrecioUnitario(number_format($equipment->pivot->price+($equipment->pivot->price*0.18),2,'.',''));
//            array_push($details, $item);
//        }
//        $valor = new  NumeroALetras();
//
//        $legend = (new Legend())
//            ->setCode('1000') // Monto en letras - Catalog. 52
//            ->setValue($valor->toInvoice($rental->total, 2, 'soles'));
//        $note->setDetails($details)
//            ->setLegends([$legend]);
//
//// Envio a SUNAT.
//
//        $res = $see->send($note);
//
//        Storage::disk('local')->put('fe_sunat/xml/' . $note->getName() . '.xml', $see->getFactory()->getLastXml());
//
//        if (!$res->isSuccess()) {
//            $final_result['status'] = 'error';
//            $final_result['code'] = $res->getError()->getCode();
//            $final_result['description'] = $res->getError()->getMessage();
//
//            return $final_result;
//        }
//
//        /**@var $res \Greenter\Model\Response\BillResult */
//        $cdr = $res->getCdrResponse();
//
//        Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $note->getName() . '.zip', $res->getCdrZip());
//
//        $code = (int)$cdr->getCode();
//
//        $final_result['code'] = $code;
//
//        if ($code === 0) {
//            $final_result['status'] = 'accepted';
//            if (count($cdr->getNotes()) > 0) {
//                $final_result['observations'] = $cdr->getNotes();
//            }
//        } else if ($code >= 2000 && $code <= 3999) {
//            $final_result['status'] = 'denied';
//        } else {
//            $final_result['status'] = 'exception';
//        }
//
//        $final_result['description'] = $cdr->getDescription();
//       return $final_result;
    }
}

?>
