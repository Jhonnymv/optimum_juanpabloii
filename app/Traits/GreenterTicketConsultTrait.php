<?php


namespace App\Traits;

use App\Alumnopago;
use App\Pago;
use Greenter\Ws\Services\SunatEndpoints;
use Greenter\See;

use Greenter\XMLSecLibs\Certificate\X509Certificate;
use Greenter\XMLSecLibs\Certificate\X509ContentType;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


trait GreenterTicketConsultTrait
{
    use GreenterTrait;

    public function consult_ticket($pago)
    {
        $pag = Pago::find($pago);
        $see = $this->getGreenter();

        $statusResult = $see->getStatus($pag->fe_ticket);

        if (!$statusResult->isSuccess()) {
            // Si hubo error al conectarse al servicio de SUNAT.
//            $final_result['status'] = $statusResult->getError();
            return $statusResult->getError();
        }

        Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $pag->fe_cdr_name . '.zip', $statusResult->getCdrZip());

        $cdr = $statusResult->getCdrResponse();

        $code = (int)$cdr->getCode();

        $final_result['code'] = $code;

        if ($code === 0) {
            $final_result['status'] = 'accepted';
            if (count($cdr->getNotes()) > 0) {
                $final_result['observations'] = $cdr->getNotes();
            }
            DB::beginTransaction();
            $pag->updated([
                'fe_estado' => 'Anulado',
            ]);
            Alumnopago::where('pago_id', $pag->id)->updated([
                'estado' => 'Anulado'
            ]);

        } else if ($code >= 2000 && $code <= 3999) {
            $final_result['status'] = 'denied';
        } else {
            $final_result['status'] = 'exception';
        }

        $final_result['description'] = $cdr->getDescription();

        return response()->json(['descripcion' => $final_result], 200);
    }


}

?>
