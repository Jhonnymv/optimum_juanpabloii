<?php


namespace App\Traits;

use Greenter\Ws\Services\SunatEndpoints;
use Greenter\See;

use Greenter\XMLSecLibs\Certificate\X509Certificate;
use Greenter\XMLSecLibs\Certificate\X509ContentType;

use Illuminate\Support\Facades\Storage;


trait GreenterTrait
{

    public function getGreenter()
    {
        $see = new See();

        ///////////////////////////////PEM//////////////////////////////
//         $pem=Stor(config('greenter.pem_cert'));
//        $pem = Storage::get(config('greenter.pem_cert'));
//         $pem=file_get_contents(asset('fe_sunat/cert/certificate.pem'));
//        $see->setCertificate($pem);
        ///////////////////////////////////////////////////////////////

        ///////////////////////////////PFX//////////////////////////////
        // $pfx = file_get_contents('C:\Users\Mario\C1902276126.pfx');

        $p12 = Storage::get(config('greenter.p12_cert'));
//        $p12 = '/storage/fe_sunat/cert/certificado.p12';
        $password = config('greenter.p12_password');
        $certificate = new X509Certificate($p12, $password);
        $see->setCertificate($certificate->export(X509ContentType::PEM));
        ///////////////////////////////////////////////////////////////

        $see->setService(config('greenter.service_mode'));
        $see->setClaveSOL(config('greenter.ruc'), config('greenter.usuario_sol'), config('greenter.clave_sol'));
        return $see;
    }


}

?>
