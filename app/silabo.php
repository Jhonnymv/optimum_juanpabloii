<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class silabo extends Model
{
    // public $fillable = ['url_document','estado','seccion_id'];

 
    // public function pecurso()
    // {        
    //     return $this->belongsTo('App\PeCurso','pe_curso_id','id');
    // }

    public function seccion()
    {        
        return $this->belongsTo('App\Seccion','seccion_id','id');
    }
    public function periodoacademico()
    {        
        return $this->belongsTo('App\Periodo','periodo_id','id');        
    }

    public function unidades()
    {
        return $this->hasMany('App\unidades');
    }

    public function lista_verificacion()
    {
        return $this->belongsToMany('App\Indicador','lista_verificacion', 'silabo_id', 'indicadores_id')->withPivot('escala','detalles')->withTimestamps();
    }
    
    
    // public function docente()
    // {
    //     //return $this->belongsTo('App\silabo_docentes');
    //     return $this->belongsTo('App\Docente');
    // }
   
}
