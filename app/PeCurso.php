<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeCurso extends Model
{
    //
    public $fillable = ['horas_teoria','horas_practica','creditos','semestre',
                    'curso_id','pe_carrera_id','estado'];



    public function planestudio()
    {
      return $this->belongsTo('App\PlanEstudio','plan_estudio_id','id');
    }

    public function pe_carrera()
    {
         return $this->belongsTo('App\PeCarrera','pe_carrera_id','id');
    }

    public function curso()
    {
         return $this->belongsTo('App\Curso','curso_id','id');
    }

    public function prerrequisitos()
    {
        return $this->hasMany('App\Prerrequisito','pe_curso_id','id');
    }

    public function secciones()
    {
        return $this->hasMany('App\Seccion','pe_curso_id','id');
    }
// reporte plan estudios
    public function pecarrera()
    {
        return $this->belongsTo('App\PeCarrera', 'pe_carrera_id', 'id');
    }

}
