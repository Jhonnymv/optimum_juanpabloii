<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    public $fillable = ['descripcion','fecha_inicio','fecha_fin','estado','id'];
    public $dates = ['fecha_inicio','fecha_fin'];
    //public $fillable = ['nombre','apellido','email'];



    public function secciones()
    {
         return $this->hasMany('App\Seccion');
    }

    public function postulaciones()
    {
        return $this->hasMany('App\Postulacion');
    }

    public function matriculas()
    {
        return $this->hasMany('App\Matricula');
    }

    public function categorias()
    {
        return $this->hasMany('App\Matricula','categoria_id','id');
    }
}
