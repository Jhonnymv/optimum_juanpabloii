<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadAdministrativa extends Model
{

    public $fillable = ['descripcion','responsable'];

    public function usuario()
    {
        return $this->belongsTo('App\Usuario','responsable_id','id');
    }

}
