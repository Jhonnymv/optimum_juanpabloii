<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
    public function postulantes()
    {
        return $this->hasMany('App\Postulante');
    }
}
