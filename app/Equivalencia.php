<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equivalencia extends Model
{
    //
    public $fillable = ['curso_equiv_id','pe_curso_id','estado'];

    public function pecurso()
    {
      return $this->belongsTo('App\PeCurso','pe_curso_id','id');
    }
    public function cursoequiv()
    {
         return $this->belongsTo('App\Curso','curso_equiv_id','id');
    }

}
