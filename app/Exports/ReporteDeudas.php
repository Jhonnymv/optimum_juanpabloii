<?php

namespace App\Exports;

use App\Alumno;
use App\Periodo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class ReporteDeudas implements FromView
{

    public function view():View
    {
        $datos=collect();
        $alumnos=collect();
        $periodo= Periodo::where('estado',1)->first();

        $alumnos = Alumno::with('persona','padre')->whereHas('matricula.periodo', function ($q) use ($periodo) {
            $q->where('id', $periodo->id);
        })->get();
        //dd($alumnos->persona->padre);
        $OrderAlumnos = $alumnos->sortBy('persona.paterno');
//        $OrderAlumnos = $alumnos->sortBy('persona.paterno');

        foreach ($OrderAlumnos as $alumno) {
            $alumnoPago = count($alumno->alumnoPago);
            $data_padre=DB::select('SELECT pe.telefono FROM alumnos al inner join padres pa on al.padre_id = pa.id inner join personas pe on pa.persona_id = pe.id where al.id =:idalumno',['idalumno'=>$alumno->id]);
            $data_madre=DB::select('SELECT pe.telefono FROM alumnos al inner join padres pa on al.madre_id = pa.id inner join personas pe on pa.persona_id = pe.id where al.id =:idalumno',['idalumno'=>$alumno->id]);
            foreach($data_padre as $link)
            {
                $tp=$link->telefono;
            }
            foreach($data_madre as $link)
            {
                $tm=$link->telefono;
            }

            $dato = [
                'DNI' =>$alumno->persona->DNI,
                'telefono' =>$alumno->persona->telefono,
                'tpadre' =>$tp,
                'tmadre' =>$tm,
                'alumno' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ', ' . $alumno->persona->nombres,
                'nivel' => $alumno->alumnocarreras->last()->carrera->nombre,
//                'grado_id'=>Grade::where([['carrera_id',$alumno->alumnocarreras->last()->carrera_id],['grade',$alumno->alumnocarreras->last()->ciclo]])->first()->id,
                'grado' => $alumno->alumnocarreras->last()->ciclo,
                'grupo' => $alumno->alumnocarreras->last()->grupo,
                'matr' => $alumnoPago >= 1 ? '0' : $alumno->categoriaActiva->last()->monto,
                'mar' => $alumnoPago >= 2 ? '0' : $alumno->categoriaActiva->last()->monto,
                'abr' => $alumnoPago >= 3 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'may' => $alumnoPago >= 4 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jun' => $alumnoPago >= 5 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jul' => $alumnoPago >= 6 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'ago' => $alumnoPago >= 7 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'sep' => $alumnoPago >= 8 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'oct' => $alumnoPago >= 9 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'nov' => $alumnoPago >= 10 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'dic' => $alumnoPago >= 11 != null ? '0' : $alumno->categoriaActiva->last()->monto
            ];
            $datos->push($dato);
        }
        $data=$datos->groupBy(['nivel','grado','grupo'])->sortByDesc('nivel');

        $dat=[
            'data'=>$data
        ];


        return view('Reports.reportDeudasExcel', $dat);
    }
}
