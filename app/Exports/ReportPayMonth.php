<?php

namespace App\Exports;


use App\Pago;
use App\Periodo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportPayMonth implements FromView,ShouldAutoSize
{

    protected $periodId,$month;

    public function __construct(int $periodId,int $month)
    {
        $this->periodId = $periodId;
        $this->month = $month;
    }
    public function view(): View
    {
        $periodo=Periodo::find($this->periodId);
        $pagos=Pago::with('persona')
            ->whereMonth('created_at','=',$this->month)
            ->whereHas('cronograma',function ($q) use($periodo){
                $q->where('periodo_id',$periodo->id);
            })
            ->where('fe_total_price','>',0)
            ->where('fe_estado','<>','Anulado')
            ->get();
        return view('SecretariaAcademica.Reportes.xportPayMonth', [
            'pagos' => $pagos
        ]);
    }
}
