<?php

namespace App\Exports;

use App\Alumno;
use App\User;
use App\Usuario;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{

    public function view(): View
    {
        return view('users', [
            'users' => Usuario::all()
        ]);
    }
}
