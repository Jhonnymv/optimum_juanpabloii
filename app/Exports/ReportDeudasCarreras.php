<?php

namespace App\Exports;

use App\Alumno;
use App\Periodo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportDeudasCarreras implements FromView, ShouldAutoSize
{
    protected $periodo_id, $carrera_id;

    public function __construct(int $periodo_id, int $carrera_id)
    {
        $this->periodo_id = $periodo_id;
        $this->carrera_id = $carrera_id;
//        dd($this->carrera_id);
    }
    public function view():View
    {
//        dd($this->carrera_id);
        $datos=collect();
        $alumnos=collect();
        $periodo= Periodo::where('estado',1)->first();

//        $alumnos = Alumno::with('persona')->whereHas('matricula.periodo', function ($q) use ($periodo) {
//            $q->where('id', $periodo->id);
//        })->whereHas('alumnocarreras.carrera', function ($q) {
//            $q->where('id', 1);
//        })->get();
        $alumnos = Alumno::with('persona')->whereHas('matricula.periodo', function ($q)  {
            $q->where('id', $this->periodo_id);
        })->whereHas('alumnocarreras.carrera', function ($q) {
            $q->where('id', $this->carrera_id);
        })->get();
//        $OrderAlumnos = $alumnos->sortByDesc('persona.paterno');
        $OrderAlumnos = $alumnos->sortBy('persona.paterno');

        foreach ($OrderAlumnos as $alumno) {
            $alumnoPago = count($alumno->alumnoPago);

            $dato = [
                'DNI' =>$alumno->persona->DNI,
                'alumno' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ', ' . $alumno->persona->nombres,
                'nivel' => $alumno->alumnocarreras->last()->carrera->nombre,
//                'grado_id'=>Grade::where([['carrera_id',$alumno->alumnocarreras->last()->carrera_id],['grade',$alumno->alumnocarreras->last()->ciclo]])->first()->id,
                'grado' => $alumno->alumnocarreras->last()->ciclo,
                'grupo' => $alumno->alumnocarreras->last()->grupo,
                'matr' => $alumnoPago >= 1 ? '0' : $alumno->categoriaActiva->last()->monto,
                'mar' => $alumnoPago >= 2 ? '0' : $alumno->categoriaActiva->last()->monto,
                'abr' => $alumnoPago >= 3 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'may' => $alumnoPago >= 4 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jun' => $alumnoPago >= 5 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jul' => $alumnoPago >= 6 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'ago' => $alumnoPago >= 7 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'sep' => $alumnoPago >= 8 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'oct' => $alumnoPago >= 9 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'nov' => $alumnoPago >= 10 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'dic' => $alumnoPago >= 11 != null ? '0' : $alumno->categoriaActiva->last()->monto
            ];
            $datos->push($dato);
        }
        $data=$datos->groupBy(['nivel','grado','grupo']);

        $dat=[
            'data'=>$data
        ];


        return view('Reports.reportDeudasExcel', $dat);
    }
}
