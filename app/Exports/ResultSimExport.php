<?php

namespace App\Exports;

use App\ExamenAlumno;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ResultSimExport implements FromView, ShouldAutoSize
{
    protected $examId;
    public function __construct(int $examId)
    {
        $this->examId = $examId;
    }
    public function view(): View
    {
        $datos=collect();
        $exaAl=ExamenAlumno::where('examen_id',$this->examId)->get();
        foreach ($exaAl as $item) {
            $dato=[
                'alumno'=>$item->alumno->persona->DNI.' - '.$item->alumno->persona->paterno.' '.$item->alumno->persona->materno.' '.$item->alumno->persona->nombres,
                'seccion'=>$item->alumno->alumnocarreras->last()->grupo,
                'examen'=>$item->examen->mes_ano,
                'nota'=>number_format($item->nota,0),
            ];
            $datos->push($dato);
        }
        $data=$datos->sortBy(['seccion']);

        $dat=[
            'data'=>$data
        ];


        return view('LMS.Docente.Simulacros.resultados_excel', $dat);
    }
}
