<?php

namespace App\Exports;

use App\Usuario;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ReportBank implements FromView
{

    public function view(): View
    {
        $datos = collect();
        $padres = \App\Padre::with('persona')
            ->where('apoderado', 'si')
            ->get();
        foreach ($padres as $padre) {
            $sumamonto = 0;
            if (count($padre->alumnop) > 0) {
                foreach ($padre->alumnop as $item) {
                    if ($item->categorias->last()) {

                        $sumamonto += $item->categorias->last()->monto;
                        $alumno = $padre->alumnop->first();
                    }
                }
            }
            if (count($padre->alumnom) > 0) {
                foreach ($padre->alumnom as $item) {
                    if ($item->categorias->last()) {

                        $sumamonto += $item->categorias->last()->monto;
                        $alumno = $padre->alumnom->first();
                    }
                }
            }

            $cronogramas = \App\Cronograma::with('concepto')
                ->whereHas('concepto', function ($q) {
                    $q->where([['nombre', 'like', 'Pens%'], ['periodo_id', \App\Periodo::where('estado', '1')->first()->id]]);
                })
                ->get();
            $pagosCronId = $alumno->alumnoPago->pluck('cronograma_id');

            foreach ($cronogramas as $cronograma) {
                if (!$pagosCronId->contains($cronograma->id)) {
                    $data = [
                        'padre' => strtoupper($padre->persona->paterno . ' ' . $padre->persona->materno . ' ' . $padre->persona->nombres),
                        'DNI' => $padre->persona->DNI,
                        'concepto' => \Carbon\Carbon::parse($cronograma->fech_vencimiento)->month . '-' . strtoupper(\Carbon\Carbon::parse($cronograma->fech_vencimiento)->monthName),
                        'vencimiento' => \Carbon\Carbon::parse($cronograma->fech_vencimiento)->format('d-m-Y'),
                        'bloqueo' => '20-03-2022',
                        'importeMax' => $sumamonto,
                        'importeMin' => $sumamonto,
                    ];
                    $datos->push($data);
                }
            }
        }
        return view('Reports.reportBank', [
            'datos' => $datos
        ]);
    }
}
