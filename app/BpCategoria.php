<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BpCategoria extends Model
{

    public $timestamps=false;

    public function dbpreguntas()
    {
        return $this->hasMany('App\SbPregunta');
    }
}
