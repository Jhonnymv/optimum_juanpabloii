<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    public $fillable = ['nota','estado','fecha','condicion','observacion',
                    'alumnno_id','producto_instrumento_id'];
     
    public function producto_instrumento()
    {
        return $this->belongsTo('App\ProductoInstrumento','producto_instrumento_id','id');
    } 

    public function alumno()
    {
        return $this->belongsTo('App\Alumno','alumno_id','id');
    }
}

