<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComproElect extends Mailable
{
    use Queueable, SerializesModels;


    public $pago,$path;
    public $subject='';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pago,$path)
    {
        $this->pago=$pago;
        $this->path=$path;
        $this->subject='Comprobante de pago '.$pago->serie.'-'.$pago->numero_doc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('SecretariaAcademica.Comprobantes.ComprobantePagoMail')
            ->attach($this->path,
                [
                    'as'=> 'Comprobante_'.$this->pago->serie.'-'.$this->pago->numero_doc.'.pdf',
                    'mime' => 'application/pdf'
                ]);
    }
}
