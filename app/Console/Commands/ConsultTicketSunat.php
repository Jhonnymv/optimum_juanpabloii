<?php

namespace App\Console\Commands;

use App\Alumnopago;
use App\Pago;
use App\Traits\GreenterTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ConsultTicketSunat extends Command
{
    use GreenterTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consult:tsunat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'este códico consulta con sunat los tiques de las bajas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pagos = Pago::where('fe_estado', 'EnRevision')->get();
        foreach ($pagos as $pago) {
            $see = $this->getGreenter();

            $statusResult = $see->getStatus($pago->fe_ticket);

            if ($statusResult->isSuccess()) {
                Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $pago->fe_cdr_name . '.zip', $statusResult->getCdrZip());
//                DB::table('pagos')->where('id',$pago->id)->u
                $pago->update([
                    'persona_id'=>1,
                    'fe_estado' => 'Anulado',
                    'fe_sub_total'=>0,
                    'fe_total_price'=>0,
                ]);
                Alumnopago::where('pago_id', $pago->id)->update([
                    'estado' => 'Anulado',
                    'fe_precio_und'=>0,
                    'fe_valor_venta'=>0
                ]);
                Log::info('Aceptado');
            }else{
                Log::info('rechazado');
            }
        }
    }
}
