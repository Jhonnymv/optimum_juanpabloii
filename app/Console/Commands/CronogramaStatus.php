<?php

namespace App\Console\Commands;

use App\Cronograma;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CronogramaStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronograma:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cambia de estado los conogrmas vencidos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cronogramas=Cronograma::with('periodo')->where('estado','PENDIENTE')->whereDate('fech_vencimiento','<',Carbon::now())->update(['estado'=>'VENCIDO']);
        Log::info('cronograma actializado');
    }
}
