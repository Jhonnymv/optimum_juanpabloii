<?php

namespace App\Console\Commands;

use App\Alumnopago;
use App\Pago;
use App\Traits\GreenterTrait;
use App\Traits\SendDocSunatTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class reSendSunat extends Command
{
//    use GreenterTrait;
    use SendDocSunatTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resend:sunat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reenviar boletas a sunat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $statusResult = $this->sendDocument(597);
//        dd($statusResult);
        $pagos = Pago::where('fe_estado', 'Pendiente factura')->get();
        foreach ($pagos as $pago) {
            $statusResult = $this->sendDocument($pago->id);
            if ($statusResult['code']=='0') {
                $pago->update([
                    'fe_estado' => '0',
                ]);
                Log::info('Aceptado '.$statusResult['description']);
            }else{
                Log::info('Rechazado '.$statusResult['description']);
            }
        }
    }
}
