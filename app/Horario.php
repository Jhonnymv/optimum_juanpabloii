<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table='horario_detalles';
    protected $fillable = ['aula_id','dia','hora_inicio','hora_fin','seccion_id','turno'];

    public function aula()
    {
        return $this->belongsTo('App\Aula','aula_id','id');
    }

    public function seccion()
    {
        return $this->belongsTo('App\Seccion', 'seccion_id', 'id');
    }
}
