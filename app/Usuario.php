<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'usuario', 'estado', 'tipos', 'password', 'google_id'
    ];

    public function persona()
    {
         return $this->belongsTo('App\Persona');
    }

    public function modulos()
    {
        return $this->belongsToMany('App\Modulo')->withTimestamps();
//        return $this->belongsToMany(Modulo::class)->withTimestamps();
    }

    public function fullName()
    {
         $persona=$this->persona;
         return $persona->nombres.' '.$persona->paterno;
    }

    public function uAdministrativa()
    {
        return $this->hasMany('App\UnidadAdministrativa', 'responsable_id','id');
    }

//    public function modulo_usuario()
//    {
//        return $this->hasMany('App\ModuloUsuario', 'usuario_id', 'id');
//    }


}
