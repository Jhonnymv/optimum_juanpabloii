<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    protected $table='instrumentos';

    public $fillable = ['instrumento','estado'];

    // public function producto_intrumento(){
    // return $this->belongsTo('App\ProductoInstrumento','instrumento_id','id');

    // }
}
