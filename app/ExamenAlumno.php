<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenAlumno extends Model
{
    protected $table='examen_alumno';

    public function examen()
    {
        return $this->belongsTo('App\Examen','examen_id','id');
    }
    public function alumno()
    {
        return $this->belongsTo(Alumno::class,'alumno_id','id');
    }

    public function ep_detalles()
    {
        return $this->hasMany(EalDetalles::class,'examen_alumno_id','id');
    }
}
