<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table='pagos';
    public $fillable = ['id','persona_id','ruc','tipo_doc','serie','numero_doc','estado','fe_sub_total','fe_igv','fe_total_price','fe_estado','fe_ticket','fe_cdr_name','fe_referencia_id','observaciones','created_at','updated_at'];

    public function cronograma()
    {
        return $this->belongsToMany(Cronograma::class, 'alumno_pago')->withTimestamps();
    }

    public function alPagos()
    {
        return $this->hasMany(Alumnopago::class);
    }

    public function alumnos()
    {
        return $this->belongsToMany('App\Alumno','alumno_pago')->withPivot('fe_descripcion','fe_unidad','fe_igv','fe_precio_und','cantidad','fe_valor_venta','estado','cronograma_id')->withTimestamps();
    }

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

    public function alumno()
    {
        return $this->belongsToMany('App\Alumno','alumno_pago')->withPivot('id','estado')->withTimestamps();
    }
}
