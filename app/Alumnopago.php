<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnopago extends Model
{
    protected $table='alumno_pago';
    protected $fillable=['id','pago_id','alumno_id','cronograma_id','cantidad','fe_descripcion','fe_unidad','fe_precio_und','fe_igv','fe_valor_venta','estado'];
    public function alumno()
    {
        return $this->belongsTo('App\Alumno');
    }
    public function cronograma()
    {
        return $this->belongsTo('App\Cronograma','cronograma_id','id');
    }

    public function pago()
    {
        return $this->belongsTo('App\Pago','pago_id');
    }
}
