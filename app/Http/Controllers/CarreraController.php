<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Seccion;
use Illuminate\Http\Request;
use App\Carrera;
use App\Responsable;
use Illuminate\Support\Facades\Validator;

class CarreraController extends Controller
{
    public function index(){

        $carreras = Carrera::where('estado','1')->get();
//        $responsables = Responsable::where('estado','=','1')->get();

        return view('SecretariaAcademica.Carreras.index',[
            'carreras' => $carreras,
//            'responsables' => $responsables
        ]);
    }

    public function store(Request $request){

        $validar=Validator::make($request->all(),[
            'nombre' => 'required|unique:carreras|max:150',
            'resolucion' =>  'required|max:200',
        ]);
        if ($validar->fails()){
            return redirect()->back()->withInput()->withErrors($validar->errors());
        }

        $carrera = new Carrera([
            'nombre' => $request->nombre,
            'resolucion' => $request->resolucion,
//            'responsable_id' => $request->responsable,
            'estado' => 1
        ]);

        $carrera->save();

        return redirect()->route('carreras.index');
    }



    public function update(Request $request){

        $carrera = Carrera::find($id);

        $carrera->nombre = $request->nombreEdit;
        $carrera->resolucion = $request->resolucionEdit;
//        $carrera->responsable_id = $request->responsableEdit;
        $carrera->save();

        return redirect()->route('carreras.index');
    }


    public function getCarrera(Request $request){

        $carrera = Carrera::find($request->id);

        return response()->json(['carrera'=> $carrera], 200);
    }

}
