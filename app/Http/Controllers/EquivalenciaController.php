<?php

namespace App\Http\Controllers;

use App\Equivalencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carrera;
use App\PeCurso;
use App\PlanEstudio;
use App\Curso;
use App\Prerrequisito;

class EquivalenciaController extends Controller
{
    public function index($envpecursoid,$envcursoid,$envcarreraid,$envpeid){         
             
        $equivalencias = Equivalencia::where([
               ['estado','=','1'],
               ['pe_curso_id','=',$envpecursoid]
            ])->get();
            
         $cursosequiv=PeCurso::select('cursos.id',
         'cursos.nombre')
           ->join('cursos','cursos.id','pe_cursos.curso_id')->where([
           ['pe_cursos.estado','=','1'],
           ['pe_cursos.plan_estudio_id','=',$envpeid],
           ['pe_cursos.carrera_id','=',$envcarreraid]
        ])->get();
   
        $pecursos=PeCurso::where(
            'id','=',$envpecursoid
        );
            return view('Admin.Equivalencias.index',[
                'equivalencias' => $equivalencias,
                'cursosequiv'=>$cursosequiv,
                'pecursos'=>$pecursos             
            ]);
        }
    
        public function store(Request $request){
            $validar = $request->validate([
                
            ]);
    
            $equivalencia = new Equivalencia([ 
                'curso_equiv_id' => $request->cursosequiv,
                'pe_curso_id' =>$request->pecursosid,                                   
                'estado' => 1
            ]);
      //dd($request->pecursos);
            $equivalencia->save();
          
            return redirect()->route('equivalencias.index',
            array('envpecursoid'=>$request->pecursosid,
            'envcursoid'=>$request->cursoid,
            'envcarreraid'=>$request->carreraid,
            'envpeid'=>$request->peid));
    
        }
    
        public function delete($id,$envpecursoid,$envcursoid,$envcarreraid,$envpeid){
    
           // $pecarrera = PeCarrera::where('plan_estudio_id','=',$plan_estudio_id)
             //                    ->where('carrera_id','=',$carrera_id)->first();
             $equivalencia = Equivalencia::find($id);
            if($equivalencia != null){
                $equivalencia->estado = 0;  
                //dd($pecarrera);          
                $equivalencia->save();
    
            }
    
            return redirect()->route('equivalencias.index',array(
            'envpecursoid'=>$envpecursoid,
            'envcursoid'=>$envcursoid,
            'envcarreraid'=>$envcarreraid,
            'envpeid'=>$envpeid));
        }
}
