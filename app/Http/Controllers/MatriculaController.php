<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use App\Alumnopago;
use App\Carrera;
use App\Concepto;
use App\Cronograma;
use App\Curso;
use App\Grade;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\Padre;
use App\PeCarrera;
use App\PeCurso;
use App\Periodo;
use App\Prerrequisito;
use App\MatriculaDetalle;
use App\Seccion;
use App\Voucher;
use Facade\FlareClient\Report;
use http\Client\Curl\User;
use MPDF;
use PDF;
use Dompdf\Dompdf;
use Redirect;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MatriculaController extends Controller
{
    public function create()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $alumno_grado = $alumno->alumnocarreras;
//        $al_ciclo1=AlumnoCarrera::where([['alumno_id',$alumno_id],['ciclo',1]])->first();
//        if ($al_ciclo1) {
        $periodo = Periodo::where('estado', 1)->first();
        if ($periodo) {
            $matricula = Matricula::whereHas('AlumnoCarrera', function ($query) use ($alumno_id, $alumno) {
                $query->where([['alumno_id', $alumno_id]]);
            })
                ->where(function ($query) use ($periodo) {
                    $query->where('periodo_id', $periodo->id);
                })
                ->first();
            if (!$matricula) {
                return view('IntranetAlumno.matriculas.create', ['carreras' => $alumno->carreras, 'periodo' => $periodo, 'carreras' => $alumno->carreras, 'alumno' => $alumno, 'grado' => $alumno_grado]);
            } else {
                return view('IntranetAlumno.matriculas.create', ['matricula' => $matricula, 'carreras' => $alumno->carreras, 'periodo' => $periodo, 'alumno' => $alumno, 'grado' => $alumno_grado]);
            }
        }
//        } else
        return view('IntranetAlumno.matriculas.index');

    }

    public function store(Request $request)
    {
//        dd($request);

//        try {
        $alumno = Auth::user()->persona->alumno;
        $alumno_carrera = AlumnoCarrera::where([['alumno_id', $alumno->id], ['carrera_id', $request->carrera_id]])->first();

        $periodo = Periodo::where('estado', 1)->first();

        DB::beginTransaction();
        $matricula = new Matricula();
        $matricula->alumno_carrera_id = $alumno_carrera->id;
        $matricula->periodo_id = $periodo->id;
        $matricula->estado = 'POR-PAGAR';
        $matricula->categoria_id = '1';
        $matricula->save();
        $matricula_id = $matricula->id;
        $pago = 120;
        $contador = 0;
        for ($i = 0; $i < count($request->seccion_id); $i++) {
//            if ($request->tipo[$i] == 'S' && $request->ciclo[$i] >= 5) {
//                DB::table('matriculas')
//                    ->where('id', $matricula_id)
//                    ->update(['estado' => 'PRE-MATRICULA']);
//            } else {
//                $pago = 120;
//                $contador=0;
            if ($request->tipo[$i] == 'S') {
                $contador += 1;
            }
//                if($contador>1){
//                    $pago=($contador-1)*60;
//                }else{
//                    $pago;
//                }
//                DB::table('matriculas')
//                    ->where('id', $matricula_id)
//                    ->update(['monto' => $pago]);
//            }
            DB::table('matricula_detalles')->insert([
                'matricula_id' => $matricula_id,
                'seccion_id' => $request->seccion_id[$i],
                'tipo' => $request->tipo[$i]
            ]);
            DB::table('secciones')->where('id', $request->seccion_id[$i])->decrement('cupos', 1);
            DB::table('secciones')->where('id', $request->seccion_id[$i])->increment('n_matriculados', 1);
        }
        if ($contador > 1) {
            $pago += 60 * ($contador - 1);
        } else {
            $pago += 0;
        }
        DB::table('matriculas')
            ->where('id', $matricula_id)
            ->update(['monto' => $pago]);
        DB::commit();
        return back();
//        } catch (QueryException $e) {
//            dd('error');
//        }
    }

    public function reporte()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;

        $periodo = Periodo::where('estado', 1)->first();
        if ($periodo) {
            $matricula = Matricula::whereHas('AlumnoCarrera', function ($query) use ($alumno_id) {
                $query->where('alumno_id', $alumno_id);
            })
                ->where(function ($query) use ($periodo) {
                    $query->where('periodo_id', $periodo->id);
                })
                ->first();
            // if (!$matricula) {
            //     return view('IntranetAlumno.matriculas.create', ['carreras' => $alumno->carreras, 'periodo' => $periodo]);
            // } else {
            //     return view('IntranetAlumno.matriculas.create', ['matricula' => $matricula]);
            // }
        }
        return view('IntranetAlumno.matriculas.ReporteMatricula', [
            'carreras' => $alumno->carreras, 'periodo' => $periodo,

            'periodosAcademicos' => $periodo
        ]);

        //return view('IntranetAlumno.matriculas.ReporteMatricula');

    }

    public function reporteb($alumno_id = '', $matricula_id = '')

    {
//        $padre = Auth::user()->persona->padre;
//        $alumno = Auth::user()->persona->alumno;
        if (Auth::user()->persona->alumno) {
            $alumno = Auth::user()->persona->alumno;
        } else {
            $alumno = Alumno::find($alumno_id);
        }
        $matricula_id = $alumno->matricula->last()->id;
//        $alumno_id = $alumno->id;
//        $alumno_carrera = AlumnoCarrera::where('alumno_id', $alumno->id)->last();
        $periodo = Periodo::where('estado', 1)->first();
        //dd($alumno->carreras->first()->nombre);
        if ($periodo) {
            $matricula = Matricula::find($matricula_id);
//            whereHas('AlumnoCarrera', function ($query) use ($alumno_id) {
//                $query->where('alumno_id', $alumno_id);
//            })
//                ->where(function ($query) use ($periodo) {
//                    $query->where('periodo_id', $periodo->id);
//                })
//                ->first();
        }
        if ($matricula) {
            $detalles = MatriculaDetalle::where('matricula_id', $matricula->id)->get();
        }
        //dd($detalles);
        if ($matricula) {
            $data = [
                'carreras' => $alumno->carreras,
                'alumno' => $alumno,
                'matricula' => $matricula,
                'detalles' => $detalles,
                'periodo' => $periodo
            ];
//            dd($data);
            if ($alumno_id) {
                $pdf = Mpdf::loadView('SecretariaAcademica.Matriculas.rpt_matricula', $data);
                return $pdf->stream();
            } else {
                $pdf = Mpdf::loadView('IntranetAlumno.matriculas.reporteFichaMat', $data);
                return $pdf->stream();
            }
        } else {
            return redirect()->back()->with('status', 'Aún no registra matrícual');
        }
        //     return view('IntranetAlumno.matriculas.ReporteFichaMat', [
        //         'carreras' => $alumno->carreras,
        //         'alumno' => $alumno,
        //      'matricula' => $matricula,
        //      'detalles' => $detalles,
        //      'periodo' => $periodo
        //  ]);

        //return view('IntranetAlumno.matriculas.ReporteMatricula');
    }

    public function validprematricula()
    {
//        $carreras = Carrera::where('estado', 1)->get();
        $periodo = Periodo::where('estado', 1)->first();

        $matriculas = AlumnoCarrera::all();

//        $matriculas = Matricula::whereHas("matriculadetalles.seccion.pe_curso", function ($query) {
//            $query->where('semestre', '<', 11);
//        })->where('periodo_id', $periodo->id)
//            ->get();
        return view('SecretariaAcademica.Matriculas.v_pre_matricula', ['matriculas' => $matriculas]);

    }

    public function validar_sub_5(Request $request)
    {
        $periodo = Periodo::where('estado', 1)->first();
        $pe_curso = $request->pe_curso_id;
        if ($request->wantsJson()) {
            $matriculas = Matricula::with('matriculadetalles.seccion.pe_curso', 'alumnocarrera.alumno.persona', 'alumnocarrera.carrera')->whereHas("matriculadetalles", function ($query) {
                $query->where('tipo', 'S');
            })->whereHas("matriculadetalles.seccion.pe_curso", function ($query) use ($pe_curso) {
                $query->where('id', $pe_curso);
            })->where([['estado', 'PRE-MATRICULA'], ['periodo_id', $periodo->id]])
                ->get();
            return response()->json(['matriculas' => $matriculas], 200);
        }
        $pe_cursos = PeCurso::whereHas('secciones.matricula_detalles', function ($query) {
            $query->where('tipo', 'S');
        })->where('semestre', '>=', 5)->get();

        return view('SecretariaAcademica.Matriculas.ver_subsanatorios', ['pe_cursos' => $pe_cursos]);
    }

    public function validarpago()
    {
////        $carreras=Carrera::where('estado',1)->get();
        $periodo = Periodo::where('estado', 1)->first();
        $matriculas = Matricula::where([['estado', 'PAGADA'], ['periodo_id', $periodo->id]])->get();
//        dd($matriculas);

        return view('AreaContabilidad.Matriculas.v_pagos', ['matriculas' => $matriculas]);
    }

    public function actualizarestado(Request $request)
    {
        $matricula = Matricula::find($request->id);
        $matricula->estado = $request->estado;
        if ($request->observaciones) {
            $matricula->observaciones = $request->observaciones;
        }
        if ($request->monto) {
            $matricula->monto = $request->monto;
        }
        $matricula->save();
        return response()->json('success', 200);
//        return response()->json($matricula, 200);
    }

//    public function listprematricula($carrera_id)
//    {
//        $carrera=Carrera::find($carrera_id);
//        $matriculas=$carrera->carreramatriculas()->where('estado',1)->get();
////        $matriculas=$matriculas->with('');
//        dd($matriculas);
//    }
    public function RptHorario($matricula_id = '')
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $periodo = Periodo::where('estado', 1)->first();

        if ($periodo) {
//            $matricula = Matricula::whereHas('AlumnoCarrera', function ($query) use ($alumno_id) {
//                $query->where('alumno_id', $alumno_id);
//            })
//                ->where(function ($query) use ($periodo) {
//                    $query->where('periodo_id', $periodo->id);
//                })
//                ->first();
            $matricula = Matricula::find($matricula_id);
            $data = [
                'matricula' => $matricula,
                'alumno' => $alumno,
                'persona' => $alumno->persona,
                'periodo' => $periodo,
            ];
            $pdf = PDF::loadView('IntranetAlumno.matriculas.ReporteHorario', $data);
            return $pdf->stream();
        } else {
            return redirect()->back();
        }

    }

    public function view_rptxseccion(Request $request)
    {
        if ($request->wantsJson()) {
            $registrados=AlumnoCarrera::with('alumno.persona','carrera')->where([['carrera_id',$request->nivel_id],['ciclo',$request->grado],['grupo',$request->grupo]])->get();

            return response()->json(['registrados'=>$registrados],200);


//            return $request;
//            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera', 'matriculadetalles.seccion.pe_curso.curso')->whereHas('matriculadetalles.seccion', function ($query) use ($request) {
//                $query->where([['grupo', $request->grupo], ['periodo_id', $request->periodo_id]]);
//            })->whereHas('matriculadetalles.seccion.pe_curso', function ($query) use ($request) {
//                $query->where('semestre', $request->semestre);
//            })->whereHas('matriculadetalles.seccion.pe_curso.curso', function ($query) use ($request) {
//                $query->where('carrera_id', $request->programa_id);
//            })->where('estado', 'CONFIRMADA')->get();
//            return response()->json(['matriculas' => $matriculas], 200);
        };
        $periodos = Periodo::all();
        $carreras = Carrera::all();
        return view('SecretariaAcademica.Matriculas.rpt_matriculadosxseccion', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function rptxseccionpdf($periodo_id,$nivel_id,$grado,$grupo)
    {
        $periodos = Periodo::find($periodo_id);
        $carrera = Carrera::find($nivel_id);
        $registrados=AlumnoCarrera::with('alumno.persona','carrera')->where([['carrera_id',$nivel_id],['ciclo',$grado],['grupo',$grupo]])->get();
        $data = [
            'alumnos' => $registrados,
            'periodo' => $periodos,
            'carrera' => $carrera,
            'grado' =>$grado,
            'grupo'=>$grupo
        ];
//        return $data;

        $pdf = PDF::loadView('SecretariaAcademica.Matriculas.rptxseccionpdf', $data);
        return $pdf->stream();

    }

    public function view_rptxperiodo(Request $request)
    {
        if ($request->wantsJson()) {
            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera')
                ->where([['periodo_id', $request->periodo_id], ['estado', 'CONFIRMADA']])
                ->get();
            return response()->json(['matriculas' => $matriculas], 200);
        }

        $periodos = Periodo::all();
        return view('SecretariaAcademica.Matriculas.rpt_matriculadosxperiodo', ['periodos' => $periodos]);
    }

    public function rptxperiodo($periodo_id)
    {
        $matriculas = Matricula::where([['periodo_id', $periodo_id], ['estado', 'CONFIRMADA']])->get();
        $periodo = Periodo::find($periodo_id);
        $data = [
            'matriculas' => $matriculas,
            'periodo' => $periodo
        ];
        $pdf = PDF::loadView('SecretariaAcademica.Matriculas.html_rpt_matriculas', $data);
        return $pdf->stream();
    }

    public function view_rptxespecialidad(Request $request)
    {
        if ($request->wantsJson()) {
            $carrera_id = $request->carrera_id;
            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera')->whereHas('alumnocarrera', function ($query) use ($carrera_id) {
                $query->where('carrera_id', $carrera_id);
            })->where([['periodo_id', $request->periodo_id], ['estado', 'CONFIRMADA']])->get();
            return response()->json(['matriculas' => $matriculas], 200);
        }

        $periodos = Periodo::all();
        $carreras = Carrera::where('estado', 1)->get();
        return view('SecretariaAcademica.Matriculas.rpt_matriculadosxespecialidad', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function rptxespecialidad($periodo_id, $carrera_id)
    {
        if ($carrera_id && $carrera_id) {

            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera')->whereHas('alumnocarrera', function ($query) use ($carrera_id) {
                $query->where('carrera_id', $carrera_id);
            })->where([['periodo_id', $periodo_id], ['estado', 'CONFIRMADA']])->get();
            $carrera = Carrera::find($carrera_id);
            $periodo = Periodo::find($periodo_id);
            $data = [
                'matriculas' => $matriculas,
                'periodo' => $periodo,
                'carrera' => $carrera
            ];
            $pdf = PDF::loadView('SecretariaAcademica.Matriculas.html_rpt_matriculas', $data);
            return $pdf->stream();
        } else return redirect()->back();
    }

    public function view_rptxnivel(Request $request)
    {
        if ($request->wantsJson()) {
            $carrera_id = $request->carrera_id;
            $nivel = Grade::find($request->nivel)->grade;
            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera')
                ->whereHas('alumnocarrera', function ($query) use ($nivel) {
                    $query->where('ciclo', $nivel);
                })->whereHas('alumnocarrera', function ($query) use ($carrera_id) {
                    $query->where('carrera_id', $carrera_id);
                })->where([['periodo_id', $request->periodo_id], ['estado', 'CONFIRMADA']])->get();
            return response()->json(['matriculas' => $matriculas], 200);
        }
        $periodos = Periodo::all();
        $carreras = Carrera::where('estado', 1)->get();
        return view('SecretariaAcademica.Matriculas.rpt_matriculadosxnivel', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function rptxnivel($periodo_id, $carrera_id, $nivel)
    {
//        return $carrera_id. $carrera_id . $nivel;
        if ($periodo_id && $carrera_id && $nivel) {
            $grado = Grade::find($nivel)->grade;
            $matriculas = Matricula::with('alumnocarrera.alumno.persona', 'alumnocarrera.carrera')
                ->whereHas('alumnocarrera', function ($query) use ($grado) {
                    $query->where('ciclo', $grado);
                })->whereHas('alumnocarrera', function ($query) use ($carrera_id) {
                    $query->where('carrera_id', $carrera_id);
                })->where([['periodo_id', $periodo_id], ['estado', 'CONFIRMADA']])->get();

            $carrera = Carrera::find($carrera_id);
            $periodo = Periodo::find($periodo_id);
            $data = [
                'matriculas' => $matriculas,
                'periodo' => $periodo,
                'carrera' => $carrera,
                'nivel' => $nivel
            ];
            $pdf = PDF::loadView('SecretariaAcademica.Matriculas.html_rpt_matriculas', $data);
            return $pdf->stream();
        } else return redirect()->back();
    }

    public function confirmar_matricula(Request $request)
    {
        DB::beginTransaction();
        $extension = $request->file('img')->getClientOriginalExtension();
        $nombre_archivo = $request->matricula_id . '_boleta_' . $request->num_operacion . '.' . $extension;

        $voucher = new Voucher();
        $voucher->fecha = $request->fecha;
        $voucher->num_operacion = $request->num_operacion;
        $voucher->monto = $request->monto;
        $voucher->tipo = 'boleta';
        $voucher->urlimg = Auth::user()->id . '/' . $nombre_archivo;
        $voucher->matricula_id = $request->matricula_id;
        $voucher->save();

        $matricula = Matricula::find($request->matricula_id);
        $matricula->estado = "CONFIRMADA";
        $matricula->observaciones = '';
        $matricula->save();
        DB::commit();
        $request->file('img')->storeAs('public/' . Auth::user()->id . '/', $nombre_archivo);
        return back();
    }

    public function my_historial()
    {
        $alumno = Auth::user()->persona->alumno;
        $matriculas = Matricula::whereHas('alumnocarrera', function ($q) use ($alumno) {
            $q->where('alumno_id', $alumno->id);
        })->get()->sortBy('alumnocarrera.alumno.persona.paterno');
        return view('IntranetAlumno.matriculas.historial_matriculas', ['matriculas' => $matriculas]);
    }

    public function Ver_Pagos_Alumnos()
    {
        $periodo = Periodo::where('estado', 1)->first();
        $matriculas = Matricula::where('periodo_id', $periodo->id)->get();
        return view('AreaContabilidad.Pagos.ver_pagos', ['matriculas' => $matriculas]);
    }

    public function report_plan_studios($matricula_id = '')
    {
        $alumno = Auth::user()->persona->alumno;

        $periodo = Periodo::whereHas('matriculas', function ($q) use ($matricula_id) {
            $q->whereId($matricula_id);
        })->first();

        $pe_carrera = PeCarrera::whereHas('pe_curso.secciones.matricula_detalles', function ($q) use ($matricula_id) {
            $q->where('matricula_id', $matricula_id);
        })->first();

        $data = [
            'pe_carrera' => $pe_carrera,
            'alumno' => $alumno,
            'periodo' => $periodo
        ];

        $pdf = PDF::loadView('IntranetAlumno.matriculas.rpt_plan_estudios', $data);
        return $pdf->stream();
    }

    // Escuelas
//    public function createEscuela()
//    {
//        $alumno = Auth::user()->persona->alumno;
//        $alumno_id = $alumno->id;
//        $periodo = Periodo::where('estado', 1)->first();
//        if ($periodo) {
//            $matricula = Matricula::whereHas('AlumnoCarrera', function ($query) use ($alumno_id) {
//                $query->where([['alumno_id', $alumno_id]]);
//            })
//                ->where(function ($query) use ($periodo) {
//                    $query->where('periodo_id', $periodo->id);
//                })
//                ->first();
//            if (!$matricula) {
//                return view('IntranetAlumno.matriculas.createEscuelas', ['carreras' => $alumno->carreras, 'periodo' => $periodo, 'alumno'=>$alumno]);
//            } else {
//                return view('IntranetAlumno.matriculas.createEscuelas', ['matricula' => $matricula,'carreras' => $alumno->carreras, 'periodo' => $periodo,'alumno'=>$alumno]);
//            }
//        }
//        return view(IntranetAlumno'IntranetAlumno.matriculas.index');
//    }

    public function crear($alumno_id)
    {
        $periodo = Periodo::where('estado', 1)->first();
        $AlumnoPago = Alumnopago::with('pago', 'alumno')
            ->where('alumno_id', $alumno_id)
//            ->whereHas('cronograma', function ($q) use ($periodo) {
//                $q->where('periodo_id', $periodo->id);)}
            ->get();
        //dd($AlumnoPago);


        if (count($AlumnoPago) == 0) {
            $Alumno = Alumno::where('id', $alumno_id)->first();
            $tpadre = Padre::find($Alumno->padre_id);
            $tmadre = Padre::find($Alumno->madre_id);
            if ($tpadre->apoderado === 'si') {
                $padre_id = $tpadre->id;
            } else {
                $padre_id = $tmadre->id;
            }

            $cronogramas = Cronograma::with('concepto')->whereHas('concepto', function ($q) {
                $q->where([['estado', 1], ['nombre', 'like', '%Matr%']])->orwhere('nombre', 'like', '%MATR%');
            })->get();
            return view('IntranetPadre.Matriculas.create', ['alumno' => $Alumno, 'alumno_id' => $alumno_id, 'cronogramas' => $cronogramas, 'padre_id' => $padre_id]);
        } else {
            return back();
            //return view('IntranetPadre.Matriculas.create', ['alumno' => $AlumnoPago, 'alumno_id' => $alumno_id, 'padre_id' => $AlumnoPago->first()->alumno->padre_id]);
        }
    }
}
