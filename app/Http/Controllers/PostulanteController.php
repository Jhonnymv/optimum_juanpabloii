<?php

namespace App\Http\Controllers;
use App\Carrera;
use App\Departamento;
use App\Distrito;
use App\Periodo;
use App\Persona;
use App\Postulacion;
use App\PostulacionAnexo;
use App\Postulante;
use App\Provincia;
use App\Voucher;
use FontLib\Table\Type\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostulanteController extends Controller
{
    //
    public function listPostulantes()
    {
        $carreras = Carrera::all();
        $postulantes = Postulante::with('persona','postulaciones.pe_carrera.carrera')->whereDoesntHave('postulaciones',function ($q){
            $q->where('estado','Solicitado');
        })->where('condicion','Ordinario')->get();
        return view('Admisiones.list_postulantes',['postulantes'=>$postulantes, 'carreras'=>$carreras]);
    }
    public function detalles($id){
        $postulante = Postulante::find($id);
        $distrito= Distrito::with('provincia.departamento')->where('id', $postulante->distrito_id)->get();
        $periodo= Periodo::where('estado',1)->first();
        $anexos = PostulacionAnexo::whereHas('postulacion', function ($q) use ($id){
            $q->where('postulante_id',$id);
        })->get();

        return view('Admisiones.detalles_postulante', ['postulante'=>$postulante,'departamento'=>$distrito,'periodo'=>$periodo,'anexos'=>$anexos]);
    }

    public function listPostulantesCarrera(Request $request)
    {
        $postulantes = Postulante::with('persona','postulaciones.pe_carrera.carrera')->whereDoesntHave('postulaciones',function ($q){
            $q->where('estado','Solicitado');
        })->whereHas('postulaciones.pe_carrera.carrera', function ($q) use ($request){
            $q->where('id',$request->carrera_id);
        })->where('condicion','Ordinario')->get();
        return response()->json(['postulantes'=>$postulantes]);
    }

    public function listPostulantesExonerado(){
        $carreras = Carrera::all();
        $postulantes = Postulante::with('persona','postulaciones.pe_carrera.carrera')->whereDoesntHave('postulaciones',function ($q){
            $q->where('estado','Solicitado');
        })->where('condicion','Exonerado')->get();
        return view('Admisiones.list_postulantes_exonerado',['postulantes'=>$postulantes, 'carreras'=>$carreras]);
    }

    public function listPostulantesExoneradoCarrera(Request $request){
        $postulantes = Postulante::with('persona','postulaciones.pe_carrera.carrera')->whereDoesntHave('postulaciones',function ($q){
            $q->where('estado','Solicitado');
        })->whereHas('postulaciones.pe_carrera.carrera', function ($q) use ($request){
            $q->where('id',$request->carrera_id);
        })->where(['condicion','=','Exonerado'],['tipo_exoneracion','=',$request->tipo_exoneracion])->get();
        return response()->json(['postulantes'=>$postulantes]);
    }

    public function validarPostulante(Request $request){
        dd($request);
        $postulante = Postulante::findOrFail($request->idPostulante);
        $postulante->observaciones = $request->observaciones;
        $postulante->observaciones = $request->observaciones;
        $postulante->save();
        $postulaciones = Postulacion::findOrFail($request->idPostulante);
        $postulaciones->estado= 'Confirmado';
        $postulaciones->save();;
        if($postulante->codicion=='Ordiario'){
            return $this->listPostulantes();
        }else{
           return $this->listPostulantesExonerado();
        }
    }

    public function reportes(){
        return view('Admisiones.admision_reportes');
    }
    public function actualizarestado(Request $request)
    {
        $postulaciones = Postulacion::find($request->id);
        $postulaciones->estado = $request->estado;
        $postulaciones->save();
        DB::beginTransaction();

        $voucher = new Voucher();
        $voucher->fecha = $postulaciones->fecha_pos;
        $voucher->tipo = 'boleta';
        $voucher->num_operacion = $request->boleta;
        //$voucher->monto = $request->monto;
        //$voucher->urlimg = Auth::user()->id . '/' . $nombre_archivo;
        //$voucher->matricula_id = $licencia->id;
        $voucher->postulacion_id = $request->id;
        //$ldate = date('Y-m-d H:i:s');
        //$date = Carbon::now()->toDateTimeString();

        if ($request->observaciones){
            $postulante = Postulante::find($request->pos_id);
            $postulante->observaciones=$request->observaciones;
            $postulante->save();
        }


        $voucher->save();
        DB::commit();
        return response()->json('success', 200);
//        return response()->json($request, 200);

    }

    public function constancia_pdf($id)
    {
        $postulante = Postulante::find($id);
        $anexos = PostulacionAnexo::whereHas('postulacion', function ($q) use ($id) {
            $q->where('postulante_id', $id);
        })->get();
        $data = [
            'postulante' => $postulante,
            'anexos' => $anexos,
        ];
        $pdf = PDF::loadview('Admisiones.constanciaInscripcion_pdf', $data);
        return $pdf->stream();
    }

    public function rptPostulatesxCarrera_pdf(){
        $carreras = Carrera::whereNotIn('id', [6, 9])->get();
        $postulantes = Postulante::whereHas('postulaciones', function ($q){
            $q->where('estado','<>','Solicitado');
        })->where('condicion','Ordinario')->get();
        $data= [
          'carreras'=>$carreras,
          'postulantes'=>$postulantes,
        ];

//        dd($postulantes);
        $pdf=PDF::loadview('Admisiones.rpt_postulantes_carrera',$data);
        return $pdf->stream();
    }

    public function rptPostulatesxCarreraExo_pdf(){
        $carreras = Carrera::whereNotIn('id', [6, 9])->get();
        $postulantes = Postulante::whereHas('postulaciones', function ($q){
            $q->where('estado','<>','Solicitado');
        })->where('condicion','Exonerado')->get();
        $data= [
            'carreras'=>$carreras,
            'postulantes'=>$postulantes,
        ];

//        dd($postulantes);
        $pdf=PDF::loadview('Admisiones.rpt_postulantes_exonerados',$data);
        return $pdf->stream();
    }

    public function auto_get_postulante()
    {

        $indicador=$_GET['term'];
        $postulante=Postulante::with('persona')->whereHas('persona',function ($q) use ($indicador){
           $q->Where('DNI','like', '%' . $indicador . '%')
               ->orWhere('paterno','like', '%' . $indicador . '%')
               ->orWhere('materno','like', '%' . $indicador . '%')
               ->orWhere('nombres','like', '%' . $indicador . '%');
        })->whereDoesntHave('entrevista_postulante')->get();

        foreach ($postulante as $item) {
            $response[] = array('value' => $item->persona->DNI.' - '.$item->persona->paterno.' '.$item->persona->materno.' '.$item->persona->nombres, 'id' => $item->id);
        }
        return $response;
    }

    public function verListPostulantes()
    {
        $carreras = Carrera::whereNotIn('id', [6, 9])->get();
        $postulantes = Postulante::whereHas('postulaciones', function ($q){
            $q->where('estado','<>','Solicitado');
        })->get();

        return view('Visitas.verPostulantes', ['postulantes' => $postulantes, 'carreras' => $carreras]);
    }

    public function verListPostulantesCarrera(Request $request)
    {
        $postulantes = Postulante::with('persona', 'postulaciones.pe_carrera.carrera')->whereDoesntHave('postulaciones', function ($q) {
            $q->where('estado', 'Solicitado');
        })->whereHas('postulaciones.pe_carrera.carrera', function ($q) use ($request) {
            $q->where('id', $request->carrera_id);
        })->get();
        return response()->json(['postulantes' => $postulantes]);
    }

    public function verListPostulantesDetalle($id) {
        $postulante = Postulante::find($id);
//        dd($postulante->postulaciones[0]->id);
        $comprobante = Voucher::where([['postulacion_id',$postulante->postulaciones->first()->id],['tipo','boleta']])->get();
        $distrito = Distrito::with('provincia.departamento')->where('id', $postulante->distrito_id)->get();
        $periodo = Periodo::where('estado', 1)->first();
        $anexos = PostulacionAnexo::whereHas('postulacion', function ($q) use ($id) {
            $q->where('postulante_id', $id);
        })->get();
//        dd($comprobante);
        return view('Visitas.verPostulanteDetalle', ['postulante' => $postulante, 'departamento' => $distrito, 'periodo' => $periodo, 'anexos' => $anexos, 'comprobante'=>$comprobante]);

    }

    public function list_all()
    {
        $carreras = Carrera::whereNotIn('id', [6, 9])->get();
        $postulantes = Postulante::all();
        return view('Admisiones.list_all_postulantes', ['postulantes' => $postulantes, 'carreras' => $carreras]);
    }
}
