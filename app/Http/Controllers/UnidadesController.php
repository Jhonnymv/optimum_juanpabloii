<?php

namespace App\Http\Controllers;

use App\silabo;
use App\unidades;
use App\Contenido;
use Illuminate\Http\Request;

class UnidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($silabo_id)
    {
        $silabo = silabo::where('estado','=','1')->get();

    
        if($silabo_id == null){
            $unidad = unidades::paginate(5);
        }
        else
        {
            $unidad = unidades::where('silabo_id','=',$silabo_id)->get();   
        }
        //$silabos_docentes = silabo_docentes::paginate(5);
        //$cursos = Curso::where('estado','=','1')->get();
        //$periodoacademicos = Periodo::where('estado','=','1')->get();
      
        return view('Admin.unidades.index',[
            'unidad' => $unidad,
            'silabo' => $silabo
        ]);
        
    }
    public function indexb()
    {
      
        $silabo = silabo::where('estado','=','1')->get();
        $unidad = unidades::paginate(5);
        //$docentes = Docente::where('estado','=','1')->get();
        //$periodoacademicos = Periodo::where('estado','=','1')->get();
        return view('Admin.unidades.index',[
            'unidad' => $unidad,
            'silabo' => $silabo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unidad = new unidades();

        $unidad->silabo_id = $request->silabo_id;
        // $unidad->capacidades = $request->capacidades;
        $unidad->unidad = $request->unidad;
        $unidad->denominacion = $request->denominacion;
        $unidad->fecha_inicio = $request->fecha_inicio;
        $unidad->fecha_fin = $request->fecha_fin;
            
        
        $unidad->save();

        return ['success'=>true,'unidad'=>$unidad];
        
    }

    public function contenidosPorUnidad(Request $request){
        return Contenido::where('unidades_id',$request->id)->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return unidades::find($request->unidades_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function edit(unidades $unidades)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $unidad = unidades::find($request->unidades_id);

        $unidad->silabo_id = $request->silabo_id;
        // $unidad->capacidades = $request->capacidades;
        $unidad->unidad = $request->unidad;
        $unidad->denominacion = $request->denominacion;
        $unidad->fecha_inicio = $request->fecha_inicio;
        $unidad->fecha_fin = $request->fecha_fin;
            
        
        $unidad->save();

        return ['success'=>true,'unidad'=>$unidad];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Contenido::where('unidades_id',$request->unidades_id)->delete();
        unidades::destroy($request->unidades_id);

        return ['success'=>true];
    }

    public function renderUnidadesCrud(Request $request){
        $silabo=silabo::with([
                            'unidades.contenidos',
                            'unidades.desempeno_criterios'
                            ])
                            ->whereId($request->silabo_id)
                            ->firstOrFail();
                            
        return view('IntranetDocente.Silabos.unidadesParcial')->with('silabo',$silabo);
    }
}
