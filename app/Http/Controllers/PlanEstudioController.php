<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PlanEstudio;

class PlanEstudioController extends Controller
{

    public function index(){

        $planestudios = PlanEstudio::where('estado','1')->get();
//       dd($planestudios);
//        return view('Admin.PlanEstudios.index', [
        return view('SecretariaAcademica.PlanEstudios.index',[
            'planestudios' => $planestudios
        ]);
    }

    public function store(Request $request){
        $validar = $request->validate([
            'plan'=>'required',
            'fecha_inicio' => 'required ',
            'fecha_fin' =>  'required'
        ]);

        $planestudio = new PlanEstudio([
            'plan'=>$request->plan,
            'fecha_inicio' => $request->fecha_inicio,
            'fecha_fin' => $request->fecha_fin,
            'estado' => 1
        ]);


        $planestudio->save();

        return redirect()->route('planestudios.index');

    }

    public function delete($id){

        $planestudio = PlanEstudio::find($id);

        if($planestudio != null){
            $planestudio->estado = 0;
            $planestudio->save();
        }

        return redirect()->route('planestudios.index');
    }

    public function update(Request $request,$id){

        $planestudio = PlanEstudio::find($id);

        $planestudio->plan = $request->planEdit;
        $planestudio->fecha_inicio = $request->fechaInicioEdit;
        $planestudio->fecha_fin = $request->fechaFinEdit;
        $planestudio->save();

        return redirect()->route('planestudios.index');
    }


    public function getPlanEstudio(Request $request){

        $planestudio = PlanEstudio::find($request->id);
        return response()->json(['planestudio'=> $planestudio], 200);
    }

    public function getRptPlanEstudios(){
        $planEstudios =PlanEstudio::all();
//        dd($planEstudios);
        $carreras = Carrera::all();
        return view('SecretariaAcademica.Reportes.rpt_planEstudio',['planEstudios'=>$planEstudios, 'carreras'=>$carreras]);
    }

    public function getPlanEstCarrera(Request $request){

        $planEstudios = PlanEstudio::wherehas('pecarreras',function ($q) use ($request){
            $q->where('carrera_id', $request->carrera_id);
        })->get();
        return response()->json(['planes_estudio'=>$planEstudios]);

    }

}
