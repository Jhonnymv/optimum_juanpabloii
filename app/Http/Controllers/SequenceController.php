<?php

namespace App\Http\Controllers;

use App\Sequence;
use Illuminate\Http\Request;

class SequenceController extends Controller
{
    public function get_sequence(Request $request)
    {
        $sequence=Sequence::where([['doc_type',$request->tipo_doc],['status',1]])->first();
        return response()->json(['sequence'=>$sequence],200);
    }
}
