<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use App\Alumnopago;
use App\Carrera;
use App\Categoria;
use App\Cronograma;
use App\Departamento;
use App\Distrito;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\MatriculaDetalle;
use App\Modulo;
use App\Padre;
use App\PeCarrera;
use App\Periodo;
use App\Persona;
use App\PlanEstudio;
use App\Postulante;
use App\Provincia;
use App\Usuario;
use Carbon\Carbon;
use Doctrine\Inflector\NoopWordInflector;
use MongoDB\Driver\WriteError;
use MPDF;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function started()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumnoPago = count($alumno->alumnoPago);

        $cronogramas = Cronograma::with('periodo')
            ->where([['periodo_id', Periodo::where('estado', '1')->first()->id], ['estado', 'VENCIDO']])
            ->whereHas('concepto', function ($q) use ($alumno) {
                $q->where('carrera_id', $alumno->alumnocarreras->last()->carrera_id)->orWhere('carrera_id', null);
            })
            ->get();
//        $cronogramas = Cronograma::with('periodo')
//            ->where([['periodo_id', Periodo::where('estado', '1')->first()->id]])
//            ->whereHas('concepto', function ($q) use ($alumno) {
//                $q->where('carrera_id', $alumno->alumnocarreras->last()->carrera_id)->orWhere('carrera_id', null);
//            })
//            ->get();
        //dd($alumno->id);
        $nopagados = collect();
        foreach ($cronogramas as $cronograma) {
            $pagado = Alumnopago::with('alumno')->where([['alumno_id', $alumno->id],['cronograma_id',$cronograma->id]])->first();
            //dd($pagado);
            if (!$pagado)
                $nopagados->push($cronograma);
        }
        //dd($nopagados);
        return view('IntranetAlumno.index', ['alumno' => $alumno, 'alumnoPago' => $alumnoPago,'noPago'=>$nopagados]);
        //return view('IntranetAlumno.index', ['alumno' => $alumno]);
    }

    public function index()
    {
        //paginate: paginar
        //$carreras = Carrera::where('estado','=','1')->get();
        //$responsables = Responsable::where('estado','=','1')->get();
        //$datos['docentes'] = Docente :: paginate(5);
        $datos['alumnos'] = Alumno::where('estado', '=', '1')->get();
        return view('Alumnos.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Alumnos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$DatosDocente = request() -> all();


        $DatosAlumno = request()->except('_token');

        Alumno::insert($DatosAlumno);

        //return response() -> json($DatosDocente);
        return redirect('alumnos')->with('Mensaje', 'CredencialesAlumno registrado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Docente $docentes
     * @return \Illuminate\Http\Response
     */
    public function show(Alumno $alumnos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Docente $docentes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $alumno = Alumno::with('persona.distrito.provincia.departamento', 'alumnocarreras.carrera')->findOrFail($id);
        return response()->json($alumno);
        //return view('Alumnos.index', compact('alumno'));
        return response()->json($alumno->toArray);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Docente $docentes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //$DatosAlumno = request() -> except('_token','_method');
        //CredencialesAlumno::where('id', '=' ,$id) -> update ($DatosAlumno);

        //$docente = Docente::findOrFail($id);
        //return view('docentes.edit', compact('docente'));
        //return redirect('alumnos') -> with('Mensaje','CredencialesAlumno modificado exitosamente!');
        $alumno = Alumno::find($id);

        $alumno->nombres = $request->nombresEdit;
        $alumno->paterno = $request->PaternoEdit;
        $alumno->materno = $request->MaternoEdit;
        $alumno->telefono = $request->telefonoEdit;
        $alumno->fecha_nacimiento = $request->fecha_nacimientoEdit;
        $alumno->sexo = $request->sexoEdit;
        $alumno->procedencia = $request->procedenciaEdit;
        $alumno->email = $request->emailEdit;
        $alumno->usuario = $request->usuarioEdit;
        $alumno->password = $request->passwordEdit;
        $alumno->dni = $request->DNIEdit;
        $alumno->save();

        return redirect()->route('alumno.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\alumno $docentes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Alumno::destroy($id);
        return redirect('alumnos')->with('Mensaje', 'CredencialesAlumno eliminado exitosamente!');

    }

    public function delete($id)
    {

        $alumnos = Alumno::find($id);

        if ($alumnos != null) {
            $alumnos->estado = 0;
            $alumnos->save();
        }

        return redirect()->route('alumno.index');
    }

    public function getAlumno(Request $request)
    {

        $alumno = Alumno::find($request->id);

        return response()->json(['alumno' => $alumno], 200);
    }

    // reporte economico por alumno
    public function rptEconomicoCreate()
    {
        $alumnos = Alumno::with('persona')->where('estado', '=', 1)->get();
//        dd($alumnos);
        return view('SecretariaAcademica.Reportes.rpt_economicoAlumno', ['alumnos' => $alumnos]);
    }

    public function getAlumnoCarrera()
    {
        $carreras = Carrera::where('estado', '=', '1')->get();
        return view('SecretariaAcademica.alumnos.list_alumno', ['carreras' => $carreras]);
    }

    public function getalumnosxcarrera(Request $request)
    {

        $alumnos = AlumnoCarrera::with('carrera', 'alumno.persona')->whereHas('alumno', function ($q) use ($request) {
            $q->where('carrera_id', $request->carrera_id);
        })->get();
        return response()->json(['alumnos' => $alumnos], 200);

    }

    public function updateEstadoAlumno($id)
    {
        $alumnos = Alumno::find($id);
        if ($alumnos->estado == "1") {
            $alumnos->estado = "0";
        } else if ($alumnos->estado == "0") {
            $alumnos->estado = "1";
        }
        $alumnos->save();

        return response()->json(['alumno' => $alumnos], 200);

    }

    public function CredencialesAlumnosAdmin()
    {
        $departamentos = Departamento::all();
        $provincias = Provincia::all();
        $distritos = Distrito::all();
        return view('Admin.CredencialesAlumno.index', ['departamentos' => $departamentos]);

    }

    public function CrearCredencialesAlumno()
    {
        $usuario = Auth::user()->persona->padre;
//       Usuario,Persona, Alumno, Alumno_carrera
        $alumno = Alumno::all();
        $departamentos = Departamento::all();
        $carreras = Carrera::where('estado', 1)->get();
        $provincias = Provincia::all();
        $distritos = Distrito::all();
//        dd($usuario);
        if ($usuario) {
            return view('IntranetPadre.RegistroAlumnos.create', ['carreras' => $carreras, 'departamentos' => $departamentos, 'provincias' => $provincias, 'distritos' => $distritos]);

        }
        return view('Admin.CredencialesAlumno.create', ['carreras' => $carreras, 'departamentos' => $departamentos, 'provincias' => $provincias, 'distritos' => $distritos]);

    }

    public function StoreCredencialesAlumno(Request $request)
    {
//        dd($request);
        $validateData = Validator::make($request->all(), [
            'paterno_padre' => ['required', 'min:3'],
            'materno_padre' => ['required', 'min:3'],
            'nombres_padre' => ['required', 'min:3'],
            'dniPadre' => ['required', 'max:8'],
            'fecha_nacimiento_padre' => ['required'],
            'telefono_padre' => ['required', 'min:6', 'max:9'],
            'email_personal_padre' => ['required'],
            'direccion_padre' => ['required'],
            'distrito_padre_id' => ['required'],
            'apoderado' => ['required'],
            'vivepadre' => ['required'],
            'vive_alumno_padre' => ['required'],
            'estado_civil' => ['required'],
            'nivel_instruccion_padre' => ['required'],
            'religion_padre' => ['required'],
            'profesion_padre' => ['required'],
            'ocupacion_padre' => ['required'],
            'centro_trabajo_padre' => ['required'],
            'paterno_madre' => ['required'],
            'materno_madre' => ['required'],
            'nombres_madre' => ['required'],
            'dniMadre' => ['required', 'max:8'],
            'fecha_nacimiento_madre' => ['required'],
            'telefono_madre' => ['required'],
            'email_personal_madre' => ['required'],
            'direccion_madre' => ['required'],
            'distrito_madre_id' => ['required'],
            'apoderado2' => ['required'],
            'viveMadre' => ['required'],
            'vive_alumno_madre' => ['required'],
            'nivel_instruccion_madre' => ['required'],
            'religion_madre' => ['required'],
            'profesion_madre' => ['required'],
            'ocupacion_madre' => ['required'],
            'centro_trabajo_madre' => ['required'],
            'paterno' => ['required'],
            'materno' => ['required'],
            'nombres' => ['required'],
            'DNI' => ['required', 'max:8', 'unique:personas,DNI'],
            'sexo' => ['required'],
            'fecha_nacimiento' => ['required'],
            'telefono' => ['required'],
            'email' => ['required'],
            'email_personal' => ['required'],
            'direccion' => ['required'],
            'distrito_id' => ['required'],
            'num_hermanos' => ['required'],
            'num_ocupa' => ['required'],
            'religion' => ['required'],
            'gru_sanginio' => ['required'],
            'parto' => ['required'],
            'enfermedades' => ['required'],
            'alergias' => ['required'],
            'accidentes' => ['required'],
            'traumas' => ['required'],
            'lim_fisicas' => ['required'],
            'peso' => ['required'],
            'talla' => ['required'],
            'vacunas' => ['required'],
            't_seguro' => ['required'],
            'n_seguro' => ['required'],
            'num_emergencia' => ['required'],
            'relacion' => ['required'],
            'm_tiempo' => ['required'],
            'carrera_id' => ['required'],
            'ciclo' => ['required'],
            'grupo' => ['required'],
            'fecha_ingreso' => ['required'],
        ]);

        if ($validateData->fails()) {
            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();
        }

        $apersona_check = Persona::where('DNI', $request->DNI)->first();
        $ppersona_check = Persona::where('DNI', $request->dniPadre)->first();
        $mpersona_check = Persona::where('DNI', $request->dniMadre)->first();
        $padre_id = '';
        $madre_id = '';

        DB::beginTransaction();
        if (!$ppersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno_padre;
            $persona->materno = $request->materno_padre;
            $persona->nombres = $request->nombres_padre;
            $persona->DNI = $request->dniPadre;
            $persona->sexo = 'M';
            $persona->fecha_nacimiento = $request->fecha_nacimiento_padre;
            $persona->telefono = $request->telefono_padre;
            $persona->email = $request->email_padre;
            $persona->email_personal = $request->email_personal_padre;
            $persona->direccion = $request->direccion_padre;
            $persona->distrito_id = $request->distrito_padre_id;
            $persona->save();
            $ppersona_id = $persona->id;


            $padre = new Padre();
            $padre->persona_id = $ppersona_id;
            $padre->apoderado = $request->apoderado;
            $padre->tipo = 'P';
            $padre->vive = $request->vivepadre;
            $padre->vive_alumno = $request->vive_alumno_padre;
            $padre->estado_civil = $request->estado_civil;
            $padre->nivel_instruccion = $request->nivel_instruccion_padre;
            $padre->religion = $request->religion_padre;
            $padre->profesion = $request->profesion_padre;
            $padre->ocupacion = $request->ocupacion_padre;
            $padre->centro_trabajo = $request->centro_trabajo_padre;
            $padre->save();
            $padre_id = $padre->id;

            if ($request->apoderado == 'si') {
                $usuario = new Usuario();
                $usuario->usuario = $request->dniPadre;
                $usuario->password = Hash::make($request->dniPadre);
                $usuario->estado = 1;
                $usuario->tipo = 'padre';
                $usuario->persona_id = $ppersona_id;
                $usuario->save();
                DB::table('modulo_usuario')->insert([
                    'modulo_id' => 10,
                    'usuario_id' => $usuario->id
                ]);
            }
        } else {
            $padre = Padre::where('persona_id', $ppersona_check->id)->get();
            $padre_id = $padre->first()->id;
        }

        if (!$mpersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno_madre;
            $persona->materno = $request->materno_madre;
            $persona->nombres = $request->nombres_madre;
            $persona->DNI = $request->dniMadre;
            $persona->sexo = 'F';
            $persona->fecha_nacimiento = $request->fecha_nacimiento_madre;
            $persona->telefono = $request->telefono_madre;
            $persona->email = $request->email_madre;
            $persona->email_personal = $request->email_personal_madre;
            $persona->direccion = $request->direccion_madre;
            $persona->distrito_id = $request->distrito_madre_id;
            $persona->save();
            $mpersona_id = $persona->id;

            $padre = new Padre();
            $padre->persona_id = $mpersona_id;
            $padre->apoderado = $request->apoderado2;
            $padre->tipo = 'M';
            $padre->vive = $request->viveMadre;
            $padre->vive_alumno = $request->vive_alumno_madre;
            $padre->estado_civil = $request->estado_civil;
            $padre->nivel_instruccion = $request->nivel_instruccion_madre;
            $padre->religion = $request->religion_madre;
            $padre->profesion = $request->profesion_madre;
            $padre->ocupacion = $request->ocupacion_madre;
            $padre->centro_trabajo = $request->centro_trabajo_madre;
            $padre->save();
            $madre_id = $padre->id;
            if ($request->apoderado2 == 'si') {
                $usuario = new Usuario();
                $usuario->usuario = $request->dniMadre;
                $usuario->password = Hash::make($request->dniMadre);
                $usuario->estado = 1;
                $usuario->tipo = 'padre';
                $usuario->persona_id = $mpersona_id;
                $usuario->save();

                DB::table('modulo_usuario')->insert([
                    'modulo_id' => 10,
                    'usuario_id' => $usuario->id
                ]);
            }
        } else {
            $madre = Padre::where('persona_id', $mpersona_check->id)->get();
            $madre_id = $madre->first()->id;
        }

        if (!$apersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->nombres = $request->nombres;
            $persona->DNI = $request->DNI;
            $persona->sexo = $request->sexo;
            $persona->fecha_nacimiento = $request->fecha_nacimiento;
            $persona->telefono = $request->telefono;
            $persona->email = $request->DNI . '@juanpabloii.edu.pe';
            $persona->email_personal = $request->email_personal;
            $persona->direccion = $request->direccion;
            $persona->distrito_id = $request->distrito_id;
            $persona->save();
            $apersona_id = $persona->id;

            $alumno = new Alumno();
            $alumno->persona_id = $apersona_id;
            $alumno->padre_id = $padre_id;
            $alumno->madre_id = $madre_id;
            $alumno->estado = '1';
            $alumno->num_hermanos = $request->num_hermanos;
            $alumno->num_ocupa = $request->num_ocupa;
            $alumno->religion = $request->religion;
            $alumno->gru_sanginio = $request->gru_sanginio;
            $alumno->parto = $request->parto;
            $alumno->enfermedades = $request->enfermedades;
            $alumno->alergias = $request->alergias;
            $alumno->accidentes = $request->accidentes;
            $alumno->traumas = $request->traumas;
            $alumno->lim_fisicas = $request->lim_fisicas;
            $alumno->peso = $request->peso;
            $alumno->talla = $request->talla;
            $alumno->vacunas = $request->vacunas;
            $alumno->t_seguro = $request->t_seguro;
            $alumno->n_seguro = $request->n_seguro;
            $alumno->num_emergencia = $request->num_emergencia;
            $alumno->relacion = $request->relacion;
            $alumno->m_tiempo = $request->m_tiempo;
            $alumno->save();
            $alumno_id = $alumno->id;


            $usuario = new Usuario();
            $usuario->usuario = $request->DNI;
            $usuario->password = Hash::make($request->DNI);
            $usuario->estado = 1;
            $usuario->tipo = 'alumno';
            $usuario->persona_id = $apersona_id;
            $usuario->save();

            DB::table('modulo_usuario')->insert([
                'modulo_id' => 1,
                'usuario_id' => $usuario->id
            ]);


            $pecarrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', '1']])->first();

            $alumno_carrera_check = AlumnoCarrera::where('alumno_id', $alumno->id)->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $request->carrera_id;
                $alumno_carrera->alumno_id = $alumno_id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->pe_carrera_id = $pecarrera->id;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();
            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }

            $categoria = Categoria::where([['carrera_id', $request->carrera_id], ['nombre', 'B']])->get();

            DB::table('alumno_categoria')->insert([
                'categoria_id' => $request->categoria_id,
                'alumno_id' => $alumno_id,
                'estado' => 1,
            ]);
        } else {
            $apersona_id = $apersona_check->id;
            $alumno = Alumno::find($apersona_id);
            $pecarrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', '1']])->first();

            $alumno_carrera_check = AlumnoCarrera::where(['alumno_id', $alumno->id], ['carrera_id', $request->carrera_id])->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $request->carrera_id;
                $alumno_carrera->alumno_id = $alumno->id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->pe_carrera_id = $pecarrera->id;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();
            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }

        }


        DB::commit();

        return redirect()->back();

    }

    public function updateCredencialesAlumno(Request $request, $id)
    {

        $persona = Persona::find($id);

        $persona->nombres = $request->nombres;
        $persona->paterno = $request->paterno;
        $persona->materno = $request->materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->DNI = $request->DNI;
        $persona->fecha_nacimiento = $request->fecha_nacimiento;
        $persona->email_personal = $request->email_personal;
        $persona->email = $request->email;
        $persona->save();

        echo "Actualizar persona con dni" . $persona->DNI . " nombre" . $persona->nombres;
        dd($persona);

//        return redirect()->route('registrar_credenciales_alumno');
    }

    public function postulanteAlumno()
    {
        $pe_carreras = PeCarrera::all();
        return view('Admisiones.ingresoPostulante', ['pe_carreras' => $pe_carreras]);
    }

    public function buscarPostulanteAlumno(Request $request)
    {
        $persona = Persona::with('postulante.postulaciones.pe_carrera', 'alumno.alumnocarreras')->where($request->buscarpor, 'like', '%' . $request->datoBuscar . '%')->get();
//        return $request;
        return response()->json(['persona' => $persona]);
    }

    public function addPostulanteAlumno(Request $request)
    {

        $alumno_check = Alumno::where('persona_id', $request->id)->first();
        $pe_carrera = PeCarrera::find($request->pe_carrera_id);
        $carrera = Carrera::find($pe_carrera->carrera_id);

//        dd($alumno_check);
        if (!$alumno_check) {
            DB::beginTransaction();
            $alumno = new Alumno();
            $alumno->estado = 1;
            $alumno->persona_id = $request->id;
            $alumno->save();

            $alumno_carrera_check = AlumnoCarrera::where([['alumno_id', $alumno->id], ['carrera_id', $request->carrera_id]])->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $carrera->id;
                $alumno_carrera->alumno_id = $alumno->id;
                $alumno_carrera->pe_carrera_id = $request->pe_carrera_id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();

                $usuario = Usuario::where('persona_id', $request->id)->first();
                DB::table('modulo_usuario')->where('usuario_id', $usuario->id)->update([
                    'modulo_id' => 1
                ]);

            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }
            if ($request->editar == '1') {
                $persona = Persona::find($request->id);
                $persona->paterno = $request->paterno;
                $persona->materno = $request->materno;
                $persona->nombres = $request->nombres;
                $persona->DNI = $request->DNI;
                $persona->fecha_nacimiento = $request->fecha_nacimiento;
                $persona->telefono = $request->telefono;
                $persona->direccion = $request->direccion;
                $persona->paterno = $request->paterno;
                $persona->save();
            }
            DB::commit();
        } else {
            DB::beginTransaction();
            $alumno = $alumno_check;
            $alumno->estado = '1';
            $alumno->save();

            $alumno_carrera_check = AlumnoCarrera::where([['alumno_id', $alumno->id], ['ciclo', $request->ciclo]])->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $carrera->id;
                $alumno_carrera->alumno_id = $alumno->id;
                $alumno_carrera->pe_carrera_id = $request->pe_carrera_id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();
            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->carrera_id = $carrera->id;
                $alumno_carrera->pe_carrera_id = $request->pe_carrera_id;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }
            if ($request->editar == '1') {
                $persona = Persona::find($request->id);
                $persona->paterno = $request->paterno;
                $persona->materno = $request->materno;
                $persona->nombres = $request->nombres;
                $persona->DNI = $request->DNI;
                $persona->fecha_nacimiento = $request->fecha_nacimiento;
                $persona->telefono = $request->telefono;
                $persona->direccion = $request->direccion;
                $persona->paterno = $request->paterno;
                $persona->save();
            }
            DB::commit();
        }
        return $this->postulanteAlumno();
    }

    public function editAlumno()
    {
        $carreras = Carrera::all();
        $modulos = Modulo::all();
        $departamentos = Departamento::all();
        $provincias = Provincia::all();
        $distritos = Distrito::all();
        return view('Admin.CredencialesAlumno.edit', ['carreras' => $carreras, 'modulos' => $modulos, 'departamentos' => $departamentos, 'provincias' => $provincias, 'distritos' => $distritos]);
    }

    public function buscarAlumnoxDni($dni)
    {
        $alumno = Alumno::with('persona.distrito.provincia.departamento')->whereHas('persona', function ($q) use ($dni) {
            $q->where('DNI', $dni);
        })->first();

        $alumno_id = $alumno->id;
        $alumno_persona_id = $alumno->persona->id;
        $madre_id = $alumno->madre_id;
        $padre_id = $alumno->padre_id;
        $padre = Padre::with('persona')->where('id', $padre_id)->first();
        $madre = Padre::with('persona')->where('id', $madre_id)->first();

        $distrito_alumno = Distrito::with('provincia.departamento')->where('id', $alumno->persona->distrito_id)->get();
        if ($padre)
            $distrito_padre = Distrito::with('provincia.departamento')->where('id', $padre->persona->distrito_id)->get();
        if ($madre)
            $distrito_madre = Distrito::with('provincia.departamento')->where('id', $madre->persona->distrito_id)->get();
        $usuario = Usuario::where('persona_id', $alumno_persona_id)->first();
        $alumno_carrera = AlumnoCarrera::where('alumno_id', $alumno_id)->first();

        $data = [
            'alumno' => $alumno,
            'padre' => $padre ?? [],
            'madre' => $madre ?? [],
            'usuario' => $usuario,
            'alumno_carrera' => $alumno_carrera,
            'distrito_alumno' => $distrito_alumno,
            'distrito_padre' => $distrito_padre ?? [],
            'distrito_madre' => $distrito_madre ?? []
        ];
        return response()->json($data, 200);

    }

    public function updateAlumno(Request $request)
    {

        DB::beginTransaction();

        if ($request->dniPadre) {
            $ppersona = Persona::find($request->idPersonaPadre);
            $ppersona->paterno = $request->paterno_padre;
            $ppersona->materno = $request->materno_padre;
            $ppersona->nombres = $request->nombres_padre;
            $ppersona->DNI = $request->dniPadre;
            $ppersona->sexo = 'M';
            $ppersona->fecha_nacimiento = $request->fecha_nacimiento_padre;
            $ppersona->telefono = $request->telefono_padre;
            $ppersona->email = $request->email_padre;
            $ppersona->email_personal = $request->email_personal_padre;
            $ppersona->direccion = $request->direccion_padre;
            $ppersona->distrito_id = $request->distrito_padre_id;
            $ppersona->save();


            $padre = Padre::find($request->idPadre);
            $padre->persona_id = $ppersona->id;
            $padre->apoderado = $request->apoderado;
            $padre->tipo = 'P';
            $padre->vive = $request->vivepadre;
            $padre->vive_alumno = $request->vive_alumno_padre;
            $padre->estado_civil = $request->estado_civil;
            $padre->nivel_instruccion = $request->nivel_instruccion_padre;
            $padre->religion = $request->religion_padre;
            $padre->profesion = $request->profesion_padre;
            $padre->ocupacion = $request->ocupacion_padre;
            $padre->centro_trabajo = $request->centro_trabajo_padre;
            $padre->save();

            $usuario_check = Usuario::where('persona_id', $ppersona->id)->first();

            if ($request->apoderado == 'si') {
                if (!$usuario_check) {
                    $usuario = new Usuario();
                    $usuario->usuario = $request->dniPadre;
                    $usuario->password = Hash::make($request->dniPadre);
                    $usuario->estado = 1;
                    $usuario->tipo = 'padre';
                    $usuario->persona_id = $ppersona->id;
                    $usuario->save();
                    DB::table('modulo_usuario')->insert([
                        'modulo_id' => 10,
                        'usuario_id' => $usuario->id
                    ]);
                } else {
                    $usuario = $usuario_check;
                    $usuario->estado = 1;
                    $usuario->save();
                }
            } else if ($request->apoderado == 'no') {
                if ($usuario_check) {
                    $usuario = Usuario::where('persona_id', $ppersona->id)->first();
                    $usuario->estado = 0;
                    $usuario->save();
                }
            }
        }
        if ($request->dniMadre) {
            $mpersona = Persona::find($request->idPersonaMadre);
            $mpersona->paterno = $request->paterno_madre;
            $mpersona->materno = $request->materno_madre;
            $mpersona->nombres = $request->nombres_madre;
            $mpersona->DNI = $request->dniMadre;
            $mpersona->sexo = 'F';
            $mpersona->fecha_nacimiento = $request->fecha_nacimiento_madre;
            $mpersona->telefono = $request->telefono_madre;
            $mpersona->email = $request->email_madre;
            $mpersona->email_personal = $request->email_personal_madre;
            $mpersona->direccion = $request->direccion_madre;
            $mpersona->distrito_id = $request->distrito_madre_id;
            $mpersona->save();
            $mpersona_id = $mpersona->id;

            $madre = Padre::find($request->idMadre);
            $madre->persona_id = $mpersona_id;
            $madre->apoderado = $request->apoderado2;
            $madre->tipo = 'M';
            $madre->vive = $request->viveMadre;
            $madre->vive_alumno = $request->vive_alumno_madre;
            $madre->estado_civil = $request->estado_civil;
            $madre->nivel_instruccion = $request->nivel_instruccion_madre;
            $madre->religion = $request->religion_madre;
            $madre->profesion = $request->profesion_madre;
            $madre->ocupacion = $request->ocupacion_madre;
            $madre->centro_trabajo = $request->centro_trabajo_madre;
            $madre->save();
            $madre_id = $madre->id;

            $usuario_check = Usuario::where('persona_id', $mpersona->id)->first();

            if ($request->apoderado2 == 'si') {
                if (!$usuario_check) {
                    $usuario = new Usuario();
                    $usuario->usuario = $request->dniMadre;
                    $usuario->password = Hash::make($request->dniMadre);
                    $usuario->estado = 1;
                    $usuario->tipo = 'padre';
                    $usuario->persona_id = $mpersona_id;
                    $usuario->save();

                    DB::table('modulo_usuario')->insert([
                        'modulo_id' => 10,
                        'usuario_id' => $usuario->id
                    ]);
                } else {
                    $usuario = $usuario_check;
                    $usuario->estado = 1;
                    $usuario->save();
                }
            } else if ($request->apoderado2 == 'no') {
                if ($usuario_check) {
                    $usuario = Usuario::where('persona_id', $mpersona->id)->first();
                    $usuario->estado = 0;
                    $usuario->save();
                }
            }
        }

        $apersona = Persona::find($request->idPersonaAlumno);
        $apersona->paterno = $request->paterno;
        $apersona->materno = $request->materno;
        $apersona->nombres = $request->nombres;
        $apersona->DNI = $request->DNI;
        $apersona->sexo = $request->sexo;
        $apersona->fecha_nacimiento = $request->fecha_nacimiento;
        $apersona->telefono = $request->telefono;
        $apersona->email = $request->DNI . '@juanpabloii.edu.pe';
        $apersona->email_personal = $request->email_personal;
        $apersona->direccion = $request->direccion;
        $apersona->distrito_id = $request->distrito_id;
        $apersona->save();
        $apersona_id = $apersona->id;

        $alumno = Alumno::find($request->idAlumno);
        $alumno->persona_id = $apersona_id;
        $alumno->padre_id = $padre->id ?? 0;
        $alumno->madre_id = $madre->id ?? 0;
        $alumno->estado = '1';
        $alumno->num_hermanos = $request->num_hermanos;
        $alumno->num_ocupa = $request->num_ocupa;
        $alumno->religion = $request->religion;
        $alumno->gru_sanginio = $request->gru_sanginio;
        $alumno->parto = $request->parto;
        $alumno->enfermedades = $request->enfermedades;
        $alumno->alergias = $request->alergias;
        $alumno->accidentes = $request->accidentes;
        $alumno->traumas = $request->traumas;
        $alumno->lim_fisicas = $request->lim_fisicas;
        $alumno->peso = $request->peso;
        $alumno->talla = $request->talla;
        $alumno->vacunas = $request->vacunas;
        $alumno->t_seguro = $request->t_seguro;
        $alumno->n_seguro = $request->n_seguro;
        $alumno->num_emergencia = $request->num_emergencia;
        $alumno->relacion = $request->relacion;
        $alumno->m_tiempo = $request->m_tiempo;
        $alumno->save();
        $alumno_id = $alumno->id;

        $pecarrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', '1']])->first();

        $alumno_carrera = AlumnoCarrera::find($request->idAlumnoCarrera);
        $alumno_carrera->carrera_id = $request->carrera_id;
        $alumno_carrera->alumno_id = $alumno_id;
        $alumno_carrera->ciclo = $request->ciclo;
        $alumno_carrera->grupo = $request->grupo;
        $alumno_carrera->pe_carrera_id = $pecarrera->id;
        $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
        $alumno_carrera->save();

        DB::commit();

        return redirect()->back();

    }

    public function showAlumnos()
    {
        $alumnos = Alumno::all();

        return view('Admin.CredencialesAlumno.cardalumno', ['alumnos' => $alumnos]);

    }

    public function get_detalleAlumno($id)
    {
        $alumno = Alumno::find($id);
        $dataQR = [
            'alumno_id' => $alumno->id,
            'Dni' => $alumno->persona->DNI,
            'Nombre' => $alumno->persona->nombres . ' ' . $alumno->persona->paterno . ' ' . $alumno->persona->materno
        ];
        $qr = QrCode::generate(implode(';', $dataQR), '../public/qrcodes/qrcode.svg');

        $data = [
            'alumno' => $alumno,
            'qr' => $qr
        ];

        $pdf = PDF::loadView('Admin.CredencialesAlumno.card', $data)->setPaper('A7', 'portrait');
        return $pdf->stream();
    }

    public function StoreCredencialesAlumnoPadre(Request $request)
    {

        $validateData = Validator::make($request->all(), [
            'paterno_padre' => ['required', 'min:3'],
            'materno_padre' => ['required', 'min:3'],
            'nombres_padre' => ['required', 'min:3'],
            'dniPadre' => ['required', 'max:8'],
            'fecha_nacimiento_padre' => ['required'],
            'telefono_padre' => ['required', 'min:6', 'max:9'],
            'email_personal_padre' => ['required'],
            'direccion_padre' => ['required'],
            'distrito_padre_id' => ['required'],
            'apoderado' => ['required'],
            'vivepadre' => ['required'],
            'vive_alumno_padre' => ['required'],
            'estado_civil' => ['required'],
            'nivel_instruccion_padre' => ['required'],
            'religion_padre' => ['required'],
            'profesion_padre' => ['required'],
            'ocupacion_padre' => ['required'],
            'centro_trabajo_padre' => ['required'],
            'paterno_madre' => ['required'],
            'materno_madre' => ['required'],
            'nombres_madre' => ['required'],
            'dniMadre' => ['required', 'max:8'],
            'fecha_nacimiento_madre' => ['required'],
            'telefono_madre' => ['required'],
            'email_personal_madre' => ['required'],
            'direccion_madre' => ['required'],
            'distrito_madre_id' => ['required'],
            'apoderado2' => ['required'],
            'viveMadre' => ['required'],
            'vive_alumno_madre' => ['required'],
            'nivel_instruccion_madre' => ['required'],
            'religion_madre' => ['required'],
            'profesion_madre' => ['required'],
            'ocupacion_madre' => ['required'],
            'centro_trabajo_madre' => ['required'],
            'paterno' => ['required'],
            'materno' => ['required'],
            'nombres' => ['required'],
//            'DNI' => ['required', 'max:8', 'unique:personas,DNI,id'],
            'DNI' => ['required', 'max:8'],
            'sexo' => ['required'],
            'fecha_nacimiento' => ['required'],
            'telefono' => ['required'],
            'email_personal' => ['required'],
            'direccion' => ['required'],
            'distrito_id' => ['required'],
            'num_hermanos' => ['required'],
            'num_ocupa' => ['required'],
            'religion' => ['required'],
            'gru_sanginio' => ['required'],
            'parto' => ['required'],
            'enfermedades' => ['required'],
            'alergias' => ['required'],
            'accidentes' => ['required'],
            'traumas' => ['required'],
            'lim_fisicas' => ['required'],
            'peso' => ['required'],
            'talla' => ['required'],
            'vacunas' => ['required'],
//            't_seguro' => ['required'],
//            'n_seguro' => ['required'],
            'num_emergencia' => ['required'],
            'relacion' => ['required'],
            'm_tiempo' => ['required'],
            'carrera_id' => ['required'],
            'ciclo' => ['required'],
            'grupo' => ['required'],
            'fecha_ingreso' => ['required'],
        ]);
        if ($validateData->fails()) {
            return response()->json(['errors' => $validateData->errors()], 200);
        }

        $apersona_check = Persona::where('DNI', $request->DNI)->first();
        $ppersona_check = Persona::where('DNI', $request->dniPadre)->first();
        $mpersona_check = Persona::where('DNI', $request->dniMadre)->first();
        $padre_id = 0;
        $madre_id = 0;

        DB::beginTransaction();
        if (!$ppersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno_padre;
            $persona->materno = $request->materno_padre;
            $persona->nombres = $request->nombres_padre;
            $persona->DNI = $request->dniPadre;
            $persona->sexo = 'M';
            $persona->fecha_nacimiento = $request->fecha_nacimiento_padre;
            $persona->telefono = $request->telefono_padre;
            $persona->email = $request->email_padre;
            $persona->email_personal = $request->email_personal_padre;
            $persona->direccion = $request->direccion_padre;
            $persona->distrito_id = $request->distrito_padre_id;
            $persona->save();
            $ppersona_id = $persona->id;


            $padre = new Padre();
            $padre->persona_id = $ppersona_id;
            $padre->apoderado = $request->apoderado;
            $padre->tipo = 'P';
            $padre->vive = $request->vivepadre;
            $padre->vive_alumno = $request->vive_alumno_padre;
            $padre->estado_civil = $request->estado_civil;
            $padre->nivel_instruccion = $request->nivel_instruccion_padre;
            $padre->religion = $request->religion_padre;
            $padre->profesion = $request->profesion_padre;
            $padre->ocupacion = $request->ocupacion_padre;
            $padre->centro_trabajo = $request->centro_trabajo_padre;
            $padre->save();
            $padre_id = $padre->id;

            if ($request->apoderado == 'si') {
                $usuario = new Usuario();
                $usuario->usuario = $request->dniPadre;
                $usuario->password = Hash::make($request->dniPadre);
                $usuario->estado = 1;
                $usuario->tipo = 'padre';
                $usuario->persona_id = $ppersona_id;
                $usuario->save();
                DB::table('modulo_usuario')->insert([
                    'modulo_id' => 10,
                    'usuario_id' => $usuario->id,
                ]);
            }
        } else {
            $ppersona_check->paterno = $request->paterno_padre;
            $ppersona_check->materno = $request->materno_padre;
            $ppersona_check->nombres = $request->nombres_padre;
            $ppersona_check->DNI = $request->dniPadre;
            $ppersona_check->sexo = 'M';
            $ppersona_check->fecha_nacimiento = $request->fecha_nacimiento_padre;
            $ppersona_check->telefono = $request->telefono_padre;
            $ppersona_check->email = $request->email_padre;
            $ppersona_check->email_personal = $request->email_personal_padre;
            $ppersona_check->direccion = $request->direccion_padre;
            $ppersona_check->distrito_id = $request->distrito_padre_id;
            $ppersona_check->save();

            $padretemp = Padre::where('persona_id', $ppersona_check->id)->get();
            $padre_id = $padretemp->first()->id;

            $padre = Padre::find($padre_id);

            $padre->apoderado = $request->apoderado;
            $padre->tipo = 'P';
            $padre->vive = $request->vivepadre;
            $padre->vive_alumno = $request->vive_alumno_padre;
            $padre->estado_civil = $request->estado_civil;
            $padre->nivel_instruccion = $request->nivel_instruccion_padre;
            $padre->religion = $request->religion_padre;
            $padre->profesion = $request->profesion_padre;
            $padre->ocupacion = $request->ocupacion_padre;
            $padre->centro_trabajo = $request->centro_trabajo_padre;
            $padre->save();

        }

        if (!$mpersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno_madre;
            $persona->materno = $request->materno_madre;
            $persona->nombres = $request->nombres_madre;
            $persona->DNI = $request->dniMadre;
            $persona->sexo = 'F';
            $persona->fecha_nacimiento = $request->fecha_nacimiento_madre;
            $persona->telefono = $request->telefono_madre;
            $persona->email = $request->email_madre;
            $persona->email_personal = $request->email_personal_madre;
            $persona->direccion = $request->direccion_madre;
            $persona->distrito_id = $request->distrito_madre_id;
            $persona->save();
            $mpersona_id = $persona->id;

            $madre = new Padre();
            $madre->persona_id = $mpersona_id;
            $madre->apoderado = $request->apoderado2;
            $madre->tipo = 'M';
            $madre->vive = $request->viveMadre;
            $madre->vive_alumno = $request->vive_alumno_madre;
            $madre->estado_civil = $request->estado_civil;
            $madre->nivel_instruccion = $request->nivel_instruccion_madre;
            $madre->religion = $request->religion_madre;
            $madre->profesion = $request->profesion_madre;
            $madre->ocupacion = $request->ocupacion_madre;
            $madre->centro_trabajo = $request->centro_trabajo_madre;
            $madre->save();
            $madre_id = $madre->id;
            if ($request->apoderado2 === 'si') {
                $usuario = new Usuario();
                $usuario->usuario = $request->dniMadre;
                $usuario->password = Hash::make($request->dniMadre);
                $usuario->estado = 1;
                $usuario->tipo = 'padre';
                $usuario->persona_id = $mpersona_id;
                $usuario->save();

                DB::table('modulo_usuario')->insert([
                    'modulo_id' => 10,
                    'usuario_id' => $usuario->id,
                ]);
            }
        } else {
            $mpersona_check->paterno = $request->paterno_madre;
            $mpersona_check->materno = $request->materno_madre;
            $mpersona_check->nombres = $request->nombres_madre;
            $mpersona_check->DNI = $request->dniMadre;
            $mpersona_check->sexo = 'F';
            $mpersona_check->fecha_nacimiento = $request->fecha_nacimiento_madre;
            $mpersona_check->telefono = $request->telefono_madre;
            $mpersona_check->email = $request->email_madre;
            $mpersona_check->email_personal = $request->email_personal_madre;
            $mpersona_check->direccion = $request->direccion_madre;
            $mpersona_check->distrito_id = $request->distrito_madre_id;
            $mpersona_check->save();

            $padre = Padre::where('persona_id', $mpersona_check->id)->get();
            $madre_id = $padre->first()->id;
            $madre = Padre::find($madre_id);
            $madre->apoderado = $request->apoderado;
            $madre->tipo = 'M';
            $madre->vive = $request->viveMadre;
            $madre->vive_alumno = $request->vive_alumno_madre;
            $madre->estado_civil = $request->estado_civil;
            $madre->nivel_instruccion = $request->nivel_instruccion_madre;
            $madre->religion = $request->religion_madre;
            $madre->profesion = $request->profesion_madre;
            $madre->ocupacion = $request->ocupacion_madre;
            $madre->centro_trabajo = $request->centro_trabajo_madre;
            $madre->save();
        }

        if (!$apersona_check) {
            $persona = new Persona();
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->nombres = $request->nombres;
            $persona->DNI = $request->DNI;
            $persona->sexo = $request->sexo;
            $persona->fecha_nacimiento = $request->fecha_nacimiento;
            $persona->telefono = $request->telefono;
            $persona->email = $request->DNI . '@juanpabloii.edu.pe';
            $persona->email_personal = $request->email_personal;
            $persona->direccion = $request->direccion;
            $persona->distrito_id = $request->distrito_id;
            $persona->save();
            $apersona_id = $persona->id;

            $alumno = new Alumno();
            $alumno->persona_id = $apersona_id;
            $alumno->padre_id = $padre_id;
            $alumno->madre_id = $madre_id;
            $alumno->estado = '1';
            $alumno->num_hermanos = $request->num_hermanos;
            $alumno->num_ocupa = $request->num_ocupa;
            $alumno->religion = $request->religion;
            $alumno->gru_sanginio = $request->gru_sanginio;
            $alumno->parto = $request->parto;
            $alumno->enfermedades = $request->enfermedades;
            $alumno->alergias = $request->alergias;
            $alumno->accidentes = $request->accidentes;
            $alumno->traumas = $request->traumas;
            $alumno->lim_fisicas = $request->lim_fisicas;
            $alumno->peso = $request->peso;
            $alumno->talla = $request->talla;
            $alumno->vacunas = $request->vacunas;
            $alumno->t_seguro = $request->t_seguro;
            $alumno->n_seguro = $request->n_seguro;
            $alumno->num_emergencia = $request->num_emergencia;
            $alumno->relacion = $request->relacion;
            $alumno->m_tiempo = $request->m_tiempo;
            $alumno->save();
            $alumno_id = $alumno->id;


            $usuario = new Usuario();
            $usuario->usuario = $request->DNI;
            $usuario->password = Hash::make($request->DNI);
            $usuario->estado = 1;
            $usuario->tipo = 'alumno';
            $usuario->persona_id = $apersona_id;
            $usuario->save();

            DB::table('modulo_usuario')->insert([
                'modulo_id' => 1,
                'usuario_id' => $usuario->id
            ]);

            $pecarrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', '1']])->first();
            $alumno_carrera_check = AlumnoCarrera::where('alumno_id', $alumno->id)->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $request->carrera_id;
                $alumno_carrera->alumno_id = $alumno_id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->pe_carrera_id = $pecarrera->id;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();
            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }

            $categoria = Categoria::where([['carrera_id', $request->carrera_id], ['nombre', 'B']])->get();
//            dd($request->carrera_id);

            DB::table('alumno_categoria')->insert([
                'categoria_id' => $categoria->first()->id,
                'alumno_id' => $alumno_id,
                'estado' => 1,
            ]);
        } else {
            $alumno_id = $apersona_check->alumno->id;
            $apersona_check->paterno = $request->paterno;
            $apersona_check->materno = $request->materno;
            $apersona_check->nombres = $request->nombres;
            $apersona_check->DNI = $request->DNI;
            $apersona_check->sexo = $request->sexo;
            $apersona_check->fecha_nacimiento = $request->fecha_nacimiento;
            $apersona_check->telefono = $request->telefono;
            $apersona_check->email = $request->DNI . '@juanpabloii.edu.pe';
            $apersona_check->email_personal = $request->email_personal;
            $apersona_check->direccion = $request->direccion;
            $apersona_check->distrito_id = $request->distrito_id;
            $apersona_check->save();
            $alumno = Alumno::find($alumno_id);
            $pecarrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', '1']])->first();

            $alumno_carrera_check = AlumnoCarrera::where([['carrera_id', $request->carrera_id], ['alumno_id', $alumno_id], ['ciclo', $request->ciclo]])->first();
            if (!$alumno_carrera_check) {
                $alumno_carrera = new AlumnoCarrera();
                $alumno_carrera->carrera_id = $request->carrera_id;
                $alumno_carrera->alumno_id = $alumno_id;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->pe_carrera_id = $pecarrera->id;
                $alumno_carrera->fecha_ingreso = $request->fecha_ingreso;
                $alumno_carrera->save();
            } else {
                $alumno_carrera = $alumno_carrera_check;
                $alumno_carrera->ciclo = $request->ciclo;
                $alumno_carrera->grupo = $request->grupo;
                $alumno_carrera->save();
            }
        }
        DB::commit();
        $apoderado = '';
        $tpadre = Padre::find($padre_id);
        $tmadre = Padre::find($madre_id);
        if ($tpadre->apoderado === 'si') {
            $apoderado = $tpadre;
        } else {
            $apoderado = $tmadre;
        }
        if ($apersona_check) {
            return response()->json(['alumno' => $alumno_id, 'apoderado' => $apoderado->id, 'carrera' => $request->carrera_id, 'grado' => $request->ciclo, 'messaje' => 'Alumno Actualizado'], 200);
        } else {
            return response()->json(['alumno' => $alumno_id, 'apoderado' => $apoderado->id, 'carrera' => $request->carrera_id, 'grado' => $request->ciclo, 'messaje' => 'Alumno Registrado'], 200);
        }

    }

    public function terminos($alumno_id, $padre_id, $carrera_id, $grado)
    {
//        dd($alumno_id,$padre_id,$carrera_id,$grado);
        $cronograma = Cronograma::where('estado', '1')->get();
        $alumno = Alumno::find($alumno_id);
        $apoderado = Padre::find($padre_id);
        $carrera = Carrera::find($carrera_id);
        $periodo = Periodo::where('estado', 1)->first();
        $grados = $alumno->alumnocarreras->last()->ciclo;
        $datos = ['alumno' => $alumno, 'apoderado' => $apoderado, 'carrera' => $carrera, 'cronograma' => $cronograma, 'grado' => $grados, 'periodo' => $periodo];
        $pdf = Mpdf::loadView('IntranetPadre.RegistroAlumnos.termino_condiciones', $datos, ['mode' => 'utf-8', 'format' => 'A4-L']);
//        return $pdf->download('termino_condiciones.pdf');
        return $pdf->stream();
    }

    public function verFicha($id)
    {
        $alumno = Alumno::find($id);
        $padre = Padre::find($alumno->padre_id);
        $madre = Padre::find($alumno->madre_id);
        $data = [
            'alumno' => $alumno,
            'padre' => $padre,
            'madre' => $madre
        ];
        $pdf = PDF::loadView('SecretariaAcademica.Cronogramas.fichaAlumno', $data);
        return $pdf->stream();
    }

    public function get_x_conograma($dni)
    {

        $period = Periodo::where('estado', 1)->first();
        $alumno = Alumno::with('persona', 'matricula')
            ->whereHas('persona', function ($q) use ($dni) {
                $q->where('DNI', $dni);
            })
            ->whereHas('alumnocarreras.matriculas', function ($q) use ($period) {
                $q->where('periodo_id', $period->id);
            })
            ->first();
//        return response()->json($alumno);
        $data_alumnos = collect();

        if (!$alumno)
            return response()->json('sinMatricula', 200);

        $cronogramas = Cronograma::with('alumno')
            ->whereDoesntHave('alumnoPago', function ($q) use ($alumno) {
                $q->where([['alumno_id', $alumno->id], ['estado', 'Pagado']]);
            })
            ->whereHas('concepto', function ($q) {
                $q->where('nombre', 'like', '%PENSI%');
            })
            ->get();
//        $NumTalleres = count(MatriculaDetalle::with('seccion.pe_curso.curso')->wherehas('seccion.pe_curso.curso', function ($query) {
//            $query->where('nombre', 'like', 'Taller%');
//        })->where('matricula_id', $alumno->matricula->first()->id)->get());
//        if ($NumTalleres > 1)
//            $masMonto = 60 * ($NumTalleres - 1);
//        else
//            $masMonto = 0;
        foreach ($cronogramas as $cronograma) {
            $data = [
                'cronograma_id' => $cronograma->id,
                'alumno_id' => $alumno->id,
                'alumno' => strtoupper($alumno->persona->paterno . ' ' . $alumno->persona->materno . ', ' . $alumno->persona->nombres),
                'nivel' => $alumno->alumnocarreras->first()->carrera->nombre,
                'grado' => $alumno->alumnocarreras->first()->ciclo,
                'categoria' => $alumno->categorias->last()->nombre,
                'mes' => strtoupper(Carbon::parse($cronograma->fech_vencimiento)->locale('es')->monthName),
                'fecha_vencimiento' => $cronograma->fech_vencimiento->format('d-m-Y'),
                'monto' => $alumno->categoriaActiva->last()->monto,
            ];
            $data_alumnos->push($data);
        }

//        dd($data_alumnos);
//        return response()->json(['alumnopagos' => $masMonto], 200);
        return response()->json(['alumnopagos' => $data_alumnos], 200);
    }

    public function getAlumnoByPadre(Request $request)
    {
        $padre = Padre::find($request->padre_id);
        $alData = [];
        if ($padre->tipo == 'P') {
            $alumnos = Alumno::with('padre', 'persona')->where('padre_id', $padre->id)->get();
        } else if ($padre->tipo == 'M') {
            $alumnos = Alumno::with('padre', 'persona')->where('madre_id', $padre->id)->get();
        }
        $periodo = Periodo::where('estado', 1)->first();
        foreach ($alumnos as $alumno) {
            $alPago = Alumnopago::with('cronograma')
                ->where('alumno_id', $alumno->id)
                ->whereHas('cronograma', function ($q) use ($periodo) {
                    $q->where('periodo_id', $periodo->id);
                })
                ->get();
            $habPago = count($alPago) > 0 ? 0 : 1;
            $data = [
                'id' => $alumno->id,
                'dni' => $alumno->persona->DNI,
                'nombre' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ' . $alumno->persona->nombres,
                'telefono' => $alumno->persona->telefono,
                'nivel' => $alumno->alumnocarreras->last()->carrera->nombre,
                'grado' => $alumno->alumnocarreras->last()->ciclo,
                'url' => route('terminos', ['alumno_id' => $alumno->id, 'padre_id' => $padre->id, 'carrera_id' => $alumno->alumnocarreras->last()->carrera_id, 'grado' => $alumno->alumnocarreras->last()->ciclo]),
                'habPago' => $habPago,
            ];
            array_push($alData, $data);
        }
        return response()->json(['alumnos' => $alData], 200);
    }

    public function updateOnlyAlumno(Request $request)
    {
        $alumno = Alumno::find($request->alumno_id);

        $persona = $alumno->persona;
        DB::beginTransaction();
        $persona->sexo = $request->sexo;
        $persona->fecha_nacimiento = $request->fecha_nacimiento;
        $persona->telefono = $request->telefono;
        $persona->email_personal = $request->email_personal;
        $persona->direccion = $request->direccion;
        $persona->distrito_id = $request->distrito_id;
        $persona->save();
        $alumno->num_hermanos = $request->num_hermanos;
        $alumno->num_ocupa = $request->num_ocupa;
        $alumno->religion = $request->religion;
        $alumno->gru_sanginio = $request->gru_sanginio;
        $alumno->parto = $request->parto;
        $alumno->enfermedades = $request->enfermedades;
        $alumno->alergias = $request->alergias;
        $alumno->accidentes = $request->accidentes;
        $alumno->traumas = $request->traumas;
        $alumno->lim_fisicas = $request->lim_fisicas;
        $alumno->peso = $request->peso;
        $alumno->talla = $request->talla;
        $alumno->vacunas = $request->vacunas;
        $alumno->t_seguro = $request->t_seguro;
        $alumno->n_seguro = $request->n_seguro;
        $alumno->num_emergencia = $request->telefonoe;
        $alumno->save();
        DB::commit();

        return response()->json(['alumno' => $alumno], 200);
    }

    public function getInscritosNivel()
    {
//        $alumnos = Alumno::with('persona')
////            ->whereHas('alumnocarreras', function ($q) {
////                $q->where('carrera_id', 2);
////            })
//            ->whereDoesntHave('alumnocarreras')
//            ->get();
//
//        foreach ($alumnos as $loop => $alumno) {
////            echo $loop+1 . ' '. $alumno->persona->DNI . ' ' . $alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ' . $alumno->persona->nombres . ' ' . $alumno->padre_id . ',' . $alumno->madre_id . "</br>";
////            echo $alumno->padre_id . ',' . $alumno->madre_id.',';
//            echo $alumno->id.',';
//        }

        $periodos = Periodo::all();
        $carreras = Carrera::all();
        return view('SecretariaAcademica.Matriculas.rpt_inscritos_nivel', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function getInscritosNivelData($periodo_id = null, $carrera_id = null)
    {
        if ($periodo_id && $carrera_id) {
            $alumos = Alumno::with('persona', 'alumnocarreras.carrera', 'categorias', 'padre.persona', 'madre.persona')
                ->whereHas('alumnocarreras', function ($q) use ($periodo_id, $carrera_id) {
                    $q->where([['periodo_id', $periodo_id], ['carrera_id', $carrera_id], ['estado', '1']]);
                })
                ->get();
//            $data=[];
//            foreach ($alumos as $alumo) {
//
//                $dato=[
//                    'dni'=>$alumo->persona->DNI
//                    ];
//                    $data->push($dato);
//            }

            return response()->json(['alumnos'=>$alumos], 200);
        } else return redirect()->back();
    }

    public function getInscritosNuevosNivel()
    {
        $alumnos = Alumno::all();
        foreach ($alumnos as $alumno) {
            echo $alumno->persona->DNI . "<br>";
        }
//        $periodos = Periodo::all();
//        $carreras = Carrera::all();
//        return view('SecretariaAcademica.Matriculas.rpt_inscritos_nuevos_nivel', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function getInscritosNuevosNivelData($periodo_id = null, $carrera_id = null)
    {
        if ($periodo_id && $carrera_id) {
            $alumos = Alumno::with('persona', 'alumnocarreras.carrera', 'categorias', 'padre.persona', 'madre.persona')
                ->whereHas('alumnocarreras', function ($q) use ($periodo_id, $carrera_id) {
                    $q->where([['periodo_id', $periodo_id], ['carrera_id', $carrera_id], ['estado', '1']]);
                })
                ->where('nuevo', '1')
                ->get();
            return response()->json(['alumnos' => $alumos], 200);
        } else return redirect()->back();
    }
}
