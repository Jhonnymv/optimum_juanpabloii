<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;

class PruebaController extends Controller
{
    public function index(){

    $lista = Alumno::all();
     return view('Alumnos.matricula',
    [
        'lista' => $lista
    ]);
    }

    public function guardar(Request $request){

        $alumno = new Alumno(
            [
                'apellido' => $request->apellido,
                'nombre' => $request->nombre,
                'email' => $request->email
            ]
        );

        
        $alumno->save();

        $lista = Alumno::all();
            return view('Alumnos.matricula',
        [
            'lista' => $lista
        ]);

    }

}
