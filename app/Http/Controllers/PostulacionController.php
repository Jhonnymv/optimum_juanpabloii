<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Departamento;
use App\PeCarrera;
use App\Periodo;
use App\Persona;
use App\Postulacion;
use App\PostulacionAnexo;
use App\Postulante;
use App\User;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\Hash;


class PostulacionController extends Controller
{
    public function index($msj='')
    {
        $periodos = Periodo::where('estado', '=', '1')->get();
        $carreras = Carrera::whereNotIn('id', [6, 9])->get();

        $departamentos = Departamento::all();

        return view('Admisiones.formulario_ingreso')
            ->with('departamentos', $departamentos)
            ->with('carreras', $carreras)
            ->with('periodos', $periodos)
            ->with('msj',$msj);
    }

    public function store(Request $request)
    {
        $pe_carrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', 1]])->first();

        $periodo = Periodo::where('estado', 1)->first();
        $persona_check = Persona::where('dni', $request->dni)->first();

//        $postulaciones_check = Postulacion::whereHas('postulante.persona', function ($q) use ($request) {
//            $q->where('dni', $request->dni);
//        })->get();

//        if (count($postulaciones_check) == 0) {
            DB::beginTransaction();
            if (!$persona_check) {
                $persona=new Persona();
                $persona->nombres = $request->nombres;
                $persona->paterno = $request->paterno;
                $persona->materno = $request->materno;
                $persona->telefono = $request->telefono;
                $persona->direccion = $request->direccion;
                $persona->dni = $request->dni;
                $persona->fecha_nacimiento = $request->fecha_nacimiento;
                $persona->email_personal = $request->email_personal;
                $persona->img_identificacion = $request->img_identificacion;
                $persona->save();
                $persona_id = $persona->id;

                $usuario=new Usuario();
                $usuario->persona_id=$persona->id;
                $usuario->usuario=$request->dni;
                $usuario->password=Hash::make($request->dni);
                $usuario->estado="1";
                $usuario->save();

                DB::table('modulo_usuario')->insert([
                    'modulo_id'=>8,
                    'usuario_id'=>$usuario->id
                ]);

            } else
                $persona_id = $persona_check->id;

            $postulante_check = Postulante::where('persona_id', $persona_id)->first();

            if (!$postulante_check) {
                $postulante = new Postulante();
                $postulante->condicion = $request->condicion;
                if ($request->condicion == 'Exonerado')
                    $postulante->tipo_exoneracion = $request->tipo_exoneracion;
                $postulante->sexo = $request->sexo;
                $postulante->persona_id = $persona_id;
                $postulante->distrito_id = $request->distrito_id;
                $postulante->colegio = $request->colegio;
                $postulante->año_culmino = $request->año_culmino;
                $postulante->datos_contacto = $request->datos_contacto;
                $postulante->telefono_contacto = $request->telefono_contacto;
                $postulante->save();
            } else {
                $postulante = $postulante_check;
                $postulante->condicion = $request->condicion;
                if ($request->condicion == 'Exonerado') {
                    $postulante->tipo_exoneracion = $request->tipo_exoneracion;
                }
                else if($request->condicion == 'Ordinario') {
                    $postulante->tipo_exoneracion = null;
                }
                $postulante->save();
            }

            $postulacion_check=Postulacion::where([['periodo_id',$periodo->id],['postulante_id',$postulante->id]])->first();
            if(!$postulacion_check) {
                $postulacion = new Postulacion();

                $postulacion->pe_carrera_id = $pe_carrera->id;
                $postulacion->periodo_id = $periodo->id;
                $postulacion->postulante_id = $postulante->id;
                $postulacion->fecha_pos = date('Y-m-d');
                $postulacion->save();

                $pos_anexo = new PostulacionAnexo();
                $pos_anexo->postulacion_id = $postulacion->id;
                $pos_anexo->tipo = "1";
                $pos_anexo->nombre = 'dni';
                $dni_name = $request->dni . 'dni.' . $request->file('img_dni')->getClientOriginalExtension();
                $request->file('img_dni')->storeAs('public/postulantes/' . $request->dni . '/', $dni_name);
                $patch = Storage::url('postulantes/' . $request->dni . '/' . $dni_name);
                $pos_anexo->url = $patch;
                $pos_anexo->save();

                $pos_anexo = new PostulacionAnexo();
                $pos_anexo->postulacion_id = $postulacion->id;
                $pos_anexo->tipo = "2";
                $pos_anexo->nombre = 'foto_name';
                $foto_name = $request->dni . 'foto.' . $request->file('foto_identificacion')->getClientOriginalExtension();
                $request->file('foto_identificacion')->storeAs('public/postulantes/' . $request->dni . '/', $foto_name);
                $patch = Storage::url('postulantes/' . $request->dni . '/' . $foto_name);
                $pos_anexo->url = $patch;
                $pos_anexo->save();

                $pos_anexo = new PostulacionAnexo();
                $pos_anexo->postulacion_id = $postulacion->id;
                $pos_anexo->tipo = "3";
                $pos_anexo->nombre = 'voucher';
                $voucher = $request->dni . 'voucher.' . $request->file('voucher')->getClientOriginalExtension();
                $request->file('voucher')->storeAs('public/postulantes/' . $request->dni . '/', $voucher);
                $patch = Storage::url('postulantes/' . $request->dni . '/' . $voucher);
                $pos_anexo->url = $patch;
                $pos_anexo->save();


                if ($request->checkCertificado != null) {
                    $pos_anexo = new PostulacionAnexo();
                    $pos_anexo->postulacion_id = $postulacion->id;
                    $pos_anexo->tipo = "4";
                    $pos_anexo->nombre = 'img_certificado';
                    $img_certificado = $request->dni . 'certificado_estudios.' . $request->file('img_certificado')->getClientOriginalExtension();
                    $request->file('img_certificado')->storeAs('public/postulantes/' . $request->dni . '/', $img_certificado);
                    $patch = Storage::url('postulantes/' . $request->dni . '/' . $img_certificado);
                    $pos_anexo->url = $patch;
                    $pos_anexo->save();
                }

                if ($request->checkPartNacimiento != null) {
                    $pos_anexo = new PostulacionAnexo();
                    $pos_anexo->postulacion_id = $postulacion->id;
                    $pos_anexo->tipo = "5";
                    $pos_anexo->nombre = 'img_part_nacimiento';
                    $img_part_nacimiento = $request->dni . 'partida_nac.' . $request->file('img_part_nacimiento')->getClientOriginalExtension();
                    $request->file('img_part_nacimiento')->storeAs('public/postulantes/' . $request->dni . '/', $img_part_nacimiento);
                    $patch = Storage::url('postulantes/' . $request->dni . '/' . $img_part_nacimiento);
                    $pos_anexo->url = $patch;
                    $pos_anexo->save();
                }
                if ($request->file('const_exonerac')) {
                    $pos_anexo = new PostulacionAnexo();
                    $pos_anexo->postulacion_id = $postulacion->id;
                    $pos_anexo->tipo = "6";
                    $pos_anexo->nombre = 'const_exonerac';
                    $const_exonerac = $request->dni . 'const_exoneracion.' . $request->file('const_exonerac')->getClientOriginalExtension();
                    $request->file('const_exonerac')->storeAs('public/postulantes/' . $request->dni . '/', $const_exonerac);
                    $patch = Storage::url('postulantes/' . $request->dni . '/' . $const_exonerac);
                    $pos_anexo->url = $patch;
                    $pos_anexo->save();
                }
            }
            DB::commit();
            return view('Admisiones.descargar_solicitud', ['postulante' => $postulante]);
//        }
//        return $this->index('ya estás registrado en este periodo');
    }

    public function update(Request $request)
    {
        $pos_anexo = new PostulacionAnexo();
        $pos_anexo->postulacion_id = $request->postulacion_id;
        $pos_anexo->tipo = "7";
        $pos_anexo->nombre = 'solicitud';
        $solicitud = $request->dni . 'solicitud.' . $request->file('solicitud')->getClientOriginalExtension();
        $request->file('solicitud')->storeAs('public/postulantes/' . $request->dni . '/' . '/', $solicitud);
        $patch = Storage::url('postulantes/' . $request->dni . '/' . $solicitud);
        $pos_anexo->url = $patch;
        $pos_anexo->save();
        DB::commit();

        return redirect()->route('admisiones');

    }


    public function download_pdf($id)
    {
        $postulante = Postulante::find($id);
        $data = [
            'postulante' => $postulante,
        ];
        $pdf = PDF::loadview('Admisiones.solicitud_pdf', $data);
        return $pdf->stream();
    }

    public function validarpago_pos()
    {
//        $postulaciones=Postulacion::doesntHave('postulante')->get();
//        dd($postulaciones);
        $periodo = Periodo::where('estado', 1)->first();
        $postulaciones = Postulacion::where([['estado', 'Solicitado'], ['periodo_id', $periodo->id]])->get();
//
//
        return view('AreaContabilidad.Postulantes.v_pagos_pos', ['postulaciones' => $postulaciones]);
//        dd($periodo);
    }

    public function indexConsultarPostulacion()
    {
        return view('Admisiones.consultar_postulación');
    }

    public function buscarxdni($dni)
    {
//        return $dni;

        $postulantexdni = Postulante::with('persona','distrito','postulaciones.pe_carrera.carrera')->whereHas('persona', function ($q) use ($dni) {
            $q->where('DNI', $dni);
        })->first();

//        $postulante_id = $postulantexdni->id;
//        $postulante = Postulante::find($postulante_id);

//        dd($postulante);

        return response()->json(['postulante' => $postulantexdni], 200);
    }
}
