<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use App\Carrera;
use App\Categoria;
use App\Curso;
use App\Departamento;
use App\Distrito;
use App\EvaluacionCategoria;
use App\Instrument;
use App\MatriculaDetalle;
use App\Padre;
use App\Periodo;
use App\Persona;
use App\PlanEstudio;
use App\Provincia;
use App\User;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class PadreController extends Controller
{
    //

    public function showHijos()
    {
        $usuario = Auth::user()->persona->padre;
        $hijos = Alumno::with('persona')->where('padre_id', '=', $usuario->id)->orWhere('madre_id', '=', $usuario->id)->get();
//        dd($hijos->carreras);
        $departamentos=Departamento::all();
        $provincias=Provincia::all();
        $distritos=Distrito::all();
        return view('IntranetPadre.RegistroAlumnos.showHijos', ['hijos' => $hijos,'departamentos'=>$departamentos,'provincias'=>$provincias,'distritos'=>$distritos]);
    }

    public function showHijosAcademica()
    {
        $usuario = Auth::user()->persona->padre;
        $hijos = Alumno::with('persona')->where('padre_id', '=', $usuario->id)->orWhere('madre_id', '=', $usuario->id)->get();
//        dd($hijos);
        return view('IntranetPadre.AreaAcademicaHijo.listaHijos', ['hijos' => $hijos]);
    }
    public function showHijosAcademica_notas()
    {
        $usuario = Auth::user()->persona->padre;
        $hijos = Alumno::with('persona')->where('padre_id', '=', $usuario->id)->orWhere('madre_id', '=', $usuario->id)->get();
//        dd($hijos);
        return view('IntranetPadre.AreaAcademicaHijo.listaHijos', ['hijos' => $hijos]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'dni' => 'unique:personas'
        ]);
        DB::beginTransaction();
        $persona = new Persona();
        $persona->paterno = $request->paterno;
        $persona->materno = $request->materno;
        $persona->nombres = $request->nombres;
        $persona->DNI = $request->dni;
        $persona->fecha_nacimiento = $request->fecha_nacimiento;
        $persona->email_personal = $request->email_personal;
        $persona->telefono = $request->telefono;
        $persona->sexo = $request->sexo;
        $persona->direccion = $request->direccion;
        $persona->distrito_id = $request->distrito_id;
        $persona->save();

        $padre = new Padre();
        $padre->persona_id = $persona->id;
        if ($request->sexo == 'M')
            $padre->tipo = 'P';
        else
            $padre->tipo = 'M';
        $padre->save();

        $usuario = new Usuario();
        $usuario->persona_id = $persona->id;
        $usuario->usuario = $request->dni;
        $usuario->password = Hash::make($request->dni);
        $usuario->save();

        $usuario->modulos()->attach(10);
        DB::commit();

        return redirect()->route('login');
    }

    public function getParents()
    {
        $padres = Padre::all();

        return view('SecretariaAcademica.Reportes.padres_list', ['padres' => $padres]);
    }

    public function create()
    {
        $carreras = Carrera::all();
        return view('SecretariaAcademica.Matriculas.parentsRegister', ['carreras' => $carreras]);
    }

    public function saveSinceSecretaria(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'dnip'=>'required',
            'paternop'=>'required',
            'maternop'=>'required',
            'nombresp'=>'required',
            'dnim'=>'required',
            'paternom'=>'required',
            'maternom'=>'required',
            'nombresm'=>'required',
            'apoderado'=>'required',
            'sDni'=>'required'
        ]);
        if ($validateData->fails()) {
            return response()->json('Asegúrese de completar todos los datos y tener estudiantes asignados', 200);
        }

        DB::beginTransaction();
        $checkPadre = Persona::where('DNI', $request->dnip)->first();
        if (!$checkPadre){
            $personap = $this->savePersona($person=null, $request->paternop, $request->maternop, $request->nombresp, $request->dnip,$sxo='M',$request->telefonop);
            $this->userCreate($personap,'padre');
        }
        else
            $personap = $this->savePersona($checkPadre, $request->paternop, $request->maternop, $request->nombresp, $request->dnip,$sxo='M',$request->telefonop);

        $parentp = Padre::where('persona_id', $personap->id)->first();

        $padrep = $parentp ?? new Padre();
        $padrep->persona_id = $personap->id;
        $padrep->apoderado = $request->apoderado == '1' ? 'si' : 'no';
        $padrep->tipo = 'P';
        $padrep->save();

        $checkMadre = Persona::where('DNI', $request->dnim)->first();
        if (!$checkMadre){
            $personam = $this->savePersona($person=null, $request->paternom, $request->maternom, $request->nombresm, $request->dnim,$sexo='F',$request->telefonom);
            $this->userCreate($personam,'padre');
        }
        else
            $personam = $this->savePersona($checkMadre, $request->paternom, $request->maternom, $request->nombresm, $request->dnim,$sexo='F',$request->telefonom);

        $parentm = Padre::where('persona_id', $personam->id)->first();
        $padrem = $parentm ?? new Padre();
        $padrem->persona_id = $personam->id;
        $padrem->apoderado = $request->apoderado == '0' ? 'si' : 'no';
        $padrem->tipo = 'M';
        $padrem->save();

        for ($i=0;$i<count($request->sDni);$i++){
            $sDni=$request->sDni[$i];
            $studentCheck=Persona::where('DNI',$sDni)->first();
            if (!$studentCheck){
                $personaS = $this->savePersona($person=null, $request->sPaterno[$i], $request->sMaterno[$i], $request->sNombres[$i], $request->sDni[$i],$sexo=null,$request->sTelefono[$i]);
                $this->userCreate($personaS,'Alumno');
            }
            else
                $personaS = $this->savePersona($studentCheck, $request->sPaterno[$i], $request->sMaterno[$i], $request->sNombres[$i], $request->sDni[$i],$sexo=null,$request->sTelefono[$i]);

            $alumnoC=Alumno::with('persona')->whereHas('persona',function ($q) use ($sDni){
               $q->where('DNI',$sDni);
            })->first();

            $alumno=$alumnoC??new Alumno();
            $alumno->persona_id=$personaS->id;
            $alumno->padre_id=$padrep->id;
            $alumno->madre_id=$padrem->id;
            $alumno->nuevo=$request->sNuevo[$i];
            $alumno->estado='1';
            $alumno->save();
            $checkAlCarr=AlumnoCarrera::where([['alumno_id',$alumno->id],['carrera_id',$request->sNivel[$i]]])->first();
            $alCarrera=$checkAlCarr??new AlumnoCarrera();
            $alCarrera->carrera_id=$request->sNivel[$i];
            $alCarrera->pe_carrera_id=$request->sNivel[$i];
            $alCarrera->alumno_id=$alumno->id;
            $alCarrera->periodo_id=Periodo::where('estado','1')->first()->id;
            $alCarrera->ciclo=$request->sCiclo[$i];
            $alCarrera->grupo=$request->sSeccion[$i];
            $alCarrera->fecha_ingreso=date('Y-m-d');
            $alCarrera->estado='1';
            $alCarrera->save();
            if($request->sNivel[$i]=='1'){
                $categoria=Categoria::where([['carrera_id', $request->sNivel[$i]], ['nombre', 'A'],['estado',1]])->first();
            }else{
                $categoria=Categoria::where([['carrera_id', $request->sNivel[$i]], ['nombre', 'C'],['estado',1]])->first();
            }
            $alumno->categorias()->attach($categoria->id,['estado'=>'1']);
        }
        DB::commit();

        return response()->json('Datos guardados');
    }

    public function savePersona($person = null, $paterno, $materno, $nombres, $dni,$sexo,$telefono)
    {
        $persona = $person ?? new Persona();
        $persona->paterno = $paterno;
        $persona->materno = $materno;
        $persona->nombres = $nombres;
        $persona->DNI = $dni;
        $persona->sexo=$sexo;
        $persona->telefono=$telefono;
        $persona->save();

        return $persona;
    }

    public function userCreate($persona,$tipo)
    {
        $user=new Usuario();
        $user->persona_id=$persona->id;
        $user->usuario=$persona->DNI;
        $user->password=Hash::make($persona->DNI);
        $user->estado='1';
        $user->tipo=$tipo;
        $user->save();
        $user->modulos()->attach($tipo=='Alumno'?1:10);
    }
    public function newRegister()
    {
        $carreras = Carrera::all();
        return view('IntranetPadre.RegistroAlumnos.newRegister', ['carreras' => $carreras]);
    }
    public function ver_RecordNotas_hijo($id)
    {
        //$usuario = Auth::user()->persona->padre;
        //$hijos = Alumno::with('persona')->where('padre_id', '=', $usuario->id)->orWhere('madre_id', '=', $usuario->id)->get();
        $alumno = Alumno::find($id);
        $alumno_id = $alumno->id;
        $periodo_id = Periodo::where('estado', 1)->first()->id;
        //$alumno = Auth::user()->persona->alumno;
        $periodo = Periodo::where('estado', 1)->first();
        $categoria = EvaluacionCategoria::all();
        $instrumentos = Instrument::all();
        //dd($categoria);
        //$carrera_id= $alumno->alumnocarreras[0]->carrera_id;
        //$carrera= Carrera::where('id',$carrera_id)->get();
        $matricula_detalle = MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno', function ($q) use ($alumno) {
            $q->where('id', $alumno->id);
        })->whereHas('matricula', function ($q) use ($periodo) {
            $q->where('periodo_id', $periodo->id);
        })->get();
        //dd($matricula_detalle);


        return view('IntranetPadre.AreaAcademicaHijo.record_notas', ['alumno' => $alumno,'matricula_detalles' => $matricula_detalle, 'categorias' => $categoria,'instrumentos'=>$instrumentos,'blade'=>'true']);

    }

}
