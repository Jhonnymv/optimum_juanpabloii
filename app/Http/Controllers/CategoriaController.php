<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Carrera;
use App\Categoria;
use App\Periodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
    public function index()
    {
        $periodos = Periodo::where('estado', '=', '1')->get();
        $carreras = Carrera::where('estado', '=', '1')->get();

        return view('Admin.Categorias.index', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function store(Request $request)
    {
        $categoria = new Categoria();
        $categoria->nombre = $request->nombre;
        $categoria->periodo_id = $request->periodo_id;
        $categoria->carrera_id = $request->carrera_id;
        $categoria->estado = 1;
        $categoria->monto = $request->monto;
        $categoria->observacion = $request->observaciones;
        $categoria->save();

        return redirect()->back();
    }

    public function getcategoriaxcarrera($id)
    {
        $categorias = Categoria::with('periodo')->with('carrera')->where('carrera_id', $id)->get();
        return response()->json(['categorias' => $categorias], 200);
    }

    public function edit($id)
    {
        $categoria = Categoria::with('periodo')->with('carrera')->where('id', $id)->first();
        return response()->json(['categoria' => $categoria], 200);
    }

    public function update(Request $request)
    {
        $categoria = Categoria::find($request->idCategoria);
        $categoria->nombre = $request->nombreEditar;
        $categoria->periodo_id = $request->periodo_idEditar;
        $categoria->carrera_id = $request->carrera_idEditar;
        $categoria->monto = $request->montoEditar;
        $categoria->observacion = $request->observacionesEditar;
        $categoria->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $categoria = Categoria::find($id);

        if ($categoria != null) {
            $categoria->estado = 0;
            $categoria->save();
        }
        return redirect()->back();
    }

    public function showCategorias($id)
    {
        $categorias = Categoria::where('carrera_id', $id)->get();
        return $categorias;
    }

    public function indexAlumnoCategorias()
    {
        $periodos = Periodo::where('estado', '=', '1')->get();
        $carreras = Carrera::where('estado', '=', '1')->get();

        return view('SecretariaAcademica.CategoriaAlumno.index', ['periodos' => $periodos, 'carreras' => $carreras]);
    }

    public function buscarAlumnoxDni($dni)
    {
        $alumno = Alumno::with('persona')->whereHas('persona', function ($q) use ($dni) {
            $q->where('DNI', $dni);
        })->first();

        $alumno_id = $alumno->id;

//        $alumno_categoria = Categoria::whereHas('alumnos', function ($q) use ($alumno_id) {
//            $q->where('alumno_id', $alumno_id);
//        })->where('estado', 1)->first();

//        $alumno_categoria = Categoria::with('alumnos')->whereHas('alumnos', function ($q) use ($alumno_id) {
//            $q->where('alumno_id', $alumno_id);
//        })->get();

//        $categoria_id = $alumno_categoria->categoria_id;

        $alumno_categoria = DB::table('alumno_categoria')->where('alumno_id', $alumno_id)->where('alumno_categoria.estado', 1)
            ->join('alumnos as a', 'alumno_categoria.alumno_id', '=', 'a.id')
            ->join('categorias as c', 'alumno_categoria.categoria_id', '=', 'c.id')
            ->first();
//        $alumno_categoria = $alumno->categoriaActiva;


//        $estadopivot = $alumno_categoria[0]->alumnos[0]->pivot->estado;

        $data = ['alumno' => $alumno,
            'alumno_categoria' => $alumno_categoria];

        return response()->json($data, 200);
    }

    public function storeAlumnoCategoria(Request $request)
    {
        $alumno = Alumno::find($request->idAlumno);
        DB::table('alumno_categoria')->where('alumno_id', $alumno->id)->update(['estado'=>0]);

        $alumno->categorias()->attach($request->categoria, ['estado' => '1']);

        return redirect()->back();
    }

    public function categoriaCarrera($id)
    {
        $categorias = Categoria::where('carrera_id', $id)->where('estado', 1)->get();
        return $categorias;
    }

    public function montoCategoria($id)
    {
        $categorias = Categoria::find($id);
        return $categorias;
    }

}
