<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Carrera;

class ControllerGeneral extends Controller
{
    public function index(){
        return view('ejemplos.ejemplo');
    }

    public function elementos(Request $request){
        
        $carrera = Carrera::all();

        return response()->json(['retornado1'=> $request->parametro1,'retornado2'=>$request->parametro2,'todos'=>$carrera], 200);
    }
}
