<?php

namespace App\Http\Controllers;

use App\Flujo;
use App\Solicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function Sodium\add;

class FlujoController extends Controller
{
    public function store(Request $request)
    {
        if ($request->accion_flujo == 'finalizar') {
            $estado = 'finalizado';
            $reenviado_a = '';
            DB::table('solicitudes')->where('id',$request->solicitud_id)->update(['estado'=>'finalizado']);
        } else {
            $estado = 'en_proceso';
            DB::table('solicitudes')->where('id',$request->solicitud_id)->update(['estado'=>'en_proceso']);
            $reenviado = [];
            if (count($request->un_administrativas) > 0) {
                foreach ($request->un_administrativas as $item) {
                    array_push($reenviado, $item);
                }
            }
            $reenviado_a = implode(',', $reenviado);
        }

        $flujo = new Flujo();
        $flujo->estado = $estado;
        $flujo->observacion = $request->observacion;
        $flujo->fecha = date('Y-m-d');
        $flujo->solicitud_id = $request->solicitud_id;
        $flujo->revisado_por = Auth::user()->id;
        $flujo->reenviado_a = $reenviado_a;
        $flujo->save();
        return redirect()->route('solicitudes.list');
    }

    public function getXSolicitud(Request $request)
    {
        if ($request->wantsJson()) {

            $flujos = Flujo::with('solicitud.usuario.persona', 'solicitud.tramite.unidad_administrativa.usuario.persona', 'usuario.persona')
                ->where('solicitud_id', $request->solicitud_id)->orderBy('created_at','desc')->get();
            return response()->json(['flujos' => $flujos], 200);
        }
    }
}
