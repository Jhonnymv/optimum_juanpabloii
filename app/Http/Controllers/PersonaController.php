<?php

namespace App\Http\Controllers;

use App\Concepto;
use App\Distrito;
use App\Padre;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Persona;
use Illuminate\Support\Facades\Auth;
use App\Periodo;
use App\Alumno;
use App\Alumnopago;

class PersonaController extends Controller
{
    //
    public function index()
    {

        $alumno = Auth::user()->persona->alumno;
        //dd($alumno);
        $alumno_id = $alumno->persona_id;

        //$persona_id = $alumno->persona_id;
        //$periodo_id=Periodo::where('estado',1)->first()->id;
        $persona = Persona::where('id', $alumno_id)->get();

        // dd($persona);

        //if (!$persona){
        return view('Alumnos.editar_datos', ['persona' => $persona]);
        //}else{
        //    return view('Alumnos.home');
        //}

    }

    public function store(Request $request)
    {

        $carrera = new Carrera([
            'nombre' => $request->nombre,
            'resolucion' => $request->resolucion,
            'estado' => 1
        ]);

        $carrera->save();

        return redirect()->route('carreras.index');
    }

    public function update(Request $request)
    {

        $persona = Persona::find($request->id);
        if ($request->hasFile('imgEdit')) {
            $extension = $request->file('imgEdit')->getClientOriginalExtension();
            $nombre_archivo = $request->DNIEdit . '_' . $request->num_operacion . '.' . $extension;
            $persona->urlimg = $nombre_archivo;

        }
        $persona->nombres = $request->nombresEdit;
        $persona->paterno = $request->paternoEdit;
        $persona->materno = $request->maternoEdit;
        $persona->telefono = $request->telefonoEdit;
        $persona->DNI = $request->DNIEdit;
        $persona->fecha_nacimiento = $request->fecha_nacEdit;
        $persona->direccion = $request->direccionEdit;
        $persona->email_personal = $request->emailEdit;

        $persona->save();
        if ($request->hasFile('imgEdit')) {
            $request->file('imgEdit')->storeAs('public/fotos_alumnos/', $nombre_archivo);
        }
        //return redirect()->route('Alumnos.editar_datos.index');
        return back();
    }

    public function buscarPadre($dni)
    {
        $padredata = Padre::with('persona')->whereHas('persona', function ($q) use ($dni) {
            $q->where('DNI', $dni);
        })->first();
        $distrito = Distrito::with('provincia.departamento')->where('id', $padredata->persona->distrito_id)->get();
        return response()->json(['padre' => $padredata, 'distrito' => $distrito]);
    }

    //metodo para buscar alumnos por padre de familia -- matrícula
    public function search_person($dni, $concepto_id)
    {
        $persona = Persona::where('DNI', $dni)->first();

        $periodo = Periodo::where('estado', 1)->first();

        $tipoPadre = Padre::with('persona')->where('persona_id', $persona->id)->pluck('tipo')->first();


        if ($tipoPadre == 'P') {
            $alumnos = Alumno::with('persona', 'categorias')
                ->WhereHas('padre.persona', function ($q) use ($persona) {
                    $q->whereId($persona->id);
                })
                ->whereHas('alumnoPago', function ($q) {
                    $q->where('fe_valor_venta', '0.00');
                })
                ->whereHas('alumnoPago.cronograma', function ($q) use ($periodo) {
                    $q->where('periodo_id', $periodo->id);
                })
                ->get();
        } else {
            $alumnos = Alumno::with('persona', 'categorias')
                ->WhereHas('madre.persona', function ($q) use ($persona) {
                    $q->whereId($persona->id);
                })
                ->whereHas('alumnoPago', function ($q) {
                    $q->where('fe_valor_venta', '0.00');
                })
                ->whereHas('alumnoPago.cronograma', function ($q) use ($periodo) {
                    $q->where('periodo_id', $periodo->id);
                })
                ->get();
        }
        $data_alumnos = collect();
        $sum = 0;
//        $concepto = Concepto::with('cronograma')->where([['periodo_id', $periodo->id], ['nombre', 'like', 'MATR%']])->pluck('precio');
        $concepto = Concepto::find($concepto_id);
        foreach ($alumnos as $alumno) {
            if ($sum < $alumno->categorias->last()->monto)
                $sum = $alumno->categorias->last()->monto;
        }
        foreach ($alumnos as $alumno) {
            $data = [
                'alumno_id' => $alumno->id,
                'alumno' => strtoupper($alumno->persona->paterno . ' ' . $alumno->persona->materno . ', ' . $alumno->persona->nombres),
                'nivel' => $alumno->alumnocarreras->first()->carrera->nombre,
                'grado' => $alumno->alumnocarreras->first()->ciclo,
                'categoria' => $alumno->categorias->last()->nombre,
                'monto' => number_format($concepto->precio / count($alumnos), 2),
                'total' => number_format(($concepto->precio / count($alumnos) * count($alumnos)), 2)
            ];
            $data_alumnos->push($data);
        }
//        return response()->json($data_alumnos, 200);
//        return response()->json($sum/3, 200);
        return response()->json(['persona' => $persona, 'alumnos' => $data_alumnos], 200);
    }

    //metodo para buscar alumnos por padre de familia -- pensión
    public function search_person_pension($dni)
    {
        $persona = Persona::with('padre')->where('dni', $dni)->first();

        $alumnos = Alumno::with('persona', 'categorias')->where('persona_id', $persona->id)
            ->orWhereHas('padre.persona', function ($q) use ($persona) {
                $q->whereId($persona->id);
            })
            ->orWhereHas('madre.persona', function ($q) use ($persona) {
                $q->whereId($persona->id);
            })
            ->get();
        $data_alumnos = collect();
        foreach ($alumnos as $alumno) {
            $alumno_pago = Alumnopago::where('alumno_id', $alumno->id);
            $data_alumnos->push($alumno_pago);
        }
        return response()->json(['persona' => $persona, 'alumnos' => $data_alumnos], 200);
    }
}
