<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Alumnopago;
use App\Concepto;
use App\Cronograma;

use App\Exports\ReportBank;
use App\Exports\ReportDeudasCarreras;
use App\Exports\ReporteDeudas;
use App\Exports\UsersExport;


use App\Grade;
use App\Padre;
use App\Pago;
use App\Periodo;
use App\Persona;
use App\Seccion;
use App\Sequence;

use App\Traits\SendDocSunatTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;
use Maatwebsite\Excel\Facades\Excel;
use MPDF;

use App\Traits\GreenterTrait;
use Greenter\Model\Client\Client;
use Greenter\Model\Company\Address;
use Greenter\Model\Company\Company;
use Greenter\Model\Response\BillResult;
use Greenter\Model\Sale\Invoice;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;
use Greenter\Ws\Services\SunatEndpoints;
use Greenter\Model\Voided\Voided;
use Greenter\Model\Voided\VoidedDetail;
use Greenter\Model\Sale\Document;
use Greenter\Model\Summary\Summary;
use Greenter\Model\Summary\SummaryDetail;


use App\Mail\ComproElect;
use phpDocumentor\Reflection\Types\Expression;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PagoController extends Controller
{
//    use GreenterTrait;
    use SendDocSunatTrait;

    public function pago_matricula()
    {
        $conceptos = Concepto::where('periodo_id', Periodo::where('estado', 1)->first()->id)->get();
        return view('SecretariaAcademica.Pagos.Registrar_Pago', ['conceptos' => $conceptos]);
    }

    public function store(Request $request)
    {
        $concepto = Concepto::find($request->concepto_id);
        $var = strtoupper($concepto->nombre);
        $tipoConcepto = $concepto->tipo;
//        $checMat = strpos($var, 'MATR');
//        $checPen = strpos($var, 'PENSI');

        $persona = Persona::find($request->id_cliente);
        if ($request->tipo_doc == '01') {
            $ruc = $request->doc_ruc;
            $social_reason = $request->nombre_empresa;
        } else {
            $ruc = null;
            $social_reason = null;
        }
        $pago_pdf = null;
        $periodo = Periodo::where('estado', 1)->first();
        DB::beginTransaction();

        if ($concepto->tipo == 'm') {
            $cronograma_id = Cronograma::with('concepto')->whereHas('concepto', function ($q) {
                $q->where('nombre', 'like', 'MATR%');
            })->where([['estado', 'PENDIENTE'], ['periodo_id', $periodo->id]])
                ->first()->id;
            for ($i = 0; $i < count($request->alumno_id); $i++) {
                $alumPago = Alumnopago::with('pago')
                    ->where([['alumno_id', $request->alumno_id[$i]], ['fe_valor_venta', 0]])
                    ->whereHas('pago', function ($q) use ($persona) {
                        $q->where([['fe_estado', 'Pago Deposito'], ['persona_id', $persona->id]]);
                    })->first();
                return response()->json($alumPago);
                if ($alumPago) {
                    $alumPago->delete();
                    $alumPago->pago->delete();
            }

            }
        }
        $pago = new Pago();
        $pago->persona_id = $request->id_cliente;
        $pago->tipo_doc = $request->tipo_doc;
        $pago->ruc = $ruc;
        $pago->social_reason = $social_reason;
        $pago->serie = $request->serie;
        $pago->numero_doc = $request->numero;
        $pago->estado = 1;
        $pago->fe_sub_total = $request->total;
        $pago->fe_igv = 0;
        $pago->fe_total_price = $request->total;
        $pago->fe_estado = 'Pendiente factura';
        $pago->observaciones = null;
        $pago->save();
        $pago_pdf = $pago;
        for ($i = 0; $i < count($request->alumno_id); $i++) {
            $alumno = Alumno::find($request->alumno_id[$i]);
            DB::table('alumno_pago')->insert([
                'pago_id' => $pago->id,
                'cronograma_id' => $request->cronograma_id[$i] ?? $cronograma_id,
                'alumno_id' => $alumno->id,
                'cantidad' => 1,
                'fe_descripcion' => $request->descripcion[$i],
                'fe_unidad' => 'ZZ',
                'fe_precio_und' => $request->monto[$i],
                'fe_igv' => 0,
                'fe_valor_venta' => $request->monto[$i],
                'estado' => 'Pagado'
            ]);
        }

        Sequence::where([['doc_type', $request->tipo_doc], ['status', 1]])->first()->increment('current', 1);
        $this->pago_pdf($pago_pdf);
        DB::commit();
//            if ($request->tipo_doc == '03') {
//            if ($persona->email)
//                Mail::to($persona->email)->send(new ComproElect($pago, 'public/doc_parciales/ComprobanteParcial.pdf'));
//            if ($persona->email_personal)
//                Mail::to($persona->email_personal)->send(new ComproElect($pago, 'public/doc_parciales/ComprobanteParcial.pdf'));


        $res = $this->sendDocument($pago->id, $tipoConcepto);

        if ($res['code'] == "0") {
            $pago->update(['status' => $res['code'], 'fe_estado' => '0']);
            return response()->json('success', 200);
        } else {
            return response()->json('error');
        }
    }

    public function get_factura($pago_id)
    {
        $pago = Pago::find($pago_id);
        return $this->pago_pdf($pago, $action = 'stream');
    }

    public function pago_pdf($pago_pdf, $action = null)
    {
        //dd($pago_pdf->tipo_doc);
        $comprobanteQR = [
            //'link' => 'http://juanpabloii.optimumlab.io/',
//            'link2' => 'http://juanpabloii.optimumlab.io/' . $pago_pdf->serie . '/' . $pago_pdf->numero_doc,
            'Ruc' => '20607116114',
            'Pago' => $pago_pdf->tipo_doc,
            'Serie' => $pago_pdf->serie,
            'Correlativo' => $pago_pdf->numero_doc,
            'igv' => $pago_pdf->fe_igv,
            'total' => -1*$pago_pdf->fe_total_price,
            'Cliente' => $pago_pdf->ruc == '' ? $pago_pdf->persona->DNI : $pago_pdf->ruc,
        ];
            //dd($comprobanteQR);
        $qr = QrCode::generate(implode('|', $comprobanteQR), '../public/qrcodes/qrcodecomprobante.svg');
 //       $qr = QrCode::generate(implode('|', $comprobanteQR), '/home/nr6rixn0zymo/public_html/juanpabloii/qrcodes/qrcodecomprobante.svg');
        if($pago_pdf->tipo_doc=='07')
        {
            //dd($pago_pdf);
            $pago_ref = Pago::where('id',$pago_pdf->fe_referencia_id)->first();
            //dd($pago_ref);
            $pdf = MPDF::loadView('SecretariaAcademica.Comprobantes.NotaCredito', ['pago' => $pago_pdf, 'pago_ref' => $pago_ref, 'qr' => $qr]);
            $doc = $pdf->Output('', 'S');
        }
        else
        {
            $pdf = MPDF::loadView('SecretariaAcademica.Comprobantes.ComprobantePago', ['pago' => $pago_pdf,  'qr' => $qr]);
            $doc = $pdf->Output('', 'S');
        }
        Storage::put('public/doc_parciales/ComprobanteParcial.pdf', $doc);
        if ($action != null) {
            return $pdf->stream();
        }
    }


    public function AllPpagos()
    {
        $pagos = Pago::all();
        return view('SecretariaAcademica.Pagos.list_pagos', ['pagos' => $pagos]);
    }

    public function search_factura(Request $request)
    {
        $exp = explode('-', $request->serie_numero);
        $pago = Pago::with('alumnos.persona', 'persona')
            ->where([['tipo_doc', '01'], ['serie', $serie = $exp[0]], ['numero_doc', $numero = $exp[1]], ['fe_estado', '<>', 'Anulado']])
            ->first();
        return response()->json($pago, 200);
    }

    public function anular_factura(Request $request)
    {
        $final_result = [];
        $see = $this->getGreenter();

        $sequence = Sequence::where('doc_type', '50')->first();

        $details = [];
        for ($i = 0; $i < count($request->pago_id); $i++) {
            $pago = Pago::find($request->pago_id[$i]);
            $detail = new VoidedDetail();
            $detail->setTipoDoc('01') // Factura
            ->setSerie($pago->serie)
                ->setCorrelativo($pago->numero_doc)
                ->setDesMotivoBaja('ERROR EN MONTOS CONSIGNADOS');
            array_push($details, $detail);
        }
        $address = new Address();
        $address->setUbigueo('060101')
            ->setDepartamento('CAJAMARCA')
            ->setProvincia('CAJAMARCA')
            ->setDistrito('CAJAMARCA')
            ->setUrbanizacion('-')
            ->setDireccion('EL INCA 524')
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 de lo contrario.

        $company = new Company();
        $company->setRuc('20607116114')
            ->setRazonSocial('INSTITUCION EDUCATIVA PARTICULAR BEATO JUAN PABLO II S.R.L')
            ->setNombreComercial('COLEGIO JUAN PABLO II')
            ->setAddress($address);
        $pago_fech = Pago::find($request->pago_id[0])->updated_at;
        $cDeBaja = new Voided();
        $cDeBaja->setCorrelativo(str_pad($sequence->current + 1, 4, "0", STR_PAD_LEFT)) // Correlativo, necesario para diferenciar c. de baja de en un mismo día.
        ->setFecGeneracion(new \DateTime(date('Y-m-d H:i:s', strtotime($pago_fech)))) // Fecha de emisión de los comprobantes a dar de baja
        ->setFecComunicacion(new \DateTime(date('Y-m-d H:i:s'))) // Fecha de envio de la C. de baja
        ->setCompany($company)
            ->setDetails($details);

        $result = $see->send($cDeBaja);  // Guardar XML


        Storage::disk('local')->put('fe_sunat/xml/' . $cDeBaja->getName() . '.xml', $see->getFactory()->getLastXml());

        if (!$result->isSuccess()) {
            // Si hubo error al conectarse al servicio de SUNAT.
            $final_result['error'] = var_dump($result->getError());
        } else {
            for ($i = 0; $i < count($request->pago_id); $i++) {
                Pago::find($request->pago_id[$i])->update([
                    'fe_estado' => 'EnRevision',
                    'fe_ticket' => $result->getTicket(),
                    'fe_cdr_name' => $cDeBaja->getName()
                ]);
                Alumnopago::where('pago_id', $pago->id)->update([
                    'estado' => 'EnRevision'
                ]);
                Sequence::where([['doc_type', '50'], ['status', 1]])->first()->increment('current', 1);
            }
            return response()->json('success', 200);
        }

    }

    public function consult_ticket($pago)
    {
        $pag = Pago::find($pago);
        $see = $this->getGreenter();

        $statusResult = $see->getStatus($pag->fe_ticket);

        if (!$statusResult->isSuccess()) {
            // Si hubo error al conectarse al servicio de SUNAT.
//            $final_result['status'] = $statusResult->getError();
            return $statusResult->getError();
        }

        Storage::disk('local')->put('fe_sunat/cdr/' . 'R-' . $pag->fe_cdr_name . '.zip', $statusResult->getCdrZip());

        $cdr = $statusResult->getCdrResponse();

        $code = (int)$cdr->getCode();

        $final_result['code'] = $code;

        if ($code === 0) {
            $final_result['status'] = 'accepted';
            if (count($cdr->getNotes()) > 0) {
                $final_result['observations'] = $cdr->getNotes();
            }
            DB::beginTransaction();
            $pag->updated([
                'fe_estado' => 'Anulado',
            ]);
            Alumnopago::where('pago_id', $pag->id)->updated([
                'estado' => 'Anulado'
            ]);

        } else if ($code >= 2000 && $code <= 3999) {
            $final_result['status'] = 'denied';
        } else {
            $final_result['status'] = 'exception';
        }

        $final_result['description'] = $cdr->getDescription();

        return response()->json(['descripcion' => $final_result], 200);
    }

    public function searchBoleta(Request $request)
    {
        $exp = explode('-', $request->serie_numero);
        $pago = Pago::with('alumnos.persona', 'persona')
            ->where([['tipo_doc', '03'], ['serie', $serie = $exp[0]], ['numero_doc', $numero = $exp[1]], ['fe_estado', '<>', 'Anulado']])
            ->first();
        return response()->json($pago, 200);
    }

    public function anularBoleta(Request $request)
    {
        $final_result = [];
        $see = $this->getGreenter();

        $address = new Address();
        $address->setUbigueo('060101')
            ->setDepartamento('CAJAMARCA')
            ->setProvincia('CAJAMARCA')
            ->setDistrito('CAJAMARCA')
            ->setUrbanizacion('-')
            ->setDireccion('EL INCA 524')
            ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 de lo contrario.

        $company = new Company();
        $company->setRuc('20607116114')
            ->setRazonSocial('INSTITUCION EDUCATIVA PARTICULAR BEATO JUAN PABLO II S.R.L')
            ->setNombreComercial('COLEGIO JUAN PABLO II')
            ->setAddress($address);
        $details = [];
        for ($i = 0; $i < count($request->pago_id); $i++) {
            $pago = Pago::find($request->pago_id[$i]);

            $detail = new SummaryDetail();
            $detail->setTipoDoc('03') // Boleta
            ->setSerieNro($pago->serie . '-' . $pago->numero_doc)
                ->setEstado('3') // Anulación
                ->setClienteTipo('1')
                ->setClienteNro($pago->persona->DNI)
                ->setTotal($pago->fe_total_price)
                ->setMtoOperGravadas(0)
                ->setMtoOperInafectas($pago->fe_total_price)
                ->setMtoOperExoneradas(0)
                ->setMtoOtrosCargos(0)
                ->setMtoIGV(0);
            array_push($details, $detail);
        }

//        dd($details);
        $pago_fech = Pago::find($request->pago_id[0])->updated_at;
        $sequence = Sequence::where('doc_type', '50')->first();
        $resumen = new Summary();
        $resumen->setFecGeneracion(new \DateTime(date('Y-m-d', strtotime($pago_fech)))) // Fecha de emisión de las boletas.
        ->setFecResumen(new \DateTime(date('Y-m-d'))) // Fecha de envío del resumen diario.
        ->setCorrelativo(str_pad($sequence->current + 1, 4, "0", STR_PAD_LEFT)) // Correlativo, necesario para diferenciar de otros Resumen diario del mismo día.
        ->setCompany($company)
            ->setDetails($details);
        $result = $see->send($resumen);

        Storage::disk('local')->put('fe_sunat/xml/' . $resumen->getName() . '.xml', $see->getFactory()->getLastXml());

        if (!$result->isSuccess()) {
            // Si hubo error al conectarse al servicio de SUNAT.
            $final_result['error'] = var_dump($result->getError());
        } else {
            for ($i = 0; $i < count($request->pago_id); $i++) {
                Pago::find($request->pago_id[$i])->update([
                    'fe_estado' => 'EnRevision',
                    'fe_ticket' => $result->getTicket(),
                    'fe_cdr_name' => $resumen->getName()
                ]);
                Sequence::where([['doc_type', '50'], ['status', 1]])->first()->increment('current', 1);
            }
            return back();
//            return response()->json('success', 200);
        }
    }

    public function RegistrarPago(Request $request)
    {
//        return $request;
        $alumno = Alumno::find($request->alumnito_id ?? $request->alumno_id);
        $cronograma = Cronograma::with('concepto')->whereHas('concepto', function ($q) use ($alumno) {
            $q->where('carrera_id', $alumno->alumnocarreras->last()->carrera_id);
        })->first();
        DB::beginTransaction();
        if (!$request->papa_id)
            $persona = Auth::user()->persona;
        else {
            $persona = Padre::find($request->papa_id)->persona;
        }
        $pago = new Pago();
        $pago->persona_id = $persona->id;
        if ($request->num_voucher)
            $pago->num_voucher = $request->num_voucher;
        else
            $pago->num_voucher = null;
        if ($request->file('file')) {
            $UrlVoucherName = $request->num_voucher . '.' . $request->file('file')->getClientOriginalExtension();
            $request->file('file')->storeAs('public/VouchersPagos/' . $alumno->persona->DNI . '/', $UrlVoucherName);
            $patch = Storage::url('VouchersPagos/' . $alumno->persona->DNI . '/' . $UrlVoucherName);
            $pago->url_voucher = $patch;
        } else
            $pago->url_voucher = null;
        $pago->fe_sub_total = 0;
        $pago->fe_total_price = 0;
        $pago->fe_estado = 'Pago Deposito';
        $pago->save();

        $alumnoPago = new Alumnopago();
        $alumnoPago->pago_id = $pago->id;
        $alumnoPago->cronograma_id = $cronograma->id;
        $alumnoPago->alumno_id = $alumno->id;
        $alumnoPago->cantidad = 1;
        $alumnoPago->fe_descripcion = 'PAGO DE MATRICULA DEL ALUMNO ' . strtoupper($alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ,' . $alumno->persona->nombres);
        $alumnoPago->fe_unidad = '';
        $alumnoPago->fe_precio_und = 0;
        $alumnoPago->fe_igv = 0;
        $alumnoPago->fe_valor_venta = 0;
        $alumnoPago->estado = 'Pagado';
        $alumnoPago->save();
        DB::commit();

        return response()->json('success', 200);
    }

    public function get_vouchr($id_alumno)
    {
        $alumno = Alumno::where('id', $id_alumno)->get();

        $persona_id = $alumno->first()->persona_id;
        $alumnoPagos = Alumnopago::where('alumno_id', $id_alumno)->get();

        $pagos = Pago::where('id', $alumnoPagos->first()->pago_id)->get();
        $urlimg = $pagos->first()->url_voucher;
        //return $urlimg;
        $file = public_path($urlimg);
//.pathinfo(public_path($urlimg), PATHINFO_EXTENSION)
//       dd($file);
        //return $file;
        return response()->download($file, 'Vouchers' . substr($urlimg, 23, 8) . '.' . pathinfo(public_path($urlimg), PATHINFO_EXTENSION));
        //return $pagos->first()->url_voucher;

    }


    public function repotBank()
    {
        return Excel::download(new ReportBank, 'rptBanco_' . now()->format('Y-M-d') . '.xlsx');
    }
    public function reportDeudasExcel()
    {
        return Excel::download(new ReporteDeudas, 'rptDeudas_' . now()->format('Y-M-d') . '.xlsx');
    }
    public function reportDeudasCarrerasExcel($periodo_id, $carrera_id)
    {
        return Excel::download(new ReportDeudasCarreras($periodo_id,$carrera_id), ('rptDeudas_'.($carrera_id==1?'Inicial_':($carrera_id==2?'Primaria_':'Secundaria_')) . now()->format('Y-M-d') . '.xlsx'));
    }


    public function reportDeudasPdf($periodId = null)
    {
        $datos=collect();

        $alumnos = Alumno::with('persona')->whereHas('matricula.periodo', function ($q) use ($periodId) {
            $q->where('id', $periodId);
        })->get();

        foreach ($alumnos as $alumno) {
            $alumnoPago = count($alumno->alumnoPago);

            $dato = [
                'alumno' => $alumno->persona->DNI . ' - ' . $alumno->persona->paterno . ' ' . $alumno->persona->materno . ', ' . $alumno->persona->nombres,
                'nivel' => $alumno->alumnocarreras->last()->carrera->nombre,
//                'grado_id'=>Grade::where([['carrera_id',$alumno->alumnocarreras->last()->carrera_id],['grade',$alumno->alumnocarreras->last()->ciclo]])->first()->id,
                'grado' => $alumno->alumnocarreras->last()->ciclo,
                'grupo' => $alumno->alumnocarreras->last()->grupo,
                'matr' => $alumnoPago >= 1 ? '0' : $alumno->categoriaActiva->last()->monto,
                'mar' => $alumnoPago >= 2 ? '0' : $alumno->categoriaActiva->last()->monto,
                'abr' => $alumnoPago >= 3 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'may' => $alumnoPago >= 4 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jun' => $alumnoPago >= 5 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'jul' => $alumnoPago >= 6 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'ago' => $alumnoPago >= 7 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'sep' => $alumnoPago >= 8 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'oct' => $alumnoPago >= 9 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'nov' => $alumnoPago >= 10 != null ? '0' : $alumno->categoriaActiva->last()->monto,
                'dic' => $alumnoPago >= 11 != null ? '0' : $alumno->categoriaActiva->last()->monto
            ];
            $datos->push($dato);
        }
        $data=$datos->groupBy(['nivel','grado','grupo'])->sortBy('nivel');
////        dd($datos);
//
//        return view('Reports.reportDeudasPdf',['data'=>$datos]);
//        $data = $this->getDataSilaboPdf($request);
//
        $dat=[
            'data'=>$data
        ];
//         return view('Reports.reportDeudasPdf', $dat);
//        $pdf = MPDF::loadView('Reports.reportDeudasPdf', $dat);
//       return $pdf->stream('document.pdf');

        $pdf = MPDF::loadView('Reports.reportDeudasPdf', $dat, [],
            [
                'title' => 'rtpDeudas',
                'format' => 'A4-L',
                'orientation' => 'L'
            ]);
        return $pdf->stream();
    }
}
