<?php

namespace App\Http\Controllers;

use App\Areas_academica;
use App\Carrera;
use App\Docente;
use App\Periodo;
use App\Persona;
use App\Seccion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //paginate: paginar
        //$carreras = Carrera::where('estado','=','1')->get();
        //$responsables = Responsable::where('estado','=','1')->get();
        //$datos['docentes'] = Docente :: paginate(5);
        $datos['docentes'] = Docente::where('estado','=','1')->get();
        return view('Docente.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Docente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$DatosDocente = request() -> all();

        $DatosDocente = request() -> except('_token');

        Docente::insert($DatosDocente);

        //return response() -> json($DatosDocente);
        return redirect('docentes') -> with('Mensaje','Empleado registrado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Docente  $docentes
     * @return \Illuminate\Http\Response
     */
    public function show(Docente $docentes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Docente  $docentes
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
        $docente = Docente::findOrFail($id);
        return view('Docente.edit', compact('docente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Docente  $docentes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //$DatosDocente = request() -> except('_token','_method');
        //Docente::where('id', '=' ,$id) -> update ($DatosDocente);

        //$docente = Docente::findOrFail($id);
        //return view('docentes.edit', compact('docente'));
        //return redirect('docentes') -> with('Mensaje','Empleado modificado exitosamente!');
        $docente = Docente::find($id);

        $docente->nombres = $request->nombresEdit;
        $docente->paterno = $request->PaternoEdit;
        $docente->materno = $request->MaternoEdit;
        $docente->telefono = $request->telefonoEdit;
        //$alumno->fecha_nacimiento = $request->fecha_nacimientoEdit;
        //$alumno->sexo = $request->sexoEdit;
        //$alumno->procedencia = $request->procedenciaEdit;
        $docente->email = $request->emailEdit;
        //$alumno->usuario = $request->usuarioEdit;
        //$alumno->password = $request->passwordEdit;
        $docente->dni = $request->DNIEdit;
        $docente->save();

        return redirect()->route('docentes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Docente  $docentes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Docente::destroy($id);
        return redirect('docentes') -> with('Mensaje','Empleado eliminado exitosamente!');

    }
    public function delete($id){

        $docente = Docente::find($id);

        if($docente != null){
            $docente->estado = 0;
            $docente->save();
        }

        return redirect()->route('docentes.index');
    }
    public function getDocente(Request $request){

        $docente = Docente::find($request->id);

        return response()->json(['docente'=> $docente], 200);
    }


    public function carrerasDocentePorPeriodo(Request $request){

        $periodo_id=$request->id;
        $docente_id=Auth::user()->persona->docente->id;

        return Seccion::with(['periodo','pe_curso.curso.carrera'])

                ->whereHas("periodo", function($query) use ($periodo_id) {
                    $query->where('periodo_id',$periodo_id);
                })
                // ->where(function ($query) {
                //     $query->where('docente_id',1);
                // })
                ->whereHas('docentes',function ($query) use ($docente_id) {
                    $query->where('docente_id',$docente_id);
                })
                ->get()
                ->pluck('pe_curso.curso.carrera')
                ->unique();
    }

    public function areasDocentePorCarrera(Request $request){

        $carrera_id=$request->id;
        $docente_id=Auth::user()->persona->docente->id;

        return Seccion::with(['pe_curso.curso'])

                ->whereHas("pe_curso.curso", function($query) use ($carrera_id) {
                    $query->where('carrera_id',$carrera_id);
                })
                // ->where(function ($query) {
                //     $query->where('docente_id',1);
                // })
                ->whereHas('docentes',function ($query) use ($docente_id) {
                    $query->where('docente_id',$docente_id);
                })
                ->get()
                ->pluck('pe_curso')
                ->unique();



    }

    public function seccionesDocentePorArea(Request $request){

        $pe_curso_id=$request->id;
        $docente_id=Auth::user()->persona->docente->id;

        // return Seccion::where('pe_curso_id',$pe_curso_id)->where('docente_id',1)->get();

        return Seccion::whereHas('docentes',function ($query) use ($docente_id) {
                            $query->where('docente_id',$docente_id);
                        })
                        ->where(function ($query) use ($pe_curso_id) {
                            $query->where('pe_curso_id',$pe_curso_id);
                        })
                        ->get();


    }
    public function mis_areas(){
        $docente_id=Auth::user()->persona->docente->id;
        $carreras=Carrera::whereHas('pe_carrera.pe_curso.secciones.docente_seccion',function ($q) use($docente_id){
            $q->where('docente_id',$docente_id);
        })->get();

        return view('IntranetDocente.Areas.index',['carreras'=>$carreras]);
    }

    public function mis_horarios()
    {
        $periodos=Periodo::all();
        $docente_id=Auth::user()->persona->docente->id;
        $carreras=Carrera::whereHas('pe_carrera.pe_curso.secciones.docente_seccion',function ($q) use($docente_id){
            $q->where('docente_id',$docente_id);
        })->get();

        return view('IntranetDocente.Horario.index',['periodos'=>$periodos,'carreras'=>$carreras]);
    }

    public function get_horarios(Request $request)
    {
        $carrera_id=$request->carrera_id;
        $docente_id=Auth::user()->persona->docente->id;
        $periodo=Periodo::find($request->periodo_id);


        return Seccion::with(['pe_curso.curso','horarios'])

            ->whereHas("pe_curso.curso", function($query) use ($carrera_id) {
                $query->where('carrera_id',$carrera_id);
            })
            ->whereHas('docentes',function ($query) use ($docente_id) {
                $query->where('docente_id',$docente_id);
            })->where('periodo_id',$periodo->id)
            ->get();
//            ->pluck('pe_curso')
//            ->unique();

    }

    public function jefesAreas()
    {
        $personas = Persona::whereHas('usuario', function ($q) {
            $q->where([['tipo', '=', 'docente'], ['estado', '1']]);
        })->get()->sortDesc();

        $areaAcademica = Areas_academica::all();
        $carreras = Carrera::all();
        $JefeCarrera = Persona::with('docente.carreras')->whereHas('usuario', function ($q) {
            $q->where('tipo', '=', 'docente');
        })->whereHas('docente.carreras')->get();
//        dd($JefeCarrera);
        return view('Admin.admiJefeAreaAcademica.jefeAreaAcademica', ['carreras' => $carreras, 'personas' => $personas]);
    }

    public function buscarDocente($id){
        $docente = Docente::with('persona')->where('persona_id', $id)->get();
        $carreras = Carrera::whereHas('responsablesCarrera', function ($q) use ($id) {
            $q->where('persona_id', $id);
        })->get();
        return response()->json(['docente' => $docente, 'carreras' => $carreras], 200);
    }

    public function asignarJefesAreas(Request $request)
    {
        $docentes =  Docente::find($request->docente_id);
        $carreras = Carrera::all()->last();
        $numCarreras= $carreras->id;
        dd($docentes,$numCarreras);
        return $this->jefesAreas();
    }

    public function carrerasPorPeriodo(Request $request){

        $periodo_id=$request->id;

        return $request;

//        return Seccion::with(['periodo','pe_curso.curso.carrera'])
//
//            ->whereHas("periodo", function($query) use ($periodo_id) {
//                $query->where('periodo_id',$periodo_id);
//            })
//            ->get()
//            ->pluck('pe_curso.curso.carrera')
//            ->unique();
    }

    public function areasPorCarrera(Request $request){

        $carrera_id=$request->id;

        return Seccion::with(['pe_curso.curso'])

            ->whereHas("pe_curso.curso", function($query) use ($carrera_id) {
                $query->where('carrera_id',$carrera_id);
            })
            ->get()
            ->pluck('pe_curso')
            ->unique();

    }

    public function seccionesPorArea(Request $request){

        $pe_curso_id=$request->id;

         return Seccion::where('pe_curso_id',$pe_curso_id)->get();
    }
}
