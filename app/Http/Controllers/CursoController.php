<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Curso;
use App\Carrera;

class CursoController extends Controller
{

    public function index(){

        $cursos = Curso::where('estado','=','1')->get();
        $carreras = Carrera::where('estado','=','1')->get();

        return view('SecretariaAcademica.Cursos.index',[
            'cursos' => $cursos,
            'carreras' => $carreras
        ]);
    }

    public function store(Request $request){
//        $validar = $request->validate([
//            'nombre' => 'required|unique:cursos|max:250',
//            'descripcion' =>  'required|max:4000'
//
//        ]);

        $curso = new Curso([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'estado' => 1,
            'carrera_id'=>$request->carrera
        ]);

        $curso->save();

        return redirect()->route('cursos.index');

    }

    public function delete($id){

        $curso = Curso::find($id);

        if($curso != null){
            $curso->estado = 0;
            $curso->save();
        }

        return redirect()->route('cursos.index');
    }

    public function update(Request $request,$id){

        $curso = Curso::find($id);

        $curso->nombre = $request->nombreEdit;
        $curso->descripcion = $request->descripcionEdit;
        $curso->carrera_id=$request->carreraidEdit;
        $curso->save();

        return redirect()->route('cursos.index');
    }


    public function getCurso(Request $request){
        $curso = Curso::find($request->id);
        return response()->json(['curso'=> $curso], 200);
    }

    public function getcursosxcarrera(Request $request)
    {
        $cursos = Curso::with('carrera')->where('carrera_id', $request->id)->get();
        return response()->json(['cursos'=>$cursos],200);
    }
}
