<?php

namespace App\Http\Controllers;

use App\Modulo;
use App\Usuario;
use Illuminate\Http\Request;

class ModuloController extends Controller
{
    //
    public function index()
    {
        $usuarios = Usuario::where('estado', 1)->get();
        $modulos = Modulo::all();
//       dd($usuarios);
        return view('Admin.AdminUsuarios.adminUsuarios_modulo', ['usuarios' => $usuarios], ['modulos' => $modulos]);
    }

    public function buscarUsuario($id)
    {
        $usuario = Usuario::with('persona')->where('id', $id)->get();
        $modulos = Modulo::whereHas('usuarios', function ($q) use ($id) {
            $q->where('persona_id', $id);
        })->get();
        return response()->json(['usuario' => $usuario, 'modulos' => $modulos], 200);
    }

    public function updatemodulos(Request $request)
    {
        $usuario = Usuario::findOrFail($request->idUsuario);
        $modulos = Modulo::all()->last();

        for ($i = 1; $i <= $modulos->id; $i++) {
            $usuario->modulos()->detach($i);
        }
        if ($request->moduloid) {
            for ($i = 0; $i < count($request->moduloid); $i++) {
                $usuario->modulos()->attach($request->moduloid[$i]);
            }
        }

        return redirect()->route('adminUsuarios_modulo.index');
    }

}
