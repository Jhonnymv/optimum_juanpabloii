<?php

namespace App\Http\Controllers;

use App\Grade;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    public function getGradexcarrera($carrera_id)
    {
        $grades=Grade::where('carrera_id',$carrera_id)->get();
        return response()->json(['grades'=>$grades],200);
    }
}
