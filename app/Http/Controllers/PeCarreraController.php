<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carrera;
use App\PeCarrera;
use App\PlanEstudio;

class PeCarreraController extends Controller
{
    public function index()
    {
        $pecarreras = PeCarrera::where('estado', '=', '1')->get();
        $carreras = Carrera::where('estado', '=', '1')->get();
        $planestudios = PlanEstudio::where('estado', '=', '1')->get();

        return view('SecretariaAcademica.PeCarreras.index', [
            'pecarreras' => $pecarreras,
            'carreras' => $carreras,
            'planestudios' => $planestudios
        ]);
    }

    public function store(Request $request)
    {
        $validar = $request->validate([

        ]);

        $pecarrera = new PeCarrera([
            'plan_estudio_id' => $request->planestudio,
            'carrera_id' => $request->carrera,
            'estado' => 1
        ]);

        $pecarrera->save();

        if ($request->vista) {
            return redirect()->route('pecarrera.listxcarrera',['carrera_id'=>$request->carrera]);
        } else {
            return redirect()->route('pecarreras.index');
        }


    }

    public function delete($id)
    {

        // $pecarrera = PeCarrera::where('plan_estudio_id','=',$plan_estudio_id)
        //                    ->where('carrera_id','=',$carrera_id)->first();
        $pecarrera = PeCarrera::find($id);
        if ($pecarrera != null) {
            $pecarrera->estado = 0;
            //dd($pecarrera);
            $pecarrera->save();

        }

        return redirect()->route('pecarreras.index');
    }

    public function listxcarrera($carrera_id)
    {
//        dd($carrera_id);
        $pe_carreras = PeCarrera::where([['carrera_id', $carrera_id], ['estado', 1]])->get();
        $planestudios = PlanEstudio::where('estado', 1)->get();
        $carrera = Carrera::find($carrera_id);
//        dd($carrera);
        return view('SecretariaAcademica.PeCarreras.listxcarrera', [
            'carrera' => $carrera,
            'pe_carreras' => $pe_carreras,
            'planestudios' => $planestudios
        ]);
    }

}
