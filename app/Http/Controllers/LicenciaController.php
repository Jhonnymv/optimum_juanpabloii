<?php

namespace App\Http\Controllers;
use App\AlumnoCarrera;
use App\Carrera;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\PeCurso;
use App\Periodo;
use App\Prerrequisito;
use App\Licencia;
use PDF;
use Dompdf\Dompdf;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;




class LicenciaController extends Controller
{
    public function create()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $periodo_id=Periodo::where('estado',1)->first()->id;

        $matricula = Matricula::whereHas('AlumnoCarrera', function ($query) use ($alumno_id) {
            $query->where('alumno_id', $alumno_id);
        })
            ->where(function ($query) use ($periodo_id) {
                $query->where('periodo_id',$periodo_id);
            })
            ->first();
            //dd($matricula);
        if($matricula??'')
        $licencia = Licencia::where('matricula_id',$matricula->id);
        else
        $licencia = [];
        //dd($licencia);
        //if (!$matricula){
        return view('IntranetAlumno.matriculas.createlicencia', ['matricula'=>$matricula,'licencia'=>$licencia]);
        //}else{
        //    return view('IntranetAlumno.matriculas.createlicencia', ['matricula'=>$matricula,'licencia'=>$licencia]);
        //}


    }

    public function store(Request $request)
    {
//        try {
            $alumno = Auth::user()->persona->alumno;
            $alumno_carrera = AlumnoCarrera::where([['alumno_id', $alumno->id], ['carrera_id', $request->carrera_id]])->first();

            $periodo = Periodo::where('estado', 1)->first();

            DB::beginTransaction();
            $matricula = new Matricula();
            $matricula->alumno_carrera_id = $alumno_carrera->id;
            $matricula->periodo_id = $periodo->id;
            $matricula->estado = 'PRE-MATRICULA';
            $matricula->save();
            $matricula_id = $matricula->id;

            for ($i = 0; $i < count($request->seccion_id); $i++) {
//                if ($request->seccion_id[$i]) {
                    DB::table('matricula_detalles')->insert([
                        'matricula_id' => $matricula_id,
                        'seccion_id' => $request->seccion_id[$i],
                        'tipo'=>$request->tipo[$i]
                    ]);
//                }
                DB::table('secciones')->where('id',$request->seccion_id[$i])->decrement('cupos',1);
            }
            DB::commit();
            return back();
//        } catch (QueryException $e) {
//            dd('error');
//        }
    }

    public function reporte()
    {
         $periodosAcademicos=Periodo::where('estado',1)->get();
         $carreras=Carrera::where('estado',1)->get();
         $c_prer = (Prerrequisito::pluck('pe_curso_id'))->toArray();

         // $pe_cursos = PeCurso::where([['estado', 1], ['carrera_id', 1]])->get();

         $carrera_id=1;


         $pe_cursos = PeCurso::whereHas("curso", function($query) use ($carrera_id) {
                         $query->where('carrera_id',$carrera_id);
                     })
                     ->where(function ($query) {
                         $query->where('estado',1);
                     })
                     ->get();


         $pe_cursos_sr = $pe_cursos->except($c_prer);
         $pe_cursos_cr = $pe_cursos->only($c_prer);
         return view('IntranetAlumno.matriculas.ReporteMatricula',[
             //    'silabos' => $silabos,
                 'carreras' => $carreras,
                 'periodosAcademicos' => $periodosAcademicos
             ])
         ->with('pe_cursos_sr', $pe_cursos_sr)
         ->with('pe_cursos_cr', $pe_cursos_cr);
         //return view('IntranetAlumno.matriculas.ReporteMatricula');

    }
    public function reporteb()
    {


        return view('IntranetAlumno.matriculas.ReporteMat');

        //return view('IntranetAlumno.matriculas.ReporteMatricula');
    }


    public function validarpago_lic()
    {

////        $carreras=Carrera::where('estado',1)->get();
        $periodo = Periodo::where('estado', 1)->first();
        $matriculas = Matricula::where([['estado', 'PAGADA'], ['periodo_id', $periodo->id]])->get();

        $licencias = Licencia::where([['estado', 'PAGADO']])->get();
        //dd($licencias);

        return view('AreaContabilidad.Matriculas.v_pagos_lic', [ 'licencias' => $licencias]);
    }

    public function actualizarestado(Request $request)
    {
        $licencia = Licencia::find($request->id);
        $licencia->estado = $request->estado;
        //$ldate = date('Y-m-d H:i:s');
        //$date = Carbon::now()->toDateTimeString();
        if ($request->observaciones){
            $licencia->observaciones=$request->observaciones;
        }
        if ($request->resolucion){
            $licencia->resolucion=$request->resolucion;
            $licencia->fecha_cierre=$request->fecha_cierre;
        }
        if ($request->monto){
            $licencia->monto=$request->monto;
        }
        $licencia->save();
        return response()->json('success', 200);
//        return response()->json($request, 200);
   }

//    public function listprematricula($carrera_id)
//    {
//        $carrera=Carrera::find($carrera_id);
//        $matriculas=$carrera->carreramatriculas()->where('estado',1)->get();
////        $matriculas=$matriculas->with('');
//        dd($matriculas);
//    }

    public function validar_sub_5(Request $request)
    {
        $periodo = Periodo::where('estado', 1)->first();
        $matriculas = Matricula::where([['estado', 'PAGADA'], ['periodo_id', $periodo->id]])->get();

        $licencias = Licencia::wherein('estado', ['ACTIVA', 'CONFIRMADA'])->get();
        //$licencias = DB::table('licencias')->where('estado', '=', 'ACTIVA')->get();
        //dd($licencias);

    return view('SecretariaAcademica.Matriculas.v_licencias', ['licencias' => $licencias]);
    }
    public function getxlicencia(Request $request, $licencia_id)
    {
//            return response()->json($request,200);
        //dd("hola");
        if ($licencia_id!=null) {
            $licencia = Licencia::where([['id', $licencia_id]])->first();
        } else {
            $licencia = Licencia::where([['id', $licencia_id]])->first();
        }
        //$urlimg = $voucher->url_path;
        return response()->json(['licencia' => $licencia]);
    }
}
