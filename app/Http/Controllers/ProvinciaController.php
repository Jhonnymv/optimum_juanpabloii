<?php

namespace App\Http\Controllers;

use App\Provincia;
use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    public function show($id_dep)
    {
        $provincias=Provincia::where('departamento_id', $id_dep)->get();
        return $provincias;
    }
}
