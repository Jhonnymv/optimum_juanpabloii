<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Documento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class DocumentoController extends Controller
{
    public function Store(Request $request)
    {
        $alumno=Alumno::find($request->alumno_id);
        $document=new Documento();
        $document->alumno_id=$alumno->id;
        $document->nombre='DOCUMENTO DE COMPROMISO'.strtoupper($alumno->persona->paterno.' '.$alumno->persona->materno.' ,'.$alumno->persona->nombres);
        $UrlName = 'Comp'.$alumno->persona->DNI.'.' . $request->file('file')->getClientOriginalExtension();
        $request->file('file')->storeAs('public/Compromisos/' . $alumno->persona->DNI . '/', $UrlName);
        $patch = Storage::url('Compromisos/' . $alumno->persona->DNI . '/' . $UrlName);
        $document->url= $patch;
        $document->save();
        return response()->json('success');
    }
    public function get_contrato($alumno_id)
    {
        //dd($alumno_id);
        $contrato = Documento::where('id',$alumno_id)->get();
        $urlimg = $contrato->first()->url;
        //return strlen($urlimg);
        $file= public_path($urlimg);

//
//        dd($file);
        return response()->download($file, 'Contratos'.substr($urlimg,21,8).'.'.pathinfo(public_path($urlimg), PATHINFO_EXTENSION));
        //return Response::download($urlimg);
    }

}
