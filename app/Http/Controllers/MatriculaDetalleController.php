<?php

namespace App\Http\Controllers;

use App\Alumnopago;
use App\Categoria;
use App\Concepto;
use App\Cronograma;
use App\Documento;
use App\Matricula;
use App\MatriculaDetalle;
use App\Periodo;
use App\AlumnoCarrera;
use App\Persona;
use App\Seccion;
use App\Pago;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class MatriculaDetalleController extends Controller
{

    public function getxmatricula(Request $request, $matricula_id)
    {
        $periodo_id = Periodo::where('estado', 1)->first()->id;
        $matr = Matricula::where([['alumno_carrera_id', $matricula_id], ['periodo_id', $periodo_id]])->first();
        $alumno = AlumnoCarrera::where('id', $matricula_id)->get();
        $alumno_pago = Alumnopago::with('cronograma')->whereHas('cronograma', function ($q) use ($periodo_id) {
            $q->where('periodo_id', $periodo_id);
        })->where('alumno_id', $alumno->first()->alumno_id)->get();
        //return $alumno_pago;
        $contrato = Documento::where('alumno_id', $alumno->first()->alumno_id)->get();

        if (count($contrato) > 0) {
            if ($matr) {
                $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
                    ->where('matricula_id', $matr->id)
                    ->get();
                return view('SecretariaAcademica.Matriculas.detallematricula', ['detalles' => $matricula_detalles, 'alumno' => $alumno, 'alumno_pago' => $alumno_pago, 'contrato' => $contrato]);
            } else
                return view('SecretariaAcademica.Matriculas.detallematricula', ['alumno' => $alumno, 'alumno_pago' => $alumno_pago, 'contrato' => $contrato]);
        }
        //else
        // $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])->get();
        //dd($matr->first()->id);

//        if(count($matr) == 0)
//        {
//            DB::beginTransaction();
//            $matricula = new Matricula();
//            $matricula->alumno_carrera_id = $matricula_id;
//            $matricula->periodo_id = $periodo->id;
//            $matricula->estado = 'POR-CONFIRMAR';
//            $matricula->categoria_id = '1';
//            $matricula->save();
//            $mat_id = $matricula->id;
//            $pago = 120;
//            $contador = 0;
//            $ciclo = $alumno->first()->ciclo;
//            $carrera = $alumno->first()->pe_carrera_id;
//            $secciones = Seccion::with(['pe_curso', 'periodo','pe_curso.curso'])
//                ->whereHas('pe_curso', function ($query) use ($carrera,$ciclo) {
//                    $query->where([['pe_carrera_id',$carrera],['semestre',$ciclo]]);
//                })
//                ->whereHas('periodo', function ($query) use ($periodo_id) {
//                    $query->whereId($periodo_id);
//                })
//                ->whereHas('pe_curso.curso', function ($query){
//                    $query->where('nombre','not like','TALLER%');
//                })->get();
//            //dd($secciones[2]->id);
//            for ($i = 0; $i < count($secciones); $i++)
//            {
//                DB::table('matricula_detalles')->insert([
//                    'matricula_id' => $mat_id,
//                    'seccion_id' => $secciones[$i]->id,
//                    'tipo' => 'R'
//                ]);
//            }
//            DB::commit();
//            $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
//                ->where('matricula_id', $mat_id)->get();
//        }
//        else {
//            $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
//                ->where('matricula_id', $matr->first()->id)->get();
//            }
//        $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
//               ->where('matricula_id', $matr->first()->id)->get();
        //dd($alumno);

        return view('SecretariaAcademica.Matriculas.detallematricula', ['alumno' => $alumno]);

    }

    public function getxmatriculan($matricula_id)
    {
        $detalles = MatriculaDetalle::with('matricula')->where('matricula_id', $matricula_id)->get();
        return \response()->json(['detalles' => $detalles], 200);
    }

    public function matriculasec(Request $request, $alumno_id)
    {
        $periodo = Periodo::where('estado', 1)->first();
        $periodo_id = $periodo->id;
        $concepto = Concepto::where([['periodo_id', $periodo_id], ['nombre', 'like', 'MATR%']])->get();
        $idpago = $concepto->first()->id;
        //return $idpago;
        $alumno = AlumnoCarrera::where('id', $alumno_id)->get();

        $id_alumno = $alumno->first()->alumno_id;
//        $pagos = Alumnopago::with('cronograma')->whereHas('cronograma', function ($query) use ($idpago) {
//            $query->where('concepto_id', $idpago);
//        })->where('alumno_id', $id_alumno)->get();
        $pagos = Alumnopago::where('alumno_id', $id_alumno)->get();
        //return $pagos;
        if (count($pagos) > 0) {
            DB::beginTransaction();
            $matricula = new Matricula();
            $matricula->alumno_carrera_id = $alumno_id;
            $matricula->periodo_id = $periodo->id;
            $matricula->estado = 'CONFIRMADA';
            $matricula->categoria_id = '1';
            $matricula->save();
            $mat_id = $matricula->id;
            $pago = 120;
            $contador = 0;
            $ciclo = $alumno->first()->ciclo;
            $carrera = $alumno->first()->pe_carrera_id;
            $secciones = Seccion::with(['pe_curso', 'periodo', 'pe_curso.curso'])
                ->whereHas('pe_curso', function ($query) use ($carrera, $ciclo) {
                    $query->where([['pe_carrera_id', $carrera], ['semestre', $ciclo]]);
                })
                ->whereHas('periodo', function ($query) use ($periodo_id) {
                    $query->whereId($periodo_id);
                })
                ->whereHas('pe_curso.curso', function ($query) {
                    $query->where('nombre', 'not like', 'TALLER%');
                })->where('grupo', $alumno->first()->grupo)->get();
            //dd($secciones[2]->id);
            for ($i = 0; $i < count($secciones); $i++) {
                DB::table('matricula_detalles')->insert([
                    'matricula_id' => $mat_id,
                    'seccion_id' => $secciones[$i]->id,
                    'tipo' => 'R'
                ]);
            }
//            $categoria = Categoria::with('alumnos')->whereHas('alumnos', function ($query) use ($id_alumno) {
//                $query->where('alumno_id', $id_alumno);
//            })->where('periodo_id', $periodo_id)->get();
//            //return($categoria->first()->monto);
//            $cronogramas = Cronograma::with('concepto')->wherehas('concepto', function ($query) {
//                $query->where('nombre', 'like', 'PENS%');
//            })->where('periodo_id', $periodo_id)->get();
//            for ($i = 0; $i < count($cronogramas); $i++) {
//                $pago = new Pago();
//                $pago->persona_id = 1;
//                $pago->tipo_doc = '03';
//                $pago->serie = '0000';
//                $pago->numero_doc = '00000';
//                $pago->estado = 1;
//                $pago->save();
//                $pago_id = $pago->id;
//                DB::table('alumno_pago')->insert([
//                    'pago_id' => $pago_id,
//                    'cronograma_id' => $cronogramas[$i]->id,
//                    'alumno_id' => $id_alumno,
//                    'fe_unidad' => '1',
//                    'cantidad' => '1',
//                    'estado' => 'Pendiente',
//                    'fe_precio_und' => $categoria->first()->monto,
//                    'fe_valor_venta' => $categoria->first()->monto
//                ]);
//            }
            DB::commit();
            return response()->json('success', 200);
        } else
            return response()->json('error', 200);

    }

    public function getxmatricula_sub(Request $request, $matricula_id)
    {
        if ($request->wantsJson()) {
            $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
                ->where('matricula_id', $matricula_id)->get();
            return response()->json(['detalles' => $matricula_detalles], 200);
        }
        $matricula_detalles = MatriculaDetalle::where('matricula_id', $matricula_id)->get();
        return view('SecretariaAcademica.Matriculas.detallematricula_sub', ['detalles' => $matricula_detalles]);

    }

    public function removedetalle(Request $request)

    {
        DB::table('secciones')->where('id', $request->seccion_id)->increment('cupos', 1);
        DB::table('secciones')->where('id', $request->seccion_id)->decrement('n_matriculados', 1);
        $detalle = MatriculaDetalle::where('id', $request->id)->get();
        //return $detalle;
        $matricula_id = $detalle->first()->matricula_id;
        MatriculaDetalle::destroy($request->id);
        $detalles = MatriculaDetalle::where('matricula_id', $matricula_id)->get();
        if (count($detalles) == 0)
            Matricula::destroy($matricula_id);
        return response()->json(['success'], 200);
    }

    public function store(Request $request)
    {
        //return $request;
        $matriculadetalle = MatriculaDetalle::where([['matricula_id', $request->matricula_id], ['seccion_id', $request->seccion_id]])->get();
        //return count($matriculadetalle);
        if (count($matriculadetalle) > 0) {
            return response()->json('error', 200);
        } else {
            $matriculadetalle = new MatriculaDetalle();
            $matriculadetalle->seccion_id = $request->seccion_id;
            $matriculadetalle->matricula_id = $request->matricula_id;
            $matriculadetalle->tipo = $request->tipo;
            $matriculadetalle->save();
            //DB::table('secciones')->where('id', $request->seccion_id)->decrement('cupos', 1);
            //DB::table('secciones')->where('id', $request->seccion_id)->increment('n_matriculados', 1);
//            if ($request->accion) {
//                $matricula = Matricula::find($request->matricula_id);
//                $matricula->estado = 'POR-PAGAR-SUBSANACION';
//                $matricula->save();
//            }
            return response()->json('success', 200);
        }
    }

    public function get_listaAlumnosNota(Request $request)
    {

        //dd($request);
        //.alumnocarrera.alumno.persona
        //$listaalumnosnotas=MatriculaDetalle::with(['matricula.alumnocarrera.alumno.persona'])
        //->where('seccion_id', $request->id)->where('matricula.estado', 'CONFIRMADA')->get();
        //dd($criterios);
        $seccion_id = $request->id;


//        $listaalumnosnotas = MatriculaDetalle::with(['matricula.alumnocarrera.alumno.persona'])
//            ->where('seccion_id', $seccion_id)
//            ->whereHas("matricula", function ($query) use ($estado) {
//                $query->where('estado', $estado);
//            })
//            ->orderBy('matricula.alumnocarrera.alumno.persona.paterno','ASC')
////            ->orderBy('matricula.alumnocarrera.alumno.persona.materno','ASC')
//            ->get()
//            ->unique()//->orderBy('matricula.alumnocarrera.alumno.persona.paterno')
//        ;
//
        $listaalumnosnotas = Persona::with('alumno')
            ->whereHas('alumno.matricula.matriculadetalles', function ($q) use ($seccion_id) {
                $q->where('seccion_id', $seccion_id);
            })
            ->whereHas('alumno.alumnocarreras.matriculas', function ($q) {
                $q->where('estado', 'CONFIRMADA');
            })
            ->orderBy('paterno')
            ->orderBy('materno')
            ->get();


        $seccion = Seccion::with('pe_curso')->where('id', $seccion_id)
            ->first();
        $grado = $seccion->pe_curso->semestre;

        //$query->where('matricula.estado', 'CONFIRMADA');
        return response()->json(['listaalumnosnotas' => $listaalumnosnotas, 'grado' => $grado], 200);

    }

    public function calcular_pago(Request $request)

    {
//        $periodo = Periodo::where('estado', 1)->first();
//        $periodo_id = $periodo->id;
//        $alumno_id = $request->alumno_id;
//        $matricula_id = $request->matricula_id;
//        $categoria = Categoria::with('alumnos')->whereHas('alumnos', function ($query) use ($alumno_id) {
//            $query->where('alumno_id', $alumno_id);
//        })->where('periodo_id', $periodo_id)->get();
//        $monto_cat = $categoria->first()->monto;
//        $detalle_matricula = MatriculaDetalle::with('seccion.pe_curso.curso')->wherehas('seccion.pe_curso.curso', function ($query) {
//            $query->where('nombre', 'like', 'Taller%');
//        })->where('matricula_id', $matricula_id)->get();
//        //return count($detalle_matricula);
//        if (count($detalle_matricula) > 1) {
//            $alumno_pago = Alumnopago::with('cronograma')->whereHas('cronograma', function ($q) use ($periodo_id) {
//                $q->where('periodo_id', $periodo_id);
//            })->where('alumno_id', $alumno_id)->get();
//            for ($i = 0; $i < count($alumno_pago); $i++) {
//                DB::table('alumno_pago')->where([['cronograma_id', $alumno_pago[$i]->cronograma_id], ['estado', 'Pendiente']])->increment('fe_precio_und', 60);
//                DB::table('alumno_pago')->where([['cronograma_id', $alumno_pago[$i]->cronograma_id], ['estado', 'Pendiente']])->increment('fe_valor_venta', 60);
//            }
//            $monto = $monto_cat + 60;
//            DB::commit();
//        } else {
//            $alumno_pago = Alumnopago::with('cronograma')->whereHas('cronograma', function ($q) use ($periodo_id) {
//                $q->where('periodo_id', $periodo_id);
//            })->where('alumno_id', $alumno_id)->get();
//            for ($i = 0; $i < count($alumno_pago); $i++) {
//                //$alumno_pago[$i]->fe_precio_und = $monto_cat;
//                DB::table('alumno_pago')->where([['cronograma_id', $alumno_pago[$i]->cronograma_id], ['estado', 'Pendiente']])->update(['fe_precio_und' => $monto_cat]);
//                DB::table('alumno_pago')->where([['cronograma_id', $alumno_pago[$i]->cronograma_id], ['estado', 'Pendiente']])->update(['fe_valor_venta' => $monto_cat]);
//            }
//            $monto = $monto_cat;
//            DB::commit();
//        }
//
//        //$monto = count($detalle_matricula);
//        //return response()->json(['monto' => $monto], 200);
//        return response()->json('success', 200);
    }


}
