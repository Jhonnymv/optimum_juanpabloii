<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\Usuario;
use App\Persona;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public function username()
    {
        return 'usuario';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    // public function redirectTo(){

        // if(Auth::user()->tipo=='docente'){
        //     $this->redirectTo= route('IntranetDocente.silabos.create');
        //     return $this->redirectTo;
        // }

        // if(Auth::user()->tipo=='alumno'){
        //     $this->redirectTo= route('intranet_alumno');
        //     return $this->redirectTo;
        // }


    // }

    // protected function authenticated(Request $request, $user)
    // {

    //     if(Auth::user()->tipo=='docente'){
    //         return redirect()->route('IntranetDocente.silabos.create');
    //     }

    //     if(Auth::user()->tipo=='alumno'){
    //         return redirect()->route('Intranet_Alumno.index');
    //     }

    //     if(Auth::user()->tipo=='secretaria_academica'){
    //         return redirect()->route('secretaria.index');
    //     }

    //     if(Auth::user()->tipo=='jefe_area_academica'){
    //         return redirect()->route('IntranetAreaAcademica.silabos.index');
    //     }



    // }



    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleGoogleCallback()
        {
            try {

                $user = Socialite::driver('google')->user();
                $existingUser = Persona::where('email', $user->email)->first();
            Auth::loginUsingId($existingUser->id);
            $alumno = Auth::user();
            return view('IntranetAlumno.index', [ 'persona' => $alumno]);
            } catch (\Exception $e) {
                return back();
            }
            //dd($user);
            // only allow people with @company.com to login
            // if(explode("@", $user->email)[1] !== 'company.com'){
            //     return redirect()->to('/');
            // }
            // check if they're an existing user


            //dd($alumno);
//            //dd($existingUser);
//            if($existingUser){
//                dd("if");
//                 // log them in
//                 auth()->login($existingUser, true);
//
//                    Auth::loginUsingId($existingUser, true);
//
//                    return redirect()->to('/mantenimientodatos.index');
//
//            } else {
//
//            }

        }
}
