<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\Persona;
use Illuminate\Http\Request;
use App\Periodo;
use App\Horario;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\MatriculaDetalle;
use App\Asistencia;
use App\Seccion;
use PDF;
use Illuminate\Support\Facades\Auth;

// use Dompdf\Dompdf;

class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periodosAcademicos = Periodo::where('estado', 1)->get();
        return view('IntranetDocente.Asistencia.index')->with('periodosAcademicos', $periodosAcademicos);
    }

    public function getSesionClase(Request $request)
    {
        $horarios = Horario::where('seccion_id', $request->seccion_id)->get();
        $periodo = Periodo::find($request->periodo_id);
        $fecha_inicio = new Carbon($periodo->fecha_inicio);
        $fecha_fin = new Carbon($periodo->fecha_fin);
        $nroDias = $fecha_inicio->diffInDays($fecha_fin) + 1;
        $sissionClases = collect();

        foreach ($horarios as $horario) {
            $diaHorario = $horario->dia;
            $turno = $horario->turno;
//            $diaIngles = "";
            for ($j = 0; $j <= $nroDias; $j++) {
                $fechaIni = new Carbon($periodo->fecha_inicio);
                $fechaIni->addDays($j);
                if (Str::upper($fechaIni->dayName) == Str::upper($diaHorario)) {
                    $verificarSesion = Asistencia::where('horario_detalle_id', $horario->id)
                        ->where('seccion_id', $request->seccion_id)
                        ->where('fecha_sesion', $fechaIni->format('Y-m-d'))
                        ->get();

                    if (!$verificarSesion->isEmpty())
                        $estadoAsistencia = "true";
                    else
                        $estadoAsistencia = "false";

                    $sesion = [
                        'fecha' => $fechaIni = $fechaIni->format('Y-m-d'),
                        'dia' => $horario->dia,
                        'turno' => $turno,
                        'hora_inicio' => $horario->hora_inicio,
                        'hora_fin' => $horario->hora_fin,
                        'aula' => $horario->aula->aula,
                        'aula_id' => $horario->aula_id,
                        'dia_id' => $horario->dia_id,
                        'turno_id' => $horario->turno_id,
                        'horario_id' => $horario->id,
                        'estadoAsistencia' => $estadoAsistencia
                    ];
                    $sissionClases->push($sesion);

                }
            }
        }
        return response()->json($sissionClases->sortBy('fecha')->values()->all(), 200);
    }

    public function store(Request $request)
    {
        if ($request->wantsJson()) {
//            return $request;
            $seccion_id = $request->seccion_id;
            $horario_id = $request->horario_id;
            $fecha_sesion = $request->fecha_sesion;
            $carbon = new Carbon();
            $date = $carbon->now();

            $horario = Asistencia::where([['seccion_id', $request->seccion_id], ['horario_detalle_id', $request->horario_id], ['fecha_sesion', $request->fecha_seccion]])->first();
            if (!$horario) {
                DB::beginTransaction();
                for ($i = 0; $i < count($request->alumno_id); $i++) {
                    DB::table('asistencias')->insert([

                        'asistencia' => $request->asistencia[$i],
                        'alumno_id' => $request->alumno_id[$i],
                        'seccion_id' => $seccion_id,
                        'horario_detalle_id' => $horario_id,
                        'fecha_sesion' => $fecha_sesion,
                        'fecha_create' => $date->format('Y-m-d'),
                        'created_at' => $date
                        //'estado' => 1
                    ]);
                }
                DB::commit();
                return response()->json(['respuesta' => 'exito'], 200);
            } else
                return response()->json(['respuesta' => 'La asistencia ya existe'], 200);
        }
    }

    public function getRegistroAsistencia(Request $request)
    {
        $registroAsistencia = Asistencia::with(['alumnos.persona'])
            ->where('seccion_id', $request->seccion_id)
            ->where('horario_detalle_id', $request->horario_id)
            ->where('fecha_sesion', $request->fecha_sesion)
            //->orderBy('alumnos.persona.paterno','asc')
            ->get();

        return response()->json(['registroAsistencia' => $registroAsistencia], 200);

    }

    public function delete(Request $request, $seccion_id, $horario_id)
    {

        if ($request->wantsJson()) {

            Asistencia::where('seccion_id', $seccion_id)
                ->where('horario_detalle_id', $horario_id)
                ->delete();

            return response()->json(['respuesta' => 'exito'], 200);
        }

    }

    public function reporteSesion(Request $request)
    {
        // dd($request);
        //   if($request->wantsJson()){

        $registroAsistencia = Asistencia::with(['alumnos.persona'])
            ->where('seccion_id', $request->seccion_id)
            ->where('horario_detalle_id', $request->horario_id)
            ->where('fecha_sesion', $request->fecha_sesion)
            //->firstOrFail();
            ->get();

        $curso = Seccion::with(['pe_curso.curso.carrera'])
            ->where('id', $request->seccion_id)->firstOrFail();

        $seccion_id = $request->seccion_id;

        $periodo = Periodo::with(['secciones'])
            ->whereHas('secciones', function ($query) use ($seccion_id) {
                $query->where('secciones.id', $seccion_id);
            })->firstOrFail();
        //->where('secciones.id',$request->seccion_id)->firstOrFail();

        $docente = Auth::user()->persona;
        $horario = Horario::find($request->horario_id);


        $data = ['registroAsistencia' => $registroAsistencia,
            'curso' => $curso,
            'docente' => $docente,
            'periodo' => $periodo,
            'horario' => $horario];
        ///  return response()->json(['data'=>$data]);
        $pdf = PDF::loadView('IntranetDocente.Asistencia.reporteAsistenciaSesion', $data);
        return $pdf->stream();


        //return response()->json(['registroAsistencia'=>$v],200);
        //  }

    }

    public function indexAsistenciaAlumno()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $periodo_id=Periodo::where('estado',1)->first()->id;

        $detalles=MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno',function ($q) use ($alumno_id){
            $q->where('id',$alumno_id);
        })->whereHas('matricula',function ($q) use($periodo_id){
            $q->where('periodo_id',$periodo_id);

        })->get();

//        dd($detalles);
        return view('IntranetAlumno.asistencias.index', ['detalles'=>$detalles]);
    }

    public function getAsistenciasAlumno($seccion)
    {
        $alumno_id = Auth::user()->persona->alumno->id;

        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion) {
            $q->where('seccion_id', $seccion);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();

        return response()->json(['registroAsistencia' => $registroAsistencia], 200);
    }

    public function reporteAsistenciasAlumno(Request $request)
    {

        $curso = Seccion::with(['pe_curso.curso.carrera'])
            ->where('id', $request->seccion_id)->firstOrFail();

        $seccion_id = $request->seccion_id;

        $periodo = Periodo::with(['secciones'])
            ->whereHas('secciones', function ($query) use ($seccion_id) {
                $query->where('secciones.id', $seccion_id);
            })->firstOrFail();
        //->where('secciones.id',$request->seccion_id)->firstOrFail();

        $alumno = Auth::user()->persona;
        $horario = Horario::find($request->horario_id);

        $alumno_id = Auth::user()->persona->alumno->id;

        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion_id) {
            $q->where('seccion_id', $seccion_id);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();


        $data = ['registroAsistencia' => $registroAsistencia,
            'curso' => $curso,
            'alumno' => $alumno,
            'periodo' => $periodo,
            'horario' => $horario];
        ///  return response()->json(['data'=>$data]);

//        dd($data);
        $pdf = PDF::loadView('IntranetAlumno.asistencias.reporteAsistenciasAlumno', $data);
        return $pdf->stream();


        //return response()->json(['registroAsistencia'=>$v],200);
        //  }

    }

    public function indexAsistenciaSecretaria()
    {
        $periodosAcademicos = Periodo::where('estado', 1)->get();
        return view('SecretariaAcademica.alumnos.asistencias_alumno', ['periodosAcademicos'=>$periodosAcademicos]);
    }

    public function getListaAsistencia(Request $request)
    {
        $seccion_id = $request->seccion_id;
        $estado = 'CONFIRMADA';
        $order = 'asc';
        $listaAlumnos = MatriculaDetalle::with(['matricula.alumnocarrera.alumno.persona'
            /*        => function ($q) use ($order) {
                  $q->orderBy('nombres', $order);
         } */])
            ->where('seccion_id', $seccion_id)
            ->whereHas("matricula", function ($query) use ($estado) {
                $query->where('estado', $estado);
            })->get()

            // ->orderBy('matricula.alumnocarrera.alumno.persona.paterno','asc')->get()
            ->unique()//->orderBy('matricula.alumnocarrera.alumno.persona.paterno')
        ;
        return response()->json(['listaAlumnos' => $listaAlumnos], 200);
    }

    public function getAsistenciasAlumnoSecretaria(Request $request)
    {
        $alumno_id = $request->alumno_id;
        $seccion = $request->seccion_id;

        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion) {
            $q->where('seccion_id', $seccion);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();

        return response()->json(['registroAsistencia' => $registroAsistencia], 200);
    }

    public function reporteAsistenciasAlumnoSecretaria(Request $request)
    {

        $curso = Seccion::with(['pe_curso.curso.carrera'])
            ->where('id', $request->seccion_id)->firstOrFail();

        $seccion_id = $request->seccion_id;

        $periodo = Periodo::with(['secciones'])
            ->whereHas('secciones', function ($query) use ($seccion_id) {
                $query->where('secciones.id', $seccion_id);
            })->firstOrFail();
        //->where('secciones.id',$request->seccion_id)->firstOrFail();

        $alumno_id = $request->alumno_id;
        $busc_alumno = Alumno::find($alumno_id);
        $alumno= Persona::find($busc_alumno->persona_id);

        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion_id) {
            $q->where('seccion_id', $seccion_id);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();


        $data = ['registroAsistencia' => $registroAsistencia,
            'curso' => $curso,
            'alumno' => $alumno,
            'periodo' => $periodo];
        ///  return response()->json(['data'=>$data]);

//        dd($data);
        $pdf = PDF::loadView('SecretariaAcademica.alumnos.reporte_asistencias', $data);
        return $pdf->stream();


        //return response()->json(['registroAsistencia'=>$v],200);
        //  }
    }

    public function getlistaAsistenciaSecretaria(Request $request)
    {
        $seccion_id = $request->seccion_id;
        $estado = 'CONFIRMADA';
        $order = 'asc';
        $listaAlumnos = MatriculaDetalle::with(['matricula.alumnocarrera.alumno.persona'
            /*        => function ($q) use ($order) {
                  $q->orderBy('nombres', $order);
         } */])
            ->where('seccion_id', $seccion_id)
            ->whereHas("matricula", function ($query) use ($estado) {
                $query->where('estado', $estado);
            })->get()

            // ->orderBy('matricula.alumnocarrera.alumno.persona.paterno','asc')->get()
            ->unique()//->orderBy('matricula.alumnocarrera.alumno.persona.paterno')
        ;
        return response()->json(['listaAlumnos' => $listaAlumnos], 200);
    }

    public function indexAsistenciaAlumnoPadre($id)
    {
        $alumno = Alumno::find($id);
        $alumno_id = $alumno->id;
        $periodo_id=Periodo::where('estado',1)->first()->id;

        $detalles=MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno',function ($q) use ($alumno_id){
            $q->where('id',$alumno_id);
        })->whereHas('matricula',function ($q) use($periodo_id){
            $q->where('periodo_id',$periodo_id);

        })->get();

//        dd($detalles);
        return view('IntranetPadre.AreaAcademicaHijo.verAsistencias', ['detalles'=>$detalles], ['alumno_id' => $alumno_id]);
    }

    public function getAsistenciasAlumnoPadre($seccion, $id)
    {
        $alumno_id = $id;

        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion) {
            $q->where('seccion_id', $seccion);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();

        return response()->json(['registroAsistencia' => $registroAsistencia], 200);
    }

    public function reporteAsistenciasAlumnoPadre(Request $request)
    {

        $curso = Seccion::with(['pe_curso.curso.carrera'])
            ->where('id', $request->seccion_id)->firstOrFail();

        $seccion_id = $request->seccion_id;

        $periodo = Periodo::with(['secciones'])
            ->whereHas('secciones', function ($query) use ($seccion_id) {
                $query->where('secciones.id', $seccion_id);
            })->firstOrFail();
        //->where('secciones.id',$request->seccion_id)->firstOrFail();

        $alumno_id = $request->alumno_id;
        $alumno = Alumno::with('persona')->where('id', $alumno_id)->first();
        $horario = Horario::find($request->horario_id);


        $registroAsistencia = Asistencia::with('horario')->whereHas('horario', function ($q) use ($seccion_id) {
            $q->where('seccion_id', $seccion_id);
        })->whereHas ('alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->get();


        $data = ['registroAsistencia' => $registroAsistencia,
            'curso' => $curso,
            'alumno' => $alumno,
            'periodo' => $periodo,
            'horario' => $horario];
        ///  return response()->json(['data'=>$data]);

//        dd($alumno);
        $pdf = PDF::loadView('IntranetPadre.AreaAcademicaHijo.rptAsistAlumnoPadre', $data);
        return $pdf->stream();


        //return response()->json(['registroAsistencia'=>$v],200);
        //  }

    }
}
