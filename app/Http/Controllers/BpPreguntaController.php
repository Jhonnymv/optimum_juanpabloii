<?php

namespace App\Http\Controllers;

use App\BpAlternativa;
use App\BpCategoria;
use App\BpPregunta;
use App\Postulacion;
use App\PostulacionAnexo;
use App\Postulante;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BpPreguntaController extends Controller
{
    public function index()
    {
        $preguntas = BpPregunta::all();
        $categorias = BpCategoria::all();

        return view('Admisiones.Banco_Preguntas.list_preguntas', ['preguntas' => $preguntas, 'categorias' => $categorias]);
    }

    public function create()
    {
        $bpCategorias = BpCategoria::all();
        return view('Admisiones.Banco_Preguntas.registrar_preguntas', ['bpCategorias' => $bpCategorias]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $pregunta = new BpPregunta();
        $pregunta->bp_categoria_id = $request->bp_categoria_id;
        if ($request->file('imagen')) {
            $im_name = date('dmYhis') . 'imagen.' . $request->file('imagen')->getClientOriginalExtension();
            $request->file('imagen')->storeAs('public/preguntas/', $im_name);
            $patch = Storage::url('preguntas/' . $im_name);
            $pregunta->url_imagen = $patch;
        }

        $pregunta->pregunta = $request->pregunta;
        $pregunta->save();


        foreach ($request->alternativas as $alternativa) {
            $alter = new BpAlternativa();
            $alter->bp_pregunta_id = $pregunta->id;
            $alter->alternativa = $alternativa['alternativa'];
            if (array_key_exists('valor', $alternativa))
                $alter->valor = $alternativa['valor'];
            else
                $alter->valor = "0";
            $alter->save();
        }
        DB::commit();
        return back()->with('msj', 'guardado');
    }

    public function get_detalle(Request $request)
    {
        $pregunta = BpPregunta::with('bpAlternativas')->find($request->pregunta_id);
        return response()->json(['pregunta' => $pregunta], 200);
    }

    public function selectCorrecta(Request $request)
    {
        $alternativas = BpAlternativa::where('bp_pregunta_id', $request->preguntaId)->get();
        foreach ($alternativas as $alternativa) {
            $redefaul = BpAlternativa::find($alternativa->id);
            $redefaul->valor = "0";
            $redefaul->save();
        }
        $correcta = BpAlternativa::find($request->valor);
        $correcta->valor = "1";
        $correcta->save();
        return $this->index();
    }
}
