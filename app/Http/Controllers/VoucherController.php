<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Matricula;
use App\Voucher;
use App\Licencia;
use App\Postulacion;
use App\PostulacionAnexo;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller
{
    public function store(Request $request)
    {
        DB::beginTransaction();
        $extension = $request->file('img')->getClientOriginalExtension();
        $nombre_archivo = $request->matricula_id . '_' . $request->num_operacion . '.' . $extension;

        $voucher = new Voucher();
        $voucher->fecha = $request->fecha;
        $voucher->num_operacion = $request->num_operacion;
        $voucher->monto = $request->monto;
        if ($request->tipo){
            $voucher->tipo="subsanacion";
        }
        $voucher->urlimg = Auth::user()->id . '/' . $nombre_archivo;
        $voucher->matricula_id = $request->matricula_id;
        $voucher->save();

        $matricula = Matricula::find($request->matricula_id);
        $matricula->estado = "PAGADA";
        $matricula->observaciones=$request->observaciones;
        $matricula->save();
        DB::commit();
        $request->file('img')->storeAs('public/' . Auth::user()->id . '/', $nombre_archivo);
        return back();
    }
    public function store_lic(Request $request)
    {
        DB::beginTransaction();
        $extension = $request->file('img')->getClientOriginalExtension();
        $nombre_archivo = $request->matricula_id . '_' . $request->num_operacion . '.' . $extension;
        $licencia = new Licencia();
        $licencia->matricula_id = $request->matricula_id;
        $licencia->descripcion = $request->sumilla;
        $licencia->estado = "PAGADO";
        $licencia->fecha_registro = $request->fecha;
        $licencia->duracion=$request->sel_duracion;
        $licencia->save();


        $voucher = new Voucher();
        $voucher->fecha = $request->fecha;
        $voucher->num_operacion = $request->num_operacion;
        $voucher->monto = $request->monto;
        $voucher->urlimg = Auth::user()->id . '/' . $nombre_archivo;
        $voucher->licencia_id = $licencia->id;
        $voucher->matricula_id = $licencia->matricula_id;

        $voucher->save();
        //actualizar el estado de la matrícula a licencia
        // $matricula = Matricula::find($request->matricula_id);
        // $matricula->estado = "PAGADA";
        // $matricula->observaciones=$request->observaciones;
        // $matricula->save();
        DB::commit();
        $request->file('img')->storeAs('public/' . Auth::user()->id . '/', $nombre_archivo);
        return back();
    }

    public function getxmatricula(Request $request, $matricula_id=null)
    {

        if ($request->alumno_id!=null) {
            $voucher = Voucher::with('matricula')->where([['matricula_id', $request->alumno_id], ['tipo', 'boleta']])->first();
        } else {
            $vouchers = Voucher::with('matricula')->where([['matricula_id', $matricula_id], ['tipo', '<>', 'boleta']])->get();
            return response()->json(['voucher' => $vouchers], 200);
        }
        return response()->json($voucher);
//        $urlimg = $voucher->url_path;
//        return response()->json(['voucher' => $voucher, 'urlimg' => $urlimg], 200);
    }

    public function getxlicencia(Request $request, $licencia_id=null)
    {
//            return response()->json($request,200);
        //dd($licencia_id);
        if ($request->lic_id!=null) {
            $voucher = Voucher::with('licencia')->where([['licencia_id', $request->lic_id], ['tipo', 'boleta']])->first();
        } else {
            $voucher = Voucher::with('licencia')->where([['licencia_id', $licencia_id], ['tipo', 'voucher']])->first();
        }
        $urlimg = $voucher->url_path;
        //dd($urlimg);
        return response()->json(['voucher' => $voucher, 'urlimg' => $urlimg], 200);
    }
    public function getxpostulacion(Request $request, $postulacion_id=null)
    {
//            return response()->json($request,200);
        //dd($licencia_id);
        if ($request->pos_id!=null) {
            $voucher = Postulacion::with(['postulante.persona'])->where([['id', $request->pos_id]])->first();
        } else {
            $voucher = Postulacion::with(['postulante.persona'])->where([['id', $postulacion_id]])->first();
        }
        $anexo = PostulacionAnexo::where([['postulacion_id' , $postulacion_id],['nombre' , 'voucher']])->first();
        $urlimg = $voucher->url_path;
        //dd($urlimg);
        //return response()->json(['voucher' => $voucher, 'urlimg' => $urlimg], 200);
        return response()->json(['voucher' => $voucher, 'anexo' => $anexo, 'urlimg' => $urlimg], 200);
    }


    public function ver_voucher_matricula(Request $request, $matricula_id=null)
    {
        if ($request->mat_id!=null) {
            $voucher = Voucher::with('matricula')->where([['matricula_id', $request->mat_id], ['tipo', 'boleta']])->first();
        } else {
            $vouchers = Voucher::with('matricula')->where([['matricula_id', $matricula_id], ['tipo', '<>', 'boleta']])->get();
            return response()->json(['voucher' => $vouchers], 200);
        }
        $urlimg = $voucher->url_path;
        return response()->json(['voucher' => $voucher, 'urlimg' => $urlimg], 200);
    }



}
