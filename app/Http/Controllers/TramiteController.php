<?php

namespace App\Http\Controllers;

use App\Tramite;
use App\UnidadAdministrativa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TramiteController extends Controller
{
    public function getXUnidadAdministrativa(Request $request)
    {
        $tramites = Tramite::where('unidad_administrativa_id', $request->unid_administrativa)->get();
        return response()->json(['tramites' => $tramites], 200);
    }

    public function getTramitesxUnidad(Request $request, $postulante_id)
    {
        $tramites = Tramite::where('unidad_administrativa_id', $postulante_id)->get();
        $uAdministrativas = UnidadAdministrativa::all();
//        dd($tramites);
        return view('OptimunTramites.UnidadesAdministrativas.VerLisTramite', ['tramites' => $tramites], ['uAdministrativas' => $uAdministrativas]);
    }

    // nuevo tramite
    public function store(Request $request)
    {
//        dd($request);
        $tramite = new Tramite();
        $tramite->descripcion = $request->descripcion;
        $tramite->unidad_administrativa_id = $request->unidad_administrativa_id;

        $tramite->save();
        return redirect()->route('tramiteController.getTramitesxUnidad',['id'=>$request->unidad_administrativa_id]);
    }
    // Editar Tramite
    public function update(Request $request)
    {
        $tramite = Tramite::find($request->id_tramite);
//       dd($request);
        $tramite->descripcion = $request->descripcion_edit;
        $tramite->unidad_administrativa_id = $request->und_administrativa_id;
        $tramite->save();
        return redirect()->route('tramiteController.getTramitesxUnidad',['id'=>$request->und_administrativa_id]);
    }
    // Buscar Tramite

    public function getTramite($id) {
        $tramite = Tramite::find($id);
//        dd($id);
        return response()->json(['tramite'=> $tramite], 200);

    }
}
