<?php

namespace App\Http\Controllers;

use App\AlumnoCarrera;
use App\Http\Controllers\Controller;
use App\Periodo;
use App\Seccion;
use App\PeCarrera;
use App\PeCurso;
use App\Persona;
use App\Docente;
use App\Horario;
use App\Carrera;
use App\DocenteSeccion;
use App\MatriculaDetalle;
use http\Env\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeccionController extends Controller
{

    // use GoogleTrait;

    public function createGoogleClassroomCourseAndTopics(Request $request)
    {

        // return true;

        $gClient = $this->getGoogleClient('ver_cursos');


        // $this->processGoogleToken($request,$gClient);

        if ($request->get('code')) {


            $accessToken = $gClient->fetchAccessTokenWithAuthCode($request->get('code'));


            $request->session()->forget('token');
            // $request->session()->put('token', $gClient->getAccessToken());
            $request->session()->put('token', $accessToken);
            $gClient->setAccessToken($accessToken);

        } else {

            if ($request->session()->get('token')) {
                $gClient->setAccessToken($request->session()->get('token'));
                if ($gClient->isAccessTokenExpired()) {
                    if ($gClient->getRefreshToken()) {
                        $gClient->fetchAccessTokenWithRefreshToken($gClient->getRefreshToken());
                    } else {
                        $authUrl = $gClient->createAuthUrl();
                        return redirect()->to($authUrl);
                    }
                }
            } else {
                $authUrl = $gClient->createAuthUrl();
                // return $authUrl;
                return redirect()->to($authUrl);
            }
        }


        /////////////////////////////////////////////


        $serviceClassroom = new \Google_Service_Classroom($gClient);

        if ($gClient->getAccessToken()) {

            // return true;

            $seccion = Seccion::with(['pe_curso.curso.carrera',
                'periodo',
                'unidades.contenidos'
            ])
            ->whereId($request->id)->firstOrFail();

            $course = new \Google_Service_Classroom_Course(array(
                'name' => $seccion->pe_curso->curso->nombre . ' - ' . $seccion->seccion,
                'section' => 'A',
                'descriptionHeading' => 'Bienvenido a ' . $seccion->pe_curso->curso->nombre,
                'description' => '',
                'room' => '',
                'ownerId' => 'me',
                'courseState' => 'ACTIVE'
            ));

            $seccion->google_classroom_course_id = $course->id;
            $seccion->save();

            foreach ($seccion->unidades->reverse() as $key => $unidad) {
                foreach ($unidad->contenidos->reverse() as $key_ => $contenido) {
                    $topic = new \Google_Service_Classroom_Topic(array(
                        'name' => $unidad->denominacion . ' - ' . $contenido->contenido
                    ));
                    $topic = $serviceClassroom->courses_topics->create($seccion->google_classroom_course_id, $topic);
                }
            }


            return ['success' => true];

        }


        return ['success' => false];


    }

    public function ver_cursos(Request $request)
    {

        $gClient = $this->getGoogleClient('ver_cursos');


        // $this->processGoogleToken($request,$gClient);

        if ($request->get('code')) {


            $accessToken = $gClient->fetchAccessTokenWithAuthCode($request->get('code'));


            $request->session()->forget('token');
            // $request->session()->put('token', $gClient->getAccessToken());
            $request->session()->put('token', $accessToken);
            $gClient->setAccessToken($accessToken);

        } else {

            if ($request->session()->get('token')) {
                $gClient->setAccessToken($request->session()->get('token'));

                if ($gClient->isAccessTokenExpired()) {
                    if ($gClient->getRefreshToken()) {
                        $gClient->fetchAccessTokenWithRefreshToken($gClient->getRefreshToken());
                    } else {
                        $authUrl = $gClient->createAuthUrl();
                        return redirect()->to($authUrl);
                    }
                }
            } else {
                $authUrl = $gClient->createAuthUrl();
                // return $authUrl;
                return redirect()->to($authUrl);
            }
        }

        /////////////////////

        // $serviceClassroom = new \Google_Service_Classroom($gClient);
        $google_oauthV2 = new \Google_Service_Oauth2($gClient);


        if ($gClient->getAccessToken()) {


            // return true;

            // $course = new \Google_Service_Classroom_Course(array(
            //     'name' => 'Matematica II',
            //     'section' => 'A',
            //     'descriptionHeading' => 'Bienvenido',
            //     'description' => '',
            //     'room' => '100',
            //     'ownerId' => 'me',
            //     'courseState' => 'PROVISIONED'
            // ));
            // $course = $serviceClassroom->courses->create($course);

            // return $course->id;

            ////////////////////////////////////////////////////////////////////


            // $optParams = array(
            //     'pageSize' => 10
            // );
            // $results=$serviceClassroom->courses->listCourses($optParams);

            // return $results->getCourses();

            //////////////////////////////

            return json_encode($google_oauthV2->userinfo->get());
        }


    }

    public function processGoogleToken($request, $gClient)
    {
        if ($request->get('code')) {


            $accessToken = $gClient->fetchAccessTokenWithAuthCode($request->get('code'));


            $request->session()->forget('token');
            // $request->session()->put('token', $gClient->getAccessToken());
            $request->session()->put('token', $accessToken);
            $gClient->setAccessToken($accessToken);

        } else {

            if ($request->session()->get('token')) {
                $gClient->setAccessToken($request->session()->get('token'));

                if ($gClient->isAccessTokenExpired()) {
                    if ($gClient->getRefreshToken()) {
                        $gClient->fetchAccessTokenWithRefreshToken($gClient->getRefreshToken());
                    } else {
                        $authUrl = $gClient->createAuthUrl();
                        return redirect()->to($authUrl);
                    }
                }
            } else {
                $authUrl = $gClient->createAuthUrl();
                // return $authUrl;
                return redirect()->to($authUrl);
            }
        }

    }

    public function getGoogleClient($routeRedirect)
    {
        $google_redirect_url = route($routeRedirect).'?px=1';

        $gClient = new \Google_Client();
        $gClient->setApplicationName(config('services.google_classroom.app_name'));
        $gClient->setClientId('387015856411-sgopfrsjgq1ul6d9hu6sqsf307ghits3.apps.googleusercontent.com');
        $gClient->setClientSecret('AclCyAXr6JZ-6P4YgEYmM38r');
        $gClient->setRedirectUri($google_redirect_url);
        $gClient->setDeveloperKey(config('services.google_classroom.api_key'));
        $gClient->setScopes(array(
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/classroom.courses',
            'https://www.googleapis.com/auth/classroom.topics'
        ));
        return $gClient;
    }

    public function getseccXcur(Request $request)
    {
        $periodo_id = Periodo::where('estado', 1)->first()->id;
        $secciones = Seccion::with('pe_curso.curso', 'horarios.aula')->where([['pe_curso_id', $request->id], ['periodo_id', $periodo_id], ['grupo', $request->grupo]])->get();
        return response()->json(['secciones'=>$secciones],200);
    }

    public function getSeccionxCurso(Request $request)
    {
//        return $request;
        $periodo_id = Periodo::where('estado', 1)->first()->id;
//        if ($request->tipo == 'S') {
//            if ($request->semestre >= 1) {
//                $seccion = Seccion::with('pe_curso.curso', 'horarios.aula')->where([['periodo_id', $periodo_id], ['pe_curso_id', $request->id], ['grupo', 'sub']])->get();
//                if (count($seccion) > 0) {
//                    return response()->json(['secciones' => $seccion], 200);
//                } else {
//                    $seccion = new Seccion([
//                        'seccion' => 'Subsanación',
//                        'grupo' => 'sub',
//                        'cupos' => 40,
//                        'periodo_id' => $periodo_id,
//                        'pe_curso_id' => $request->id
//                    ]);
//                    $seccion->save();
//                    $seccion = Seccion::with('pe_curso.curso', 'horarios.aula')->where([['periodo_id', $periodo_id], ['pe_curso_id', $request->id], ['grupo', 'sub']])->get();
//                    return response()->json(['secciones' => $seccion], 200);
//                }
//            } else
//                $secciones = Seccion::with('pe_curso.curso', 'horarios.aula')->where([['pe_curso_id', $request->id], ['periodo_id', $periodo_id]])->get();
//        } else {
            //$alumno = Auth::user()->persona->alumno;
//            $alumnocarrera = AlumnoCarrera::where([['carrera_id', $request->carrera_id], ['alumno_id', $alumno->id]])->first();

            $secciones = Seccion::with('pe_curso.curso', 'horarios.aula')->where([['pe_curso_id', $request->id], ['periodo_id', $periodo_id]])->get();
       // $secciones = Seccion::with('pe_curso.curso', 'horarios.aula')->get();

        //}

        return response()->json(['secciones' => $secciones], 200);
//        return $request;
    }

    public function updateGoogleClassroomCourseId(Request $request){

        $seccion=Seccion::find($request->seccion_id);
        $seccion->google_classroom_course_id=$request->googleClassroomCourseId;
        $seccion->save();
        return ['success'=>true];
    }

    public function getFullSeccion(Request $request)
    {

        $seccion_id = $request->seccion_id;
        $docente_id = Auth::user()->persona->docente->id;

        $seccion = Seccion::with([
            'silabo.unidades.contenidos',
            'pe_curso.curso.carrera',
            'periodo',
            'horarios'
        ])
            ->whereId($seccion_id)->firstOrFail();


        $periodo_id = $seccion->periodo_id;

        $secciones_con_silabo = Seccion::with([
            'pe_curso.curso.carrera',
            'periodo',
            'silabo'
        ])
            ->where(function ($query) use ($periodo_id) {
                $query->where('periodo_id', $periodo_id);
            })
            ->where(function ($query) use ($seccion_id) {
                $query->where('id', '!=', $seccion_id);
            })
            ->whereHas('silabo')
            ->whereHas('docentes', function ($query) use ($docente_id) {
                $query->where('docente_id', $docente_id);
            })
            ->get();

        return ['seccion' => $seccion, 'secciones_con_silabo' => $secciones_con_silabo];


        // $silabos=silabo::where('seccion_id',$request->seccion_id)
        //                 ->get();

        // if($silabos->count() > 0){
        //     return ['seccionTieneSilabo'=>1,'silabo'=>$silabos->first()];
        // }

        // return ['seccionTieneSilabo'=>0,'silabo'=>null];
    }

    public function index()
    {
//        $secciones = Seccion::with(['pe_curso', 'pe_curso.curso', 'pe_curso.curso.carrera','docentes.persona', 'docente_seccion', 'docente_seccion.docente', 'docente_seccion.docente.persona'])->get();
        $secciones = Seccion::all();
//        $secciones = DocenteSeccion::with(['seccion','seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.pe_curso.curso.carrera','docente.persona'])->get();
        //$secciones = Seccion::with('pe_curso.pecarrera.carrera','docentes.persona','horarios','pe_curso.curso')->get();
        $pe_cursos = Pecurso::with('curso')->get();
        $carreras = Carrera::where('estado', 1)->get();
        $periodos = Periodo::where('estado', 1)->get();
        //$personas=Persona::with('docente')->get();
        //$docentes=Docente::where('estado',1)->get()->personas_id;
        $docentes = Docente::with('persona')->get();
        //$secciones = MatriculaDetalle::with(['pe_curso', 'pe_curso.curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
        //->where('matricula_id', $matricula_id)->get();
        //return response()->json(['detalles' => $matricula_detalles], 200);
        //}
        //dd($secciones->seccion);
        return view('JefeAreaAcademica.CargaHoraria.index', [
            'carreras' => $carreras,
            'periodos' => $periodos,
            'secciones' => $secciones,
            'docentes' => $docentes,
            'pe_cursos' => $pe_cursos

        ]);
    }

    public function getCargarPlanesCarrera(Request $request)
    {

        $carrera_id = $request->carrera_id;
        // dd($carrera_id);
        return PeCarrera::with(['planestudio'])
            ->where("carrera_id", $carrera_id)->get();


    }

    public function alumnos(Request $request){

        $seccion_id=$request->seccion_id;

        return MatriculaDetalle::with(['matricula.alumnocarrera.alumno.persona'])
                                ->whereHas('matricula',function($query){
                                    $query->where('estado','CONFIRMADA');
                                })
                                ->where('seccion_id',$request->seccion_id)
                                ->get();



    }

    public function seccionesRespAreaPorCarrera(Request $request)
    {
        $carrera_id = $request->carrera_id;
        $periodo_id = $request->periodo_id;
        return Seccion::with(['pe_curso.curso.carrera', 'silabo', 'periodo', 'docentes.persona'])
            ->whereHas('silabo', function ($q){
                $q->where('estado',2);
            })
            ->whereHas('docentes', function ($query) use ($carrera_id) {
                $query->where('rol', 'principal');
            })
            ->whereHas('pe_curso.curso.carrera', function ($query) use ($carrera_id) {
                $query->whereId($carrera_id);
            })
            ->whereHas('periodo', function ($query) use ($periodo_id) {
                $query->whereId($periodo_id);
            })
            ->get();
    }


    public function getcargarGrupos(Request $request)
    {

        $carrera_id = $request->carrera_id;
        return
            AlumnoCarrera::where('carrera_id', $carrera_id)->distinct()->get(['grupo']);
        //dd($criterios);
        //return response()->json(['grupos'=>$grupos],200);

    }

    public function getCursosPe(Request $request)
    {

        // $carrera_id=$request->carrera_id;
        $pecarrera_id = $request->pecarrera_id;

        $pe_cursos = PeCurso::with('curso')
            ->where('pe_carrera_id', $pecarrera_id)->get();

        return response()->json(['pe_cursos' => $pe_cursos], 200);


    }

    public function store(Request $request)
    {


        DB::beginTransaction();
        $seccion = new Seccion([
            'seccion' => $request->grupo_id,
            'grupo' => $request->grupo_id,
            'cupos' => $request->cupos,
            'periodo_id' => $request->sel_periodo2,
            'pe_curso_id' => $request->sel_curso
        ]);

        $seccion->save();
        $seccion_id = $seccion->id;


//                if ($request->seccion_id[$i]) {
        DB::table('docente_seccion')->insert([
            'docente_id' => $request->docente_id,
            'seccion_id' => $seccion_id,
            'rol' => "principal"
        ]);
//                }
        //DB::table('secciones')->where('id', $request->seccion_id[$i])->decrement('cupos', 1);

        DB::commit();


       //return response()->json(['Mensaje' => 'success'], 200);
        return back();

    }

    public function listxseccion($seccion_id)
    {
//        dd($carrera_id);
        $secciones = Seccion::with(['pe_curso', 'pe_curso.curso', 'pe_curso.curso.carrera', 'docente_seccion', 'docente_seccion.docente', 'docente_seccion.docente.persona'])->where([['id', $seccion_id]])->get();

        $aulas = DB::table('aulas')->get();
        $horarios = Horario::with('aula')->where([['seccion_id', $seccion_id]])->get();

        // $carrera = Carrera::find($carrera_id);
//        dd($carrera);
        return view('JefeAreaAcademica.CargaHoraria.listxhorario', [
            'secciones' => $secciones,
            'aulas' => $aulas,
            'horarios' => $horarios
        ]);
    }

    public function storehorario(Request $request)
    {
        $horario = new Horario([
            'dia' => $request->dia,
            'hora_inicio' => $request->horainicio,
            'hora_fin' => $request->horafin,
            'seccion_id' => $request->seccion_id,
            'aula_id' => $request->aula_id,
            'turno' => $request->turno_id,

        ]);

        $horario->save();

        return redirect()->route('cargarhoraria.listxseccion', ['seccion_id' => $horario->seccion_id]);
    }

    public function cargaHoraria()
    {
        $carreras = Carrera::all();
        return view('SecretariaAcademica.Secciones.cargaHoraria', ['carreras' => $carreras]);
    }

    public function buscarCargaHoraria(Request $request)
    {
        $periodo = Periodo::where('estado',1)->first();
//        dd($periodos);
        $secciones = Seccion::with('pe_curso.pecarrera.carrera','docentes.persona','horarios','pe_curso.curso')->whereHas('pe_curso.pecarrera.carrera', function ($q) use ($request){
            $q-> where('carrera_id', $request->carrera_id);
        })->where('periodo_id',$periodo->id)->get();
        return response()->json(['secciones' => $secciones], 200);
    }
    public function getseccion(Request $request){

        $seccion = Seccion::with('pe_curso.pecarrera.carrera','pe_curso.pecarrera.planestudio','docente_seccionb.docente.persona','horarios','pe_curso.curso','periodo')->find($request->id);

        return response()->json(['seccion'=> $seccion], 200);
    }
    public function delete($id){

        $carrera = Carrera::find($id);

        if($carrera != null){
            $carrera->estado = 0;
            $carrera->save();
        }

        return redirect()->route('carreras.index');
    }
    public function update(Request $request)
    {

        $seccion = Seccion::find($request->id);



        $seccion_id = $seccion->id;
        $seccion->cupos = $request->cuposEdit;
        $seccion->save();
        $docente_seccion = DocenteSeccion::where('seccion_id',$seccion_id)->first();
        if($docente_seccion == null)
        {
            $docente_seccion = new DocenteSeccion([
                'docente_id' => $request->sel_docenteEdit,
                'seccion_id' => $seccion_id,
                'rol' => "principal",
            ]);
        }
        else
        {
        $docente_seccion->docente_id = $request->sel_docenteEdit;
        }
        $docente_seccion->save();

        //return response()->json(['Mensaje' => 'success'], 200);
        return back();

    }

}
