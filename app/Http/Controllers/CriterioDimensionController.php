<?php

namespace App\Http\Controllers;

use App\CriterioDimension;
use Illuminate\Http\Request;

class CriterioDimensionController extends Controller
{
    public function get_dimension(Request $request)
    {
        $cri_dimensiones=CriterioDimension::whereHas('dimension', function ($q) use ($request){
            $q->where('dimension',$request->dimension);
        })->get();
        return $cri_dimensiones;
    }
}
//->where('carrera_id',$request->carrera_id)
