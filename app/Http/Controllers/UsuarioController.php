<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Departamento;
use App\Docente;
use App\Modulo;
use App\ModuloUsuario;
use App\Padre;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Alumno;
use App\Usuario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function index()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $persona_id = $alumno->persona_id;
        $usuario = Usuario::where('persona_id', $persona_id)->get();
        //dd($usuario);


        //if (!$alumno){
        return view('Alumnos.cambio_clave', ['usuario' => $usuario]);
        //}else{
        //    return view('IntranetAlumno.matriculas.createlicencia', ['matricula'=>$matricula]);
        //}

    }

    public function create()
    {
        $carreras = Carrera::all();
        $modulos = Modulo::all();
        $departamentos = Departamento::all();
        return view('Admin.AdminUsuarios.nuevo_usuario', ['carreras' => $carreras, 'modulos' => $modulos, 'departamentos' => $departamentos]);
    }

    public function update(Request $request)
    {

        $rules = array(
            //'claveEdit' => 'required',
            'claveEditb' => 'required|min:8',
            'claveEditc' => 'required|same:claveEditb'
        );
        $messages = array(
            'required' => 'El campo :attribute es obligatorio.',
            'min' => 'El campo :attribute no puede tener menos de: min carácteres.'
        );

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            //dd($messages);
            //return Redirect::to('password')->withErrors($validation)->withInput("hola");
            $request->session()->flash('alert-success', 'Error: vuelva a intentarlo según las indicaciones');
            //return Redirect::to('form')->withInput(Input::except('password'));
            return back()->withErrors($validation);
        } else {
            //dd($request);
            $password = Hash::make($request->claveEditb);
            //dd($password);
            $usuario = Usuario::where('usuario', $request->usuarioEdit)->first();
            // dd($usuario);
            $usuario->password = $password;
            $usuario->save();
            $request->session()->flash('alert-success', 'Clave Guardada Satisfactoriamente');

            // if($password == Usuario::find(Auth::user()->Password))
            // {
            //     $persona = Persona::find($request->id);
            //     $usuario = new cliente(array(
            //     $cliente = cliente::find(Auth::user()->id),
            //     $cliente->Password = Hash::make(Input::get('newpassword'))
            //     ));
            //     if($cliente->save())
            //     {
            //         return Redirect::to('password')->with('notice', 'Nueva contraseña guardada correctamente');
            //     }
            //     else
            //     {
            //         return Redirect::to('password')->with('notice', 'No se ha podido guardar la nueva contaseña');
            //     }
            // }
            // else
            // {
            //     return Redirect::to('password')->with('notice', 'La contraseña actual no es correcta')->withInput();
            // }

        }


        //return redirect()->route('Alumnos.editar_datos.index');
        return back();
    }

    public function createUsuarios()
    {

    }

    public function storeAdminUsuaios(Request $request)
    {
        $persona_check = Persona::where('DNI', $request->DNI)->first();
        $tipo_usuario = $request->tipo_usuario;

        DB::beginTransaction();
        if (!$persona_check) {
//            echo "La persona con Dni ".$request->DNI."no existe";
            $persona = new Persona();
            $persona->nombres = $request->nombres;
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->telefono = $request->telefono;
            $persona->DNI = $request->DNI;
            $persona->fecha_nacimiento = $request->fecha_nacimiento;
            $persona->sexo = $request->sexo;
            $persona->email_personal = $request->email_personal;
            $persona->email = $request->email;
            $persona->distrito_id = $request->distrito_id;
            $persona->save();
            $persona_id = $persona->id;
            if ($tipo_usuario == 'docente') {
                $docente = new Docente();
                $docente->persona_id = $persona_id;
                $docente->estado = 1;
                $docente->save();
            }
            if ($tipo_usuario == 'padre') {
                $padre = new Padre();
                $padre->persona_id = $persona_id;
                $padre->apoderado = 'si';
                if($request->sexo=='M'){
                    $padre->tipo = 'P';
                }else{
                    $padre->tipo = 'M';
                }
                $padre->save();
            }
//            echo "Persona id".$persona_id;
        } else {
//            echo "La persona con Dni ".$request->DNI."ya existe";
            $persona_id = $persona_check->id;
//            echo "Persona id".$persona_id;
        }

        $usuario_check = Usuario::where('persona_id', $persona_id)->first();

        if (!$usuario_check) {
//            echo "El Usuario con persona id ".$persona_id."no existe";
            $usuario = new Usuario();
            $usuario->tipo = $request->tipo_usuario;
            $usuario->estado = 1;
            $usuario->usuario = $request->usuario;
            $usuario->password = Hash::make($request->password);
            $usuario->persona_id = $persona_id;
            $usuario->save();
            foreach ($request->modulo_id as $modulo) {
                DB::table('modulo_usuario')->insert([
                    'modulo_id' => $modulo,
                    'usuario_id' => $usuario->id
                ]);
            }
        }
        else {
//            echo "El Usuario con persona id ".$persona_id."ya existe";
            $usuario = $usuario_check;
            $usuario->save();
        }


        DB::commit();
        return redirect()->back();
    }

    public function createUsuariosContraseña()
    {
        return view('Admin.AdminUsuarios.contraseñas_usuarios');
    }

    public function buscarUsuarioxDni($dni)
    {
//        return $dni;
        $usuario = Usuario::with('persona')->whereHas('persona', function ($q) use ($dni) {
            $q->where('DNI', $dni);
        })->first();


        return response()->json(['usuario' => $usuario], 200);

    }

    public function updateContraseña(Request $request)
    {
        $usuario = Usuario::find($request->idUsuario);
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        return redirect()->back();

    }

    public function indexPerfil()
    {
        $usuario = Auth::user();
        return view('Perfil.index', ['usuario' => $usuario]);
    }

    public function updateFotoPerfil(Request $request)
    {
        $persona_id = Auth::user()->persona->id;
        $dni = Auth::user()->persona->DNI;
        $tipo_usuario = Auth::user()->tipo;

        if ($tipo_usuario != 'alumno') {
            $persona = Persona::find($persona_id);
            $img = $dni . 'urlimg.' . $request->file('urlimg')->getClientOriginalExtension();
            $request->file('urlimg')->storeAs('public/foto_perfil_docentes/' . $dni . '/', $img);
            $patch = Storage::url('foto_perfil_docentes/' . $dni . '/' . $img);
            $persona->urlimg = $patch;
            $persona->save();
            DB::commit();

        } else if ($tipo_usuario == 'alumno') {
            $persona = Persona::find($persona_id);
            $img = $dni . 'urlimg.' . $request->file('urlimg')->getClientOriginalExtension();
            $request->file('urlimg')->storeAs('public/foto_perfil_alumnos/' . $dni . '/', $img);
            $patch = Storage::url('foto_perfil_alumnos/' . $dni . '/' . $img);
            $persona->urlimg = $patch;
            $persona->save();
            DB::commit();
        }

        return redirect()->back();
    }

    public function updateDatosPersonales(Request $request)
    {
        $persona_id = Auth::user()->persona->id;
        $persona = Persona::find($persona_id);
        $persona->nombres = $request->nombres;
        $persona->paterno = $request->paterno;
        $persona->materno = $request->materno;
        $persona->email_personal = $request->email_personal;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->save();

        return redirect()->back();
    }

    public function updateContraseñaPerfil(Request $request)
    {
        $usuario_id = Auth::user()->id;
        $usuario = Usuario::find($usuario_id);
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        return redirect()->back();
    }

}
