<?php

namespace App\Http\Controllers;

use App\Distrito;
use Illuminate\Http\Request;

class DistritoController extends Controller
{
    public function show($id_prov)
    {
        $distrito=Distrito::where('provincia_id', $id_prov)->get();
        return $distrito;
   }
}
