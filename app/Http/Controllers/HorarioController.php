<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horario;
use App\DocenteSeccion;
use App\Periodo;
use Carbon;
use Carbon\CarbonPeriod;


class HorarioController extends Controller
{
    //

    public function removehorario(Request $request)
    {
        Horario::destroy($request->id);

        return response()->json(['success'],200);
    }

    public function sesionesApredizaje(Request $request){

        $horarios=Horario::where('seccion_id',$request->seccion_id)->get();
        // return $horarios;
        $periodo=Periodo::find($request->periodo_id);
        $carbon_period = CarbonPeriod::create($periodo->fecha_inicio, $periodo->fecha_fin);
        $sesiones=[];
        foreach ($carbon_period as $date) {
            // echo $date->format('Y-m-d');
            foreach ($horarios as $key => $horario) {

                $dayOfWeek=$this->getDayOfWeek($horario->dia);

                if($date->dayOfWeek==$dayOfWeek){
                    $sesiones[]=[
                        'date'=>$date->format('Y-m-d'),
                        'horario'=>$horario
                    ];
                }

            }

        }

        return $sesiones;
    }


    function getDayOfWeek($dia){
        if($dia == 'DOMINGO'){
            return 0;
        }

        if($dia == 'LUNES'){
            return 1;
        }

        if($dia == 'MARTES'){
            return 2;
        }

        if($dia == 'MIERCOLES'){
            return 3;
        }

        if($dia == 'JUEVES'){
            return 4;
        }

        if($dia == 'VIERNES'){
            return 5;
        }

        if($dia == 'SABADO'){
            return 6;
        }
    }

}
