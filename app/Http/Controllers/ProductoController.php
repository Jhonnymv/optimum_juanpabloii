<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Instrument;
use App\PeCurso;
use App\Seccion;
use Illuminate\Http\Request;
use App\Periodo;
use App\EvaluacionCategoria;
use App\Producto;
use App\Instrumento;
use App\ProductoInstrumento;
use App\silabo;
use App\Contenido;
use App\unidades;

class ProductoController extends Controller
{
    public function index()
    {

        $periodosAcademicos =Periodo::where('estado',1)->get();
        $categorias=EvaluacionCategoria::where('estado',1)->get();
        $instrumento=Instrumento::where('estado',1)->get();
        //$evaluacioncriterios=EvaluacionCriterio::where('estado',1)->get();
       // $evaluaciones=Evaluacion::where('estado',1)->get();


        return view('IntranetDocente.SistemaEvaluacion.index',[
            'periodosAcademicos' => $periodosAcademicos ,
            'categorias'=>$categorias,
            'instrumento'=>$instrumento
            //'evaluaciones'=>$evaluaciones
            //'evaluacioncriterios'=>$evaluacioncriterios

        ]);

    }

    public function store(Request $request){

        /* $validar=Validator::make($request->all(),[
            'nombre' => 'required|unique:carreras|max:150',
            'resolucion' =>  'required|max:200',
        ]);
        if ($validar->fails()){
            return redirect()->back()->withInput()->withErrors($validar->errors());
        } */

        if($request->wantsJson()){
            $producto = new Producto([
                'producto' => $request->producto,
                'evaluacion_categoria_id' => $request->categoria_id2,
                'seccion_id'=>$request->seccion_id2,
                'estado' => 1
            ]);
            $producto->save();
            return response()->json(['producto'=>$producto],200);

        };


    }

    public function getProductosCategoria(Request $request){
        //dd($request);
        $productoscategoria=Producto::where('evaluacion_categoria_id',$request->categoria_id)->
        where('seccion_id',$request->seccion_id)->where('estado',1)->get();
        $categoria=EvaluacionCategoria::where('id',$request->categoria_id)->get();
        //dd($criterios);
        return response()->json(['productoscategoria'=>$productoscategoria,
                                 'categoria'=>$categoria],200);

    }

    public function getProducto(Request $request){

        $producto = Producto::find($request->id);

        return response()->json(['producto'=> $producto], 200);
    }

    public function update(Request $request,$id){
        if($request->wantsJson()){
            $producto = Producto::find($id);

            $producto->producto = $request->productoEdit;
            $producto->save();

            //s$product=Producto::where('estado',1)->get();
            return response()->json(['respuesta'=>'exito'],200);
        }

    }

    public function delete(Request $request,$id){


    if($request->wantsJson()){
        $producto = Producto::find($id);

        if($producto != null){
            $producto->estado = 0;
            $producto->save();
        }

        return response()->json(['respuesta'=>'exito'],200);

    }

       // return back();
    }



    public function prodinstStore(Request $request){

        /* $validar=Validator::make($request->all(),[
            'nombre' => 'required|unique:carreras|max:150',
            'resolucion' =>  'required|max:200',
        ]);
        if ($validar->fails()){
            return redirect()->back()->withInput()->withErrors($validar->errors());
        } */
 if($request->wantsJson()){

        $prodInst = new ProductoInstrumento([
            'producto_id' => $request->producto_id,
            'instrumento_id' => $request->slcInstrumento,
            'contenido_id'=>$request->slcContenido,
            'fecha'=>$request->fecha,
            'estado' => 1
        ]);

        $prodInst->save();

        return response()->json(['respuesta'=>'exito'],200);
    }


        //return back();
    }

    public function getInstrumentosProducto(Request $request){
        //return $request;
       $seccion = Seccion::where('id',$request->producto_id)->first();
       $pe_cur = PeCurso::where('id', $seccion->pe_curso_id)->first();
       //$prodInstrumento = Instrument::where('description',$pe_cur->creditos)->orWhere('tipo',2)->get();
       $prodInstrumento = Instrument::where('description',$pe_cur->creditos)->get();
//         $prodInstrumento=ProductoInstrumento::with(['instrumentos','contenidos'])
//                   ->where('producto_id',$request->producto_id)
//                   ->where('estado',1)->get();
//         //dd($prodInstrumento);
        return response()->json(['prodInstrumento'=>$prodInstrumento],200);

    }

    public function getProdInstrumento(Request $request){

        $prodinstrumento = ProductoInstrumento::with('contenidos')
        ->find($request->id);

        return response()->json(['prodinstrumento'=> $prodinstrumento], 200);
    }

    public function updateProdInstrumento(Request $request,$id){
    if($request->wantsJson()){
        $prodInstrumento = ProductoInstrumento::find($id);
        $prodInstrumento->instrumento_id = $request->instrumentoEdit;
        $prodInstrumento->fecha = $request->fechaEdit;
        $prodInstrumento->contenido_id = $request->slcContenidoEdit;
        $prodInstrumento->save();

        return response()->json(['respuesta'=>'exito'],200);
    }

       // return back();
    }

    public function deleteProdInstrumento(Request $request,$id){

    if($request->wantsJson()){
        $prodInstrumento = ProductoInstrumento::find($id);

        if($prodInstrumento != null){
            $prodInstrumento->estado = 0;
            $prodInstrumento->save();
        }
        return response()->json(['respuesta'=>'exito'],200);
    }
        //$categorias=EvaluacionCategoria::where('estado',1)->last();
        //return back();
        /* return view('IntranetDocente.SistemaEvaluacion.index',[
            'categorias' => $categorias
            //'evaluaciones'=>$evaluaciones
            //'evaluacioncriterios'=>$evaluacioncriterios

        ]); */
    }

    public function getUnidades(Request $request,$seccion_id){
       if($request->wantsJson()){
            $listaunidades = silabo::with(['unidades'])
            ->where('seccion_id',$seccion_id)->get()
            ->pluck('unidades')
            ->unique();

            return response()->json(['listaunidades'=> $listaunidades], 200);
        }
    }

    public function getContenidos(Request $request,$unidad_id){
        if($request->wantsJson()){
           $contenidos = Contenido::where('unidades_id',$unidad_id)->get();
           $unidad=unidades::find($unidad_id);

           return response()->json(['contenidos'=> $contenidos,
                                    'unidad'=>$unidad], 200);
        }
    }

    public function findContenido(Request $request,$contenido_id){
        if($request->wantsJson()){
           $contenido = Contenido::find($contenido_id);


           return response()->json(['contenido'=> $contenido], 200);
        }
    }

}
