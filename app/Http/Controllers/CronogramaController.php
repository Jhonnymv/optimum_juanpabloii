<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Alumnopago;
use App\Carrera;
use App\Concepto;
use App\Cronograma;
use App\Periodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class CronogramaController extends Controller
{
    //
    public function index()
    {
        $periodos = Periodo::where('estado', 1)->get();
        $conceptos = Concepto::where('estado', 1)->get();
        return view('SecretariaAcademica.Cronogramas.index', ['periodos' => $periodos, 'conceptos' => $conceptos]);
    }

    public function store(Request $request)
    {
        $cronograma_check = Cronograma::where([['periodo_id', $request->periodo_id], ['concepto_id', $request->concepto_id]])->first();
        if (!$cronograma_check) {
            for ($i = 0; $i < $request->cuotas; $i++) {
                DB::beginTransaction();
                $cronograma = new Cronograma();
                $cronograma->num_couta = $request->num_couta[$i];
                $cronograma->fech_vencimiento = $request->fechaVencimiento[$i];
                $cronograma->periodo_id = $request->periodo_id;
                $cronograma->estado = 1;
                $cronograma->concepto_id = $request->concepto_id;
                $cronograma->save();
                DB::commit();
            }
            return back();
        } else {
            return back();
        }
    }

    public function show(Request $request)
    {
        if ($request->periodo_id && !$request->concepto_id) {
            $cronograma = Cronograma::with('concepto')->where('periodo_id', $request->periodo_id)->get();
        }
        if (!$request->periodo_id && $request->concepto_id) {
            $cronograma = Cronograma::with('concepto')->where('concepto_id', $request->concepto_id)->get();
        }
        if ($request->periodo_id && $request->concepto_id) {
            $cronograma = Cronograma::with('concepto')->where([['periodo_id', $request->periodo_id], ['concepto_id', $request->concepto_id]])->get();
        }
        return response()->json(['cronograma' => $cronograma], 200);
    }

    public function verCronograma($idbuscas)
    {
//        $padre=Auth::user()->persona->padre->id;
        $persona = Auth::user();
        $periodo=Periodo::all()->last();
        $modulos = $persona->modulos;
//        dd($periodo);
        $modulo = '';
//        dd($modulos);
//        $cronograma = Cronograma::whereHas('alumno', function ($q) use ($idbuscas) {
//            $q->where('alumno_id', $idbuscas);
//        })->get();
        $alumno=Alumno::find($idbuscas);
//        dd();
        $carrera= Carrera::find($alumno->alumnocarreras->first()->carrera_id);
        $alumno_pagos= Alumnopago::where('alumno_id',$idbuscas)->get();
        $cronograma = Cronograma::where('periodo_id',$periodo->id)->get();
//        dd($cronograma);

        if ($persona->modulos->first()->code === 'intranet_alumno') {
            return view('IntranetAlumno.pagos.pagosAlumnos', ['cronograma' => $cronograma,'carrera'=>$carrera,'alumnoPago'=>$alumno_pagos]);
        } elseif ($persona->modulos->first()->code === 'padres') {
            return view('IntranetPadre.Pagos.verCronograma', ['cronograma' => $cronograma,'carrera'=>$carrera,'alumnoPago'=>$alumno_pagos]);
        } else {
            return view('SecretariaAcademica.Cronogramas.cronogramaAlumno', ['cronograma' => $cronograma,'carrera'=>$carrera,'alumnoPago'=>$alumno_pagos]);
        }

    }

    public function verAlumnosPagos()
    {
        $alumnos = Alumno::all();
        return view('SecretariaAcademica.Cronogramas.lista_alumnos_pagos', ['alumnos' => $alumnos]);
    }

    //Método es para optener el monto de la matricula para la vista padres de familia
    public function GetCronograma($cronograma_id)
    {
        $cronogrma = Cronograma::with('concepto')->find($cronograma_id);
        return response()->json($cronogrma, 200);
    }

}
