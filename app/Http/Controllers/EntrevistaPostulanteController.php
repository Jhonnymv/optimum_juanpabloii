<?php

namespace App\Http\Controllers;

use App\EntrevistaPostulante;
use App\Persona;
use App\Postulacion;
use App\Postulante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EntrevistaPostulanteController extends Controller
{
    public function index()
    {
        return view('Admisiones.examenes.resultado_entrevista');
//        $postulantes=Persona::whereHas('postulante')->whereHas('alumno')->get() ;
//        foreach ($postulantes as $post){
//            echo $post->email."<br>";
//        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $postulante=Postulante::find($request->postulante_id);


        $et_pos=new EntrevistaPostulante();
        $et_pos->postulante_id=$request->postulante_id;
        $et_pos->puntaje=$request->puntaje;
        $im_name = $postulante->persona->DNI . '_acta.' . $request->file('url_acta')->getClientOriginalExtension();
        $request->file('url_acta')->storeAs('public/ActasEntrevistas/', $im_name);
        $patch = Storage::url('ActasEntrevistas/' . $im_name);
        $et_pos->url_acta = $patch;
        $et_pos->save();

        $postulacion=Postulacion::where('postulante_id',$request->postulante_id)->get()->last();
        $postulacion->puntaje_entrevista=$request->puntaje;
        $postulacion->save();
        DB::commit();

        return back();
    }
}
