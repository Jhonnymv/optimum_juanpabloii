<?php

namespace App\Http\Controllers;

use App\EalDetalles;
use App\ExamenAlumno;
use App\Periodo;
use App\Persona;
use App\Postulante;
use bar\baz\source_with_namespace;
use FontLib\Table\Type\name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PharIo\Manifest\RequiresElementTest;

class ExamenPostulanteController extends Controller
{
    public function store(Request $request)
    {
//        try {
        session()->forget('msj');
            $postulante = Auth::user()->persona->postulante;
            $pos_examen = ExamenAlumno::where([['examen_id', $request->examen_id], ['postulante_id', $postulante->id]])->first();
            if ($request->alternativa) {
                if ((!$pos_examen)) {
                    DB::beginTransaction();
                    $e_p = new ExamenAlumno();
                    $e_p->postulante_id = $postulante->id;
                    $e_p->examen_id = $request->examen_id;
                    $e_p->save();

                    foreach ($request->pregunta as $item) {
                        $ep_detalle = new EalDetalles();
                        $ep_detalle->examen_postulante_id = $e_p->id;
                        $ep_detalle->pregunta_id = $item;
                        if (array_key_exists($item, $request->alternativa))
                            $ep_detalle->alternativa_id = $request->alternativa[$item];
                        else
                            $ep_detalle->alternativa_id = 0;
                        $ep_detalle->save();
                    }
                    DB::commit();
                    return view('Postulantes.index');
                } else
                    return view('Postulantes.index');
            }else{
                session(['msj'=>'Seleccione almenos una pregunta']);
                return back();
            }
        }
//        catch (\Exception $e){
//            return back();
//            return redirect()->route('examen.index');
//            return app(ExamenController::class)->index('SELECCIONE AL MENOS UNA PREGUNTA');
//            return redirect()->action('ExamenController@index',['msj'=>'SELECCIONE AL MENOS UNA PREGUNTA']);
//        }
//    }
}
