<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carrera;
use App\PeCurso;
use App\PlanEstudio;
use App\Curso;
use App\Prerrequisito;

class PrerrequisitoController extends Controller
{
    public function index($envpecursoid,$envcursoid,$envcarreraid,$envpeid){         
             
     $prerrequisitos = Prerrequisito::where([
            ['estado','=','1'],
            ['pe_curso_id','=',$envpecursoid]
         ])->get();
         
      $cursospre=PeCurso::select('cursos.id',
      'cursos.nombre')
        ->join('cursos','cursos.id','pe_cursos.curso_id')->where([
        ['pe_cursos.estado','=','1'],
        ['pe_cursos.plan_estudio_id','=',$envpeid],
        ['pe_cursos.carrera_id','=',$envcarreraid]
     ])->get();

     $pecursos=PeCurso::where(
         'id','=',$envpecursoid
     );
         return view('Admin.Prerrequisitos.index',[
             'prerrequisitos' => $prerrequisitos,
             'cursospre'=>$cursospre,
             'pecursos'=>$pecursos             
         ]);
     }
 
     public function store(Request $request){
         $validar = $request->validate([
             
         ]);
 
         $prerrequisito = new Prerrequisito([ 
             'curso_pre_id' => $request->cursospre,
             'pe_curso_id' =>$request->pecursosid,                                   
             'estado' => 1
         ]);
   //dd($request->pecursos);
         $prerrequisito->save();
       
         return redirect()->route('prerrequisitos.index',
         array('envpecursoid'=>$request->pecursosid,
         'envcursoid'=>$request->cursoid,
         'envcarreraid'=>$request->carreraid,
         'envpeid'=>$request->peid));
 
     }
 
     public function delete($id,$envpecursoid,$envcursoid,$envcarreraid,$envpeid){
 
        // $pecarrera = PeCarrera::where('plan_estudio_id','=',$plan_estudio_id)
          //                    ->where('carrera_id','=',$carrera_id)->first();
          $prerrequisito = Prerrequisito::find($id);
         if($prerrequisito != null){
             $prerrequisito->estado = 0;  
             //dd($pecarrera);          
             $prerrequisito->save();
 
         }
 
         return redirect()->route('prerrequisitos.index',array(
         'envpecursoid'=>$envpecursoid,
         'envcursoid'=>$envcursoid,
         'envcarreraid'=>$envcarreraid,
         'envpeid'=>$envpeid));
     }
}
