<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use Illuminate\Http\Request;
use App\Modulo;
use Auth;

class AdministratorController extends Controller
{
    //

    public function module_chooser()
    {
        session(['module_id' => 'default']);
        $modules = Auth::user()->modulos;
        return view('auth.module_chooser')->with('modules', $modules);
    }

    public function select_module(Request $request)
    {
        $modulo = Modulo::find($request->modulo_id);

        session(['module_id' => $modulo->id]);

        if ($modulo->code == 'intranet_docente') {
            return redirect()->route('IntranetDocente.silabos.create');
        }
        if ($modulo->code == 'contabilidad') {
            return redirect()->route('contabilidad.index');
        }

        if ($modulo->code == 'intranet_alumno') {
            return redirect()->route('Intranet_Alumno.index');
        }

        if ($modulo->code == 'intranet_secretaria') {
            return redirect()->route('secretaria.index');
        }

        if ($modulo->code == 'intranet_area_academica') {
            return redirect()->route('IntranetAreaAcademica.silabos.index');
        }
        if ($modulo->code == 'administrador') {
            return redirect()->route('admin.index');
        }
        if ($modulo->code == 'admision') {
            return redirect()->route('admision.index');
        }
        if ($modulo->code == 'visita') {
            return redirect()->route('visitas.index');
        }
        if ($modulo->code == 'postulante') {
            return redirect()->route('examen.index');
        }
        if ($modulo->code == 'padres') {
            return redirect()->route('padre.index');
        }
    }
}
