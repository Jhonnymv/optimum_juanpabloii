<?php

namespace App\Http\Controllers;

use App\Flujo;
use App\Solicitud;
use App\Tramite;
use App\UnidadAdministrativa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SolicitudController extends Controller
{
    public function create()
    {
        $tramites = Tramite::all();
        return view('OptimunTramites.tramites.nuevoTramite', ['tramites' => $tramites]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $solicitud = new Solicitud();
        $solicitud->asunto = $request->asunto;
        $solicitud->descripcion = $request->descripcion;
        $solicitud->usuario_id = Auth::user()->id;
        $solicitud->tramite_id = $request->tramite_id;
        $solicitud->save();

        $num = count($request->file('images'));

        for ($i = 0; $i < $num; $i++) {
            foreach ($request->file()['images'][$i] as $img) {
                $extension = $img->getClientOriginalExtension();
                $nombre_archivo = Auth::user()->id . '_' . $solicitud->id . '_' . $i . '.' . $extension;
                $patch = $img->storeAs('public/tramites', $nombre_archivo);
                DB::table('anexo_solicitudes')->insert([
                    'solicitud_id' => $solicitud->id,
                    'nombre' => $nombre_archivo,
                    'url' => $patch
                ]);
            }
        }
        DB::commit();

        return back();
    }

    public function get_detalle(Request $request, $solicitud_id)
    {
        if ($request->wantsJson()) {
            $solicitud = Solicitud::with('usuario.persona','tramite.unidad_administrativa.usuario.persona', 'flujos.usuario.persona')
                ->where('id',$solicitud_id)
                ->orderBy('created_at', 'desc')->first();
            return response()->json(['solicitud' => $solicitud], 200);
        } else {
            $solicitud = Solicitud::find($solicitud_id);
            $usuario_id = Auth::user()->id;
            $uni_administrativas = UnidadAdministrativa::where('responsable_id', '<>', $usuario_id)->get();
            return view('OptimunTramites.tramites.detalle', ['solicitud' => $solicitud, 'uni_administrativas' => $uni_administrativas]);
        }
    }

    public function list(Request $request)
    {
        $usuario_id = Auth::user()->id;

        $uni_administrativa = UnidadAdministrativa::where('responsable_id', $usuario_id)->first();
        if ($uni_administrativa) {
            $uni_administrativa_id = $uni_administrativa->id;

            $solicitudes = Solicitud::whereHas('tramite.unidad_administrativa', function ($query) use ($usuario_id) {
                $query->where('responsable_id', $usuario_id);
            })->orWhereHas('last_flujo', function ($q) use ($uni_administrativa_id, $usuario_id) {
                $q->whereIn('reenviado_a', array($uni_administrativa_id));
            })->get()->sortByDesc('created_at');

            return view('OptimunTramites.tramites.list_tramites', ['solicitudes' => $solicitudes]);
        } else {
            return back();
        }

    }

    public function send_solicitudes()
    {
        $solicitudes = Solicitud::where('usuario_id', Auth::user()->id)->get();
        return view('OptimunTramites.tramites.mis_tramites', ['solicitudes' => $solicitudes]);
    }
}
