<?php

namespace App\Http\Controllers;

use App\Periodo;
use Illuminate\Http\Request;

class PeriodoController extends Controller
{
    public function index()
    {
        $periodos = Periodo::all();
        return view('SecretariaAcademica.Periodos.index', ['periodos' => $periodos]);
//        dd($periodos);
    }

    public function store(Request $request)
    {
        $periodoacademico = new Periodo([
            'descripcion' => $request->descripcion,
            'fecha_inicio' => $request->fecha_inicio,
            'fecha_fin' => $request->fecha_fin,
            'estado' => 1
        ]);

        $periodoacademico->save();
        if ($request->wantsJson()) {
            return response()->json(['Mensaje' => 'success'], 200);
        } else {
            return redirect()->action('PeriodoController@index')->with('Mensaje', 'CredencialesAlumno registrado exitosamente!');
        }
    }

    public function edit($id)
    {
        $periodo = Periodo::find($id);
        return response()->json(['periodo' => $periodo], 200);
    }

    public function update(Request $request, $id)
    {
        $periodo = Periodo::find($id);
        $periodo->descripcion = $request->descripcion;
        $periodo->fecha_inicio = $request->fecha_inicio;
        $periodo->fecha_fin = $request->fecha_fin;
        $periodo->save();

        return redirect()->action('PeriodoController@index');
    }

    public function delete($id)
    {

        $pa = Periodo::find($id);
        $pa->estado = '0';
        $pa->save();

        return redirect()->route('periodos.index');
    }

    public function getPeriodoacademico(Request $request)
    {

        $pa = Periodo::find($request->id);

        return response()->json(['periodoacademico' => $pa], 200);
    }
}
