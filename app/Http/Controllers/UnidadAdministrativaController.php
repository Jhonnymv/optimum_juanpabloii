<?php

namespace App\Http\Controllers;

use App\Tramite;
use App\UnidadAdministrativa;
use App\Usuario;
use Illuminate\Http\Request;

class UnidadAdministrativaController extends Controller
{
    public function create()
    {
        $uAdministrativas = UnidadAdministrativa::all();
        $responsables = Usuario::all();
        return view('OptimunTramites.UnidadesAdministrativas.index', ['uAdministrativas' => $uAdministrativas], ['responsables' => $responsables]);
    }

    //Regstrar Unidad Administrativa
    public function store(Request $request)
    {
        $uAdministrativas = new  UnidadAdministrativa();
        $uAdministrativas->descripcion = $request->descripcion;
        $uAdministrativas->responsable_id = $request->responsable_id;

        $uAdministrativas->save();
        return redirect()->route('unidadAdministrativa.create');
    }

    public function getUndAdmin($id) {
        $uAdministrativa = UnidadAdministrativa::find($id);
        return response()->json(['uAdministrativa'=>$uAdministrativa], 200);
    }

    public function update(Request $request) {
//        dd($request);
        $uAdministrativa = UnidadAdministrativa::find($request->id_und_admi);
        $uAdministrativa->descripcion = $request->ud_descripcion_edit;
        $uAdministrativa->responsable_id = $request->responsable_id_edit;
        $uAdministrativa->save();
        return redirect()->route('unidadAdministrativa.create');
    }
}
