<?php

namespace App\Http\Controllers;

use App\Colegio;
use Illuminate\Http\Request;

class ColegioController extends Controller
{
    public function store(Request $request)
    {

        $colegio = new Colegio([
            'nombre' => $request->nombre,
            'distrito_id' => $request->distrito_id,
        ]);

        $colegio->save();
    }

    public function create()
    {

    }

}
