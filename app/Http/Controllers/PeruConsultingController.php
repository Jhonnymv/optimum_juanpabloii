<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use Peru\Sunat\RucFactory;
use Peru\Jne\DniFactory;
//use jossmp\essalud\asegurado;

class PeruConsultingController extends Controller
{
    public function search_ruc($ruc)
    {
        $factory = new RucFactory();
        $cs = $factory->create();

        $company = $cs->get($ruc);
        if (!$company) {
            return response()->json(['res' => 'Not found']);
        }
        return response()->json($company, 200);
    }

    public function searchPerson($dni)
    {
        $factory = new DniFactory();
        $cs = $factory->create();

        $person = $cs->get($dni);
        if (!$person) {
            return response()->json(['res' => 'Not found']);
        }
        return response()->json($person, 200);
    }
}
