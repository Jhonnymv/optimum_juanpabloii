<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\AlumnoCarrera;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\PeCarrera;
use Illuminate\Http\Request;
use App\Carrera;
use App\PeCurso;
use App\PlanEstudio;
use App\Curso;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;

class PeCursoController extends Controller
{

    public function index($envidcarrera, $envidpe)
    {
        // $pecursos = PeCurso::where('estado','=','1')
        //     ->where('carrera_id','=',$envidcarrera)
        //     ->where('plan_estudio_id','=',$envidpe)->get();


        $pecursos = PeCurso::where([
            ['estado', '=', '1'],
            ['carrera_id', '=', $envidcarrera],
            ['plan_estudio_id', '=', $envidpe]
        ])->get();

        $carreras = Carrera::where('estado', '=', '1')
            ->where('id', '=', $envidcarrera)->get();
        $planestudios = PlanEstudio::where('estado', '=', '1')
            ->where('id', '=', $envidpe)->get();
        $cursos = Curso::where('estado', '=', '1')
            ->where('carrera_id', '=', $envidcarrera)->get();

        return view('SecretariaAcademica.PeCursos.index', [
            'pecursos' => $pecursos,
            'carreras' => $carreras,
            'planestudios' => $planestudios,
            'cursos' => $cursos
        ]);
    }

    public function store(Request $request)
    {
        $validar = $request->validate([

        ]);

        $pecurso = new PeCurso([
            'horas_teoria' => $request->horas_teoria,
            'horas_practica' => $request->horas_practica,
            'semestre' => $request->semestre,
            'creditos' => $request->creditos,
            'curso_id' => $request->curso_id,
            'pe_carrera_id' => $request->pe_carrera_id,
            'estado' => 1
        ]);

        $pecurso->save();
        if ($request->vista) {
            return redirect()->route('pecursos.listxpecarrera', ['carrera_id' => $pecurso->pe_carrera_id]);
        } else {
            return redirect()->route('pecursos.index', array('envidcarrera' => $request->carreraid
            , 'envidpe' => $request->peid));
        }

    }

    public function delete($id, $carreraid, $peid)
    {
        // $pecarrera = PeCarrera::where('plan_estudio_id','=',$plan_estudio_id)
        //                    ->where('carrera_id','=',$carrera_id)->first();
        $pecurso = PeCurso::find($id);
        if ($pecurso != null) {
            $pecurso->estado = 0;
            //dd($pecarrera);
            $pecurso->save();
        }

        return redirect()->route('pecursos.index', array('envidcarrera' => $carreraid
        , 'envidpe' => $peid));
    }

    public function get_pecurso(Request $request)
    {
        //dd($request);
        //$alumno = Auth::user()->persona->alumno;
        //$carrera= Carrera::find($alumno->alumnocarreras->first()->carrera_id);
        $pe_curso = PeCurso::with(['curso'])->find($request->id);
//        return response()->json(['pe_curso' => $pe_curso,'carrera'=>$carrera], 200);
        return response()->json(['pe_curso' => $pe_curso], 200);
    }
    public function listxpecarrerasec(Request $request, $carrera_id)
    {
    $alumno = Auth::user()->persona->alumno;
    //return $carrera_id;
    //dd($carrera_id);
    $pe_carrera = PeCarrera::where('carrera_id',$carrera_id)->get();
    //return $pe_carrera;
    $pe_cursos = PeCurso::with('curso')->where([['pe_carrera_id', $pe_carrera->first()->id], ['estado', 1]])->whereHas('curso',function ($q){
        $q->where('nombre','like','TALLER%');
    })->get();
    //return $pe_cursos;
    $cursos = Curso::where([['carrera_id', $carrera_id], ['estado', 1]])->get();
    return response()->json(['pe_cursos' => $pe_cursos], 200);
    }

    public function listxpecarrera(Request $request, $carrera_id)
    {
        $alumno = Auth::user()->persona->alumno;
        if ($request->wantsJson()) {
            $carrera = $alumno->alumnocarreras->first()->carrera->first()->nombre;
            $carrera = $alumno->alumnocarreras->first()->carrera->first()->nombre;
            $alumno_carrera = AlumnoCarrera::where([['alumno_id', $alumno->id], ['carrera_id', $carrera_id]])->first();
            $matricula = Matricula::with('matriculadetalles.seccion.pe_curso.curso','matriculadetalles.seccion.horarios')->where('alumno_carrera_id', $alumno_carrera->id)->first();
            if (!$matricula) {
                $pe_carrera = PeCarrera::find($alumno_carrera->pe_carrera_id);
                if ($request->tipo_peticion) {
                    $pe_cursos = PeCurso::with('curso')
                        ->where([['pe_carrera_id', $pe_carrera->id], ['semestre', $alumno_carrera->ciclo], ['estado', 1]])
                        ->whereHas('curso',function ($q){
                            $q->where('nombre','not like','Taller%');
                        })->get();
                } else {
                    $pe_cursos = PeCurso::with('curso')
                        ->where([['pe_carrera_id', $pe_carrera->id], ['estado', 1]])
                        ->orderBy('semestre', 'asc')->get();
                }
                return response()->json(['pe_cursos' => $pe_cursos,'alumno'=>$alumno,'carrera'=>$carrera], 200);
            }
            else{
                return response()->json(['matricula'=>$matricula,'carrera'=>$carrera], 200);
            }
        }
        $pe_carrera = PeCarrera::find($request->carrera_id);
        $pe_cursos = PeCurso::where([['pe_carrera_id', $request->carrera_id], ['estado', 1]])->get();
        $cursos = Curso::where([['carrera_id', $pe_carrera->carrera_id], ['estado', 1]])->get();
        return view('SecretariaAcademica.PeCursos.listxpecarrera', [
            'pe_carrera' => $pe_carrera,
            'pe_cursos' => $pe_cursos,
            'cursos' => $cursos
        ]);
    }

    public function get_adicionales(Request $request)
    {

        if ($request->wantsJson()) {
            $pe_carrera = PeCarrera::where([['carrera_id', $request->carrera_id], ['estado', 1]])->first();
            $carrera= Carrera::find($request->carrera_id);
            $ciclo = AlumnoCarrera::where([['carrera_id', $pe_carrera->id], ['alumno_id', Auth::user()->persona->alumno->id]])->first()->ciclo;
//            $pe_cursos = PeCurso::with('curso')->where([['pe_carrera_id', $pe_carrera->id], ['semestre', '=', $ciclo], ['estado', 1]])->orderBy('semestre', 'asc')->get();
            $pe_cursos = PeCurso::with('curso')
                ->where([['pe_carrera_id', $pe_carrera->id], ['semestre', '=', $ciclo], ['estado', 1]])
                ->orderBy('semestre', 'asc')
                ->whereHas('curso',function ($q){
                    $q->where('nombre','like','Taller%');
                })->get();
            return response()->json(['pe_cursos' => $pe_cursos,'carrera'=>$carrera], 200);
        }
    }

// reporte de plan de estudios
    public function list_pe_carrera(Request $request)
    {
        $pe_cursos = PeCurso::with('curso')->whereHas('pecarrera', function ($q) use ($request) {
            $q->where([['plan_estudio_id', $request->plan_estudio_id], ['carrera_id', $request->carrera_id]]);
        })->orderBy('semestre', 'asc')->get();
        return response()->json(['pe_cursos' => $pe_cursos], 200);
    }

    public function get_curso_subsanacion(Request $request)
    {

        $alumno_carrera = AlumnoCarrera::find($request->alumno_carrera_id);
        $pe_cursos = PeCurso::with('curso')->whereHas('pe_carrera.carrera', function ($q) use ($alumno_carrera) {
            $q->where('carrera_id', $alumno_carrera->carrera_id);
        })->where('semestre', '<', $alumno_carrera->ciclo)->get();
        return response()->json(['pe_cursos' => $pe_cursos]);
    }

}
