<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Matricula;
use App\MatriculaDetalle;
use App\PeCurso;
use App\Persona;
use App\PlanEstudio;
use Illuminate\Http\Request;
use App\Periodo;
use App\EvaluacionCriterio;
use App\Evaluacion;
use App\Nota;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Illuminate\Support\Facades\DB;
use App\EvaluacionCategoria;
use App\Seccion;
use App\Carrera;
use App\Curso;
use App\Producto;
use App\Instrumento;
use App\Instrument;
use App\ProductoInstrumento;
use App\unidades;
use Illuminate\Support\Facades\Auth;
use PDF;
use Carbon\Carbon;
use MPDF;

class NotaController extends Controller
{
    public function index()
{

    $periodosAcademicos = Periodo::where('estado', 1)->get();
    $categorias = EvaluacionCategoria::where('estado', 1)->get();
    //$evaluacioncriterios=EvaluacionCriterio::where('estado',1)->get();
    // $evaluaciones=Evaluacion::where('estado',1)->get();


    return view('IntranetDocente.Nota.index', [
        'periodosAcademicos' => $periodosAcademicos,
        'categorias' => $categorias
        //'evaluacioncriterios'=>$evaluacioncriterios

    ]);

}
    public function index_al()
    {
        $alumno = Auth::user()->persona->alumno;
//        $carrera = Carrera::where('id',$alumno->carrera_id)->first();
        $periodo = Periodo::where('estado', 1)->get();
        $matr = Matricula::where([['alumno_carrera_id', $alumno->pe_carreras->first()->carrera_id], ['periodo_id', $periodo->first()->id]])->first();
        //dd($matr);
        $categorias = EvaluacionCategoria::where('estado', 1)->get();
        if ($matr) {
            $matricula_detalles = MatriculaDetalle::with(['seccion', 'seccion.pe_curso', 'seccion.pe_curso.curso', 'seccion.horarios', 'seccion.horarios.aula'])
                ->where('matricula_id', $matr->id)
                ->get();
        }
        //$evaluacioncriterios=EvaluacionCriterio::where('estado',1)->get();
        // $evaluaciones=Evaluacion::where('estado',1)->get();


        return view('IntranetAlumno.notas.indexb', [
            'periodo' => $periodo,
            'categorias' => $categorias,
            'matricula_det' => $matricula_detalles
            //'evaluacioncriterios'=>$evaluacioncriterios

        ]);

    }


    public function store(Request $request)
    {
        // $validar = $request->validate([
        //     'silabocol' => 'required|unique:carreras|max:150',
        //     'referencias_bib' =>  'required|max:200',
        // ]);

        // dd($request);
        // dd($request);
        //return $request;
        $carbon = new Carbon();
        $date = $carbon->now();
        $seccion_id = $request->seccion_id;
        $cat_id = $request->cat_id;
        $inst_id = $request->inst_id;
        //return response()->json(['respuesta' =>'error','alumnos' =>$request->alumno], 200);
        $Produc = Producto::where('seccion_id',$seccion_id)->where('evaluacion_categoria_id',$cat_id)->where('instrumento_id',$inst_id)->first();

        if(!isset($Produc)){
            //$Prod_instr = ProductoInstrumento::where('producto_id', $Produc->id)->where('instrumento_id', $inst_id)->first();
            //return response()->json(['respuesta' => $Prod_instr], 200);
            //if (!isset($Prod_instr)) {
            if ($request->wantsJson()) {
            DB::beginTransaction();
            $Prod = new Producto();
            $Prod->seccion_id = $seccion_id;
            $Prod->estado = 1;
            $Prod->evaluacion_categoria_id = $cat_id;
            $Prod->instrumento_id = $inst_id;
            $Prod->fecha = $date->format('Y-m-d H:i:s');
            $Prod->save();
            $Produc_id = $Prod->id;
            $Prod_instru = new ProductoInstrumento();
            $Prod_instru->producto_id = $Produc_id;
            $Prod_instru->instrumento_id = $inst_id;
            $Prod_instru->estado = 1;
            $Prod_instru->save();
            $prod_inst_id = $Prod_instru->id;
            for ($i = 0; $i < count($request->alumno_id); $i++) {
                //  if ($request->seccion_id[$i]) {
                DB::table('notas')->insert([

                    'nota' => $request->nota[$i],
                    //'fecha' => $request->fecha,
                    'alumno_id' => $request->alumno_id[$i],
                    'producto_instrumento_id' => $prod_inst_id,
                    'estado' => 1,
                    'fecha' => $date->format('Y-m-d H:i:s'),

                    //'seccion_id' => $request->seccion_id[$i]
                ]);
                //       }

            }

            DB::commit();
            //return back();

            return response()->json(['respuesta' => 'exito','prod_inst_id' => $prod_inst_id], 200);
        }}
            else
                return response()->json(['respuesta' => 'error'], 200);
//        }
//        else
//            return response()->json(['respuesta' => 'error'], 200);
        //return redirect()->route('intranetdocente.notas.index');
    }


    public function update(Request $request, $id)
    {

        /* $nota = Evaluacion::find($id);
        $nota->nota = $request->notaEdit;
        $nota->fecha = $request->fechaEdit;

        $nota->save();
        return redirect()->route('intranetdocente.notas.index'); */
        //return redirect()->route('silabos.index'.$silabo->id);
    }


    public function getNota(Request $request)
    {

        $nota = Nota::find($request->id);
        //echo"hola";
        return response()->json(['nota' => $nota], 200);
    }

    public function delete(Request $request, $id)
    {
        //if ($request->wantsJson()) {

            //Nota::where('producto_instrumento_id', $id)->delete();
             $pi = ProductoInstrumento::where('id',$id)->first();
             $p_id = $pi->producto_id;
             $i_id = $pi->instrumento_id;
             DB::table('producto_instrumentos')->delete($id);
             $p = Producto::where('id',$p_id)->where('instrumento_id',$i_id)->first();
             DB::table('productos')->delete($p_id);
             //$p->delete();
             //$p->save();
             $consnotas = Nota::where('producto_instrumento_id',$id)->get();
        //return response()->json(['respuesta' => $consnotas ], 200);
             foreach($consnotas as $nota)
              {
                 $nota->delete();
              }



             //$p = Producto::where('id',$p_id)->first();

                //$consnotas->truncate();
                 //$consnotas->save();


            return response()->json(['respuesta' => 'exitoso'], 200);
        //}
        //return redirect()->route('intranetdocente.notas.index');
    }

    public function get_notasAlumnos(Request $request)
    {
        //return response()->json(['sw' => $request], 200);
        $Producto = Producto::where('seccion_id',$request->sec_id)->where('instrumento_id', $request->categoria_id)->where('evaluacion_categoria_id', $request->bimestre)->first();
        //return response()->json(['sw' => $Producto], 200);
        if(isset($Producto)) {
            $Prod_instr = ProductoInstrumento::where('producto_id', $Producto->id)->first();
            if (isset($Prod_instr)) {
                $Proinst = $Prod_instr->id;
                $listanotasalum = Nota::with(['alumno.persona'])
                    ->where('producto_instrumento_id', $Prod_instr->id)->get();
                $sorted = $listanotasalum->sortBy('alumno.persona.paterno');


                return response()->json(['listanotasalum' => $sorted->values()->all(), 'sw' => 1, 'prod_inst_id' => $Proinst], 200);
            } else
                return response()->json(['sw' => 0], 200);
        }
        else
            return response()->json(['sw' => 0], 200);
    }

    public function reporteNotasInstrumneto(Request $request)
    {

        //   if($request->wantsJson()){



        $seccion_id = $request->seccion_id;

        $periodo = Periodo::find($request->periodo_id)->firstOrFail();
        $carrera = Carrera::find($request->carrera_id)->firstOrFail();
        $seccion = Seccion::find($request->seccion_id)->firstOrFail();
        $prod = Producto::where('seccion_id',$request->seccion_id)->where('instrumento_id',$request->prod_inst_id)->first();
        $p_i = ProductoInstrumento::where('producto_id',$prod->id)->first();

        $registroNotas = Nota::with(['alumno.persona'])
            ->where('producto_instrumento_id', $p_i->id)
            ->get();
        $reg_notas_may = $registroNotas->sortBy('alumno.persona.paterno');
        //dd($registroNotas);
        $curso = PeCurso::with('curso')->where('id',$request->area_id)->firstOrFail();
        //dd($curso->curso->nombre);
        $categoria = EvaluacionCategoria::find($request->cat_id)->firstOrFail();
        //dd($request->prod_inst_id);
        $instrumento = Instrument::find($request->prod_inst_id)->firstOrFail();
        //dd($instrumento);

        //$producto = Producto::find($request->prod_id)->firstOrFail();

        //$instrumento = ProductoInstrumento::with(['instrumentos', 'contenidos'])
        //    ->where('id', $request->prod_inst_id)->firstOrFail();

//        $unidad = unidades::with(['contenidos'])
//            ->whereHas('contenidos', function ($query) use ($instrumento) {
//                $query->where('contenidos.id', $instrumento->contenidos->id);
//            })->firstOrFail();
        // ->where('contenidos.id',$instrumento->contenidos->id)->firstOrFail();;
        //dd($instrumento);
        $docente = Auth::user()->persona;
        //dd($docente);

        $data = ['registroNotas' => $reg_notas_may,
            'curso' => $curso,
            'docente' => $docente,
            'periodo' => $periodo,
            'seccion' => $seccion,
            'carrera' => $carrera,
            'categoria' => $categoria,
            //'producto' => $producto,
            'instrumento' => $instrumento];
            //'unidad' => $unidad];
        //dd($data);
        $pdf = PDF::loadView('IntranetDocente.Nota.reporteNotasInstrumento', $data);
        return $pdf->stream();


        //return response()->json(['registroAsistencia'=>$v],200);
        //  }

    }

    public function getNotaCursoAlumno()
    {
        $alumno = Auth::user()->persona->alumno;
        $alumno_id = $alumno->id;
        $periodo_id = Periodo::where('estado', 1)->first()->id;

        $detalles = MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno', function ($q) use ($alumno_id) {
            $q->where('id', $alumno_id);
        })->whereHas('matricula', function ($q) use ($periodo_id) {
            $q->where('periodo_id', $periodo_id);

        })
            ->get();
        return view('IntranetAlumno.notas.index', ['detalles' => $detalles]);
    }

    public function ver_notas_alumnos($action = '')
    {
        $alumno = Auth::user()->persona->alumno;
        $periodo = Periodo::where('estado', 1)->first();
        $categoria = EvaluacionCategoria::all();
        $instrumentos = Instrument::all();
        //dd($categoria);
        $carrera_id= $alumno->alumnocarreras[0]->carrera_id;
        $carrera= Carrera::where('id',$carrera_id)->get();
        $matricula_detalle = MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno', function ($q) use ($alumno) {
            $q->where('id', $alumno->id);
        })->whereHas('matricula', function ($q) use ($periodo) {
            $q->where('periodo_id', $periodo->id);
        })->get();
        //dd($matricula_detalle);

//        if ($action == '') {
            return view('IntranetAlumno.notas.index', ['matricula_detalles' => $matricula_detalle, 'categorias' => $categoria,'instrumentos'=>$instrumentos,'blade'=>'true']);
//        }


//        if ($action == 'pdf') {
//            $data = [
//                'matricula_detalles' => $matricula_detalle,
//                'categorias' => $categoria,
//                'alumno'=>$alumno,
//                'periodo'=>$periodo,
//                'carrera'=>$carrera,
//            ];
////            return view('IntranetAlumno.notas.rpt_notas',$data);
////            $pdf = MPDF::loadView('IntranetAlumno.notas.rpt_notas', $data);
////            return $pdf->stream();
//            $pdf = PDF::loadview('IntranetAlumno.notas.rpt_notas', $data)->setPaper('a4', 'landscape');
//            return $pdf->stream('Notas.pdf');
//        }
    }

    public function ver_RecordNotas()
    {
        $alumno = Auth::user()->persona->alumno;
        $periodo = Periodo::where('estado', 1)->first();
        $categoria = EvaluacionCategoria::all();
        $carrera= Carrera::where('id',$alumno->alumnocarreras[0]->carrera_id)->get();
        $plan= PlanEstudio::where('id',$alumno->pe_carreras->first()->plan_estudio_id)->get();
        $cursos= Curso::all()->sortBy('pe_cursos.semestre');

        $matricula_detalle = MatriculaDetalle::whereHas('matricula.alumnocarrera.alumno', function ($q) use ($alumno) {
            $q->where('id', $alumno->id);
        })->get();


        return view('IntranetAlumno.notas.record_notas',['matricula_detalle'=>$matricula_detalle,'plan'=>$plan,'cursos'=>$cursos]);
    }
    public function ver_notas_alumnos_b($periodo,$area,$bimestre)
    {

        //dd($request);
        //$periodo = Periodo::find($request->periodo);
        $alumno = Auth::user()->persona->alumno;
        $periodo = Periodo::where('estado', 1)->first();
        $categoria = EvaluacionCategoria::all();
        $instrumentos = Instrument::all();
        //dd($categoria);

        //return response()->json(['alumno'=>$area],200);

            $matricula_detalle = MatriculaDetalle::with('seccion.productos.notas', 'seccion.pe_curso.curso')->whereHas('matricula.alumnocarrera.alumno', function ($q) use ($alumno) {
                $q->where('id', $alumno->id);
            })->whereHas('matricula', function ($q) use ($periodo) {
                $q->where('periodo_id', $periodo->id);
            })->whereHas('seccion.productos.evaluacion_categoria', function ($q) use ($bimestre) {
                $q->where('id', $bimestre);
            })->whereHas('seccion.productos.producto_instrumentos.instrumentos', function ($q) use ($area) {
                $q->where('competencia_id', $area);
            })->whereHas('seccion.productos.notas', function ($q) use ($alumno) {
                $q->where('alumno_id', $alumno->id);
            })->get();

            $notas = Nota::with('producto_instrumento.nota', 'producto_instrumento.instrumentos', 'producto_instrumento.productos.seccion.pe_curso.curso')
                ->whereHas('producto_instrumento.productos.evaluacion_categoria', function ($q) use ($bimestre) {
                    $q->where('id', $bimestre);
                })->whereHas('producto_instrumento.instrumentos', function ($q) use ($area) {
                    $q->where('competencia_id', $area);
                })
                ->where('alumno_id', $alumno->id)->get();
//$productos = $instrumentos->producto_instrumento->get();
            return response()->json(['md' => $matricula_detalle,'area'=>$area, 'bimestre'=> $bimestre,'alumno_id' => $alumno->id, 'notas' => $notas, 'instrumentos' => $instrumentos],200);

        //return response()->json(['md'=>$matricula_detalle,'instrumentos'=>$instrumentos], 200);
    }

}
