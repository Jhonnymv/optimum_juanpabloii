<?php

namespace App\Http\Controllers;

use App\BpCategoria;
use App\BpPregunta;
use App\Carrera;
use App\EalDetalles;
use App\Examen;
use App\ExamenAlumno;
use App\Periodo;
use App\Postulacion;
use App\Postulante;
use Carbon\Traits\Creator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

//use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PhpParser\Node\Stmt\DeclareDeclare;

class ExamenController extends Controller
{
    public function create()
    {
        $catedorias = BpCategoria::all();
        $periodo = Periodo::where('estado', 1)->first();
        return view('Admisiones.Banco_Preguntas.generar_examen', [
            'categorias' => $catedorias,
            'periodo' => $periodo,
        ]);
    }

    public function generar(Request $request)
    {

        $preguntas_cat_1 = BpPregunta::with('bpAlternativas')->where('bp_categoria_id', 1)->get()->random($request->cat_1);
        $preguntas_cat_2 = BpPregunta::with('bpAlternativas')->where('bp_categoria_id', 2)->get()->random($request->cat_2);
        $preguntas_cat_3 = BpPregunta::with('bpAlternativas')->where('bp_categoria_id', 3)->get()->random($request->cat_3);

        return response()->json(['pre_cat_1' => $preguntas_cat_1, 'pre_cat_2' => $preguntas_cat_2, 'pre_cat_3' => $preguntas_cat_3], 200);
    }

    public function store(Request $request)
    {

        $examenes = Examen::where('periodo_id', $request->periodo_id)->get();
        if (count($examenes) === 0) {
            DB::beginTransaction();
            $examen = new Examen();
            $examen->periodo_id = $request->periodo_id;
            $examen->fecha_hora = $request->fecha_hora;
            $examen->duracion = $request->duracion;
            $examen->save();

            foreach ($request->pregunta_id as $pregunta_id) {
                DB::table('examen_preguntas')->insert([
                    'examen_id' => $examen->id,
                    'bp_pregunta_id' => $pregunta_id
                ]);
            }
            DB::commit();
            return back();
        }
        return back();
    }

    public function index()
    {
        $postulante = Auth::user()->persona->postulante;
        $examen = Examen::with('periodo')->whereHas('periodo', function ($q) {
            $q->where('estado', 1);
        })->where('estado',1)->first();
        if ($examen) {
            $hora = explode(':', $examen->duracion);
            $fecha_fin = new Carbon($examen->fecha_hora);
            $fecha_fin->add($hora[0], 'hour');
            $fecha_fin->add($hora[1], 'minute');

            $f = new Carbon($fecha_fin);
            $alerta = $f->subtract('20', 'minute');

            $f_10 = new Carbon($fecha_fin);
            $alerta_2 = $f_10->subtract('10', 'minute');

            $ep = ExamenAlumno::where([['postulante_id', $postulante->id], ['examen_id', $examen->id]])->first();
            if (!$ep)
                return view('Postulantes.examen', ['examen' => $examen, 'fecha_fin' => $fecha_fin, 'alerta' => $alerta, 'alerta_2' => $alerta_2]);
            else
                return back();
        } else {
            return back();
        }

    }
    public function resumen()
    {
        $postulante = Auth::user()->persona->postulante;
        $examen = Examen::with('periodo')->whereHas('periodo', function ($q) {
            $q->where('estado', 1);
        })->where('estado',1)->first();
        if ($examen) {
            $hora = explode(':', $examen->duracion);
            $fecha_fin = new Carbon($examen->fecha_hora);
            $fecha_fin->add($hora[0], 'hour');
            $fecha_fin->add($hora[1], 'minute');

            $ep = ExamenAlumno::where([['postulante_id', $postulante->id], ['examen_id', $examen->id]])->first();
            //$preguntas = BpPregunta::with('bpAlternativas')where();
            //$respuestas = EalDetalles::where('examen_alumno_id',$ep->id);
            if (!$ep)
                return view('Postulantes.examen', ['examen' => $examen, 'fecha_fin' => $fecha_fin]);
            else
                return back();
        } else {
            return back();
        }

    }

    public function v_index()
    {
        $examenes = Examen::with('periodo')->whereHas('periodo', function ($q) {
            $q->where('estado', '1');
        })->where('estado', '1')->get();
        return view('Admisiones.examenes.Evaluar_examen', ['examenes' => $examenes]);


    }

    public function evaluar(Request $request)
    {
        try {

            $examen = Examen::find($request->examen_id);
            $claves = [];
            foreach ($examen->preguntas as $pregunta) {
//            echo 'categoría: '.$pregunta->categoria->categoria."<br>";
//            echo 'pregunta: ' . $pregunta->pregunta . "<br>";
                foreach ($pregunta->bpalternativas as $alternativa) {
                    if ($alternativa->valor == '1') {

//                    echo 'alternativa: ' . $alternativa->alternativa . "<br><br>";

                        $claves += array($alternativa->bp_pregunta_id => ['pregunta_id' => $alternativa->bp_pregunta_id, 'clave_id' => $alternativa->id]);
                    }
                }
            }
            $examen_postulantes = ExamenAlumno::where('examen_id', $request->examen_id)->get();
            foreach ($examen_postulantes as $ex_pos) {
                $pun_mat = 0;
                $pun_comu = 0;
                $pun_cultura = 0;
                foreach ($ex_pos->ep_detalles as $e_detalle) {
                    $clave = $claves[$e_detalle->pregunta_id];
                    if ($e_detalle->alternativa_id == $clave['clave_id']) {
                        $pregunta = BpPregunta::find($e_detalle->pregunta_id);
                        switch ($pregunta->bp_categoria_id) {
                            case 1:
                                $pun_mat += 1;
                                break;
                            case 2:
                                $pun_comu += 1;
                                break;
                            case 3:
                                $pun_cultura += 1;
                                break;
                        }
                    }
                }
                $punt_mate_f = $pun_mat / 5;
                $punt_comu_f = $pun_comu / 5;
                $punt_cultura_f = $pun_cultura / 5;
                $postulacion = Postulacion::where('postulante_id', $ex_pos->postulante_id)->first();
                $postulacion->puntaje_mate = $punt_mate_f;
                $postulacion->puntaje_comu = $punt_comu_f;
                $postulacion->puntaje_cultura = $punt_cultura_f;
                $postulacion->save();
            }
            $examenes = Examen::with('periodo')->whereHas('periodo', function ($q) {
                $q->where('estado', '1');
            })->where('estado', '1')->get();
            return view('Admisiones.examenes.Evaluar_examen', ['postulaciones' => Postulacion::all(), 'examenes' => $examenes]);
        } catch (\Exception $e) {
            $examenes = Examen::with('periodo')->whereHas('periodo', function ($q) {
                $q->where('estado', '1');
            })->where('estado', '1')->get();
            return view('Admisiones.examenes.Evaluar_examen', ['msj' => 'Asegúrese que todas las preguntas tengan su alternativa correcta', 'examenes' => $examenes]);
        }

    }
}
