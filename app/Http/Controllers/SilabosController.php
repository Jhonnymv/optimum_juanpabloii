<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Carrera;
use App\Dimension;
use App\Seccion;
use App\Docente;
use App\silabo;
use App\Indicador;
use App\unidades;
use App\Contenido;
use App\Desempeno_criterio;
use App\Areas_academica;
use App\Areas_Academicas_Docente;

use Auth;
use DB;
use Illuminate\Support\Facades\Storage;
use PDF;
use MPDF;
use App\Periodo;
use App\silabo_docente;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\DeclareDeclare;
use Symfony\Component\Console\Logger\ConsoleLogger;

class SilabosController extends Controller
{

    public function index()
    {
        // return 1;

        $periodos = Periodo::all();
        $responsable_area_academica_id = Auth::user()->persona->docente->id;

        //dd($responsable_area_academica_id);
        $carreras = Carrera::whereHas('area_academica', function ($query) use ($responsable_area_academica_id) {
            $query->where('docente_id', $responsable_area_academica_id)->where('estado_gestion', 1);
        })
            ->get();
        // $id_are_ac=[];
        // $docente=Docente::find($responsable_area_academica_id);

        // foreach($docente->area_academica_docentes as $ar){
        //     // Arr::add($id_are_ac,$ar->areas_academica_id);
        //     array_push($id_are_ac,$ar->areas_academica_id);
        // }
        // $carreras=Carrera::whereIn('areas_academica_id',$id_are_ac)->get();
        // dd($carreras);

        return view('IntranetAreaAcademica.Silabos.index')->with('periodos', $periodos)->with('carreras', $carreras);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$datos['silabos'] = silabos::where('estado','=','1')->get();
        //$datos['silabos'] =silabos :: paginate(5);
        //return view('silabos.index', $datos);

        //$silabos = silabos::paginate(5);
        //$silabos = silabos::find($silabo_id);
        /*if($silabo_id == null){
            $silabos = silabo::paginate(5);
        }
        else
        {
            $silabos = silabo::where('id','=',$silabo_id)->get();
        }*/


        //$silabos = silabos::where('id','=',$silabo_id)->get();

        //$silabos_docentes = silabo_docente::paginate(5);
        //$cursos = Curso::where('estado','=','1')->get();

        $periodosAcademicos = Periodo::where('estado', 1)->get();
        $dimensiones = Dimension::all();
        return view('IntranetDocente.Silabos.create', [
            //'silabos' => $silabos,
            //'cursos' => $cursos,
            'dimensiones' => $dimensiones,
            'periodosAcademicos' => $periodosAcademicos
        ]);

    }


    public function store(Request $request)
    {

        // $url_document=$request->url_document;
        // $subs_position=strpos($url_document, "/view");
        // if($subs_position){
        //     $url_document=substr($url_document, 0, $subs_position).'/preview';
        // }

        $silabo = new silabo();
        $silabo->seccion_id = $request->seccion_id;
        $silabo->tipo = $request->tipo;

        if ($request->tipo == 'archivo') {
            if ($request->file('file')) {
                $name_archivo = $request->seccion_id . '.' . $request->file('file')->getClientOriginalExtension();
                $request->file('file')->storeAs('public/silabos/' . Auth::user()->persona->DNI . '/', $name_archivo);
                $patch = Storage::url('silabos/' . Auth::user()->persona->DNI . '/' . $name_archivo);
                $silabo->url_silabo = $patch;
                $silabo->estado = 1;
            }
        }

        else {
            $silabo->fundamentacion = $request->fundamentacion;
            $silabo->proyecto_institucional = $request->proyecto_institucional;
            $silabo->bibliografia = $request->bibliografia;
            $silabo->estado = 1;

            if ($silabo->tipo == '2019') {
                $silabo->temas_transversales = $request->temas_transversales;
            }

            if ($silabo->tipo == '2020') {
                $silabo->enfoque_derecho_1 = $request->enfoque_derecho_1;
                $silabo->enfoque_derecho_2 = $request->enfoque_derecho_2;
                $silabo->enfoque_derecho_3 = $request->enfoque_derecho_3;
                $silabo->enfoque_intercultural_1 = $request->enfoque_intercultural_1;
                $silabo->enfoque_intercultural_2 = $request->enfoque_intercultural_2;
                $silabo->enfoque_intercultural_3 = $request->enfoque_intercultural_3;
                $silabo->competencias = $request->competencias;
                $silabo->capacidades = $request->capacidades;
                $silabo->estandar = $request->estandar;
                $silabo->metodologia = $request->metodologia;
            }
        }

        $silabo->save();


        ////// ATTACH INDICADORES EN TABLA lista_verificacion//////////
        $indicadores = Indicador::all();
        foreach ($indicadores as $key => $indicador) {
            $attach[$indicador->id] = ['escala' => 0];
        }


        $silabo->lista_verificacion()->attach($attach);
        /////////////////////////////////////////////////////////////////

        return ['success' => true, 'silabo' => $silabo];
         // return 0;
    }

    public function attach_inidcadores(Request $request)
    {

        $silabo = silabo::find($request->id);
        $indicadores = Indicador::all();
        foreach ($indicadores as $key => $indicador) {
            $attach[$indicador->id] = ['escala' => 0];
        }

        $silabo->lista_verificacion()->attach($attach);

        return ['success' => true];
    }

    public function _store(Request $request)
    {
        // $validar = $request->validate([
        //     'silabocol' => 'required|unique:carreras|max:150',
        //     'referencias_bib' =>  'required|max:200',
        // ]);

        // dd($request);

        $silabo = new silabo([
            'pe_curso_id' => $request->pe_curso_id,
            'periodo_id' => $request->periodo_id,
            'fundamentacion' => $request->fundamentacion,
            'sumilla' => $request->sumilla,
            'competencias' => $request->competencias,
            'sistema_eval' => $request->sistema_eval,
            'condiciones_aprobacion' => $request->condiciones_aprobacion,
            'referencias_bib' => $request->referencias_bib,
            'estado' => 1
        ]);

        $silabo->save();


        return redirect()->route('IntranetDocente.silabos.create');
    }


    public function update(Request $request)
    {

        $silabo = silabo::find($request->id);

        $silabo->seccion_id = $request->seccion_id;
        $silabo->tipo = $request->tipo;
        if ($request->tipo == 'archivo') {
            $name_archivo = $request->seccion_id.'.'. $request->file->getClientOriginalExtension();

            $request->file->storeAs('public/silabos/' . Auth::user()->persona->DNI . '/', $name_archivo);
            $patch = Storage::url('silabos/' . Auth::user()->persona->DNI . '/', $name_archivo);
            $silabo->url_silabo = $patch;
            $silabo->estado = 1;
        } else {
            $silabo->fundamentacion = $request->fundamentacion;
            $silabo->proyecto_institucional = $request->proyecto_institucional;
            $silabo->bibliografia = $request->bibliografia;
            // $silabo->estado = 1;

            if ($silabo->tipo == '2019') {
                $silabo->temas_transversales = $request->temas_transversales;
            }

            if ($silabo->tipo == '2020') {
                $silabo->enfoque_derecho_1 = $request->enfoque_derecho_1;
                $silabo->enfoque_derecho_2 = $request->enfoque_derecho_2;
                $silabo->enfoque_derecho_3 = $request->enfoque_derecho_3;
                $silabo->enfoque_intercultural_1 = $request->enfoque_intercultural_1;
                $silabo->enfoque_intercultural_2 = $request->enfoque_intercultural_2;
                $silabo->enfoque_intercultural_3 = $request->enfoque_intercultural_3;
                $silabo->competencias = $request->competencias;
                $silabo->capacidades = $request->capacidades;
                $silabo->estandar = $request->estandar;
                $silabo->metodologia = $request->metodologia;
            }
        }
        $silabo->save();

        return ['success' => true, 'silabo' => $silabo];

        // $silabo->silaboscol = $request->silabocoledit;
        // $silabo->curso_id = $request->cursoedit;
        // $silabo->periodo_academico_id = $request->periodoacademicoedit;
        // $silabo->horas_teoria = $request->horas_teoriaedit;
        // $silabo->horas_practica = $request->horas_practicaedit;
        // $silabo->condiciones = $request->condicionesedit;
        // $silabo->referencias_bib = $request->referencias_bibedit;
        // $silabo->referencias_bib = $request->criterios_evaluacionedit;
        // $silabo->save();

        // return redirect()->route('silabos.index');
    }

    public function show($id)
    {

        $silabo = Silabo::with([
            'unidades.contenidos',
            'unidades.desempeno_criterios',
            'seccion.pe_curso.curso.carrera',

            //'seccion.productos.producto_instumentos.instrumentos',

            'seccion.periodo',
            'seccion.docentes.persona'
        ])
            ->whereId($id)
            ->firstOrFail();

        //dd($silabo);
        // $indicadores=Indicador::all();

        // return  $silabo;


        return view('IntranetAreaAcademica.Silabos.detalle')->with('silabo', $silabo);
        // ->with('indicadores',$indicadores);
    }


    public function destroy(silabo $silabo)
    {
        //
    }

    public function silabo_pdf_download(Request $request)
    {
        $data = $this->getDataSilaboPdf($request);
        $pdf = MPDF::loadView('IntranetDocente.Silabos.silabo_pdf_template', $data);
        return $pdf->download('Silabo.pdf');
    }

    public function silabo_pdf(Request $request)
    {

        $data = $this->getDataSilaboPdf($request);

        // return view('IntranetDocente.Silabos.silabo_pdf_template', $data);

        $pdf = MPDF::loadView('IntranetDocente.Silabos.silabo_pdf_template', $data);
        return $pdf->stream();


    }

    public function getDataSilaboPdf($request)
    {
        $silabo = silabo::with([
            'unidades.contenidos',
            'unidades.desempeno_criterios',
            // 'seccion.pe_curso.curso.carrera',
            // 'seccion.pe_curso.curso.carrera.area_academica.docentes.persona',
            'seccion.pe_curso.curso.carrera.responsablesCarrera.persona',
            'seccion.periodo',
            'seccion.docentes.persona',
            'seccion.productos.producto_instrumentos.instrumentos',
            //'seccion.productos.evaluacion_categoria',
        ])
            ->whereId($request->id)
            // ->whereHas('seccion.pe_curso.curso.carrera.area_academica.docentes',function ($query){
            //     $query->where('estado_gestion',1);
            // })
            ->whereHas('seccion.pe_curso.curso.carrera.responsablesCarrera', function ($query) {
                $query->where('estado_gestion', 1);
            })
            ->firstOrFail();

        $area_practica_investigacion = Areas_academica::with('docentes')
            ->whereHas('docentes', function ($query) {
                $query->where('estado_gestion', 1);
            })
            ->whereId(3)
            ->firstOrFail();
        // return $silabo;

        return ['silabo' => $silabo, 'area_practica_investigacion' => $area_practica_investigacion];
    }

    public function publicar(Request $request)
    {
        $silabo = silabo::find($request->silabo_id);
        $silabo->estado = 3;
        $silabo->save();
        return ['success' => true];
    }


    public function copiar_silabo(Request $request)
    {


        $silabo_origen_id = $request->silabo_origen_id;
        $silabo_destino_id = $request->silabo_destino_id;

        $silabo_origen = silabo::with([
            'unidades.contenidos',
            'unidades.desempeno_criterios'
        ])
            ->whereId($silabo_origen_id)
            ->firstOrFail();

        $silabo_destino = silabo::find($silabo_destino_id);

        DB::beginTransaction();

        try {

            $silabo_destino->tipo = $silabo_origen->tipo;
            $silabo_destino->fundamentacion = $silabo_origen->fundamentacion;
            $silabo_destino->proyecto_institucional = $silabo_origen->proyecto_institucional;
            $silabo_destino->bibliografia = $silabo_origen->bibliografia;
            $silabo_destino->estado = 1;


            $silabo_destino->temas_transversales = $silabo_origen->temas_transversales;

            $silabo_destino->enfoque_derecho_1 = $silabo_origen->enfoque_derecho_1;
            $silabo_destino->enfoque_derecho_2 = $silabo_origen->enfoque_derecho_2;
            $silabo_destino->enfoque_derecho_3 = $silabo_origen->enfoque_derecho_3;
            $silabo_destino->enfoque_intercultural_1 = $silabo_origen->enfoque_intercultural_1;
            $silabo_destino->enfoque_intercultural_2 = $silabo_origen->enfoque_intercultural_2;
            $silabo_destino->enfoque_intercultural_3 = $silabo_origen->enfoque_intercultural_3;
            $silabo_destino->competencias = $silabo_origen->competencias;
            $silabo_destino->capacidades = $silabo_origen->capacidades;
            $silabo_destino->estandar = $silabo_origen->estandar;
            $silabo_destino->metodologia = $silabo_origen->metodologias;


            $silabo_destino->save();


            unidades::where('silabo_id', $silabo_destino->id)->delete();


            foreach ($silabo_origen->unidades as $key_u => $unidad) {

                $unidad_destino = new unidades();
                $unidad_destino->silabo_id = $silabo_destino->id;

                $unidad_destino->denominacion = $unidad->denominacion;
                $unidad_destino->unidad = $unidad->unidad;
                $unidad_destino->fecha_inicio = $unidad->fecha_inicio;
                $unidad_destino->fecha_fin = $unidad->fecha_fin;

                $unidad_destino->save();

                // $silabo_destino->unidades->delete();

                foreach ($unidad->contenidos as $key_c => $contenido) {
                    $contenido_destino = new Contenido();
                    $contenido_destino->unidades_id = $unidad_destino->id;
                    $contenido_destino->nro_semana = $contenido->nro_semana;
                    $contenido_destino->fecha_inicio = $contenido->fecha_inicio;
                    $contenido_destino->fecha_fin = $contenido->fecha_fin;
                    $contenido_destino->indicadores_desempeno = $contenido->indicadores_desempeno;
                    $contenido_destino->capacidades = $contenido->capacidades;
                    $contenido_destino->estrategias = $contenido->estrategias;
                    $contenido_destino->instrumentos = $contenido->instrumentos;
                    $contenido_destino->conocimientos = $contenido->conocimientos;
                    $contenido_destino->producto_evidencias = $contenido->producto_evidencias;

                    $contenido_destino->save();

                }
                foreach ($unidad->desempeno_criterios as $key_dc => $criterio) {
                    $dc_destino = new Desempeno_criterio();
                    $dc_destino->unidades_id = $unidad_destino->id;

                    $dc_destino->titulo = $criterio->titulo;
                    $dc_destino->descripcion = $criterio->descripcion;

                    $dc_destino->save();
                }
            }

            DB::commit();

            return ['success' => true, 'silabo' => $silabo_destino];

        } catch (\Exception $e) {
            DB::rollback();
            // return $e;
            return ['success' => false];
        }
    }

    public function getSilabo(Request $request)
    {

        $silabo = silabo::find($request->id);
        //echo"hola";
        return response()->json(['silabo' => $silabo], 200);
    }

    public function modalContentListaVerificacion(Request $request)
    {
        $silabo = Silabo::with([
            'lista_verificacion'
        ])
            ->whereId($request->id)
            ->firstOrFail();

        return view('IntranetAreaAcademica.Silabos.modalContentListaVerificacionPartial')->with('silabo', $silabo);

    }

    public function update_lista_verificacion(Request $request)
    {
        $silabo = silabo::find($request->silabo_id);


        // dd($request->indicador_1);

        $indicadores = Indicador::all();
        foreach ($indicadores as $key => $indicador) {

            $escala = $request->get('indicador_' . $indicador->id);
            $detalles = $request->get('detalles_' . $indicador->id);
            $attach[$indicador->id] = ['escala' => ($escala == 'si' ? 1 : 0), 'detalles' => $detalles];
        }

        $silabo->lista_verificacion()->sync($attach);

        $silabo->estado = 1;
        $silabo->save();
        return redirect()->back();
//         $silabo->lista_verificacion()->updateExistingPivot($roleId, $attributes);

    }

    public function cambiar_estado(Request $request)
    {
        $silabo = silabo::find($request->silabo_id);
        $silabo->estado = $request->estado;
        $silabo->save();
    }

}
