<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contenido;

class ContenidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $semana=new Contenido();
        $semana->unidades_id=$request->unidades_id;
        $semana->nro_semana=$request->nro_semana;
        $semana->fecha_inicio=$request->fecha_inicio;
        $semana->fecha_fin=$request->fecha_fin;


        $semana->indicadores_desempeno=$request->indicadores_desempeno;
        $semana->capacidades='';
        $semana->estrategias=$request->estrategias;
        $semana->instrumentos=$request->instrumentos;
        $semana->conocimientos=$request->conocimientos;
        $semana->producto_evidencias=$request->producto_evidencias;

        $semana->save();

        return ['success'=>true,'semana'=>$semana];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return Contenido::find($request->semana_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $semana=Contenido::find($request->semana_id);
        $semana->unidades_id=$request->unidades_id;
        $semana->nro_semana=$request->nro_semana;
        $semana->fecha_inicio=$request->fecha_inicio;
        $semana->fecha_fin=$request->fecha_fin;


        $semana->indicadores_desempeno=$request->indicadores_desempeno;
        $semana->capacidades='';
        $semana->estrategias=$request->estrategias;
        $semana->instrumentos=$request->instrumentos;
        $semana->conocimientos=$request->conocimientos;
        $semana->producto_evidencias=$request->producto_evidencias;

        $semana->save();

        return ['success'=>true,'semana'=>$semana];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Contenido::destroy($request->semana_id);

        return ['success'=>true];
    }
}
