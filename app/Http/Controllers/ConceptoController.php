<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Concepto;
use App\Periodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConceptoController extends Controller
{
    //
    public $timestamps = false;
    public function index()
    {
        $conceptos = Concepto::all();
        $carreras= Carrera::all();
        $periodos=Periodo::all();
        return view('Admin.Pagos.conceptosPagos',['conceptos'=>$conceptos,'periodos'=>$periodos,'carreras'=>$carreras]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $concepto = new Concepto();
        $concepto->nombre = $request->nombre;
        $concepto->periodo_id = $request->periodo_id;
        $concepto->carrera_id = $request->carrera_id;
        $concepto->precio = $request->precio;
        $concepto->estado = 1;
        $concepto->observaciones = $request->observaciones;
        $concepto->save();
        DB::commit();
        return redirect()->back();
    }

    public function edit($id)
    {
        $concepto = Concepto::where('id', $id)->first();
        return response()->json(['concepto' => $concepto], 200);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        $concepto = Concepto::find($request->idConcepto);
        $concepto->nombre = $request->nombreEditar;
        $concepto->periodo_id = $request->periodo_idEditar;
        $concepto->carrera_id = $request->carrera_idEditar;
        $concepto->precio = $request->precioEditar;
        $concepto->observaciones = $request->observacionesEditar;
        $concepto->save();
        DB::commit();
        return redirect()->back();

    }
}
