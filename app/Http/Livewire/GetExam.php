<?php

namespace App\Http\Livewire;

use App\BpPregunta;
use App\Curso;
use App\Examen;
use App\Periodo;
use Carbon\Carbon;
use Livewire\Component;

class GetExam extends Component
{
    public $periods, $perioId, $months, $month,$cursos,$exam;

    public function mount()
    {
//        $this->periods = Periodo::where('estado','1')->get();
//        $this->perioId = $this->periods->first()->id;
        $this->months = collect();
        for ($i = 1; $i <= 12; $i++) {
            $now=Carbon::now();
            $date=Carbon::parse($now->year.'-'.$i.'-01');
            $dato = [
                'month'=>$date->month,
                'monthName'=>$date->monthName,
                'year'=>$date->year
            ];
            $this->months->push($dato);
        }
        $this->month=$this->months[0]['month'];
    }
    public function render()
    {
        return view('livewire.get-exam');
    }

    public function searchExam()
    {
//        $this->cursos=Curso::with('preguntas')->whereNotNull('preg_simulacro')->get();
        $this->exam=Examen::with('periodo')->where('mes_ano',$this->month)->first();
    }
}
