<?php

namespace App\Http\Livewire;

use App\Pago;
use App\Persona;
use App\Sequence;
use App\Traits\SendNoteCredTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use App\Alumnopago;

class NotesCreDev extends Component
{
    use SendNoteCredTrait;

    public $noteType, $docAfec, $pago_id;
    public $tipoDoc, $serie, $correlativo, $cliTypeDoc, $cliDoc, $cliNombre, $detalles,$getPago;

    public function mount()
    {
        $this->noteType = '07';
        $this->detalles = collect();
    }

    public function render()
    {
        return view('livewire.notes-cre-dev');
    }

    public function searchComp()
    {
//        if ($this->tDocAfec == '01') {
        $docexplode = explode('-', $this->docAfec);
        //dd($docexplode);
        $serie = $docexplode[0];
        $number = $docexplode[1];
        $pago = Pago::where([['serie', $serie], ['numero_doc', $number]])->first();
//        }
//        if ($this->tDocAfec == '03') {
//            $serie = Sequence::where([['doc_type', '03'], ['serie', 'like', 'B' . '%'], ['status', 1]])->first()->serie;
//        }
        if ($pago) {
            $this->getPago($pago->id);
           // $this->tipoDoc = 'DNI';
        }
    }

    public function clearInputs()
    {
        $this->noteType = null;
        $this->pago_id = null;
        $this->tipoDoc = null;
        $this->serie = null;
        $this->correlativo = null;
        $this->cliTypeDoc = null;
        $this->cliDoc = null;
        $this->cliNombre = null;
        $this->detalles = collect();
    }

    public function getPago($pagoId)
    {
        $pago = Pago::find($pagoId);
        $this->pago_id = $pagoId;
        $this->tipoDoc = $pago->tipo_doc == '01' ? 'Factura' : 'Boleta';
        $this->serie = $pago->serie;
        $this->correlativo = $pago->numero_doc;
        $this->cliTypeDoc = 'DNI';
        $this->cliDoc = $pago->persona->DNI;
        $this->cliNombre = strtoupper($pago->persona->paterno . ' ' . $pago->persona->materno . ', ' . $pago->persona->nombres);
        $this->detalles = collect();
        foreach ($pago->alumnos as $loop => $alumno) {
            //dd($alumno->pivot);
            $data = [
                'orden' => $loop + 1,
                'detalle' => $alumno->pivot->fe_descripcion,
                'alumno_id' => $alumno->pivot->alumno_id,
                'precio' => $alumno->pivot->fe_precio_und,
                'cantidad' => $alumno->pivot->cantidad,
                'total' => $alumno->pivot->fe_valor_venta,
                'cronograma' => $alumno->pivot->cronograma_id,
            ];
            $this->detalles->push($data);
        }
        $this->getPago=$pago;

    }

    public function sendNote()
    {
        $docexplode = explode('-', $this->docAfec);
        //dd($docexplode);
        $serie = $docexplode[0];
        $number = $docexplode[1];
        $pago_b = Pago::where([['serie', $serie], ['numero_doc', $number]])->first();
        //dd($pago_b->fe_referencia_id);
        //dd(empty($pago_b->fe_referencia_id));
        if(empty($pago_b->fe_referencia_id)==false){
            session()->flash('message_danger', 'Nota de crédito ya existe.....');
        }
        else {
            DB::beginTransaction();
            $pago = new Pago();
            $pago->persona_id = $this->getPago->persona_id;
            /// nota de credito acá .......
            $pago->tipo_doc = '07';

            if ($this->getPago->tipo_doc == '01') {
                $ruc = $this->getPago->doc_ruc;
                $social_reason = $this->getPago->nombre_empresa;
                $serie = 'FC01';
            } else {
                $ruc = null;
                $social_reason = null;
                $serie = 'BC01';
            }
            $sequence = Sequence::where('doc_type', '07')->where('serie', $serie)->first();
            $sequence->increment('current', 1);
            $pago->ruc = $ruc;
            $pago->social_reason = $social_reason;
            $pago->serie = $serie;
            $pago->numero_doc = (str_pad($sequence->current, 4, "0", STR_PAD_LEFT));
            $pago->estado = 1;
            $pago->fe_sub_total = -1 * ($this->getPago->fe_total_price);
            $pago->fe_igv = 0;
            $pago->fe_total_price = -1 * ($this->getPago->fe_total_price);
            $pago->fe_estado = 'Pendiente factura';
            $pago->fe_referencia_id = $this->getPago->id;
            $pago->observaciones = 'Anulación de Operación';
            $pago->save();
            //dd($this->detalles[0] );
            $pagoid = $pago->id;
            //dd($pago_b);
            $pago_b->fe_referencia_id = $pagoid;
            $pago_b->save();
            $cont = count($this->detalles);
            $alumnoPago = new Alumnopago();
            for ($i = 0; $i < $cont; $i = $i + 1) {
                $alumnoPago = new Alumnopago();
                $alumnoPago->pago_id = $pagoid;
                $alumnoPago->cronograma_id = $this->detalles[$i]['cronograma'];
                $alumnoPago->alumno_id = $this->detalles[$i]['alumno_id'];
                $alumnoPago->cantidad = $this->detalles[$i]['cantidad'];
                //$alumnoPago->fe_descripcion = 'NC. PAGO DE MATRICULA DEL ALUMNO ' . strtoupper($alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ,' . $alumno->persona->nombres);
                $alumnoPago->fe_descripcion = 'NC. ' . $this->detalles[$i]['detalle'];
                $alumnoPago->fe_unidad = '';
                $alumnoPago->fe_precio_und = -1 * $this->detalles[$i]['total'];
                $alumnoPago->fe_igv = 0;
                $alumnoPago->fe_valor_venta = -1 * $this->detalles[$i]['total'];;
                $alumnoPago->estado = 'Pagado';
                $alumnoPago->save();
            }

            DB::commit();
            session()->flash('message', 'Nota de crédito registrada.....');
            $res = $this->sendNoteCred($this->pago_id);
            if ($res['code'] == "0") {
                $pago->update(['status' => $res['code'], 'fe_estado' => '0']);
            } else {
                $pago->update(['status' => $res['code'], 'fe_estado' => '1']);
            }
            //dd($this->sendNoteCred($this->pago_id));
        }
    }

}
