<?php

namespace App\Http\Livewire;

use App\Seccion;
use Livewire\Component;

class CourseDocente extends Component
{
    public $seccionId, $seccion;
    public $programation;

    public function mount()
    {
        $this->seccion=Seccion::find($this->seccionId);
        $this->programation=$this->seccion->programation;
    }
    public function render()
    {
        return view('livewire.course-docente');
    }
}
