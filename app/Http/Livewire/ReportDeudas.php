<?php

namespace App\Http\Livewire;

use App\Alumno;
use App\Alumnopago;
use App\Carrera;
use App\Concepto;
use App\Cronograma;
use App\Grade;
use App\Periodo;
use Livewire\Component;

class ReportDeudas extends Component
{
    public $periods, $periodId, $levels, $levelId, $grades, $gradeId, $section;

    public $data;

    public function mount()
    {
        $this->periods = Periodo::all();
        $this->periodId = $this->periods->last()->id;
        $this->levels = Carrera::all();
        $this->levelId = $this->levels->first()->id;
        $this->grades = Grade::where('carrera_id', $this->levelId)->get();
        $this->gradeId = $this->grades->first()->grade;
        $this->section = 'A';
        $this->data = collect();

    }

    public function render()
    {
        return view('livewire.report-deudas');
    }

    public function updatedLevelId($value)
    {
        $this->grades = Grade::where('carrera_id', $value)->get();
        $this->gradeId = $this->grades->first()->id;
    }

    public function getData()
    {
        $this->data=collect();
        $carrera_id = $this->levelId;
        $grade = $this->gradeId;
        $section = $this->section;
        $alumnos = Alumno::whereHas('alumnocarreras', function ($q) use ($carrera_id, $grade, $section) {
            $q->where('carrera_id', $carrera_id)->where('ciclo', $grade)->where('grupo', $section);
        })
            ->get();

        foreach ($alumnos as $alumno) {
            $alumnoPago = count($alumno->alumnoPago);

            $dato=[
                'alumno'=>$alumno->persona->DNI.' - '.$alumno->persona->paterno.' '.$alumno->persona->materno.', '.$alumno->persona->nombres,
                'matr'=>$alumnoPago>=1?'0':$alumno->categoriaActiva->last()->monto,
                'mar'=>$alumnoPago>=2?'0':$alumno->categoriaActiva->last()->monto,
                'abr'=>$alumnoPago>=3!=null?'0':$alumno->categoriaActiva->last()->monto,
                'may'=>$alumnoPago>=4!=null?'0':$alumno->categoriaActiva->last()->monto,
                'jun'=>$alumnoPago>=5!=null?'0':$alumno->categoriaActiva->last()->monto,
                'jul'=>$alumnoPago>=6!=null?'0':$alumno->categoriaActiva->last()->monto,
                'ago'=>$alumnoPago>=7!=null?'0':$alumno->categoriaActiva->last()->monto,
                'sep'=>$alumnoPago>=8!=null?'0':$alumno->categoriaActiva->last()->monto,
                'oct'=>$alumnoPago>=9!=null?'0':$alumno->categoriaActiva->last()->monto,
                'nov'=>$alumnoPago>=10!=null?'0':$alumno->categoriaActiva->last()->monto,
                'dic'=>$alumnoPago>=11!=null?'0':$alumno->categoriaActiva->last()->monto
            ];
            $this->data->push($dato);
        }
    }
}
