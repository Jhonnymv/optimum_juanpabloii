<?php

namespace App\Http\Livewire;

use App\Alumno;
use App\Padre;
use App\Persona;
use Livewire\Component;

class SeeDataAlumno extends Component
{
    public $tsearch, $dniSearch, $nombresSearch, $apMsearch, $apPsearch;

    public $dni, $nombres, $paterno, $materno, $telefono, $direccion, $grado, $pension,$idAlumno,$nivel, $fecha_nacimiento, $sexo;
    public $dniM, $nombresM, $paternoM, $maternoM, $telefonoM, $direccionM, $gradoM, $idMadre, $emailM,$apoderadoM ;
    public $dniP, $nombresP, $paternoP, $maternoP, $telefonoP, $direccionP, $gradoP, $idPadre, $emailP,$apoderadoP;

    public $alumnos;
    public $rp, $rm;

    public function mount()
    {
        $this->alumnos = collect();
    }

    public function render()
    {
        return view('livewire.see-data-alumno');
    }

    public function search()
    {
        if ($this->tsearch === 'dni') {
            if (strlen($this->dniSearch) === 8) {
                $this->alumnos = Alumno::with('persona')->whereHas('persona', function ($q) {
                    $q->where('DNI', $this->dniSearch);
                })->get();
            } else {
                session()->flash('message', 'Ingrese de número de Ruc válido.');
            }
        } elseif ($this->tsearch === 'nombre') {
            $this->alumnos = Alumno::with('persona')->whereHas('persona', function ($q) {
                $q->where('nombres', 'like', '%' . $this->nombresSearch . '%');
            })->get();
        } else {
            $this->alumnos = Alumno::with('persona')->whereHas('persona', function ($q) {
                $q->where([['paterno', 'like', '%' . $this->apPsearch . '%'], ['materno', 'like', '%' . $this->apMsearch . '%']]);
            })->get();
        }
    }

    public function getDatos($id)
    {
        $data = Alumno::find($id);
        $dPadre = Padre::find($data->padre_id);
        $dMadre = Padre::find($data->madre_id);
        $this->dni = $data->persona->DNI;
        $this->nombres = $data->persona->nombres;
        $this->paterno = $data->persona->paterno;
        $this->materno = $data->persona->materno;
        $this->telefono = $data->persona->telefono;
        $this->direccion = $data->persona->direccion;
        $this->fecha_nacimiento = $data->persona->fecha_nacimiento;
        $this->sexo = $data->persona->sexo;
        $this->grado = $data->alumnocarreras->first()->ciclo ?? '';
        $this->pension=$data->categorias->last()->monto;
        $this->idAlumno=$data->persona->id;
        $this->nivel=$data->carreras->last()->nombre;
//        dd($this->nivel);

        $this->dniP = $dPadre->persona->DNI;
        $this->nombresP = $dPadre->persona->nombres;
        $this->paternoP = $dPadre->persona->paterno;
        $this->maternoP = $dPadre->persona->materno;
        $this->telefonoP = $dPadre->persona->telefono;
        $this->direccionP = $dPadre->persona->direccion;
        $this->direccionP = $dPadre->persona->direccion;
        $this->idPadre=$dPadre->persona->id;
        $this->emailP=$dPadre->persona->email_personal;
        $this->apoderadoP=$dPadre->apoderado;


        $this->dniM = $dMadre->persona->DNI;
        $this->nombresM = $dMadre->persona->nombres;
        $this->paternoM = $dMadre->persona->paterno;
        $this->maternoM = $dMadre->persona->materno;
        $this->telefonoM = $dMadre->persona->telefono;
        $this->direccionM = $dMadre->persona->direccion;
        $this->emailM = $dMadre->persona->email_personal;
        $this->idMadre=$dMadre->persona->id;
        $this->apoderadoM=$dMadre->apoderado;


    }

    public function editAlumno()
    {
        $palumno=Persona::find($this->idAlumno);
        $palumno->nombres = $this->nombres;
        $palumno->paterno = $this->paterno;
        $palumno->materno = $this->materno;
        $palumno->telefono = $this->telefono;
        $palumno->direccion = $this->direccion;
        $palumno->fecha_nacimiento = $this->fecha_nacimiento;
        $palumno->sexo = $this->sexo;
        $palumno->save();

    }

    public function editPadre()
    {
        $pPadre = Persona::find($this->idPadre);
        $pPadre->nombres = $this->nombresP;
        $pPadre->paterno = $this->paternoP;
        $pPadre->materno = $this->maternoP;
        $pPadre->telefono = $this->telefonoP;
        $pPadre->direccion = $this->direccionP;
        $pPadre->email_personal = $this->emailP;
        $pPadre->save();
        if ($this->apoderadoP!=$this->rp){
            $padre = Padre::find($pPadre->padre->id);
            $padre->apoderado='si';
            $padre->save();

            $madre = Persona::find($this->idMadre);
            $madre = Padre::find($madre->padre->id);
            $madre->apoderado='no';
            $madre->save();
        }

    }

    public function editMadre()
    {
        $pMadre = Persona::find($this->idMadre);
        $pMadre->nombres = $this->nombresM;
        $pMadre->paterno = $this->paternoM;
        $pMadre->materno = $this->maternoM;
        $pMadre->telefono = $this->telefonoM;
        $pMadre->direccion = $this->direccionM;
        $pMadre->email_personal = $this->emailM;
        $pMadre->save();
        if ($this->apoderadoP!=$this->rm){
            $madre = Padre::find($pMadre->padre->id);
            $madre->apoderado='si';
            $madre->save();

            $padre = Persona::find($this->idPadre);
            $padre = Padre::find($padre->padre->id);
            $padre->apoderado='no';
            $padre->save();
        }
    }

    public function changeEvent($value)
    {
        if ($value==='si'){
            $this->apoderadoP='no';
        }else{
            $this->apoderadoP='si';
        }

    }
    public function changeEventP($value)
    {
        if ($value==='si'){
            $this->apoderadoM='no';
        }else{
            $this->apoderadoM='si';
        }
    }
}
