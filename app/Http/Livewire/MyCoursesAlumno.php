<?php

namespace App\Http\Livewire;

use App\Periodo;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class MyCoursesAlumno extends Component
{
    use WithPagination;

    public $periodoId, $periodos;
    public $myCourses;

    public function mount()
    {
        $this->periodos = Periodo::all();
        $this->myCourses = Auth::user()->persona->alumno->matricula->last()->matriculadetalles;
        //dd($this->myCourses);
    }

    public function render()
    {
        return view('livewire.my-courses-alumno');
    }
}
