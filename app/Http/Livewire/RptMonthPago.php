<?php

namespace App\Http\Livewire;

use App\Pago;
use App\Periodo;
use Carbon\Carbon;
use Livewire\Component;
use App\Exports\ReportPayMonth;
use Maatwebsite\Excel\Facades\Excel;

class RptMonthPago extends Component
{
    public $periodos, $periodoId, $months,$month, $data;
    public function mount()
    {
        $this->months=collect();
        $this->data=collect();
        $this->periodos=Periodo::all();
        $this->periodoId=$this->periodos->first()->id;
        $monthNum=1;
        while ($monthNum <= 12) {
            $now=Carbon::parse('2021-'.$monthNum.'-01');
            $data = [
                'value' => $now->month,
                'text' => $now->monthName
            ];
            $monthNum += 1;
            $this->months->push($data);
        }
    }
    public function render()
    {
        return view('livewire.rpt-month-pago');
    }

    public function updatedMonth($value)
    {
        $periodo=Periodo::find($this->periodoId);
        $this->data=Pago::with('persona')
            ->whereMonth('created_at','=',$this->month)
            ->whereHas('cronograma',function ($q) use($periodo){
                $q->where('periodo_id',$periodo->id);
            })
            ->where('fe_total_price','>',0)
            ->get();
    }
    public function export()
    {
        return Excel::download(new ReportPayMonth($this->periodoId,$this->month), 'invoices.xlsx');
    }
}
