<?php

namespace App\Http\Livewire;

use App\EalDetalles;
use App\Examen;
use App\ExamenAlumno;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ExamStudentAnswer extends Component
{
    public $student, $levelGrade, $examAl;

    public function mount()
    {
        $this->student = Auth::user()->persona->alumno;
        $alCarrera = $this->student->alumnocarreras->where('estado', 1)->first();
        $this->levelGrade = $alCarrera->carrera_id . '-' . $alCarrera->ciclo;
    }

    public function render()
    {
        $now = Carbon::now();
        $examen = Examen::with('periodo')
            ->where('mes_ano', $now->month . '-' . $now->year)->where('estado','2')
            ->whereDate('fecha_hora', '<=', $now->format('Y-m-d'))
            ->whereTime('fecha_hora', '<=', $now->format('H:i:s'))
            ->first();
        if ($examen) {
            $horMin = explode(':', $examen->duracion);
            $finish = $examen->fecha_hora->addHours($horMin[0])->addMinutes($horMin[1]);
            $this->examAl=ExamenAlumno::with('alumno')->where([['alumno_id',$this->student->id],['examen_id',$examen->id]])->first()->id??null;
        }
        return view('livewire.exam-student-answer', ['exam' => $examen,'fecha'=>$now,'finish'=>$finish??null]);
    }


}
