<?php

namespace App\Http\Livewire;

use App\Periodo;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class MyCoursesDocente extends Component
{
    use WithPagination;

    public $periodoId, $periodos;
    public $myCourses;

    public function mount()
    {
        $this->periodos = Periodo::all();
        $this->myCourses = Auth::user()->persona->docente->secciones;
    }

    public function render()
    {
        return view('livewire.my-courses-docente', ['myCourses' => $this->myCourses]);
    }

    public function updatedPeriodoId($value)
    {
        $this->myCourses = Auth::user()->persona->docente->secciones->where('periodo_id', $this->periodoId);
    }

}
