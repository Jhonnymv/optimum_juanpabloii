<?php

namespace App\Http\Livewire;

use App\Doc_programation;
use App\Documento;
use App\Fase;
use App\Periodo;
use App\Programation;
use App\Seccion;
use App\unidades;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use PhpParser\Node\Stmt\DeclareDeclare;
use File;

class ProgramationDocente extends Component
{
    use WithFileUploads;

    public $seccion;
    public $programationId, $metodologia, $descripcion, $nombreClase, $linkClase, $bibliografia, $estado;
    public $documents = [], $documentNames = [], $links = [];
    public $i = 0, $inputs = [];
    public $fases;
    public $unitId, $unitUnidad, $unitFaseId, $unitDenomination, $unitFechaInicio, $unitFechaFin, $unitDuracion;
    public $contNombre, $contDescription, $contFechaInicio, $contFechaFin, $contDuracion;

    public function mount()
    {
        $progamation = Programation::where('seccion_id', $this->seccion->id)->first();
        $this->fases = Fase::where([['periodo_id', Periodo::where('estado', '1')->first()->id], ['carrera_id', $this->seccion->pe_curso->pe_carrera->carrera_id]])->get();
        if (count($this->fases) > 0) {
            $this->unitFaseId = $this->fases->first()->id;
            if ($progamation) {
                $this->programationId = $progamation->id;
                $this->metodologia = $progamation->metodologia;
                $this->descripcion = $progamation->descripcion;
                $this->nombreClase = $progamation->nombre_clase;
                $this->linkClase = $progamation->link_clase;
            }
        } else {
            session()->flash('message', 'Bimestres no registrados.');
        }
    }

    public function render()
    {
        $units = collect();
        if ($this->programationId)
            $units = unidades::where('programation_id', $this->programationId)->get();
        $this->unitUnidad = count($units) + 1;
        $programation = Programation::where('seccion_id', $this->seccion->id)->first();
        return view('livewire.programation-docente', ['units' => $units, 'programation' => $programation]);
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function storeProgramation()
    {
        $this->validate([
            'metodologia' => 'required',
            'descripcion' => 'required',
            'nombreClase' => 'required',
            'linkClase' => 'required',
        ], [
            'required' => '*Este campo es obligatorio'
        ]);
        DB::beginTransaction();
        $programation = $this->programationId == null ? new Programation() : Programation::find($this->programationId);
        $programation->seccion_id = $this->seccion->id;
        $programation->metodologia = $this->metodologia;
        $programation->descripcion = $this->descripcion;
        $programation->nombre_clase = $this->nombreClase;
        $programation->link_clase = $this->linkClase;
        $programation->save();
        $this->programationId = $programation->id;
        for ($i = 0; $i < count($this->documentNames); $i++) {
            $document = new Doc_programation();
            $document->programation_id = $this->programationId;
            $document->nombre = $this->documentNames[$i];
            if (array_key_exists($i, $this->documents)) {
                $s_path = $this->documents[$i]->storeAs('public/docsProgramation/' . $this->programationId, $this->documentNames[$i] . '.' . $this->documents[$i]->extension());
                $p_patc = Storage::url($s_path);
                $document->url = $p_patc;
            }
            if (array_key_exists($i, $this->links)) {
                $document->link = $this->links[$i];
            }
            $document->save();
        }
        DB::commit();

    }

    public function updatedUnitFechaInicio($value)
    {
        if ($this->unitFechaFin) {
            $inicio = Carbon::make($this->unitFechaInicio);
            $fin = Carbon::make($this->unitFechaFin);
            $this->unitDuracion = $inicio->diffInDays($fin) + 1 . ' Días';
        }
    }

    public function updatedUnitFechaFin($value)
    {
        if ($this->unitFechaInicio) {
            $inicio = Carbon::make($this->unitFechaInicio);
            $fin = Carbon::make($this->unitFechaFin);
            $this->unitDuracion = $inicio->diffInDays($fin) + 1 . ' Días';
        }
    }

    public function saveUnit()
    {
        $this->validate([
            'unitDenomination' => 'required',
            'unitUnidad' => 'required',
            'unitFaseId' => 'required',
            'unitFechaInicio' => 'required',
            'unitFechaFin' => 'required',
            'unitDuracion' => 'required'
        ]);

        $unidad = $this->unitId == null ? new unidades() : unidades::find($this->unitId);
        $unidad->programation_id = $this->programationId;
        $unidad->fase_id = $this->unitFaseId;
        $unidad->denominacion = $this->unitDenomination;
        $unidad->unidad = $this->unitUnidad;
        $unidad->fecha_inicio = $this->unitFechaInicio;
        $unidad->fecha_fin = $this->unitFechaFin;
        $unidad->duracion = $this->unitDuracion;
        $unidad->save();
        $this->fases = Fase::where('periodo_id', Periodo::where('estado', '1')->first()->id)->get();
        $this->unitFaseId = $this->fases->first()->id;
        $this->unitId = null;
        $this->unitDenomination = null;
        $this->unitUnidad = null;
        $this->unitFechaInicio = null;
        $this->unitFechaFin = null;
        $this->unitDuracion = null;
    }

    public function addSessions($unitId)
    {
        $this->unitId = $unitId;
    }

    public function removeDocProgra($id)
    {
        $docProg = Doc_programation::find($id);
        if ($docProg->url) {
            File::delete(public_path($docProg->url));
        }
        $docProg->delete();
    }

    public function editUnidad($uId)
    {
        $this->unitId = $uId;
        $unidad = unidades::find($this->unitId);
        $this->unitUnidad = $unidad->unidad;
        $this->unitFaseId = $unidad->fase_id;
        $this->unitDenomination = $unidad->denominacion;
        $this->unitFechaInicio = $unidad->fecha_inicio;
        $this->unitFechaFin = $unidad->fecha_fin;
        $this->unitDuracion = $unidad->duracion;
    }
}
