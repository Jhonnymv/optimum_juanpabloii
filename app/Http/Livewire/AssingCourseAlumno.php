<?php

namespace App\Http\Livewire;

use App\Asignacion;
use App\Fase;
use App\Periodo;
use App\Programation;
use App\Seccion;
use App\unidades;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class AssingCourseAlumno extends Component
{
    use WithFileUploads;
    public $seccionId,$fases;
    public $alumno,$assignId,$respuesta,$docRespuesta;
    public $now;

    public function mount()
    {
        $this->fases=Fase::where('periodo_id',Periodo::where('estado','1')->first()->id)->get();
        $this->alumno=Auth::user()->persona->alumno;
    }
    public function render()
    {
        $this->now=Carbon::now();
        $programation=Programation::where('seccion_id',$this->seccionId)->first();
        $units=unidades::where('programation_id',$programation->id)->get();
        $seccion=Seccion::find($this->seccionId);
        return view('livewire.assing-course-alumno',['seccion'=>$seccion,'programation'=>$programation,'units'=>$units]);
    }

    public function getAssignId($assignId)
    {
        $this->assignId=$assignId;
    }

    public function storeResp()
    {
        $this->validate([
            'respuesta'=>'required',
        ]);
        if ($this->docRespuesta){
            $s_path = $this->docRespuesta->storeAs('public/respuestasAsignaciones/' . $this->alumno->persona->DNI.'/'. $this->assignId . '.' . $this->docRespuesta->extension());
            $p_patc = Storage::url($s_path);
        }
        $this->alumno->asignaciones()->attach($this->assignId,[
            'respuesta'=>$this->respuesta,
            'doc_respuesta'=>$p_patc,
        ]);
    }
}
