<?php

namespace App\Http\Livewire;

use App\MaterialApoyo;
use Livewire\Component;

class FrameContends extends Component
{
    public $materialId,$type,$url;

    public function mount()
    {
        $material=MaterialApoyo::find($this->materialId);
        if ($this->type=='doc')
            $this->url=asset($material->url_doc);

    }
    public function render()
    {
        return view('livewire.frame-contends');
    }
}
