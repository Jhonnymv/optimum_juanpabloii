<?php

namespace App\Http\Livewire;

use App\MaterialApoyo;
use App\Tema;
use App\unidades;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use PhpParser\Node\Stmt\DeclareDeclare;
use File;

class Temas extends Component
{
    use WithFileUploads;

    public $temaId;
    public $unidadId;
    public $nombre, $descripcion, $guideUrl;
    public $documents = [], $documentNames = [], $documentLinks = [], $documentPins = [], $startDate = [], $startTime = [], $endDate = [], $endTime = [];
    public $documentsC = [], $documentNamesC = [], $documentLinksC = [], $documentPinsC = [], $startDateC = [], $startTimeC = [], $endDateC = [], $endTimeC = [];
    public $iC = -1, $inputsC = [];
    public $unidad;
    public $chkL = '', $chkV = '', $chkQ = '', $chkT = '';

    public function mount()
    {
        $this->unidad = unidades::find($this->unidadId);
        if ($this->temaId){
            $tema=Tema::find($this->temaId);
            $this->nombre=$tema->nombre;
        }
        $this->startTime[0] = now()->format('H:i');
        $this->startTime[1] = now()->format('H:i');
        $this->startTime[2] = now()->format('H:i');
        $this->startTime[3] = now()->format('H:i');
        $this->startDate[0] = now()->format('Y-m-d');
        $this->startDate[1] = now()->format('Y-m-d');
        $this->startDate[2] = now()->format('Y-m-d');
        $this->startDate[3] = now()->format('Y-m-d');
        $this->endTime[0] = now()->format('H:i');
        $this->endTime[1] = now()->format('H:i');
        $this->endTime[2] = now()->format('H:i');
        $this->endTime[3] = now()->format('H:i');
        $this->endDate[0] = Carbon::now()->addDay(1)->format('Y-m-d');
        $this->endDate[1] = Carbon::now()->addDay(1)->format('Y-m-d');
        $this->endDate[2] = Carbon::now()->addDay(1)->format('Y-m-d');
        $this->endDate[3] = Carbon::now()->addDay(1)->format('Y-m-d');

    }

    public function render()
    {
        $tema=Tema::find($this->temaId);
        return view('livewire.temas',['tema'=>$tema]);
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->iC = $i;
        array_push($this->inputsC, $i);
    }

    public function remove($i)
    {
        unset($this->inputsC[$i]);
    }

    public function store()
    {
        $this->validate([
            'nombre' => 'required',
//            'descripcion' => 'required',
//            'guideUrl' => 'required',
//            'documentNames.0' => 'required',
        ], [
            'required' => '*Este campo es obligatorio'
        ]);
        DB::beginTransaction();
        $tema = $this->temaId==null?new Tema():Tema::find($this->temaId);
        $tema->unidad_id = $this->unidadId;
        $tema->nombre = $this->nombre;
//        $tema->descripcion = $this->descripcion;
//        $s_path = $this->guideUrl->storeAs('public/docsTemasGuides/' . $tema->id.'-Guia', $this->guideUrl->extension());
//        $p_patc = Storage::url($s_path);
//        $tema->guia_url = $p_patc;
        $tema->save();

        if ($this->chkL != '')
            $this->saveMaterial(0, $tema->id);
        if ($this->chkV != '')
            $this->saveMaterial(1, $tema->id);
        if ($this->chkQ != '')
            $this->saveMaterial(2, $tema->id);
        if ($this->chkT != '')
            $this->saveMaterial(3, $tema->id);
            for ($i = 0; $i < count($this->documentNamesC); $i++) {
                $materiaApoyo = new MaterialApoyo();
                $materiaApoyo->tema_id =$tema->id;
                $materiaApoyo->nombre = $this->documentNamesC[$i];
                if (array_key_exists($i, $this->documentsC)) {
                    $s_path = $this->documentsC[$i]->storeAs('public/docsTemas/' . $tema->id, $this->documentNamesC[$i] . '.' . $this->documentsC[$i]->extension());
                    $p_patc = Storage::url($s_path);
                    $materiaApoyo->url_doc = $p_patc;
                }
                if (array_key_exists($i, $this->documentLinksC)) {
                    $materiaApoyo->link = $this->documentLinksC[$i];
                }
                if (array_key_exists($i, $this->documentPinsC)) {
                    $materiaApoyo->pin = $this->documentPinsC[$i];
                }
                $materiaApoyo->startDate = $this->startDateC[$i] . ' ' . $this->startTimeC[$i];
                $materiaApoyo->endDate = $this->endDateC[$i] . ' ' . $this->endTimeC[$i];
                $materiaApoyo->save();
            }
        DB::commit();
        return redirect()->to(route('CourseDocenteProgramation', ['seccionId' => $this->unidad->programacion->seccion_id]));
    }

    public function saveMaterial($i,$tema_id)
    {
        $materiaApoyo = new MaterialApoyo();
        $materiaApoyo->tema_id = $tema_id;
        $materiaApoyo->nombre = $this->documentNames[$i];
        if (array_key_exists($i, $this->documents)) {
            $s_path = $this->documents[$i]->storeAs('public/docsTemas/' . $tema_id, $this->documentNames[$i] . '.' . $this->documents[$i]->extension());
            $p_patc = Storage::url($s_path);
            $materiaApoyo->url_doc = $p_patc;
        }
        if (array_key_exists($i, $this->documentLinks)) {
            $materiaApoyo->link = $this->documentLinks[$i];
        }
        if (array_key_exists($i, $this->documentPins)) {
            $materiaApoyo->pin = $this->documentPins[$i];
        }
        $materiaApoyo->startDate = $this->startDate[$i] . ' ' . $this->startTime[$i];
        $materiaApoyo->endDate = $this->endDate[$i] . ' ' . $this->endTime[$i];
        $materiaApoyo->save();
    }
    public function removeMatApoyo($id)
    {
        $matApoyo = MaterialApoyo::find($id);
        if ($matApoyo->url_doc) {
            File::delete(public_path($matApoyo->url_doc));
        }
        $matApoyo->delete();
    }
}
