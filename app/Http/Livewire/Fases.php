<?php

namespace App\Http\Livewire;

use App\Fase;
use Carbon\Carbon;
use Livewire\Component;
use phpDocumentor\Reflection\Types\Null_;

class Fases extends Component
{
    public $periodoId;
    public $faseId,$numFase,$denominacion,$fechaInicio,$fechaFin,$duracion;
    public function render()
    {
        $fases=Fase::where('periodo_id',$this->periodoId)->get();
        return view('livewire.fases',['fases'=>$fases]);
    }
    public function updatedFechaInicio($value)
    {
        if ($this->fechaFin) {
            $inicio = Carbon::make($this->fechaInicio);
            $fin = Carbon::make($this->fechaFin);
            $this->duracion = $inicio->diffInDays($fin) + 1 . ' Días';
        }
    }

    public function updatedFechaFin($value)
    {
        if ($this->fechaInicio) {
            $inicio = Carbon::make($this->fechaInicio);
            $fin = Carbon::make($this->fechaFin);
            $this->duracion = $inicio->diffInDays($fin) + 1 . ' Días';
        }
    }

    public function create()
    {
        $this->resetInputs();
        $fases=Fase::where('periodo_id',$this->periodoId)->get();
        $this->numFase=count($fases)+1;
    }
    function resetInputs(){
        $this->denominacion=null;
        $this->fechaInicio=null;
        $this->fechaFin=null;
        $this->duracion=null;
    }
    public function savefase()
    {
        $this->emit('closeModal');
        $this->validate([
            'numFase'=>'required',
            'denominacion'=>'required',
            'fechaInicio'=>'required',
            'fechaFin'=>'required',
            'duracion'=>'required',
        ]);
        $fase=new Fase();
        $fase->periodo_id=$this->periodoId;
        $fase->num_fase=$this->numFase;
        $fase->denominacion=$this->denominacion;
        $fase->fecha_inicio=$this->fechaInicio;
        $fase->fecha_fin=$this->fechaFin;
        $fase->duracion=$this->duracion;
        $fase->save();
        $this->resetInputs();
    }

    public function edit($id)
    {
        $fase=Fase::find($id);
        $this->faseId=$fase->id;
        $this->numFase=$fase->num_fase;
        $this->denominacion=$fase->denominacion;
        $this->fechaInicio=$fase->fecha_inicio;
        $this->fechaFin=$fase->fecha_fin;
        $this->duracion=$fase->duracion;
    }

    public function update()
    {
        $this->emit('closeModal');
        $this->validate([
            'numFase'=>'required',
            'denominacion'=>'required',
            'fechaInicio'=>'required',
            'fechaFin'=>'required',
            'duracion'=>'required',
        ]);
        $fase=Fase::find($this->faseId);
        $fase->periodo_id=$this->periodoId;
        $fase->num_fase=$this->numFase;
        $fase->denominacion=$this->denominacion;
        $fase->fecha_inicio=$this->fechaInicio;
        $fase->fecha_fin=$this->fechaFin;
        $fase->duracion=$this->duracion;
        $fase->save();

        $this->resetInputs();
    }

    public function delete($id)
    {
        Fase::find($id)->delete();
    }
}
