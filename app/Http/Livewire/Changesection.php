<?php

namespace App\Http\Livewire;

use App\Alumno;
use App\AlumnoCarrera;
use App\Carrera;
use App\Grade;
use App\Matricula;
use App\PeCarrera;
use App\Periodo;
use App\Seccion;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use function Complex\sec;

class Changesection extends Component
{
    public $alDni, $alId, $alDatos, $alumno, $levels, $levelId, $grades, $gradeId, $sections, $sectionId, $alumnoCarrera;

    public function mount()
    {
        $this->levels = Carrera::all();
    }

    public function render()
    {
        return view('livewire.changesection');
    }

    public function getAlumno()
    {
        $dni = $this->alDni;
        $this->alumno = Alumno::with('persona')
            ->whereHas('persona', function ($q) use ($dni) {
                $q->where('DNI', $dni);
            })->first();
        $this->alDatos = $this->alumno->persona->paterno . ' ' . $this->alumno->persona->materno . ' ' . $this->alumno->persona->nombres;
        $this->alumnoCarrera = $this->alumno->alumnocarreras->where('estado', 1)->last();
        $alumnoCarrera = $this->alumnoCarrera;
        $this->levelId = $this->alumnoCarrera->carrera_id;
        $this->grades=Grade::where('carrera_id',$this->levelId)->get();
        $grade = Grade::where([['carrera_id', $this->alumnoCarrera->carrera_id], ['grade', $this->alumnoCarrera->ciclo]])->first();
        $this->sections = Seccion::with('pe_curso')
            ->whereHas('pe_curso.pe_carrera', function ($q) use ($alumnoCarrera) {
                $q->where('carrera_id', $alumnoCarrera->carrera_id);
            })
            ->whereHas('pe_curso', function ($q) use ($alumnoCarrera) {
                $q->where('semestre', $alumnoCarrera->ciclo);
            })
            ->pluck('grupo')->unique();
        $this->gradeId = $grade->id;
        $this->sectionId=$alumnoCarrera->grupo;
    }

    public function updatedLevelId($value)
    {
        $this->grades=Grade::where('carrera_id',$value)->get();
        $this->gradeId=$this->grades->first()->id;
    }

    public function updatedGradeId($value)
    {
        $carreraId=$this->levelId;
        $grade=Grade::find($value);
        $this->sections = Seccion::with('pe_curso')
            ->whereHas('pe_curso.pe_carrera', function ($q) use ($carreraId) {
                $q->where('carrera_id', $carreraId);
            })
            ->whereHas('pe_curso', function ($q) use ($grade) {
                $q->where('semestre', $grade->grade);
            })
            ->pluck('grupo')->unique();
        $this->sectionId = $this->sections->first();
    }

    public function store()
    {
        $carreraId=$this->levelId;
        $grade=Grade::find($this->gradeId);
        $peCarrera=PeCarrera::where([['carrera_id',$carreraId],['estado',1]])->first();
        $sections = Seccion::with('pe_curso')
            ->whereHas('pe_curso.pe_carrera', function ($q) use ($carreraId) {
                $q->where('carrera_id', $carreraId);
            })
            ->whereHas('pe_curso', function ($q) use ($grade) {
                $q->where('semestre', $grade->grade);
            })
            ->where('grupo',$this->sectionId)
            ->pluck('id');
        DB::beginTransaction();
        AlumnoCarrera::with('alumno')->find($this->alumnoCarrera->id)->update(['carrera_id'=>$carreraId,'pe_carrera_id',$peCarrera->id,'grupo'=>$this->sectionId]);
        Matricula::with('alumnocarrera')
            ->where('periodo_id',Periodo::where('estado',1)->first()->id)
            ->where('alumno_carrera_id',$this->alumnoCarrera->id)
            ->first()
            ->seccions()->sync($sections);
        DB::commit();
        return redirect()->route('changeSection');

    }
}
