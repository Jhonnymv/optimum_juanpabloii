<?php

namespace App\Http\Livewire;

use App\Fase;
use App\MaterialApoyo;
use App\Periodo;
use App\Programation;
use App\Seccion;
use App\Tema;
use App\unidades;
use Carbon\Carbon;
use Livewire\Component;

class ContCourseAlumno extends Component
{
    public $seccionId,$seccion,$fases, $now;

    public function mount()
    {
        $this->seccion=Seccion::find($this->seccionId);
        $this->fases = Fase::where([['periodo_id', Periodo::where('estado', '1')->first()->id], ['carrera_id', $this->seccion->pe_curso->pe_carrera->carrera_id]])->get();
    }
    public function render()
    {
        $this->now=Carbon::now();
        $programation=Programation::where('seccion_id',$this->seccionId)->first();
        $units=unidades::where('programation_id',$programation->id)->get();

//        $a=Tema::with('unidad')->where('unidad_id',$units->first()->id)->first();
//        $b=MaterialApoyo::with('tema')->whereDate('startDate','>=',$this->now)->get();
//        dd($b);
        return view('livewire.cont-course-alumno',['programation'=>$programation,'units'=>$units]);
    }
}
