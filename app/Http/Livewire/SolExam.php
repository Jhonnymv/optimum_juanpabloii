<?php

namespace App\Http\Livewire;

use App\Examen;
use App\ExamenAlumno;
use App\Exports\ResultSimExport;
use App\Periodo;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class SolExam extends Component
{
    public $exams,$examId,$periods,$periodId,$results,$habEvaluar=0;

    public function mount()
    {
        $this->periods=Periodo::all();
        $this->exams=collect();
    }
    public function render()
    {
        return view('livewire.sol-exam');
    }

    public function updatedPeriodId($value)
    {
        $this->exams=Examen::with('periodo')->where('periodo_id',$this->periodId)->get();
    }

    public function updatedExamId($value)
    {
        $this->results=ExamenAlumno::with('alumno')->where('examen_id',$this->examId)->whereNotNull('nota')->get();
        if (count($this->results)==0)
            $this->habEvaluar=1;
    }

    public function evaluar()
    {
        try {

            $examen = Examen::find($this->examId);
            $claves = [];
            foreach ($examen->preguntas as $pregunta) {
//            echo 'categoría: '.$pregunta->categoria->categoria."<br>";
//            echo 'pregunta: ' . $pregunta->pregunta . "<br>";
                foreach ($pregunta->bpalternativas as $alternativa) {
                    if ($alternativa->valor == '1') {

//                    echo 'alternativa: ' . $alternativa->alternativa . "<br><br>";

                        $claves += array($alternativa->bp_pregunta_id => ['pregunta_id' => $alternativa->bp_pregunta_id, 'clave_id' => $alternativa->id]);
                    }
                }
            }
            $examen_alumnos = ExamenAlumno::where('examen_id', $this->examId)->get();
            foreach ($examen_alumnos as $ex_al) {
                $puntos=0;
                foreach ($ex_al->ep_detalles as $e_detalle) {
                    $clave = $claves[$e_detalle->pregunta_id];
                    if ($e_detalle->alternativa_id == $clave['clave_id']) {
                        $puntos+=1;
                    }
                }
                $nota=$puntos/3;
                $ex_al->nota=$nota;
                $ex_al->save();
            }
            $this->results=ExamenAlumno::with('alumno')->where('examen_id',$this->examId)->whereNotNull('nota')->get();
        } catch (\Exception $e) {
            dd('error');
        }
    }
    public function export()
    {
        return Excel::download(new ResultSimExport($this->examId), 'resultados.xlsx');
    }
}
