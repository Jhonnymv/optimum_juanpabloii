<?php

namespace App\Http\Livewire;

use App\Alumno;
use App\Seccion;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ListStudents extends Component
{
    public $sections, $sectionId, $students;

    public function mount()
    {
        $this->sections = Auth::user()->persona->docente->secciones;
        $this->students = collect();
    }

    public function render()
    {
        return view('livewire.list-students');
    }

    public function updatedSectionId($value)
    {
        $matriculas = Seccion::find($value)->matricula;

        foreach ($matriculas as $matricula) {
            if ($matricula->alumnocarrera->alumno->padre_id !== 0)
                if ($matricula->alumnocarrera->alumno->padre->apoderado === 'si')
                    $Apo = $matricula->alumnocarrera->alumno->padre->persona->DNI . ' - ' . $matricula->alumnocarrera->alumno->padre->persona->paterno . ' ' . $matricula->alumnocarrera->alumno->padre->persona->materno . ' - ' . $matricula->alumnocarrera->alumno->padre->persona->nombres . ' ' . $matricula->alumnocarrera->alumno->padre->persona->telefono??'';
            if ($matricula->alumnocarrera->alumno->madre_id !== 0)
                if ($matricula->alumnocarrera->alumno->madre->apoderado === 'si')
                    $Apo = $matricula->alumnocarrera->alumno->madre->persona->DNI . ' - ' . $matricula->alumnocarrera->alumno->madre->persona->paterno . ' ' . $matricula->alumnocarrera->alumno->madre->persona->materno . ' - ' . $matricula->alumnocarrera->alumno->madre->persona->nombres . ' ' . $matricula->alumnocarrera->alumno->madre->persona->telefono??'';
            $dato = [
                'student'=>$matricula->alumnocarrera->alumno->persona->DNI.' - '.$matricula->alumnocarrera->alumno->persona->paterno.' '.$matricula->alumnocarrera->alumno->persona->materno.' '.$matricula->alumnocarrera->alumno->persona->nombres??'',
                'nivel'=>$matricula->alumnocarrera->carrera->nombre??'',
                'graSec'=>$matricula->alumnocarrera->ciclo.' / '.$matricula->alumnocarrera->grupo??'',
                'apoderado'=>$Apo
            ];
            $this->students->push($dato);
        }
    }
}
