<?php

namespace App\Http\Livewire;

use App\Asignacion;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class SeeResAssign extends Component
{
    public $assignId;
//, $assignation
    public $nota;

//    public function mount()
//    {
//        $this->assignation=Asignacion::find($this->assignId);
//    }
    public function render()
    {
        $assignation=Asignacion::find($this->assignId);
        return view('livewire.see-res-assign',['assignation'=>$assignation]);
    }

    public function storeNote($alumnoId)
    {
        $this->validate([
            'nota'=>'required'
        ]);
        DB::table('asignacion_alumno')->where([['alumno_id',$alumnoId],['asignacion_id',$this->assignId]])->update(['nota'=>$this->nota[$alumnoId]]);
        $this->nota=null;
    }
}
