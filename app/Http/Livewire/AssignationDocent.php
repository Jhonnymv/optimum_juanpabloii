<?php

namespace App\Http\Livewire;

use App\Asignacion;
use App\Fase;
use App\Periodo;
use App\Programation;
use App\Seccion;
use App\unidades;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class AssignationDocent extends Component
{
    use WithFileUploads;
    public $seccionId,$fases, $checkInstrumento;
    public $unidadId,$nombre,$instrumento,$descripcion,$url_doc,$link,$start_date,$startTime,$end_date,$endTime;


    public function mount()
    {
        $this->fases=Fase::where([['periodo_id',Periodo::where('estado','1')->first()->id],['carrera_id',Seccion::find($this->seccionId)->pe_curso->pecarrera->carrera_id]])->get();
    }
    public function render()
    {
        $programation=Programation::where('seccion_id',$this->seccionId)->first();
        $units=unidades::where('programation_id',$programation->id)->get();
        $seccion=Seccion::find($this->seccionId);
        return view('livewire.assignation-docent',['seccion'=>$seccion,'programation'=>$programation,'units'=>$units]);
    }
    public function getUnitId($unitId)
    {
        $this->unidadId=$unitId;
    }
    public function storeAssignation()
    {
        $this->validate([
           'unidadId'=>'required',
           'nombre'=>'required',
           'descripcion'=>'required',
//           'url_doc'=>'required',
           'link'=>'required',
           'start_date'=>'required',
           'end_date'=>'required',
        ]);
        $assignation=new Asignacion();
        $assignation->unidad_id=$this->unidadId;
        $assignation->nombre=$this->nombre;
        $assignation->instrumento=$this->instrumento;
        $assignation->descripcion=$this->descripcion;
        if ($this->url_doc){
            $s_path = $this->url_doc->storeAs('public/asignaciones/' . $this->unidadId, count(Asignacion::where('unidad_id',$this->unidadId)->get())+1 . '.' . $this->url_doc->extension());
            $p_patc = Storage::url($s_path);
            $assignation->url_doc = $p_patc;
        }
        $assignation->link=$this->link;
        $assignation->start_date=$this->start_date.' '.$this->startTime;
        $assignation->end_date=$this->end_date.' '.$this->endTime;
        $assignation->save();
    }

    public function resetInputsAssignation()
    {
        $this->unidadId=null;
        $this->nombre=null;
        $this->instrumento=null;
        $this->descripcion=null;
        $this->url_doc=null;
        $this->link=null;
        $this->start_date=null;
        $this->end_date=null;
        $this->emit('closeModal');
    }
}
