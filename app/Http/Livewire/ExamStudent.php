<?php

namespace App\Http\Livewire;

use App\EalDetalles;
use App\Examen;
use App\ExamenAlumno;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use phpseclib3\Exception\BadDecryptionException;

class ExamStudent extends Component
{
    public $student, $levelGrade, $examAl;

    public function mount()
    {
        $this->student = Auth::user()->persona->alumno;
        $alCarrera = $this->student->alumnocarreras->where('estado', 1)->first();
        $this->levelGrade = $alCarrera->carrera_id . '-' . $alCarrera->ciclo;
    }

    public function render()
    {
        $now = Carbon::now();
        $examen = Examen::with('periodo')
            ->where('mes_ano', $now->month . '-' . $now->year)
            ->whereDate('fecha_hora', '<=', $now->format('Y-m-d'))
            ->whereTime('fecha_hora', '<=', $now->format('H:i:s'))
            ->first();
        if ($examen) {
            $horMin = explode(':', $examen->duracion);
            $finish = $examen->fecha_hora->addHours($horMin[0])->addMinutes($horMin[1]);
            $this->examAl=ExamenAlumno::with('alumno')->where([['alumno_id',$this->student->id],['examen_id',$examen->id]])->first()->id??null;
        }
        return view('livewire.exam-student', ['exam' => $examen,'fecha'=>$now,'finish'=>$finish??null]);
    }

    public function saveResponse($examId,$questionId,$alternativeId)
    {
        DB::beginTransaction();
        $examAl=ExamenAlumno::with('alumno')->where([['alumno_id',$this->student->id],['examen_id',$examId]])->first()??new ExamenAlumno();
        $examAl->alumno_id=$this->student->id;
        $examAl->examen_id=$examId;
        $examAl->save();
        $this->examAl=$examAl;

        $eal_detalles=EalDetalles::where([['examen_alumno_id',$examAl->id],['pregunta_id',$questionId]])->first()??new EalDetalles();
        $eal_detalles->examen_alumno_id=$examAl->id;
        $eal_detalles->pregunta_id=$questionId;
        $eal_detalles->alternativa_id=$alternativeId;
        $eal_detalles->save();
        DB::commit();
//        session()->flash('message', 'Se ha registrado tu respuesta');
        $this->emit('msgAlert');

    }
}
