<?php

namespace App\Http\Livewire;

use App\BpAlternativa;
use App\BpPregunta;
use App\Curso;
use App\Examen;
use App\PeCurso;
use App\Periodo;
use App\Seccion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Symfony\Component\Console\Question\Question;
use File;

class QuestionRegister extends Component
{
    use WithFileUploads;

//    Periodo,meses, mes
    public $examId, $periodDes, $periodId, $months, $month, $date, $hour, $duration;

//    pe_cursos para categorías
    public $cursos, $cursoId,$cursoQuestions;

//    preguntas
    public $questionName, $questionImage,$questionNumber;
//    alternativas
    public $alterName = [], $alterImg =[],$alterValor;
    public $i = 0, $inputs = [];

//    editar
    public $idEdit, $textEdit, $typeEdit;

    public function mount()
    {
        $persona = Auth::user()->persona;
//        optener periodo
        $period = Periodo::where('estado', '1')->first();
        $this->periodDes = $period->descripcion;
        $this->periodId = $period->id;
//        obtener los meses
        $this->months = collect();
        $now = Carbon::now()->locale('es');
        $this->month = $now->month . '-' . $now->year;
        $monthNum = $now->month;
        while ($monthNum <= 12) {
            $data = [
                'value' => $now->month . '-' . $now->year,
                'text' => $now->monthName . '-' . $now->year
            ];
            $monthNum += 1;
            $now->addMonth();
            $this->months->push($data);
        }
//        validar si hay un exámen registrado
        $this->examId = Examen::with('periodo')->where('mes_ano', '=', Carbon::now()->month . '-' . Carbon::now()->year)->first()->id ?? null;
        if ($this->examId) {
            $exam = Examen::find($this->examId);
            $this->month = $exam->mes_ano;
            $this->date = $exam->fecha_hora->format('Y-m-d');
            $this->hour = $exam->fecha_hora->format('H:i');
            $this->duration = $exam->duracion;
        }
//        lista de secciones por docente de 5 grado de secundaria
        $this->cursos = Curso::with('pe_cursos.secciones')
            ->whereHas('pe_cursos.secciones.docentes', function ($q) use ($persona) {
                $q->where('persona_id', $persona->id);
            })
            ->whereHas('pe_cursos.pe_carrera', function ($q) {
                $q->where('carrera_id', 3);
            })
            ->whereHas('pe_cursos', function ($q) {
                $q->where('semestre', 5);
            })
            ->get();
//        preguntas
        $this->preguntas = collect();
    }

    public function render()
    {
        if ($this->cursoId) {
            $examId = $this->examId;
            $preguntas = Examen::find($examId)->preguntas->where('curso_id', $this->cursoId);
            $this->questionNumber=count($preguntas);
            $this->cursoQuestions=Curso::find($this->cursoId)->preg_simulacro;
        } else
            $preguntas = collect();
        return view('livewire.question-register', ['preguntas' => $preguntas]);
    }

    public function storeExam()
    {

        $this->validate([
            'periodId' => 'required',
            'month' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'duration' => 'required'
        ], [
            'required' => '*este campo es obligatorio'
        ]);
        $exam = new Examen();
        $exam->periodo_id = $this->periodId;
        $exam->mes_ano = $this->month;
        $exam->fecha_hora = $this->date . ' ' . $this->hour;
        $exam->duracion = $this->duration;
        $exam->save();
        $this->examId = $exam->id;
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        $this->i-=1;
        unset($this->inputs[$i]);
    }

    public function storeQuestion()
    {

        $this->validate([
            'cursoId' => 'required',
            'alterName.*' => 'required',
            'alterValor' => 'required',
        ]
//            , [
//            'required' => '* Campo Obligatorio'
//        ]
        );
//        for ($i=0;$i<=$this->i;$i++){
//            $this->validate([
//                'alterName.*' => 'required',
//            ]);
//        }
        DB::beginTransaction();
        $question = new BpPregunta();
        $question->curso_id = $this->cursoId;
        $question->pregunta = $this->questionName;
        if ($this->questionImage != null) {
            $s_path = $this->questionImage->storeAs('public/ImgQuestionSim', 'imgQuestion-'.date('Ymdhis').$this->cursoId.'-' .$this->questionNumber. '.' . $this->questionImage->extension());
            $p_patc = Storage::url($s_path);
            $question->url_imagen = $p_patc;
        }
        $question->save();
        for ($i = 0; $i <= $this->i; $i++) {
            $alter = new BpAlternativa();
            $alter->bp_pregunta_id = $question->id;
            $alter->alternativa = $this->alterName[$i];
            if (array_key_exists($i, $this->alterImg ?? [])) {
                $s_path = $this->alterImg[$i]->storeAs('public/imgAlternativasSim', 'imgAlter_'.date('Ymdhis').$question->id.'_'.$i . '.' . $this->alterImg[$i]->extension());
                $p_patc = Storage::url($s_path);
                $alter->url_img = $p_patc;
            }
            $alter->valor = $this->alterValor == $i ? '1' : '0';
            $alter->save();
        }
        Examen::find($this->examId)->preguntas()->attach($question->id);
//        $this->clearAlter();
        DB::commit();
        $this->questionName = null;
        $this->questionImage = null;
        $this->alterName = null;
        $this->alterValor = null;
        $this->alterImg=null;
        $this->i = 0;
        $this->inputs = [];
    }

//    protected function clearAlter()
//    {
//        $this->questionName = null;
//        $this->questionImage = null;
//        $this->alterName = null;
//        $this->alterValor = null;
//        $this->i = 0;
//        $this->inputs = [];
//    }

    public function edit($id, $type)
    {
        if ($type == 'question') {
            $question = BpPregunta::find($id);
            $this->idEdit = $question->id;
            $this->textEdit = $question->pregunta;
            $this->typeEdit = $type;
        }
        if ($type == 'alter') {
            $alter = BpAlternativa::find($id);
            $this->idEdit = $alter->id;
            $this->textEdit = $alter->alternativa;
            $this->typeEdit = $type;
        }
    }

    public function saveEdit()
    {
        if ($this->typeEdit == 'question') {
            $question = BpPregunta::find($this->idEdit);
            $question->pregunta = $this->textEdit;
            $question->save();
        }
        if ($this->typeEdit == 'alter') {
            $alter = BpAlternativa::find($this->idEdit);
            $alter->alternativa = $this->textEdit;
            $alter->save();
        }
        $this->idEdit = null;
        $this->textEdit = null;
        $this->typeEdit = null;
    }

    public function checkTrue($questionId, $alterId)
    {
        BpAlternativa::with('dbPregunta')->where('bp_pregunta_id', $questionId)->update(['valor' => '0']);
        BpAlternativa::find($alterId)->update(['valor' => '1']);
    }

    public function deleteQuestion($questionId)
    {
        $pregunta = BpPregunta::find($questionId);
        $alternativas=$pregunta->bpAlternativas;
        foreach ($alternativas as $alter){
            if ($alter->url_img) {
                File::delete(public_path($alter->url_img));
            }
        }
        $pregunta->bpAlternativas()->delete();
        Examen::find($this->examId)->preguntas()->detach($questionId);
        if ($pregunta->url_imagen) {
            File::delete(public_path($pregunta->url_imagen));
        }
        $pregunta->delete();
    }

    public function deleteAlter($alterId)
    {
        $alter=BpAlternativa::find($alterId);
        if ($alter->url_img) {
            File::delete(public_path($alter->url_img));
        }
        $alter->delete();
    }
}
