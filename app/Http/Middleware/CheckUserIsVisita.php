<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserIsVisita
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $module_id=session('module_id');
        $has_auth=Auth::user()->modulos->where('code','visita')->isNotEmpty();

        if( $module_id==null || $module_id=='default' || !$has_auth){
            return redirect()->route('module_chooser');
        }
        return $next($request);
    }
}
