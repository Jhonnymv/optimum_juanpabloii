<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIfUserIsAlumno
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $tipoUsuario = Auth::user()->tipo;
        // if ($tipoUsuario == 'alumno') {
        //     return $next($request);
        // }

        // switch ($tipoUsuario) {
        //     case 'docente':
        //         return redirect()->route('IntranetDocente.silabos.index');
        //         break;
        //     case 'administrador':
        //         return redirect()->route('/admin');
        //         break;
        //     case 'secretaria_academica':
        //         return redirect()->route('secretaria.index');
        //         break;
        // }

        $module_id=session('module_id');
        $has_auth=Auth::user()->modulos->where('code','intranet_alumno')->isNotEmpty();

        if( $module_id==null || $module_id=='default' || !$has_auth){
            return redirect()->route('module_chooser');
        }


        return $next($request);
    }
}
