<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $fillable=['seccion','grupo','cupos','periodo_id','pe_curso_id','n_matriculados'];
    protected $table="secciones";

    public function periodo()
    {
         return $this->belongsTo('App\Periodo');
    }

//     public function docente()
//     {
//          return $this->belongsTo('App\Docente');
//     }

    public function pe_curso()
    {
         return $this->belongsTo('App\PeCurso','pe_curso_id','id');
    }

    public function horarios()
    {
        return $this->hasMany('App\Horario','seccion_id','id');
    }

    public function docentes()
    {
        return $this->belongsToMany('App\Docente')->withPivot('rol')->withTimestamps();
    }

    public function silabo()
    {
        return $this->hasOne('App\silabo','seccion_id','id');
    }

    public function unidades()
    {
        return $this->hasMany('App\unidades');
    }

    public function productos()
    {
        return  $this->hasMany('App\Producto');
        //return $this->hasOne('App\Producto','id','producto_id');
    }

    public function docente_seccion()
    {
         return $this->hasMany('App\DocenteSeccion','seccion_id','id');
//         return $this->hasMany('App\DocenteSeccion' ) ;
    }
    public function docente_seccionb()
    {
         return $this->hasone('App\DocenteSeccion','seccion_id','id');
//         return $this->hasMany('App\DocenteSeccion' ) ;
    }
    public function matricula_detalles()
    {
        return $this->hasMany('App\MatriculaDetalle','seccion_id','id');
    }

    public function matricula()
    {
        return $this->belongsToMany('App\Matricula','matricula_detalles','seccion_id','matricula_id','id','id');
    }

    public function programation()
    {
        return $this->hasOne(Programation::class,'seccion_id');
    }
}
