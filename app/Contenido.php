<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{
     //public $fillable = ['unidades_id','nro_semana','fecha_incio','fecha_fin','contenido','destrezas','estrategias','valores_actitudes'];
    
    public function unidad()
    {
        return $this->hasOne('App\unidades');
    }
}
