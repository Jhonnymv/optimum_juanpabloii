<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    protected $table="asignaciones";

    public function alumno()
    {
        return $this->belongsToMany(Alumno::class,'asignacion_alumno')->withPivot('respuesta','doc_respuesta','nota')->withTimestamps();
    }
}
