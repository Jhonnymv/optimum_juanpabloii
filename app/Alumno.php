<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    public function persona()
    {
         return $this->belongsTo('App\Persona');
    }
    public function carreras(){
        return $this->belongsToMany('App\Carrera','alumno_carreras')->withPivot('id');
    }
    public function alumnocarreras(){
        return $this->hasMany('App\AlumnoCarrera','alumno_id','id');
    }
    public function pe_carreras(){
        return $this->belongsToMany('App\PeCarrera','alumno_carreras','alumno_id','pe_carrera_id')->withPivot('id');
    }
    public function matricula(){
        return $this->hasManyThrough('App\Matricula','App\AlumnoCarrera','alumno_id','alumno_carrera_id','id','id');
    }

    public function notas(){
        return $this->hasMany('App\Notas','alumno_id','id');
    }

    public function padre()
    {
        return $this->belongsTo('App\Padre');
    }
    public function madre()
    {
        return $this->belongsTo('App\Padre','madre_id');
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Categoria', 'alumno_categoria')->withPivot('id','estado')->withTimestamps();
    }

    public function categoriaActiva()
    {
        return $this->belongsToMany('App\Categoria', 'alumno_categoria')->wherePivot('estado','=',1)->withPivot('id','estado')->withTimestamps();
    }

    public function cronograma()
    {
        return $this->belongsToMany('App\Cronograma','alumno_pago')->withPivot('id','estado','pago_id')->withTimestamps();
    }

    public function pago()
    {
        return $this->belongsToMany('App\Pago','alumno_pago')->withPivot('id','estado')->withTimestamps();
    }

    public function alumnoPago()
    {
        return $this->hasMany('App\Alumnopago');
    }

    public function asignaciones()
    {
        return $this->belongsToMany(Asignacion::class,'asignacion_alumno')->withPivot('respuesta','doc_respuesta','nota')->withTimestamps();
    }
}
