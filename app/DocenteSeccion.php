<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocenteSeccion extends Model
{
    protected $table="docente_seccion";

    public $fillable = ['id','docente_id','seccion_id','rol','created_at','updated_at'];

    public function docente()
    {
         return $this->belongsto('App\Docente','docente_id','id');
    }
    public function seccion()
    {
        return $this->belongsto('App\Seccion','seccion_id','id');
    }

}
