<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table='vouchers';
    protected $fillable=['fecha','num_operacion','monto','estado','tipo','urlimg','matricula_id','postulacion_id'];

    public function matricula()
    {
        return $this->belongsTo('App\Matricula','matricula_id','id');
    }
    public function licencia()
    {
        return $this->belongsTo('App\Licencia','licencia_id','id');
    }
    public function postulacion()
    {
        return $this->belongsTo('App\Postulacon','postulacion_id','id');
    }
    public function getUrlPathAttribute()
    {
        return \Storage::url($this->urlimg);
    }
    public function url_img_tests(){
        return asset($this->urlimg);
    }

}
