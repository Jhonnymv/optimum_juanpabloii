<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table='asistencias';

    public $fillable = ['asistencia','fecha_sesion','fecha_create',
                   'seccion_id','alumno_id','horario_detalle_id'];

    public function alumno()
    {
        return $this->belongsTo('App\Alumno','alumno_id','id');
    }

    public function horario()
    {
        return $this->belongsTo('App\Horario','horario_detalle_id','id');
    }

}
