<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Padre extends Model
{
    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function alumno()
    {
        return $this->hasMany('App\Alumno');
    }

    public function alumnop()
    {
        return $this->hasMany(Alumno::class, 'padre_id');
    }

    public function alumnom()
    {
        return $this->hasMany(Alumno::class, 'madre_id');
    }
}
