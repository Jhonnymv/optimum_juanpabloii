<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    //


//     public function secciones()
//     {
//          return $this->hasMany('App\Seccion');
//     }

    public function persona()
    {
         return $this->belongsTo('App\Persona','persona_id','id');
    }

    public function secciones()
    {
        return $this->belongsToMany('App\Seccion')->withPivot('rol')->withTimestamps();
    }

    public function carreras()
    {
        return $this->belongsToMany('App\Carrera')->withPivot('fecha_inicio', 'fecha_fin','estado_gestion')->withTimestamps();
    }

    public function areasAcademicas()
    {
        return $this->belongsToMany('App\Areas_academica')->withPivot('fecha_inicio', 'fecha_fin','estado_gestion')->withTimestamps();
    }
    public function area_academica_docentes(){
        return $this->hasMany('App\Areas_academica');
    }

    public function dcarreras(){
        return $this->belongsToMany('App\Carrera','carrera_docente','docente_id','carrera_id')->withTimestamps();
    }
}
