<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    public function provincia()
    {
        return $this->belongsTo('App\Provincia');
    }

    public function postulantes()
    {
        return $this->hasMany('App\Postulante');
    }
    public function persona()
    {
        return $this->hasMany('App\Persona');
    }

}
