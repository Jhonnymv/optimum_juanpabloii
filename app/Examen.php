<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examen extends Model
{
    protected $dates=['fecha_hora'];
    protected $table='examenes';

    public function carrera()
    {
        return $this->belongsTo('App\Carrera', 'carrera_id','id');
    }
    public function periodo()
    {
        return $this->belongsTo('App\Periodo', 'periodo_id','id');
    }

    public function preguntas()
    {
        return $this->belongsToMany('App\BpPregunta','examen_preguntas');
    }
}
