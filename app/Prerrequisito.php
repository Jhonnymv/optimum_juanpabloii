<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prerrequisito extends Model
{
    //
    public $fillable = ['curso_pre_id','pe_curso_id','estado'];

    public function pecurso()
    {
      return $this->belongsTo('App\PeCurso','pe_curso_id','id');
    }
    public function cursopre()
    {
         return $this->belongsTo('App\Curso','curso_pre_id','id');
    }

}
