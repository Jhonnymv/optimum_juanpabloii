<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class silabo_docente extends Model
{
    public $fillable = ['silabo_id','docente_id','rol','silaboscol'];
    public function docente()
    {
        
        return $this->belongsTo('App\Docente');
    }
}
