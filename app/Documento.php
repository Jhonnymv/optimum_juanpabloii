<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    public function alumno()
    {
        return $this->belongsTo('App\Alumno');
    }
}
