<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //

    protected $table='personas';

    public function docente()
    {
         return $this->hasOne('App\Docente');
    }

    public function alumno()
    {
         return $this->hasOne('App\Alumno');
    }

    public function usuario()
    {
         return $this->hasOne('App\Usuario');
    }

    public function postulante()
    {
        return $this->hasOne('App\Postulante');
    }

    public function fullName()
    {
         return $this->nombres.' '.$this->paterno.' '.$this->materno;
    }

    public function padre()
    {
        return $this->hasOne('App\Padre');
    }

    public function distrito()
    {
        return $this->belongsTo('App\Distrito');
    }


}
