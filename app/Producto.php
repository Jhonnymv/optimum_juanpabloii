<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table='productos';

    public $fillable = ['producto','estado','fecha','fecha_cierre',
    'seccion_id','evaluacion_categoria_id','instrumento_id'];

    public function producto_instrumentos()
    {
        return $this->hasMany('App\ProductoInstrumento');
    }
    public function evaluacion_categoria()
    {
        return $this->belongsTo('App\EvaluacionCategoria','evaluacion_categoria_id','id');
    }
    public function notas()
    {
        return $this->hasManyThrough('App\Nota','App\ProductoInstrumento','producto_id','producto_instrumento_id','id','id');
    }

    public function seccion()
    {
        return  $this->belongsTo('App\Seccion','seccion_id','id');
        //return $this->hasMany('App\Seccion');
    }


}
