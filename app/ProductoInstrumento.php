<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoInstrumento extends Model
{
    protected $table='producto_instrumentos';

    public $fillable = ['producto_id','instrumento_id','estado','fecha','contenido_id'];

    public function instrumentos()
    {

        return $this->belongsTo('App\Instrument','instrumento_id','id');
    }

    public function contenidos(){

        return $this->belongsTo('App\Contenido','contenido_id','id');

    }

    public function productos(){

        return $this->belongsTo('App\Producto','producto_id','id');

    }
    public function nota()
    {
        return $this->hasMany('App\Nota','producto_instrumento_id','id');
    }

}
