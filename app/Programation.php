<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programation extends Model
{

    public function seccion()
    {
        return $this->belongsTo(Seccion::class,'seccion_id');
    }

    public function unidades()
    {
        return $this->hasMany(unidades::class,'programation_id');
    }

    public function documentos()
    {
        return $this->hasMany(Doc_programation::class,'programation_id');
    }
}
