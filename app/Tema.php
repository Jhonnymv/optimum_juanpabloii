<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    public function materialesApoyo()
    {
        return $this->hasMany(MaterialApoyo::class,'tema_id');
    }

    public function unidad()
    {
        return $this->belongsTo(unidades::class,'unidad_id');
    }
}
