<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CriterioDimension extends Model
{
    protected $table='criterio_dimension';

    public function dimension()
    {
        return $this->belongsTo('App\Dimension','dimension_id','id');
    }
}
