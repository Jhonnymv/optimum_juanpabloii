<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AnexoSolicitud extends Model
{
    protected $table='anexo_solicitudes';
    public function solicitud()
    {
        return $this->belongsTo('App\Solicitud','solicitud_id','id');
    }
    public function getUrlPathAttribute()
    {
        return Storage::url($this->url);
    }
}
