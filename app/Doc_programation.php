<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doc_programation extends Model
{
    public function programation()
    {
        return $this->belongsTo(Doc_programation::class.'programation_id');
    }
}
