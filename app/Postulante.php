<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulante extends Model
{
    public function persona()
    {
        return $this->belongsTo('App\Persona','persona_id','id');
    }

    public function postulaciones(){
        return $this->hasMany('App\Postulacion','postulante_id','id');
    }
    public function distrito()
    {
        return $this->belongsTo('App\Distrito');
    }

    public function colegio()
    {
        return $this->belongsTo('App\Colegio');
    }

    public function entrevista_postulante()
    {
        return $this->hasMany('App\EntrevistaPostulante','postulante_id','id');
    }

    public function examen_postulante()
    {
        return $this->hasMany('App\ExamenAlumno');
    }
}
