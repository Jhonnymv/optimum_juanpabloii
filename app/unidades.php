<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unidades extends Model
{
    protected $table='unidades';

//    protected $dates=['fecha_inicio','fecha_fin'];

    // public $fillable = ['silabo_id','unidad','fecha_incio','fecha_fin','duracion'];

    // public function silabo()
    // {
    //     return $this->belongsTo('App\silabo');
    // }

    public $fillable = ['seccion_id','capacidades','unidad','denominacion','fecha_incio','fecha_fin'];

    public function silabo()
    {
        return $this->belongsTo('App\silabo');
    }

    public function contenidos()
    {
        return $this->hasMany('App\Contenido');
    }

    public function desempeno_criterios(){
        return $this->hasMany('App\Desempeno_criterio');
    }

    public function fase()
    {
        return $this->belongsTo(Fase::class,'fase_id');
    }

    public function programacion()
    {
        return $this->belongsTo(Programation::class,'programation_id');
    }

    public function temas()
    {
        return $this->hasMany(Tema::class,'unidad_id');
    }

    public function asignaciones()
    {
        return $this->hasMany(Asignacion::class,'unidad_id');
    }
}
