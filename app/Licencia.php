<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia extends Model
{
    protected $table='licencias';
    public $fillable = ['id','matricula_id','descripcion','estado','fecha_registro','fecha_cierre','duracion','resolucion'];
    
 public function matricula()
  {
       return $this->belongsto('App\Matricula','matricula_id');
  }
}
