<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanEstudio extends Model
{
    //
    public $fillable = ['plan','fecha_inicio','fecha_fin','estado'];
    

    public function pecarreras()
    {
        return $this->hasMany('App\PeCarrera');
    }
}
