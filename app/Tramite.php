<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $table='tramites';

    public function unidad_administrativa()
    {
        return $this->belongsTo('App\UnidadAdministrativa','unidad_administrativa_id','id');
    }
}
