<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table='cursos';
    public $fillable = ['nombre','descripcion','estado','carrera_id'];

    public function carrera()
    {
        return $this->belongsTo('App\Carrera');
    }
    public function pe_cursos()
    {
        return $this->hasMany('App\PeCurso');
    }

    public function preguntas()
    {
        return $this->hasMany(BpPregunta::class);
    }

}
