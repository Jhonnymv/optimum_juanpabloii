<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeCarrera extends Model
{
    //
    public $fillable = ['plan_estudio_id','carrera_id','estado'];
    //protected $primarykey=['plan_estudio_id','carrera_id'];

    public function planestudio()
    {
        return $this->belongsTo('App\PlanEstudio','plan_estudio_id','id');
    }

    public function carrera()
    {
        return $this->belongsTo('App\Carrera','carrera_id','id');
    }

    public function pe_curso()
    {
        return $this->hasMany('App\PeCurso','pe_carrera_id','id');
    }

    public function postulacion()
    {
        return $this->hasMany('App\Postulacion', 'pe_carrera_id');
    }

    public function cursos()
    {
        return $this->belongsToMany('App\Curso','pe_cursos','pe_carrera_id','curso_id')->withPivot('semestre','horas_teoria','horas_practica','creditos');
    }

}
