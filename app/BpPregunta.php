<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BpPregunta extends Model
{
    public function categoria()
    {
        return $this->belongsTo('App\BpCategoria','bp_categoria_id','id');
    }

    public function bpAlternativas()
    {
        return $this->hasMany('App\BpAlternativa','bp_pregunta_id','id');
    }

    public function examen()
    {
        return $this->belongsToMany(Examen::class,'examen_preguntas')->withTimestamps();
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class,'curso_id');
    }

}
