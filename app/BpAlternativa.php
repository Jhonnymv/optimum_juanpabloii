<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BpAlternativa extends Model
{
    protected $fillable=['valor'];
    public function dbPregunta()
    {
        return $this->belongsTo('App\BdPregunta');
    }
}
