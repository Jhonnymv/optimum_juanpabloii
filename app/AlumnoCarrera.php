<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumnoCarrera extends Model
{
    protected $table='alumno_carreras';

    protected $fillable=['carrera_id','alumno_id','fecha_ingreso','fecha_egreso','grupo'];

    public function alumno()
    {
        return $this->belongsTo(Alumno::class,'alumno_id','id');
    }
    public function carrera()
    {
        return $this->belongsTo('App\Carrera','carrera_id','id');
    }

    public function matriculas()
    {
        return $this->hasMany('App\Matricula','alumno_carrera_id','id');
    }
}
