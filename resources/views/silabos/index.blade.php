@extends('Layouts.main')
@section('title','silabos')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Sílabos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Sílabo</span>
    </a>
</div>
@endsection
@section('header_subtitle')
<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Sílabos</span>
@endsection
@section('header_subbuttons')

@endsection
@section('content')
@if (Session::has('Mensaje'))
{{
    Session::get('Mensaje')
}}
@endif

<table class="table table-light">
    <thead class="table-light">
        <tr>
            <th>#</th>
            <th>Sílabos col</th>
            <th>Curso</th>
            <th>Periodo Académico</th>
            <th>Horas Teoría</th>
            <th>Horas Práctica</th>
            <th>Condiciones</th>
            <th>Referencias Bibliográficas</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($silabos as $item)
        <tr>
            <td scope="row">{{$loop -> iteration}}</td>
            <td>{{$item -> silaboscol}}</td>
            <td> {{ $item->curso != null? $item->curso->nombre:'' }} </td>
            <td> {{ $item->periodoacademico != null? $item->periodoacademico->descripcion:'hola' }} </td>
            <td>{{$item -> horas_practica}}</td>
            <td>{{$item -> horas_teoria}}</td>
            <td>{{$item -> condiciones}}</td>
            <td>{{$item -> referencias_bib}}</td>



            <td>
                <a href="{{url('/unidades/'.$item -> id)}}">Ver Unidades</a>
                |
                <form method="post" action="{{url('/docentes/'.$item -> silabo_id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button type="submit" onclick="return confirm('¿Desea borrar el registro?');">Borrar</button>


                    <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id }})"><i
                            class="fas fa-trash"></i></a>
                    <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})"><i class="fas fa-pen"></i></a>

                </form>
            </td>
        </tr>
        @endforeach
        <tr>

        </tr>
    </tbody>
</table>
@endsection

@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/silabos">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo Sílabo</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Silabos Col:</label>
                    <div class="col-lg-9">
                        <input type="text" name="silabocol" class="form-control" placeholder="Descripción..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Curso:</label>
                    <div class="col-md-10">
                        <select name="curso" class="form-control">

                            @forelse ($cursos as $item)
                            <option value="{{$item->id}}">
                                {{$item->nombre}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Periodo Académico:</label>
                    <br>
                    <div class="col-md-10">
                        <select name="periodoacademico" class="form-control">

                            @forelse ($periodoacademicos as $itemb)
                            <option value="{{$itemb->id}}">
                                {{$itemb->descripcion}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Horas Teoria:</label>
                    <div class="col-lg-9">
                        <input type="text" name="horas_teoria" class="horasform-control" placeholder="Horas Teoría..."
                            required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Horas Práctica:</label>
                    <div class="col-lg-9">
                        <input type="text" name="horas_practica" class="horasform-control"
                            placeholder="Horas Práctica..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Condiciones:</label>
                    <div class="col-lg-10">
                        <textarea rows="3" cols="3" name="condiciones" class="form-control"
                            placeholder="Condiciones...."></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Referencias Bibliográficas:</label>
                    <div class="col-lg-10">
                        <textarea rows="3" cols="3" name="referencias_bib" class="form-control"
                            placeholder="Referencias Bibliográficas...."></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Criterios de evaluación:</label>
                    <div class="col-lg-10">
                        <textarea rows="3" cols="3" name="criterios_evaluacion" class="form-control"
                            placeholder="Criterios de evaluación..."></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"></label>
                        <div class="col-lg-9">
                            <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/periodosacademicos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Periodo AAcadémico?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Periodo Académico</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<!-- editar modal -->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/silabos">
            @method('PUT');
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Actualizar Sílabos</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Silabos Col:</label>
                    <div class="col-lg-9">
                        <input type="text" name="silabocoledit" id="silabocoledit" class="form-control"
                            placeholder="Descripción..." required>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Curso:</label>
                        <div class="col-md-10">
                            <select name="cursoedit" , id="cursoedit" class="form-control">

                                @forelse ($cursos as $item)
                                <option value="{{$item->id}}">
                                    {{$item->nombre}}
                                </option>
                                @empty

                                @endforelse

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Periodo Académico:</label>
                        <br>
                        <div class="col-md-10">
                            <select name="periodoacademicoedit" id="periodoacademicoedit" class="form-control">

                                @forelse ($periodoacademicos as $itemb)
                                <option value="{{$itemb->id}}">
                                    {{$itemb->descripcion}}
                                </option>
                                @empty

                                @endforelse

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Horas Teoria:</label>
                        <div class="col-lg-9">
                            <input type="text" name="horas_teoriaedit" id="horas_teoriaedit" class="horasform-control"
                                placeholder="Horas Teoría..." required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Horas Práctica:</label>
                        <div class="col-lg-9">
                            <input type="text" name="horas_practicaedit" id="horas_practicaedit"
                                class="horasform-control" placeholder="Horas Práctica..." required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Condiciones:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" name="condicionesedit" id="condicionesedit" class="form-control"
                                placeholder="Condiciones...."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Referencias Bibliográficas:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" name="referencias_bibedit" id="referencias_bibedit"
                                class="form-control" placeholder="Referencias Bibliográficas...."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Criterios de evaluación:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" name="criterios_evaluacionedit" id="criterios_evaluacionedit"
                                class="form-control" placeholder="Criterios de evaluación..."></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>

@endsection


@section('script')
<script>
    function eliminar(id)
    {
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/periodosacademicos/remove/'+id);
    }

    function editar(id)
    {
        console.log(id);
        $("#formEditar").attr('action','/silabos/'+id);
        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traersilabo',
                data: data,
                success: function(data) {
                    console.log('data');
                    $('#silabocoledit').val(data['silabo']['silaboscol']);
                    $('#cursoedit').val(data['silabo']['curso_id']);
                    $('#periodoacademicoedit').val(data['silabo']['periodo_academico_id']);
                    $('#horas_teoriaedit').val(data['silabo']['horas_teoria']);
                    $('#horas_practicaedit').val(data['silabo']['horas_practica']);
                    $('#condicionesedit').val(data['silabo']['condicciones']);
                    $('#referencias_bibedit').val(data['silabo']['referencias_bib']);
                    $('#criterios_evaluacionedit').val(data['silabo']['criterios_evalucion']);
                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>

@endsection
