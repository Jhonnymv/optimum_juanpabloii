@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>

        .rombo {
            width: 70px;
            height: 70px;
            border: 3px solid #555;
            background: #7a929e;
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            transform: rotate(45deg);
            margin: 20px 20px;
        }

        .rectangulo {
            margin: 20px 20px;
            width: 100px;
            height: 70px;
            border: 3px solid #555;
            background: #7a929e;
        }

        .circulo {
            width: 70px;
            height: 70px;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            background: #7a929e;
            margin: 20px 20px;
            border: 3px solid #555;
        }

        #containment-wrapper {
            width: 200px;
            border: 2px solid #ccc;
            padding: 10px;
            display: flex;
            height:50em;
            overflow: scroll;
        }

        h3 {
            clear: left;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-3">
            <h3>Herramientas:</h3>
            <div class="circulo draggable ui-widget-content text-center"></div>
            <div class="rombo draggable ui-widget-content text-center">
            </div>
            <div class="rectangulo draggable ui-widget-content">
            </div>
        </div>
        <div class="col-12 col-md-9 text-center" id="containment-wrapper">

        </div>
    </div>
@endsection
@section('modals')
    <div id="modal-inicio" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nuevo Proceso</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nombre del proceso</label>
                        <input type="text" name="proccess_name" id="" class="form-control form-control">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link ">Gardar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-tarea" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tarea</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nombre del proceso</label>
                        <input type="text" name="task_name" id="" class="form-control form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Dependencia</label>
                        <select class="form-control">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">condicional</label>
                        <select class="form-control">
                            <option>SI</option>
                            <option>NO</option>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link ">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-decision" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Basic modal</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Pregunta</label>
                        <input type="text" name="Pregunta" id="" class="form-control form-control">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link ">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('assets/js/jquery.connections.js')}}"></script>
    <script>
        $(function () {
            $(".draggable").draggable({
                helper: 'clone',
                revert: false,
            });
            var cc = 1;
            var cr = 1;
            var cre = 1;
            $('#containment-wrapper').droppable({
                accept: '.draggable',
                scroll:true,
                drop: function (ev, ui) {
                    if (ui.draggable.hasClass('circulo')) {
                        if (cc == 1) {
                            var droppendItem = $(ui.draggable).clone()
                                .css('margin', '50px 50px')
                                .removeClass('draggable')
                                .addClass('child')
                                .attr('ondblclick', "$('#modal-inicio').modal('show')")
                                .html("<p style='margin-top: 40%; color: white'>Inicio</p>").draggable({containment: "#containment-wrapper", scroll: false});
                            cc += 1
                        } else {
                            var droppendItem = $(ui.draggable).clone()
                                .css('margin', '50px 50px')
                                .removeClass('draggable')
                                .addClass('child')
                                .text('fin')
                                .draggable({containment: "#containment-wrapper", scroll: false});
                        }
                    } else if (ui.draggable.hasClass('rombo')) {
                        var droppendItem = $(ui.draggable).clone()
                            .css('margin', '50px 50px')
                            .removeClass('draggable')
                            .addClass('child')
                            .attr('ondblclick', "$('#modal-decision').modal('show')")
                            .html("<p style='color: white; transform: rotate(-45deg); margin-left: -10px; margin-top: 40%'>Decisión " + cr + "</p>")
                            .draggable({containment: "#containment-wrapper", scroll: false});

                        cr += 1
                    } else if (ui.draggable.hasClass('rectangulo')) {
                        var droppendItem = $(ui.draggable).clone()
                            .css('margin', '50px 50px')
                            .removeClass('draggable')
                            .addClass('child')
                            .attr('ondblclick', "$('#modal-tarea').modal('show')")
                            .html("<p style='margin-top: 20%; color: white'>Tarea" + cre + "</p>")
                            .draggable({containment: "#containment-wrapper", scroll: false});
                        cre += 1
                    }
                    $(this).append(droppendItem);
                }
            });
            $('.child').draggable({
                containment: "#containment-wrapper", scroll: false
            })
        });
    </script>
@endsection

