@extends('Layouts.main')
@section('title','CredencialesAlumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Alumnos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Alumno</span>
    </a>
</div>
@endsection
@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Alumnos</span>
@endsection
@section('header_subbuttons')

@endsection
@section('content')
@if (Session::has('Mensaje'))
{{
    Session::get('Mensaje')
}}
@endif
<a href="{{url('docentes/create')}}">Agregar Alumno</a>
<table class="table table-light">
    <thead  class="table-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Nombres</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>DNI</th>
            <th>Sexo</th>
            <th>Fecha de Nacimiento</th>
            <th>Procedencia</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>Usuario</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($alumnos as $alumnos)
        <tr>
            <td scope="row">{{$loop -> iteration}}</td>
            <td>{{$alumnos -> urlimg}}</td>
            <td>{{$alumnos -> nombres}}</td>
            <td>{{$alumnos -> paterno}}</td>
            <td>{{$alumnos -> materno}}</td>
            <td>{{$alumnos -> DNI}}</td>
            <td>{{$alumnos -> sexo}}</td>
            <td>{{$alumnos -> fecha_nacimiento}}</td>
            <td>{{$alumnos -> procedencia}}</td>
            <td>{{$alumnos -> email}}</td>
            <td>{{$alumnos -> telefono}}</td>
            <td>{{$alumnos -> usuario}}</td>
            <td>{{$alumnos -> estado}}</td>
            <td>
            {{--  <a href="{{url('/docentes/'.$docentes -> id.'/edit')}}">Editar</a>
            |
            <form method="post" action="{{url('/docentes/'.$docentes -> id)}}">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" onclick="return confirm('¿Desea borrar el registro?');">Borrar</button>  --}}

            <a href="#" class="btn btn-danger" onclick="eliminar({{ $alumnos->id }})"><i class="fas fa-trash"></i></a>
            <a href="#" class="btn btn-warning" onclick="editar({{ $alumnos->id }})"><i  class="fas fa-pen"></i></a>


            </form>
            </td>
        </tr>
    @endforeach
        <tr>

        </tr>
    </tbody>
</table>
@endsection

@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/alumnos">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo alumno</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombres:</label>
                    <div class="col-lg-9">
                        <input type="text" name="nombres" class="form-control" placeholder="Nombres..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Paterno:</label>
                    <div class="col-lg-9">
                        <input type="text" name="Paterno" class="form-control" placeholder="Apellido paterno..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Materno:</label>
                    <div class="col-lg-9">
                        <input type="text" name="Materno" class="form-control" placeholder="Apellido Materno..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">DNI:</label>
                    <div class="col-lg-9">
                        <input type="text" name="DNI" class="form-control" placeholder="DNI..." required>
                    </div>
                </div>
                <div class="form-group">
					<label class="d-block">Sexo:</label>
                        <div class="form-check form-check-inline">
							<label class="form-check-label">
								<input type="radio" class="form-input-styled" name="sexo" value="M">
								Masculino
							</label>
						</div>
				        <div class="form-check form-check-inline">
					        <label class="form-check-label">
							    <input type="radio" class="form-input-styled" name="sexo" value="F">
								Femenino
							</label>
						</div>
			    </div>
                <div class="form-group row">
									<label class="col-form-label col-md-2">Fecha de nacimiento</label>
									<div class="col-md-10">
										<input class="form-control" type="date" name="fecha_nacimiento">
									</div>
								</div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">teléfono:</label>
                    <div class="col-lg-9">
                        <input type="text" name="telefono" class="form-control" placeholder="Teléfono..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Procedencia:</label>
                    <div class="col-lg-9">
                        <input type="text" name="procedencia" class="form-control" placeholder="Procedencia..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email:</label>
                    <div class="col-lg-9">
                        <input type="text" name="email" class="form-control" placeholder="Email..." required>
                    </div>
                </div>
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Usuario:</label>
                    <div class="col-lg-9">
                        <input type="text" name="usuario" class="form-control" placeholder="Usuario..." required>
                    </div>
                </div>
                <!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Password<span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="password" id="password" class="form-control" required placeholder="Minimum 5 characters allowed">
									</div>
								</div>
								<!-- /password field -->
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/alumnos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar Alumno?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Alumno</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/alumnos">
            @method('PUT');
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Actualizar alumno</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombres:</label>
                    <div class="col-lg-9">
                        <input type="text"  id="nombresEdit" name="nombresEdit" class="form-control" placeholder="Nombres..." required>
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Paterno:</label>
                    <div class="col-lg-9">
                        <input type="text" id="PaternoEdit" name="PaternoEdit" class="form-control" placeholder="Apellido paterno..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Materno:</label>
                    <div class="col-lg-9">
                        <input type="text" id="MaternoEdit" name="MaternoEdit" class="form-control" placeholder="Apellido Materno..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">DNI:</label>
                    <div class="col-lg-9">
                        <input type="text" id="DNIEdit" name="DNIEdit" class="form-control" placeholder="DNI..." required>
                    </div>
                </div>
                <div class="form-group">
					<label class="d-block">Sexo:</label>
                        <div class="form-check form-check-inline">
							<label class="form-check-label">
								<input type="radio" class="form-input-styled" id="sexoEdit" name="sexoEdit" value="M" required>
								Masculino
							</label>
						</div>
				        <div class="form-check form-check-inline">
					        <label class="form-check-label">
							    <input type="radio" class="form-input-styled" id="sexoEdit" name="sexoEdit" value="F" required>
								Femenino
							</label>
						</div>
			    </div>
                <div class="form-group row">
									<label class="col-form-label col-md-2">Fecha de nacimiento</label>
									<div class="col-md-10">
										<input class="form-control" type="date" id="fecha_nacimientoEdit" name="fecha_nacimientoEdit">
									</div>
								</div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">teléfono:</label>
                    <div class="col-lg-9">
                        <input type="text" id="telefonoEdit" name="telefonoEdit" class="form-control" placeholder="Teléfono..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Procedencia:</label>
                    <div class="col-lg-9">
                        <input type="text" id="procedenciaEdit" name="procedenciaEdit" class="form-control" placeholder="Procedencia..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email:</label>
                    <div class="col-lg-9">
                        <input type="text" id="emailEdit" name="emailEdit" class="form-control" placeholder="Email..." required>
                    </div>
                </div>
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Usuario:</label>
                    <div class="col-lg-9">
                        <input type="text" id="usuarioEdit" name="usuarioEdit" class="form-control" placeholder="Usuario..." required>
                    </div>
                </div>
                <!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Password<span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="passwordEdit" id="passwordEdit" class="form-control" required placeholder="Minimum 5 characters allowed">
									</div>
								</div>
								<!-- /password field -->
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>



@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/alumnos/remove/'+id);
    }
    function mostrar(id)
    {
        var ruta = "http://localhots:8000/alumno"+id.value+"/edit";
        $.get(ruta,function(res){
            $("#nombres").val(res.nombres);
        });
    }
       function editar(id){
        $("#formEditar").attr('action','/alumnos/'+id);
        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traeralumno',
                data: data,
                success: function(data) {
                    $('#nombresEdit').val(data['alumno']['nombres']);
                    $('#PaternoEdit').val(data['alumno']['paterno']);
                    $('#MaternoEdit').val(data['alumno']['materno']);
                    $('#DNIEdit').val(data['alumno']['DNI']);
                    $('#sexoEdit').val(data['alumno']['sexo']);
                    $('#fecha_nacimientoEdit').val(data['alumno']['fecha_nacimiento']);
                    $('#telefonoEdit').val(data['alumno']['telefono']);
                    $('#procedenciaEdit').val(data['alumno']['procedencia']);
                    $('#emailEdit').val(data['alumno']['email']);
                    $('#usuarioEdit').val(data['alumno']['usuario']);
                    $('#passwordEdit').val(data['alumno']['password']);


                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }

    $(function () {
        $('#btnBuscar').click(function () {
            var dniBuscar = $('#DNI').val();
            var url = $('#url_buscarxdni').val() + dniBuscar;
            console.log(url);
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: function (data) {
                    $('#nombres').val(data.usuario.persona.nombres);
                    $('#paterno').val(data.usuario.persona.paterno);
                    $('#materno').val(data.usuario.persona.materno);
                    $('#usuario').val(data.usuario.usuario);
                    $('#password').val(data.usuario.password);
                }
            })
        })
    })
</script>



@endsection
