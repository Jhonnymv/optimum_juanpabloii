@extends('Layouts.main')
@section('title','CredencialesAlumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
@endsection

@section('header_buttons')

<div class="d-flex justify-content-center">
    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
        <i class="icon-bars-alt text-pink-300"></i>
        <span>Statistics</span>
    </a>
    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
        <i class="icon-calculator text-pink-300"></i>
        <span>Invoices</span>
    </a>
    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Schedule</span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Dashboard</span>
@endsection
@section('header_subbuttons')
<div class="breadcrumb justify-content-center">
    <a href="#" class="breadcrumb-elements-item">
        <i class="icon-comment-discussion mr-2"></i>
        Support
    </a>

    <div class="breadcrumb-elements-item dropdown p-0">
        <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
            <i class="icon-gear mr-2"></i>
            Settings
        </a>

        <div class="dropdown-menu dropdown-menu-right">
            <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
            <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
            <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-3">
    Hola @if(session('id'))
    <div class="alert alert-primary" role="alert">
        {{ session('id')}}
    </div>
    @endif
        <!-- Members online -->
        <div class="card bg-teal-400">
            <div class="card-body">
                <div class="d-flex">
                    <h3 class="font-weight-semibold mb-0">3,450</h3>
                    <span class="badge bg-teal-800 badge-pill align-self-center ml-auto">+53,6%</span>
                </div>

                <div>
                    Members online
                    <div class="font-size-sm opacity-75">489 avg</div>
                </div>
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>
        <!-- /members online -->
    </div>
</div>
@endsection

@section('script')
<script>


</script>

@endsection
