@extends('Layouts.main')
@section('Cuerpo')
<h2>Lista de alumnos</h2>
<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>email</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($lista as $item)
        <tr>
            <td>{{$item->nombre}}</td>
            <td>{{$item->apellido}}</td>
            <td>{{$item->email}}</td>
        </tr>
        @empty

        @endforelse
    </tbody>

</table>

<h2>Matricula</h2>

<form action="/matricular" method="POST">
   
    @csrf
    <label for="name">Nombre:</label>
    <input type="text" name="nombre">
    <br>
    <label for="name">Apellido:</label>
    <input type="text" name="apellido">
    <br>
    <label for="name">Email:</label>
    <input type="text" name="email">
    <br>
    <button type="submit">Guardar</button>
</form>
@endsection