@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pre-Matrícula')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Actualización de Datos</h6>

                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        @if($matricula??'')
                            <p>Estas matrículado en el perio actual</p>

                        @endif
                    </div>

                    <ul class="nav nav-tabs nav-tabs-solid border-0">

                        </li>
                        @if($matricula??'')
                            @if($matricula->estado=='POR-PAGAR')
                                <li class="nav-item"><a href="#registrar-sol" class="nav-link active"
                                                        data-toggle="tab">Registrar Solicitud</a>
                                </li>
                            @endif
                            @if($matricula->estado=='POR-CONFIRMAR')
                                <li class="nav-item"><a href="#confirmar-matricula" class="nav-link"
                                                        data-toggle="tab">Confirmar Matrícula</a>
                                </li>
                            @endif
                        @endif
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane show active" id="registrar-sol">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Ficha Personal</h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('mantenimientodatos.update')}}" method="post"
                                          enctype="multipart/form-data">
                                          @method('PUT')
                                        @csrf

                                    <fieldset class="mb-3">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nombre:</label>
                                        <div class="col-lg-9">
                                        <input type="text" id="nombresEdit" name="nombresEdit" value="{{$persona->first()->nombres}}" class="form-control" placeholder="Nombres..."
                                                required>
                                                <input type="hidden" id="id" name="id" value="{{$persona->first()->id}}" class="form-control" placeholder="Carrera Ejemplo..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Paterno:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="paternoEdit" name="paternoEdit" value="{{$persona->first()->paterno}}" class="form-control" placeholder="Apellido Paterno..."
                                                required>
                                        </div>
                                    </div>
                                      <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Materno:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="maternoEdit" name="maternoEdit" value="{{$persona->first()->materno}}" class="form-control" placeholder="Apellido Materno..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">DNI:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="DNIEdit" name="DNIEdit" value="{{$persona->first()->DNI}}" class="form-control" placeholder="DNI..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Fecha de Nacimiento:</label>
                                        <div class="col-lg-9">
                                            <input type="date"  id="fecha_nacEdit" name="fecha_nacEdit" value="{{$persona->first()->fecha_nacimiento}}" class="form-control" placeholder="fecha nacimiento..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Dirección:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="direccionEdit" name="direccionEdit" value="{{$persona->first()->direccion}}" class="form-control" placeholder="Dirección..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Teléfono:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="telefonoEdit" name="telefonoEdit" value="{{$persona->first()->telefono}}" class="form-control" placeholder="Teléfono..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">E-mail:</label>
                                        <div class="col-lg-9">
                                            <input readonly type="text value="{{$persona->first()->email}}" class="form-control" placeholder="E-mail..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">E-mail Personal:</label>
                                        <div class="col-lg-9">
                                            <input type="text" id="emailEdit" name="emailEdit" value="{{$persona->first()->email_personal}}" class="form-control" placeholder="E-mail..."
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <div class="col-md-6">
                                                <fieldset>
                                                    <legend class="font-weight-semibold"><i class="icon-image2 mr-2"></i> Imagen del
                                                        Alumno
                                                    </legend>
                                                    <div class="card">
                                                        <div class="card-img-actions m-1">
                                                        <img class="card-img img-fluid" id="imgEdit" alt="" src="{{asset('storage').'/fotos_alumnos/'.$persona->first()->urlimg}}">
                                                            <div class="card-img-actions-overlay card-img">
                                                                <a id="imgEdit"  class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round legitRipple" data-popup="lightbox" rel="group">
                                                                    <i class="icon-plus3"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                    {{--                                <img id="img_comprobante" style="height: 20em;width: 20em">--}}
                                                </fieldset>
                                            </div>
                                        </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Imagen del Alumno: </label>
                                                <div class="col-lg-10">
                                                    <input class="" type="file" name="imgEdit" >
                                                </div>
                                            </div>

                                        </fieldset>
                                        <div class="card-footer text-center">
                                            <button type="submit" class="btn btn-primary">Guardar Datos</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="confirmar-matricula">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Cursos</h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            {{--                                                <th><input type="checkbox" name="all_select" id="all_select"></th>--}}
                                            <th>Horario</th>
                                            <th>Nombre del curso</th>
                                            <th>Nivel</th>
                                            <th>Créditos</th>
                                            {{--                                                <th>Estado</th>--}}
                                            {{--                                                <th>Observación</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($matricula??'')
                                            @foreach($matricula->matriculadetalles as $detalle)
                                                <tr>
                                                    <td>
                                                        @foreach($detalle->seccion->horarios as $horario)
                                                            {{$horario->hora_inicio.' - '.$horario->hora_fin.' '.$horario->dia}}
                                                            <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                                                    <td>{{$detalle->seccion->pe_curso->semestre}}</td>
                                                    <td>{{$detalle->seccion->pe_curso->creditos}}</td>
                                                    {{--                                                    <td></td>--}}
                                                    {{--                                                    <td></td>--}}
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <p><strong>Nota: </strong> El estado de la matrícula se encontrará en pendiente de
                                confirmación hasta que el alumno la haya confirmado</p>
                            <p><strong>Resumen de matrícula:</strong></p>
                            <p>Número de créditos regulares totales registrados: </p>
                            <p>Número de créditos adicionales totales registrados: </p>
                            <br>
                            <p>Total de creditos matriculados: </p>
                            <div class="card-footer text-center">
                                <button type="submit" onclick="confirmar()" class="btn btn-primary">Confirmar
                                    Matrícula
                                </button>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#modal-observar-matricula">Observar Matrícula
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="Url_Get_Curso" value="{{route('pecursos.get_pecurso')}}">
    <input type="hidden" id="Url_Get_adicionales" value="{{route('pecursos.get_adicionales')}}">
    <input type="hidden" id="Url_Get_SeccionesxCurso" value="{{route('seccion.getSecciondexCurso')}}">
    <input type="hidden" id="url_listxpecarrera" value="{{route('pecursos.listxpecarrera',[''])}}/">
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
@endsection
@section('modals')

    <div id="modal-observar-matricula" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Observar Matrícula</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body py-0">
                    <p>Describa detalladamente su observación acerca de su ficha de matrícula envida:</p>
                    <div class="form-group row">
                        <div class="col-12">
                                <textarea id="text_observation" class="form-control" name="" cols="30" rows="10"
                                          placeholder="Observación"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="observar()" class="btn btn-primary">Enviar</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        var tcr = 0;
        var tca = 0;

        function sumar(id) {
            var creditos = parseFloat($('#creditos' + id).text())
            var creditos_adicionales = parseFloat($('#creditos_ad' + id).text())

            if (creditos) {
                tcr += creditos;
                $('#tcr').val(tcr)
            } else {
                tca += creditos_adicionales;
                $('#tcsr').val(tca)
            }
        }

        function quitar(id) {
            var creditos = parseFloat($('#creditos' + id).text())
            var creditos_adicionales = parseFloat($('#creditos_ad' + id).text())

            if (creditos) {
                tcr -= creditos;
                $('#tcr').val(tcr)
            } else {
                tca -= creditos_adicionales;
                $('#tcsr').val(tca)
            }

            $('#seccion_id' + id).val('').attr('disabled', '')
            $('#tipo' + id).attr('disabled', '')
            $('#elegir_horario' + id).removeClass('btn-success').addClass('btn-warning').html('Matricular')
            $('#quitar' + id).attr('hidden', '')
        }


        function get_adicionales() {
            var url = $('#Url_Get_adicionales').val();
            var data = {'carrera_id': $('#carrera').val(), 'semestre': $('#semestre').val()}
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#tbody_add_cursos').html('')
                    data.pe_cursos.forEach(element => {
                        var template = "<tr>"
                            + "<td>" + element.curso.nombre + "</td>"
                            + "<td>" + element.semestre + "</td>"
                            + "<td>" + element.creditos + "</td>"
                            + "<td>" + element.horas_teoria / element.horas_practica + "</td>"
                            + "<td><button class='btn btn-light' onclick='add_curso_adicional(" + element.id + ")' data-dismiss='modal'>Agregar</button></td>"
                            + "</tr>"

                        $('#tbody_add_cursos').append(template)
                    })
                }
            })
        }

        function add_curso_adicional(id) {
            var url = $('#Url_Get_Curso').val()
            var data = {'id': id}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    var template = "<tr>"
                        + "<td><input class='seccion_id' disabled type='hidden' id='seccion_id" + data.pe_curso.id + "' name='seccion_id[]'><button onclick='get_secciones(" + data.pe_curso.id + ")' type='button' class='btn btn-warning legitRipple' data-toggle='modal' id='elegir_horario" + data.pe_curso.id + "' data-target='#modal-horario'>Matricular</button></td>"
                        + "<td>" + data.pe_curso.curso.nombre + "</td>"
                        + "<td>" + data.pe_curso.semestre + "</td>"
                        + "<td><span id='creditos_ad" + data.pe_curso.id + "'>" + data.pe_curso.creditos + "</span></td>"
                        + "<td>" + data.pe_curso.horas_teoria + " / " + data.pe_curso.horas_practica + "</td>"
                        + "<td><input id='tipo" + data.pe_curso.id + "' disabled type='hidden' name='tipo[]' value='S'><button id='quitar" + data.pe_curso.id + "' class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple' onclick='quitar(" + data.pe_curso.id + ")' type='button' hidden><i class='icon-cross2'></i></button></td>"
                        + "</tr>"
                    $('#body_cursos').append(template)
                    $('#t_cursos').DataTable({
                        "language": {
                            "url": "{{asset('assets/DataTables/Spanish.json')}}"
                        }
                    })
                }
            })
        }

        function get_secciones(id) {
            // restar(id)
            var url = $('#Url_Get_SeccionesxCurso').val();
            var data = {'id': id}
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data.secciones)
                    $('#tbody_horarios').html('');
                    data.secciones.forEach(element => {

                        var horarios = "";
                        $.each(element.horarios, function (index, value) {
                            // console.log(value.dia+' / '+value.hora_inicio+' - '+value.hora_fin)
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                        });

                        var template = "<tr>"
                            + "<td>" + element.pe_curso.curso.nombre + "</td>"
                            + "<td>" + element.seccion + "</td>"
                            + "<td>" +
                            horarios
                            + "</td>"
                            + "<td><button type='button' class='btn btn-light legitRipple' onclick='seleccionar_horario(" + element.id + "," + id + ")' data-dismiss='modal'>Seleccionar</button></td>"
                            + "</tr>"

                        $('#tbody_horarios').append(template)
                    })
                }
            })
        }

        function seleccionar_horario(seccion_id, input_curso_id) {
            $('#elegir_horario' + input_curso_id).removeClass('btn-warning').addClass('btn-success').html('Cambiar Horario')
            $('#quitar' + input_curso_id).removeAttr('hidden')
            // if ($('#seccion_id' + input_curso_id).val() === '') {
            sumar(input_curso_id)
            // }

            $('#seccion_id' + input_curso_id).val(seccion_id).removeAttr('disabled')
            $('#tipo' + input_curso_id).removeAttr('disabled')
        }

        $(function () {
            $('#carrera').change(function () {
                if ($(this !== '')) {
                    $('.semestre').addClass('show')
                    $('.add_curso').addClass('show')
                }
            })
        })
        $(function () {
            $('#semestre').change(function () {
                var id = $('#carrera').val()
                var url = $('#url_listxpecarrera').val() + id;
                var data = {'semestre': $(this).val()}
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data.pe_cursos)
                        tcr = 0
                        $('#body_cursos').html('')
                        $('#tcr').val(0)
                        data.pe_cursos.forEach(element => {
                            var template = "<tr>"
                                + "<td><input class='seccion_id' disabled type='hidden' id='seccion_id" + element.id + "' name='seccion_id[]'><button onclick='get_secciones(" + element.id + ")' type='button' class='btn btn-warning legitRipple' data-toggle='modal' id='elegir_horario" + element.id + "' data-target='#modal-horario'>Matricular</button></td>"
                                + "<td>" + element.curso.nombre + "</td>"
                                + "<td>" + element.semestre + "</td>"
                                + "<td><span id='creditos" + element.id + "'>" + element.creditos + "</span></td>"
                                + "<td>" + element.horas_teoria + " / " + element.horas_practica + "</td>"
                                + "<td><input id='tipo" + element.id + "'  disabled type='hidden' name='tipo[]' value='R'><button id='quitar" + element.id + "' class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple' onclick='quitar(" + element.id + ")' type='button' hidden><i class='icon-cross2'></i></button></td>"
                                + "</tr>"
                            $('#body_cursos').append(template)
                        })
                        $('#t_cursos').DataTable({
                            "language": {
                                "url": "{{asset('assets/DataTables/Spanish.json')}}"
                            }
                        })
                    }
                })
                $('#carrera_id').val(id)
            })
        })

        function update_estado(adddata) {
            var url = $('#url_actualizar_matricula').val()
            // console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: adddata,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    location.reload()
                }
            })
        }

        function observar() {
            var observaciones = $('#text_observation').val()
            if (observaciones.length >= 1) {
                var id = $('#matricula_id').val()
                var data = {'id': id, 'estado': 'OBSERVADA', 'observaciones': observaciones}
                update_estado(data)
            } else {
                alert('escriba una observación')
            }
        }

        function confirmar() {
            var id = $('#matricula_id').val()
            var data = {'id': id, 'estado': 'CONFIRMADA'}
            update_estado(data)
        }

        $('#form_store_matricula').on("submit", function (e) {
            if (tcr >= 1) {
                $(this).submit();
            } else {
                e.preventDefault();
                alert('matriculate en al menos un curso')
            }
        })

        $(document).ready(function() {
            if ($('.c_regular')){
                var sum = 0;
                $('.c_regular').each(function()
                {
                    sum += parseFloat($(this).val());
                });

                $('#to_tcr').val(sum)
            }
            if ($('.c_adicional')){
                var sum = 0;
                $('.c_adicional').each(function()
                {
                    sum += parseFloat($(this).val());
                });

                $('#to_tcsr').val(sum)
            }
        })

    </script>
@endsection
{{--onchange='sum(" + element.id + ")'--}}
{{--<input class='check' type='checkbox' id='check" + element.id + "' >--}}
