<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Optimum Lab - @yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet"
          type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets/global_assets/js/main/jquery.min.js"></script>
    <script src="/assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/perfect_scrollbar.min.js"></script>
    <script src="/assets/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/assets/js/app.js"></script>
    @yield('style')
    <style>
        .foto a {
            display: inline-block;
            position: relative;
        }

        .foto a label {
            position: absolute;
            text-align: center;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            display: none;
        }

        .foto a:hover label {
            color: white;
            display: block;
            cursor: pointer;
        }

        .foto a:hover img {
            filter: brightness(0.5);
        }
    </style>

    <script>
        var FixedSidebarCustomScroll = function () {


            //
            // Setup module components
            //

            // Perfect scrollbar
            var _componentPerfectScrollbar = function () {
                if (typeof PerfectScrollbar == 'undefined') {
                    console.warn('Warning - perfect_scrollbar.min.js is not loaded.');
                    return;
                }

                // Initialize
                var ps = new PerfectScrollbar('.sidebar-fixed .sidebar-content', {
                    wheelSpeed: 2,
                    wheelPropagation: true
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function () {
                    _componentPerfectScrollbar();
                }
            }
        }();

        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function () {
            FixedSidebarCustomScroll.init();
        });
    </script>
    <!-- /theme JS files -->

</head>

<body class="navbar-top">
<div style="position: fixed;
bottom: 0;
right: 0;
width: 80px;
display: block;
height: 160px;
font-size: 10px;
z-index: 1000;
margin-right: 50px;text-align:center;color:#fff">
    <img src="/assets/img/circular/2.png" alt="" style="width: 80%;">
    <br>
</div>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-slate-800 fixed-top">
    <div class="navbar-brand">
        <a href="{{url('/')}}" class="d-inline-block">
            <img src="/assets/img/optimum-logo-blanco.svg" alt="" style="width:100%">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="navbar-text ml-md-3">
                <span class="badge badge-mark border-orange-300 mr-2"></span>
                Bienvenido/a {{"Variable Usuario"}}
            </span>

        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group mr-2"></i>
                    Connectar
                </a>

                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                    <div class="dropdown-content-body p-2">
                        <div class="row no-gutters">
                            <div class="col-12 col-sm-4">
                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-github4 icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Github</div>
                                </a>

                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-dropbox text-blue-400 icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Dropbox</div>
                                </a>
                            </div>

                            <div class="col-12 col-sm-4">
                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-dribbble3 text-pink-400 icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Dribbble
                                    </div>
                                </a>

                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-google-drive text-success-400 icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Drive</div>
                                </a>
                            </div>

                            <div class="col-12 col-sm-4">
                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-twitter text-info-400 icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Twitter</div>
                                </a>

                                <a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
                                    <i class="icon-youtube text-danger icon-2x"></i>
                                    <div class="font-size-sm font-weight-semibold text-uppercase mt-2">Youtube</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-pulse2 mr-2"></i>
                    Actividad
                </a>

                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                    <div class="dropdown-content-header">
                            <span class="font-size-sm line-height-sm text-uppercase font-weight-semibold">Última
                                actividad</span>
                        <a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
                    </div>

                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-success-400 rounded-round btn-icon"><i
                                            class="icon-mention"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Usuario ejemplo</a> ejemplo notificación
                                    <div class="font-size-sm text-muted mt-1">4 minutes ago</div>
                                </div>
                            </li>


                        </ul>
                    </div>

                    <div class="dropdown-content-footer bg-light">
                        <a href="#"
                           class="font-size-sm line-height-sm text-uppercase font-weight-semibold text-grey mr-auto">Ver
                            todo</a>
                        <div>
                            <a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i
                                    class="icon-checkmark3"></i></a>
                            <a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i
                                    class="icon-gear"></i></a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-switch2"></i>
                    <span class="d-md-none ml-2">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user-material">
                <div class="sidebar-user-material-body">
                    <div class="card-body text-center">
                        @if (Auth::user()->persona->urlimg)
                            <div class="foto">
                                <a href="{{route('indexPerfil')}}">
                                    <img src="{{asset(Auth::user()->persona->urlimg)}}"
                                         class="img-fluid rounded-circle shadow-1 mb-3" style="width: 80px; height: 80px"
                                         width="80" height="80" alt="">
                                    <label style="font-weight: bold; font-size: smaller">Mi Perfil</label>
                                </a>
                            </div>
                            <a href="{{route('indexPerfil')}}">
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @else
                            <div class="foto">
{{--                                <a href="{{route('indexPerfil')}}">--}}
{{--                                    <img src="{{asset('assets/img/optimum-logo.svg')}}"--}}
{{--                                         class="img-fluid rounded-circle shadow-1 mb-3" style="width: 80px; height: 80px"--}}
{{--                                         width="80" height="80" alt="">--}}
{{--                                    <label>Mi Perfil</label>--}}
{{--                                </a>--}}
                            </div>
                            <a href="{{route('indexPerfil')}}">
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @endif
                    </div>

                    <div class="sidebar-user-material-footer">
                        <a href="#user-nav"
                           class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                           data-toggle="collapse"><span>Mi cuenta</span></a>
                    </div>
                </div>

                <div class="collapse" id="user-nav">
                    <ul class="nav nav-sidebar">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-user-plus"></i>
                                <span>Mi perfil</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-comment-discussion"></i>
                                <span>Mensajes</span>
                                <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">
                                <i class="icon-key"></i>
                                <span>Cambiar de Módulo</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i>
                                <span>Cerrar Sesión</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /user menu -->

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    <!-- Layout -->
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-register"></i> <span>Trámites</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
                            <li class="nav-item"><a href="{{route('solicitudTramite.create')}}" class="nav-link"><i
                                        class=""></i> Nuevo Trámite</a></li>
                            <li class="nav-item"><a href="{{route('solicitudes.list')}}" class="nav-link"><i
                                        class=""></i> Trámites Recibidos</a></li>
                            <li class="nav-item"><a href="{{route('solicitudes.enviadas')}}" class="nav-link"><i
                                        class=""></i> Trámites Enviados</a></li>
                            <li class="nav-item"><a href="javascript:window.history.back()" class="nav-link"><i
                                        class=""></i>Retornar</a></li>
                        </ul>
                    </li>
                    <!-- /layout -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    @yield('header_title')
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_buttons')
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        @yield('header_subtitle')
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_subbuttons')
                </div>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            @yield('content')


        </div>
        <!-- /content area -->

    @yield('modals')

    <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
{{--                        &copy; 2015 - 2020. <a href="#">OptimumLab powered by <b>WA</b> &nbsp;&nbsp; rights of use assigned to the institute Antonio Raimondi.. </a>--}}
                    </span>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
@yield('script')

</html>
