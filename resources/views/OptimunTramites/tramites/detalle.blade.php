@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Detalle Tramites')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home -</span> Detalle de Solicitud

    </h4>
@endsection

@section('style')
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <a href="{{route('solicitudes.list')}}" class="breadcrumb-item">Solicitudes</a>
    <span class="breadcrumb-item active">Detalle de Solicitud</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para ver los detalles de una solicitud
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="alumno"> Alumno:</label>
                            <input type="text" id="alumno" class="form-control" readonly
                                   value="{{$solicitud->usuario->persona->paterno.' '.$solicitud->usuario->persona->materno.', '.$solicitud->usuario->persona->nombres}}">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="tramite"> Trámite:</label>
                            <input type="text" id="tramite" class="form-control"
                                   value="{{$solicitud->tramite->descripcion}}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="asunto" class="mt-2">Asunto:</label>
                            <textarea name="" id="asunto" rows="1"
                                      class="form-control" readonly>{{$solicitud->asunto}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-10">
                            <label for="detalle" class="mt-2">Descripción:</label>
                            <textarea name="" id="detalle" rows="5"
                                      class="form-control" readonly>{{$solicitud->descripcion}}</textarea>
                        </div>
                        <div class="col-12 col-md-2 mt-4">
                            <label for="detalle" class="mt-2">Anexos:</label><br>
                            @foreach($solicitud->anexoSolicitudes as $anexo)
                                <a href="{{$anexo->url_path}}" target="_blank"><i class="icon-file-empty"
                                                                                  style="size: 10px"></i>{{$anexo->nombre}}
                                </a> <br>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    @forelse($solicitud->flujos as $flujo)
                        <div class="row">
                            <div class="col-12 col-sm-10">
                                <label for="detalle" class="mt-2">Observación: {{$flujo->fecha}}</label>
                                <textarea name="" id="detalle" rows="5"
                                          class="form-control" readonly>{{$flujo->observacion}}</textarea>
                            </div>
                        </div>
                    @empty
                    @endforelse
                    <form action="{{route('flujo.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="observacion">Observación:</label>
                                <textarea name="observacion" id="observacion" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <label class="d-block font-weight-semibold">¿que hacer?</label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="accion_flujo"
                                                       checked="" value="finalizar" id="rb_finalizar">
                                                Finalizar proceso
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="accion_flujo"
                                                       value="reenviar" id="rb_reenviar">
                                                Reenviar proceso
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="font-weight-semibold">Áreas Académicas</label>
                                        @foreach($uni_administrativas as $item)
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input uni_academicas"
                                                           name="un_administrativas[]" value="{{$item->id}}" disabled>
                                                    {{$item->descripcion}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary"><i class="icon-floppy-disk"></i>Guardar
                                    Datos
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#rb_reenviar').change(function () {
                if ($(this).is(':checked')) {
                    $('.uni_academicas').removeAttr('disabled')
                }
            })
            $('#rb_finalizar').change(function () {
                if ($(this).is(':checked')) {
                    $('.uni_academicas').removeAttr('checked').attr('disabled', '')
                }
            })
        })
    </script>
@endsection
