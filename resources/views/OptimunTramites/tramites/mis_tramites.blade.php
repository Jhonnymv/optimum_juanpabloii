@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Mis Tramites')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar mis Trámites
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar mis trámites</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de trámites</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los trámites
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_solicitudes">
                            <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>Trámite</th>
                                <th>Asunto</th>
                                <th>Fecha de Registro</th>
                                <td>Estado</td>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($solicitudes as $item)
                                <tr>
                                    <td>{{$item->usuario->persona->paterno.' '.$item->usuario->persona->materno.','.$item->usuario->persona->nombres}}</td>
                                    <td>{{$item->tramite->descripcion}}</td>
                                    <td>{{$item->asunto}}</td>
                                    <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                    <td><span
                                            class="badge badge-{{$item->estado='pendiente'?'warning':$item->estado='en_proceso'?'info':'success'}}">{{$item->estado='pendiente'?'Pendiente':$item->estado='en_proceso'?'En Proceso':'Atendida'}}</span>
                                    </td>
                                    <td>
                                        <button onclick="get_flujos({{$item->id}})"
                                                class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                title="Ver detalles">
                                            <i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_get_flujosxsolicitud" value="{{route('solicitudes.flujoxsolicitud')}}">
    <input type="hidden" name="" id="url_solicitudxid" value="{{route('solicitud.detalle',[''])}}/">
@endsection
@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Historial de trámite</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body py-0">
                    <div id="accordion-styled">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_solicitudes').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_flujos(solicitud_id) {
            var url = $('#url_solicitudxid').val() + solicitud_id;
            // var data = {'solicitud_id': solicitud_id}

            $.ajax({
                type: 'GET',
                url: url,
                // data: data,
                dataType: 'json',
                success: function (data) {
                    var solicitud = data.solicitud
                    var respo_area_aca = solicitud.tramite.unidad_administrativa.usuario.persona
                    var template_inicio = "<div class='card'>" +
                        "    <div class='card-header bg-info'>" +
                        "        <h6 class='card-title'>" +
                        "            <a data-toggle='collapse' class='text-white collapsed'" +
                        "               href='#accordion-styled-group" + solicitud.id + "' aria-expanded='false'>" + solicitud.fecha + " Estado: Inicio" + "</a>" +
                        "        </h6>" +
                        "    </div>" +
                        "    <div id='accordion-styled-group" + solicitud.id + "' class='collapse' data-parent='#accordion-styled' style=''>" +
                        "        <div class='card-body'>" +
                        "<span>Dirigido A: <strong>" + respo_area_aca.paterno + " " + respo_area_aca.materno + ", " + respo_area_aca.nombres + "</strong></span><br>" +
                        "        </div>" +
                        "    </div>" +
                        "</div>"
                    $('#accordion-styled').html('').append(template_inicio)
                    solicitud.flujos.forEach(element => {
                        if (element.usuario) {
                            var revisadox = element.usuario.persona;
                            var lbl_respo = 'Revisado por: ' + revisadox.paterno + ' ' + revisadox.materno + ' ' + revisadox.nombres
                        }

                        if (element.estado === 'en_proceso') {
                            var color = 'bg-warning'
                            var estado = 'Trámite en proceso'
                        } else if (element.estado === 'finalizado') {
                            var color = 'bg-success'
                            var estado = 'Trámite finalizado'
                        }
                        if (element.observacion === null)
                            var observacion = ''
                        else
                            var observacion = element.observacion
                        var template = "<div class='card'>" +
                            "    <div class='card-header " + color + "'>" +
                            "        <h6 class='card-title'>" +
                            "            <a data-toggle='collapse' class='text-white collapsed'" +
                            "               href='#accordion-styled-subgroup" + element.id + "' aria-expanded='false'>" + element.fecha + ' Estado:' + estado + "</a>" +
                            "        </h6>" +
                            "    </div>" +
                            "    <div id='accordion-styled-subgroup" + element.id + "' class='collapse' data-parent='#accordion-styled' style=''>" +
                            "        <div class='card-body'>" +
                            lbl_respo + "<br>" +
                            "              observaciones: " + observacion + "<br>" +
                            "        </div>" +
                            "    </div>" +
                            "</div>"

                        $('#accordion-styled').append(template)
                    })
                    $('#modal-detalle').modal('show')
                }
            })
        }
    </script>
@endsection
