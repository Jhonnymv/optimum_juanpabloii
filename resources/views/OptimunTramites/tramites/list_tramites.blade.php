@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Lista de Tramites')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Trámites
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar trámites</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de trámites</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los trámites
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_solicitudes">
                            <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>Trámite</th>
                                <th>Asunto</th>
                                <th>Fecha de Registro</th>
                                <td>Estado</td>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($solicitudes as $item)
                                <tr>
                                    <td>{{$item->usuario->persona->paterno.' '.$item->usuario->persona->materno.','.$item->usuario->persona->nombres}}</td>
                                    <td>{{$item->tramite->descripcion}}</td>
                                    <td>{{$item->asunto}}</td>
                                    <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                    <td><span
                                            class="badge badge-{{$item->estado=='finalizado'?'success':($item->estado=='pendiente'?'warning':'info')}}">{{$item->estado=='finalizado'?'Finalizado':($item->estado=='pendiente'?'Pendiente':'En proceso')}}</span>
                                    </td>
                                    <td>
                                        @if($item->estado!='finalizado')
                                            @if($item->flujos->last()->revisado_por!=Auth::user()->id)
                                                <a href="{{route('solicitud.detalle',['solicitud_id'=>$item->id])}}"
                                                   class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                   title="Ver detalles">
                                                    <i class="icon-eye"></i></a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_get_solicitud" value="{{route('solicitudes.list')}}">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_solicitudes').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
@endsection
