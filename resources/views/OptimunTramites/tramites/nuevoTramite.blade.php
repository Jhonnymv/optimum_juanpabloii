@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Nuevo Trámite')
@section('title','Pre-Matrícula')

@section('style')

@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Trámite Documentario</h4>
        </div>
        <form action="{{route('solicitudTramite.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Trámite:</label>
                    <div class="col-lg-4">
                        <select id="tramite" name="tramite_id" class="form-control">
                            <option> Seleccione un trámite</option>
                            @foreach($tramites as $tramite)
                                <option value="{{$tramite->id}}">{{$tramite->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Asunto del pedido (sumilla):</label>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="asunto" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Descripción del pedido</label>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="descripcion" rows="2"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12 mb-2 file-repeater">
                        <label for="">Imagen</label>
                        <div data-repeater-list="images">
                            <div data-repeater-item="">
                                <div class="row mb-1">
                                    <div class="col-9 col-xl-10">
                                        <input type="file" class="form-control" name="image" >
                                    </div>
                                    <div class="col-2 col-xl-1">
                                        <button type="button" data-repeater-delete=""
                                                class="btn btn-icon btn-danger mr-1">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" data-repeater-create="" class="btn btn-secondary">
                            <i class="fas fa-plus"></i> Agregar imágen
                        </button>
                    </div>
                </div>
{{--                <div class="form-group row">--}}
{{--                    <label class="col-lg-2 col-form-label">Anexo</label>--}}
{{--                    <button class="btn btn-success" type="button" id="btn_add_anexo"><i class="icon-plus-circle2"></i>--}}
{{--                    </button>--}}
{{--                    <div id="anexos" class="col-lg-12">--}}
{{--                        <input type="file" name="anexo[]" class="form-control">--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="card-footer text-center">
                <button class="btn btn-primary" type="submit"> Guardar datos</button>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/js/jquery.repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form-repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/sweetalert2.all.js')}}"></script>
    <script>
        $(function () {
            $('#btn_add_anexo').click(function () {
                var template = "<input type='file' name='anexo[]' class='form-control'>"
                $('#anexos').append(template)
            })
        })
    </script>
@endsection
