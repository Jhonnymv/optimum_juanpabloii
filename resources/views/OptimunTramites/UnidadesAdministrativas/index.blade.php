@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Unidades Administrativas')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Unidades Administrativas
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Unidad Administrativa</span>
        </a>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Unidades administrativas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de unidades administrativas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para ver la lsita de Unidades Administrativas
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_uAdministrativas">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Responsable</th>
                                <th>Ver Trámites de Unidad Administrativa</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($uAdministrativas as $item)
                                <tr>
                                    <td>{{$item->descripcion}}</td>
                                    <td>{{$item->usuario->persona->paterno.' '.$item->usuario->persona->materno.', '.$item->usuario->persona->nombres}}</td>
                                    <td><a href="{{route('tramiteController.getTramitesxUnidad',['id'=>$item->id])}}"
                                           class="btn btn-outline-primary"><i class="icon-file-eye2"> Tramites</i></a>
                                        <button type="button" onclick="buscar({{$item->id}})"
                                           class="btn btn-outline-success"><i class="icon-pencil6"> Editar </i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_get_undTra" value="{{route('UnidadAdministrativaController.getUndAdmin',[''])}}/">
@endsection
@section('modals')
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('unidadAdministrativa.store')}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Nueva Unidad Administrativa</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="descripcion" class="form-control"
                                   placeholder="Ingrese Descripcion..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Responsable:</label>
                        <div class="col-lg-9">
                            {{--                            <input type="text" name="responsable" class="form-control" placeholder="Responsable..."--}}
                            {{--                                   required>--}}
                            <select name="responsable_id" class="col-md-6 form-control">
                                <option>Seleccione Responsable}</option>
                                @foreach($responsables as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->persona->nombres}} {{$item->persona->paterno}}  {{$item->persona->materno}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>

    <div id="modalEditar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('UnidadAdministrativaController.update')}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Editar Unidad Administrativa</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="ud_descripcion_edit" id="ud_descripcion_edit" class="form-control"
                                   placeholder="Ingrese Descripcion..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Responsable:</label>
                        <div class="col-lg-9">
                            {{--                            <input type="text" name="responsable" class="form-control" placeholder="Responsable..."--}}
                            {{--                                   required>--}}
                            <select name="responsable_id_edit" id="responsable_id_edit" class="col-md-6 form-control">
                                <option>Seleccione Responsable}</option>
                                @foreach($responsables as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->persona->nombres}} {{$item->persona->paterno}}  {{$item->persona->materno}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
                <input type="text" name="id_und_admi" id="id_und_admi">
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_uAdministrativas').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        function buscar(id){
            var ruta = $('#url_get_undTra').val()+id;
            $('#modalEditar').modal('show');
            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#ud_descripcion_edit').val(data.uAdministrativa.descripcion);
                    $('#responsable_id_edit').val(data.uAdministrativa.responsable_id)
                    $('#id_und_admi').val(data.uAdministrativa.id);
                }
            })

        }
    </script>
@endsection
