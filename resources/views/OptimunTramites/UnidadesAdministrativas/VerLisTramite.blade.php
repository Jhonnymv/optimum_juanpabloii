@extends('OptimunTramites.Layouts.main_tramite')
@section('title','Lista de Tramites')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Unidades Administrativas
        - Trámites
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" onclick="add({{$tramites->first()->unidad_administrativa->id??''}})">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Trámite</span>
        </a>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active"> Trámites </span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Trámites de {{$tramites->first()->unidad_administrativa->descripcion??''}} </h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los trámites
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_tramites">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Unidad Administrativa</th>
                                <th>Editar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tramites as $item)
                                <tr>
                                    <td>{{$item->descripcion}}</td>
                                    <td>{{$item->unidad_administrativa->descripcion}}</td>
                                    <td><button type="button" href="" class="btn btn-danger" onclick="buscar({{$item->id}})"><i class="icon-pencil"></i>Editar</button>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <input type="hidden" id="url_get_tramite" value="{{route('tramiteController.getTramite',[''])}}/">
@endsection

@section('modals')
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('tramiteController.store')}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Trámite</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="descripcion" class="form-control"
                                   placeholder="Ingrese Descripcion..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Unidad Administrativa:</label>
                        <div class="col-lg-9">
                            {{--                            <input type="text" name="responsable" class="form-control" placeholder="Responsable..."--}}
                            {{--                                   required>--}}
                            <select id="unidad_administrativa_id" name="unidad_administrativa_id" class="col-md-6 form-control">
                                <option>Seleccione Unidad Administrativa</option>
                                @foreach($uAdministrativas as $item)
                                    <option value="{{$item->id}}">{{$item->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar</button>
                </div>
                <input type="hidden" id="id_tramite_create" name="id_tramite_create">
            </form>
        </div>
    </div>

    <div id="modalEditar" class="modal fade" tabindex="-2">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('tramiteController.update')}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Editar Trámite</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="descripcion_edit" id="descripcion_edit" class="form-control"
                                   placeholder="Ingrese Descripcion..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Unidad Administrativa:</label>
                        <div class="col-lg-9">
                            {{--                            <input type="text" name="responsable" class="form-control" placeholder="Responsable..."--}}
                            {{--                                   required>--}}
                            <select name="und_administrativa_id" id="und_administrativa_id"
                                    class="col-md-6 form-control">
                                <option>Seleccione Unidad Administrativa</option>
                                @foreach($uAdministrativas as $item)
                                    <option value="{{$item->id}}">{{$item->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar</button>
                </div>
                <input type="hidden" id="id_tramite" name="id_tramite">
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_solicitudes').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>

    <script>
        function buscar (id){
            var ruta = $('#url_get_tramite').val()+ id;
            $('#modalEditar').modal('show');

            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#descripcion_edit').val(data.tramite.descripcion);
                    $('#und_administrativa_id').val(data.tramite.unidad_administrativa_id);
                    $('#id_tramite').val(data.tramite.id);

                }
            })
            // console.log(ruta);
        }
        function add(id){
            $('#modalCrear').modal('show');
            $('#unidad_administrativa_id').val(id);

        }
    </script>

@endsection
