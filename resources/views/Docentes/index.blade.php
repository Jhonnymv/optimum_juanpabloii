@extends('Layouts.main')
@section('title','CredencialesAlumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Docentes</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Docente</span>
    </a>
</div>
@endsection
@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Docentes</span>
@endsection
@section('header_subbuttons')

@endsection
@section('content')
@if (Session::has('Mensaje'))
{{
    Session::get('Mensaje')
}}
@endif
<a href="{{url('docentes/create')}}">Agregar Docente</a>
<table class="table table-light">
    <thead  class="table-light">
        <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>DNI</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($docentes as $docentes)
        <tr>
            <td scope="row">{{$loop -> iteration}}</td>
            <td>{{$docentes -> nombres}}</td>
            <td>{{$docentes -> paterno}}</td>
            <td>{{$docentes -> materno}}</td>
            <td>{{$docentes -> DNI}}</td>
            <td>{{$docentes -> email}}</td>
            <td>{{$docentes -> telefono}}</td>
            <td>{{$docentes -> estado}}</td>
            <td>
            {{--  <a href="{{url('/docentes/'.$docentes -> id.'/edit')}}">Editar</a>
            |
            <form method="post" action="{{url('/docentes/'.$docentes -> id)}}">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" onclick="return confirm('¿Desea borrar el registro?');">Borrar</button>  --}}

            <a href="#" class="btn btn-danger" onclick="eliminar({{ $docentes->id }})"><i class="fas fa-trash"></i></a>
            <a href="#" class="btn btn-warning" onclick="editar({{ $docentes->id }})"><i  class="fas fa-pen"></i></a>
            </form>
            </td>
        </tr>
    @endforeach
        <tr>

        </tr>
    </tbody>
</table>
@endsection

@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/docentes">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo docente</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombres:</label>
                    <div class="col-lg-9">
                        <input type="text" name="nombres" class="form-control" placeholder="Nombres..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Paterno:</label>
                    <div class="col-lg-9">
                        <input type="text" name="Paterno" class="form-control" placeholder="Apellido paterno..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Materno:</label>
                    <div class="col-lg-9">
                        <input type="text" name="Materno" class="form-control" placeholder="Apellido Materno..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">DNI:</label>
                    <div class="col-lg-9">
                        <input type="text" name="DNI" class="form-control" placeholder="DNI..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">teléfono:</label>
                    <div class="col-lg-9">
                        <input type="text" name="telefono" class="form-control" placeholder="Teléfono..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email:</label>
                    <div class="col-lg-9">
                        <input type="text" name="email" class="form-control" placeholder="Email..." required>
                    </div>
                </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/docentes/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar Docente?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Docente</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/docentes">
            @method('PUT');
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Actualizar docente</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombres:</label>
                    <div class="col-lg-9">
                        <input type="text"  id="nombresEdit" name="nombresEdit" class="form-control" placeholder="Nombres..." required>
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Paterno:</label>
                    <div class="col-lg-9">
                        <input type="text" id="PaternoEdit" name="PaternoEdit" class="form-control" placeholder="Apellido paterno..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Apellido Materno:</label>
                    <div class="col-lg-9">
                        <input type="text" id="MaternoEdit" name="MaternoEdit" class="form-control" placeholder="Apellido Materno..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">DNI:</label>
                    <div class="col-lg-9">
                        <input type="text" id="DNIEdit" name="DNIEdit" class="form-control" placeholder="DNI..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">teléfono:</label>
                    <div class="col-lg-9">
                        <input type="text" id="telefonoEdit" name="telefonoEdit" class="form-control" placeholder="Teléfono..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email:</label>
                    <div class="col-lg-9">
                        <input type="text" id="emailEdit" name="emailEdit" class="form-control" placeholder="Email..." required>
                    </div>
                </div>

                  <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>




@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/docentes/remove/'+id);
    }
    function editar(id){
        $("#formEditar").attr('action','/docentes/'+id);


        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traerdocente',
                data: data,
                success: function(data) {
                    $('#nombresEdit').val(data['docente']['nombres']);
                    $('#PaternoEdit').val(data['docente']['paterno']);
                    $('#MaternoEdit').val(data['docente']['materno']);
                    $('#DNIEdit').val(data['docente']['DNI']);
                    //$('#sexoEdit').val(data['alumno']['sexo']);
                    //$('#fecha_nacimientoEdit').val(data['alumno']['fecha_nacimiento']);
                    $('#telefonoEdit').val(data['docente']['telefono']);
                    //$('#procedenciaEdit').val(data['alumno']['procedencia']);
                    $('#emailEdit').val(data['docente']['email']);
                    //$('#usuarioEdit').val(data['alumno']['usuario']);
                    //$('#passwordEdit').val(data['alumno']['password']);


                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>

@endsection
