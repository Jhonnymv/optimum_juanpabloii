<form action="{{url('/docentes/'.$docente ->id)}}" method="post">
{{csrf_field()}}
{{method_field('PATCH')}}
@include('docentes.form', ['Modo' => 'editar'])
    
</form>