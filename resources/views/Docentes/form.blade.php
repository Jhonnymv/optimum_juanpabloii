{{ $Modo=='crear' ? 'Agregar Docente':'Modificar Docente'}}
 <label for="Nombre">{{'Nombres'}}</label>
    <input type="text" name="Nombres" id="Nombres" value="{{isset($docente -> nombres)?$docente -> nombres:''}}">
    </br>
    <label for="Apellido Paterno">{{'Apellido Paterno'}}</label>
    <input type="text" name="Paterno" id="Paterno" value="{{isset($docente -> paterno)?$docente -> paterno:''}}">
    </br>
    <label for="Apellido Materno">{{'Apellido Materno'}}</label>
    <input type="text" name="Materno" id="Materno" value="{{isset($docente -> materno)?$docente -> materno:''}}">
    </br>
    <label for="DNI">{{'DNI'}}</label>
    <input type="text" name="DNI" id="DNI" value="{{isset($docente -> DNI)?$docente -> DNI:''}}">
    </br>
    <label for="Estado">{{'Estado'}}</label>
    <input type="text" name="estado" id="estado" value="{{isset($docente -> estado)?$docente -> estado:''}}">
    </br>
    <label for="Email">{{'Email'}}</label>
    <input type="text" name="Email" id="Email" value="{{isset($docente -> email)?$docente -> email:''}}">
    </br>
    <label for="Teléfono">{{'Teléfono'}}</label>
    <input type="text" name="telefono" id="telefono" value="{{isset($docente -> telefono)?$docente -> telefono:''}}">
    </br>
    <input type="submit" value="{{ $Modo=='crear' ? 'Agregar':'Modificar'}}">
    <a href="{{url('docentes')}}">Regresar</a>