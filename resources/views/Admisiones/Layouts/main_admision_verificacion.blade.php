<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Optimum Lab - @yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet"
          type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets/global_assets/js/main/jquery.min.js"></script>
    <script src="/assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/perfect_scrollbar.min.js"></script>
    <script src="/assets/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/assets/js/app.js"></script>
    @yield('style')
    <style>
        .foto a {
            display: inline-block;
            position: relative;
        }

        .foto a label {
            position: absolute;
            text-align: center;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            display: none;
        }

        .foto a:hover label {
            color: white;
            display: block;
            cursor: pointer;
        }

        .foto a:hover img {
            filter: brightness(0.5);
        }
    </style>

    <script>
        var FixedSidebarCustomScroll = function () {

            var _componentPerfectScrollbar = function () {
                if (typeof PerfectScrollbar == 'undefined') {
                    console.warn('Warning - perfect_scrollbar.min.js is not loaded.');
                    return;
                }

                // Initialize
                var ps = new PerfectScrollbar('.sidebar-fixed .sidebar-content', {
                    wheelSpeed: 2,
                    wheelPropagation: true
                });
            };

            return {
                init: function () {
                    _componentPerfectScrollbar();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function () {
            FixedSidebarCustomScroll.init();
        });
    </script>
    <!-- /theme JS files -->

</head>

<body class="navbar-top">

<div style="position: fixed;
bottom: 0;
right: 0;
width: 80px;
display: block;
height: 160px;
font-size: 10px;
z-index: 1000;
margin-right: 50px;text-align:center;color:#fff">
    <img src="/assets/img/circular/2.png" class="insigniainst" alt="" style="width: 125%;">
    <br>
</div>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-slate-800 fixed-top">
    <div class="navbar-brand">
        <a href="{{url('/')}}" class="d-inline-block">
            <img src="/assets/img/optimum-logo-blanco.svg" alt="" style="width:100%">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">
            <!-- User menu -->
            <div class="sidebar-user-material">
                <div class="sidebar-user-material-body">
                    <div class="card-body text-center">
                        @if (Auth::user()->persona->urlimg)
                            <div class="foto">
                                <a href="{{route('indexPerfil')}}">
                                    <img src="{{asset(Auth::user()->persona->urlimg)}}"
                                         class="img-fluid rounded-circle shadow-1 mb-3" style="width: 80px; height: 80px"
                                         width="80" height="80" alt="">
                                    <label style="font-weight: bold; font-size: smaller">Mi Perfil</label>
                                </a>
                            </div>
                            <a href="{{route('indexPerfil')}}">
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @else
                            <div class="foto">
                                <a href="{{route('indexPerfil')}}">
                                    <img src="{{asset('assets/img/optimum-logo.svg')}}"
                                         class="img-fluid rounded-circle shadow-1 mb-3" style="width: 80px; height: 80px"
                                         width="80" height="80" alt="">
                                    <label>Mi Perfil</label>
                                </a>
                            </div>
                            <a href="{{route('indexPerfil')}}">
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @endif
                    </div>

                    <div class="sidebar-user-material-footer">
                        <a href="#user-nav"
                           class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                           data-toggle="collapse"><span>Mi cuenta</span></a>
                    </div>
                </div>

                <div class="collapse" id="user-nav">
                    <ul class="nav nav-sidebar">
                        <li class="nav-item">
                            <a href="{{route('indexPerfil')}}" class="nav-link">
                                <i class="icon-user-plus"></i>
                                <span>Mi perfil</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-comment-discussion"></i>
                                <span>Mensajes</span>
                                <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">
                                <i class="icon-key"></i>
                                <span>Cambiar de módulo</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i>
                                <span>Cerrar Sesión</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /user menu -->

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Layout -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Admisión</div>
                        <i class="icon-menu"
                           title="Layout options"></i>
                    </li>
                    <!-- Adminisiones-->
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link legitRipple"><i class="icon-magazine"></i> <span>Admisión</span><div class="legitRipple-ripple" style="left: 83.3333%; top: 71.5066%; transform: translate3d(-50%, -50%, 0px); width: 202.513%; opacity: 0;"></div></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Service pages" style="display: none;">
                            <li class="nav-item"><a href="{{route('admisiones_all_postulantes')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Ver Postulantes </a></li>
                            <li class="nav-item"><a href="{{route('listaPostulantes.listPostulantes')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Ver Postulantes Ordinarios</a></li>
                            <li class="nav-item"><a href="{{route('listPostulantesExonerado.listPostulantes')}}" class="nav-link legitRipple"> <i class="icon-list"></i> Ver Postulantes Exonerados</a></li>
                            <li class="nav-item nav-item-submenu nav-item-open"><a href="#" class="nav-link legitRipple"><i class="icon-reading"></i>Reportes</a>
                                <ul class="nav nav-group-sub" style="display: block;">
                                    <li class="nav-item"><a target="_blank" href="{{route('rpt_postulantesCarreras.pdf')}}" class="nav-link legitRipple">Reporte Ordinarios</a></li>
                                    <li class="nav-item"><a target="_blank" href="{{route('rpt_postulantesCarrerasExonerados.pdf')}}" class="nav-link legitRipple">Reporte Exonerados</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link legitRipple"><i class="icon-magazine"></i> <span>Preguntas</span><div class="legitRipple-ripple" style="left: 83.3333%; top: 71.5066%; transform: translate3d(-50%, -50%, 0px); width: 202.513%; opacity: 0;"></div></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Service pages" style="display: none;">
                            <li class="nav-item"><a href="{{route('BpPregunta.create')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Nueva pregunta</a></li>
                            <li class="nav-item"><a href="{{route('BpPreguntas.index')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Lista de preguntas</a></li>
                            <li class="nav-item"><a href="{{route('examen.create')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Generar Exámen</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link legitRipple"><i class="icon-magazine"></i> <span>Evaluación</span><div class="legitRipple-ripple" style="left: 83.3333%; top: 71.5066%; transform: translate3d(-50%, -50%, 0px); width: 202.513%; opacity: 0;"></div></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Service pages" style="display: none;">
                            <li class="nav-item"><a href="{{route('entrevista_postulante.index')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> evaluación Entrevista</a></li>
                            <li class="nav-item"><a href="{{route('examen.v_evaluar')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Evaluar Exámen</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link legitRipple"><i class="icon-magazine"></i> <span>Ingresante</span><div class="legitRipple-ripple" style="left: 83.3333%; top: 71.5066%; transform: translate3d(-50%, -50%, 0px); width: 202.513%; opacity: 0;"></div></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Service pages" style="display: none;">
                            <li class="nav-item"><a href="{{route('admisionAlumno')}}" class="nav-link legitRipple"> <i class="icon-list-numbered"></i> Postulante</a></li>
                        </ul>
                    </li>

                    <!--Admisiones-->

                    <!-- /layout -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    @yield('header_title')
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_buttons')
                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        @yield('header_subtitle')
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_subbuttons')
                </div>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            @yield('content')


        </div>
        <!-- /content area -->

    @yield('modals')

    <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2015 - 2020. <a href="#">OptimumLab powered by <b>WA</b> &nbsp;&nbsp; rights of use assigned to I.E.P JUAN PABLO II.. </a>
                    </span>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
@yield('script')




</html>
