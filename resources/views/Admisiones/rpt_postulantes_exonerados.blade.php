<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

        .labels {
            font-size: 16px;
        }

        .datos {
            font-size: 17px;
            font-weight: bold;
            margin-right: 20px;
        }

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
        #customers {
            margin-left: 20px;
            margin-right: 20px;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            counter-reset: ranking;
        }


        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
            font-size: 10px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            background-color: #ddd;
        }

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #6c757d;
            color: white;
        }

        td.rank > span:before {
            content: counter(ranking);
        }

        #customers tbody tr {
            counter-increment: ranking;
        }
    </style>
</head>
<body style="">
<div>
    <table style="margin-top: -20px">
        <tr>
            <td style="text-align: center; width: 15%">
                <img src="{{public_path()}}/assets/img/logo_I.S.P.png"
                     style="width: 70px;padding-bottom: -25px; float: right; margin-top: 20px" alt="">
            </td>
            <td style="text-align: center; width: 100%">
                <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: 30px">INSTITUTO DE EDUCACIÓN
                    SUPERIOR PEDAGÓGICO </p>
                <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: -15px">“HNO. VICTORINO ELORZ
                    GOICOECHEA”</p>
                <br>
                <br>
                <p style="font-size:20px;font-weight: bold; font-family:arial">Postulantes Exonerados por Carrera</p>
            </td>
        </tr>
    </table>

    <br>
    <div class="row">
        @foreach($carreras as $carrera)
            <span>Carrera: {{$carrera->nombre, $i=0, $a=0,$b=0,$c=0,$d=0,$e=0}}<br></span>
            <br>
            <span id="cant" name="cant"> </span>
            <table id="customers" onchange="">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Dni</th>
                    <th>Nombre</th>
                    <th>Carrera</th>
                    <th>Condición</th>
                    <th>Typo de Exoneración</th>
                </tr>
                </thead>
                @foreach($postulantes as $postulante)
                    @if($postulante->postulaciones[0]->pe_carrera->carrera->id == $carrera->id)
                        <tbody>
                        <tr>
                            <td class="rank"><span>{{$i=($i+1)}}</span></td>
                            <td>{{$postulante->persona->DNI}}</td>
                            <td>{{$postulante->persona->paterno. ' '. $postulante->persona->materno.', '. $postulante->persona->nombres}}</td>
                            <td>{{$postulante->postulaciones[0]->pe_carrera->carrera->nombre}}</td>
                            <td>{{$postulante->condicion}}</td>
                            <td>{{$postulante->tipo_exoneracion}}
                                @if($postulante->tipo_exoneracion==='Fuerzas Armadas')
                                    <span style="display: none">{{$a=$a+1}}</span>
                                @endif
                                @if($postulante->tipo_exoneracion==='Conadis')
                                    <span style="display: none">{{$b=$b+1}}</span>
                                @endif
                                @if($postulante->tipo_exoneracion==='Primer y Segundo Puesto')
                                    <span style="display: none">{{$c=$c+1}}</span>
                                @endif
                                @if($postulante->tipo_exoneracion==='Preparatoria')
                                    <span style="display: none">{{$d=$d+1}}</span>
                                @endif
                                @if($postulante->tipo_exoneracion==='Atleta Calificado')
                                    <span style="display: none">{{$e=$e+1}}</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    @endif
                @endforeach
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px">NÚMERO DE
                    POSTULANTES: {{$carrera->nombre}} : {{$total= $i}}</label> br
            </table>
            <div>
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px"> Fuerzas Armadas: {{$total= $a}}</label> <br>
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px">Conadis: {{$total= $b}}</label><br>
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px"> Primer y Segundo Puesto {{$total= $c}}</label><br>
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px">Preparatoria {{$total= $d}}</label><br>
                <label style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px">Atleta Calificado {{$total= $e}}</label><br>
            </div>


    </div>
    <br>
    <br>
    @endforeach
    <br>
    <br>
    <br>
    <div class="row">
        <table style="margin-top: -20px">
            <tr>
                <td style="text-align: center; width: 100%">

                    <span><strong style="border-top: solid">Director(a) del IESP</strong></span>
                    <br>
                    <span> <strong> </strong></span>

                </td>
            </tr>
        </table>
    </div>
    <br>
</div>
</body>
</html>
