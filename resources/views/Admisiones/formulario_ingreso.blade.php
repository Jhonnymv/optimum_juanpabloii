@extends('Admisiones.Layouts.main_admision')
@section('content')
    <form id="form_data" name="formAdmision" action="{{route('postulacion.store')}}" method="POST" class="form-group"
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style="padding-top: 25px; font-weight: bold" class="card-title text-center">Proceso de
                                Postulación
                                del Instituto De Educación Superior Pedagógico “HNO. VICTORINO ELORZ
                                GOICOECHEA”</h5>
                        </div>
                        @if($msj??'')
                            <div class="alert alert-warning" role="alert">
                                {{$msj}}
                            </div>
                        @endif
                    </div>
                    <hr>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12" style="display: flex; flex-direction: row; align-items: center">
                                <div class="">
                                    <label class="text-center" style="margin: 0; padding-top: 7px">Todos los campos que
                                        tengan un <label for=""
                                                         style="color: red">
                                            * </label> son
                                        obligatorios</label>
                                </div>
                                <div class="">

{{--                                    <button id="btnTutorial" style="margin-left: 2em" class="btn btn-success"><a--}}
{{--                                            style="color: white"--}}
{{--                                            href="https://www.youtube.com/watch?v=PFWXBsziZt8&ab_channel=Optimum"--}}
{{--                                            target="_blank">Video--}}
{{--                                            Tutorial</a></button>--}}
                                </div>
                                <div>
                                    <button type="button" id="btnConsultar" class="btn btn-success" style="margin-left: 2em">
                                        <a href="{{route('admision.indexConsultarPostulacion')}}" style="color: white">Consultar Postulación</a></button>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row"
                             style="display: flex; flex-direction: row; align-items: center">
                            {{--                            <div class="">--}}
                            <div class="col-12 col-md-4">

                                <div class="form-control" style="border-style: none">
                                    <label for="condPostulante" style="margin: 0; font-weight: bold">Condición del
                                        Postulante (<label
                                            for="" style="color: red; margin: 0">*</label>)</label></label>
                                    <select name="condicion" id="condPostulante" class="form-control">
                                        <option value="Ordinario">Ordinario</option>
{{--                                        <option value="Exonerado">Exonerado</option>--}}
                                    </select>
                                </div>
                            </div>
{{--                            <div class="col-12 col-md-4" style="display: none" id="selTipoExoneracion">--}}
{{--                                <div class="form-control" style="border-style: none">--}}
{{--                                    <label for="tipo_exoneracion" style="margin: 0; font-weight: bold">Tipo de--}}
{{--                                        Exoneración (<label--}}
{{--                                            for="" style="color: red; margin: 0">*</label>)</label></label>--}}
{{--                                    <select name="tipo_exoneracion" id="tipo_exoneracion" class="form-control">--}}
{{--                                        --}}{{--                                        <option value="">Seleccione un Tipo de Exoneración</option>--}}
{{--                                        --}}{{--                                        <option value="Fuerzas Armadas">Fuerzas Armadas</option>--}}
{{--                                        --}}{{--                                        <option value="Conadis">Conadis</option>--}}
{{--                                        --}}{{--                                        <option value="Primer y Segundo Puesto">Primer y Segundo Puesto de Colegio--}}
{{--                                        --}}{{--                                        </option>--}}
{{--                                        <option value="Preparatoria">Primer y Segundo Puesto Preparatoria Victorino--}}
{{--                                            2020--}}
{{--                                        </option>--}}
{{--                                        <option value="Atleta Calificado">Atleta Calificado</option>--}}
{{--                                    </select>--}}
{{--                                    --}}{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                        </div>
                        <br>
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="#basic-pill1" id="li_datos_personales" class="nav-link legitRipple active"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos Personales</a>
                            </li>
                            <li class="nav-item fade" id="pill-contacto" style="display: none">
                                <a href="#basic-pill2" class="nav-link legitRipple" id="pill-contacto_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos de Contacto</a>
                            </li>
                            <li class="nav-item fade" id="pill-estudio" style="display: none">
                                <a href="#basic-pill3" class="nav-link legitRipple" id="pill-estudio_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos de Estudio</a>
                            </li>
                            <li class="nav-item fade" id="pill-inscripcion" style="display: none">
                                <a href="#basic-pill4" class="nav-link legitRipple" id="pill-inscripcion_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos de la Inscripción</a>
                            </li>
                        </ul>
                        <br>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="basic-pill1">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="nombres" style="font-weight: bold; margin: 0">Nombre: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label>
                                            <input type="text" name="nombres" id="nombres" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="paterno" style="font-weight: bold; margin: 0">Apellido Paterno:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="paterno" id="paterno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="materno" style="font-weight: bold; margin: 0">Apellido Materno:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="materno" id="materno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="DNI" style="font-weight: bold; margin: 0">DNI: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text"  minlength="8" maxlength="8"
                                                   name="dni" id="DNI" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento" style="font-weight: bold; margin: 0">Fecha de
                                                Nacimiento: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="date" name="fecha_nacimiento" id="fecha_nacimiento"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="email_personal" style="font-weight: bold; margin: 0">Email:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="email_personal" id="email_personal"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="celular-postulante" style="font-weight: bold; margin: 0">Número
                                                de
                                                Celular: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" minlength="9"
                                                   maxlength="9" name="telefono" id="celular-postulante"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="sexo-postulante" style="font-weight: bold; margin: 0">Sexo:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" name="sexo" id="sexo-postulante" required>
                                                <option value="">Seleccionar</option>
                                                <option value="M">Masculino</option>
                                                <option value="F">Femenino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="direccion" style="font-weight: bold; margin: 0">Dirección:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="direccion" id="direccion"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento" name="departamento_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia" name="provincia_id">
                                                <option selected>Seleccione una Provincia</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito" name="distrito_id" required>
                                                <option value="" selected>Seleccione un Distrito</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button onclick="validardatos_personales()" type="button"
                                                class="btn btn-success"
                                                style="margin: 15px">
                                            Siguiente <i class="icon-arrow-right6"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="basic-pill2">
                                <div class="row">
                                    <div class="col-11 col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="colegio_id" id="colegio_id">
                                            <label for="info-familiar" style="font-weight: bold; margin: 0">Apellidos y
                                                Nombres de
                                                Familiar Directo: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="datos_contacto" id="info-familiar"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="celular-familiar" style="font-weight: bold; margin: 0">Número de
                                                Celular de Familiar Directo (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="telefono_contacto"
                                                   minlength="6" maxlength="9" id="celular-familiar"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button onclick="validardatos_contacto()" type="button" class="btn btn-success"
                                                style="margin: 15px">
                                            Siguiente <i class="icon-arrow-right6"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="basic-pill3">
                                <div class="row">
                                    <div class="col-11 col-md-4">
                                        <div class="form-group">
                                            <input type="hidden" name="colegio_id" id="colegio_id">
                                            <label for="colegio" style="font-weight: bold; margin: 0">Colegio: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="colegio" id="colegio" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <div class="form-group">
                                            <label for="año_culmino" style="font-weight: bold; margin: 0">Año de
                                                Culminación: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="date" name="año_culmino" id="año_culmino"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button onclick="validardatos_colegio()" type="button" class="btn btn-success"
                                                style="margin: 15px">Siguiente <i
                                                class="icon-arrow-right6"></i></button>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane" id="basic-pill4">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="carrera_id" style="font-weight: bold; margin: 0">Programa de
                                                Estudio: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select name="carrera_id" id="carrera_id" class="form-control">
                                                <option value="">Seleccione un Programa de Estudio</option>
                                                @foreach($carreras as $carrera)
                                                    <option
                                                        value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="periodo" style="font-weight: bold; margin: 0">Periodo: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select name="periodo" id="periodo" class="form-control">
                                                @foreach($periodos as $periodo)
                                                    <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{--                                    <div class="col-12 col-md-4">--}}
                                    {{--                                        <div class="form-group">--}}
                                    {{--                                            <label for="fecha-postulacion" style="font-weight: bold">Fecha de--}}
                                    {{--                                                Inscripción:</label>--}}
                                    {{--                                            <input type="date" name="fecha_postulacion" id="fecha-postulacion"--}}
                                    {{--                                                   class="form-control">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div>
                                <br>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 col-md-3">
                                            <div class="form-group">
                                                <label for="imgDNI" style="font-weight: bold; margin: 0">Imagen del DNI:
                                                    (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <input type="file" name="img_dni" id="imgDNI"
                                                       class="form-control form-control-file" required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <div class="form-group">
                                                <label for="foto-identificacion" style="font-weight: bold">Foto para
                                                    Identificación: (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <input type="file" name="foto_identificacion" id="foto-identificacion"
                                                       class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12 col-md-3">
                                            <div class="form-group">
                                                <label for="voucher" style="font-weight: bold; margin: 0">Imagen del
                                                    Voucher: (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <input type="file" name="voucher" id="voucher" class="form-control"
                                                       required>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-12 col-md-3">--}}
                                        {{--                                            <div class="form-group">--}}
                                        {{--                                                <label for="declaracion-jurada" style="font-weight: bold">Declaración--}}
                                        {{--                                                    Jurada:</label>--}}
                                        {{--                                                <a href=""><label for="" style="padding-left: 3px">Descargar Formato--}}
                                        {{--                                                        Aquí</label></a>--}}
                                        {{--                                                <input type="file" name="declaracion_jurada" id="declaracion-jurada"--}}
                                        {{--                                                       class="form-control">--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-12 col-md-3" style="display: none" id="divConstExonerac">
                                            <div class="form-group">
                                                <input type="checkbox" id="checkCons_exo" name="checkCons_exo">
                                                <label for="checkCons_exo" style="font-weight: bold; margin: 0">Constancia
                                                    de Exoneración:
                                                    <label id="lblConsExoAst"
                                                           style="color: red; margin: 0; display: none"><label
                                                            style="color: black">(</label>*<label
                                                            style="color: black">)</label></label>
                                                </label>
                                                <input type="file" name="const_exonerac" id="constExonerac"
                                                       class="form-control form-control-file" style="display: none">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <label for="">Checkear y subir los Archivos con los que cuente</label>
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <input type="checkbox" id="checkCertificado" name="checkCertificado">
                                                <label class="form-check-label" for="checkCertificado"
                                                       style="font-weight: bold; margin: 0">Certificado de
                                                    Estudios:</label>
                                                <input type="file" name="img_certificado" id="imgCertificado"
                                                       class="form-control" style="display: none">
                                            </div>
                                            {{--                                            <div class="form-group" id="divDecCertificado">--}}
                                            {{--                                                <label class="form-check-label"--}}
                                            {{--                                                       style="font-weight: bold" for="imgDecCertificado">Adjuntar--}}
                                            {{--                                                    Declaración de Entrega de Certificado de Estudios:</label>--}}
                                            {{--                                                <input type="file" name="imgDecCertificado" id="imgDecCertificado"--}}
                                            {{--                                                       class="form-control">--}}
                                            {{--                                            </div>--}}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <input type="checkbox" id="checkPartNacimiento"
                                                       name="checkPartNacimiento">
                                                <label class="form-check-label" for="checkPartNacimiento"
                                                       style="font-weight: bold; margin: 0">Partida de
                                                    Nacimiento:</label>
                                                <input type="file" name="img_part_nacimiento" id="imgPartNacimiento"
                                                       class="form-control" style="display: none">
                                            </div>
                                            {{--                                            <div class="form-group" id="divDecPartNac">--}}
                                            {{--                                                <label class="form-check-label"--}}
                                            {{--                                                       style="font-weight: bold" for="imgDecPartNac">Adjuntar--}}
                                            {{--                                                    Declaración de Entrega de Partida de Nacimiento:</label>--}}
                                            {{--                                                <input type="file" name="imgDecPartNac" id="imgDecPartNac"--}}
                                            {{--                                                       class="form-control">--}}
                                            {{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row justify-content-center">
                                    <div class="">
                                        <button type="button" id="btn_enviardatos" onclick="validardatos_finales()"
                                                class="btn btn-primary">Enviar Datos Para Solicitud
                                        </button>
                                        {{--<button type="submit" class="btn btn-primary">Envíar</button>--}}
                                    </div>
                                    {{--                                    <div class="row">--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>

{{--                    <div class="card-footer" style="display: flex; flex-direction: row; justify-content: center">--}}
{{--                        <div class="row"--}}
{{--                             style="display: flex; flex-direction: row; align-items: center; justify-content: center">--}}
{{--                            <div>--}}
{{--                                <div>--}}
{{--                                    <canvas id="canvas" width="480" height="320"--}}
{{--                                            style="border-radius: 10px; border:1px solid #1a2225; width: auto; height: 180px; margin: 7px"></canvas>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div>--}}
{{--                                <div>--}}
{{--                                    <video id="video" playsinline autoplay muted--}}
{{--                                           style="border-radius: 10px; border:1px solid #1a2225; width: 240px; height: auto; margin: 7px"></video>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div>--}}
{{--                                <div>--}}
{{--                                    <button id="snap" class="btn btn-primary" type="button" style="margin: 7px">--}}
{{--                                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-camera-fill"--}}
{{--                                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                            <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>--}}
{{--                                            <path fill-rule="evenodd"--}}
{{--                                                  d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2zm.5 2a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0z"/>--}}
{{--                                        </svg>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <input type="text" id="img_identificacion" name="img_identificacion" style="display:none">--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>
            <!-- /basic layout -->

        </div>
    </form>


    <input type="hidden" id="url_distritos_prov" value="{{route('distrito_prov', [''])}}/">
    <input type="hidden" id="url_provincias_dep" value="{{route('provincia_dep', [''])}}/">
    <input type="hidden" id="url_save" value="{{route('postulacion.store')}}">
    <input type="hidden" id="url_update" value="{{route('postulacion.update',[''])}}/">
@endsection

@section('modals')
    <div id="modalVerificar" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Verificación de Datos</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <br>
                <div class="modal-body py-0">
                    <div class="tab-content">
                        <div class="form-group">
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="col-md-1">--}}
                            {{--                                        <label for="nombres-modal">Nombre:</label>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="col-md-3">--}}
                            {{--                                        <input disabled name="nombres-modal" id="nombres-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="paterno-modal">Apellido Paterno:</label>--}}
                            {{--                                        <input type="text" disabled name="paterno-modal" id="paterno-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="materno-modal">Apellido Materno:</label>--}}
                            {{--                                        <input type="text" disabled name="materno-modal" id="materno-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="DNI-modal">DNI:</label>--}}
                            {{--                                        <input type="text" disabled name="DNI-modal" id="DNI-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="fecha_nacimiento-modal">Fecha de Nacimiento:</label>--}}
                            {{--                                        <input type="date" disabled name="fecha_nacimiento-modal"--}}
                            {{--                                               id="fecha_nacimiento-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="email_personal-modal">Email:</label>--}}
                            {{--                                        <input type="text" disabled name="email_personal-modal"--}}
                            {{--                                               id="email_personal-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-4 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="departamento-modal">Departamento</label>--}}
                            {{--                                        <input disabled id="departamento-modal" name="departamento_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-4 col-md4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="provincia-modal">Provincia</label>--}}
                            {{--                                        <input disabled id="provincia-modal" name="provincia_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-4 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="distrito-modal">Distrito</label>--}}
                            {{--                                        <input disabled id="distrito-modal" name="distrito_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <br>--}}
                            {{--                        <div class="form-group">--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-11 col-md-6">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <input type="hidden" name="colegio_id" id="colegio_id">--}}
                            {{--                                        <label for="colegio-modal">Colegio:</label>--}}
                            {{--                                        <input type="text" disabled name="colegio-modal" id="colegio-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-6">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="año_culmino-modal">Año de Culminación:</label>--}}
                            {{--                                        <input type="date" disabled name="año_culmino-modal" id="año_culmino-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <br>--}}
                            {{--                        <div class="form-group">--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="carrera_id-modal">Carrera:</label>--}}
                            {{--                                        <input name="carrera_id-modal" disabled id="carrera_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="periodo-modal">Periodo:</label>--}}
                            {{--                                        <input name="periodo-modal" disabled id="periodo-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="fecha-postulacion-modal">Fecha de Postulación:</label>--}}
                            {{--                                        <input type="date" disabled name="fecha-postulacion-modal"--}}
                            {{--                                               id="fecha-postulacion-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="expediente-modal">Expediente:</label>--}}
                            {{--                                        <input type="file" disabled name="expediente-modal" id="expediente-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <br>
                            <div class="row justify-content-center">
                                <label for="" class="text-center">Haga click en "Guardar Datos" y finalmente en
                                    "Descargar".
                                    Luego imprima su solicitud, firme y coloque su huella.
                                </label>
                                <br>
                                <label for="" class="text-center">Nota: Si el Botón "Descargar" no aparece, verifique
                                    que haya llenado todos los datos anteriores
                                    Exceptuando el archivo de solicitud</label>
                            </div>
                            <br>
                            <div class="row justify-content-center">
                                <div class="">
                                    <button type="submit" class="btn btn-info" data-dismiss="modal">Atrás</button>
                                    <button onclick="guardar_datos()" type="button" class="btn btn-primary">guardar
                                        datos
                                    </button>
                                    <a id="descargar" target="_blank" class="btn btn-info fade">Descargar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        'use strict';

        const video = document.getElementById('video');
        const canvas = document.getElementById('canvas');
        const span = document.getElementById('snap');
        //const errorMsg = document.getElementById('spa#nErrorMsg');

        const constrains = {
            audio: true,
            video: {
                width: 640, height: 480
            }
        };

        async function init() {
            try {
                const stream = await navigator.mediaDevices.getUserMedia(constrains);
                handleSuccess(stream);
            } catch (e) {
                // errorMsg.innerHTML = 'Error'+e.toString();
            }
        }

        function handleSuccess(stream) {
            window.stream = stream;
            video.srcObject = stream;
        }

        init();

        var context = canvas.getContext('2d');
        snap.addEventListener('click', function () {
            context.drawImage(video, 0, 0, 480, 360);
            document.getElementById('img_identificacion').value = canvas.toDataURL();
        });
    </script>
    <script>
        $(function () {
            $('#departamento').change(function () {
                $('#distrito').html('');
                $('#distrito').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia').html('');
                        $('#provincia').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito').html('');
                            $('#distrito').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
        });

        // function load_data(id_select, data) {
        //     $(id_select).html('');
        //     $(id_select).append(new Option("Seleccionar un campo"));
        //     $.each(data, function (index, value) {
        //         $(id_select).append(new Option(value.nombre, value.id));
        //     })
        // }
    </script>

    <script>
        // $(function getData() {
        //     console.log($('#nombres').val());
        // })

        function getData() {
            $('#nombres-modal').val($('#nombres').text());
            $('#paterno-modal').val($('#paterno').val());
            $('#materno-modal').val($('#materno').val());
            $('#DNI-modal').val($('#DNI').val());
            $('#fecha_nacimiento-modal').val($('#fecha_nacimiento').val());
            $('#email_personal-modal').val($('#email_personal').val());
            $('#departamento-modal').val($('#departamento').text());
            $('#provincia-modal').val($('#provincia').val());
            $('#distrito-modal').val($('#distrito').val());
            $('#colegio-modal').val($('#colegio').val());
            $('#año_culminacion-modal').val($('#año_culminacion').val());
            $('#carrera-label').val($('#carrera').val());
            $('#periodo-modal').val($('#periodo').val());
            $('#fecha-postulacion-modal').val($('#fecha-postulacion').val());
            $('#expediente-modal').val($('#expediente').val());
        }

        function mostrar_modal() {
            getData()
            $('#modalVerificar').modal('show');
        }

        function guardar_datos() {
            var nombre = $('#nombres').val().length
            var paterno = $('#paterno').val().length
            var materno = $('#materno').val().length
            var dni = $('#DNI').val().length
            var fecha_nac = $('#fecha_nacimiento').val()
            var email = $('#email_personal').val().length
            var distrito = $('#distrito').val()
            var i_familiar = $('#info-familiar').val().length
            var c_familiar = $('#celular-familiar').val().length
            var colegio = $('#colegio').val().length
            var culminacion = $('#año_culmino').val()
            var url = $('#url_save').val()
            var url_update = $('#url_update').val()
            var carrera_id = $('#carrera_id').val()
            if (nombre < 1 || paterno < 1 || materno < 1 || dni !== 8 || fecha_nac === "" || email < 1 || distrito === "" || i_familiar < 1 || c_familiar < 1 || colegio < 1 || culminacion === "" || carrera_id === "vacio") {
                alert('¡¡Complete todos los datos requeridos antes de guardar los cambios!!')
            } else {
                var data = new FormData($('#form_data'))
                // contentType: false,
                // cache: false,
                // processData:false,

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#descargar').removeClass('fade')
                        $('#form_data').attr('action', url_update + data.datos.postulante.id)
                        $('#descargar').attr('href', $('#url_descarga').val() + data.datos.postulante.id)
                    }
                })
                alert('Sus Datos han Sido Enviados')
            }
        }


        $(function () {
            $('#checkPartNacimiento').change(function () {
                if (!$(this).prop('checked')) {
                    $('#imgPartNacimiento').hide();
                    $('#divDecPartNac').show();
                } else {
                    $('#imgPartNacimiento').show();
                    $('#divDecPartNac').hide();
                }

            })
        });

        $(function () {
            $('#checkCertificado').change(function () {
                if (!$(this).prop('checked')) {
                    $('#imgCertificado').hide();
                    $('#divDecCertificado').show();
                } else {
                    $('#imgCertificado').show();
                    $('#divDecCertificado').hide();
                }
            })
        });

        $(function () {
            $('#condPostulante').change(function () {
                if ($(this).val() === "Ordinario") {
                    $('#divConstExonerac').hide();
                    $('#selTipoExoneracion').hide();
                } else if ($(this).val() === "Exonerado") {
                    $('#divConstExonerac').show();
                    $('#selTipoExoneracion').show();
                }
            })

            $('#tipo_exoneracion').change(function () {
                switch ($(this).val()) {
                    case 'Fuerzas Armadas':
                        $('#checkCons_exo').prop('checked', false).removeAttr('disabled');
                        break
                    case 'Conadis':
                        $('#checkCons_exo').prop('checked', false).removeAttr('disabled');
                        break
                    case 'Primer y Segundo Puesto':
                        $('#checkCons_exo').prop('checked', true).attr('disabled', '');
                        $('#constExonerac').show().prop('required', true)
                        break
                    case 'Preparatoria':
                        $('#checkCons_exo').prop('checked', true).attr('disabled', '');
                        $('#constExonerac').show().prop('required', true)
                        break
                    case 'Atleta Calificado':
                        $('#checkCons_exo').prop('checked', false).removeAttr('disabled');
                        break
                }
            })

            $('#checkCons_exo').change(function () {
                if ($(this).is(':checked')) {
                    if ($('#tipo_exoneracion').val() == 'Primer y Segundo Puesto') {
                        $('#constExonerac').show().prop('required', true)
                    }
                    $('#constExonerac').show()
                } else {
                    $('#constExonerac').hide().prop('required', false)
                }
            })

            $('#checkCons_exo').change(function () {
                if ($(this).is(':checked')) {
                    if ($('#tipo_exoneracion').val() == 'Preparatoria') {
                        $('#constExonerac').show().prop('required', true)
                    }
                    $('#constExonerac').show()
                } else {
                    $('#constExonerac').hide().prop('required', false)
                }
            })
        });

    </script>
    <!-- Código de instalación Cliengo para jorge.lezama.bazan@gmail.com -->
    <script type="text/javascript">(function () {
            var ldk = document.createElement('script');
            ldk.type = 'text/javascript';
            ldk.async = true;
            ldk.src = 'https://s.cliengo.com/weboptimizer/5f5e7af082e31d002a6aa1cc/5f5e7af182e31d002a6aa1cf.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ldk, s);
        })();</script>

    <script>
        function validardatos_personales() {
            var nombre = $('#nombres').val().length
            var paterno = $('#paterno').val().length
            var materno = $('#materno').val().length
            var dni = $('#DNI').val().length
            var fecha_nac = $('#fecha_nacimiento').val()
            var email = $('#email_personal').val().length
            var distrito = $('#distrito').val()
            var sexo = $('#sexo-postulante').val()
            if (nombre === 0 && paterno === 0 && materno === 0 && dni === 0 && fecha_nac === "" && email === 0 && distrito === "" || distrito === "Seleccione un Distrito" && sexo === "") {
                alert('Los campos con (*) son necesarios!')
            }
            else {
                if (nombre < 1 || paterno < 1 || materno < 1 || dni !== 8 || fecha_nac === "" || email < 1 || distrito === "" || distrito === "Seleccione un Distrito" || sexo === "") {

                    if (nombre < 1) {
                        alert('Verifique que sus Nombres estén correctamente escritos.')
                    }

                    if (paterno < 1 || materno < 1) {
                        alert('Verifique que sus Apellidos estén correctamente escritos.')
                    }

                    if (fecha_nac === "") {
                        alert('Verifique su Fecha de Nacimiento.')
                    }
                    if (email < 1) {
                        alert('Verifique que su E-mail sea correcto.')
                    }

                    if (distrito === "" || distrito === "Seleccione un Distrito") {
                        alert('Verifique que haya elegido un Departamento, una Provincia y un Distrito.')
                    }

                    if (dni !== 8) {
                        alert('Verifique que su Dni sea correcto (8 números).')
                    }

                    if (sexo === "") {
                        alert('Elija un Género antes de continuar.')
                    }
                }
                else {
                    $('#li_datos_personales').removeClass('active')
                    $('#basic-pill1').removeClass('show').removeClass('active')
                    $('#pill-contacto').removeClass('fade').addClass('active')
                    $('#pill-contacto_a').addClass('active')
                    $('#basic-pill2').addClass('active').addClass('show')
                    $('#pill-contacto').show()
                }
            }
        }

        function validardatos_contacto() {
            var i_familiar = $('#info-familiar').val().length
            var c_familiar = $('#celular-familiar').val().length

            if (i_familiar === 0 && c_familiar === 0) {
                alert('Los campos con (*) son necesarios!')
            }
            else {
                if (i_familiar < 1 || c_familiar < 1) {
                    if(i_familiar === 0){
                        alert('Verifica los Nombres y Apellidos de tu familiar.')
                    }
                    if(c_familiar === 0){
                        alert('Verifica el número de teléfono del familiar.')
                    }
                } else {
                    $('#pill-contacto').removeClass('active')
                    $('#pill-contacto_a').removeClass('active')
                    $('#basic-pill2').removeClass('show').removeClass('active')
                    $('#pill-estudio').removeClass('fade').addClass('active')
                    $('#pill-estudio_a').addClass('active')
                    $('#basic-pill3').addClass('active').addClass('show')
                    $('#pill-estudio').show()
                }
            }
        }

        function validardatos_colegio() {
            var colegio = $('#colegio').val().length
            var culminacion = $('#año_culmino').val()
            if (colegio === 0 && culminacion === "") {
                alert('Los campos con (*) son necesarios!')
            } else {
                if (colegio < 1 || culminacion === "") {
                    if(colegio === 0){
                        alert('Verifica los Datos del Colegio.')
                    }
                    if(culminacion === ""){
                        alert('Verifica la fecha de culminación.')
                    }
                } else {
                    $('#pill-estudio').removeClass('active')
                    $('#pill-estudio_a').removeClass('active')
                    $('#basic-pill3').removeClass('show').removeClass('active')
                    $('#pill-inscripcion').removeClass('fade').addClass('active')
                    $('#pill-inscripcion_a').addClass('active')
                    $('#basic-pill4').addClass('active').addClass('show')
                    $('#pill-inscripcion').show()
                }
            }
        }

        function validardatos_finales() {
            var carrera = $('#carrera_id').val()
            // var imgDni = $("#imgDNI")[0].files.length;
            // var fotoid = $("#foto-identificacion")[0].files.length;
            // var voucher = $("#voucher")[0].files.length;

            if ($('#condPostulante').val() === "Ordinario") {
                if (!$('#imgDNI').val() || carrera === "" || !$('#foto-identificacion').val() || !$('#voucher').val()) {
                    alert('Recuerde que los campos con (*) son necesarios!')
                } else {
                    if (confirm('¿Estas seguro de enviar este formulario?')) {
                        document.formAdmision.submit()
                    }
                }
            } else if ($('#condPostulante').val() === "Exonerado") {
                if ($('#tipo_exoneracion').val() === "Primer y Segundo Puesto" || $('#tipo_exoneracion').val() === "Preparatoria") {
                    if (!$('#constExonerac').val() || !$('#imgDNI').val() || carrera === "" || !$('#foto-identificacion').val() || !$('#voucher').val()) {
                        alert('Recuerde que los campos con (*) son necesarios!')
                    } else {
                        if (confirm('¿Estas seguro de enviar este formulario?')) {
                            document.formAdmision.submit()
                        }
                    }
                } else if ($('#tipo_exoneracion').val() === "Fuerzas Armadas" || $('#tipo_exoneracion').val() === "Atleta Calificado" || $('#tipo_exoneracion').val() === "Conadis") {
                    if (!$('#imgDNI').val() || carrera === "" || !$('#foto-identificacion').val() || !$('#voucher').val()) {
                        alert('Recuerde que los campos con (*) son necesarios!')
                    } else {
                        if (confirm('¿Estas seguro de enviar este formulario?')) {
                            document.formAdmision.submit()
                        }
                    }
                }
            }

        }

        $("#tipo_exoneracion").on('change', function () {
            if ($('#tipo_exoneracion').val() === "Primer y Segundo Puesto" || $('#tipo_exoneracion').val() === "Preparatoria") {
                $('#lblConsExoAst').show();
            } else {
                $('#lblConsExoAst').hide();
            }
        });

        $(document).ready(function () {
            $("#btnTutorial").click(function () {
                document.getElementById("nombres").required = false;
                document.getElementById("paterno").required = false;
                document.getElementById("materno").required = false;
                document.getElementById("DNI").required = false;
                document.getElementById("fecha_nacimiento").required = false;
                document.getElementById("email_personal").required = false;
                document.getElementById("celular-postulante").required = false;
                document.getElementById("sexo-postulante").required = false;
                document.getElementById("direccion").required = false;
                document.getElementById("departamento").required = false;
                document.getElementById("provincia").required = false;
                document.getElementById("distrito").required = false;
            });
        });

        $(document).ready(function () {
            $("#btnConsultar").click(function () {
                document.getElementById("nombres").required = false;
                document.getElementById("paterno").required = false;
                document.getElementById("materno").required = false;
                document.getElementById("DNI").required = false;
                document.getElementById("fecha_nacimiento").required = false;
                document.getElementById("email_personal").required = false;
                document.getElementById("celular-postulante").required = false;
                document.getElementById("sexo-postulante").required = false;
                document.getElementById("direccion").required = false;
                document.getElementById("departamento").required = false;
                document.getElementById("provincia").required = false;
                document.getElementById("distrito").required = false;
            });
        });

    </script>
@endsection
