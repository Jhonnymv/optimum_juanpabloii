@extends('Admisiones.Layouts.main_admision_verificacion')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Examen</h5>
            @if($msj??'')
                <div class="alert alert-warning" role="alert">
                    {{$msj}}
                </div>
            @endif
        </div>
        <div class="card-body">
            <form action="{{route('examen.evaluar')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-9">
                        <select name="examen_id" id="examen_id" class="form-control">
                            @foreach($examenes as $examen)
                                <option
                                    value="{{$examen->id}}">{{$examen->periodo->descripcion.' - '.$examen->fecha_hora}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-lg-3">
                        <button type="submit" class="btn btn-primary">Evaluar</button>
                    </div>
                </div>
            </form>
            <div class="row">
                @if($postulaciones??'')
                    <table class="table table-striped" id="MyTable">
                        <thead>
                        <tr>
                            <th>Esatudiante</th>
                            <th>DNI</th>
                            <th>Tipo</th>
                            <th>Carrera</th>
                            <th>Cal. Matemática</th>
                            <th>Cal. Comunicación</th>
                            <th>Cal. Cultura</th>
                            <th>Examen</th>
                            <th>Entrevista</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($postulaciones as $postulacion)
                            <tr>
                                <td>{{$postulacion->postulante->persona->paterno.' '.$postulacion->postulante->persona->materno.' '.$postulacion->postulante->persona->nombres}}</td>
                                <td>{{$postulacion->postulante->persona->DNI}}</td>
                                <td>{{$postulacion->postulante->condicion}}</td>
                                <td>{{$postulacion->pe_carrera->carrera->nombre}}</td>
                                <td>{{$postulacion->puntaje_mate??'0'}}</td>
                                <td>{{$postulacion->puntaje_comu??'0'}}</td>
                                <td>{{$postulacion->puntaje_cultura??'0'}}</td>
                                <td>{{round($postulacion->puntaje_mate+$postulacion->puntaje_comu+$postulacion->puntaje_cultura)??'0'}}</td>
                                <td>{{$postulacion->puntaje_entrevista??'0'}}</td>
                                <td>{{$postulacion->puntaje_entrevista+round($postulacion->puntaje_mate+$postulacion->puntaje_comu+$postulacion->puntaje_cultura)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        <div class="card-footer">
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/js/jquery.repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form-repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/sweetalert2.all.js')}}"></script>
    <script>
        $('#MyTable').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            },
            // paginate:false
        })
    </script>
@endsection
