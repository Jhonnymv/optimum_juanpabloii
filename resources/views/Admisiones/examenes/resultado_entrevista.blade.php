@extends('Admisiones.Layouts.main_admision_verificacion')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection
@section('content')
    <div class="card">
        <div class="card-header">

        </div>
        <form action="{{route('entrevista_postulante.estore')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-control" style="border-style: none">
                            <input type="hidden" id="postulante_id" name="postulante_id" required>
                            <label for="postulante">Postulante</label>
                            <input type="text" name="postulante" id="postulante" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="form-control" style="border-style: none">
                            <label for="puntaje">puntaje</label>
                            <input type="text" name="puntaje" id="puntaje" class="form-control" onkeypress="return filterFloat(event,this);" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-control" style="border-style: none">
                            <label for="url_acta">Acta</label>
                            <input type="file" name="url_acta" id="url_acta" class="form-control" required >
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary"><i class=""></i>Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <input type="hidden" id="url_get_postulantes" value="{{route('auto_get_postulante')}}">
@endsection
@section('script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        var url_search_user = $('#url_get_postulantes').val();

        $("#postulante").autocomplete({
            source: url_search_user,
            minLength: 2,
            select: function (event, ui) {
                $('#postulante_id').val(ui.item.id);
            }
        });




        function filterFloat(evt,input){
            // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
            var key = window.Event ? evt.which : evt.keyCode;
            var chark = String.fromCharCode(key);
            var tempValue = input.value+chark;
            if(key >= 48 && key <= 57){
                if(filter(tempValue)=== false){
                    return false;
                }else{
                    return true;
                }
            }else{
                if(key == 8 || key == 13 || key == 0) {
                    return true;
                }else if(key == 46){
                    if(filter(tempValue)=== false){
                        return false;
                    }else{
                        return true;
                    }
                }else{
                    return false;
                }
            }
        }
        function filter(__val__){
            var preg = /^([0-9]+\.?[0-9]{0,2})$/;
            if(preg.test(__val__) === true){
                return true;
            }else{
                return false;
            }

        }


    </script>
@endsection
