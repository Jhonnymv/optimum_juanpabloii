@extends('Admisiones.Layouts.main_admision_verificacion')
@section('title','Postulantes')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Lista de Postulantes
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar Postulantes</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Postulate</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    <form name="" action="{{route('agregarPostulanteAlumno')}}" method="POST">
                        @csrf
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="buscarpor" style="margin: 0; font-weight: bold">Buscar por: </label>
                                    <select name="buscarpor" id="buscarpor" class="form-control" required>
                                        <option value="DNI">DNI</option>
                                        <option value="paterno">Apellido Paterno</option>
                                        <option value="materno">Apellido Materno</option>
                                        <option value="nombres">Nombres</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Ingrese Dato:</label>
                                    <input type="text" class="form-control" name="datoBuscar" id="datoBuscar"
                                           placeholder="Ingrese Dato">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary" name="buscar" id="buscar">Buscar<i
                                            class="icon-search4 ml-2"></i></button>
                                </div>
                            </div>
                        </div>

                        <div id="datos" style="margin-top: 5%; display: none">
                            <h5 class="card-title">Datos Personales</h5>
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Apellido Paterno:</label>
                                    <input type="hidden" class="form-control" name="id" id="id"
                                           placeholder="Ingrese Dato">
                                    <input type="text" class="form-control" name="paterno" id="paterno"
                                           placeholder="Ingrese Dato" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Apellido Materno:</label>
                                    <input type="text" class="form-control" name="materno" id="materno" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Nombres:</label>
                                    <input type="text" class="form-control" name="nombres" id="nombres" disabled>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">DNI:</label>
                                    <input type="text" class="form-control" name="DNI" id="DNI" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Fecha Nacimiento:</label>
                                    <input type="date" class="form-control" name="fecha_nacimiento"
                                           id="fecha_nacimiento" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Teléfono:</label>
                                    <input type="text" class="form-control" name="telefono" id="telefono" disabled>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Dirección:</label>
                                    <input type="text" class="form-control" name="direccion" id="direccion" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Email:</label>
                                    <input type="text" class="form-control" name="email" id="email" disabled>
                                </div>
                                <div class="col-md-4">
                                    <a type="button" class="btn btn-secondary" onclick="activar()"><i
                                            class="icon-pencil6"></i> Editar</a>
                                    <input type="hidden" name="editar" id="editar" value="0">
                                </div>
                            </div>
                            <br>
                            <h5 class="card-title">Datos Ingreso</h5>
                            <div class="row">
                                <div class="col-md-2">
                                    <label style="margin: 0; font-weight: bold">Condición:</label>
                                    <input class="form-control" id="condicion" name="condicion" value="1" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Programa:</label>
                                    <select class="form-control" id="pe_carrera_id" name="pe_carrera_id" >
                                        <option>Seleccione un programa</option>
                                        @foreach($pe_carreras as $p_carrera)
                                            <option
                                                value="{{$p_carrera->id}}">{{$p_carrera->carrera->nombre. ' - ('.$p_carrera->planestudio->plan  .')'}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label style="margin: 0; font-weight: bold">Ciclo:</label>
                                    <input class="form-control" id="ciclo" name="ciclo" value="1" readonly>
                                </div>
                                <div class="col-md-2">
                                    <label for="grupo" style="margin: 0; font-weight: bold" >Grupo: </label>
                                    <select name="grupo" id="grupo" class="form-control" required readonly>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label style="margin: 0; font-weight: bold">Fecha Ingreso:</label>
                                    <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso"
                                           required>
                                </div>
                                <br>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-4" style="justify-content: space-evenly">
                                    <button type="submit" class="btn btn-success"><i class="icon-add-to-list"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_buscar" value="{{route('buscarPostulante')}}">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_postulantes').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        $('#buscar').click(function () {
            var buscarpor = $('#buscarpor').val();
            var datobuscar = $('#datoBuscar').val();
            var ruta = $('#url_buscar').val();
            var data = {'buscarpor': buscarpor, 'datoBuscar': datobuscar}
            console.log(data);
            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('#datos').show();
                    console.log(data.persona);
                    data.persona.forEach(element => {
                        $('#id').val(element.id);
                        $('#paterno').val(element.paterno);
                        $('#materno').val(element.materno);
                        $('#nombres').val(element.nombres);
                        $('#DNI').val(element.DNI);
                        $('#fecha_nacimiento').val(element.fecha_nacimiento);
                        $('#telefono').val(element.telefono);
                        $('#direccion').val(element.direccion);
                        $('#condicion').val(element.postulante.condicion);
                        $('#pe_carrera_id').val(element.alumno.alumnocarrera[0].pe_carrera_id);
                        // $('#pe_carrera_id').val(element.alumno.alumnocarreras[0].pe_carrera_id);
                        if (element.alumno){
                        $('#grupo').val(element.alumno.alumnocarreras[0].grupo);
                        $('#fecha_ingreso').val(element.alumno.alumnocarreras[0].fecha_ingreso);
                        }
                    })

                }
            })
        })

        function activar() {
            $('#paterno').prop("disabled", false);
            $('#materno').prop("disabled", false);
            $('#nombres').prop("disabled", false);
            $('#DNI').prop("disabled", false);
            $('#fecha_nacimiento').prop("disabled", false);
            $('#telefono').prop("disabled", false);
            $('#direccion').prop("disabled", false);
            $('#condicion').prop("disabled", false);
            $('#editar').val('1');

        }

    </script>
@endsection

