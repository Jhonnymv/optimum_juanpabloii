<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

        .labels {
            font-size: 16px;
        }

        .datos {
            font-size: 17px;
            font-weight: bold;
            margin-right: 20px;
        }

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            justify-content: center;
            align-items: center;
            width: 100%;
        }

        .left {
            float: left;
            width: 50%;
            text-align: right;
            margin: 2px 10px;
            display: inline;
            justify-content: right;
        }

        .right {
            float: left;
            text-align: left;
            margin: 2px 10px;
            display: inline;
            justify-content: left;
        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">
<div style=" border-style: solid; color: #0c0c0c">
    <div class="row">
        <table style="margin-top: -20px">
            <tr>
                <td style="text-align: center; width: 15%">
                    <img src="{{public_path()}}/assets/img/logo_I.S.P.png"
                         style="width: 70px;padding-bottom: -25px; float: right; margin-top: 20px" alt="">
                </td>
                <td style="text-align: center; width: 100%">
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: 30px">INSTITUTO DE
                        EDUCACIÓN SUPERIOR PEDAGÓGICO </p>
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: -15px">“HNO. VICTORINO
                        ELORZ GOICOECHEA”</p>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="row">
        <table>
            <tr>
                <td>
                    <p style="font-size:14px;font-weight: bold; font-family:arial; margin-left: 30px">Sistema de
                        Información Académica</p>
                    <p style="font-size:17px;font-weight: bold; font-family:arial; margin-left: 30px; margin-top: 0">
                        Proceso de Admisión " {{$postulante->postulaciones->last()->periodo->descripcion}} "</p>
                    <p style="font-size:20px;font-weight: bold; font-family:arial; margin-left: 30px; margin-top: 0">
                        Constancia de Inscripción N°: {{$postulante->id}}</p>
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-left: 30px; margin-top: 0">
                        Condición del Postulante: {{$postulante->condicion}}</p>
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-left: 30px; margin-top: 0">
                        Tipo del Exoneración: {{$postulante->tipo_exoneracion??'Ninguna'}}</p>
                </td>
                <td>
                    <div style="text-align: center; width: 100%">
                        <img src="{{public_path($postulante->postulaciones[0]->postulacion_anexos[1]->url)}}" alt=""  style="width:20%"><br>
                        <p style="font-size:14px;font-weight: bold; font-family:arial">
                            Código:{{$postulante->persona->DNI}}</p>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="row" >
        <div class="left">
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Apellido Paterno:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Apellido Materno:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Nombres:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Especialidad:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Documento de Identidad:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Sexo:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Domicilio:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Teléfono:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Correo Electrónio:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Fecha Y Lugar de Nacimiento:</p>
            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Fecha de Postulación:</p>
        </div>
        <div class="right">
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->paterno}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->materno}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->nombres}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->postulaciones[0]->pe_carrera->carrera->nombre}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->DNI}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->sexo = 'M'? 'Masculino':'Femenino'}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->direccion}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->telefono}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->email_personal}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->persona->fecha_nacimiento}}</p>
            <p style="justify-content: left;font-size:14px; font-family:arial">{{$postulante->postulaciones[0]->fecha_pos}}</p>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p style="font-size:14px; font-family:arial; margin-left: 30px; margin-top: 0;">Indicaciones del Proceso de
        Admisión</p>
    <p style="font-size:14px; font-family:arial; margin-left: 60px;margin-top: 0;margin-bottom: 0">1. Presentarse en la
        sede de aplicación, con 1 hora de anticipación a la hora establecida.</p>
    <p style="font-size:14px; font-family:arial; margin-left: 60px;margin-top: 0;margin-bottom: 0">2. Portar su
        documento de identificacion (DNI) al ingresar al local.</p>
    <p style="justify-content: right; font-size:14px; font-family:arial; margin-left: 60px;margin-top: 0;margin-bottom: 0">
        3. Presentar esta constancia <strong style="font-size:14px;font-weight: bold; font-family:arial">(Con foto,
            Sello institucional y firmas del postulante y director(a) de la institución de Educacion Superior)"</strong>
    </p>
    <p style="font-size:14px; font-family:arial; margin-left: 30px;margin-bottom: 0">ESTE ES EL ÚNICO DOCUMENTO QUE LO
        ACREDITA COMO POSTULANTE CORRECTAMENTE REGISTRADO EN EL SISTEMA DE INFORMACIÓN ACADÉMICA DEL MINEDU Y PERMITE SU
        ACCESO AL LOCAL PARA RENDIR LAS PRUEBAS.</p>
    <br>
    <br>
    <div class="row">
        <table style="margin-top: -20px">
            <tr>
                <td style="text-align: center; width: 100%">

                    <span><strong style="border-top: solid">Director(a) del IESP</strong></span>
                    <br>
                    <span> <strong> </strong></span>

                </td>
                <td style="text-align: center; width: 100%">

        <span><strong
                style="border-top: solid">{{$postulante->persona->paterno.' '.$postulante->persona->materno.', '.$postulante->persona->nombres}}</strong></span>
                    <br>
                    <span>DNI:  <strong>{{$postulante->persona->DNI}}</strong></span>

                </td>
            </tr>
        </table>
    </div>
    <br>
    <br>
    <br>
    <br>
</div>
</body>
</html>
