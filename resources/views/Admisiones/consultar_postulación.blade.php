@extends('Admisiones.Layouts.main_admision')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Consultar Postulación</h5>
                    <div class="header-elements">

                    </div>
                </div>
                <div class="card-body">
                    <form id="formContraseña" name="formConsulta" action="" method="POST">
                        @csrf
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="dni">DNI:</label>
                                        <input type="text" name="DNI" id="DNI" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" id="btnBuscar">Comprobar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="resultadoBusqueda" style="display: flex; flex-direction: column; margin-left: 3em">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_buscarxdni" value="{{route('admision.buscarUsuarioxDni', [''])}}/">
@endsection
@section('script')
    <script>
        $(function () {
            $('#btnBuscar').click(function () {
                var dniBuscar = $('#DNI').val();
                var url = $('#url_buscarxdni').val() + dniBuscar;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#resultadoBusqueda').html("<h4 style='color: #1ecd93'>Cargando...</h4>")
                    },
                    success: function (data) {
                        $('#resultadoBusqueda').html('')
                        if (data.postulante) {
                            var template =
                                "<h2 style='color: #0aa7ef'>¡¡Registrado!!</h2>" +
                                "<h4>Datos Personales</h4>" +
                                "<div class='col-md-4'>" +
                                "<strong>Nombre: </strong>" +
                                "<label>" + data.postulante.persona.nombres + "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Apellidos: </strong>" +
                                "<label>" + data.postulante.persona.paterno + " " + data.postulante.persona.materno + "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Fecha de Nacimiento: </strong>" +
                                "<label>" + moment(data.postulante.persona.fecha_nacimiento).format('L') + "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Email: </strong>" +
                                "<label>" + data.postulante.persona.email_personal + "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Dirección: </strong>" +
                                "<label>" + data.postulante.persona.direccion + "</label>" +
                                "</div>" +
                                "<br>" +
                                "<h4>Datos de Postulación</h4>" +
                                "<div class='col-md-4'>" +
                                "<strong>Condición del Postulante: </strong>" +
                                "<label>" + data.postulante.condicion + ' '+data.postulante.tipo_exoneracion+ "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Carrera: </strong>" +
                                "<label>" + data.postulante.postulaciones[0].pe_carrera.carrera.nombre+ "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Lugar: </strong>" +
                                "<label>" + data.postulante.distrito.nombre + "</label>" +
                                "</div>" +
                                "<div class='col-md-4'>" +
                                "<strong>Colegio: </strong>" +
                                "<label>" + data.postulante.colegio + "</label>" +
                                "</div>"
                                // "<div class='col-md-4'>" +
                                // "<label>Estimado postulante recuerda que además de haberte registrado en este formulario has debido registrarte en el siguiente enlace</label><br>" +
                                // "<a target='_blank' href='https://sistema.siges-pedagogicos.pe/site/preinscripcion'>https://sistema.siges-pedagogicos.pe/site/preinscripcion</a>"+
                                // "</div>"
                            ;
                            $('#resultadoBusqueda').append(template);
                        }
                        else {
                            var template = "<div class='col-md-4'>" +
                                "<strong style='color: orangered'>¡¡El postulante no está registrado!!</strong>" +
                                "</div>";
                            $('#resultadoBusqueda').append(template);
                        }
                    },
                })
            })
        })
    </script>
@endsection
