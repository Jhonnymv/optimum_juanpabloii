<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>
    <style>

        .labels {
            font-size: 16px;
        }

        .datos {
            font-size: 17px;
            font-weight: bold;
            margin-right: 20px;
        }

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">

<table style="margin-top: -20px">
    <tr>
        <td style="text-align: center; width: 15%">
            <img src="{{public_path()}}/assets/img/logo_I.S.P.png" style="width: 70px;padding-bottom: -25px" alt="">
        </td>
        <td style="text-align: center; width: 100%">
            <p style="padding-bottom: -15px;font-size:15px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCACIÓN
                SUPERIOR PEDAGÓGICO “HNO. VICTORINO ELORZ GOICOECHEA”</p>
            <p style="padding-bottom: -15px;font-size:14px;font-weight: bold; font-family:arial;">EXAMEN DE ADMISIÓN {{$postulante->postulaciones->last()->periodo->descripcion}}</p>
            <p style="padding-bottom: -15px;font-size:16px;font-weight: bold; font-family:arial;">SOLICITUD DE
                POSTULANTE</p>
        </td>
    </tr>
</table>
<span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>
<br>
<br>
<br>
<span style="size: 10px"><strong>Sr. </strong> Director General del IESP "Hno. Victorino Elorz Goicoechea"</span>
<br>
<span>- Cajamarca</span>
<br>
<br>
<p style="text-align: justify">Yo,
    <strong>{{$postulante->persona->paterno.' '.$postulante->persona->materno.', '.$postulante->persona->nombres}}</strong>
    identificado con número de DNI: <strong>{{$postulante->persona->DNI}}</strong>, con dirección
    en {{$postulante->persona->direccion}} en el Departamento de
    <label style="font-weight: bold; font-size: 15px"> {{$postulante->distrito->provincia->departamento->nombre}}</label>, Provincia de
    <label style="font-weight: bold; font-size: 15px">{{$postulante->distrito->provincia->nombre}}</label>, Distrito de
    <label style="font-weight: bold; font-size: 15px">{{$postulante->distrito->nombre}}</label>; con correo electrónico
    <strong>{{$postulante->persona->email_personal}}</strong>, postulante al programa:
    <strong>{{$postulante->postulaciones->last()->pe_carrera->carrera->nombre}}</strong> ante usted, expongo;
</p>
<p>
    Que habiendo concluido mis estudios en educación básica, solicito ser considerado como postulante en el proceso de
    admisión {{$postulante->postulaciones->last()->periodo->descripcion}}.
</p>
<span>Para el efecto, adjunto los siguientes documentos:</span>
<ol>
    @foreach($postulante->postulaciones->last()->postulacion_anexos as $anexo)
        @switch($anexo->tipo)
            @case('1')
            <li>Imagen de DNI.</li>
            @break
            @case('2')
            <li>Fotografía para Identificación.</li>
            @break
            @case('3')
            <li>Imagen del Voucher de Pago.</li>
            @break
            @case('4')
            <li>Certificado de Estudios.</li>
            @break
            @case('5')
            <li>Partida de Nacimiento.</li>
            @break
            @case('6')
            <li>Constancia de Exoneración.</li>
            @break
        @endswitch
    @endforeach
    <li>Solicitud</li>
</ol>
<p>
    DECLARO BAJO JURAMENTO la auntenticidad de los documentos adjuntados y de los que adjuntaré, caso contrario asumo que
    estaré incurriendo en delito contra el Código Penal vigente, y me someto a las medidas que la institución adopte.
</p>
<p style="text-align: justify">En el caso de no haber adjuntado copias digitales de los documentos: Partida de Nacimiento, Certificado de estudios
    o Certificado de Exoneración (Solo para postulantes con Condición de Exonerados), además de los ya adjuntados, me
    comprometo a hacerlos llegar, como máximo hasta el día 4 de Octubre del 2020, en caso contrario perderé mi condición de ingresante,
    si, luego de rendir las evaluaciones, hubiera logrado una vacante de ingreso.
</p>
<p Además, DECLARO que los datos presentados en el presente formulario los realizo con carácter de DECLARACIÓN JURADA></p>
<span>CAJAMARCA, {{date('d-m-Y', strtotime($postulante->postulaciones->last()->created_at))}}</span>
<br>
<br>
<br>
<br>
<span>Firma</span>
<br>
<br>
<br>
<br>
<div style="text-align: center">
<span><strong style="border-top: solid">{{$postulante->persona->paterno.' '.$postulante->persona->materno.', '.$postulante->persona->nombres}}</strong></span>
    <br>
<span>DNI:  <strong>{{$postulante->persona->DNI}}</strong></span>
</div>

<div id="footer" class="page-break">
    <div class="page-number"></div>
</div>

{{--<input type="hidden" value="{{$postulante->postulacion_anexos->}}">--}}
</body>
</html>
