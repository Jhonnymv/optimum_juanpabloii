@extends('Admisiones.Layouts.main_admision')
@section('content')
    <form id="form_data" name="formAdmisionSolicitud" action="{{route('postulacion.update')}}" method="POST"
          class="form-group"
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style="padding-top: 25px; font-weight: bold" class="card-title text-center">Proceso de Postulación
                                del Instituto De Educación Superior Pedagógico “HNO. VICTORINO ELORZ
                                GOICOECHEA”</h5>
                        </div>
                        {{--                        <div class="header-elements">--}}
                        {{--                            <div class="list-icons">--}}
                        {{--                                <a class="list-icons-item" data-action="collapse"></a>--}}
                        {{--                                <a class="list-icons-item" data-action="reload"></a>--}}
                        {{--                                <a class="list-icons-item" data-action="remove"></a>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    <hr>

                    <div class="card-body" style="padding-left: 50px; padding-right: 50px">
                        <form action="{{route('postulacion.update')}}" method="POST" class="form-group"
                              enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" name="postulacion_id"
                                   value="{{$postulante->postulaciones->last()->id}}">
                            <input type="hidden" name="dni" value="{{$postulante->persona->DNI}}">
                            <div class="row justify-content-center">
                                <label for="">Haga click en "Descargar Solicitud"</label>
                            </div>
                            <div class="row justify-content-center">
                                <label for="" class="text-center">Luego Imprima su Solicitud, fírmela y suba una imagen nítida del
                                    documento.</label>
                            </div>
                            <br>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-4"
                                     style="display: flex; flex-direction: column; justify-content: center; align-items: center">
                                    <div class="form-group">
                                        <label for="" style="font-weight: bold; margin: 0">SOLICITUD: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label>
                                        <a href="{{route('postulante_solicitud_pdf',['id'=>$postulante->id])}}"
                                           target="_blank" class="btn btn-info" style="margin-left: 15px">Descargar
                                            Solicitud</a>
                                    </div>
                                    <div>
                                        <input type="file" name="solicitud" id="solicitud" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer"
                         style="border-style:none; display: flex; flex-direction: row; justify-content: center">
                        <div class="col-md-4 col-12"
                             style="display: flex; flex-direction: column; justify-content: center; align-items: center">
                            <button type="button" onclick="validardatos_finales()" class="btn btn-primary"
                                    style="display: none" id="expediente">Enviar Expediente
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('modals')
    <div id="modalVerificar" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Verificación de Datos</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <br>
                <div class="modal-body py-0">
                    <div class="tab-content">
                        <div class="form-group">
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="col-md-1">--}}
                            {{--                                        <label for="nombres-modal">Nombre:</label>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="col-md-3">--}}
                            {{--                                        <input disabled name="nombres-modal" id="nombres-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="paterno-modal">Apellido Paterno:</label>--}}
                            {{--                                        <input type="text" disabled name="paterno-modal" id="paterno-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="materno-modal">Apellido Materno:</label>--}}
                            {{--                                        <input type="text" disabled name="materno-modal" id="materno-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="DNI-modal">DNI:</label>--}}
                            {{--                                        <input type="text" disabled name="DNI-modal" id="DNI-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="fecha_nacimiento-modal">Fecha de Nacimiento:</label>--}}
                            {{--                                        <input type="date" disabled name="fecha_nacimiento-modal"--}}
                            {{--                                               id="fecha_nacimiento-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="email_personal-modal">Email:</label>--}}
                            {{--                                        <input type="text" disabled name="email_personal-modal"--}}
                            {{--                                               id="email_personal-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-4 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="departamento-modal">Departamento</label>--}}
                            {{--                                        <input disabled id="departamento-modal" name="departamento_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-4 col-md4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="provincia-modal">Provincia</label>--}}
                            {{--                                        <input disabled id="provincia-modal" name="provincia_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-4 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="distrito-modal">Distrito</label>--}}
                            {{--                                        <input disabled id="distrito-modal" name="distrito_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <br>--}}
                            {{--                        <div class="form-group">--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-11 col-md-6">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <input type="hidden" name="colegio_id" id="colegio_id">--}}
                            {{--                                        <label for="colegio-modal">Colegio:</label>--}}
                            {{--                                        <input type="text" disabled name="colegio-modal" id="colegio-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-6">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="año_culmino-modal">Año de Culminación:</label>--}}
                            {{--                                        <input type="date" disabled name="año_culmino-modal" id="año_culmino-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <br>--}}
                            {{--                        <div class="form-group">--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="pe_carrera_id-modal">Carrera:</label>--}}
                            {{--                                        <input name="pe_carrera_id-modal" disabled id="pe_carrera_id-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="periodo-modal">Periodo:</label>--}}
                            {{--                                        <input name="periodo-modal" disabled id="periodo-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="fecha-postulacion-modal">Fecha de Postulación:</label>--}}
                            {{--                                        <input type="date" disabled name="fecha-postulacion-modal"--}}
                            {{--                                               id="fecha-postulacion-modal" class="form-control"--}}
                            {{--                                               style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-12 col-md-4">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label for="expediente-modal">Expediente:</label>--}}
                            {{--                                        <input type="file" disabled name="expediente-modal" id="expediente-modal"--}}
                            {{--                                               class="form-control" style="border-style: none; margin: 0; padding: 0">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <br>
                            <div class="row justify-content-center">
                                <label for="" class="text-center">Haga click en "Guardar Datos" y finalmente en
                                    "Descargar".
                                    Luego imprima su solicitud, firme y coloque su huella.
                                </label>
                                <br>
                                <label for="" class="text-center">Nota: Si el Botón "Descargar" no aparece, verifique
                                    que haya llenado todos los datos anteriores
                                    Exceptuando el archivo de solicitud</label>
                            </div>
                            <br>
                            <div class="row justify-content-center">
                                <div class="">
                                    <button type="submit" class="btn btn-info" data-dismiss="modal">Atrás</button>
                                    <button onclick="guardar_datos()" type="button" class="btn btn-primary">guardar
                                        datos
                                    </button>
                                    <a id="descargar" target="_blank" class="btn btn-info fade">Descargar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_descarga" value="{{route('postulante_solicitud_pdf',[''])}}/">
@endsection

@section('script')
    <script>
        $("#solicitud").on('change', function () {
            if ($('#solicitud').val()) {
                $('#expediente').show();
            } else {
                $('#expediente').hide();
            }
        });
    </script>
    <script>
        function validardatos_finales() {
            if (confirm('¿Estás seguro de envíar esta solicitud?')) {
                document.formAdmisionSolicitud.submit()
                alert('Su información se ha registrado correctamente');
            }
        }
    </script>
@endsection
