@extends('Admisiones.Layouts.main_admision_verificacion')
@section('title','Postulantes')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Lista de Postulantes
        Ordinarios
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar Postulantes</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Postulantes Ordinarios</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    Esta es una pantalla para ver los postulantes al proceso de admisión<br>

                    <div class="row">
                        <div class="col-4">
                            <label for="carrera_id"
                                   style="font-weight: bold; margin: 0">Carrera</label></label>
                            <select class="form-control" id="carrera_id" name="carrera_id">
                                <option>Seleccione una Carrera</option>
                                @foreach($carreras as $carrera)
                                    <option
                                        value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="offset-5 col-md-3">
                            <label for="departamento" style="font-weight: bold; margin: 0">Número de
                                Postulantes:</label>
                            <input type="text" class="text-info" name="cant" id="cant" value="{{count($postulantes)}}"
                                   disabled>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_postulantes">
                            <thead>
                            <tr>
                                <th>DNI</th>
                                <th>Postulante</th>
                                <th>Carrera</th>
                                <th>Tipo</th>
                                <th>Colegio</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                                <th>Verificar</th>
                            </tr>
                            </thead>
                            <tbody id="table_body_postulantes">
                            @foreach($postulantes as $item)
                                <tr>
                                    <td>{{$item->persona->DNI}}</td>
                                    <td>{{$item->persona->paterno.' '.$item->persona->materno.', '.$item->persona->nombres}}</td>
                                    <td>{{$item->postulaciones[0]->pe_carrera->carrera->nombre??''}}</td>
                                    <td>{{$item->condicion}}</td>
                                    <td>{{$item->colegio}}</td>
                                    <td>{{date('d-m-Y',strtotime($item->postulaciones[0]->fecha_pos??''))}}</td>
                                    <td>
                                            <span
                                                class="badge badge-{{$item->postulaciones[0]->estado=='Por-Confirmar'? 'warning':($item->postulaciones[0]->estado=='Confirmado'?'success':($item->postulaciones[0]->estado=='Observado'?'danger':'primary'))}}">{{$item->postulaciones[0]->estado}}</span>
                                        <a target="_blank" href="{{route('constancia.pdf',['id'=>$item->id])}}"><i
                                                class="icon-printer2"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{route('detallePostulante.detalles',['postulante_id'=>$item->id])}}"
                                           class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                           title="Ver detalles">
                                            <i class="icon-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_post_x_carrera" value="{{route('listaPostulantesCarrera.listPostulantesCarrera')}}">
    <input type="hidden" id="url_detalle" value="{{route('detallePostulante.detalles',[''])}}/">
    <input type="hidden" id="url_imprimir" value="{{route('constancia.pdf',[''])}}/">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_postulantes').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        $('#carrera_id').change(function () {
            var url = $('#url_post_x_carrera').val();
            var urld = $('#url_detalle').val();
            var urlp = $('#url_imprimir').val();
            var data = {'carrera_id': $(this).val()}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#table_body_postulantes').html('')
                    data.postulantes.forEach(element => {
                        var template = "<tr>" +
                            "<td>" + element.persona.DNI + "</td>" +
                            "<td>" + element.persona.paterno + ' ' + element.persona.materno + ', ' + element.persona.nombres + "</td>" +
                            "<td>" + element.postulaciones[0].pe_carrera.carrera.nombre + "</td>" +
                            "<td>" + element.condicion + "</td>" +
                            "<td>" + element.colegio + "</td>" +
                            "<td>" + element.postulaciones[0].fecha_pos + "</td>" +
                            "<td>" +
                            "<span class='badge badge-" + (element.postulaciones[0].estado == 'Por-Confirmar' ? 'warning' : (element.postulaciones[0].estado == 'Confirmado' ? 'success' : (element.postulaciones[0].estado == 'Observado' ? 'danger' : 'primary'))) + "'>"
                            + element.postulaciones[0].estado
                            + "</span>" + "<a target='_blank' href='" + urlp + element.id + "'>" + "<i class='icon-printer2' style='margin-left: 2px'>" + "</i>" + "</a>" +
                            "</td>" +
                            "<td>" + "<a href='" + urld + element.id + "' class='btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple'>" + "<i class='icon-eye'></i>" + "</a>" + "</td>"
                            "<td>" + "</tr>"
                        $('#table_body_postulantes').append(template);
                    $('#cant').val(Object.keys(data).length);
                    });
                }
            })
        })
    </script>
@endsection

