@extends('Admisiones.Layouts.main_admision_verificacion')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Lista de Preguntas</h5>
            @if($msj??'')
                <div class="alert alert-warning" role="alert">
                    {{$msj}}
                </div>
            @endif
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-header">
                    <label for="categoria">Categoría</label>
                    <select class="form-control">
                        <option>Seleccione una categoría para filtrar</option>
                        @foreach($categorias as $cat)
                            <option value="{{$cat->id}}">{{$cat->categoria}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="preguntas_table">
                            <thead>
                            <tr>
                                <th>Categoría</th>
                                <th>Pregunta</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($preguntas as $pre)
                                <tr>
                                    <td>{{$pre->categoria->categoria}}</td>
                                    <td>{{$pre->pregunta}}</td>
                                    <td>
                                        <button onclick="get_detalles({{$pre->id}})"
                                                class="btn btn-success rounded-right"><i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_get_detalles" value="{{route('BpPreguntas.get_detalle')}}">
@endsection
@section('modals')
    <div id="modal_edit" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <form action="{{route('BpPreguntas.selectCorrecta')}}">
            @csrf

            <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header pb-3">
                        <h5 class="modal-title">Editar pregunta</h5>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <div class="modal-body py-0">
                        <div class="text-center">
                            <h4 id="modal_pregunta"></h4>
                        </div>
                        {{--                    <div id="alternativas" style="border-style: solid; border-color: #0c0c0c">--}}

                        {{--                    </div>--}}
                        <div class="table-responsive" style="">
                            <table class="table">
                                <thead>
                                <tr class="">
                                    <th class="table-active" style="width: 5%">N°</th>
                                    <th class="table-info" style="width: 85% ">Alternativas</th>
                                    <th class="table-warning" style="width: 7%">Correcta</th>
                                </tr>
                                </thead>
                                <tbody id="alternativasPregunta">
                                <tr class="table-success">
                                </tr>
                                </tbody>
                            </table>
                            <br>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary legitRipple"><i
                                    class="icon-database-edit2 mr-2"></i> Guardar
                            </button>
                            <br>
                        </div>
                    </div>
                    <input type="hidden" name="preguntaId" id="preguntaId" value="">
                </div>
            </div>
        </form>

    </div>
@endsection
@section('script')
    <script>
        $('#preguntas_table').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        function get_detalles(pregunta_id) {
            var url = $('#url_get_detalles').val();
            var data = {'pregunta_id': pregunta_id}
            var item = 0;
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#alternativas').html('')
                    $('#alternativasPregunta').html('')
                    $('#modal_pregunta').text(data.pregunta.pregunta);
                    $('#preguntaId').val(data.pregunta.id);
                    data.pregunta.bp_alternativas.forEach(element => {
                        var verdadero = ''
                        var falso = ''
                        if (element.valor === "1") {
                        //     falso = 'checked'
                        // } else {
                            verdadero = 'checked'
                        }
                        // var template =
                        //     "<div class='row'>" +
                        //     "<div class='col-12 col-md-6'>" +
                        //     "<input name='alternativa_id' type='hidden' value='"+element.id+"'>"+
                        //     "<span>"+element.alternativa+"<span>"+
                        //     "</div>" +
                        //     "<div class='col-12 col-md-3'>" +
                        //     "<input name='valor"+element.id+"' type='radio' value='1' "+verdadero+"><label>Verdadero</label>"+
                        //     "<input class='ml-4' name='valor"+element.id+"' type='radio' value='0' "+falso+"><label>Falso</label>"+
                        //     "</div>" +
                        //     "</div>"
                        // $('#alternativas').append(template);

                        var template =
                            "<tr>" +
                            "<td class='table-active' style='width: 5%'>" + (item = item + 1) + "<input name='alternativa_id[]' type='hidden'  value='"+element.id+"'>"+"</td>" +
                            "<td class='table-info' style='width: 85%'>"+"<span>"+element.alternativa+"<span>"+ "</td>" +
                            "<td class='table-warning' style='width: 7%'>" + "<input class='verdadera' name='valor' type='radio' value='" + element.id + "' " + verdadero + "><label>Verdadero</label>" + "</td>" +
                            "</tr>"
                        $('#alternativasPregunta').append(template)
                    })
                    $('#modal_edit').modal('show');
                }
            })
        }
    </script>
    <script>

    </script>

@endsection
