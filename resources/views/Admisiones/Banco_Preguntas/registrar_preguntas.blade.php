@extends('Admisiones.Layouts.main_admision_verificacion')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Registrar Preguntas</h5>
            @if($msj??'')
                <div class="alert alert-warning" role="alert">
                    {{$msj}}
                </div>
            @endif
        </div>
        <div class="card-body">
            <form action="{{route('BpPregunta.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-control" style="border-style: none">
                            <label for="bp_categoria_id" style="margin: 0; font-weight: bold">Categoría de pregunta
                                (<label
                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                            <select name="bp_categoria_id" id="bp_categoria_id" class="form-control">
                                @foreach($bpCategorias as $element)
                                    <option value="{{$element->id}}">{{$element->categoria}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-control" style="border-style: none">
                            <label for="pregunta">Imágen</label>
                            <input type="file" name="imagen" id="imagen" class="form-control" accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-control" style="border-style: none">
                            <label for="pregunta">Pregunta</label>
                            <textarea class="form-control border-blue" name="pregunta" id="pregunta" rows="2"
                                      required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="">Alternativas</label>
                    <div class="form-group col-12 mb-2 file-repeater">
                        <div data-repeater-list="alternativas">
                            <div data-repeater-item>
                                <div class="row">
                                    <div class="col-12 col-md-9">
                                        <div class="form-control" style="border-style: none">
                                            <label for="alternativa">Alternativa</label>
                                            <input type="text" name="alternativa" id="alternativa"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <div class="form-group">
                                            <label class="d-block font-weight-semibold">valor</label>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="valor" value="1">
                                                    Verdadero
                                                </label>
                                            </div>
                                        </div>
                                    </div>

{{--                                            <div class="form-check form-check-inline">--}}
{{--                                                <label class="form-check-label">--}}
{{--                                                    <input type="radio" class="form-check-input" name="valor" checked="" value="0">--}}
{{--                                                    Falso--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="col-12 col-md-1">
                                        <button type="button" data-repeater-delete=""
                                                class="btn btn-icon btn-danger mr-1">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" data-repeater-create="" class="btn btn-secondary">
                            <i class="fas fa-plus"></i> Agregar Alternativa
                        </button>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/js/jquery.repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form-repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/sweetalert2.all.js')}}"></script>
    <script>
        $(document).on('change','input[type="file"]',function(){

            var fileName = this.files[0].name;
            var ext = fileName.split('.').pop();

            ext = ext.toLowerCase();

            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'pdf': break;
                default:
                    alert('El archivo no tiene la extensión adecuada');
                    this.value = ''; // reset del valor
                    this.files[0].name = '';
            }
            // }
        });
    </script>
@endsection
