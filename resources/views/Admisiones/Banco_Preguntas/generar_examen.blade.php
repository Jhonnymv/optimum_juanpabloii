@extends('Admisiones.Layouts.main_admision_verificacion')
@section('style')
    <script src="{{asset('/assets/global_assets/js/plugins/pickers/anytime.min.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{asset('/assets/global_assets/js/demo_pages/picker_date.js')}}"></script>
@endsection
@section('content')
    <form action="{{route('examen.store')}}" method="POST">
        @csrf
        <div class="card">
            <div class="card-header">
                <h5>Generar Examen</h5>
                @if($msj??'')
                    <div class="alert alert-warning" role="alert">
                        {{$msj}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="form-control" style="border-style: none">
                            <label for="periodo_id">Periodo</label>
                            <input type="hidden" name="periodo_id" value="{{$periodo->id}}" required>
                            <input type="text" readonly name="periodo" id="periodo" required
                                   value="{{$periodo->descripcion}}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-control" style="border-style: none">
                            <label for="fecha_hora">Fecha y hora de inicio</label>
                            <div class="input-group">
										<span class="input-group-prepend">
											<button type="button" class="btn btn-light btn-icon legitRipple"
                                                    id="ButtonCreationDemoButton"><i
                                                    class="icon-calendar3"></i></button>
										</span>
                                <input type="text" class="form-control" id="ButtonCreationDemoInput"
                                       placeholder="Select a date" readonly="" name="fecha_hora" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-1">
                        <div class="form-control" style="border-style: none">
                            <label for="pregunta">Duración</label>
                            <input class="form-control" type="text" name="duracion" id="duracion" required pattern="^([01]?[0-9]|2[0-3]):[0-5][0-9]$" placeholder="hh:mm">
                        </div>
                    </div>
                </div>
                <label for="">N° de preguntas por categoría</label><br>
                <div class="row">
                    @foreach($categorias as $categoria)
                        <div class="col-12 col-md-3">
                            <span class="ml-2">{{$categoria->categoria}}</span>
                            <input class="ml-1 form-control" type="number" id="cat-{{$categoria->id}}" min="0">
                        </div>
                    @endforeach
                    <button class="btn btn-success" onclick="generar()" type="button">Generar</button>
                </div>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-body">
                        @foreach($categorias as $categoria)
                            <h4><u>{{$categoria->categoria}}</u></h4>
                            <div id="contenido_cat{{$categoria->id}}"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12 text-center">
                        <button type="submit" disabled id="guardar_data" class="btn btn-success">Guardar Datos</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="url_generar" value="{{route('examen.generar')}}">
    <input id="patch" type="hidden" value="{{asset('')}}">
@endsection
@section('script')
    <script>
        function generar() {
            var url = $('#url_generar').val();
            var data = {
                'cat_1': $('#cat-1').val(),
                'cat_2': $('#cat-2').val(),
                'cat_3': $('#cat-3').val()
            }
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#guardar_data').prop('disabled',false)
                    $('#contenido_cat1').html('')
                    $('#contenido_cat2').html('')
                    $('#contenido_cat3').html('')

                    load(data.pre_cat_1,1)
                    load(data.pre_cat_2,2)
                    load(data.pre_cat_3,3)
                }
            })
        }
        function load(data,id){
            $.each(data, function (index, element) {
                var alternativas = ""
                element.bp_alternativas.forEach(al => {
                    alternativas += "<li class='ml-4'>" + al.alternativa + "<span>"
                })
                var im=""
                if (element.url_imagen){
                    im+="<img style='width: 35em; height: 35em' src='"+$('#patch').val()+element.url_imagen+"'><br>"
                }
                else
                    im+=""
                var template = "<pre>"+(index+1)+ ") "+ element.pregunta + "</pre><br>" +
                    im+
                    "<input type='hidden' name='pregunta_id[]' value='"+element.id+"'>"+
                    "<i class='ml-2'>Alternativas</i> </br>" +
                    "<ol type='a'>"+
                    alternativas+
                    "</ol>"
                $('#contenido_cat'+id).append(template)
            })
        }
    </script>
@endsection
