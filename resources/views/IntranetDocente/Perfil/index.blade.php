@extends('Layouts.main')
@section('title','CredencialesAlumno')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Perfil</span>
        - {{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</h4>
@endsection

@section('content')
    <form id="form_data" name="formAdmision" action="{{route('postulacion.store')}}" method="POST" class="form-group"
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style="padding-top: 25px; font-weight: bold" class="card-title text-center">Datos del Usuario</h5>
                        </div>
                    </div>

                    <div class="card-body text-center">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-12 col-md-12">
                                    <strong for="">Nombres y Apellidos: </strong>
                                    <label for="">{{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</label>
                                </div>
                                <div class="col-12 col-md-12">
                                    <strong for="">Email: </strong>
                                    <label for="">{{$usuario->persona->email}}</label>
                                </div>
                                <div class="col-12 col-md-12">
                                    <strong for="">Dirección: </strong>
                                    <label for="">{{$usuario->persona->direccion}}</label>
                                </div>
                                <div class="col-12 col-md-12">
                                    <strong for="">Dni: </strong>
                                    <label for="">{{$usuario->persona->DNI}}</label>
                                </div>
                                <div class="col-12 col-md-12">
                                    <strong for="">Teléfono: </strong>
                                    <label for="">{{$usuario->persona->telefono}}</label>
                                </div>
                                <div class="col-12 col-md-12">
                                    <strong for="">Dni: </strong>
                                    <label for="">{{$usuario->persona->DNI}}</label>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <img src="" style="border-style: solid; width: 80px; height: 100px"
                                     class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
                                <h6 class="mb-0 text-white text-shadow-dark">{{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</h6>
                                <span class="font-size-sm text-white text-shadow-dark"></span>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
@endsection

@section('modals')


@endsection


@section('script')

@endsection
