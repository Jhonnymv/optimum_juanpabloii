<div class="row">

    <div class="col-md-3">
        <div class="form-group form-group-float" style="width: 200px">
            <label class="form-label" >Periodo</label>

            <select id="selectPeriodo" name="periodo_id"  data-placeholder="Seleccionar un periodo" class="form-control form-control-select2" data-fouc=""  >
                <option value="">Seleccione un periodo</option>
                @foreach ($periodosAcademicos as $periodo)
                <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-float" style="width: 200px">
            <label class="form-label" >Carrera</label>
            <select id="selectCarrera" name="carrera_id" data-placeholder="Seleccionar una carrera" class="form-control form-control-select2" data-fouc="">
                <option value="">Seleccione una carrera</option>
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-float" style="width: 200px">
            <label class="form-label" >Area</label>
            <select id="selectArea" name="area_id" data-placeholder="Seleccionar un area" class="form-control form-control-select2" data-fouc="">
                <option value="">Seleccione un area</option>
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-float" style="width: 200px">
            <label class="form-label" >Seccion/Grupo</label>
            <select id="selectGrupo" name="grupo_id" data-placeholder="Seleccionar un grupo" class="form-control form-control-select2" data-fouc="">
                <option value="">Seleccione un grupo</option>
            </select>
        </div>
    </div>

    <div class="col-md-2">


        <div class="btn-group" id="btn_google" style="display: none">
            <a class="btn bg-teal-400 btn-labeled btn-labeled-left dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false" style="
            background: #e73224;">
                <b><i class="icon-google"></i></b>GOOGLE SERVICES
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="createGoogleClassroomCourse()"><i class="icon-plus2"></i> Crear curso en Classroom</a>
                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="registerStudentsToClassroomCourse()"><i class="icon-reading"></i> Invitar alumnos a la clase</a>

                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="openClassroomCourse()"><i class="icon-reading"></i> Abrir curso en Classroom</a>
                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="openClassroomCourseDriveFolder()"><i class="icon-google-drive"></i> Abrir carpeta de curso en Drive</a>
                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="openClassroomCourseCalendar()"><i class="icon-calendar"></i> Abrir Calendar de curso</a>
                <a href="javascript:void(0)" class="dropdown-item" style="color: black !important" onclick="createGoogleClassroomCalendarEvents()"><i class="icon-calendar"></i> Crear sesiones de clase en Calendar</a>
                {{-- <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item" style="color: black !important"><i class="icon-gear"></i> Separated line</a> --}}
            </div>
        </div>

        
    </div>

    

</div>