@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Silabos')


@section('header_title')



@section('style')
    <style>
        option {
            background: #1B1E2F
        }
    </style>
@endsection

@include('IntranetDocente.vistasParciales.filtroPeriodo')


@endsection

@section('header_buttons')
    <div class="d-flex justify-content-center">

        {{-- <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
            data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Nuevo Silabo</span>
        </a> --}}
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Intranet Docente</span>
    <span class="breadcrumb-item active">Silabos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')

    {{-- <form method="POST" action="/silabos"> --}}

    {{-- <input type="hidden" id="pe_curso_id" name="pe_curso_id" value="0">
    <input type="hidden" id="periodo_id" name="periodo_id" value="0"> --}}


    <!--Importante-->
    @csrf
    <!--Importante-->

    <div class="row" style="margin-top: 10%" id="InfoPanel">
        <div class="col-md-12 text-center">
            <i class="icon-search4 icon-3x text-info border-info border-3 rounded-round p-3 mb-3"></i>
        </div>
        <div class="col-md-12 text-center">
            <span class="text-info" style="font-size: x-large">Seleccione una sección para mostrar las opciones.</span>
        </div>
    </div>

    <div class="row justify-content-center" style="display: none" id="SilaboPanel1">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">

                <div class="card-header header-elements-inline">
                    <legend id="carrera-label" class="text-uppercase font-size-sm font-weight-bold">

                    </legend>

                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Periodo:</label>
                                <div class="form-control-plaintext" id="periodo-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Duración:</label>
                                <div class="form-control-plaintext" id="duracion-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Inicio:</label>
                                <div class="form-control-plaintext" id="inicio-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Fin:</label>
                                <div class="form-control-plaintext" id="fin-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Curso:</label>
                                <div class="form-control-plaintext" id="curso-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Sección:</label>
                                <div class="form-control-plaintext" id="seccion-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Horas Teoría:</label>
                                <div class="form-control-plaintext" id="hteoria-label"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Horas Práctica:</label>
                                <div class="form-control-plaintext" id="hpractica-label"></div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="font-weight-semibold">Estado:</label>
                                <div class="form-control-plaintext"><span class="badge" id="badge-estado"
                                                                          style="font-size: 1em;"></span></div>

                            </div>
                        </div>

                    </div>


                </div>


            </div>
            <!-- /basic table -->

        </div>
    </div>



    <div class="row" style="display: none" id="SilaboPanel2">
        <div class="col-md-12">
            <div class="card" id="SilaboCard" style="display: block">


                <div class="card-header header-elements-inline">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        Silabo de la seccion


                        <button type="button" style="float: right" data-toggle="modal"
                                data-target="#modal_lista_verificacion"
                                class="btn btn-info btn-sm rounded-round legitRipple"><i class="icon-check mr-2"></i>Lista
                            de verificación
                        </button>
                    </legend>

                </div>

                {{-- <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left">
                                <b><i class="icon-reading"></i></b> Registrar silabo</button> --}}
                <div id="section-no-document" style="display: block"
                     class="card-body justify-content-center text-center">
                    <i class="icon-question7 icon-2x text-blue border-blue border-3 rounded-round p-3 mb-3"></i>
                    <h5 class="card-title">Aun no ha registrado un sílabo para está sección.</h5>
                    <a href="javascript:void(0)" onclick="createSilabo()" class="btn bg-blue legitRipple">Registrar
                        silabo<i class="icon-arrow-right14 ml-2"></i></a>
                </div>


                <div id="section-iframe" style="display: none" class="card-body justify-content-center text-center">
                    <iframe style="width: 100%;
						height: 100em;
						overflow-x: hidden;" src="" frameborder="0"></iframe>
                </div>

                <div id="section-silabo" style="display: none" class="card-body">

                    <form id="form_copiar" action="{{route('IntranetDocente.silabos.copiar')}}" method="POST">

                        @csrf


                        <input type="hidden" id="silabo_destino_id" name="silabo_destino_id" value="0">

                        <div class="row">
                            <div class="col-md-3">
                                <label class="d-block font-weight-semibold">Copiar silabo desde: </label>
                                <select id="list-secciones-silabo" name="silabo_origen_id" class="form-control">

                                </select>

                            </div>

                            <div class="col-md-3">
                                <label class="d-block" style="opacity: 0">-</label>
                                <button type="submit" class="btn btn-outline-primary">Copiar</button>

                            </div>
                        </div>

                        <hr>

                    </form>


                    <form id="form-silabo" action="" method="POST" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" id="id" name="id" value="0">
                        <input type="hidden" class="seccion_id" name="seccion_id" value="0">
                        <input type="hidden" id="silabo_exist" value="0">

                        <div class="col-md-12">

                            <label class="d-block font-weight-semibold">Estructura de silabo</label>

                            <input type="hidden" id="tipo" name="tipo" value="2019">

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" name="radio_tipo" value="2019"
                                           class="form-check-input-styled-primary" checked data-fouc>
                                    FORMATO ANTIGUO (2010)
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" name="radio_tipo" value="2020"
                                           class="form-check-input-styled-primary" data-fouc>
                                    FORMATO NUEVO
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" name="radio_tipo" value="archivo"
                                           class="form-check-input-styled-primary" data-fouc>
                                    SUBIR ARCHIVO
                                </label>
                            </div>
                        </div>
                        <br><br>
                        <div class="col-md-12">
                            <div class="form-group form-group-float" id="content_archivo" style="display: none">
                                <label class="form-group-float-label is-visible" style="font-weight: bold"
                                       id="archivo_label">Archivo</label>
                                <input type="file" id="input_archivo" name="archivo">
                            </div>
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible" style="font-weight: bold"
                                       id="fundamentacion-sumilla-label">Fundamentación</label>
                                <textarea id="fundamentacion" name="fundamentacion" rows="5" cols="5"
                                          class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible" style="font-weight: bold"
                                       id="proyecto-label">Proyecto institucional</label>
                                <textarea id="proyecto_institucional" name="proyecto_institucional" rows="4" cols="5"
                                          class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="form-group form-group-float" id="temas-transversales-seccion">
                                <label class="form-group-float-label is-visible" style="font-weight: bold">Temas
                                    transversales</label>
                                <textarea id="temas_transversales" name="temas_transversales" rows="4" cols="5"
                                          class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="form-group form-group-float" id="enfoque-derecho-seccion" style="display: none">
                                <label class="form-group-float-label is-visible" style="font-weight: bold">Enfoque de
                                    derecho</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">Enfoque</label>
                                        <textarea id="enfoque_derecho_1" name="enfoque_derecho_1" rows="5" cols="5"
                                                  class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">¿Cuándo son
                                            observables?</label>
                                        <textarea id="enfoque_derecho_2" name="enfoque_derecho_2" rows="5" cols="5"
                                                  class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">¿En qué acciones concretas se
                                            observa?</label>
                                        <textarea id="enfoque_derecho_3" name="enfoque_derecho_3" rows="5" cols="5"
                                                  class="form-control" placeholder=""></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group form-group-float" id="enfoque-intercultural-seccion"
                                 style="display: none">
                                <label class="form-group-float-label is-visible" style="font-weight: bold">Enfoque de
                                    intercultural</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">Enfoque</label>
                                        <textarea id="enfoque_intercultural_1" name="enfoque_intercultural_1" rows="5"
                                                  cols="5" class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">¿Cuándo son
                                            observables?</label>
                                        <textarea id="enfoque_intercultural_2" name="enfoque_intercultural_2" rows="5"
                                                  cols="5" class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">¿En qué acciones concretas se
                                            observa?</label>
                                        <textarea id="enfoque_intercultural_3" name="enfoque_intercultural_3" rows="5"
                                                  cols="5" class="form-control" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-float" id="competencias-seccion" style="display: none">
                                <label class="form-group-float-label is-visible" style="font-weight: bold">Articulación
                                    entre competencias del perfil y estándares de la FID</label>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">Competencias</label>
                                        <textarea id="competencias" name="competencias" rows="5" cols="5"
                                                  class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">Capacidades</label>
                                        <textarea id="capacidades" name="capacidades" rows="5" cols="5"
                                                  class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-group-float-label is-visible">Estandar</label>
                                        <textarea id="estandar" name="estandar" rows="5" cols="5" class="form-control"
                                                  placeholder=""></textarea>
                                    </div>
                                </div>

                            </div>
                            <hr>
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible" style="font-weight: bold">Organización
                                    y evaluación de los aprendizajes</label>

                                <div class="row" style="margin-top:5%" id="aviso-unidades-seccion">

                                    <div class="col-md-12 text-center">
                                        <i class="icon-question7 icon-2x text-blue border-blue border-2 rounded-round p-2 mb-2S"></i>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <span class="text-info">Debe registrar el presente silabo para editar los aprendizajes.</span>
                                    </div>
                                </div>

                                <div class="row" id="unidades-seccion" style="display: none">


                                </div>

                            </div>

                            <hr>

                            <div class="form-group form-group-float" id="metodologias-seccion" style="display: none">
                                <label class="form-group-float-label is-visible"
                                       style="font-weight: bold">Metodologia</label>
                                <textarea id="metodologia" name="metodologia" rows="3" cols="5" class="form-control"
                                          placeholder=""></textarea>
                            </div>

                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible"
                                       style="font-weight: bold">Bibliografía</label>
                                <textarea id="bibliografia" name="bibliografia" rows="4" cols="5" class="form-control"
                                          placeholder=""></textarea>
                            </div>

                            <hr>

                            <button type="submit" id="guardar_cambios" class="btn bg-primary legitRipple">Guardar
                                cambios
                            </button>
                            <button type="button" onclick="vista_previa()" class="btn btn-info ">Vista previa</button>
                            <button type="button" id="btn_send_check" class="btn bg-info legitRipple" disabled>Enviar
                                para Revisar
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="display: none">
            <div class="card" id="ProgramacionContenidosCard">
                <div class="card-header header-elements-inline">
                    <legend class="text-uppercase font-size-sm font-weight-bold">
                        Programación de unidades y contenidos
                    </legend>

                </div>

                <div class="card-body justify-content-center text-center">
                    <button type="button" style="float: left;    margin-right: 1%;" id="btn_unidad" data-toggle="modal"
                            data-target="#modal_unidad" class="btn bg-teal-400 btn-labeled btn-labeled-left">
                        <b><i class="icon-reading"></i></b> Registrar unidad
                    </button>

                    <a style="float: left;" href="https://classroom.google.com/u/2/h" target="_blank"
                       class="btn bg-teal-400 btn-labeled">
                        Aula Virutal</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="float: right;" onclick="registrarClassroom()"
                       class="btn bg-teal-400 btn-labeled btn-labeled-left">
                        <b><i class="icon-reading"></i></b> Registrar en Classroom</a>

                    <div id="unidades-aprendizaje-seccion" class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Unidad</th>
                                <th>Denominación</th>
                                <th>Capacidades</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5">
                                    AUN NO SE HAN REGISTRADO UNIDADES DE APRENDIZAJE.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>



    <!-- /solid tabs -->
    {{-- </form> --}}

    <input type="hidden" id="url_cambiar_estado" value="{{route('silabo.cambiar_estado')}}">

@endsection


@section('modals')

    <!-- Basic modal -->
    <div id="modal_create_silabo" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form_create_silabo" action="/IntranetDocente/silabos" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">REGISTRAR SILABO</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" class="seccion_id" id="seccion_id" name="seccion_id" value="0">

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">URL Silabo</label>
                            <div class="col-sm-9">
                                <input type="url" required name="url_document"
                                       placeholder="Copie aquí la url del archivo de Google Drive."
                                       class="form-control">
                            </div>
                        </div>


                        {{-- <hr> --}}


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

    <!-- Basic modal -->
    <div id="modal_unidad" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form_unidad" action="/IntranetDocente/unidades/store" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="titulo-modal-unidades">REGISTRAR UNIDAD</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" class="silabo_id" name="silabo_id" value="0">
                        <input type="hidden" id="unidades_id" name="unidades_id" value="0">

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Denominación</label>
                            <div class="col-sm-9">
                                <input type="text" required id="denominacion" name="denominacion"
                                       placeholder="Denominacion de la unidad" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Nro Unidad</label>
                            <div class="col-sm-9">
                                <input type="number" required id="unidad" name="unidad" placeholder="Numero de unidad"
                                       class="form-control">

                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label class="col-form-label col-sm-3">Capacidades</label>
                            <div class="col-sm-9">

                                <textarea name="capacidades"  cols="30" rows="4" class="form-control"></textarea>

                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Fecha inicio</label>
                            <div class="col-sm-9">
                                <input type="date" required id="fecha_inicio" name="fecha_inicio" class="form-control">


                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Fecha fin</label>
                            <div class="col-sm-9">
                                <input type="date" required id="fecha_fin" name="fecha_fin" class="form-control">


                            </div>
                        </div>


                        {{-- <hr> --}}


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

    <!-- Large modal -->
    <div id="modal_contenidos_" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div id="contenidos-seccion" class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Semana</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th>Contenido</th>
                                <th>Destrezas</th>
                                <th>Estrategias</th>
                                <th>Vaores</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="8">
                                    AUN NO SE HAN REGISTRADO CONTENIDOS.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Registrar</button>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- /large modal -->


    <!-- Large modal -->
    <div id="modal_contenidos" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo-modal-contenidos"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form id="form_semana" action="{{route('IntranetDocente.contenidos.store')}}" method="POST">

                    <div class="modal-body">

                        @csrf

                        <input type="hidden" class="unidades_id" name="unidades_id" value="0">
                        <input type="hidden" id="semana_id" name="semana_id" value="0">

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">Nro semana</label>
                            <div class="col-sm-1">
                                <input type="number" min="1" required id="nro_semana" name="nro_semana"
                                       class="form-control">
                            </div>

                            <label class="col-form-label col-sm-2">Fecha inicio</label>
                            <div class="col-sm-3">
                                <input type="date" required id="fecha_inicio_semana" name="fecha_inicio"
                                       class="form-control">
                            </div>

                            <label class="col-form-label col-sm-1">Fecha fin</label>
                            <div class="col-sm-3">
                                <input type="date" required id="fecha_fin_semana" name="fecha_fin" class="form-control">
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label class="col-form-label col-sm-3">Fecha inicio</label>
                            <div class="col-sm-9">
                                <input type="date" required id="fecha_inicio" name="fecha_inicio" class="form-control">
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Fecha fin</label>
                            <div class="col-sm-9">
                                <input type="date" required id="fecha_fin" name="fecha_fin" class="form-control">
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label ">Código de competencia y capacidad</label>

                                    <textarea id="capacidad_semana" name="capacidades" rows="5" cols="5"
                                              class="form-control" placeholder=""></textarea>
                                </div>

                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label ">Indicadores / Desempeño</label>

                                    <textarea id="indicadores_desempeno" name="indicadores_desempeno" rows="5" cols="5"
                                              class="form-control" placeholder=""></textarea>
                                </div>

                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label ">Instrumentos</label>

                                    <textarea id="instrumentos" name="instrumentos" rows="2" cols="5"
                                              class="form-control"
                                              placeholder=""></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label">Producto o evidencias</label>

                                    <textarea id="producto_evidencias" name="producto_evidencias" rows="2" cols="5"
                                              class="form-control" placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label ">Conocimientos</label>

                                    <textarea id="conocimientos" name="conocimientos" rows="5" cols="5"
                                              class="form-control"
                                              placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group row">
                                    <label class="col-form-label">Estrategias</label>

                                    <textarea id="estrategias" name="estrategias" rows="5" cols="5" class="form-control"
                                              placeholder=""></textarea>
                                </div>

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn bg-primary">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /large modal -->

    <!-- Basic modal -->
    <div id="modal_desempeno_criterios" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form_desempeno_criterio" action="/IntranetDocente/criterio-desempeno/store" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="titulo-modal-depempeno_criterios">REGISTRAR ESTÁNDARES DE
                            DESEMPEÑO</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" id="desempeno_criterio_id" name="desempeno_criterio_id" value="0">
                        <input type="hidden" id="unidades_id_cd" name="unidades_id" value="0">

{{--                        <div class="form-group row">--}}
{{--                            <label class="col-form-label col-sm-3">Titulo</label>--}}
{{--                            <div class="col-sm-9">--}}
{{--                                <select id="titulo" name="titulo" class="form-control">--}}
{{--                                    <option>Seleccione una dimension para mostrar sus criterios</option>--}}
{{--                                    @foreach($dimensiones as $dimension)--}}
{{--                                        <option value="{{$dimension->dimension}}">{{$dimension->dimension}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                --}}{{--							<input type="text" required id="titulo" name="titulo" placeholder="" class="form-control">--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group row">
                            <label class="col-form-label col-sm-3">Descripción</label>
                            <div class="col-sm-9">
                                <div class='form-group'>
                                    <textarea id="descripcion_desempeno_criterio" required name="descripcion" rows="4" cols="5" class="form-control" placeholder=""></textarea>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

    <div id="modal_lista_verificacion" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="lista_verificacion_content">

            </div>
        </div>
    </div>

    <input type="hidden" id="url_get_criterios" value="{{route('criterioDimension.get_dimensiones')}}">

    <!-- Large modal -->
    <div id="modal-preview" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo-modal-contenidos"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>


                <div class="modal-body">
                    <iframe id="pre_view" frameborder="0" height="500em" width="100%"></iframe>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                </div>

                </form>
            </div>
        </div>
    </div>
    <!-- /large modal -->

@endsection


@section('script')
    <script src="{{asset('assets/js/moment.js')}}"></script>

    {{--    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>--}}
    <script>
        // moment.locale('en')
        $(function () {
            $('#titulo').change(function () {

                var url = $('#url_get_criterios').val();
                var data = {'dimension': $(this).val(), 'carrera_id': $('#selectCarrera').val()}
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        data.forEach(element => {
                            var template = "<div class='form-check'><label class='form-check-label'>" +
                                "<input type='checkbox' name='criterio[]' class='form-check-input' value='" + element.criterio + "'>" + element.criterio + "</label></div>"
                            $('#list_check').append(template)
                        })
                    }
                })
            })
        })

        function vista_previa() {
            // console.log('hola')
            $('#modal-preview').modal('show')
        }
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });
        var seccion = null;
        var googleClassroomCourse = null;

        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary text-primary'
        });

        $('#modal_contenidos').on('hide.bs.modal', function () {

            $('.unidades_id').val(0);
            $('#semana_id').val(0);
            $('#nro_semana').val('');
            $('#fecha_inicio_semana').val('');
            $('#fecha_fin_semana').val('');

            $('#indicadores_desempeno').val('');
            $('#capacidad_semana').val('');
            $('#estrategias').val('');
            $('#instrumentos').val('');
            $('#conocimientos').val('');
            $('#producto_evidencias').val('');
        });

        $('#modal_unidad').on('hide.bs.modal', function () {

            $('#unidades_id').val(0);
            $('#denominacion').val('');
            $('#unidad').val('');
            $('#fecha_inicio').val('');
            $('#fecha_fin').val('');

            $('#titulo-modal-unidades').html('REGISTRAR UNIDAD');

        });

        $('#modal_desempeno_criterios').on('hide.bs.modal', function () {


            $('#unidades_id_cd').val(0);

            $('#titulo').val('');
            $('#descripcion_desempeno_criterio').val('');

            $('#titulo-modal-depempeno_criterios').html('REGISTRAR CRITERIOS DE DESEMPEÑO');

        });


        $("input[name='radio_tipo']").change(function () {
            var tipo = $(this).val();
            $("#tipo").val(tipo);

            if (tipo == '2020') {
                $('#fundamentacion-sumilla-label').html('Sumilla');
                $('#proyecto-label').html('Vinculación con proyecto integrador');

                $('#fundamentacion').parent().css('display', 'block')
                $('#proyecto_institucional').parent().css('display', 'block')
                $('#temas_transversales').parent().css('display', 'block')
                $('#bibliografia').parent().css('display', 'block')
                $('#temas-transversales-seccion').css('display', 'none');
                $('#enfoque-derecho-seccion').css('display', 'block');
                $('#enfoque-intercultural-seccion').css('display', 'block');
                $('#competencias-seccion').css('display', 'block');
                $('#metodologias-seccion').css('display', 'block');
                $('#content_archivo').hide()


            }
            if (tipo == '2019') {
                $('#fundamentacion-sumilla-label').html('Fundamentación');
                $('#proyecto-label').html('Proyecto institucional');

                $('#fundamentacion').parent().css('display', 'block')
                $('#proyecto_institucional').parent().css('display', 'block')
                $('#temas_transversales').parent().css('display', 'block')
                $('#bibliografia').parent().css('display', 'block')
                $('#temas-transversales-seccion').css('display', 'block');
                $('#enfoque-derecho-seccion').css('display', 'none');
                $('#enfoque-intercultural-seccion').css('display', 'none');
                $('#competencias-seccion').css('display', 'none');
                $('#metodologias-seccion').css('display', 'none');
                $('#content_archivo').hide()
            }
            if (tipo == 'archivo') {
                $('#enfoque-derecho-seccion').css('display', 'none');
                $('#enfoque-intercultural-seccion').css('display', 'none');
                $('#competencias-seccion').css('display', 'none');
                $('#metodologias-seccion').css('display', 'none');
                $('#fundamentacion').parent().css('display', 'none');
                $('#proyecto_institucional').parent().css('display', 'none')
                $('#bibliografia').parent().css('display', 'none')
                $('#temas_transversales').parent().css('display', 'none')

                $('#content_archivo').show();
            }
        });


        function handleClientLoad() {
            gapi.load('client:auth2', initClient);
        }

        /**
         *  Initializes the API client library and sets up sign-in state
         *  listeners.
         */
        function initClient() {

            // Client ID and API key from the Developer Console
            var CLIENT_ID = '151251881265-28l4usnlealreulu3gil4gvjgn4c13hg.apps.googleusercontent.com';
            var API_KEY = 'AIzaSyDX-4T_kRdldExpDmPBgZ7mqHG2poLsipo';

            // Array of API discovery doc URLs for APIs used by the quickstart
            var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/classroom/v1/rest",
                "https://people.googleapis.com/$discovery/rest?version=v1",
                "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

            // Authorization scopes required by the API; multiple scopes can be
            // included, separated by spaces.
            // var SCOPES = "https://www.googleapis.com/auth/classroom.courses.readonly";

            var SCOPES = "https://www.googleapis.com/auth/classroom.courses.readonly https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/classroom.courses https://www.googleapis.com/auth/classroom.topics https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/classroom.rosters https://www.googleapis.com/auth/classroom.profile.emails https://www.googleapis.com/auth/classroom.profile.photos";
            // var SCOPES= 'profile';


            gapi.client.init({
                apiKey: API_KEY,
                clientId: CLIENT_ID,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES
            }).then(function () {
                // Listen for sign-in state changes.
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                // Handle the initial sign-in state.
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            }, function (error) {
                console.log(JSON.stringify(error, null, 2));
            });
        }

        function updateSigninStatus(isSignedIn) {
            if (isSignedIn) {
                //   listCourses();
                // user_info();
                // alert('logged in');

            } else {
                // alert('not  logged in');
                gapi.auth2.getAuthInstance().signIn();
            }
        }

        function createGoogleClassroomCourseTopics(course_id) {

            seccion.silabo.unidades.forEach(unidad => {
                unidad.contenidos.forEach(contenido => {

                    gapi.client.classroom.courses.topics
                        .create({
                            'courseId': course_id,
                            'name': ('Semana ' + contenido.nro_semana + ' del ' + contenido.fecha_inicio + ' al ' + contenido.fecha_fin)
                        })
                        .then(function (responseTopic) {
                            console.log(responseTopic.result);
                        });

                });
            });


        }

        function registerStudentsToClassroomCourse() {


            if (seccion.google_classroom_course_id != null && seccion.google_classroom_course_id != '') {


                $("body").block({
                    message: '<div class="loader"></div> <p><br />Guardando los cambios...</p>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });

                var data = {'seccion_id': seccion.id};
                $.ajax({
                    type: "GET",
                    url: '/IntranetDocente/secciones/alumnos',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        console.log('Enrolled students: ');
                        console.log(response);

                        response.forEach(detalle => {
                            gapi.client.classroom.invitations.create({
                                courseId: seccion.google_classroom_course_id,
                                userId: detalle.matricula.alumnocarrera.alumno.persona.email,
                                role: 'STUDENT'
                            })
                                .then(function (responseInvitation) {
                                    console.log('Invitation made');
                                    console.log(responseInvitation);
                                });
                        });

                        $("body").unblock();

                        alert('Las invitaciones han sido enviadas.');

                    }
                });


            } else {
                alert('El curso aun no ha sido creado en CLASSROOM');
            }
        }

        function delelteGoogleClassroomCourse(id) {
            gapi.client.classroom.courses.delete({'id': id})
                .then(function (response) {
                    console.log(response);
                });
        }

        function createGoogleClassroomCalendarEvents() {
            // var classroomCourse=getCourse(seccion.googleClassroomCourseId);

            if (seccion.google_classroom_course_id != null && seccion.google_classroom_course_id != '') {


                $("body").block({
                    message: '<div class="loader"></div> <p><br />Registrando sesiones de aprendizaje...</p>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });

                gapi.client.classroom.courses.get({'id': seccion.google_classroom_course_id})
                    .then(function (response) {

                        console.log(response.result);
                        var classroomCourse = response.result;
                        var data = {'seccion_id': seccion.id, 'periodo_id': seccion.periodo_id};


                        $.ajax({
                            type: "GET",
                            url: '/IntranetDocente/horarios/sesionesApredizaje',
                            dataType: 'json',
                            data: data,
                            success: function (sesiones) {
                                console.log(sesiones);

                                var cont = 1;
                                var eventDateTime = '';
                                var eventDateTimeEnd = '';
                                sesiones.forEach(sesion => {

                                    eventDateTimeStart = sesion.date + ' ' + sesion.horario.hora_inicio + ':00';
                                    eventDateTimeEnd = sesion.date + ' ' + sesion.horario.hora_fin + ':00';

                                    gapi.client.calendar.events.insert({
                                        'sendNotifications': true,
                                        'sendUpdates': 'all',
                                        'calendarId': classroomCourse.calendarId,
                                        'conferenceDataVersion': 1,
                                        'resource': {
                                            'end': {
                                                'dateTime': (new Date(eventDateTimeEnd)).toISOString(),
                                                'timeZone': 'America/Lima',
                                            },

                                            'start': {
                                                'dateTime': (new Date(eventDateTimeStart)).toISOString(),
                                                'timeZone': 'America/Lima',
                                            },

                                            'summary': 'Sesion de aprendizaje N° ' + cont,
                                            'secuence': cont,

                                            // 'reminders': {
                                            // 	'useDefault': true,
                                            // 	'overrides': [
                                            // 		{'method': 'email', 'minutes': 24 * 60},
                                            // 		{'method': 'popup', 'minutes': 10}
                                            // 	]
                                            // },

                                            // 'guestsCanInviteOthers' : false,

                                            // "conferenceData": {
                                            // 	"createRequest": {
                                            // 		"requestId": "Sesion-"+seccion.id+"-"+cont,
                                            // 		"conferenceSolutionKey" :{
                                            // 			"type" : "hangoutsMeet"
                                            // 		}
                                            // 	}
                                            // }

                                        }

                                    }).then(function (responseEvent) {
                                        console.log('Event registered');
                                        console.log(responseEvent);
                                    });

                                    cont++;

                                });

                                $("body").unblock();
                            }
                        });


                    });

            } else {
                alert('El curso aun no ha sido creado en CLASSROOM');
            }


        }

        function createGoogleClassroomCourse() {

            var isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get();

            if (isSignedIn) {


                if (seccion.google_classroom_course_id == null || seccion.google_classroom_course_id == '') {


                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Guardando los cambios...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    gapi.client.classroom.courses.create({
                        'name': seccion.pe_curso.curso.nombre + ' - ' + seccion.seccion,
                        'section': seccion.seccion,
                        'descriptionHeading': '',
                        'description': 'Bienvenido a ' + seccion.pe_curso.curso.nombre + ' (' + seccion.periodo.descripcion + ')',
                        'room': '',
                        'ownerId': 'me',
                        'courseState': 'ACTIVE'
                    })
                        .then(function (response) {
                            googleClassroomCourse = response.result

                            console.log(googleClassroomCourse);
                            // console.log(googleClassroomCourse.id);
                            // console.log(seccion.silabo.unidades[0].contenidos[0]);

                            createGoogleClassroomCourseTopics(googleClassroomCourse.id);

                            updateGoogleClassroomCourseId(googleClassroomCourse.id);

                            // googleClassroomCourse=getCourse(response.result.id);
                        });


                } else {

                    alert('El presente curso ya se encuentra registrado en CLASSROOM.');
                }

            } else {
                gapi.auth2.getAuthInstance().signIn();
            }


        }

        function updateGoogleClassroomCourseId(googleClassroomCourseId) {
            var token = '{{csrf_token()}}';
            var data = {'seccion_id': seccion.id, 'googleClassroomCourseId': googleClassroomCourseId, '_token': token};
            $.ajax({
                type: "POST",
                url: '/IntranetDocente/seccion/updateGoogleClassroomCourseId',
                dataType: 'json',
                data: data,
                success: function (data) {
                    console.log(data);
                    $('#selectGrupo').change();

                    $("body").unblock();

                    alert('Curso creado en GOOGLE CLASSROOM');
                    // googleClassroomCourse=getCourse(googleClassroomCourseId);
                }
            });
        }

        function openClassroomCourse() {
            if (seccion.google_classroom_course_id != null && seccion.google_classroom_course_id != '') {
                gapi.client.classroom.courses.get({'id': seccion.google_classroom_course_id})
                    .then(function (response) {

                        var win = window.open(response.result.alternateLink + '?authuser={{Auth::user()->persona->email}}', '_blank');
                        win.focus();

                    });
            } else {
                alert('El curso aun no ha sido creado en CLASSROOM');
            }
        }

        function openClassroomCourseDriveFolder() {
            if (seccion.google_classroom_course_id != null && seccion.google_classroom_course_id != '') {
                gapi.client.classroom.courses.get({'id': seccion.google_classroom_course_id})
                    .then(function (response) {

                        var win = window.open(response.result.teacherFolder.alternateLink + '?authuser={{Auth::user()->persona->email}}', '_blank');
                        win.focus();

                    });
            } else {
                alert('El curso aun no ha sido creado en CLASSROOM');
            }
        }

        function openClassroomCourseCalendar() {
            if (seccion.google_classroom_course_id != null && seccion.google_classroom_course_id != '') {
                gapi.client.classroom.courses.get({'id': seccion.google_classroom_course_id})
                    .then(function (response) {

                        var win = window.open('https://calendar.google.com/calendar/r?cid=' + response.result.calendarId + '&authuser={{Auth::user()->persona->email}}', '_blank');
                        win.focus();

                    });
            } else {
                alert('El curso aun no ha sido creado en CLASSROOM');
            }
        }

        function user_info() {

            // gapi.client.request({
            // 'path': 'https://people.googleapis.com/v1/people/me?requestMask.includeField=person.names',
            // })
            gapi.client.people.people.get({
                'resourceName': 'people/me',
                'requestMask.includeField': 'person.names'
            })
                .then(function (response) {
                    console.log(response);
                });


        }

        function getGoogleDriveFolder(id) {

        }

        function listCourses() {
            gapi.client.classroom.courses.list({
                pageSize: 10
            }).then(function (response) {
                var courses = response.result.courses;
                console.log('Courses:');

                if (courses.length > 0) {
                    for (i = 0; i < courses.length; i++) {
                        var course = courses[i];
                        console.log(course.name + ' ' + course.id)
                    }
                } else {
                    console.log('No courses found.');
                }
            });
        }

        function getCourse(id) {
            gapi.client.classroom.courses.get({'id': id})
                .then(function (response) {
                    console.log(response.result);
                });
        }

        function open_modal_contenidos(unidades_id) {
            $('#modal_contenidos').modal({
                backdrop: 'static',
                keyboard: false
            });

            $('#titulo-modal-contenidos').html('REGISTRAR SEMANA');
            $('.unidades_id').val(unidades_id);
        }

        function open_modal_desempeno_criterios(unidades_id) {
            $('#list_check').html('')
            $('#modal_desempeno_criterios').modal({
                backdrop: 'static',
                keyboard: false
            });

            // $('#titulo-modal-depempeno_criterios').html('REGISTRAR CRITERIOS DE DESEMPEÑO');
            $('#unidades_id_cd').val(unidades_id);
        }

        function open_modal_unidades_edit(unidades_id) {

            var data = {'unidades_id': unidades_id};

            $.ajax({
                type: "GET",
                url: '/unidades/show',
                dataType: 'json',
                data: data,
                success: function (data) {
                    console.log(data);

                    $('#modal_unidad').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    $('#titulo-modal-unidades').html('EDITAR UNIDAD');


                    $('#unidades_id').val(data.id);
                    $('#denominacion').val(data.denominacion);
                    $('#unidad').val(data.unidad);
                    $('#fecha_inicio').val(data.fecha_inicio);
                    $('#fecha_fin').val(data.fecha_fin);


                }
            });

        }

        function open_modal_contenidos_edit(semana_id) {

            var data = {'semana_id': semana_id};

            $.ajax({
                type: "GET",
                url: '/contenidos/show',
                dataType: 'json',
                data: data,
                success: function (data) {
                    console.log(data);

                    $('#modal_contenidos').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    $('#titulo-modal-contenidos').html('EDITAR SEMANA');

                    $('.unidades_id').val(data.unidades_id);
                    $('#semana_id').val(data.id);
                    $('#nro_semana').val(data.nro_semana);
                    $('#fecha_inicio_semana').val(data.fecha_inicio);
                    $('#fecha_fin_semana').val(data.fecha_fin);

                    $('#indicadores_desempeno').val(data.indicadores_desempeno);
                    $('#capacidad_semana').val(data.capacidades);
                    $('#estrategias').val(data.estrategias);
                    $('#instrumentos').val(data.instrumentos);
                    $('#conocimientos').val(data.conocimientos);
                    $('#producto_evidencias').val(data.producto_evidencias);

                }
            });

        }

        function delete_unidad(unidades_id) {

            var r = confirm("Se eliminará la unidad y contenidos\n¿Desea eliminar esta unidad?");

            if (r) {
                var data = {'unidades_id': unidades_id};

                $.ajax({
                    type: "GET",
                    url: '/IntranetDocente/unidades/eliminar',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        alert('La unidad ha sido eliminada');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id)
                    }
                });
            }
        }

        function delete_semana(semana_id) {

            var r = confirm("¿Desea eliminar esta semana?");

            if (r) {
                var data = {'semana_id': semana_id};

                $.ajax({
                    type: "GET",
                    url: '/IntranetDocente/contenidos/eliminar',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        alert('La semana ha sido eliminado.');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id)
                    }
                });
            }
        }

        function delete_desempeno_criterio(desempeno_criterio_id) {

            var r = confirm("¿Desea eliminar este criterio de desempeño?");

            if (r) {
                var data = {'desempeno_criterio_id': desempeno_criterio_id};

                $.ajax({
                    type: "GET",
                    url: '/IntranetDocente/criterio-desempeno/eliminar',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        alert('El criterio de desempeño ha sido eliminado.');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id)
                    }
                });
            }
        }

        function createSilabo() {
            $('#SilaboCard').find('#section-no-document').css('display', 'none');
            $('#SilaboCard').find('#section-silabo').css('display', 'block');

            $('#id').val(0);
            $('.silabo_id').val(0);
            $('#fundamentacion').val('');
            $('#proyecto_institucional').val('');
            $('#bibliografia').val('');

            $("input[value='2019']").click();
            $('#tipo').val('2019');

            $('#temas_transversales').val('');

            $('#enfoque_derecho_1').val('');
            $('#enfoque_derecho_2').val('');
            $('#enfoque_derecho_3').val('');
            $('#enfoque_intercultural_1').val('');
            $('#enfoque_intercultural_2').val('');
            $('#enfoque_intercultural_3').val('');
            $('#competencias').val('');
            $('#capacidades').val('');
            $('#estandar').val('');
            $('#metodologia').val('');

        }

        function registrarClassroom() {

            var seccion_id = $('#seccion_id').val();
            var data = {'id': seccion_id};

            alert("Registrando informacion en classroom, espere un momento, por favor.");

            $.ajax({
                type: "GET",
                url: "/CreateGoogleClassroomCourseAndTopics",
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    alert("Registro exitoso");
                    // if(data.success=='true'){
                    // 	alert("Registro exitoso");
                    // }else{
                    // 	alert("Error");
                    // }
                }
            });

        }

        function load_unidades_crud(silabo_id) {
            var data = {'silabo_id': silabo_id};

            $.ajax({
                type: "GET",
                url: '/IntranetDocente/unidades/parcial',
                // dataType:'json',
                data: data,
                success: function (data) {
                    // console.log(data);
                    $('#unidades-seccion').html(data);
                }
            });
        }

        function load_modal_content_lista_verificacion(silabo_id) {
            var data = {'id': silabo_id};

            $.ajax({
                type: "GET",
                url: '/modalContentListaVerificacion',
                // dataType:'json',
                data: data,
                success: function (data) {
                    // console.log(data);
                    $('#lista_verificacion_content').html(data);
                    $('.form-check-input-styled-primary').uniform({
                        wrapperClass: 'border-primary text-primary'
                    });
                }
            });
        }

        // function load_semanas_table(unidades_id){
        // 	var data={'unidades_id':silabunidades_ido_id};

        // 	$.ajax({
        // 		type: "GET",
        // 		url: '/IntranetDocente/semanas',
        // 		dataType:'json',
        // 		data    : data,
        // 		success: function(data){
        // 			// console.log(data);
        // 			// $('#unidades-seccion').html(data);
        // 		}
        // 	});
        // }

        // function modal_contenidos(id){

        // 	var data={'id':id};

        // 	$.ajax({
        //         type: "GET",
        //         url: "/ContenidosPorUnidad",
        //         data: data,
        //         dataType:'json',
        //         success: function(data){
        // 			console.log(data);

        // 			$('#contenidos-seccion').find('table').find('tbody').html('');

        // 			data.forEach(element => {
        // 				var template = "<tr>"
        // 								+ "<td>" + element.nro_semana + "</td>"
        // 								+ "<td>" + element.fecha_inicio + "</td>"
        // 								+ "<td>" + element.fecha_fin + "</td>"
        // 								+ "<td style='white-space: pre-wrap;'><a title='" + element.contenido + "'><i class='icon-info3'></i></a></td>"
        // 								+ "<td style='white-space: pre-wrap;'>" + element.destrezas + "</td>"
        // 								+ "<td style='white-space: pre-wrap;'>" + element.estrategias + "</td>"
        // 								+ "<td style='white-space: pre-wrap;'>" + element.valores_actitudes + "</td>"


        // 								+ "<td></td>"
        // 								+ "</tr>";

        // 				$('#contenidos-seccion').find('table').find('tbody').append(template)
        // 			});

        // 			$('#modal_contenidos').modal({
        // 				backdrop: 'static',
        // 				keyboard: false
        // 			});

        //         }
        //     });

        // }


        // function procesar_tabla_unidades(unidades){
        // 	unidades.forEach(element => {
        // 		var template = "<tr>"
        // 						+ "<td>" + element.unidad + "</td>"
        // 						+ "<td>" + element.denominacion + "</td>"
        //						+ "<td style='white-space: pre-wrap;'>" + element.capacidades + "</td>"
        // 						+ "<td>" + element.fecha_inicio + "</td>"
        // 						+ "<td>" + element.fecha_fin + "</td>"
        // 						+ "<td><button type='button' class='btn btn-light legitRipple' onclick='modal_contenidos("+element.id+")' data-dismiss='modal'>Contenidos</button></td>"
        // 						+ "</tr>";

        // 		$('#unidades-aprendizaje-seccion').find('table').find('tbody').append(template)
        // 	});


        // }

        document.addEventListener('DOMContentLoaded', function () {

            $('.form-control-select2').select2();

            $('#selectPeriodo').change(function () {
                var id = $(this).val();

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Cargando programas...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });


                    var data = {'id': id};

                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/CarrerasDocentePorPeriodo",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            $("body").unblock();

                            $("#selectCarrera").html('');
                            $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            for (var i in data) {
                                $("#selectCarrera").append(new Option(data[i].nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                        }
                    });

                } else {
                    $("#selectCarrera").html('');
                    $("#selectCarrera").append(new Option('Seleccione una carrera', ''));
                    $('#selectCarrera').change();

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione un grupo', ''));
                }


            });


            $('#selectCarrera').change(function () {
                var id = $(this).val()


                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Cargando areas...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });


                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/AreasDocentePorCarrera",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            $("body").unblock();

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));
                            $('#selectArea').change();

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));
                            $('#selectGrupo').change();

                            for (var i in data) {
                                $("#selectArea").append(new Option(data[i].curso.nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {
                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                }


            });


            $('#selectArea').change(function () {
                var id = $(this).val()

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Cargando secciones...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/SeccionesDocentePorArea",
                        data: data,
                        dataType: 'json',
                        success: function (data) {

                            $("body").unblock();

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                            $('#selectGrupo').change();

                            for (var i in data) {
                                $("#selectGrupo").append(new Option(data[i].seccion, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {
                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                }
            });


            $('#selectGrupo').change(function () {
                var id = $(this).val()

                $("#btn_google").css('display', 'none');

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Cargando información de la sección...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    var data = {'seccion_id': id};
                    $.ajax({
                        type: "GET",
                        url: "/GetFullSeccion",
                        data: data,
                        dataType: 'json',
                        success: function (data) {

                            $("body").unblock();

                            // console.log(data);

                            seccion = data.seccion;

                            $("#form_copiar").css('display', 'none');

                            $("#btn_google").css('display', 'none');

                            ////////////////////////

                            $('.seccion_id').val(id);
                            $('#carrera-label').html(data.seccion.pe_curso.curso.carrera.nombre);
                            $('#periodo-label').html(data.seccion.periodo.descripcion);
                            $('#duracion-label').html(' semanas');
                            $('#inicio-label').html(data.seccion.periodo.fecha_inicio);
                            $('#fin-label').html(data.seccion.periodo.fecha_fin);
                            $('#curso-label').html(data.seccion.pe_curso.curso.nombre);
                            $('#seccion-label').html(data.seccion.seccion);
                            $('#hteoria-label').html(data.seccion.pe_curso.horas_teoria);
                            $('#hpractica-label').html(data.seccion.pe_curso.horas_practica);

                            $('#InfoPanel').css('display', 'none');
                            $('#SilaboPanel1').css('display', 'block');
                            $('#SilaboPanel2').css('display', 'block');


                            $('#SilaboCard').find('#section-no-document').css('display', 'none');
                            $('#SilaboCard').find('#section-silabo').css('display', 'none');

                            $('#SilaboCard').find('#section-iframe').find('iframe').prop('src', '');
                            // $('#SilaboCard').find('#section-no-document').css('display','none');
                            $('#SilaboCard').find('#section-iframe').css('display', 'none');

                            if (data.seccion.silabo != null) {


                                if (data.seccion.silabo.estado == 1) {
                                    ///////////////

                                    $('#btn_send_check').removeAttr('disabled').attr("onclick", "send_check(" + data.seccion.silabo.id + ")")
                                    $('#guardar_cambios').removeAttr('disabled')
                                    $('#badge-estado').removeClass('badge-success');
                                    $('#badge-estado').removeClass('badge-info');
                                    $('#badge-estado').addClass('badge-warning');
                                    $('#badge-estado').html('En Edición');


                                    $("#form_copiar").css('display', 'block');
                                    $("#list-secciones-silabo").html('');
                                    var secciones_con_silabo = data.secciones_con_silabo;
                                    $("#list-secciones-silabo").append(new Option('Seleccione una sección', 0));
                                    for (var i in secciones_con_silabo) {
                                        $("#list-secciones-silabo").append(new Option(secciones_con_silabo[i].pe_curso.curso.nombre + ' - ' + secciones_con_silabo[i].seccion + ' - ' + secciones_con_silabo[i].pe_curso.curso.carrera.nombre, secciones_con_silabo[i].silabo.id));
                                    }

                                    $("#silabo_destino_id").val(data.seccion.silabo.id);

                                    /////////////

                                    $("#silabo_exist").val('1')


                                    $('#SilaboCard').find('#section-no-document').css('display', 'none');
                                    $('#SilaboCard').find('#section-silabo').css('display', 'block');

                                    $('#id').val(data.seccion.silabo.id);
                                    $('.silabo_id').val(data.seccion.silabo.id);
                                    $('#fundamentacion').val(data.seccion.silabo.fundamentacion);
                                    $('#proyecto_institucional').val(data.seccion.silabo.proyecto_institucional);
                                    $('#bibliografia').val(data.seccion.silabo.bibliografia);

                                    $('#tipo').val(data.seccion.silabo.tipo);

                                    if (data.seccion.silabo.tipo == '2019') {
                                        $("input[value='2019']").click();
                                        $('#temas_transversales').val(data.seccion.silabo.temas_transversales);
                                        $('#temas_transversales').val(data.seccion.silabo.temas_transversales);
                                        $('#pre_view').prop('src', "{{route('IntranetDocente.silabos.pdf')}}?id=" + data.seccion.silabo.id);

                                    }

                                    if (data.seccion.silabo.tipo == '2020') {
                                        $("input[value='2020']").click();

                                        $('#enfoque_derecho_1').val(data.seccion.silabo.enfoque_derecho_1);
                                        $('#enfoque_derecho_2').val(data.seccion.silabo.enfoque_derecho_2);
                                        $('#enfoque_derecho_3').val(data.seccion.silabo.enfoque_derecho_3);
                                        $('#enfoque_intercultural_1').val(data.seccion.silabo.enfoque_intercultural_1);
                                        $('#enfoque_intercultural_2').val(data.seccion.silabo.enfoque_intercultural_2);
                                        $('#enfoque_intercultural_3').val(data.seccion.silabo.enfoque_intercultural_3);
                                        $('#competencias').val(data.seccion.silabo.competencias);
                                        $('#capacidades').val(data.seccion.silabo.capacidades);
                                        $('#estandar').val(data.seccion.silabo.estandar);
                                        $('#metodologia').val(data.seccion.silabo.metodologia);
                                        $('#pre_view').prop('src', "{{route('IntranetDocente.silabos.pdf')}}?id=" + data.seccion.silabo.id);
                                    }
                                    if (data.seccion.silabo.tipo == 'archivo') {
                                        $("input[value='archivo']").click();
                                        $('#pre_view').prop('src','https://docs.google.com/viewer?url='+window.location.origin + data.seccion.silabo.url_silabo);
                                    }


                                    $('#aviso-unidades-seccion').css('display', 'none');
                                    $('#unidades-seccion').css('display', 'block');

                                    load_unidades_crud(data.seccion.silabo.id);

                                    // load_modal_content_lista_verificacion(data.seccion.silabo.id);


                                    // if(data.silabo.unidades.length>0){
                                    // 	$('#unidades-aprendizaje-seccion').find('table').find('tbody').html('');

                                    // 	procesar_tabla_unidades(data.unidades);

                                    // }else{
                                    // 	$('#unidades-aprendizaje-seccion').find('table').find('tbody').html('<tr><td colspan="5">AUN NO SE HAN REGISTRADO UNIDADES DE APRENDIZAJE.</td></tr>');
                                    // }
                                    // console.log(data)

                                }


                                if (data.seccion.silabo.estado == 2) {
                                    $('#btn_send_check').attr('disabled', '');
                                    $('#guardar_cambios').attr('disabled', '')
                                    $('#badge-estado').removeClass('badge-warning');
                                    $('#badge-estado').removeClass('badge-success');
                                    $('#badge-estado').addClass('badge-info');
                                    $('#badge-estado').html('En revision');


                                    $("#form_copiar").css('display', 'block');
                                    $("#list-secciones-silabo").html('');
                                    var secciones_con_silabo = data.secciones_con_silabo;
                                    $("#list-secciones-silabo").append(new Option('Seleccione una sección', 0));
                                    for (var i in secciones_con_silabo) {
                                        $("#list-secciones-silabo").append(new Option(secciones_con_silabo[i].pe_curso.curso.nombre + ' - ' + secciones_con_silabo[i].seccion + ' - ' + secciones_con_silabo[i].pe_curso.curso.carrera.nombre, secciones_con_silabo[i].silabo.id));
                                    }

                                    $("#silabo_destino_id").val(data.seccion.silabo.id);

                                    /////////////

                                    $("#silabo_exist").val('1')


                                    $('#SilaboCard').find('#section-no-document').css('display', 'none');
                                    $('#SilaboCard').find('#section-silabo').css('display', 'block');

                                    $('#id').val(data.seccion.silabo.id);
                                    $('.silabo_id').val(data.seccion.silabo.id);
                                    $('#fundamentacion').val(data.seccion.silabo.fundamentacion);
                                    $('#proyecto_institucional').val(data.seccion.silabo.proyecto_institucional);
                                    $('#bibliografia').val(data.seccion.silabo.bibliografia);

                                    $('#tipo').val(data.seccion.silabo.tipo);

                                    if (data.seccion.silabo.tipo == '2019') {
                                        $("input[value='2019']").click();
                                        $('#temas_transversales').val(data.seccion.silabo.temas_transversales);
                                    }

                                    if (data.seccion.silabo.tipo == '2020') {
                                        $("input[value='2020']").click();

                                        $('#enfoque_derecho_1').val(data.seccion.silabo.enfoque_derecho_1);
                                        $('#enfoque_derecho_2').val(data.seccion.silabo.enfoque_derecho_2);
                                        $('#enfoque_derecho_3').val(data.seccion.silabo.enfoque_derecho_3);
                                        $('#enfoque_intercultural_1').val(data.seccion.silabo.enfoque_intercultural_1);
                                        $('#enfoque_intercultural_2').val(data.seccion.silabo.enfoque_intercultural_2);
                                        $('#enfoque_intercultural_3').val(data.seccion.silabo.enfoque_intercultural_3);
                                        $('#competencias').val(data.seccion.silabo.competencias);
                                        $('#capacidades').val(data.seccion.silabo.capacidades);
                                        $('#estandar').val(data.seccion.silabo.estandar);
                                        $('#metodologia').val(data.seccion.silabo.metodologia);
                                    }
                                    if (data.seccion.silabo.tipo == 'archivo') {
                                        $("input[value='archivo']").click();
                                        $('#pre_view').prop('src','https://docs.google.com/viewer?url='+window.location.origin + data.seccion.silabo.url_silabo);
                                    }


                                    $('#aviso-unidades-seccion').css('display', 'none');
                                    $('#unidades-seccion').css('display', 'block');

                                    load_unidades_crud(data.seccion.silabo.id);

                                }

                                if (data.seccion.silabo.estado == 3) {
                                    $('#btn_send_check').attr('disabled', '');
                                    $('#badge-estado').removeClass('badge-warning');
                                    $('#badge-estado').removeClass('badge-info');
                                    $('#badge-estado').addClass('badge-success');
                                    $('#badge-estado').html('Publicado');

                                    $("#btn_google").css('display', 'block');

                                    $('#SilaboCard').find('#section-iframe').find('iframe').prop('src', "{{route('IntranetDocente.silabos.pdf')}}?id=" + data.seccion.silabo.id);
                                    $('#SilaboCard').find('#section-no-document').css('display', 'none');
                                    $('#SilaboCard').find('#section-iframe').css('display', 'block');
                                }

                                load_modal_content_lista_verificacion(data.seccion.silabo.id);

                            } else {
                                // console.log('sin silabo');
                                $("#silabo_exist").val('0')
                                $('#guardar_cambios').removeAttr('disabled')
                                $('#aviso-unidades-seccion').css('display', 'block');
                                $('#unidades-seccion').css('display', 'none');

                                $('#SilaboCard').find('#section-no-document').css('display', 'block');
                                $('#SilaboCard').find('#section-silabo').css('display', 'none');
                            }

                        }
                    });

                } else {
                    // $('#SilaboCard').css('display','none');
                    $('.seccion_id').val(0);
                    $('.silabo_id').val(0);
                    $('#InfoPanel').css('display', 'block');
                    $('#SilaboPanel1').css('display', 'none');
                    $('#SilaboPanel2').css('display', 'none');
                }
            });

            $('#nro_semana').change(function () {
                var id = $('#selectGrupo').val()
                var data = {'seccion_id': id};
                $.ajax({
                    type: "GET",
                    url: "/GetFullSeccion",
                    data: data,
                    dataType: 'json',
                    success: function (data) {

                        if (data.seccion.silabo.unidades.length > 0) {
                            var i = data.seccion.silabo.unidades.length - 1;
                            if (data.seccion.silabo.unidades[i].contenidos.length > 0) {
                                var ii = data.seccion.silabo.unidades[i].contenidos.length - 1;
                                var start = moment(data.seccion.silabo.unidades[i].contenidos[ii].fecha_fin)
                                // console.log(start)
                                var startdate = start.add('3', 'd')
                            } else {
                                var startdate = moment(data.seccion.silabo.unidades[0].fecha_inicio)
                            }
                        }
                        $('#fecha_inicio_semana').val(startdate.format('YYYY-MM-DD'))
                        $('#fecha_fin_semana').val(startdate.add('4', 'd').format('YYYY-MM-DD'))

                    }
                })
            })
            // $('#form_create_silabo').submit(function(event){
            // 	event.preventDefault();
            // 	var form_data = $(this).serialize();
            // 	var url = $(this).attr('action');

            // 	$.ajax({
            //         type: "POST",
            //         url: url,
            //         dataType:'json',
            //         data    : form_data,
            //         success: function(data){
            // 			// console.log(data);
            // 			$('#modal_create_silabo').modal('hide');
            // 			$('#SilaboCard').find('#section-silabo').find('iframe').prop('src',data.silabo.url_document);
            // 			$('#SilaboCard').find('#section-no-document').css('display','none');
            // 			$('#SilaboCard').find('#section-silabo').css('display','block');
            // 		}
            // 	});
            // });

            $('#form_unidad').submit(function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                var url = '';
                var action = $("#unidades_id").val() != '0' ? 1 : 0;

                if (action == 0) {
                    url = '/IntranetDocente/unidades/store';
                }

                if (action == 1) {
                    url = '/IntranetDocente/unidades/update';
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: form_data,
                    success: function (data) {
                        // console.log(data);

                        $('#modal_unidad').modal('hide');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id)
                        // procesar_tabla_unidades([unidad]);
                    }
                });
            });


            $('#form_desempeno_criterio').submit(function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                var url = '';
                var action = $("#desempeno_criterio_id").val() != '0' ? 1 : 0;

                if (action == 0) {
                    url = '/IntranetDocente/criterio-desempeno/store';
                }

                if (action == 1) {
                    url = '/IntranetDocente/criterio-desempeno/update';
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: form_data,
                    success: function (data) {
                        console.log(data);

                        $('#modal_desempeno_criterios').modal('hide');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id)
                        // procesar_tabla_unidades([unidad]);
                    }
                });
            });


            $('#form_semana').submit(function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                var url = '';
                var action = $("#semana_id").val() != '0' ? 1 : 0;

                if (action == 0) {
                    url = '/IntranetDocente/contenidos/store';
                }

                if (action == 1) {
                    url = '/IntranetDocente/contenidos/update';
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: form_data,
                    success: function (data) {
                        console.log(data);

                        $('#modal_contenidos').modal('hide');
                        var silabo_id = $('.silabo_id').first().val();
                        load_unidades_crud(silabo_id);
                    }
                });
            });


            $('#form-silabo').submit(function (event) {
                event.preventDefault();

                if ($("#tipo").val() === "archivo") {
                    var formData = new FormData($('#form-silabo')[0]);
                    formData.append('file', $('input[type=file]')[0].files[0]);
                }
                else {
                    var formData = new FormData($('#form-silabo')[0]);
                }
                var url = '';

                var action = $("#silabo_exist").val() == '1' ? 1 : 0;

                if (action == 0) {
                    url = '/IntranetDocente/silabos/store';
                }

                if (action == 1) {
                    url = '/IntranetDocente/silabos/update';
                }

                $("#section-silabo").block({
                    message: '<div class="loader"></div> <p><br />Guardando los cambios...</p>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        alert("Se han guardado los cambios.");
                        $("#section-silabo").unblock();
                        // console.log(data);

                        // $("#form_copiar").css('display','block');

                        $('.silabo_id').val(data.silabo.id);

                        // $('#aviso-unidades-seccion').css('display','none');
                        // $('#unidades-seccion').css('display','block');


                        // load_unidades_crud(data.silabo.id);

                        $('#selectGrupo').change();

                    }
                });
            });


            $('#form_copiar').submit(function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                var url = $(this).attr('action');

                var r = confirm('¿Está seguro de copiar la informacón al presente silabo?');

                if (r) {

                    $("#section-silabo").block({
                        message: '<div class="loader"></div> <p><br />Copiando información...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });


                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'json',
                        data: form_data,
                        success: function (data) {
                            alert("Se ha realizado la copia con éxito.");
                            $("#section-silabo").unblock();
                            console.log(data);

                            // $("#form_copiar").css('display','block');

                            $('.silabo_id').val(data.silabo.id);

                            // $('#aviso-unidades-seccion').css('display','none');
                            // $('#unidades-seccion').css('display','block');

                            $('#selectGrupo').change();


                            // load_unidades_crud(data.silabo.id);

                        }
                    });

                }
            });
        });

        function send_check(silabo_id) {
            $('#form-silabo').submit()

            var url = $('#url_cambiar_estado').val();
            var data = {'silabo_id': silabo_id, 'estado': "2"}
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data === 'success') {
                        alert('El sílabu fue enviado para revisar')
                    }
                }
            })
        }


    </script>


    <script async defer src="https://apis.google.com/js/api.js"
            onload="this.onload=function(){};handleClientLoad()"
            onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>


@endsection
