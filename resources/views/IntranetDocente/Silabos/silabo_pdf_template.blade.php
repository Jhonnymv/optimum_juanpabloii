<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Silabo</title>

    <style>

        ul {
            /* margin: 0;  */

            padding-left: 10%;
            padding-top: 0;
            padding-bottom: 0;
        }

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;
            margin-bottom: 10%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">

<table style="margin-top: -20px">
    <tr>
        <td style="width: 30%">
            <p style="text-align: center;font-weight: bold;font-size: 11px">MISIÓN INSTITUCIONAL</p>
            <p style="text-align: center;font-size: 10px">
                Formamos profesionales de la educación comprometidos con el desarrollo social mediante un modelo de
                mejora continua.
            </p>
        </td>
        <td style="text-align: center; width: 40%">
            <img src="{{public_path()}}/assets/img/logo_I.S.P.png" style="width: 7%;" alt="">
            <br>
            <p style="padding-bottom: -15px;font-size:11px;font-weight: bold">INSTITUTO DE EDUCACIÓN SUPERIOR
                PEDAGÓGICO</p>
            <p style="padding-bottom: -15px;font-size:11px;font-weight: bold">“HNO. VICTORINO ELORZ GOICOECHEA”</p>
            <p style="padding-bottom: -20px;font-size:11px;font-weight: bold">CAJAMARCA</p>
        </td>
        <td style="width: 30%">
            <p style="text-align: center;font-weight: bold;font-size: 11px">VISIÓN INSTITUCIONAL</p>
            <p style="text-align: center;font-size: 10px">
                Al año 2024, somos una Escuela de Educación Superior Pedagógica coprotagónica del desarrollo social que
                forma profesionales críticos, investigadores, innovadores y gestores del cambio.
            </p>
        </td>
    </tr>
</table>

<table>
    <tr>
        <td style="text-align: center;font-size: 12px;font-style: italic">Formando líderes para la educación del Perú
        </td>
    </tr>
</table>

<br><br>

<table>
    <tr>
        <td style="text-align: center;font-size: 18px;font-weight: bold">SÍLABO
            DE {{strtoupper($silabo->seccion->pe_curso->curso->nombre)}}</td>
    </tr>
</table>

<br>

<table>
    <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold" colspan="2">
            I. &nbsp;&nbsp;&nbsp; Datos Generales
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. PROGRAMA DE ESTUDIOS
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{$silabo->seccion->pe_curso->curso->carrera->nombre}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. ÁREA
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{$silabo->seccion->pe_curso->curso->nombre}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. SEMESTRE ACADÉMICO
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{$silabo->seccion->pe_curso->semestre}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. HORAS SEMANALES
        </td>
        <td style="text-align: left;font-size: 15px">
            @php
                $total_horas_semana= ($silabo->seccion->pe_curso->horas_teoria) + ($silabo->seccion->pe_curso->horas_practica) ;
            @endphp
            :&nbsp;&nbsp;{{($total_horas_semana<10?'0':'').$total_horas_semana}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. CRÉDITOS
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{($silabo->seccion->pe_curso->creditos<10?'0':'').$silabo->seccion->pe_curso->creditos}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. DURACIÓN
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;Inicio: {{$silabo->seccion->periodo->fecha_inicio}}
            &nbsp;&nbsp;&nbsp;&nbsp;Fin: {{$silabo->seccion->periodo->fecha_fin}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7. JEFE DE UNIDAD ACADÉMICA
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{$silabo->seccion->pe_curso->curso->carrera->responsablesCarrera[0]->persona->fullName()}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8. JEFE DEL ÁREA DE PRÁCTICA PRE
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROFESIONAL E INVESTIGACIÓN
        </td>
        <td style="text-align: left;font-size: 15px">
            :&nbsp;&nbsp;{{$area_practica_investigacion->docentes->first()->persona->fullName()}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 15px;width: 45%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9. DOCENTES / correo electrónico
        </td>
        <td style="text-align: left;font-size: 15px">
            @foreach ($silabo->seccion->docentes as $docente)
                &nbsp;&nbsp;&nbsp;{{$docente->persona->fullName()}} ({{$docente->persona->email}})
            @endforeach

        </td>
    </tr>

</table>

<br>
<br>

<table>
    <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold">
            @if ($silabo->tipo=='2019')
                II. &nbsp;&nbsp;&nbsp; Fundamentación
            @endif

            @if ($silabo->tipo=='2020')
                II. &nbsp;&nbsp;&nbsp; Sumilla
            @endif
        </td>
    </tr>
    <tr>
        <td style="text-align: justify;font-size: 15px">
            @php
                $fundamentacion=explode("\n",$silabo->fundamentacion);
            @endphp
            @foreach ($fundamentacion as $parrafo)
                @if (trim($parrafo)!='')
                    &nbsp;&nbsp;&nbsp;&nbsp;{{$parrafo}}
                    <br><br>
                @endif
            @endforeach

        </td>
    </tr>
</table>

<br>

<table>
    <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold">
            @if ($silabo->tipo=='2019')
                III. &nbsp;&nbsp;&nbsp; Proyecto institucional
            @endif

            @if ($silabo->tipo=='2020')
                III. &nbsp;&nbsp;&nbsp; Vinculación con el proyecto integrador
            @endif
        </td>
    </tr>
    <tr>
        <td style="text-align: justify;font-size: 15px">
            @php
                $proyecto_institucional=explode("\n",$silabo->proyecto_institucional);
            @endphp
            @foreach ($proyecto_institucional as $parrafo)
                @if (trim($parrafo)!='')
                    &nbsp;&nbsp;&nbsp;&nbsp;{{$parrafo}}
                    <br><br>
                @endif
            @endforeach

        </td>
    </tr>
</table>

<br>

{{--<div id="footer" class="page-break">--}}
{{--    <div class="page-number"></div>--}}
{{--</div>--}}

<br>


<table>
    {{-- <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold">

        </td>
    </tr> --}}
    <tr>
        <td style="text-align: justify;font-size: 15px;">
            <p style="font-size: 15px;font-weight: bold">IV. &nbsp;&nbsp;&nbsp; Temas transversales</p>
            @php
                $temas_transversales=explode("\n",$silabo->temas_transversales);
            @endphp
            @foreach ($temas_transversales as $parrafo)
                @if (trim($parrafo)!='')
                    &nbsp;&nbsp;&nbsp;&nbsp;{{$parrafo}}
                    <br><br>
                @endif
            @endforeach

        </td>
    </tr>
</table>


<br>

<table>
    <tr>
        <td style="text-align: left">
            <p style="font-size: 15px;font-weight: bold">V. &nbsp;&nbsp;&nbsp; Organización y evaluación de los
                aprendizajes</p>
        </td>
    </tr>
</table>
@foreach ($silabo->unidades->sortBy('unidad') as $unidad)
    <table class="contenidos" style="margin-top: 20px; page-break-inside: avoid">
        <tr>
            <td colspan="7" style="text-align: center;font-size: 12px;font-weight: bold">
                {{$unidad->unidad}} UNIDAD: "{{strtoupper($unidad->denominacion)}}"
                ({{$unidad->contenidos->count()}} semanas) Del {{$unidad->fecha_inicio}}
                al {{$unidad->fecha_fin}}
            </td>
        </tr>
        <tr style="background-color: #FFFF00">
            <td colspan="4" style="text-align: center">
                EVALUACIÓN
            </td>
            <td colspan="3" style="text-align: center">
                ORGANIZACIÓN DE LOS APRENDIZAJES
            </td>
        </tr>
        <tr style="background-color: #FFFF00">
            <td style="text-align: center; font-size: 10px">
                Criterio de desempeño
            </td>
            <td style="text-align: center; font-size: 10px">
                Indicadores
            </td>
            <td style="text-align: center; font-size: 10px">
                Instrumentos
            </td>
            <td style="text-align: center; font-size: 10px">
                Productos o evidencias
            </td>
            <td style="text-align: center; font-size: 10px">
                Conocimientos
            </td>
            <td style="text-align: center; font-size: 10px">
                Estrategias
            </td>
            <td style="text-align: center; font-size: 10px">
                Semanas
            </td>
        </tr>
        @foreach ($unidad->contenidos->sortBy('nro_semana') as $loop => $semana)
            <tr>
                @if ($loop->index==0)
                    <td rowspan="{{$unidad->contenidos->count()}}"
                        style="text-align: left;font-size: 10px;width: 10%">
                        <br>
                        @foreach ($unidad->desempeno_criterios as $criterio)
                            <span style="text-decoration: underline">{{$criterio->titulo}}</span><br>
                            {{$criterio->descripcion}}
                            <br><br>
                        @endforeach
                    </td>
                @endif
                <td style="text-align: left;font-size: 10px;width: 20%">
                    @php
                        $indicadores_desempeno=explode("\n",$semana->indicadores_desempeno);
                    @endphp
                    <ul>
                        @foreach ($indicadores_desempeno as $parrafo)
                            @if (trim($parrafo)!='')
                                <li>{{$parrafo}}</li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td style="text-align: left;font-size: 10px;width: 10%">
                    @php
                        $instrumentos=explode("\n",$semana->instrumentos);
                    @endphp
                    <ul>
                        @foreach ($instrumentos as $parrafo)
                            @if (trim($parrafo)!='')
                                <li>{{$parrafo}}</li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td style="text-align: left;font-size: 10px;width: 10%">
                    @php
                        $producto_evidencias=explode("\n",$semana->producto_evidencias);
                    @endphp
                    <ul>
                        @foreach ($producto_evidencias as $parrafo)
                            @if (trim($parrafo)!='')
                                <li>{{$parrafo}}</li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td style="text-align: left;font-size: 10px;width: 24%">
                    @php
                        $conocimientos=explode("\n",$semana->conocimientos);
                    @endphp
                    <ul>
                        @foreach ($conocimientos as $parrafo)
                            @if (trim($parrafo)!='')
                                <li>{{$parrafo}}</li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td style="text-align: left;font-size: 10px;width: 24%">
                    @php
                        $estrategias=explode("\n",$semana->estrategias);
                    @endphp
                    <ul>
                        @foreach ($estrategias as $parrafo)
                            @if (trim($parrafo)!='')
                                <li>{{$parrafo}}</li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td style="text-align: center;font-size: 10px;width: 2%">
                    {{$semana->nro_semana}} S <br>
                    {{$semana->fecha_inicio}} al
                    {{$semana->fecha_fin}}
                </td>
            </tr>
        @endforeach
    </table>
@endforeach

<br>


{{-- // aqui estaba --}}


<table>
    <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold">
            VI. &nbsp;&nbsp;&nbsp; Requisitos de aprobación del área
        </td>
    </tr>
</table>

<table>
    <tr>
        <td style="text-align: left;font-size: 15px">
            <ul>
                <li>
                    Nota mínima aprobatoria es 11.
                </li>
                <li>
                    Si el estudiante FALTA por 20 días consecutivos, será retirado y perderá su
                    condición de estudiante.
                </li>
                <li>
                    Los instrumentos y porcentajes de evaluación según SIGES es:
                </li>
            </ul>
        </td>
    </tr>
</table>

@foreach  ($silabo->seccion->productos as $producto)
    @if($producto->count()<0)
        <p style="color:#FF0000">ADVERTENCIA: Es necesario registrar su sistema de evaluación</p>
    @endif
@endforeach
{{-- <table class="contenidos" style="margin-left: -5%"> --}}
<table class="contenidos">
    <tr>

        @php
            $cont=0;
        @endphp
        @foreach  ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 1)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}

                    @endif
                @endforeach
            @endif
        @endforeach


        <td colspan={{$cont}} style="text-align: center;font-size: 12px;font-weight: bold;
        ">
        PRODUCTOS DE PROCESOS 25%
        </td>
        {{-- @php
        $cont=0;
        @endphp
        @foreach ($silabo->seccion->productos as $producto)
        @if ($producto->evaluacion_categoria_id == 2)
            {{$cont++}}
        @endif
        @endforeach --}}

        @php
            $cont=0;
        @endphp
        @foreach  ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
            @endif

        @endforeach
        <td colspan={{$cont}} style="text-align: center;font-size: 12px;font-weight: bold;
        ">
        AUTOEVALUACIÓN Y COEVALUACIÓN 15%
        </td>
        @php
            $cont=0;
        @endphp
        @foreach  ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
            @endif
        @endforeach
        <td colspan={{$cont}} style="text-align: center;font-size: 12px;font-weight: bold;
        ">
        PRODUCTO FINAL 35%
        </td>

        @php
            $cont=0;
        @endphp
        @foreach  ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
            @endif
        @endforeach
        <td colspan={{$cont}} style="text-align: center;font-size: 12px;font-weight: bold;
        ">
        PORTAFOLIO 25%
        </td>
    </tr>

    <tr>

        @php
            $cont=0;
        @endphp
        @foreach  ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
                {{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
                <td style="font-size: 10px; text-align: center" colspan={{$cont}} >
                    {{$producto->producto}}
                </td>
            @endif
            {{$cont=0}}
        @endforeach

        @php
            $cont=0;
        @endphp
        {{-- @foreach (['Producto 1','Producto 2'] as $producto) --}}
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
                <td style="font-size: 10px; text-align: center" colspan={{$cont}} >
                    {{$producto->producto}}
                </td>
            @endif
            {{$cont=0}}
        @endforeach
        @php
            $cont=0;
        @endphp
        {{-- @foreach (['Producto 1'] as $producto) --}}
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
                <td style="font-size: 10px; text-align: center" colspan={{$cont}} >
                    {{$producto->producto}}
                </td>
            @endif
            {{$cont=0}}
        @endforeach

        @php
            $cont=0;
        @endphp
        {{-- @foreach (['Producto 1'] as $producto) --}}
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                        {{$cont++}}
                    @endif
                @endforeach
                <td style="font-size: 10px; text-align: center" colspan={{$cont}}>
                    {{$producto->producto}}
                </td>
            @endif
            {{$cont=0}}
        @endforeach

    </tr>

    <tr>
        {{-- @foreach (['Instrumento 1','Instrumento 2','Instrumento 3'] as $instrumento)
            <td style="font-size: 10px">
                <p class="rotate">{{$instrumento}}</p>
            </td>
        @endforeach --}}
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if($p_i->estado ==1 )
                        <td style="font-size: 10px; text-align: center">
                            {{$p_i->instrumentos->instrumento}}
                        </td>
                    @endif
                @endforeach
            @endif
        @endforeach
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if($p_i->estado ==1 )
                        <td style="font-size: 10px; text-align: center">
                            {{$p_i->instrumentos->instrumento}}
                        </td>
                    @endif
                @endforeach
            @endif
        @endforeach
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if($p_i->estado ==1 )
                        <td style="font-size: 10px; text-align: center">
                            {{$p_i->instrumentos->instrumento}}
                        </td>
                    @endif
                @endforeach
            @endif
        @endforeach
        @foreach ($silabo->seccion->productos as $producto)
            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                @foreach  ($producto->producto_instrumentos as $p_i)
                    @if($p_i->estado ==1 )
                        <td style="font-size: 10px; text-align: center">
                            {{$p_i->instrumentos->instrumento}}
                        </td>
                    @endif
                @endforeach
            @endif
        @endforeach
    </tr>
</table>


<br>


<br>

{{-- <table>
    <tr>
        <td style="text-align: left;font-size: 15px;font-weight: bold">
            VII. &nbsp;&nbsp;&nbsp; REFERENCIA BIBLIOGRÁFICA (APA)
        </td>
    </tr>
</table> --}}


<table>
    <tr>
        <td style="text-align: justify">
            <p style="font-size: 20px;font-weight: bold">VII. &nbsp;&nbsp;&nbsp; REFERENCIA BIBLIOGRÁFICA (APA)</p>
            @php
                $bibliografia=explode("\n",$silabo->bibliografia);
            @endphp
            @foreach ($bibliografia as $parrafo)
                @if (trim($parrafo)!='')
                    &nbsp;&nbsp;&nbsp;&nbsp;<p style="font-size: 20px">{{$parrafo}}</p>
                    {{-- <br><br> --}}
                @endif
            @endforeach
        </td>
    </tr>
</table>

</body>
</html>
