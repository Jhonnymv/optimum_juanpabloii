<div class="form-group">
    <button type="button" id="btn_unidad" style="float: left;    margin-right: 1%;" data-toggle="modal" data-target="#modal_unidad" class="btn bg-teal-400 btn-labeled btn-labeled-left">
        <b><i class="icon-reading"></i></b> Registrar unidad</button>
</div>


<br>
<br>
<br>


<ul class="nav nav-tabs nav-tabs-solid border-0">

    @foreach ($silabo->unidades->sortBy('unidad') as $key => $unidad)
    <li class="nav-item">
        <a href="#solid-tab{{$key}}" class="nav-link {{$key==0?'active':''}}" data-toggle="tab">Unidad {{$unidad->unidad}}</a>
    </li>
    @endforeach
</ul>

<div class="tab-content">

    @foreach ($silabo->unidades->sortBy('unidad') as $key => $unidad)
    <div class="tab-pane fade {{$key==0?'show active':''}}" id="solid-tab{{$key}}">
		<div class="row">

            {{-- <div class="form-group row"> --}}
                <label class="col-md-1 col-form-label" style="font-weight: bold;">Unidad Nro:</label>
                <div class="col-md-1">
                    <div class="form-control-plaintext">{{$unidad->unidad}}</div>
                </div>

                <label class="col-md-2 col-form-label" style="font-weight: bold;">Denominacion:</label>
                <div class="col-md-2">
                    <div class="form-control-plaintext">{{$unidad->denominacion}}</div>
                </div>



                <label class="col-md-1 col-form-label" style="font-weight: bold;">Fecha inicio:</label>
                <div class="col-md-1">
                    <div class="form-control-plaintext">{{$unidad->fecha_inicio}}</div>
                </div>

                <label class="col-md-1 col-form-label" style="font-weight: bold;">Fecha Fin:</label>
                <div class="col-md-1">
                    <div class="form-control-plaintext">{{$unidad->fecha_fin}}</div>
                </div>

                <div class="col-md-1">
                    <a href="javascript:void(0)" onclick="open_modal_unidades_edit({{$unidad->id}})"> <i class="icon-pencil7"></i> Editar </a><br>
                    <a href="javascript:void(0)" onclick="delete_unidad({{$unidad->id}})" style="color: #c10a2c"> <i class="icon-trash"></i> Eliminar </a>
                </div>
            {{-- </div> --}}



        </div>

        <br>
        <br>

        <div class="row">
            <div class="col-md-12">
                <a class="btn bg-primary" style="float: left;" onclick="open_modal_desempeno_criterios({{$unidad->id}})"><i class="icon-plus2"></i> Estándares de desempeño</a>
                <a class="btn bg-primary" style="float: right;" onclick="open_modal_contenidos({{$unidad->id}})"><i class="icon-plus2"></i> semana</a>
            </div>

            <div class="col-md-4">
                <div id="desempeno-criterios-seccion-{{$key}}" class="table-responsive">
                    <table class="table table-hover">
                        <thead>
{{--                            <th>Titulo</th>--}}
                            <th>Descripción</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @if ($unidad->desempeno_criterios->isNotEmpty())

                            @foreach ($unidad->desempeno_criterios->sortBy('id') as $key_dc => $desempeno_criterio)
                            <tr>
{{--                                <td>--}}
{{--                                    {{$desempeno_criterio->titulo}}--}}
{{--                                </td>--}}
                                <td>
                                    {{$desempeno_criterio->descripcion}}
                                </td>
                                <td>
{{--                                    <a href="javascript:void(0)" onclick="open_modal_desempeno_criterio_edit({{$desempeno_criterio->id}})"> <i class="icon-pencil7"></i> </a> |--}}
                                    <a href="javascript:void(0)" onclick="delete_desempeno_criterio({{$desempeno_criterio->id}})" style="color: #c10a2c"> <i class="icon-trash"></i> </a>
                                </td>
                            </tr>
                            @endforeach


                        @else

                            <tr>
                                <td colspan="2">
                                    <p style="font-size: 0.9em;">AUN NO SE HAN REGISTRADO CRITERIOS DE DESEMPEÑO.</p>
                                </td>
                            </tr>

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-1">
            </div>

            <div class="col-md-7">
                <div id="contenidos-seccion-{{$key}}" class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Semana</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($unidad->contenidos->isNotEmpty())

                                @foreach ($unidad->contenidos->sortBy('id')  as $key_c => $semana)
                                <tr>
                                    <td>
                                        {{$semana->nro_semana}}
                                    </td>
                                    <td>
                                        {{$semana->fecha_inicio}}
                                    </td>
                                    <td>
                                        {{$semana->fecha_fin}}
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="open_modal_contenidos_edit({{$semana->id}})"> <i class="icon-pencil7"></i> </a> |
                                        <a href="javascript:void(0)" onclick="delete_semana({{$semana->id}})" style="color: #c10a2c"> <i class="icon-trash"></i> </a>
                                    </td>
                                </tr>
                                @endforeach


                            @else

                                <tr>
                                    <td colspan="4">
                                        <p style="margin-left: 35%;">AUN NO SE HAN REGISTRADO SEMANAS.</p>
                                    </td>
                                </tr>

                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
    @endforeach

</div>
