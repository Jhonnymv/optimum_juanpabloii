<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

/* .td-v{
    width: 3px !important
} */

        .rotate{
            /* writing-mode: vertical-lr; */
                    /* -ms-writing-mode: tb-rl; */
                    white-space: nowrap;
                    transform: rotate(270deg);
                    margin-right: -100%;
                    margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }
        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }
        .page-number{
            margin-top: 10px;
            font-weight: bold
        }
        .page-number:before {
            content: "" counter(page);
        }

        .contenidos{
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr{
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td{
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
              /* border: 2px solid black; */
            }

            table{
                /* border: 2px solid black; */
                width: 100%

            }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">

    <table style="margin-top: -20px">
        <tr>
            <td style="text-align: center; width: 15%">
                <img src="{{asset('assets/img/escudo_raimondi.png')}}" style="width: 50%;padding-bottom: -25px" alt="">
            </td>
            <td style="text-align: center; width: 70%">
                <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCACIÓN SUPERIOR PEDAGÓGICO “HNO. VICTORINO ELORZ GOICOECHEA”</p>
                <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE ADMISIÓN  Y REGISTRO ACADÉMICO</p>
                <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">ESPECIALIDAD DE {{strtoupper($curso->pe_curso->curso->carrera->nombre)}}</p>
                <br>
                <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">REPORTE DEL REGISTRO DE ASISTENCIAS</p>
            </td>

        </tr>
    </table>

    <span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>

    <table>
        <tr>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}
            </td>
        </tr>
        <tr>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CURSO
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{$curso->pe_curso->curso->nombre}}
            </td>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCION
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{$curso->seccion}}
            </td>
        </tr>
        <tr>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOCENTE
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{$docente->paterno}} {{$docente->materno}} {{$docente->nombres}}
            </td>

        </tr>
        <tr>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{ $registroAsistencia != '[]'? $registroAsistencia[0]->fecha_sesion:'' }}
            </td>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HORARIO
            </td>
            <td style="text-align: left;font-size: 11px; font-family:arial;">
                :&nbsp;&nbsp;&nbsp;{{ $horario->dia}}   {{ $horario->hora_inicio}} -   {{ $horario->hora_fin}} ({{ $horario->turno}})
            </td>
        </tr>

    </table>

    <span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>

    <br><br>
    <div class="row mt-0 font-size-sm">
    <table class="table table-sm table-striped">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial;">Nº</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial;">CÓDIGO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 10%; font-family:arial;">ALUMNO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 10%; font-family:arial;">ASISTENCIA</th>
        </tr>
        </thead>

        <tbody>
        @foreach($registroAsistencia as $asistencia)
            <tr>

                <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;"> {{$loop->index + 1}}</td>
                <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;">{{$asistencia->alumnos->persona->DNI}}</td>
                <td style="text-align: left;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$asistencia->alumnos->persona->paterno}} {{$asistencia->alumnos->persona->materno}} {{$asistencia->alumnos->persona->nombres}}</td>
                <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{ $asistencia->asistencia == 1? 'A':'F' }}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
    <br>





    <div id="footer" class="page-break">
        <div  class="page-number"></div>
    </div>






</body>
</html>
