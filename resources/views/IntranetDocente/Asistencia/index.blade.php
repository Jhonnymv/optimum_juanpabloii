@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Asistencia')

@section('header_title')
    {{-- <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4> --}}

    @include('IntranetDocente.vistasParciales.filtroPeriodo')

@endsection


@section('filtroSeccion')



@endsection


@section('header_buttons')
    <style>

        .table-hover tbody tr:hover {
            background-color: rgba(90, 119, 129, 0.7);
            color: rgb(10, 10, 10);
        }

        input[type=checkbox] {
            display: none;
        }

        input[type=checkbox] + label {
            cursor: pointer;
        }

        .asistencia:before {
            content: "";
            background: transparent;
            border: 2.3px solid #88c64b;
            text-shadow: 4px -2px 3px gray;
            border-radius: 5px;
            display: inline-block;
            height: 20px;
            margin-right: 20px;
            text-align: center;
            vertical-align: middle;
            width: 20px;
        }

        input[type=checkbox]:checked + label:before {
            content: '✓';
            font-size: 14px;
            color: #88c64b;
            font-family: "Times New Roman";
        }


    </style>
    <div class="d-flex justify-content-center">

    </div>
@endsection


@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Aula</a>
    <span class="breadcrumb-item active">Asistencia</span>
@endsection
@section('header_subbuttons')
    <div class="breadcrumb justify-content-center">
        <a href="#" class="breadcrumb-elements-item">
            <i class="icon-comment-discussion mr-2"></i>
            Support
        </a>

        <div class="breadcrumb-elements-item dropdown p-0">
            <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                <i class="icon-gear mr-2"></i>
                Settings
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
            </div>
        </div>
    </div>
@endsection



@section('content')
    <input type="hidden" id="url_report" value="{{route('asistencia.reporteSesion.pdf')}}">
    <div class="row">

        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{route('asistencia.store')}}" method="post" id="formStoreAsistencia">
                @csrf
                <input type="hidden" id="seccion_id" name="seccion_id" value="0">
                <input type="hidden" id="contador" name="contador" value="0">


                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Asistencia</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" id="tabOpciones">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#paso1" id="paso1-tab" class="nav-link active"
                                                    data-toggle="tab">PASO 1 - Seleccionar Fecha de la Sesión de
                                    Clase</a></li>
                            <li class="nav-item" id="paso2-tab-container"><a href="#paso2" id="paso2-tab"
                                                                             class="nav-link" data-toggle="tab">PASO 2 -
                                    Registrar Asistencia</a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="paso1">
                                <div id="sesionesAcademicas" class="table-responsive">
                                    <table id="tbSesionClase" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nro</th>
                                            <th>Fecha</th>
                                            <th>Día</th>
                                            <th>Turno</th>
                                            <th>Hora</th>
                                            <th>Aula</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodySesionClase">
                                        <tr>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="paso2">
                                <div class="container">
                                    <div class="form-group row border border-primary col-md-7">
                                        <input type="hidden" id="horario_id_store" name="horario_id" value="0">
                                        <input type="hidden" id="fecha_sesion" name="fecha_sesion" value="0">
                                        <input type="hidden" id="hora_inicioSt" name="hora_inicio" value="0">
                                        <input type="hidden" id="hora_finSt" name="hora_fin" value="0">
                                        <input type="hidden" id="diaSt" name="hora_fin" value="0">
                                        <input type="hidden" id="turnoSt" name="hora_fin" value="0">
                                        <input type="hidden" id="aulaSt" name="hora_fin" value="0">


                                        <label class="col-lg-3 col-form-1 bg-primary-400">Fecha Sesión</label>
                                        <label class="col-lg-2 col-form-1 bg-primary-400">Día</label>
                                        <label class="col-lg-2 col-form-1 bg-primary-400">Turno</label>
                                        <label class="col-lg-3 col-form-1 bg-primary-400">Hora</label>
                                        <label class="col-lg-2 col-form-1 bg-primary-400">Aula</label>
                                        {{--  <div class="col-lg-2"> </div>    --}}
                                        <label id="fechaAs" class="col-lg-3 col-form-1 bg-green-400"></label>
                                        <label id="diaAs" class="col-lg-2 col-form-1 "></label>
                                        <label id="turnoAs" class="col-lg-2 col-form-1 "></label>
                                        <label id="horaAs" class="col-lg-3 col-form-1 "></label>
                                        <label id="aulaAs" class="col-lg-2 col-form-1 "></label>
                                        {{--  <div class="col-lg-4"> </div>  --}}

                                    </div>
                                </div>
                                <div>
                                    <br>
                                    <button id="btnGenerar" href="#" type="button" onclick=""
                                            class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                            style="font-size:8pt">
                                        <b><i class="icon-add mr-1 icon-1x"></i>
                                        </b> Lista de Alumnos
                                    </button>

                                    <button id="btnGuardar" href="#" type="submit"
                                            class="btn bg-blue-400 btn-labeled btn-labeled-left" size="10"
                                            style="font-size:8pt; display:none;">
                                        <b><i class="icon-task mr-1 icon-1x"></i>
                                        </b> Guardar
                                    </button>

                                    <button id="btnEliminar" href="#" type="button"
                                            class="btn bg-orange-400 btn-labeled btn-labeled-left" size="10"
                                            style="font-size:8pt" data-toggle="modal" data-target="#modalCrear">
                                        <b><i class="icon-close2  mr-1 icon-1x"></i>
                                        </b>Eliminar
                                    </button>

                                    <a id="btnReporte" target="_blank"
                                       size="10"
                                       class="btn btn-success"
                                       style="font-size:8pt">
                                        <b><i class="icon-copy  mr-1 icon-1x"></i> Reporte </b> </a>


                                </div>
                                <div id="sesionesAcademicas" class="table-responsive">
                                    <table id="tbAsistencia" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nro</th>
                                            <th>Codigo</th>
                                            <th>Alumno</th>
                                            <th>Asistencia</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyAsistencia">

                                        </tbody>
                                    </table>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>


    </div>


    <!-- Danger modal Asistencia -->
    <div id="modalEliminarAsistencia" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEliminarAsistencia" class="modal-content" action="/asistencia/remove/" method="POST">

                @method('DELETE');

                <!--Importante-->
            @csrf
            <!--Importante-->


                <div class="modal-header bg-danger">
                    <h6 class="modal-title">Desea Eliminar el registro de asistencias?</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-danger">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
    <!-- default modal danger Asistencia-->
@endsection

@section('script')
    <script>

        document.addEventListener('DOMContentLoaded', function () {

            $('.form-control-select2').select2();

            $('#selectPeriodo').change(function () {
                var id = $(this).val();
                irAsistencia('', '', '', '', '', '', '');
                // console.log(id);


                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};

                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/CarrerasDocentePorPeriodo",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);
                            $("#selectCarrera").html('');
                            $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#tbSesionClase tr").remove();

                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectCarrera").append(new Option(data[i].nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {
                    $("#selectCarrera").html('');
                    $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione un grupo', ''));

                    $("#tbSesionClase tr").remove();
                }


            });


            $('#selectCarrera').change(function () {
                var id = $(this).val()
                irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/AreasDocentePorCarrera",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#tbSesionClase tr").remove();
                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectArea").append(new Option(data[i].curso.nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));

                    $("#tbSesionClase tr").remove();
                }


            });


            $('#selectArea').change(function () {
                var id = $(this).val()
                irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/SeccionesDocentePorArea",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            //console.log(data);
                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                            $("#tbSesionClase tr").remove();
                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectGrupo").append(new Option(data[i].seccion, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                        }
                    });

                } else {

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                    $("#tbSesionClase tr").remove();
                }


            });

            $('#selectGrupo').change(function () {

                var id = $(this).val()
                $('#seccion_id').val(id)

                irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    var seccion_id = $('#selectGrupo').val();
                    var periodo_id = $('#selectPeriodo').val();

                    var data = {'seccion_id': seccion_id, 'periodo_id': periodo_id};

                    $.ajax({
                        type: "GET",
                        url: "getSesionClase",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            $('#tbodySesionClase').html('')
                            $('#contador').val(0)

                            data.forEach(element => {
                                var nro = parseInt($('#contador').val()) + 1;
                                $('#contador').val(nro);

                                if (element.estadoAsistencia == "false") {
                                    var estadoAsistencia = "<button id='btnAsistencia-" + element.fecha + "' href='#'  onclick='irAsistencia(" + element.horario_id + ", \"" + element.fecha + "\" , \"" + element.dia + "\" , \"" + element.turno + "\" , \"" + element.hora_inicio + "\" , \"" + element.hora_fin + "\" , \"" + element.aula + "\"  )' type='button' class='btn bg-blue-400 btn-labeled btn-labeled-left' size='6' style='font-size:7pt' ><b><i class='icon-checkmark-circle2 mr-1 icon-0.1x'  ></i></b>Registrar</button>"
                                } else {
                                    var estadoAsistencia = "<button id='btnAsistencia-" + element.fecha + "' href='#'  onclick='irAsistencia(" + element.horario_id + ", \"" + element.fecha + "\" , \"" + element.dia + "\" , \"" + element.turno + "\" , \"" + element.hora_inicio + "\" , \"" + element.hora_fin + "\" , \"" + element.aula + "\"  )' type='button' class='btn bg-green-400 btn-labeled btn-labeled-left' size='6' style='font-size:7pt' ><b><i class='icon-eye8 mr-0.1 icon-0.1x'  ></i></b>Visualizar</button>"

                                }
                                var template = "<tr>"
                                    + "<input type='hidden' id='horario_id' value='" + element.horario_id + "' name='horario_id[]'>"
                                    + "<td>" + nro + "</td>"
                                    + "<td>" + element.fecha + "</td>"
                                    + "<td>" + element.dia + "</td>"
                                    + "<td>" + element.turno + "</td>"
                                    + "<td>" + element.hora_inicio + " - " + element.hora_fin + "</td>"
                                    + "<td>" + element.aula + "</td>"

                                    + "<td> " + estadoAsistencia + " </td>"
                                    // + "<td>  <input type='radio' name='rbtSesion' id='rbtSesion' onclick='irAsistencia(" + element.horario_id + ", \"" + element.fecha + "\" , \"" + element.dia + "\" , \"" + element.turno + "\" , \"" + element.hora_inicio + "\" , \"" + element.hora_fin + "\" , \"" + element.aula + "\"  )' /></td>"
                                    + "</tr>"
                                $('#tbodySesionClase').append(template)

                            })
                        }
                    });

                } else {

                    $("#tbSesionClase tr").remove();

                }


            });


        });


    </script>


    <script>
        $(function () {
            $('#btnGenerar').click(function () {
                if ($('#selectGrupo').val() > 0 && ($('#tbAsistencia >tbody >tr').length) <= 0
                    && ($('#horario_id').val().length) > 0) {
                    generarListaAsistencia();
                    $('#btnGuardar').attr('style', 'font-size:8pt; display:inline;');

                    // alert($('#tbNotas').DataTable().data().count());
                } else {
                    alert('- Seleccionar Fecha de la Sesión de Clase \n'
                        + '- Registro ya existe')
                }
            });
        });


        $('#formStoreAsistencia').submit(function (e) {
            var $checkboxes = $('#formStoreAsistencia').find('input[type="checkbox"]');


            $checkboxes.filter(':checked').val(1);
            $checkboxes.not(':checked').val(0);
            $checkboxes.prop('checked', true);


            {{--  $(this).find('input[type=checkbox]').each(function() {
               var checkbox = $(this);
                if ( !checkbox.prop('checked') ) {
                    checkbox.clone() .prop('type', 'hidden') .val(0) .insertBefore(checkbox);
                     }

              });  --}}
            e.preventDefault();
            var data = $(this).serialize()
            var url = $(this).attr('action')

            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    $horario_id = $('#horario_id').val()
                    $fecha = $('#fecha_sesion').val()
                    $hora_inicio = $('#hora_inicioSt').val()
                    $hora_fin = $('#hora_finSt').val()
                    $dia = $('#diaSt').val()
                    $turno = $('#turnoSt').val()
                    $aula = $('#aulaSt').val()

                    irAsistencia($horario_id, $fecha, $dia, $turno, $hora_inicio, $hora_fin, $aula)
                    //$('#slcInstrumento').change()
                    $('#btnAsistencia-' + $fecha).removeClass('bg-blue-400').addClass('bg-green-400')
                }
            })
        });
        $('#tbSesionClase tbody tr td').on('click', function () {
            var id = $(this).find('td:first').html();
            alert(id)
            //$('#txtNombre').val(dato);
        });
        {{--  $('#tbSesionClase tbody tr td').click(function (event) {
            if (event.target.type !== 'radio') {
                //$(':radio', this).trigger('click');
                alert('Radiobtn')
            }
            alert('fff')
        });  --}}
        $('#btnEliminar').click(function () {
            if ($('#horario_id').val() > 0 && ($('#tbAsistencia >tbody >tr').length) > 0) {
                $('#modalEliminarAsistencia').modal('show')
            } else {
            }

        });
        $('#formEliminarAsistencia').submit(function (e) {
            var seccion_id = $('#selectGrupo').val()
            var horario_id = $('#horario_id').val()
            $("#formEliminarAsistencia").attr('action', 'asistencia/remove/' + seccion_id + '/' + horario_id);
            e.preventDefault();
            var data = $(this).serialize()
            var url = $(this).attr('action')

            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {

                    $('#modalEliminarAsistencia').modal('hide')
                    // $('#slcInstrumento').change();

                    $horario_id = $('#horario_id').val()
                    $fecha = $('#fecha_sesion').val()
                    $hora_inicio = $('#hora_inicioSt').val()
                    $hora_fin = $('#hora_finSt').val()
                    $dia = $('#diaSt').val()
                    $turno = $('#turnoSt').val()
                    $aula = $('#aulaSt').val()

                    irAsistencia($horario_id, $fecha, $dia, $turno, $hora_inicio, $hora_fin, $aula)

                },
                error: function () {
                    console.log("ERROR");
                }
            })
        });
    </script>
    <script>
        function generarListaAsistencia() {
            var seccion_id = $('#selectGrupo').val()
            var data = {'seccion_id': seccion_id}

            $.ajax({
                url: 'getListaAsistencia',
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#tbodyAsistencia').html('')
                    $('#contador').val(0)

                    data.listaAlumnos.forEach(element => {
                        var nro = parseInt($('#contador').val()) + 1;
                        $('#contador').val(nro);

                        var template = "<tr>"
                            + "<input type='hidden' id='alumno_id' value='" + element.matricula.alumnocarrera.alumno.id + "' name='alumno_id[]'>"
                            + "<td>" + nro + "</td>"
                            + "<td>" + element.matricula.alumnocarrera.alumno.persona.DNI + "</td>"
                            + "<td>" + element.matricula.alumnocarrera.alumno.persona.paterno + " " +
                            element.matricula.alumnocarrera.alumno.persona.materno + " " +
                            element.matricula.alumnocarrera.alumno.persona.nombres + "</td>"
                            + "<td> <input type='checkbox' id='asistencia" + nro + "' name='asistencia[]' value='1' checked><label  for='asistencia" + nro + "' class='asistencia'></label></td>"
                            + "</tr>"
                        $('#tbodyAsistencia').append(template)
                    })
                    // console.log(data.listaAlumnos)
                    //$('#t_notas').DataTable()
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }

        function irAsistencia(horario_id, fecha, dia, turno, hora_inicio, hora_fin, aula) {

            $('#horario_id_store').val(horario_id)
            $('#fecha_sesion').val(fecha)
            $('#horario_id').val(horario_id)

            $('#hora_inicioSt').val(hora_inicio)
            $('#hora_finSt').val(hora_fin)
            $('#diaSt').val(dia)
            $('#turnoSt').val(turno)
            $('#aulaSt').val(aula)

            $('#fechaAs').html('')
            $('#fechaAs').append(fecha)

            $('#diaAs').html('')
            $('#diaAs').append(dia)

            $('#turnoAs').html('')
            $('#turnoAs').append(turno)

            $('#horaAs').html('')
            $('#horaAs').append(hora_inicio + ' - ' + hora_fin)

            $('#aulaAs').html('')
            $('#aulaAs').append(aula)

            if (horario_id != '') {
                $("#paso2-tab-container").show();
                $("#paso2-tab").tab("show");
            }

            $('#btnGuardar').attr('style', 'font-size:8pt; display:none;');


            //alert(fecha+" dia: "+dia)
            getRegistroAsistencia();

            var seccion_id = $('#selectGrupo').val();
            var horario_id = $('#horario_id').val()
            var fecha = $('#fecha_sesion').val()

            var url_pdf = $('#url_report').val() + '/' + seccion_id + '/' + horario_id + '/' + fecha

            $('#btnReporte').attr('href', url_pdf)

        }

        function getRegistroAsistencia() {
            var seccion_id = $('#selectGrupo').val()
            var horario_id = $('#horario_id').val()
            var fecha_sesion = $('#fecha_sesion').val()

            var data = {'seccion_id': seccion_id, 'horario_id': horario_id, 'fecha_sesion': fecha_sesion}

            $.ajax({
                url: 'getRegistroAsistencia',
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#tbodyAsistencia').html('')
                    $('#contador').val(0)

                    data.registroAsistencia.forEach(element => {
                        var nro = parseInt($('#contador').val()) + 1;
                        $('#contador').val(nro);

                        if (element.asistencia == 1) {
                            var asistencia = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>A</font></font></span>"
                        } else {
                            var asistencia = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>F</font></font></span>"

                        }
                        var template = "<tr>"
                            + "<input type='hidden' id='alumno_id' value='" + element.alumnos.id + "' name='alumno_id[]'>"
                            + "<td>" + nro + "</td>"
                            + "<td>" + element.alumnos.persona.DNI + "</td>"
                            + "<td>" + element.alumnos.persona.paterno + " " +
                            element.alumnos.persona.materno + " " +
                            element.alumnos.persona.nombres + "</td>"
                            + "<td>" + asistencia + "</td>"
                            + "</tr>"
                        $('#tbodyAsistencia').append(template)
                    })
                    //console.log(data.registroAsistencia)
                    //$('#t_notas').DataTable()
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }
    </script>
@endsection
