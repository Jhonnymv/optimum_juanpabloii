@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Curricula')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Intranet Docente</span> - Curricula</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo C</span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Intranet Docente</span>
<span class="breadcrumb-item active">Curricula</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Curricula del Curso</h5>
                <div class="header-elements">

                </div>
            </div>

            <div class="card-body">
                Visalizacion de la Curricula
            </div>

        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection


@section('modals')




@endsection


@section('script')
<script>

</script>

@endsection
