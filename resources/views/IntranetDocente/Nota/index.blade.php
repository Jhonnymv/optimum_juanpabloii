@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Notas')

@section('header_title')
    {{-- <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4> --}}

    @include('IntranetDocente.vistasParciales.filtroPeriodo')



@endsection


@section('filtroSeccion')



@endsection


@section('header_buttons')
    <style>
        .table-hover tbody tr:hover {
            background-color: rgba(90, 119, 129, 0.7);
            color: rgb(10, 10, 10);
        }
    </style>
    <div class="d-flex justify-content-center">

    </div>
@endsection


@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Aula</a>
    <span class="breadcrumb-item active">Registro de Notas</span>
@endsection
@section('header_subbuttons')
    <div class="breadcrumb justify-content-center">
        <a href="#" class="breadcrumb-elements-item">
            <i class="icon-comment-discussion mr-2"></i>
            Support
        </a>

        <div class="breadcrumb-elements-item dropdown p-0">
            <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                <i class="icon-gear mr-2"></i>
                Settings
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
            </div>
        </div>
    </div>
@endsection



@section('content')
    <div class="row align-items-start">

        <input type="hidden" id="url_listaNotas" name="url_listaNotas" value="{{route('url_getlistaNotas')}}">
        <input type="hidden" id="url_getevaluacionesxseccion" name="url_getevaluacionesxseccion"
               value="{{route('url_getevaluacionesxseccion')}}">
{{--          <input type="hidden" id="url_getcriteriosxevaluacion" name="url_getcriteriosxevaluacion" value="{{route('url_getcriteriosxevaluacion')}}">--}}
        <input type="hidden" id="evaluacion_id" name="evaluacion_id" value="0">
        <input type="hidden" id="url_listaAlumnosNota" name="url_listaAlumnosNota"
              value="{{route('url_getlistaAlumnosNota')}}">

        <input type="hidden" id="seccion_id" name="seccion_id" value="0">
        <input type="hidden" id="cred_id" name="cred_id" value="0">
        <input type="hidden" id="url_report" value="{{route('notas.reporteNotasInstrumneto.pdf')}}">

        <div class="col-md-6" class="row align-items-start">

            <!-- Basic layout-->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Seleccione el bimestre a evaluar</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">

                    <div class="container">
                        <div class="form-group row border border-primary col-md-12">

                            <label class="col-lg-3 col-form-label">Bimestres:</label>
                            <div class="col-lg-8">
                                <select id="slcCategoria" class="form-control">
                                    <option>Seleccionar bimestre</option>
                                </select>
                            </div>
                            <div class="col-lg-1"></div>

{{--                            <div class="col-lg-6">--}}
{{--                                <br>--}}
{{--                                <h7 class="text-info" id='lblInfoCategoria'>Mínimo de 2 Productos</h7>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>

                <div class="card-body">

                    <div class="container">
                        <div class="form-group row border border-secondary col-md-12">
{{--                            <label class="col-lg-3 col-form-label">Producto:</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                <select id="slcProducto" class="form-control">--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-4"></div>--}}

                        </div>
                    </div>

                </div>
            </div>


            <!-- /basic layout -->

        </div>


        <div class="col-md-6" class="row align-items-end">

{{--            <!-- Basic layout-->--}}


            <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Instrumentos</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="reload"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">

                       <div class="container">
                         <div class="form-group row border border-secondary col-md-12">

{{--                             <label class="col-lg-3 col-form-label">Producto:</label>--}}
{{--                             <div class="col-lg-8">--}}
{{--                                 <select id="slcProducto" class="form-control">--}}
{{--                                 </select>--}}
{{--                             </div>--}}
{{--                             <div class="col-lg-4"> </div>--}}

                         </div>
                     </div>

                    <div class="container">
                        <div class="form-group row border border-secondary col-md-12">

                            <label class="col-lg-3 col-form-label">Instrumento:</label>
                            <div class="col-lg-8">
                                <select id="slcInstrumento" class="form-control">
                                </select>
                            </div>
                            <div class="col-lg-1"></div>


{{--                            <br>--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <div class="form-group row">--}}

{{--                                    <label class="col-lg-3 col-form-4 bg-primary">Fecha Aplicación:</label>--}}
{{--                                    <label class="col-lg-7 col-form-1 text-success" id='lblInfoInstrumento'></label>--}}
{{--                                    <label class="col-lg-3 col-form-1 ">Contenido:</label>--}}
{{--                                    <textarea class="col-lg-8 col-form-1 text-success" name="lblInfoContenido"--}}
{{--                                              id="lblInfoContenido" rows="5" cols="12" disabled="false"--}}
{{--                                              style="background:rgb(52, 60, 84)"></textarea>--}}
{{--                                      <label class="col-lg-7 col-form-1 text-success" id='lblInfoContenido'></label>--}}
{{--                                    <label class="col-lg-10 col-form-1 "></label>--}}

{{--                                    <label class="col-lg-3 col-form-1 ">Fechas:</label>--}}
{{--                                    <label class="col-lg-7 col-form-1 text-success " id='lblInfoContFechas'></label>--}}

{{--                                </div>--}}
{{--                            </div>--}}
                        </div>

                    </div>

                </div>
            </div>
            <!-- /basic layout -->

        </div>


    </div>

    <!-- Basic layout-->
    <form action="{{route('notas.store')}}" method="post" id="form_store_notas">
        <input type="hidden" id="instrumento_id" name="instrumento_id" value="0">
        <input type="hidden" id="seccion" name="seccion" value="0">
        @csrf

        <input type="hidden" id="prodins_id" name="prodins_id" value="0">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Registro de Notas</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="reload"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div>
                    <button id="btnGenerar" href="#" type="button" onclick=""
                            class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                            style="font-size:8pt; ">
                        <b><i class="icon-add mr-1 icon-1x"></i>
                        </b> Generar
                    </button>

                    <button id="btnGuardar" href="#" type="submit"
                            class="btn bg-blue-400 btn-labeled btn-labeled-left " size="10"
                            style="font-size:8pt; display:none;">
                        <b><i class="icon-task  mr-1 icon-1x"></i>
                        </b> Guardar
                    </button>

                    <button id="btnEliminarNotas" href="#" type="button" onclick=""
                            class="btn bg-orange-400 btn-labeled btn-labeled-left" size="10"
                            style="font-size:8pt;">
                        <b><i class="icon-close2 mr-1 icon-1x"></i>
                        </b> Eliminar
                    </button>

                    <a id="btnReporte" target="_blank"
                       size="10"
                       class="btn btn-success"
                       style="font-size:8pt">
                        <b><i class="icon-copy  mr-1 icon-1x" ></i> Reporte</b></a>

                </div>
                <div id="sesionesAcademicas" class="table-responsive">
                    <table id="tbNotas" class="table table-hover" id="t_notas">
                        <thead>
                        <tr>
                            <th>Nro</th>
                            <th>Código</th>
                            <th>Apellidos y Nombres</th>
                            <th>Nota</th>
                        </tr>
                        </thead>
                        <tbody id="t_body_notas">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </form>
    <!-- /basic layout -->

    </div>



    </div>


    <!-- Danger modal Notas -->
    <div id="modalEliminarNotas" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEliminarNotas" class="modal-content" action="/notas/remove/" method="POST">

                @method('DELETE');

                <!--Importante-->
            @csrf
            <!--Importante-->


                <div class="modal-header bg-danger">
                    <h6 class="modal-title">Desea Eliminar el registro de notas?</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-danger">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
    <!-- default modal danger Notas-->

@endsection

@section('script')
    <script>

        document.addEventListener('DOMContentLoaded', function () {

            $('.form-control-select2').select2();

            $('#selectPeriodo').change(function () {
                var id = $(this).val();

                // console.log(id);


                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};

                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/CarrerasDocentePorPeriodo",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);
                            $("#selectCarrera").html('');
                            $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#slcCategoria").html('');
                            $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                            $("#slcProducto").html('');
                            $("#slcProducto").append(new Option('Seleccione un area', '0'));

                            $("#slcInstrumento").html('');
                            $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                            // $("#slcInstrumento").change();

                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectCarrera").append(new Option(data[i].nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {
                    $("#selectCarrera").html('');
                    $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione un grupo', ''));

                    $("#slcCategoria").html('');
                    $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                    $("#slcProducto").html('');
                    $("#slcProducto").append(new Option('Seleccione un area', '0'));

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                   // $("#slcInstrumento").change();
                }


            });


            $('#selectCarrera').change(function () {
                var id = $(this).val()


                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/AreasDocentePorCarrera",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#slcCategoria").html('');
                            $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                            $("#slcProducto").html('');
                            $("#slcProducto").append(new Option('Seleccione un area', '0'));

                            // $("#slcInstrumento").html('');
                            // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                            //$("#slcInstrumento").change();

                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectArea").append(new Option(data[i].curso.nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));

                    $("#slcCategoria").html('');
                    $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                    $("#slcProducto").html('');
                    $("#slcProducto").append(new Option('Seleccione un area', '0'));

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                  //  $("#slcInstrumento").change();
                }


            });


            $('#selectArea').change(function () {
                var id = $(this).val()


                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/SeccionesDocentePorArea",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);
                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione una seccion', ''));

                            $("#slcCategoria").html('');
                            $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                            $("#slcProducto").html('');
                            $("#slcProducto").append(new Option('Seleccione un area', '0'));

                            // $("#slcInstrumento").html('');
                            // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                         //   $("#slcInstrumento").change();

                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectGrupo").append(new Option(data[i].seccion, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {


                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));

                    $("#slcCategoria").html('');
                    $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                    $("#slcProducto").html('');
                    $("#slcProducto").append(new Option('Seleccione un area', '0'));

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                   // $("#slcInstrumento").change();
                }


            });

            $('#selectGrupo').change(function () {
                var id = $(this).val();

                $('#seccion_id').val(id);
                $('#seccion_id2').val(id);

                if (id != "") {

                    $("#slcCategoria").html('');
                    $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                    $("#slcProducto").html('');
                    $("#slcProducto").append(new Option('Seleccione un area', '0'));

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                    //$("#slcInstrumento").change();

                    @forelse ($categorias as $item)
                        @if($item->siges_id =='0')
                            {
                            $('#slcCategoria').append("<option value={{$item->id}} disabled> {{$item->categoria}}</option>");}
                        @else
                            $('#slcCategoria').append("<option value={{$item->id}} > {{$item->categoria}}</option>");
                        @endif
                    @empty
                    @endforelse

                } else {

                    $("#slcCategoria").html('');
                    $("#slcCategoria").append(new Option('Seleccione un bimestre', '0'));

                    $("#slcProducto").html('');
                    $("#slcProducto").append(new Option('Seleccione un area', '0'));

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));

                    //$("#slcInstrumento").change();


                }


            });

            // $('#slcCategoria').change(function () {
            //     var id = $(this).val()
            //
            //
            //     if (id != "") {
            //         // $('#categoria_id').val(id)
            //         // $('#categoria_id2').val(id)
            //
            //         $("#slcProducto").html('');
            //         $("#slcProducto").append(new Option('Seleccione un area', '0'));
            //
            //         $("#slcInstrumento").html('');
            //         $("#slcInstrumento").append(new Option('Seleccione un area', '0'));
            //         $("#slcInstrumento").change();
            //
            //         var data = {'categoria_id': id, 'seccion_id': $('#seccion_id').val()}
            //
            //         $.ajax({
            //             url: 'getProductosCategoria',
            //             type: 'GET',
            //             data: data,
            //             datatype: 'json',
            //             success: function (data) {
            //                 //console.log(data.productoscategoria)
            //
            //                 $('#lblInfoCategoria').html('')
            //                 data.categoria.forEach(element => {
            //                     $('#lblInfoCategoria').append("Mínimo " + element.min_prod + " Productos <br>"
            //                         + "Peso: " + element.peso + " % <br>"
            //                         + "ID SIGES: " + element.siges_id + "<br>")
            //                 })
            //
            //                 $('#slcProducto').html('')
            //                 $('#slcProducto').append(new Option('Seleccionar produto', '0'))
            //
            //                 data.productoscategoria.forEach(element => {
            //                     $('#slcProducto').append("<option value=" + element.id + ">" + element.producto + "</option>");
            //
            //                 })
            //             },
            //             error: function () {
            //                 console.log("ERROR");
            //             }
            //         });
            //     } else {
            //         // $('#categoria_id').val(0)
            //         // $('#categoria_id2').val(0)
            //
            //         $("#slcProducto").html('');
            //         $("#slcProducto").append(new Option('Seleccione un area', '0'));
            //
            //         $("#slcInstrumento").html('');
            //         $("#slcInstrumento").append(new Option('Seleccione un area', '0'));
            //         $("#slcInstrumento").change();
            //
            //     }
            //
            //
            // });
            //

            $('#slcCategoria').change(function () {
                var id = $('#seccion_id').val()

                console.log(id);
                if (id != "") {
                    // $('#producto_id').val(id)
                    $("#slcInstrumento").html('');
                    $("#slcInstrumento").append(new Option('Seleccione un competencia', '0'));
                    //$("#slcInstrumento").change();

                    var data = {'producto_id': id};
                    $.ajax({
                        type: "GET",
                        url: 'getInstrumentosProducto',
                        data: data,
                        datatype: 'json',
                        success: function (data) {
                            //console.log(data)
                            $('#slcInstrumento').html('')
                            $('#slcInstrumento').append(new Option('Seleccionar una competencia', '0'))

                            data.prodInstrumento.forEach(element => {
                                $('#slcInstrumento').append("<option value=" + element.id + ">" + element.nombre + "</option>");

                            })

                            //console.log(data)
                        },
                        error: function () {
                            console.log("ERROR");
                        }
                    });

                } else {
                    $('#producto_id').val(0)

                    // $("#slcInstrumento").html('');
                    // $("#slcInstrumento").append(new Option('Seleccione un area', '0'));
                    // $("#slcInstrumento").change();

                }
            });


            $('#slcInstrumento').change(function () {
                //var id = $(this).val()
                var id =  $('#slcInstrumento').val()
                $('#instrumento_id').val(id)

                var seccion_id = $('#seccion_id').val()
                var sec_id = $('#seccion_id').val(id)
                //console.log( $('#slcCategoria').val());
                $('#prodins_id').val(id)
                var slcCategoria =$('#slcCategoria').val();
                var url = $('#url_listaNotas').val()
                var data = {'categoria_id': id, 'sec_id': seccion_id, 'bimestre':slcCategoria}
                //console.log(seccion_id);
                $('#btnGuardar').attr('style', 'font-size:8pt; display:none;');


                if (id != "" && seccion_id !="") {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: data,
                        datatype: 'json',
                        success: function (data) {
                            console.log(data);
                            $('#contador').val(0)
                            if(data.sw==1) {
                                $('#prodins_id').val(data.prod_inst_id);
                                $('#t_body_notas').html('')
                                var contador = 0;
                                data.listanotasalum.forEach(element => {
                                     var nro = contador+1;
                                    contador=nro;
                                    // if (element.nota <= 9)
                                    //     notaString = '0' + notaString;

                                    var template = "<tr>"
                                        + "<input type='hidden' id='alumno_id' value='" + element.alumno.id + "' name='alumno_id[]'>"
                                        + "<td>" + nro +"</td>"
                                        + "<td>" + element.alumno.persona.DNI + "</td>"
                                        + "<td>" + element.alumno.persona.paterno + " " +
                                        element.alumno.persona.materno + " " +
                                        element.alumno.persona.nombres + "</td>"
                                        + "<td> " + element.nota + "</td>"
                                        + "</tr>"
                                    $('#t_body_notas').append(template)
                                    //$('#slcCategoria').change();
                                })

                                //console.log(data.listanotasalum)
                                //$('#tbNotas').DataTable()
                                var seccion_id = $('#selectGrupo').val();
                                var prod_inst_id = id;
                                var inst_id = $('#inst_id').val();
                                var periodo_id = $('#selectPeriodo').val();
                                var carrera_id = $('#selectCarrera').val();
                                var area_id = $('#selectArea').val();
                                var cat_id = $('#slcCategoria').val();
                                var prod_id = $('#slcProducto').val();

                                // var url_pdf = $('#url_report').val() + '/' + seccion_id + '/' + prod_inst_id + '/'
                                //     + periodo_id + '/' + carrera_id + '/' + area_id + '/' + cat_id + '/' + prod_id
                                var url_pdf = $('#url_report').val() + '/' + seccion_id + '/' + prod_inst_id + '/'
                                    + periodo_id + '/' + carrera_id + '/' + area_id + '/' + cat_id
                                $('#btnReporte').attr('href', url_pdf)
                                $('#btnGenerar').hide();

                            }
                            else{
                                $('#btnGenerar').show();
                                $('#t_body_notas').html('')


                                    var template = "<label>Aún no se registra notas para este bimestre...</label>"
                                    $('#t_body_notas').append(template)
                                //$('#slcCategoria').change();
                            }
                        },
                        error: function () {
                            console.log("ERROR");
                        }

                    });

            //         // var id=$(this).val()
            //         // if (id != 0) {
            //         //     var data = {'id': id}
            //         //     $.ajax({
            //         //         type: "GET",
            //         //         url: 'traerprodinstrumento',
            //         //         data: data,
            //         //         success: function (data) {
            //         //             $('#lblInfoInstrumento').html('')
            //         //             $('#lblInfoInstrumento').append("" + data['prodinstrumento']['fecha'] + "")
            //         //
            //         //             var contenido = '', fechasCont = '';
            //         //             if (data['prodinstrumento']['contenido_id'] != null &&
            //         //                 data['prodinstrumento']['contenido_id'] != 0) {
            //         //
            //         //                 contenido = data['prodinstrumento']['contenidos']['conocimientos'];
            //         //                 fechasCont = data['prodinstrumento']['contenidos']['fecha_inicio']
            //         //                     + " / " + data['prodinstrumento']['contenidos']['fecha_fin'];
            //         //
            //         //             }
            //         //
            //         //             $('#lblInfoContenido').html('')
            //         //             $('#lblInfoContenido').append("" + contenido + "")
            //         //
            //         //             $('#lblInfoContFechas').html('')
            //         //             $('#lblInfoContFechas').append("" + fechasCont + "")
            //         //
            //         //
            //         //             //console.log(data);
            //         //         },
            //         //         error: function () {
            //         //             console.log("ERROR");
            //         //         }
            //         //     });
            //         // } else {
            //         //     $('#lblInfoInstrumento').html('')
            //         //     $('#lblInfoContenido').html('')
            //         //     $('#lblInfoContFechas').html('')
            //         //
            //         // }
                } else {
                    $('#categoria_id').val(0)


                }


            });


        });
    </script>

    <script>
        //console.log("getnotas");

        function getNotas(criterio_id) {
            $('#criterio_id').val(criterio_id);
            //$('#inst_id').val(inst_id);
            //console.log('#criterio_id');
            var url = $('#url_listaNotas').val()
            var data = {'criterio_id': 7}

            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#btnGenerar').hide();
                    $('#t_body_notas').html('')
                    data.listanotasalum.forEach(element => {
                        var template = "<tr>"
                            + "<input type='hidden' id='alumno_id' value='" + element.alumno.id + "' name='alumno_id[]'>"
                            + "<td>" + "1</td>"
                            + "<td>" + element.alumno.persona.DNI + "</td>"
                            + "<td>" + element.alumno.persona.paterno + " " +
                            element.alumno.persona.materno + " " +
                            element.alumno.persona.nombres + "</td>"
                            + "<td> " + element.nota + "</td>"
                            + "</tr>"
                        $('#t_body_notas').append(template)
                        $('#slcInstrumento').change();
                    })
                    // console.log(data.listanotasalum)
                    //$('#t_notas').DataTable()
                },
                error: function () {
                    console.log("ERROR");
                }
            });

        }

        function generarLista() {
            var seccion_id = $('#selectGrupo').val()
            var categoria_id = $('#slcCategoria').val()
            var instr_id = $('#slcInstrumento').val()
            var url = $('#url_listaAlumnosNota').val()
            var data = {'id': seccion_id,'cat_id': categoria_id, 'instr_id': instr_id}
            //console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    console.log(data);
                    $('#t_body_notas').html('')
                    var grade = data.grado
                    if(grade<4){
                    data.listaalumnosnotas.forEach(element => {
                        var template = "<tr>"
                            + "<input type='hidden' id='seccion_id' value='" + seccion_id+ "' name='seccion_id'>"
                            + "<input type='hidden' id='cat_id' value='" + categoria_id+ "' name='cat_id'>"
                            + "<input type='hidden' id='inst_id' value='" + instr_id+ "' name='inst_id'>"
                            + "<input type='hidden' id='alumno_id' value='" + element.alumno.id + "' name='alumno_id[]'>"
                            + "<td>" + "1</td>"
                            + "<td>" + element.DNI + "</td>"
                            + "<td>" + element.paterno + " " +
                            element.materno + " " +
                            element.nombres + "</td>"
                            +  "<td> <select id='nota' class='form-control nota' name='nota[]' required>"+
                                "<option>Seleccionar Nota</option>"+
                                "<option value='A'>A</option>"+
                                "<option value='B'>B</option>"+
                                "<option value='AD'>AD</option>"+
                                "<option value='NP'>No se presentó</option>"+
                                "</select></td> "
                            + "</tr>"
                        $('#t_body_notas').append(template)
                    })}
                    else{
                        data.listaalumnosnotas.forEach(element => {
                            var template = "<tr>"
                                + "<input type='hidden' id='seccion_id' value='" + seccion_id+ "' name='seccion_id'>"
                                + "<input type='hidden' id='cat_id' value='" + categoria_id+ "' name='cat_id'>"
                                + "<input type='hidden' id='inst_id' value='" + instr_id+ "' name='inst_id'>"
                                + "<input type='hidden' id='alumno_id' value='" + element.alumno.id + "' name='alumno_id[]'>"
                                + "<td>" + "1</td>"
                                + "<td>" + element.DNI + "</td>"
                                + "<td>" + element.paterno + " " +
                                element.materno + " " +
                                element.nombres + "</td>"
                                // + "<td> <input type='number' id='nota' name='nota[]' class='form-control nota' min='10' step='1' max='20' required value='10'> </td>"
                            +"<td> <select id='nota' class='form-control nota' name='nota[]' required>"+
                            "<option>Seleccionar Nota</option>"+
                            "<option value='10'>10</option>"+
                            "<option value='11'>11</option>"+
                            "<option value='12'>12</option>"+
                                "<option value='13'>13</option>"+
                                "<option value='14'>14</option>"+
                                "<option value='15'>15</option>"+
                                "<option value='16'>16</option>"+
                                "<option value='17'>17</option>"+
                                "<option value='18'>18</option>"+
                                "<option value='19'>19</option>"+
                                "<option value='20'>20</option>"+
                            "<option value='NP'>No se presentó</option>"+
                            "</select></td> "
                            + "</tr>"
                            $('#t_body_notas').append(template)
                        })
                    }
                    // console.log(data.listaalumnosnotas)
                    //$('#t_notas').DataTable()
                    $('#btnGenerar').hide();

                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }


        // function removeNotas() {
        //     var prodins_id = $('#prodins_id').val()
        //     //var url=$('#url_listaAlumnosNota').val()
        //     var data = {'id': prodins_id}
        //     console.log(data);
        //     //var data=$(this).serialize()
        //
        //     $.ajax({
        //         url: 'notas/remove/' + prodins_id,
        //         type: 'GET',
        //         data: data,
        //         datatype: 'json',
        //         success: function (data) {
        //
        //             //$('#t_notas').DataTable()
        //             console.log(data);
        //         },
        //         error: function () {
        //             console.log("ERROR");
        //         }
        //     });
        // }

    </script>

    <script>
        $(function () {
            $('#btnGenerar').click(function () {
                if ($('#slcInstrumento').val() > 0 && ($('#tbNotas >tbody >tr').length) <= 0) {
                    generarLista();
                    $('#btnGuardar').attr('style', 'font-size:8pt; display:inline;');
                    $('#btnGenerar').hide();

                     alert('Lista generada...');
                } else {
                   alert('> Seleccionar un Bimestre \n'
                       + '> Registro ya existe')
              }
            });


        });


        $('#form_store_notas').submit(function (e) {
            e.preventDefault();
            var data = $(this).serialize()
            var url = $(this).attr('action')
            console.log(data)
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    //console.log(data)
                    if(data.respuesta=='exito'){
                        alert("Registro guardado correctamente");
                        $('#prodins_id').val(data.prod_inst_id);
                        $('#slcInstrumento').change();
                        //getNotas(data.prod_inst_id);
                    }
                    else{
                        //console.log(data)
                        alert("El registro ya existe")}
                    {{--  var template = "<tr> "
                        +"<td>"+data.producto.id+ "</td>"
                        + "<td>" + data.producto.producto + "</td>"
                        + "<td>" + data.producto.estado + "</td>"
                        + "<td>"
                           +"<a href='#' class='btn btn-warning' size='4' onclick='editarProducto("+ data.producto.id +")'><i class='fas fa-pen '></i></a>"
                           +" <a href='#' class='btn btn-danger' onclick='eliminarProducto("+ data.producto.id  +")'><i class='fas fa-trash '></i></a>"
                         +"</td>"
                        + "</tr>"

            $('#tbody_producto').append(template)
            $('#modalCrearProducto').modal('hide')  --}}

                    $('#slcInstrumento').change()

                }
            })
        });
    </script>

    <script>
        $(function () {

            $('#formEliminarNotas').submit(function (e) {
                var id = $('#prodins_id').val()
                $("#formEliminarNotas").attr('action', 'notas/remove/' + id);
                e.preventDefault();
                var data = $(this).serialize()
                var url = $(this).attr('action')

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        //$('#slcCategoria').change();
                        console.log(data)
                        $('#modalEliminarNotas').modal('hide')
                        $('#slcInstrumento').change();

                    },
                    error: function () {
                        console.log("ERROR");
                    }
                })
            });


            $('#btnEliminarNotas').click(function () {
                if ($('#slcInstrumento').val() > 0 && ($('#tbNotas >tbody >tr').length) > 0) {
                    $('#modalEliminarNotas').modal('show')
                } else {
                }

            });


        });


    </script>
@endsection
