@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Sistema Evaluación')

@section('header_title')
{{-- <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4> --}}

@include('IntranetDocente.vistasParciales.filtroPeriodo')



@endsection


@section('filtroSeccion')



@endsection





@section('header_subtitle')
<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Aula</a>
<span class="breadcrumb-item active">Sistema de Evaluaciones</span>
@endsection
@section('header_subbuttons')
<div class="breadcrumb justify-content-center">
    <a href="#" class="breadcrumb-elements-item">
        <i class="icon-comment-discussion mr-2"></i>
        Support
    </a>

    <div class="breadcrumb-elements-item dropdown p-0">
        <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
            <i class="icon-gear mr-2"></i>
            Settings
        </a>

        <div class="dropdown-menu dropdown-menu-right">
            <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
            <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
            <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
        </div>
    </div>
</div>
@endsection



@section('content')
<div class="row">

    <div class="col-md-12">
        
        <input type="hidden" id="categoria_id" name="categoria_id" value="0">
        <input type="hidden" id="seccion_id" name="seccion_id" value="0">

        <!-- Basic layout-->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Sistema de Evaluación</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            
            <div class="container">
            <div class="form-group row border border-primary col-md-12">  
                           
                <label class="col-lg-1 col-form-label">Categorias:</label>
                <div class="col-lg-5">
                    <select id="slcCategoria" class="form-control">
                        <option>Seleccionar categoria</option>                                                   
                    </select>
                </div>                      
                <div class="col-lg-4"> </div>
                
                <div class="col-lg-5">  
                 <br>   
                <h7 class="text-info" id='lblInfoCategoria'>Registrar un mínimo de 2 Productos</h7>
                </div>
            </div>
        </div>

            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="#paso1" class="nav-link active" data-toggle="tab">Productos</a></li>
                    <li class="nav-item"><a href="#paso2" class="nav-link" data-toggle="tab">Instrumentos</a></li>

                </ul>

                

                <div class="tab-content">                    
                       
                    <div class="tab-pane fade show active" id="paso1">
                        <div class="col-lg-2"> 
                            <button id='btnNuevoProducto' href="#" type="button" onclick=""
                                class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                style="font-size:8pt" >
                                <b><i class="icon-add mr-1 icon-1x"  ></i>
                            </b> Nuevo</button>                                    
             
                        </div>
                                               
                        <br>
                        <div id="sesionesAcademicas" class="table-responsive">
                            <table id="tProducto" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Producto</th>
                                        <th>Estado</th>
                                        <th></th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="tbody_producto">                                   
                                </tbody>
                            </table>
                        </div>
                    
                    </div>
                   

                    <div class="tab-pane fade" id="paso2">
                        <div class="container">
                            <div class="form-group row border border-secondary col-md-12">  
                                           
                                <label class="col-lg-1 col-form-label">Productos:</label>
                                <div class="col-lg-5">
                                    <select id="slcProducto" class="form-control">                                                                
                                    </select>
                                </div>                      
                                <div class="col-lg-4"> </div>

                                <div> 
                                    <button href="#" id="btnNuevoInstrumento" type="button" onclick=""
                                        class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                        style="font-size:8pt" >
                                        <b><i class="icon-add mr-1 icon-1x"  ></i>
                                    </b> Nuevo</button>                                    
                     
                                </div>
                                
                                <div class="col-lg-5">  
                                 <br>   
                                <h7 class="text-info" id='lblInfoProducto'></h7>
                                </div>
                            </div>
                           
                        </div>
                        
                        
                        <div id="sesionesAcademicas" class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Instrumento</th>
                                        <th>Estado</th>
                                        <th>Fecha Aplicación</th>
                                        <th>Contenidos</th>
                                        <th>Fechas Contenidos</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_Instrumento">                                                                    
                                </tbody>
                            </table>
                        </div>


                    </div>


                </div>
            </div>
        </div>
        <!-- /basic layout -->

    </div>


</div>
@endsection


@section('modals')

<!-- crear modal Producto-->
<div id="modalCrearProducto" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('productos.store')}}" id="formProductoStore">            
            <!--Importante-->
            @csrf
            <!--Importante-->

            <input type="hidden" id="seccion_id2" name="seccion_id2" value="0">
            <input type="hidden" id="categoria_id2" name="categoria_id2" value="0">

            <div class="modal-header">
                <h5 class="modal-title">Registrar Producto</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">                              

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Producto:</label>
                    <div class="col-lg-9">
                        <input type="text" name="producto" class="form-control" placeholder="" required>
                    </div>
                </div>            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal Producto -->


<!-- modal Editar Producto -->
<div id="modalEditarProducto" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditarProducto" class="modal-content" method="POST" action="/productos">

            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Producto</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">                              

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Producto:</label>
                    <div class="col-lg-9">
                        <input type="text" name="productoEdit" id="productoEdit" class="form-control" placeholder="" required>
                    </div>
                </div>            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /modal Editar Producto -->


<!-- Danger modal Producto -->
<div id="modalEliminarProducto" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminarProducto" class="modal-content" action="/productos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Producto?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Producto</button>
            </div>
        </form>
    </div>
</div>
<!-- default modal danger Producto-->


<!-- crear modal Instrumento-->
<div id="modalCrearInstrumento" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formInstrumentoStore" class="modal-content" method="POST" action="{{route('produtoInstrumento.store')}}">            
            <!--Importante-->
            @csrf
            <!--Importante-->

            <input type="hidden" id="producto_id" name="producto_id" value="0">            

            <div class="modal-body">                              

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Instrumento:</label>
                <div class="col-lg-9">
                    <select id="slcInstrumento" name='slcInstrumento' class="form-control" required>
                        <option>Seleccionar instrumento</option>  
                        @forelse ($instrumento as $item)
                        <option value="{{$item->id}}">
                            {{$item->instrumento}}
                        </option>
                        @empty

                        @endforelse                          
                    </select>                   
                </div> 
                </div>  
                
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Fecha de Aplicación:</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="date" id="fecha" name="fecha" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Unidades:</label>
                <div class="col-lg-9">
                    <select id="slcUnidad" name='slcUnidad' class="form-control form-control-select2"
                    data-placeholder="Seleccionar una unidad" required>  
                    <option value="">Seleccione una unidad</option>                                               
                    </select>                   
                </div> 
                </div>
                
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Fechas:</label>
                    <div class="col-lg-9">
                        <label id="lblFechasUnidad" class="col-lg-8 col-form-label text-success"></label>
                    </div>
                </div>
                

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Contenidos:</label>
                <div class="col-lg-9">
                    <select id="slcContenido" name='slcContenido' class="form-control form-control-select2"
                    data-placeholder="Seleccionar un contenido" required>
                    <option value="">Seleccione un contenido</option>   
                                                                          
                    </select>                   
                </div> 
                </div>
                
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Fechas:</label>
                    <div class="col-lg-9">
                        <label id="lblFechasContenido" class="col-lg-8 col-form-label text-success"></label>
                    </div>
                </div>


                <div>
                <br><br><br><br><br>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal Instrumento -->

<!-- modal Editar Instrumento -->
<div id="modalEditarInstrumento" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditarInstrumento" class="modal-content" method="POST" action="/proinstrumento">

            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Instrumento</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">                              

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Instrumento:</label>
                <div class="col-lg-9">
                    <select id="instrumentoEdit" name='instrumentoEdit' class="form-control">
                        <option>Seleccionar instrumento</option>  
                        @forelse ($instrumento as $item)
                        <option value="{{$item->id}}">
                            {{$item->instrumento}}
                        </option>
                        @empty

                        @endforelse                          
                    </select>
                </div> 
                </div>  
                
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Fecha de Aplicación:</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="date" id="fechaEdit" name="fechaEdit">
                    </div>
                </div>

            


            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Unidades:</label>
            <div class="col-lg-9">
                <select id="slcUnidadEdit" name='slcUnidadEdit' class="form-control form-control-select2"
                data-placeholder="Seleccionar una unidad" required>  
                <option value="">Seleccione una unidad</option>                                               
                </select>                   
            </div> 
            </div>
            
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Fechas:</label>
                <div class="col-lg-9">
                    <label id="lblFechasUnidadEdit" class="col-lg-8 col-form-label text-success"></label>
                </div>
            </div>
            

            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Contenidos:</label>
            <div class="col-lg-9">
                <select id="slcContenidoEdit" name='slcContenidoEdit' class="form-control form-control-select2"
                data-placeholder="Seleccionar un contenido" required>
                <option value="">Seleccionar un contenido</option>   
                                                                      
                </select>                   
            </div> 
            </div>
            
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Fechas:</label>
                <div class="col-lg-9">
                    <label id="lblFechasContenidoEdit" class="col-lg-8 col-form-label text-success"></label>
                </div>
            </div>

        
            <div>
            <br><br><br><br><br>
            </div>


        </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>

        
        </form>
    </div>
</div>
<!-- /modal Editar Instrumento -->


<!-- Danger modal Instrumento -->
<div id="modalEliminarInstrumento" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminarInstrumento" class="modal-content" action="" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Instrumento?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Instrumento</button>
            </div>
        </form>
    </div>
</div>
<!-- default modal danger Instrumento-->

@endsection





@section('script')
<script>
  
    $(function(){
        $('#formProductoStore').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){
                    var template = "<tr> "
                        +"<td>"+data.producto.id+ "</td>"
                        + "<td>" + data.producto.producto + "</td>"
                        + "<td>" + data.producto.estado + "</td>"                       
                        + "<td>" 

                            
                           +"<a href='#' class='btn btn-warning bg-secondary' size='4' onclick='editarProducto("+ data.producto.id +")'><i class='fas fa-pen '></i></a>"
                           +" <a href='#' class='btn btn-danger bg-danger' onclick='eliminarProducto("+ data.producto.id  +")'><i class='fas fa-trash '></i></a>" 
                         +"</td>"
                        + "</tr>"

            $('#tbody_producto').append(template)
            $('#modalCrearProducto').modal('hide')
                }
            })
        });

        $('#btnNuevoProducto').click(function(){
            if($('#slcCategoria').val()>0){
                $('#modalCrearProducto').modal('show')
            }else{
                alert('Seleccionar una categoría')
            }
        });

        $('#btnNuevoInstrumento').click(function(e){
            if($('#slcProducto').val()>0){
                $('#modalCrearInstrumento').modal('show')

                $('#slcUnidad').html('') 
                $('#slcContenido').html('')
                $('#lblFechasUnidad').html('');
                $('#lblFechasContenido').html('');
                 

                e.preventDefault();
                var seccion_id=$('#seccion_id').val() 
                var data=$(this).serialize()
                var url=$(this).attr('action')
    
                $.ajax({
                    url:'productos/getUnidades/'+seccion_id,
                    type:'GET',
                    data:data,
                    dataType:'json',
                    success:function(data){  
                       $('#slcUnidad').html('') 
                       $("#slcUnidad").append(new Option('Seleccione una unidad',''));

                       data=data.listaunidades;
                       for(var i in data) { 
                           var unid=data[i];

                           for(var j in unid){
                           $("#slcUnidad").append(new Option(unid[j].denominacion, unid[j].id));
                           // console.log(unid[j].denominacion)
                           }
                       }       
                       //console.log(data)

                    },
                    error: function() {
                        console.log("ERROR");
                    }

                });
           
            }else{
                alert('Seleccionar un producto')
            }
        });


        $('#formEditarProducto').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){                                      
                  $('#slcCategoria').change();
                  $('#modalEditarProducto').modal('hide')
                }
            })
        });
        

        $('#formEliminarProducto').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){                                      
                  $('#slcCategoria').change();
                  $('#modalEliminarProducto').modal('hide')
                }
            })
        });
        
        $('#formInstrumentoStore').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){                
                   $('#slcProducto').change();
                   $('#modalCrearInstrumento').modal('hide')
                }
            })
        });

        $('#formEditarInstrumento').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){                                      
                  $('#slcProducto').change();
                  $('#modalEditarInstrumento').modal('hide')
                }
            })
        });

        $('#formEliminarInstrumento').submit(function(e){
            e.preventDefault();
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data){                                      
                  $('#slcProducto').change();
                  $('#modalEliminarInstrumento').modal('hide')
                }
            })
        });



    })

    document.addEventListener('DOMContentLoaded', function() {
       
        $('.form-control-select2').select2();

        $('#slcContenido').select2({
            dropdownParent: $('#modalCrearInstrumento')

        });

        $('#slcUnidad').select2({
            dropdownParent: $('#modalCrearInstrumento')

        });

        

        $('#slcUnidadEdit').select2({
            dropdownParent: $('#modalEditarInstrumento')

        });

        $('#slcContenidoEdit').select2({
            dropdownParent: $('#modalEditarInstrumento')

        });

       
        

        $('#selectPeriodo').change(function(){
            var id=$(this).val();

            // console.log(id);


            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};

                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/CarrerasDocentePorPeriodo",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);
                        $("#selectCarrera").html('');
                        $("#selectCarrera").append(new Option('Seleccione una carrera',''));

                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione un area',''));

                        $("#selectArea").html('');
                        $("#selectArea").append(new Option('Seleccione un area',''));

                        $("#slcCategoria").html('');
                        $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                        $("#slcProducto").html('');
                        $("#slcProducto").append(new Option('Seleccione producto','0'));

                        $("#slcCategoria").change();
                        $("#slcProducto").change();

                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectCarrera").append(new Option(data[i].nombre,data[i].id));
                        }
                        // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    }
                });

            }else{
                $("#selectCarrera").html('');
                $("#selectCarrera").append(new Option('Seleccione una carrera',''));

                $("#selectArea").html('');
                $("#selectArea").append(new Option('Seleccione un area',''));

                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione un grupo',''));

                $("#slcCategoria").html('');
                $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                $("#slcProducto").html('');
                $("#slcProducto").append(new Option('Seleccione producto','0'));

                $("#slcCategoria").change();
                $("#slcProducto").change();
            }


        });


        $('#selectCarrera').change(function(){
            var id=$(this).val()


            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};
                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/AreasDocentePorCarrera",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);

                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione un area',''));

                        $("#selectArea").html('');
                        $("#selectArea").append(new Option('Seleccione un area',''));

                        $("#slcCategoria").html('');
                        $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                        $("#slcProducto").html('');
                        $("#slcProducto").append(new Option('Seleccione producto','0'));

                        $("#slcCategoria").change();
                        $("#slcProducto").change();

                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectArea").append(new Option(data[i].curso.nombre,data[i].id));
                        }
                        // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    }
                });

            }else{

                $("#selectArea").html('');
                $("#selectArea").append(new Option('Seleccione un area',''));

                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione una seccion',''));

                $("#slcCategoria").html('');
                $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                $("#slcProducto").html('');
                $("#slcProducto").append(new Option('Seleccione producto','0'));

                $("#slcCategoria").change();
                $("#slcProducto").change();
            }


        });


        $('#selectArea').change(function(){
            var id=$(this).val()


            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};
                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/SeccionesDocentePorArea",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);
                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione una seccion',''));

                        $("#slcCategoria").html('');
                        $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                        $("#slcProducto").html('');
                        $("#slcProducto").append(new Option('Seleccione producto','0'));

                        $("#slcCategoria").change();
                        $("#slcProducto").change();

                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectGrupo").append(new Option(data[i].seccion,data[i].id));
                        }
                        // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    }
                });

            }else{


                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione una seccion',''));

                $("#slcCategoria").html('');
                $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                $("#slcProducto").html('');
                $("#slcProducto").append(new Option('Seleccione producto','0'));

                $("#slcCategoria").change();
                $("#slcProducto").change();
            }


        });

        $('#selectGrupo').change(function(){
            var id=$(this).val()


            if(id != ""){
                $('#seccion_id').val(id) 
                $('#seccion_id2').val(id)  
                
                $("#slcCategoria").html('');
                $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                $("#slcProducto").html('');
                $("#slcProducto").append(new Option('Seleccione producto','0'));

                $("#slcCategoria").change();
                $("#slcProducto").change();
               
                @forelse ($categorias as $item)
                     $('#slcCategoria').append("<option value={{$item->id}}> {{$item->categoria}}</option>");
                @empty
                @endforelse 
            }

            else{
                $('#seccion_id').val(0)
                $('#seccion_id2').val(0)

                $("#slcCategoria").html('');
                $("#slcCategoria").append(new Option('Seleccionar categoría','0'));

                 $("#slcProducto").html('');
                 $("#slcProducto").append(new Option('Seleccione producto','0'));

                 $("#slcCategoria").change();
                 $("#slcProducto").change();

            }
        });

        
        $('#slcCategoria').change(function(){
            var id=$(this).val()


            if(id != ""){
               $("#slcProducto").html('');
               $("#slcProducto").append(new Option('Seleccione producto','0'));
               
               $("#slcProducto").change();

                $('#categoria_id').val(id)        
                $('#categoria_id2').val(id)
                var data={'categoria_id':id,'seccion_id':$('#seccion_id').val()}

                $.ajax({
                    url:'getProductosCategoria',
                    type:'GET',
                    data:data,
                    datatype:'json',
                    success: function(data){
                        $('#tbody_producto').html('')
                        data.productoscategoria.forEach(element => {
                            var template = "<tr> "
                                +"<td>"+element.id+ "</td>"
                                + "<td>" + element.producto + "</td>"
                                + "<td>" + element.estado + "</td>"                       
                                + "<td>" 
                                   +"<a href='#' class='btn btn-warning bg-secondary' size='4' onclick='editarProducto("+ element.id +")'><i class='fas fa-pen '></i></a>"
                                   +" <a href='#' class='btn btn-danger bg-danger' onclick='eliminarProducto("+ element.id  +")'><i class='fas fa-trash '></i></a>" 
                                 +"</td>"
                                + "</tr>"
                            $('#tbody_producto').append(template)
                        })  
                        //console.log(data.productoscategoria)
                        
                        $('#lblInfoCategoria').html('')
                        data.categoria.forEach(element => {
                           $('#lblInfoCategoria').append("Registrar Mínimo "+element.min_prod+" Productos <br>"
                                                         +"Peso: "+element.peso+" % <br>"
                                                         +"ID SIGES: "+element.siges_id+"<br>")
                        })
                        
                        $('#slcProducto').html('')
                        $('#slcProducto').append(new Option('Seleccionar produto','0'))

                        data.productoscategoria.forEach(element => {
                             $('#slcProducto').append("<option value=" + element.id + ">" + element.producto + "</option>");

                         })  

                    },
                    error: function() {
                        console.log("ERROR");
                    }
                });   
            }

            else{
                $('#categoria_id').val(0)
                $('#categoria_id2').val(0)

                $("#slcProducto").html('');
               $("#slcProducto").append(new Option('Seleccione producto','0'));
               
               $("#slcProducto").change();

            }
        });

    $('#slcProducto').change(function(){
        var id=$(this).val()


         if(id != ""){
            $('#producto_id').val(id)             
                    
            var data={'producto_id':id};            
            $.ajax({
                    type: "GET",
                    url: 'getInstrumentosProducto',
                    data: data,
                    datatype:'json',
                    success: function(data) {
                        $('#tbody_Instrumento').html('')
                          data.prodInstrumento.forEach(element => {
                             var fecha=' ';
                             if(element.fecha!=null)
                                fecha=element.fecha;

                              var contenido='', fechaCont='';
                              if(element.contenido_id!=null && element.contenido_id!=0){
                                 contenido=element.contenidos.conocimientos;
                                 fechaCont=element.contenidos.fecha_inicio+" / "+
                                        element.contenidos.fecha_fin;
                              }

                             var template = "<tr> "
                                 +"<td>"+element.id+ "</td>"
                                 + "<td>" + element.instrumentos.instrumento + "</td>"
                                 + "<td>" + element.estado + "</td>"  
                                 +  "<td>" + fecha + "</td>"                                                                              
                                 +  "<td>" + contenido + "</td>" 
                                 +  "<td>" + fechaCont + "</td>" 
                                 + "<td>" 
                                    +"<a href='#' id='btnEditarInst' class='btn btn-secondary bg-secondary' size='4' onclick='editarInstrumento("+ element.id +")'><i class='fas fa-pen '></i></a>"
                                    +" <a href='#' class='btn btn-danger bg-danger' onclick='eliminarInstrumento("+ element.id +")'><i class='fas fa-trash '></i></a>" 
                                  +"</td>"
                                 + "</tr>"
                             $('#tbody_Instrumento').append(template)
                         })   
                         //console.log(data)  
                    },
                    error: function() {
                        console.log("ERROR");
                    }
                });
             
        }

           else{
               $('#producto_id').val(0)            

          }
       });


    });


$('#slcUnidad').change(function(e){
        var id=$(this).val()


        if(id != ""){
            e.preventDefault();
                var unidad_id=id 
                var data=$(this).serialize()
                var url=$(this).attr('action')
    
                $.ajax({
                    url:'productos/getContenidos/'+unidad_id,
                    type:'GET',
                    data:data,
                    dataType:'json',
                    success:function(data){  
                       $('#slcContenido').html('') 
                       $("#slcContenido").append(new Option('Seleccionar un contenido',''));
                       

                       data.contenidos.forEach(element => {
                                                 
                           $("#slcContenido").append(new Option(element.conocimientos, element.id));
                           
                           //console.log(element.denominacion)
                        
                       })  
                       
                       $('#lblFechasUnidad').html('');
                       $('#lblFechasContenido').html('');
                                             
                       $('#lblFechasUnidad').append(data.unidad.fecha_inicio +" / "+ data.unidad.fecha_fin);

                        
                       //console.log(data)

                    },
                    error: function() {
                        console.log("ERROR");
                    }

                });

        }
        
        else{

        }
}); 

$('#slcContenido').change(function(e){
    var id=$(this).val()


    if(id != ""){
        e.preventDefault();
            var contenido_id=id 
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:'productos/findContenido/'+contenido_id,
                type:'GET',
                data:data,
                dataType:'json',
                success:function(data){  

                   $('#lblFechasContenido').html('');                                         
                   $('#lblFechasContenido').append(data.contenido.fecha_inicio +" / "+ data.contenido.fecha_fin);

                    
                   //console.log(data)

                },
                error: function() {
                    console.log("ERROR");
                }

            });

    }
    
    else{

    }
}); 


$('#slcUnidadEdit').change(function(e){
    var id=$(this).val()


    if(id != ""){
        e.preventDefault();
            var unidad_id=id 
            var data=$(this).serialize()
            var url=$(this).attr('action')

            $.ajax({
                url:'productos/getContenidos/'+unidad_id,
                type:'GET',
                data:data,
                dataType:'json',
                success:function(data){  
                   $('#slcContenidoEdit').html('') 
                   $("#slcContenidoEdit").append(new Option('Seleccionar un contenido',''));
                   

                   data.contenidos.forEach(element => {
                                             
                       $("#slcContenidoEdit").append(new Option(element.contenido, element.id));
                       
                       //console.log(element.denominacion)
                    
                   })  
                   
                   $('#lblFechasUnidadEdit').html('');
                   $('#lblFechasContenidoEdit').html('');
                                         
                   $('#lblFechasUnidadEdit').append(data.unidad.fecha_inicio +" / "+ data.unidad.fecha_fin);

                    
                   //console.log(data)

                },
                error: function() {
                    console.log("ERROR");
                }

            });

    }
    
    else{

    }
}); 

$('#slcContenidoEdit').change(function(e){
  var id=$(this).val()


 if(id != ""){
    e.preventDefault();
        var contenido_id=id 
        var data=$(this).serialize()
        var url=$(this).attr('action')
        console.log('con'+id)
        $.ajax({
            url:'productos/findContenido/'+contenido_id,
            type:'GET',
            data:data,
            dataType:'json',
            success:function(data){  
                console.log(data)
               $('#lblFechasContenidoEdit').html('');                                         
               $('#lblFechasContenidoEdit').append(data.contenido.fecha_inicio +" / "+ data.contenido.fecha_fin);

                
              

            },
            error: function() {
                console.log("ERROR");
            }

        });

}

else{

}
});


    function eliminarProducto(id){
        $("#modalEliminarProducto").modal('show');
        $("#formEliminarProducto").attr('action','productos/remove/'+id);
    }
    function editarProducto(id){
        $("#formEditarProducto").attr('action','productos/'+id);


        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: 'traerproducto',
                data: data,
                success: function(data) {
                    $('#productoEdit').val(data['producto']['producto']);                    

                    $("#modalEditarProducto").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }


    function eliminarInstrumento(id){
        $("#modalEliminarInstrumento").modal('show');
        $("#formEliminarInstrumento").attr('action','prodInstrumentos/remove/'+id);
    }
    function editarInstrumento(id){
        $("#formEditarInstrumento").attr('action','prodInstrumentos/'+id);  

        $('#slcUnidadEdit').html('') 
        $('#slcContenidoEdit').html('')  
        $('#lblFechasUnidadEdit').html('');
        $('#lblFechasContenidoEdit').html('');

        obtenerUnidadesEdit();

        var data = {'id':id};
        var contenido_id=0;
        $.ajax({
                type: "GET",
                url: 'traerprodinstrumento',
                data: data,
                success: function(data) {
                    var selectRol = $('#instrumentoEdit');
                    selectRol.val(data['prodinstrumento']['instrumento_id']).attr('selected', 'selected'); 
                     
                    $('#fechaEdit').val(data['prodinstrumento']['fecha']); 

                    if(data['prodinstrumento']['contenido_id']!=null && 
                    data['prodinstrumento']['contenido_id']!=0){

                    $("#slcUnidadEdit").val(data['prodinstrumento']['contenidos']['unidades_id']).trigger('change');
                   // $("#slcContenidoEdit").val(data['prodinstrumento']['contenidos']['id']).trigger('change');
                   
                    obtenerContenidos(data['prodinstrumento']['contenidos']['unidades_id'],data['prodinstrumento']['contenidos']['id']);
                    //$("#slcUnidadEdit").change()
                     //$("#slcContenidoEdit").val(data['prodinstrumento']['contenidos']['id']).trigger('change');

                   //console.log($("#slcContenidoEdit").val())
                   // $("#slcContenidoEdit").select2().select2('val',data['prodinstrumento']['contenidos']['contenido']);

                   // $("#slcContenidoEdit").val(1).trigger('change');  
                   //console.log('222')
                    }
                                    
                   
                    contenido_id=data['prodinstrumento']['contenido_id'];
                    
                   // contenido_id
                   

                    $("#modalEditarInstrumento").modal('show');

                    
                    
                   

                    
                    //console.log(data);
                },
                error: function() {
                    console.log("ERROR");
                }
            });
            
    }

  

function obtenerUnidadesEdit(){
   // e.preventDefault();
                var seccion_id=$('#seccion_id').val() 
                var data=$(this).serialize()
                var url=$(this).attr('action')
    
                $.ajax({
                    url:'productos/getUnidades/'+seccion_id,
                    type:'GET',
                    data:data,
                    dataType:'json',
                    success:function(data){  
                       $('#slcUnidadEdit').html('') 
                       $("#slcUnidadEdit").append(new Option('Seleccione una unidad',''));

                       data=data.listaunidades;
                       for(var i in data) { 
                           var unid=data[i];

                           for(var j in unid){
                           $("#slcUnidadEdit").append(new Option(unid[j].denominacion, unid[j].id));
                           // console.log(unid[j].denominacion)
                           }
                       }       
                       //console.log(data)

                    },
                    error: function() {
                        console.log("ERROR");
                    }

                });
}

function obtenerContenidos(unidad_id,contenido_id){
    //var unidad_id=id 
            var data=$(this).serialize()
            var url=$(this).attr('action')
            //console.log('111')
            $.ajax({
                url:'productos/getContenidos/'+unidad_id,
                type:'GET',
                data:data,
                dataType:'json',
                success:function(data){  
                   $('#slcContenidoEdit').html('') 
                   $("#slcContenidoEdit").append(new Option('Seleccionar un contenido',''));
                   

                   data.contenidos.forEach(element => {
                       if(contenido_id==element.id)
                       $("#slcContenidoEdit").append(new Option(element.conocimientos, element.id,true,true));
                       else
                       $("#slcContenidoEdit").append(new Option(element.conocimientos, element.id));
                       
                       
                       //console.log(element.id)
                    
                   })  
        
                   $("#slcContenidoEdit").change()
                },
                error: function() {
                    console.log("ERROR");
                }

            });

            
}


</script>

@endsection
