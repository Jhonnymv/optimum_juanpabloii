@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Evaluaciones')


@section('header_title')

@include('IntranetDocente.vistasParciales.filtroPeriodo')


@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    {{-- <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
		data-target="#modalCrear">
		<i class="icon-calendar5 text-pink-300"></i>
		<span>Añadir Nuevo Silabo</span>
	</a> --}}
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Intranet Docente</span>
<span class="breadcrumb-item active">Evaluaciones</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')

<form>

    <input type="hidden" id="seccion_id" name="seccion_id" value="0">
    <input type="hidden" id="url_getcriteriosxevaluacion" name="url_getcriteriosxevaluacion" value="{{route('url_getcriteriosxevaluacion')}}">

    <!--Importante-->
    @csrf
    <!--Importante-->

    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">


                <div class="card-body">


                    <!-- Single item selection -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="text-uppercase font-size-sm font-weight-bold">
                                Evaluaciones
                            </h5>

                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                </div>
                            </div>
                        </div>



                        <table id="tablaEval" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Evaluación</th>
                                    <th>Desde</th>
                                    <th>Al</th>
                                    <th>Sigla</th>
                                    <th>Peso</th>
                                    <th>
                                        <button href="#" type="button"
                                            class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                            style="font-size:8pt" data-toggle="modal" data-target="#modalCrear">
                                            <b><i class="icon-add mr-1 icon-1x"  ></i></b> Registrar</button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($evaluaciones as $item)
                                <tr onclick="get_criterios({{ $item->id }})">

                                    <td>
                                        {{ $item->descripcion }}
                                    </td>
                                    <td>
                                        {{ $item->fecha }}
                                    </td>
                                    <td>
                                        {{ $item->fecha_cierre }}
                                    </td>
                                    <td>
                                        {{ $item->sigla }}
                                    </td>
                                    <td>
                                        {{$item->peso}}
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-warning" style="font-size:6pt"
                                            onclick="editar({{ $item->id }})">
                                            <i class="fas fa-pen"></i></a>
                                        <a href="#" class="btn btn-danger" style="font-size:6pt"
                                            onclick="eliminar({{ $item->id }})"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">No hay elementos</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /single item selection -->


                </div>

            </div>
            <!-- /basic table -->


        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">


                <div class="card-body">


                    <!-- Single item selection -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="text-uppercase font-size-sm font-weight-bold">
                                Criterios de Evaluación

                            </h5>

                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                </div>
                            </div>
                        </div>

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Crietrio</th>
                                    <th>Sigla</th>
                                    <th>Peso</th>
                                    <th>
                                        <button href="#" type="button"
                                            class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                            style="font-size:8pt" data-toggle="modal" data-target="#modalCrearCE">
                                            <b><i class="icon-add mr-1 icon-1x"></i></b> Registrar</button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="t_body_criterios">
                                @forelse ($evaluacioncriterios as $item)
                                <tr>
                                    <td>
                                        {{ $item->descripcion }}
                                    </td>
                                    <td>
                                        {{ $item->sigla }}
                                    </td>
                                    <td>
                                        {{ $item->peso }}
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-warning" style="font-size:6pt"
                                            onclick="editarCE({{ $item->id }})">
                                            <i class="fas fa-pen"></i></a>
                                        <a href="#" class="btn btn-danger" style="font-size:6pt"
                                            onclick="eliminarCE({{ $item->id }})"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">No hay elementos</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /single item selection -->


                </div>

            </div>
            <!-- /basic table -->


        </div>
    </div>



</form>

@endsection


@section('modals')

<!-- crear modal Evaluacion-->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('evaluaciones.store')}}">
            <input type="hidden" id="seccion_id2" name="seccion_id2" value="0">
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Registrar Evaluación</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" name="descripcion" class="form-control" placeholder="Descripción Ejemplo..."
                            required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Del:</label>
                    <div class="col-md-10">
                        <input class="form-control" type="date" name="fecha" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Al:</label>
                    <div class="col-md-10">
                        <input class="form-control" type="date" name="fecha_cierre" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Siglas:</label>
                    <div class="col-lg-9">
                        <input type="text" name="sigla" class="form-control" placeholder="Sigla Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Peso:</label>
                    <div class="col-lg-2">
                        <input type="number" name="peso" class="form-control" min="1" step="1" required>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->

<!-- Editar modal Evaluacion-->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/evaluaciones">


            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Evaluación</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" id="descripcionEdit" name="descripcionEdit" class="form-control"
                            placeholder="Descripción Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Del:</label>
                    <div class="col-md-10">
                        <input class="form-control" type="date" id="fechaEdit" name="fechaEdit" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Al:</label>
                    <div class="col-md-10">
                        <input class="form-control" type="date" id="fecha_cierreEdit" name="fecha_cierreEdit" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Sigla:</label>
                    <div class="col-lg-9">
                        <input type="text" id="siglaEdit" name="siglaEdit" class="form-control"
                            placeholder="Sigla Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Peso:</label>
                    <div class="col-lg-2">
                        <input type="number" id="pesoEdit" name="pesoEdit" class="form-control" min="1" step="1" required>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->

<!-- crear modal Criterio Eva-->
<div id="modalCrearCE" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form  class="modal-content" method="POST" action="{{route('evaluacioncriterios.store')}}">
            <!--Importante-->
            @csrf
            <!--Importante-->
            <input type="hidden" id="evaluacion_id" name="evaluacion_id" value="0">

            <div class="modal-header">
                <h5 class="modal-title">Registrar Criterio de Evaluación</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" name="descripcionec" class="form-control" placeholder="Descripción Ejemplo..."
                            required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Siglas:</label>
                    <div class="col-lg-9">
                        <input type="text" name="siglaec" class="form-control" placeholder="Sigla Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Peso:</label>
                    <div class="col-lg-2">
                        <input type="number" name="pesoec" class="form-control" min="1" step="1" required>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->

<!-- Editar modal Criterio Eval-->
<div id="modalEditarCE" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditarCE" class="modal-content" method="POST" action="/evaluacioncriterios">


            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Criterio de Evaluación</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" id="descripcionecEdit" name="descripcionecEdit" class="form-control"
                            placeholder="Descripción Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Sigla:</label>
                    <div class="col-lg-9">
                        <input type="text" id="siglaecEdit" name="siglaecEdit" class="form-control"
                            placeholder="Sigla Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Peso:</label>
                    <div class="col-lg-2">
                        <input type="number" id="pesoecEdit" name="pesoecEdit" class="form-control" min="1" step="1" required>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->

<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/evaluaciones/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar la Evaluación?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<!-- Danger modal Criterios Eval-->
<div id="modalEliminarCE" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminarCE" class="modal-content" action="/evaluacioncriterios/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Criterio de Evaluación?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->


@endsection


@section('script')
<script>
    document.addEventListener('DOMContentLoaded', function() {


        $('.form-control-select2').select2();

        $('#selectPeriodo').change(function(){
            var id=$(this).val();

            // console.log(id);


            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};

                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/CarrerasDocentePorPeriodo",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);
                        $("#selectCarrera").html('');
                        $("#selectCarrera").append(new Option('Seleccione una carrera',''));

                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione un area',''));

                        $("#selectArea").html('');
                        $("#selectArea").append(new Option('Seleccione un area',''));

                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectCarrera").append(new Option(data[i].nombre,data[i].id));
                        }
						// $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    }
                });

            }else{
                $("#selectCarrera").html('');
                $("#selectCarrera").append(new Option('Seleccione una carrera',''));

                $("#selectArea").html('');
                $("#selectArea").append(new Option('Seleccione un area',''));

                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione un grupo',''));
            }


        });


        $('#selectCarrera').change(function(){
            var id=$(this).val()


            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};
                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/AreasDocentePorCarrera",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);

                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione un area',''));

                        $("#selectArea").html('');
                        $("#selectArea").append(new Option('Seleccione un area',''));
                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectArea").append(new Option(data[i].curso.nombre,data[i].id));
                        }
                        // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    }
                });

            }else{

                $("#selectArea").html('');
                $("#selectArea").append(new Option('Seleccione un area',''));

                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione una seccion',''));
            }


        });


        $('#selectArea').change(function(){
            var id=$(this).val()
			$('#pe_curso_id').val(id);

			///////////////////////////////////////////////////////////////////
			var data = {'pe_curso_id':id, 'periodo_id':$('#periodo_id').val() };
			$.ajax({
                    type: "GET",
                    url: "/IntranetDocente/VerificarSilaboCurso",
                    data: data,
                    dataType:'json',
                    success: function(data){



                    }
            });







			/////////////////////////////////////////////////////////////////



            if(id != ""){
                // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                var data = {'id':id};
                $.ajax({
                    type: "GET",
                    url: "/IntranetDocente/SeccionesDocentePorArea",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);
                        $("#selectGrupo").html('');
                        $("#selectGrupo").append(new Option('Seleccione una seccion',''));
                        for(var i in data) {
                            // console.log(data[i]);
                            $("#selectGrupo").append(new Option(data[i].seccion,data[i].id));
                        }
						// $('.ibox').children('.ibox-content').toggleClass('sk-loading');


                    }
                });

            }else{


                $("#selectGrupo").html('');
                $("#selectGrupo").append(new Option('Seleccione una seccion',''));
            }


        });

        $('#selectGrupo').change(function(){
            var id=$(this).val();

            $('#seccion_id').val(id);
            $('#seccion_id2').val(id);

            if(id != ""){



            }else{


            }


        });
    });


</script>

<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','evaluaciones/remove/'+id);
    }

    function editar(id){
        //dd(id);
        $("#formEditar").attr('action','evaluaciones/'+id);


        var data = {'id':id};

        $.ajax({
                type: "GET",
                url: 'traerevaluacion',
                data: data,
                success: function(data) {
                    $('#descripcionEdit').val(data['evaluacion']['descripcion']);
                    $('#siglaEdit').val(data['evaluacion']['sigla']);
                    $('#fechaEdit').val(data['evaluacion']['fecha']);
                    $('#fecha_cierreEdit').val(data['evaluacion']['fecha_cierre']);
                    $('#pesoEdit').val(data['evaluacion']['peso']);

                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>



<script>
    function eliminarCE(id){
        $("#modalEliminarCE").modal('show');
        $("#formEliminarCE").attr('action','evaluacioncriterios/remove/'+id);
    }

    function editarCE(id){
        //dd(id);
        $("#formEditarCE").attr('action','evaluacioncriterios/'+id);


        var data = {'id':id};

        $.ajax({
                type: "GET",
                url: 'traerevaluacioncriterio',
                data: data,
                success: function(data) {
                    $('#descripcionecEdit').val(data['evaluacioncriterios']['descripcion']);
                    $('#siglaecEdit').val(data['evaluacioncriterios']['sigla']);
                    $('#pesoecEdit').val(data['evaluacioncriterios']['peso']);

                    $("#modalEditarCE").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>



<script>



     function get_criterios(evaluacion_id){
        var url=$('#url_getcriteriosxevaluacion').val()
        var data={'id':evaluacion_id}

        $('#evaluacion_id').val(evaluacion_id);

        $.ajax({
            url:url,
            type:'GET',
            data:data,
            datatype:'json',
            success: function(data){
                $('#t_body_criterios').html('')
                data.criterios.forEach(element => {
                    var template = "<tr>"
                        +"<td>"+element.descripcion+ "</td>"
                        + "<td>" + element.sigla + "</td>"
                        + "<td>" + element.peso + "</td>"
                        +"<td><a class='btn btn-warning' style='font-size:6pt' onclick='editarCE("+element.id+")'><i class='fas fa-pen'></i></a>"
                        +" <a class='btn btn-danger' style='font-size:6pt' onclick='eliminarCE("+element.id+")'><i class='fas fa-trash'></i></a></td>"
                        + "</tr>"
                    $('#t_body_criterios').append(template)
                })
               // console.log(data.criterios)
            },
            error: function() {
                console.log("ERROR");
            }
        });
    }
</script>
@endsection
