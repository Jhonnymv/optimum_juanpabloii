@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Horario')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Intranet Docente</span>
        - Áreas</h4>
@endsection

{{--@section('header_buttons')--}}
{{--    <div class="d-flex justify-content-center">--}}

{{--        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"--}}
{{--           data-target="#modalCrear">--}}
{{--            <i class="icon-calendar5 text-pink-300"></i>--}}
{{--            <span>Añadir Nuevo Horario</span>--}}
{{--        </a>--}}
{{--    </div>--}}
{{--@endsection--}}

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Intranet Docente</span>
    <span class="breadcrumb-item active">Áreas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Mis Áreas</h5>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <label for="periodo_id">Periodo: </label>
                            <select name="periodo_id" id="periodo_id" class="form-control">
                                <option>Seleccione una opción</option>
                                @foreach($periodos as $periodo)
                                    <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-sm-4">
                            <label for="periodo_id">Periodo: </label>
                            <select name="carrera_id" id="carrera_id" class="form-control">
                                <option>Seleccione una opción</option>
                                @foreach($carreras as $carrera)
                                    <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            {{--                            <th>#</th>--}}
                            <th>Area</th>
                            <th>Grupo</th>
                            <th>Ciclo</th>
                            <th>H. teoría/H. practica</th>
                            <th>creditos</th>
                            <th>Horarios</th>
                        </tr>
                        </thead>
                        <tbody id="t_body_horarios">
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
    <input type="hidden" id="url_docenteHorarios" value="{{route('IDocente.get_mis_horarios')}}">
@endsection
@section('modals')
@endsection
@section('script')
    <script>
        $('#carrera_id').change(function () {
            var url = $('#url_docenteHorarios').val()
            var data = {'carrera_id': $('#carrera_id').val(),'periodo_id':$('#periodo_id').val()}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    console.log(data)
                    $('#t_body_horarios').html('')
                    data.forEach( element => {
                        if (element.horarios.length > 0) {
                            var horarios = "";
                            $.each(element.horarios, function (index, value) {
                                horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                            });
                        } else {
                            var horarios = 'El horario será definido con el docente'
                        }
                        var template = "<tr>" +
                            "<td>" + element.pe_curso.curso.nombre + "</td>" +
                            "<td>" + element.grupo + "</td>" +
                            "<td>" + element.pe_curso.semestre + "</td>" +
                            "<td>" + element.pe_curso.horas_teoria+'/'+element.pe_curso.horas_practica + "</td>" +
                            "<td>" + element.pe_curso.creditos + "</td>" +
                            "<td>" + horarios + "</td>" +
                            "</tr>"

                        $('#t_body_horarios').append(template)
                    })
                }
            })
        })
    </script>

@endsection
