@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Horario')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Intranet Docente</span> - Horario</h4>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Días</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-md-1 col-form-label text-center">Días</label>
                        <div class="col-md-2">
                        <select id="dias" class="form-control">
                            <option>Seleccione el dia</option>
                            @foreach($dias as $dia)
                                <option value="{{$dia}}">{{$dia}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Horario</h5>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="table_horario">
                                    <thead>
                                    <tr>
                                        <th>Aula</th>
                                        <th>Hora Inicio</th>
                                        <th>Hora Fin</th>
                                        <th>Sección</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody_horario">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_gethorarioxdiadocente" value="{{route('gethorarioxdiadocente')}}">
    <input type="hidden" name="dia" id="idia">
@endsection

@section('script')
    <script>
        $(function () {
            $('#dias').change(function () {
                var url = $('#url_gethorarioxdiadocente').val();
                var data = {'dia': $(this).val()};
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#tbody_horario').html('');
                        data.horario.forEach(element => {

                            var template = "<tr>"
                                + "<td>" + element.aula.aula + "</td>"
                                + "<td>" + element.hora_inicio +"</td>"
                                + "<td>" + element.hora_fin + "</td>"
                                + "<td>" + element.seccion_id + "</td>"
                                + "</tr>"
                            $('#tbody_horario').append(template)
                        });
                        $('#table_horario').dataTable({
                            'language': {
                                'url': '{{asset('assets/DataTables/Spanish.json')}}'
                            }
                        });
                    }
                });
            })
        });
    </script>
@endsection
