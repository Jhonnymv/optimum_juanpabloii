@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Horario')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Intranet Docente</span>
        - Áreas</h4>
@endsection

{{--@section('header_buttons')--}}
{{--    <div class="d-flex justify-content-center">--}}

{{--        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"--}}
{{--           data-target="#modalCrear">--}}
{{--            <i class="icon-calendar5 text-pink-300"></i>--}}
{{--            <span>Añadir Nuevo Horario</span>--}}
{{--        </a>--}}
{{--    </div>--}}
{{--@endsection--}}

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Intranet Docente</span>
    <span class="breadcrumb-item active">Áreas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Mis Áreas</h5>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <label for="periodo_id">Progrma: </label>
                            <select name="carrera_id" id="carrera_id" class="form-control">
                                <option>Seleccione una opción</option>
                                @foreach($carreras as $carrera)
                                    <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
{{--                            <th>#</th>--}}
                            <th>Area</th>
                            <th>Ciclo</th>
                            <th>H. teoría/H. practica</th>
                            <th>creditos</th>
                        </tr>
                        </thead>
                        <tbody id="t_body_areas">
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
    <input type="hidden" id="url_docentesecciones" value="{{route('docente.areaDocenteXCarrera')}}">
@endsection

@section('modals')




@endsection


@section('script')
    <script>
        $('#carrera_id').change(function () {
            var url = $('#url_docentesecciones').val()
            var data = {'id': $('#carrera_id').val()}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#t_body_areas').html('')
                    data.forEach( element => {
                        var template = "<tr>" +
                            "<td>" + element.curso.nombre + "</td>" +
                            "<td>" + element.semestre + "</td>" +
                            "<td>" + element.horas_practica+'/'+element.horas_teoria + "</td>" +
                            "<td>" + element.creditos + "</td>" +
                            "</tr>"

                        $('#t_body_areas').append(template)
                    })
                }
            })
        })
    </script>

@endsection
