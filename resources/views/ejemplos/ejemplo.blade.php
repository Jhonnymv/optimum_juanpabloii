@extends('Layouts.main')
@section('title','CredencialesAlumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Ejemplos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">


</div>
@endsection

@section('header_subtitle')
<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Carreras</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
<div class="row justify-content-center">
    <a href="#" class="btn btn-primary" onclick="traerAjax()">Traer elementos</a>
</div>
@endsection


@section('modals')




@endsection


@section('script')
<script>
    function traerAjax(){
        var data = {'parametro1':'dato1','parametro2':'dato2'}
        $.ajax({
                type: "GET",
                url: '/elementos',
                data: data,
                success: function(data) {
                    console.log(data['retornado1']);
                    console.log(data['retornado2']);
                    console.log(data['todos']);
                },
                error: function() {
                    console.log("ERROR");
                }
            });

    }

</script>

@endsection
