@extends('Admin.Layouts.main_gadmin')
@section('title','Docentes áreas')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Administración Jefes de
        Área
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">
{{--        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"--}}
{{--           data-target="#modalAsignarJefe">--}}
{{--            <i class="icon-calendar5 text-pink-300"></i>--}}
{{--            <span>Agregar Jefe de Área</span>--}}
{{--        </a>--}}
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Lista de Docentes</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Administrar Jefes de Área Académica</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Lista personal de la Institución
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_admin">
                            <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Apellidos Y Nombres</th>
                                <th>Área</th>
                                <th>Asignar área</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($personas as $item)
                                <tr>
                                    <td>{{$item->DNI}}</td>
                                    <td>{{$item->paterno. ' ' .$item->materno. ' '. $item->nombres}}</td>
                                    <td>
                                        @foreach($item->docente->carreras as $carrera)
                                            <span>{{$carrera->nombre}}</span> <br>
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-outline-success" onclick="buscar({{$item->id}})"><i
                                                class="icon-pencil6"> Editar </i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <input type="hidden" id="url_buscar_id" value="{{route('buscarDocente',[''])}}/">
@endsection
@section('modals')
    <div id="modalAsignarJefe" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <form class="modal-content" method="POST" action="{{route('asignarJefeArea')}}">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title">Asingnar Jefatura</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">

                            <label for="personaId" style="font-weight: bold; margin: 0">Selecione
                                Encargado</label></label>
                            <select id="persona_id" name="persona_id" class="form-control form-control-select2">
                                <option value="">Seleccionar una persona</option>
                                @foreach($personas as $persona)
                                    <option
                                        value="{{$persona->id}}">{{$persona->paterno. ' ' .$persona->materno.', '.$persona->nombres}}</option>
                                @endforeach
                            </select>

                        </div>

                    </div>
                    <div class="form-group offset-1">
                        <h5 class="font-weight-semibold">Áreas ó Carreras</h5>
                        <h6 style="color: red">Selecione Una Área ó Carrera</h6>
                        <div>
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($carreras as $item)
                                        <div class="form-check col-md-6">
                                            <label class="form-check-label">
                                                <input type="checkbox" id="carrera_id-{{$item->id}}" name="carrera_id[]"
                                                       value="{{$item->id}}">
                                                {{$item->nombre}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="align-content: space-evenly">
                    <div class="row">
                        <div class="" style="align-content: center; align-items:center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn bg-primary">Asignar</button>
                        </div>
                    </div>
                </div>

                                <input type="hidden" name="docente_id" id="docente_id">
            </form>
        </div>
    </div>


@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_admin').DataTable({

            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        function buscar(id) {
            var ruta = $('#url_buscar_id').val() + id;
            $('#modalAsignarJefe').modal('show');
            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data)

                    var docente_id= "";
                    $.each(data.docente, function (index, value) {
                        console.log(value.id)
                        $('#docente_id').val(value.id)
                        $('#persona_id').val(value.persona_id);
                    });
                    $.each(data.modulos, function (index, value) {
                        $('#moduloid-' + value.id).attr('checked', '')
                    })

                    // $('#idUsuario').val(idpersona)
                }
            })

        }

    </script>
@endsection

