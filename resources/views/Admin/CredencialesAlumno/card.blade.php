<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .container {
            background: #e0e0e0;
            margin: 0 0 1rem;
            height: 10rem;
            /*display: flex;*/
            align-items: start;
        }

        .centere {
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }

        @page {
            margin: 0px;
        }

        body {
            margin: 10px;
            /*background-image: url("../public/assets/img/fondoFotcheck.png");*/
            /*background-position: center;*/
            /*background-color: rgba(255,0,0,0.5);*/
        }
    </style>
</head>
<body>
<div>
    <div style="text-align: center; width: 100%">
        <br>
        <br>
        <img src="../public/assets/img/logo_raimondi.png" alt="" style="width: 40%">
{{--        <img src="../public/assets/img/fondoFotcheck.png" alt="" style="width: 40%">--}}
    </div>

    <div style="text-align: center; width: 100%">
        @if($alumno->persona->urlimg)
        <img src="{{public_path($alumno->persona->urlimg)}}" alt="" style="width: 50%">
        @endif
    </div>
    <div style="text-align: center; width: 100%; border-bottom: 50px">
        <br>
        <label class="text-center" style="font-weight: bold">{{$alumno->persona->nombres}}</label><br>
        <label class="text-center"
               style="font-size: small">{{$alumno->persona->paterno.' '. $alumno->persona->materno}}</label>
        <br>
    </div>
    <table>
        <tr >
            <th>
                <label style="font-size:small; margin-left: 50px">{{$alumno->gru_sanginio}}</label>
            </th>
            <th>
                <img style="width: 25%; margin-left: 10px" alt="" src="../public/qrcodes/qrcode.svg">
            </th>
        </tr>
    </table>
    <div style="text-align: center; width: 100%">
        <label style="font-size: 5px">{{$alumno->persona->DNI}}</label>
    </div>
</div>
</body>
</html>
