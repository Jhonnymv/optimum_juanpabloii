@extends('Admin.Layouts.main_gadmin')
@section('content')
    <form id="formUpdateAlumno" name="formUpdateAlumno" method="POST" action="{{route('updateAlumno')}}">

        <!--Importante-->
    @csrf
    <!--Importante-->
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style=" font-weight: bold" class="card-title text-center">Editar
                                Alumnos</h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12" style="display: flex; flex-direction: row; align-items: center">
                                <div class="">
                                    <label class="text-center" style="margin: 0; padding-top: 7px">Todos los campos que
                                        tengan un <label for="" style="color: red"> * </label> son obligatorios</label>
                                </div>
                                {{--                                @if ($errors->any())--}}
                                {{--                                    <div class="alert alert-danger">--}}
                                {{--                                        <ul>--}}
                                {{--                                            @foreach ($errors->all() as $error)--}}
                                {{--                                                <li>{{ $error }}</li>--}}
                                {{--                                            @endforeach--}}
                                {{--                                        </ul>--}}
                                {{--                                    </div>--}}
                                {{--                                @endif--}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <label style="font-weight: bold; border-style: none">DNI:</label>
                                <input type="text" name="DNI" id="DNI" class="form-control" placeholder="Número de DNI"
                                       style="margin-top: 0">
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="btnBuscar">Comprobar</button>
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>

                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="#basic-pill1" id="li_datos_personales" class="nav-link legitRipple active"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos Alumno</a>
                            </li>
                            <li class="nav-item" id="pill-contacto">
                                <a href="#basic-pill2" class="nav-link legitRipple" id="pill-contacto_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos de Padre</a>
                            </li>
                            <li class="nav-item" id="pill-estudio">
                                <a href="#basic-pill3" class="nav-link legitRipple" id="pill-estudio_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos de Madre</a>
                            </li>
                            <li class="nav-item" id="pill-inscripcion">
                                <a href="#basic-pill4" class="nav-link legitRipple" id="pill-inscripcion_a"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos Apoderado</a>
                            </li>
                        </ul>
                        <br>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="basic-pill1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Nombres: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="nombres" id="nombres" class="form-control"
                                               placeholder="Nombres...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Paterno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>

                                        <input type="text" name="paterno" id="paterno" class="form-control"
                                               placeholder="Apellido paterno..."
                                        >
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Materno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="materno" id="materno" class="form-control"
                                               placeholder="Apellido Materno..."
                                        >
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label for="ciclo" style="font-weight: bold; margin: 0">Sexo: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select name="sexo" id="sexo" class="form-control">
                                            <option>Seleccione Sexo</option>
                                            <option value="F"> Femenino</option>
                                            <option value="M"> Masculino</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Fecha de Nacimiento: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input class="form-control" type="date" name="fecha_nacimiento"
                                               id="fecha_nacimiento"
                                        >
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento" name="departamento_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia" name="provincia_id">
                                                <option selected>Seleccione una Provincia</option>
                                                @foreach($provincias as $provincia)
                                                    <option
                                                        value="{{$provincia->id}}">{{$provincia->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito" name="distrito_id">
                                                <option value="" selected>Seleccione un Distrito</option>
                                                @foreach($distritos as $distrito)
                                                    <option
                                                        value="{{$distrito->id}}">{{$distrito->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">N° de Hermanos:</label>
                                        <input type="number" name="num_hermanos" id="num_hermanos" class="form-control"
                                               placeholder="Hermanos...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Lugar que Ocupa: </label>
                                        <input type="number" name="num_ocupa" id="num_ocupa" class="form-control"
                                               placeholder="Lugar que ocupa...">
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Religion: </label>
                                        <input type="text" name="religion" id="religion" class="form-control"
                                               placeholder="Lugar que ocupa...">
                                    </div>

                                    <div class="col-md-3">
                                        <label for="ciclo" style="font-weight: bold; margin: 0">Grupo Sanguinio: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select name="gru_sanginio" id="gru_sanginio" class="form-control">
                                            <option>Seleccione</option>
                                            <option value="A+"> A+</option>
                                            <option value="A-"> A-</option>
                                            <option value="B+"> B+</option>
                                            <option value="B-"> B-</option>
                                            <option value="AB+"> AB</option>
                                            <option value="AB-"> AB</option>
                                            <option value="O+"> O+</option>
                                            <option value="O-"> O-</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Parto: </label>
                                        <select name="parto" id="parto" class="form-control">
                                            <option>Seleccione</option>
                                            <option value="1"> Normal</option>
                                            <option value="2"> Cesárea</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Enfermedades:</label>
                                        <textarea type="text" name="enfermedades" id="enfermedades" class="form-control"
                                                  placeholder="Enfermedades..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Alergias:</label>
                                        <textarea type="text" name="alergias" id="alergias" class="form-control"
                                                  placeholder="Alergias..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Accidentes Graves:</label>
                                        <textarea type="text" name="accidentes" id="accidentes" class="form-control"
                                                  placeholder="Accidentes..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Exp. Traumaticas:</label>
                                        <textarea type="text" name="traumas" id="traumas" class="form-control"
                                                  placeholder="Traumas..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Lim. Fisicas</label>
                                        <textarea type="text" name="lim_fisicas" id="lim_fisicas" class="form-control"
                                                  placeholder="Fiscas..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Peso:</label>
                                        <input type="text" name="peso" id="peso" class="form-control"
                                               placeholder="Peso...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Talla:</label>
                                        <input type="text" name="talla" id="talla" class="form-control"
                                               placeholder="Talla...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Vacunas:</label>
                                        <textarea type="text" name="vacunas" id="vacunas" class="form-control"
                                                  placeholder="Vacunas..."
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Tipo de Seguro:</label>
                                        <select name="t_seguro" id="t_seguro" class="form-control">
                                            <option>Seleccione tipo</option>
                                            <option value="1"> SIS</option>
                                            <option value="2"> EsSalud</option>
                                            <option value="3"> Rimac</option>
                                            <option value="4"> Mapfre</option>
                                            <option value="5"> Sanitas</option>
                                            <option value="6"> Pacifico</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">N° Seguro:</label>
                                        <input type="text" name="n_seguro" id="n_seguro" class="form-control"
                                               placeholder="N° de Seguro...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Teléfono:</label>
                                        <input type="text" name="telefono" id="telefono" class="form-control"
                                               placeholder="Teléfono...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Email: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="email" id="email" class="form-control"
                                               placeholder="Email...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Email Personal:</label>
                                        <input type="text" name="email_personal" id="email_personal"
                                               class="form-control" placeholder="Email...">
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Dirección:</label>
                                        <input type="text" name="direccion" id="direccion" class="form-control"
                                               placeholder="Dirección">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Usuario: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="usuario" id="usuario" class="form-control"
                                               placeholder="Usuario..."
                                        >
                                    </div>
                                    {{--                                    <div class="col-md-3">--}}
                                    {{--                                        <label style="font-weight: bold; margin: 0">Contraseña: (<label--}}
                                    {{--                                                for="" style="color: red; margin: 0">*</label>)</label></label><span--}}
                                    {{--                                            class="text-danger">*</span></label>--}}
                                    {{--                                        <input type="text" name="password" id="password" class="form-control"--}}
                                    {{--                                               placeholder="Contraseña..."  >--}}
                                    {{--                                    </div>--}}
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Estado Alumno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="estado" id="estado" class="form-control" value="1"
                                               readonly
                                        >
                                    </div>
                                    <br>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Tipo Usuario: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select name="tipo" id="tipo" class="form-control">
                                            <option value="">Seleccionar Usuario</option>
                                            <option value="alumno">Alumno</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="carrera_id" style="font-weight: bold; margin: 0">Nivel: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label>
                                        <select name="carrera_id" id="carrera_id" class="form-control">
                                            <option value="">Seleccione un Nivel</option>
                                            @foreach($carreras as $carrera)
                                                <option
                                                    value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="ciclo" style="font-weight: bold; margin: 0">Grado: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select name="ciclo" id="ciclo" class="form-control">
                                            <option>Seleccione Grado</option>
                                            <option value="1"> 1</option>
                                            <option value="2"> 2</option>
                                            <option value="3"> 3</option>
                                            <option value="4"> 4</option>
                                            <option value="5"> 5</option>
                                            <option value="6"> 6</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="grupo" style="font-weight: bold; margin: 0">Sección: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select name="grupo" id="grupo" class="form-control">
                                            <option>Seleccione Sección</option>
                                            <option value="A"> A</option>
                                            <option value="B"> B</option>
                                            <option value="C"> C</option>
                                        </select>
                                    </div>

                                    <br>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fecha_ingreso" style="font-weight: bold; margin: 0">Fecha de
                                                Ingreso:
                                                (<label
                                                    for=""
                                                    style="color: red; margin: 0">*</label>)</label></label></label>
                                            <input type="date" name="fecha_ingreso" id="fecha_ingreso"
                                                   class="form-control"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="basic-pill2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apoderado: </label>

                                        <label style=" border-style: none">Si:<input type="radio" name="apoderado"
                                                                                     id="apoderado1"
                                                                                     class="form-control"
                                                                                     value="si"></label>
                                        <label style=" border-style: none">No:<input type="radio" name="apoderado"
                                                                                     id="apoderado2"
                                                                                     class="form-control"
                                                                                     value="no"></label>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none"> Padre Vive: </label>

                                        <label style=" border-style: none">Si:<input type="radio" name="vivepadre"
                                                                                     id="vivepadre"
                                                                                     class="form-control"
                                                                                     value="si"></label>
                                        <label style=" border-style: none">No:<input type="radio" name="vivepadre"
                                                                                     id="vivepadre"
                                                                                     class="form-control"
                                                                                     value="no"></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">DNI: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="dniPadre" id="dniPadre" class="form-control"
                                               placeholder="DNI...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Paterno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="paterno_padre" id="paterno_padre" class="form-control"
                                               placeholder="Apellido Paterno...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Materno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="materno_padre" id="materno_padre" class="form-control"
                                               placeholder="Apellido Materno...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Nombres: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="nombres_padre" id="nombres_padre" class="form-control"
                                               placeholder="Nombres...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Fecha Nacimiento: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="date" name="fecha_nacimiento_padre" id="fecha_nacimiento_padre"
                                               class="form-control"
                                        >
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento_padre"
                                                    name="departamento_padre_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia_padre" name="provincia_padre_id">
                                                <option selected>Seleccione una Provincia</option>
                                                @foreach($provincias as $provincia)
                                                    <option
                                                        value="{{$provincia->id}}">{{$provincia->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito_padre_id"
                                                    name="distrito_padre_id">
                                                <option value="" selected>Seleccione un Distrito</option>
                                                @foreach($distritos as $distrito)
                                                    <option
                                                        value="{{$distrito->id}}">{{$distrito->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Vive con el alumno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select type="text" name="vive_alumno_padre" id="vive_alumno_padre"
                                                class="form-control">
                                            <option>Seleccione</option>
                                            <option value="S">SI</option>
                                            <option value="N">NO</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Dirección (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="direccion_padre" id="direccion_padre"
                                               class="form-control" placeholder="Direción...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Nivel de Instrucción
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        {{--                                        <input type="text" name="nivel_instruccion_padre" id="nivel_instruccion_padre"--}}
                                        {{--                                               class="form-control" placeholder=" Nivel de Instru ..."  >--}}

                                        <select type="text" name="nivel_instruccion_padre" id="nivel_instruccion_padre"
                                                class="form-control">
                                            <option>Seleccione</option>
                                            <option value="1">Iletrado</option>
                                            <option value="2">Primaria</option>
                                            <option value="3">Secundaria</option>
                                            <option value="4">Superior</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Religión
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="religion_padre" id="religion_padre"
                                               class="form-control" placeholder=" Nivel de Instru ...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Profesión (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="profesion_padre" id="profesion_padre"
                                               class="form-control" placeholder="profesión...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Ocupación (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="ocupacion_padre" id="ocupacion_padre"
                                               class="form-control" placeholder="ocupación ...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Centro de Trabajo (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="centro_trabajo_padre" id="centro_trabajo_padre"
                                               class="form-control"
                                               placeholder="Centro de tra...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Email (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="email_personal_padre" id="email_personal_padre"
                                               class="form-control"
                                               placeholder="email de tra...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Celular (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="telefono_padre" id="telefono_padre"
                                               class="form-control"
                                               placeholder="telefono...">
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="basic-pill3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apoderado: </label>

                                        <label style=" border-style: none">Si:<input type="radio" name="apoderado2"
                                                                                     id="apoderado3"
                                                                                     class="form-control"
                                                                                     value="si"></label>
                                        <label style=" border-style: none">No:<input type="radio" name="apoderado2"
                                                                                     id="apoderado4"
                                                                                     class="form-control"
                                                                                     value="no"></label>
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Madre Vive: </label>

                                        <label style=" border-style: none">Si:<input type="radio" name="viveMadre"
                                                                                     id="viveMadre"
                                                                                     class="form-control"
                                                                                     value="si"></label>
                                        <label style=" border-style: none">No:<input type="radio" name="viveMadre"
                                                                                     id="viveMadre"
                                                                                     class="form-control"
                                                                                     value="no"></label>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">DNI: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="dniMadre" id="dniMadre" class="form-control"
                                               placeholder="DNI...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Paterno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="paterno_madre" id="paterno_madre" class="form-control"
                                               placeholder="Apellido Paterno...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Apellido Materno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="materno_madre" id="materno_madre" class="form-control"
                                               placeholder="Apellido Materno...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Nombres: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="nombres_madre" id="nombres_madre" class="form-control"
                                               placeholder="Nombres...">
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Fecha Nacimiento: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="date" name="fecha_nacimiento_madre" id="fecha_nacimiento_madre"
                                               class="form-control"
                                        >
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento_madre"
                                                    name="departamento_madre_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia_madre" name="provincia_madre_id">
                                                <option selected>Seleccione una Provincia</option>
                                                @foreach($provincias as $provincia)
                                                    <option
                                                        value="{{$provincia->id}}">{{$provincia->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito_madre_id"
                                                    name="distrito_madre_id">
                                                <option value="" selected>Seleccione un Distrito</option>
                                                @foreach($distritos as $distrito)
                                                    <option
                                                        value="{{$distrito->id}}">{{$distrito->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Vive con el alumno: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <select type="text" name="vive_alumno_madre" id="vive_alumno_madre"
                                                class="form-control">
                                            <option>Seleccione</option>
                                            <option value="S">SI</option>
                                            <option value="N">NO</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Dirección (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="direccion_madre" id="direccion_madre"
                                               class="form-control" placeholder="Direción...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Nivel de Instrucción
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        {{--                                        <input type="text" name="nivel_instruccion_madre" id="nivel_instruccion_madre"--}}
                                        {{--                                               class="form-control" placeholder=" Nivel de Instru ..."  >--}}
                                        <select type="text" name="nivel_instruccion_madre" id="nivel_instruccion_madre"
                                                class="form-control">
                                            <option>Seleccione</option>
                                            <option value="1">Iletrado</option>
                                            <option value="2">Primaria</option>
                                            <option value="3">Secundaria</option>
                                            <option value="4">Superior</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Religión
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="religion_madre" id="religion_madre"
                                               class="form-control" placeholder=" Nivel de Instru ...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Profesión (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="profesion_madre" id="profesion_madre"
                                               class="form-control" placeholder="profesión...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Ocupación (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="ocupacion_madre" id="ocupacion_madre"
                                               class="form-control" placeholder="ocupación ...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Centro de Trabajo (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="centro_trabajo_madre" id="centro_trabajo_madre"
                                               class="form-control"
                                               placeholder="Centro de tra...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Email (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="email_personal_madre" id="email_personal_madre"
                                               class="form-control"
                                               placeholder="email de tra...">
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; border-style: none">Celular (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label></label>
                                        <input type="text" name="telefono_madre" id="telefono_madre"
                                               class="form-control"
                                               placeholder="telefono...">
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="basic-pill4">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="estadoCivil" style="font-weight: bold; margin: 0">Estado Civil
                                                de
                                                los PADRES: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select name="estado_civil" id="estado_civil" class="form-control">
                                                <option value="">Seleccione Uno</option>
                                                <option value="1">Solteros</option>
                                                <option value="2">Casados</option>
                                                <option value="3">Convivientes</option>
                                                <option value="4">Separados</option>
                                                <option value="5">Divorciados</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="periodo" style="font-weight: bold; margin: 0">Relacion de
                                                Padres: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select name="relacion" id="relacion" class="form-control">
                                                <option value="">Seleccione Uno</option>
                                                <option value="1">Muy Buena</option>
                                                <option value="2">Buena</option>
                                                <option value="3">Regular</option>
                                                <option value="4">Mala</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="periodo" style="font-weight: bold; margin: 0">Aparte de los
                                                padres que otra persona pasa más tiempo con el niño(a): (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <textarea type="text" name="m_tiempo" id="m_tiempo" class="form-control"
                                                      placeholder="Persona más tiempo..."
                                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="periodo" style="font-weight: bold; margin: 0">Numero de
                                                Emergencia: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <textarea type="text" name="num_emergencia" id="num_emergencia"
                                                      class="form-control"
                                                      placeholder="Persona más tiempo..."
                                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="">
                                        <button type="submit" id="btn_enviardatos"
                                                class="btn btn-primary">Guardar Datos alumno
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /basic layout -->
            <input type="hidden" id="url_distritos_prov" value="{{route('distrito_prov', [''])}}/">
            <input type="hidden" id="url_provincias_dep" value="{{route('provincia_dep', [''])}}/">
            <input type="hidden" id="url_buscarxdni" value="{{route('busAlumnoxDni', [''])}}/">
            <input type="hidden" id="idPersonaAlumno" name="idPersonaAlumno">
            <input type="hidden" id="idPersonaPadre" name="idPersonaPadre">
            <input type="hidden" id="idPersonaMadre" name="idPersonaMadre">
            <input type="hidden" id="idMadre" name="idMadre">
            <input type="hidden" id="idPadre" name="idPadre">
            <input type="hidden" id="idAlumno" name="idAlumno">
            <input type="hidden" id="idAlumnoCarrera" name="idAlumnoCarrera">
        </div>
    </form>
@endsection

@section('script')

    <script>
        $(function () {
            $('#departamento').change(function () {
                $('#distrito').html('');
                $('#distrito').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia').html('');
                        $('#provincia').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito').html('');
                            $('#distrito').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
        });

        $(function () {
            $('#departamento_padre').change(function () {
                $('#distrito_padre_id').html('');
                $('#distrito_padre_id').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia_padre').html('');
                        $('#provincia_padre').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia_padre').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia_padre').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito_padre_id').html('');
                            $('#distrito_padre_id').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito_padre_id').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
        });

        $(function () {
            $('#departamento_madre').change(function () {
                $('#distrito_madre_id').html('');
                $('#distrito_madre_id').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia_madre').html('');
                        $('#provincia_madre').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia_madre').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia_madre').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito_madre_id').html('');
                            $('#distrito_madre_id').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito_madre_id').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
        });


    </script>

    <script>
        $("#apoderado1").click(function () {
            $("#apoderado1").prop("checked", true);
            $("#apoderado1").prop("value", "si");

            $("#apoderado2").prop("checked", false);

            $("#apoderado3").prop("checked", false);

            $("#apoderado4").prop("checked", true);
            $("#apoderado4").prop("value", "no");
        });

        $("#apoderado2").click(function () {
            $("#apoderado2").prop("checked", true);
            $("#apoderado2").prop("value", "no");

            $("#apoderado1").prop("checked", false);

            $("#apoderado4").prop("checked", false);

            $("#apoderado3").prop("checked", true);
            $("#apoderado3").prop("value", "si");
        });

        $("#apoderado3").click(function () {
            $("#apoderado2").prop("checked", true);
            $("#apoderado2").prop("value", "no");

            $("#apoderado1").prop("checked", false);

            $("#apoderado4").prop("checked", false);

            $("#apoderado3").prop("checked", true);
            $("#apoderado3").prop("value", "si");
        });

        $("#apoderado4").click(function () {
            $("#apoderado4").prop("checked", true);
            $("#apoderado4").prop("value", "no");

            $("#apoderado2").prop("checked", false);

            $("#apoderado3").prop("checked", false);

            $("#apoderado1").prop("checked", true);
            $("#apoderado1").prop("value", "si");
        });
    </script>

    <script>
        var departamento = '';
        var provincia = '';
        var distrito = '';
        $(function () {
            $('#btnBuscar').click(function () {
                var dniBuscar = $('#DNI').val();
                var url = $('#url_buscarxdni').val() + dniBuscar;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        if (data !== null) {
                            $('#departamento option[value=' + data.distrito_alumno[0].provincia.departamento.id + ']').attr('selected', true);
                            $('#provincia option[value=' + data.distrito_alumno[0].provincia.id + ']').attr('selected', true);
                            $('#distrito option[value=' + data.distrito_alumno[0].id + ']').attr('selected', true);
                            $('#nombres').val(data.alumno.persona.nombres);
                            $('#paterno').val(data.alumno.persona.paterno);
                            $('#materno').val(data.alumno.persona.materno);
                            $('#sexo').val(data.alumno.persona.sexo);
                            $('#fecha_nacimiento').val(data.alumno.persona.fecha_nacimiento);
                            $('#num_hermanos').val(data.alumno.num_hermanos);
                            $('#num_ocupa').val(data.alumno.num_ocupa);
                            $('#religion').val(data.alumno.religion);
                            $('#gru_sanginio').val(data.alumno.gru_sanginio);
                            $('#parto').val(data.alumno.parto);
                            $('#enfermedades').val(data.alumno.enfermedades);
                            $('#alergias').val(data.alumno.alergias);
                            $('#accidentes').val(data.alumno.accidentes);
                            $('#traumas').val(data.alumno.traumas);
                            $('#lim_fisicas').val(data.alumno.lim_fisicas);
                            $('#peso').val(data.alumno.peso);
                            $('#talla').val(data.alumno.talla);
                            $('#vacunas').val(data.alumno.vacunas);
                            $('#t_seguro').val(data.alumno.t_seguro);
                            $('#n_seguro').val(data.alumno.n_seguro);
                            $('#telefono').val(data.alumno.persona.telefono);
                            $('#email').val(data.alumno.persona.email);
                            $('#email_personal').val(data.alumno.persona.email_personal);
                            $('#direccion').val(data.alumno.persona.direccion);
                            $('#usuario').val(data.usuario.usuario);
                            // $('#password').val(data.usuario.password);
                            $('#estado').val(data.usuario.estado);
                            $('#tipo').val(data.usuario.tipo);
                            $('#carrera_id').val(data.alumno_carrera.carrera_id);
                            $('#ciclo').val(data.alumno_carrera.ciclo);
                            $('#grupo').val(data.alumno_carrera.grupo);
                            $('#fecha_ingreso').val(data.alumno_carrera.fecha_ingreso);

                            if (data.padre.length !== 0) {
                                $('#departamento_padre option[value=' + data.distrito_padre[0].provincia.departamento.id + ']').attr('selected', true);
                                $('#provincia_padre option[value=' + data.distrito_padre[0].provincia.id + ']').attr('selected', true);
                                $('#distrito_padre_id option[value=' + data.distrito_padre[0].id + ']').attr('selected', true);
                                $('input:radio[name=apoderado]').val([data.padre.apoderado]);
                                $('input:radio[name=vivepadre]').val([data.padre.vive]);
                                $('#dniPadre').val(data.padre.persona.DNI);
                                $('#paterno_padre').val(data.padre.persona.paterno);
                                $('#materno_padre').val(data.padre.persona.materno);
                                $('#nombres_padre').val(data.padre.persona.nombres);
                                $('#fecha_nacimiento_padre').val(data.padre.persona.fecha_nacimiento);
                                $('#vive_alumno_padre').val(data.padre.vive_alumno);
                                $('#direccion_padre').val(data.padre.persona.direccion);
                                $('#nivel_instruccion_padre').val(data.padre.nivel_instruccion);
                                $('#religion_padre').val(data.padre.religion);
                                $('#profesion_padre').val(data.padre.profesion);
                                $('#ocupacion_padre').val(data.padre.ocupacion);
                                $('#centro_trabajo_padre').val(data.padre.centro_trabajo);
                                $('#email_personal_padre').val(data.padre.persona.email_personal);
                                $('#telefono_padre').val(data.padre.persona.telefono);
                                $('#estado_civil').val(data.padre.estado_civil);
                            }

                            if (data.madre.length !== 0) {
                                $('#departamento_madre option[value=' + data.distrito_madre[0].provincia.departamento.id + ']').attr('selected', true);
                                $('#provincia_madre option[value=' + data.distrito_madre[0].provincia.id + ']').attr('selected', true);
                                $('#distrito_madre_id option[value=' + data.distrito_madre[0].id + ']').attr('selected', true);
                                $('input:radio[name=apoderado2]').val([data.madre.apoderado]);
                                $('input:radio[name=viveMadre]').val([data.madre.vive]);
                                $('#dniMadre').val(data.madre.persona.DNI);
                                $('#paterno_madre').val(data.madre.persona.paterno);
                                $('#materno_madre').val(data.madre.persona.materno);
                                $('#nombres_madre').val(data.madre.persona.nombres);
                                $('#fecha_nacimiento_madre').val(data.madre.persona.fecha_nacimiento);
                                $('#vive_alumno_madre').val(data.madre.vive_alumno);
                                $('#direccion_madre').val(data.madre.persona.direccion);
                                $('#nivel_instruccion_madre').val(data.madre.nivel_instruccion);
                                $('#religion_madre').val(data.madre.religion);
                                $('#profesion_madre').val(data.madre.profesion);
                                $('#ocupacion_madre').val(data.madre.ocupacion);
                                $('#centro_trabajo_madre').val(data.madre.centro_trabajo);
                                $('#email_personal_madre').val(data.madre.persona.email_personal);
                                $('#telefono_madre').val(data.madre.persona.telefono);
                            }
                            $('#relacion').val(data.alumno.relacion);
                            $('#m_tiempo').val(data.alumno.m_tiempo);
                            $('#num_emergencia').val(data.alumno.num_emergencia);

                            $('#idPersonaAlumno').val(data.alumno.persona.id);
                            if (data.padre.length != 0)
                                $('#idPersonaPadre').val(data.padre.persona.id);
                            if (data.madre.length != 0)
                                $('#idPersonaMadre').val(data.madre.persona.id);
                            $('#idAlumno').val(data.alumno.id);
                            $('#idMadre').val(data.madre.id);
                            $('#idPadre').val(data.padre.id);
                            $('#idAlumnoCarrera').val(data.alumno_carrera.id);

                        } else {
                            console.log(data)
                            alert('El usuario con dni: ' + +'no existe.')
                        }
                    }
                })
            })
        })
    </script>
@endsection
