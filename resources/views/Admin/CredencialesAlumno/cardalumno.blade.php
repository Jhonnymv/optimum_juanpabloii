@extends('Admin.Layouts.main_gadmin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Alumnos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para ver Alumnos
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_alumnos">
                            <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>Grupo</th>
                                <th>Ver Card</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($alumnos as $item)
                                <tr>
                                    <td>{{$item->persona->paterno.' '.$item->persona->materno.', '.$item->persona->nombres}}</td>
                                    <td>{{$item->carreras->first()->nombre}}</td>
                                    <td>{{$item->alumnocarreras->first()->ciclo}}</td>
                                    <td>{{$item->alumnocarreras->first()->grupo}}</td>
                                    <td>
                                        <a type="button"  target="_blank" class="btn btn-outline-success" href="{{route('showAlumnosCard',$item->id)}}"><i
                                                class="icon-pencil6"> Imprimir </i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="url_get_card" value="{{route('showAlumnosCard',[''])}}/">
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        function buscar (id){
            var ruta = $('#url_get_card').val()+ id;

            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',

            })
            console.log(ruta);
        }
    </script>
@endsection
