@extends('Admin.Layouts.main_gadmin')
@section('title','ConceptoPagos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Concepto Pagos</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal" data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir un Nuevo</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Conceptos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Conceptos de Pago</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para la Edición y Mantenimiento de Concepto de pagos
                    <br>
                    <br>

                    <div class="table-responsive">
                        <table class="table" id="table_categorias">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Observaciones</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_conceptos">
                            @foreach($conceptos as $item)
                                <tr>
                                    <td>
                                        {{$item->nombre}}
                                    </td>
                                    <td>
                                        {{$item->precio}}
                                    </td>
                                    <td>
                                        {{$item->observaciones}}
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal" data-target="#modalEditar" onclick="editar({{$item->id}})">
                                            <i class="icon-calendar5 text-pink-300"></i>
                                            <span>Editar</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /basic table -->
        </div>
    </div>
{{--    <input type="hidden" id="url_getconceptos" value="{{route('conceptopagos', [''])}}/">--}}
    <input type="hidden" id="url_editarConceptos" value="{{route('editConceptos', [''])}}/">
@endsection


@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('storeConcepto')}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Crear Nueva Concepto</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nombre:</label>
                        <div class="col-lg-9">
                            <input type="text" id="nombre" name="nombre" class="form-control"
                                   placeholder="Descripcion"
                                   required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <select name="periodo_id" id="periodo_id" class="form-control">
                                <option value="">Seleccione un Periodo</option>
                                @foreach($periodos as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->descripcion .' - '. $item->tipo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Carrera:</label>
                        <div class="col-lg-9">
                            <select name="carrera_id" id="carrera_id" class="form-control">
                                <option value="">Seleccione una Carrera</option>
                                @foreach($carreras as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Precio:</label>
                        <div class="col-lg-9">
                            <input type="number" id="precio" name="precio" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Observaciones:</label>
                        <div class="col-lg-9">
                            <input type="text" id="observaciones" name="observaciones" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->


    {{--    <!-- editar modal -->--}}
    <div id="modalEditar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEditar" class="modal-content" method="POST" action="{{route('updateConceptos')}}">
                @method('PUT')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Editar Concepto</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <input type="hidden" name="idConcepto" id="idConcepto">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nombre:</label>
                        <div class="col-lg-9">
                            <input type="text" id="nombreEditar" name="nombreEditar" class="form-control"
                                   placeholder="Descripcion"
                                   required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <select name="periodo_idEditar" id="periodo_idEditar" class="form-control">
                                <option value="">Seleccione un Periodo</option>
                                @foreach($periodos as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->descripcion .' - '. $item->tipo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Carrera:</label>
                        <div class="col-lg-9">
                            <select name="carrera_idEditar" id="carrera_idEditar" class="form-control">
                                <option value="">Seleccione una Carrera</option>
                                @foreach($carreras as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Precio:</label>
                        <div class="col-lg-9">
                            <input type="number" id="precioEditar" name="precioEditar" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Observaciones:</label>
                        <div class="col-lg-9">
                            <input type="text" id="observacionesEditar" name="observacionesEditar" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /editar modal -->



@endsection



@section('script')
    <script>
        function editar(id) {
            // console.log(id);
            var url = $('#url_editarConceptos').val() + id;
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#nombreEditar').val(data.concepto.nombre);
                    $('#periodo_idEditar').val(data.concepto.periodo_id);
                    $('#carrera_idEditar').val(data.concepto.carrera_id);
                    $('#precioEditar').val(data.concepto.precio);
                    $('#observacionesEditar').val(data.concepto.observaciones);
                    $('#idConcepto').val(data.concepto.id);

                    // $("#modalEditar").modal('show');
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }

    </script>

@endsection

