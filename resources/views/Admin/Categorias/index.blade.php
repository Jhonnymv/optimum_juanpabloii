@extends('Admin.Layouts.main_gadmin')
@section('title','Categorías')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Categorías</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal" data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Nueva Categoría</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Categorías</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Categorías</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para la Edición y Mantenimiento de Categorías
                    <br>
                    <br>
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Periodo:</label>
                        <div class="col-lg-3">
                            <select id="carrera_select" name="carrera_select" class="form-control">
                                <option>Seleccione el Nivel</option>
                                @forelse ($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table" id="table_categorias">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>Nombre</th>
                                <th>Periodo</th>
                                <th>Nivel</th>
                                <th>Monto</th>
                                <th>Observaciones</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbody_categorias">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /basic table -->
        </div>
    </div>
    <input type="hidden" id="url_getcategoriaxcarrera" value="{{route('getcategoriaxcarrera', [''])}}/">
    <input type="hidden" id="url_editarcategoria" value="{{route('editCategoria', [''])}}/">
@endsection


@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('storeCategoria')}}">
            @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Crear Nueva Categoría</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nombre:</label>
                        <div class="col-lg-9">
                            <input type="text" id="nombre" name="nombre" class="form-control"
                                   placeholder="Categoría"
                                   required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <select id="periodo_id" name="periodo_id" class="form-control">
                                <option>Seleccione el Periodo</option>
                                @forelse ($periodos as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->descripcion}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nivel:</label>
                        <div class="col-lg-9">
                            <select id="carrera_id" name="carrera_id" class="form-control">

                                @forelse ($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @empty

                                @endforelse

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Observaciones:</label>
                        <div class="col-lg-9">
                            <input type="text" id="observaciones" name="observaciones" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Monto:</label>
                        <div class="col-lg-9">
                            <input type="text" id="monto" name="monto" class="form-control"
                                   placeholder="Monto">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->


    {{--    <!-- editar modal -->--}}
    <div id="modalEditar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEditar" class="modal-content" method="POST" action="{{route('updateCategoria')}}">
            @method('PUT')
            @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Editar Categoría</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <input type="hidden" name="idCategoria" id="idCategoria">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nombre:</label>
                        <div class="col-lg-9">
                            <input type="text" id="nombreEditar" name="nombreEditar" class="form-control" placeholder="Categoría" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <select id="periodo_idEditar" name="periodo_idEditar" class="form-control">
                                <option>Seleccione el Periodo</option>
                                @forelse ($periodos as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->descripcion}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nivel:</label>
                        <div class="col-lg-9">
                            <select id="carrera_idEditar" name="carrera_idEditar" class="form-control">
                                @forelse ($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Observaciones:</label>
                        <div class="col-lg-9">
                            <input type="text" id="observacionesEditar" name="observacionesEditar" class="form-control"
                                   placeholder="Observaciones">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Monto:</label>
                        <div class="col-lg-9">
                            <input type="text" id="montoEditar" name="montoEditar" class="form-control" placeholder="Monto">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /editar modal -->


    <!-- Danger modal -->
    <div id="modalEliminar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEliminar" class="modal-content" action="/Admin/eliminarcategoria/" method="POST">
                @method('DELETE');
                <!--Importante-->
            @csrf
            <!--Importante-->


                <div class="modal-header bg-danger">
                    <h6 class="modal-title">Desea Eliminar esta Categoría?</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-danger">Eliminar Categoría</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /default modal -->


@endsection



@section('script')
    <script>
        function eliminar(id) {
            // var url = $('#url_editarcategoria').val() + id;
            // console.log(url)
            $("#formEliminar").attr('action', '/Admin/eliminarcategoria/' + id);
        }

        function editar(id) {
            // console.log(id);
            var url = $('#url_editarcategoria').val() + id;
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#nombreEditar').val(data.categoria.nombre);
                    $('#montoEditar').val(data.categoria.monto);
                    $('#periodo_idEditar').val(data.categoria.periodo.id);
                    $('#carrera_idEditar').val(data.categoria.carrera.id);
                    $('#observacionesEditar').val(data.categoria.observacion);
                    $('#idCategoria').val(data.categoria.id);

                    // $("#modalEditar").modal('show');
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }

        $(function () {
            $('#carrera_select').change(function () {
                var id_carrera = $('#carrera_select').val();
                var url = $('#url_getcategoriaxcarrera').val() + id_carrera;
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        $('#tbody_categorias').html('')
                        data.categorias.forEach(element => {

                            if (element.estado === '1') {
                                var est = "<span class='badge badge-success' style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Activo</font></font></span>"
                            } else {
                                var est = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Inactivo</font></font></span>"
                            }
                            var template = "<tr>"
                                // +"<td>"+element.index+"</td>"
                                + "<td>" + element.nombre + "</td>"
                                + "<td>" + element.periodo.descripcion + "</td>"
                                + "<td>" + element.carrera.nombre + "</td>"
                                + "<td>" + element.monto + "</td>"
                                + "<td>" + element.observacion + "</td>"
                                + "<td>" + est + "</td>"
                                + "<td><button class='btn btn-warning' data-toggle=\"modal\" data-target=\"#modalEditar\" onclick='editar("+element.id+")'><i class='fas fa-pen'></i></button><button class='btn btn-danger' data-toggle=\"modal\" data-target=\"#modalEliminar\" onclick='eliminar("+element.id+")'><i class='fas fa-trash'></i></button></td>"
                                + "</tr>"
                            $('#tbody_categorias').append(template)
                        })
                    }
                })
            })
        })
    </script>

@endsection
