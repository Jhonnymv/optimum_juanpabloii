@extends('Admin.Layouts.main_gadmin')
@section('title','Usuarios-modulos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Administración de
        Usuarios
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Administrar Usuario</span>
        </a>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Lista de Usuarios</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Administrar Usuarios</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para administrar usuarios
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_admin">
                            <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Apellidos Y nombres</th>
                                <th>Tipo de Usuario</th>
                                <th>Modulos Asignados</th>
                                <th>Asignar Modulo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarios as $item)
                                <tr>
                                    <td>{{$item->usuario}}</td>
                                    <td>{{$item->persona->paterno. ' ' .$item->persona->materno. ' '. $item->persona->nombres}}</td>
                                    <td>{{$item->tipo}}</td>
                                    <td>@foreach($item->modulos as $modulo)
                                            <span>{{$modulo->nombre}}</span> <br>
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-outline-success" onclick="buscar({{$item->id}})"><i
                                                class="icon-pencil6"> Editar </i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_get_user_id" value="{{route('adminUsuarios_modulo.buscarUsuario',[''])}}/">
@endsection
@section('modals')
    <div id="modalAsignarModulo" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('adminUsuarios_modulo.updatemodulos')}}">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title">Asingnar Módulos</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Usuario:</label>
                        <div class="col-lg-9">
                            <input type="text" name="useriomodulo" id="useriomodulo" class="form-control" required
                                   disabled>
                        </div>
                    </div>
                    <div class="form-group offset-1">
                        <h5 class="font-weight-semibold">Módulos del Sistemas</h5>
                        <h6 style="color: red">Selecione Módulos para asignar al usuario</h6>
                        <div>
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($modulos as $item)
                                        <div class="form-check col-md-6">
                                            <label class="form-check-label">
                                                <input type="checkbox" id="moduloid-{{$item->id}}" name="moduloid[]"
                                                       value="{{$item->id}}">
                                                {{$item->nombre}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Asignar Módulos</button>
                </div>
                <input type="hidden" name="idUsuario" id="idUsuario">
            </form>
        </div>
    </div>
@endsection
@section('script')
    {{--Para Tabla--}}
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_admin').DataTable({

            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
    <script>
        function buscar(id) {
            var ruta = $('#url_get_user_id').val() + id;
            $('#modalAsignarModulo').modal('show');
            $.ajax({
                url: ruta,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    var persona = "";
                    var idpersona = "";
                    $.each(data.usuario, function (index, value) {
                        persona = value.persona.paterno + ' ' + value.persona.materno + ' ' + value.persona.nombres;
                        idpersona = value.persona_id;
                    });
                    $.each(data.modulos, function (index, value) {
                        $('#moduloid-' + value.id).attr('checked', '')
                    })
                    $('#useriomodulo').val(persona)
                    $('#idUsuario').val(idpersona)
                }
            })

        }

    </script>
@endsection

