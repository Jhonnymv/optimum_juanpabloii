@extends('Admin.Layouts.main_gadmin')
@section('title','Usuarios por modulos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Administración de
        Usuarios
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Administrar Usuarios</h5>
                    <div class="header-elements">

                    </div>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a href="#basic-pill1" class="nav-link legitRipple active"
                                                data-toggle="tab" id="li_datos_personales">Datos
                                Personales</a></li>
                        <li class="nav-item"><a href="#basic-pill2" class="nav-link legitRipple " data-toggle="tab"
                                                id="li_permisos">Permisos</a>
                        </li>
                    </ul>
                    <form action="{{route('adminUsuarios_modulo.storeAdminUsuaios')}}" method="POST">
                        @csrf
                        <div class="tab-content">
                            <div class="tab-pane active show" id="basic-pill1">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="dni">DNI:</label>
                                            <input type="text" name="DNI" id="dni" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="paterno">Apellido Paterno:</label>
                                            <input type="text" name="paterno" id="paterno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="materno">Apellido Materno:</label>
                                            <input type="text" name="materno" id="materno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="nombres">Nombres:</label>
                                            <input type="text" name="nombres" id="nombres" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="tipo_usuario">Sexo:</label>
                                            <select name="sexo" id="sexo" class="form-control">
                                                <option value="">Seleccionar Tipo de Usuario</option>
                                                <option value="F">Femenino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                                            <input type="date" name="fecha_nacimiento" id="fecha_nacimiento"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="telefono">Teléfono:</label>
                                            <input type="text" name="telefono" id="telefono" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Email:</label>
                                        <input type="text" name="email" class="form-control" placeholder="Email..."
                                               required>
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-weight: bold; margin: 0">Email Personal:</label>
                                        <input type="text" name="email_personal" class="form-control"
                                               placeholder="Email..." required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento" name="departamento_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia" name="provincia_id">
                                                <option selected>Seleccione una Provincia</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito" name="distrito_id" required>
                                                <option value="" selected>Seleccione un Distrito</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{--                                <div class="row">--}}
                                {{--                                    <div class="col-12 text-center">--}}
                                {{--                                        <button type="button" onclick="validar_d_personales()" class="btn btn-success">Siguiente <i--}}
                                {{--                                                class="icon-arrow-right6"></i></button>--}}
                                {{--                                    </div>--}}

                                {{--                                </div>--}}
                            </div>

                            <div class="tab-pane" id="basic-pill2">
                                <div class="row mt-0">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label class="font-weight-semibold">Módulos de Acceso</label>
                                            <div class="form-check">
                                                @foreach($modulos as $modulo)
                                                    <label class="form-check-label">
                                                        <input type="checkbox" id="modulo_id" name="modulo_id[]"
                                                               class="form-check-input"
                                                               value="{{$modulo->id}}">
                                                        {{$modulo->nombre}}
                                                    </label><br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <label for="tipo_usuario">Tipo de Usuario:</label>
                                                <select name="tipo_usuario" id="tipo_usuario" class="form-control">
                                                    <option value="">Seleccionar Tipo de Usuario</option>
                                                    <option value="docente">Docente</option>
                                                    <option value="alumno">Alumno</option>
                                                    <option value="administrador">Administrador</option>
                                                    <option value="secretaria_academica">Secretaría Académica</option>
                                                    <option value="jefe_area_academica">Jefe de Área Académica</option>
                                                    <option value="padre">Padre</option>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <label for="usuario">Usuario:</label>
                                                <input type="text" name="usuario" id="usuario" class="form-control"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <label for="password">Contraseña:</label>
                                                <input type="text" name="password" id="password" class="form-control"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn-success btn">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_distritos_prov" value="{{route('distrito_prov', [''])}}/">
    <input type="hidden" id="url_provincias_dep" value="{{route('provincia_dep', [''])}}/">
@endsection
@section('modals')
    {{--    <div id="modalAsignarModulo" class="modal fade" tabindex="-1">--}}
    {{--        <div class="modal-dialog">--}}
    {{--            <form class="modal-content" method="POST" action="{{route('adminUsuarios_modulo.updatemodulos')}}">--}}
    {{--                @csrf--}}
    {{--                @method('PUT')--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h5 class="modal-title">Asingnar Módulos</h5>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
    {{--                </div>--}}
    {{--                <div class="modal-body">--}}
    {{--                    <div class="form-group row">--}}
    {{--                        <label class="col-lg-3 col-form-label">Usuario:</label>--}}
    {{--                        <div class="col-lg-9">--}}
    {{--                            <input type="text" name="useriomodulo" id="useriomodulo" class="form-control" required--}}
    {{--                                   disabled>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="form-group offset-1">--}}
    {{--                        <h5 class="font-weight-semibold">Módulos del Sistemas</h5>--}}
    {{--                        <h6 style="color: red">Selecione Módulos para asignar al usuario</h6>--}}
    {{--                        <div>--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-md-12">--}}
    {{--                                    @foreach($modulos as $item)--}}
    {{--                                        <div class="form-check col-md-6">--}}
    {{--                                            <label class="form-check-label">--}}
    {{--                                                <div class="border-primary text-primary"><span--}}
    {{--                                                        class=""><input type="checkbox" name="moduloid[]"--}}
    {{--                                                                        class="form-check-input-styled-primary"--}}
    {{--                                                                        value="{{$item->id}}"></span></div>--}}
    {{--                                                {{$item->nombre}}--}}
    {{--                                            </label>--}}
    {{--                                        </div>--}}
    {{--                                    @endforeach--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="modal-footer">--}}

    {{--                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>--}}
    {{--                    <button type="submit" class="btn bg-primary">Asignar Módulos</button>--}}
    {{--                </div>--}}
    {{--                <input type="hidden" name="idUsuario" id="idUsuario">--}}
    {{--            </form>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection
@section('script')

    {{--    <script>--}}
    {{--        function validar_d_personales() {--}}
    {{--            if ($('#paterno').val()===""||$('#materno').val()===""||$('#nombres').val()===""||$('#dni').val().length !== 8 ||$('#fecha_nacimiento').val()===""){--}}
    {{--                alert('Los campos con (*) son obligatorios')--}}
    {{--            }--}}
    {{--            else {--}}
    {{--                $('#li_datos_personales').removeClass('active')--}}
    {{--                $('#basic-pill1').removeClass('show').removeClass('active')--}}
    {{--                $('#li-permisos').removeClass('fade').addClass('active')--}}
    {{--                $('#basic-pill2').addClass('active').addClass('show')--}}
    {{--                $('#li-permisos').show()--}}
    {{--            }--}}
    {{--        }--}}
    {{--    </script>--}}

    <script>
        $(function () {
            $('#departamento').change(function () {
                $('#distrito').html('');
                $('#distrito').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia').html('');
                        $('#provincia').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito').html('');
                            $('#distrito').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
            $('#dni').keyup(function () {
                if ($(this).val().length === 8) {
                    $.ajax({
                        url: '/searchPerson/' + $(this).val(),
                        type: 'GET',
                        datatype: 'json',
                        success: function (data) {
                            $('#paterno').val(data.apellidoPaterno)
                            $('#materno').val(data.apellidoMaterno)
                            $('#nombres').val(data.nombres)
                        }
                    })
                }
            })
        });
    </script>
@endsection

