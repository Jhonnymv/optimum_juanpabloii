@extends('Admin.Layouts.main_gadmin')
@section('title','Usuarios por modulos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Administración de
        Usuarios
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Administrar Usuarios</h5>
                    <div class="header-elements">

                    </div>
                </div>
                <div class="card-body">
                    <form id="formContraseña" name="formContraseña" action="{{route('updateUsuariosContraseña')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="dni">DNI:</label>
                                        <input type="text" name="DNI" id="DNI" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" id="btnBuscar">Comprobar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="paterno">Apellido Paterno:</label>
                                        <input type="text" name="paterno" id="paterno" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="materno">Apellido Materno:</label>
                                        <input type="text" name="materno" id="materno" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="nombres">Nombres:</label>
                                        <input type="text" name="nombres" id="nombres" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="usuario">Usuario:</label>
                                        <input type="text" name="usuario" id="usuario" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-4" style="display: none" id="div_password">
                                    <div class="form-group">
                                        <label for="password">Nueva Contraseña:</label>
                                        <input type="text" name="password" id="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-4" style="display: none" id="div_repeat_password">
                                    <div class="form-group">
                                        <label for="repeat_password">Repita la Contraseña:</label>
                                        <input type="text" name="repeat_password" id="repeat_password"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="idUsuario" name="idUsuario">
                            <div>
                                <button type="button" onclick="comprobar_contraseña()" class="btn-success btn">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_buscarxdni" value="{{route('buscarUsuarioxDni', [''])}}/">
@endsection
@section('script')
    <script>
        $(function () {
            $('#btnBuscar').click(function () {
                var dniBuscar = $('#DNI').val();
                var url = $('#url_buscarxdni').val() + dniBuscar;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        if (data !== null) {
                            $('#idUsuario').val(data.usuario.id);
                            $('#nombres').val(data.usuario.persona.nombres);
                            $('#paterno').val(data.usuario.persona.paterno);
                            $('#materno').val(data.usuario.persona.materno);
                            $('#usuario').val(data.usuario.usuario);

                            $('#div_password').show();
                            $('#div_repeat_password').show();
                        } else {
                            console.log(data)
                            alert('El usuario con dni: ' + +'no existe.')
                        }
                    }
                })
            })
        })

        function comprobar_contraseña() {
            if($('#password').val() !== $('#repeat_password').val() || $('#repeat_password').val() !== $('#password').val()){
                alert('Las Contraseñas no coinciden')
            }
            else{
                $('#formContraseña').submit();
            }
        }

    </script>
@endsection

