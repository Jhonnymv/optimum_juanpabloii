@extends('Perfil.mainPerfil')
@section('title','Perfil')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Perfil</span>
        - {{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</h4>
@endsection

@section('style')
    <style>
        .imgprev {
            width: 125px;
            height: 125px;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <form id="form_data" name="formAdmision" action="" method="POST" class="form-group"
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row" style="padding-top: 25px">
                            <div class="col-12 col-md-12">
                                <div class="file-field">
                                    <div class="mb-4"
                                         style="display: flex; flex-direction: column; justify-content: center; align-items: center">
                                        <div class="foto">
                                            <a onclick="modalfoto()" style="cursor: pointer">
                                                <img id="imgprev" src="{{asset($usuario->persona->urlimg)}}"
                                                     class="img-fluid rounded-circle shadow-1 mb-3 imgprev" width="125"
                                                     height="125"
                                                     alt="" onclick="modalfoto()">
                                                <label onclick="modalfoto()" style="font-weight: bold; font-size: medium">Cambiar Foto</label>
                                            </a>
                                        </div>
                                        <h6 class="mb-0 text-black-100 text-shadow-dark">{{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</h6>
                                        <span class="font-size-sm text-black-100 text-shadow-dark"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 25px">
                            <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 style="font-weight: bold" class="card-title text-center">Datos
                                                Personales</h5>
                                        </div>
                                        <div class="card-body" style="margin-left: 1em">
                                            <div class="col-12 col-md-12">
                                                <strong for="">Nombres y Apellidos: </strong>
                                                <label
                                                    for="">{{$usuario->persona->nombres}} {{$usuario->persona->paterno}} {{$usuario->persona->materno}}</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <strong for="">E-mail Personal: </strong>
                                                <label for="">{{$usuario->persona->email_personal}}</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <strong for="">Dirección: </strong>
                                                <label for="">{{$usuario->persona->direccion}}</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <strong for="">Dni: </strong>
                                                <label for="">{{$usuario->persona->DNI}}</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <strong for="">Teléfono: </strong>
                                                <label for="">{{$usuario->persona->telefono}}</label>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class=""
                                                 style="display: flex; flex-direction: row; justify-content: center">
                                                <button type="button" class="btn btn-success"
                                                        onclick="modaldatospersonales()">Actualizar Datos
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 style="font-weight: bold" class="card-title text-center">Datos de
                                                Usuario</h5>
                                        </div>
                                        <div class="card-body" style="margin-left: 1em">
                                            <div class="col-12 col-md-12">
                                                <strong for="">E-mail Corporativo: </strong>
                                                <label for="">{{$usuario->persona->email}}</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <strong for="">Tipo de Usuario: </strong>
                                                <label for="">{{$usuario->tipo}}</label>
                                            </div>
                                            <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                                <label class="form-control col-md-2"
                                                       style="margin: 0; font-weight: bold">Usuario: </label>
                                                <label class="form-control"
                                                       style="margin: 0">{{$usuario->usuario}}</label>
                                            </div>
                                            <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                                <label class="form-control col-md-2"
                                                       style="margin: 0; font-weight: bold">Contraseña: </label>
                                                <input class="form-control" type="password"
                                                       value="{{$usuario->password}}" disabled>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class=""
                                                 style="display: flex; flex-direction: row; justify-content: center">
                                                <button type="button" class="btn btn-success"
                                                        onclick="modalcambiarcontraseña()">Cambiar Contraseña
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('modals')
    <div id="modalFotoPerfil" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('updateFotoPerfil')}}"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-header" style="display: flex; flex-direction: row; justify-content: center">
                    <h5 class="modal-title">Foto de Perfil</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div
                                style="display: flex; flex-direction: column; justify-content: center; align-items: center">
                                <div class="mb-4"
                                     style="margin: 0; display: flex; flex-direction: column; justify-content: center; align-items: center">
                                    <img id="urlimg" src="{{asset($usuario->persona->urlimg)}}"
                                         style="width: 75%; height: 75%; margin: 0"
                                         class="shadow-1 mb-3" width="500" height="500" alt="">
                                </div>
                                <div class="col-md-12">
                                    <div class="" style="display: flex; flex-direction: row; justify-content: center">
                                        <div class="col-md-4" style="display: flex; justify-content: flex-end">
                                            <button type="button" class="btn btn-success" style="margin-right: 1em">
                                                <label style="cursor: pointer; margin: 0" for="file-upload"
                                                       class="custom-file-upload">Elegir Foto</label>
                                                <input id="file-upload" name="urlimg" type="file" style="display: none"
                                                       onchange="previewFile()">
                                            </button>
                                        </div>
                                        <div class="col-md-4" style="display: flex; justify-content: end">
                                            <button style="margin-left: 1em" type="submit" class="btn btn-success">
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </form>
        </div>
    </div>

    <div id="modalDatosPersonales" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('updateDatosPersonales')}}">
                @csrf
                @method('PUT')
                <div class="modal-header" style="display: flex; flex-direction: row; justify-content: center">
                    <h5 class="modal-title">Actualizar Datos Personales</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="card-body" style="margin-left: 1em">
                                <div class="row">
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="nombres"
                                               style="margin: 0; font-weight: bold">Nombres: </label>
                                        <input class="form-control" id="nombres" name="nombres"
                                               value="{{$usuario->persona->nombres}}">
                                    </div>
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="paterno"
                                               style="margin: 0; font-weight: bold">Apellido Paterno: </label>
                                        <input class="form-control" id="paterno" name="paterno"
                                               value="{{$usuario->persona->paterno}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="materno"
                                               style="margin: 0; font-weight: bold">Apellido Materno: </label>
                                        <input class="form-control" id="materno" name="materno"
                                               value="{{$usuario->persona->materno}}">
                                    </div>
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="email_personal"
                                               style="margin: 0; font-weight: bold">Email Personal: </label>
                                        <input class="form-control" id="email_personal" name="email_personal"
                                               value="{{$usuario->persona->email_personal}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="direccion"
                                               style="margin: 0; font-weight: bold">Dirección: </label>
                                        <input class="form-control" id="direccion" name="direccion"
                                               value="{{$usuario->persona->direccion}}">
                                    </div>
                                    <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                        <label class="form-control col-md-3" for="telefono"
                                               style="margin: 0; font-weight: bold">Teléfono: </label>
                                        <input class="form-control" id="telefono" name="telefono"
                                               value="{{$usuario->persona->telefono}}">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-md-12">
                                    <div class="" style="display: flex; flex-direction: row; justify-content: center">
                                        <button style="margin-left: 1em" type="submit" class="btn btn-success">Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="modalCambiarContraseña" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" id="formContraseña" method="POST" action="{{route('updateContraseña')}}">
                @csrf
                @method('PUT')
                <div class="modal-header" style="display: flex; flex-direction: row; justify-content: center">
                    <h5 class="modal-title">Cambiar Contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="card-body" style="margin-left: 1em">
                                <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                    <label class="form-control col-md-3" for="contraseña"
                                           style="margin: 0; font-weight: bold">Contraseña Actual: </label>
                                    <input type="password" class="form-control" id="contraseña" name="contraseña"
                                           value="{{$usuario->password}}" disabled>
                                </div>

                                <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                    <label class="form-control col-md-3" for="password"
                                           style="margin: 0; font-weight: bold">Nueva Contraseña: </label>
                                    <input class="form-control" id="password" name="password" type="password">
                                </div>
                                <div class="col-12 col-md-12" style="display: flex; flex-direction: row">
                                    <label class="form-control col-md-3" for="repeat_password"
                                           style="margin: 0; font-weight: bold">Repetir Contraseña: </label>
                                    <input class="form-control" id="repeat_password" name="repeat_password"
                                           type="password">
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-md-12">
                                    <div class="" style="display: flex; flex-direction: row; justify-content: center">
                                        <button type="button" onclick="comprobar_contraseña()" class="btn btn-success">
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('script')
    <script>
        function previewFile() {
            var preview = document.querySelector('#urlimg');
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();
            console.log(preview, file, reader);
            reader.onloadend = function () {
                preview.src = reader.result;
            };
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }

        function modalfoto() {
            $('#modalFotoPerfil').modal('show');
        }

        function modaldatospersonales() {
            $('#modalDatosPersonales').modal('show');
        }

        function modalcambiarcontraseña() {
            $('#modalCambiarContraseña').modal('show');
        }

        function comprobar_contraseña() {
            if ($('#password').val() !== $('#repeat_password').val() || $('#repeat_password').val() !== $('#password').val()) {
                alert('Las Contraseñas no coinciden')
            } else {
                $('#formContraseña').submit();
            }
        }

    </script>
@endsection
