<div>
    <div class="card">
        <div class="card-header">
            <h4>Reporte de pagos por mes</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label for="periodoId">Periodo</label>
                    <select name="periodoId" id="periodoId" wire:model="periodoId" class="form-control">
                        @foreach($periodos as $periodo)
                            <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-12 col-md-4">
                    <label for="month">Mes</label>
                    <select name="mes" id="month" wire:model="month" class="form-control">
                        <option>Seleccione un mes</option>
                        @foreach($months as $month)
                            <option value="{{$month['value']}}">{{$month['text']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card bg-blue-300">
                        <div class="card-body mt-2">
                            <strong style="font-size: 20px">Total:</strong>
                            <span style="font-size: 20px">S/ {{number_format($data->sum('fe_total_price'),2)}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                @if($data)
                    <button type="button" wire:click="export">Descargar</button>
                @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Apoderado</th>
                            <th>Comprobante</th>
                            <th>Concepto</th>
                            <th>Monto</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data as $loop=>$item)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{$item->persona->DNI.' - '.$item->persona->paterno.' '.$item->persona->materno.' '.$item->persona->nombres}}</td>
                                <td>{{$item->serie.'-'.$item->numero_doc}}</td>
                                <td>{{$item->alPagos->first()->cronograma->concepto->nombre}}</td>
                                <td>{{$item->fe_total_price}}</td>
                                <td>{{$item->created_at->format('d-m-Y')}}</td>
                                <td>
                                    <a target="_blank" href="{{route('get_pdf_factura',['pago_id'=>$item->id])}}"><i
                                            class="fa fa-print"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" style="background-color: darkgray; text-align: center">No se encuentran
                                    Registros
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
