<div>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class=" col-12 col-md-4">
                    <label for="noteType">Tipo de Nota:</label>
                    <select name="notetype" id="noteType" class="form-control" wire:model="noteType">
                        <option value="07">Nota de Crédito</option>
                        <option value="08">Nota de Débito</option>
                    </select>
                </div>
                <div class="col-12 col-md-4">
                    <label for="docAfec">Documento Afectado:</label>
                    <input type="text" name="docAfec" id="docAfec" wire:model="docAfec" class="form-control">
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-dark" wire:click="searchComp()"><i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <label for="tipoDoc">Tipo Documento Afectado</label>
                    <input type="text" name="tipoDoc" id="tipoDoc" wire:model="tipoDoc" class="form-control" readonly>
                </div>
                <div class="col-12 col-md-4">
                    <label for="serie">Serie Documento Afectado</label>
                    <input type="text" name="serie" id="serie" wire:model="serie" class="form-control" readonly>
                </div>
                <div class="col-12 col-md-4">
                    <label for="correlativo">Correlativo</label>
                    <input type="text" name="correlativo" id="correlativo" wire:model="correlativo" class="form-control"
                           readonly>
                </div>
            </div>
            <h6>Cliente</h6>
            <div class="row">
                <div class="col-12 col-md-4">
                    <label for="cliTypeDoc">Tipo Documento Cliente</label>
                    <input type="text" name="cliTypeDoc" id="cliTypeDoc" wire:model="cliTypeDoc" class="form-control"
                           readonly>
                </div>
                <div class="col-12 col-md-4">
                    <label for="cliDoc">Documento Cliente</label>
                    <input type="text" name="cliDoc" id="cliDoc" wire:model="cliDoc" class="form-control" readonly>
                </div>
                <div class="col-12 col-md-4">
                    <label for="cliNombre">Nombre Cliente</label>
                    <input type="text" name="cliNombre" id="cliNombre" wire:model="cliNombre" class="form-control"
                           readonly>
                </div>
            </div>
            <h6>Detalles</h6>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--                     @if($detalles)--}}
                    @forelse($detalles as $detalle)
                        <tr>
                            <td>{{$detalle['orden']}}</td>
                            <td>{{$detalle['detalle']}}</td>
                            <td>{{$detalle['precio']}}</td>
                            <td>{{$detalle['cantidad']}}</td>
                            <td>{{$detalle['total']}}</td>
                        </tr>
                    @empty
                    @endforelse
                    <tr>
                        <td colspan="4" class="text-right">Total</td>
                        <td>{{number_format($detalles->sum('total'),2)}}</td>
                    </tr>
                    {{--                         @endif--}}
                    </tbody>
                </table>
            </div>
            <div class="row">
                @if (session()->has('message_danger'))
                    <div class="alert alert-danger">
                        {{ session('message_danger') }}
                    </div>
                @endif
            </div>
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
            <div class="row">
                <button type="button" class="btn btn-primary" wire:click="sendNote"><i class="fa fa-save"></i>Generar
                    Nota de Crédito
                </button>
            </div>


        </div>
    </div>
</div>
