<div>
     <div class="card" style="height: 100%">
         <div class="card-header">
             <h5>Iframe</h5>
         </div>
         <div class="card-body">
             <iframe src="{{$url}}" frameborder="0" style="width: 100%; height: 100em"></iframe>
         </div>
     </div>
</div>
