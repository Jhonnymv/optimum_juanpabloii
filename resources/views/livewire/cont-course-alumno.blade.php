<div>
    <style>
        ul, #myUL {
            list-style-type: none;
        }

        /* Remove margins and padding from the parent ul */
        #myUL {
            margin: 0;
            padding: 0;
        }

        /* Style the caret/arrow */
        .caret {
            cursor: pointer;
            user-select: none; /* Prevent text selection */
        }

        /* Create the caret/arrow with a unicode, and style it */
        .caret::before {
            content: "\25B6";
            color: black;
            display: inline-block;
            margin-right: 6px;
        }

        /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
        .caret-down::before {
            transform: rotate(90deg);
        }

        /* Hide the nested list */
        .nested {
            display: none;
        }

        /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
        .active {
            display: block;
        }
    </style>
    <div class="card" wire:poll>
        <div class="card-header" style="background-color: #0aa7ef">
            <label>Courso: </label><strong> {{$seccion->pe_curso->curso->nombre.' - '.$seccion->pe_curso->pe_carrera->carrera->nombre}}</strong>
        </div>
        <div class="card-body">
            @forelse($fases as $loop=>$fase)
                <ul id="myUL">
                    <li><span class="caret">{{$fase->num_fase.' - '.$fase->denominacion}}</span>
                        <ul class="nested active">
                            @foreach($units->where('fase_id',$fase->id) as $unit)
                                <li><span class="caret">{{$unit->denominacion}}</span>
                                    <ul class="nested active">
                                        @foreach($unit->temas as $tema)
                                            <li><span class="caret">{{$tema->nombre}}</span>
                                                <ul class="nested active">
                                                    @forelse($tema->materialesApoyo->where('startDate','<=',$now)->where('endDate','>=',$now) as $loop=>$material)
                                                        @if($material->url_doc)
                                                            <a target="_blank" class="ml-3"
                                                               href="{{route('frame_contends',['materialId'=>$material->id,'type'=>'doc'])}}">{{$material->nombre.' Documento'}}
                                                                <i class="fa fa-file"></i></a>
                                                            /
                                                        @endif
                                                        @if($material->link)
                                                            <a target="_blank" class="ml-3"
                                                               {{--                                                               href="{{route('frame_contends',['materialId'=>$material->id,'type'=>'link'])}}">{{$material->nombre.' Link'}}</a>--}}
                                                               href="{{$material->link}}">{{$material->nombre.' Link'}}
                                                                <i class="fa fa-link"></i></a>
                                                        @endif
                                                        @if($material->pin)
                                                            / <span>{{$material->pin}} <i
                                                                    class="fa fa-key"></i></span>
                                                        @endif
                                                        <br>
                                                    @empty
                                                    @endforelse
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            @empty
                <marquee behavior="" direction="left">No data</marquee>
            @endforelse
        </div>
    </div>
    <script !src="">
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function () {
                this.parentElement.querySelector(".nested").classList.toggle("active");
                this.classList.toggle("caret-down");
            });
        }
    </script>
</div>
