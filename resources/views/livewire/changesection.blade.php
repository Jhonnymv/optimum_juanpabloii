<div>
    <div class="card">
        <div class="card-header">
            <h6>Cambio de sección de alumnos</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label for="alDni">DNI del estudiante:</label>
                    <input type="text" id="alDni" wire:model="alDni" class="form-control">
                </div>
                <div class="col-12 col-md-1">
                    <button class="btn btn-info mt-4" wire:click="getAlumno"><i class="fa fa-search"></i></button>
                </div>
                @if($alumno)
                    <div class="col-12 col-md-7">
                        <label for="alDatos">Datos del alumno:</label>
                        <input type="text" name="alDatos" id="alDatos" wire:model="alDatos" readonly
                               class="form-control">
                    </div>
                    <div class="col-12 col-md-3">
                        <label for="levelId">Nivel:</label>
                        <select name="levelID" id="levelId" wire:model="levelId" class="form-control">
                            @foreach($levels as $level)
                                <option value="{{$level->id}}">{{$level->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-md-3">
                        <label for="levelId">Grado:</label>
                        <select name="levelID" id="levelId" wire:model="gradeId" class="form-control">
                            @foreach($grades as $grade)
                                <option value="{{$grade->id}}">{{$grade->grade}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-md-3">
                        <label for="levelId">Sección:</label>
                        <select name="levelID" id="levelId" wire:model="sectionId" class="form-control">
                            @foreach($sections as $section)
                                <option value="{{$section}}">{{$section}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-md-3">
                        <button type="button" class="btn btn-primary mt-4" wire:click="store">Guardar</button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
