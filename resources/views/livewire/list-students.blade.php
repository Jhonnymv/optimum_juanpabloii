<div>
    <div class="card">
        <div class="card-header">
            <h6>Lista de estudiantes</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label for="sectionId">Secciones:</label>
                    <select name="sectionId" id="sectionId" wire:model="sectionId" class="form-control">
                        <option>Seleccione un sección para mostrar los estudiantes</option>
                        @forelse($sections as $section)
                            <option
                                value="{{$section->id}}">{{$section->pe_curso->curso->nombre.' - '.$section->grupo}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Apellidos y nombres</th>
                            <th>Nivel</th>
                            <th>grado/sección</th>
                            <th>Apoderado</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($students as $loop=>$student)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{$student['student']}}</td>
                                <td>{{$student['nivel']}}</td>
                                <td>{{$student['graSec']}}</td>
                                <td>{{$student['apoderado']}}</td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
