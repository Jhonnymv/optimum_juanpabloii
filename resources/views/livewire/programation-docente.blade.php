<div>
    <div class="card">
        <div class="card-header" style="background-color: #0aa7ef">
            <h4>Registro de programación del periodo <strong>{{$seccion->periodo->descripcion}}</strong></h4>
            <div>
                @if (session()->has('message'))
                    <br>
                    <div class="alert alert-danger">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    <label for="metodologia">Enfoque</label>
                    @error('metodologia') <span class="text-danger">{{ $message }}</span>@enderror
                    <textarea name="metodologia" id="metodologia" rows="1" wire:model="metodologia"
                              class="form-control"></textarea>
                </div>
                <div class="col-12 col-md-6">
                    <label for="descripcion">Descripción</label>
                    @error('metodologia') <span class="text-danger">{{ $message }}</span>@enderror
                    <textarea name="descripcion" id="descripcion" rows="1" wire:model="descripcion"
                              class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 form-group">
                    <label for="nombreClase">Nombre Link de Meet:</label>
                    @error('nombreClase') <span class="text-danger">{{ $message }}</span>@enderror
                    <input type="text" wire:model="nombreClase" class="form-control">
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="linkClase">Link:</label>
                    @error('linkClase') <span class="text-danger">{{ $message }}</span>@enderror
                    <input type="text" wire:model="linkClase" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if($programationId)
                        @forelse($programation->documentos as $documento)
                            @if($documento->url)
                                <a target="_blank" href="{{asset($documento->url)}}">{{$documento->nombre}} <i
                                        class="fa fa-file"></i></a> //
                            @endif
                            @if($documento->link)
                                <a target="_blank" href="{{$documento->link}}">{{$documento->nombre}} <i
                                        class="fa fa-link"></i></a>
                            @endif
                            <i class="fa fa-trash ml-3 text-danger" style="cursor: pointer"
                               wire:click="removeDocProgra({{$documento->id}})"></i>
                            <br>
                        @empty
                        @endforelse
                    @endif
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12 col-md-4 form-group">
                    <label for="document">Documento:</label>
                    <input type="text" wire:model="documentNames.0" class="form-control">
                </div>
                <div class="col-12 col-md-4 form-group">
                    <label for="document">Documento:</label>
                    <input type="file" wire:model="documents.0" class="form-control">
                    <div wire:loading wire:target="documents.0">Espere. Cargando...</div>
                </div>
                <div class="col-12 col-md-3 form-group">
                    <label for="link">Link:</label>
                    <input type="text" wire:model="links.0" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-success mt-4" wire:click="add({{$i}})">+</button>
                </div>
            </div>
            @foreach($inputs as $key=>$value)
                <div class="row">
                    <div class="col-12 col-md-4 form-group">
                        <label for="document">Nombre Documento:</label>
                        <input type="text" wire:model="documentNames.{{$value}}" class="form-control">
                    </div>
                    <div class="col-12 col-md-4 form-group">
                        <label for="document">Documento adicional:</label>
                        <input type="file" wire:model="documents.{{$value}}">
                        <div wire:loading wire:target="documents.{{$value}}">Espere. Cargando...</div>
                    </div>
                    <div class="col-12 col-md-3 form-group">
                        <label for="link">Link:</label>
                        <input type="text" wire:model="links.{{$value}}" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-danger btn-sm" wire:click.prevent="remove({{$key}})">-</button>
                    </div>
                </div>
            @endforeach
            {{--            <div class="row">--}}
            {{--                <div class="col-12">--}}
            {{--                    <label for="bibliografia">Bibliografía</label>--}}
            {{--                    <textarea name="bibliografia" id="bibliografia" rows="1" wire:model="bibliografia"--}}
            {{--                              class="form-control"></textarea>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-12" style="text-align: center">
                    <button type="button" class="btn btn-primary"
                            wire:click="storeProgramation">{{$programationId==null?"REGISTRAR":'EDITAR'}} PROGRAMACIÓN
                    </button>
                </div>
            </div>
        </div>
    </div>
    @if($programationId)
        <div class="card" id="secUnits">
            <div class="card-header">
                <strong>Unidades</strong>
                <button class="btn btn-success" data-toggle="modal" data-target="#modal_unit">Nueva Unidad</button>
            </div>
            <div class="card-body">
                <div class="accordion-sortable ui-sortable" id="accordion-controls">
                    @forelse($units as $loop=>$unit)
                        <div class="card">
                            <div class="card-header header-elements-inline" style="background-color: #00bcd4">
                                <h6 class="card-title">
                                    <a data-toggle="collapse" class="text-default"
                                       href="#accordion-controls-group{{$loop->index+1}}">
                                        <strong>Fase: </strong>{{$unit->fase->denominacion}} &nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong>Denominación: </strong>{{$unit->denominacion.' ('.$unit->fecha_inicio.' / '.$unit->fecha_fin.' )'}}
                                        <strong>N° {{$unit->unidad}}</strong></a>
                                    <button type="button" class="btn btn-warning btn-sm"
                                            wire:click="editUnidad({{$unit->id}})" data-toggle="modal"
                                            data-target="#modal_unit">Editar Unidad
                                    </button>
                                </h6>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="reload"></a>
                                        <a class="list-icons-item" data-action="fullscreen"></a>
                                    </div>
                                </div>
                            </div>

                            <div id="accordion-controls-group{{$loop->index+1}}" class="collapse show"
                                 data-parent="#accordion-controls">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <p>{{$unit->descripcion}}</p>
                                            <p>Fecha Inicio: <span>{{$unit->fecha_inicio}}</span></p>
                                            <p>Fecha Fin: <span>{{$unit->fecha_fin}}</span></p>
                                            <p>Duración: <span>{{$unit->duracion}}</span></p>
                                            <hr>
                                            <a class="btn btn-success btn-sm"
                                               href="{{route('temaDocente',['unidadId'=>$unit->id])}}">Agregar
                                                sesiones
                                            </a>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <h4>Temas</h4>
                                            @forelse($unit->temas as $tema)
                                                <h6 class="ml-1">{{$tema->nombre}}
                                                <a class="text-warning" href="{{route('temaDocente',['unidadId'=>$unit->id,'temaId'=>$tema->id])}}"><i class="fa fa-edit"></i>
                                                </a>
                                                </h6>
                                                @forelse($tema->materialesApoyo as $material)
                                                    @if($material->url_doc)
                                                        <a target="_blank" class="ml-3"
                                                           href="{{asset($material->url_doc)}}">{{$material->nombre.' Documento'}}
                                                            <i class="fa fa-file"></i></a> /
                                                    @endif
                                                    @if($material->link)
                                                        <a target="_blank" class="ml-3"
                                                           href="{{asset($material->link)}}">{{$material->nombre.' Link'}}
                                                            <i class="fa fa-link"></i></a>
                                                    @endif
                                                    @if($material->pin)
                                                        /<span>{{$material->pin}}</span>
                                                    @endif
                                                    <br>
                                                    <span class="ml-4"
                                                          style="color: grey">{{$material->startDate}} - {{$material->endDate}}</span>
                                                    <br>
                                                @empty
                                                @endforelse
                                            @empty
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <marquee behavior="" direction="left">No data</marquee>
                    @endforelse
                </div>
            </div>
        </div>
    @endif
    <div wire:ignore.self id="modal_unit" class="modal fade" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Unidad</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12 col-md-3">
                            <label for="unitUnidad">N° Unidad</label>
                            <input type="text" name="unitUnidad" id="unitUnidad" wire:model="unitUnidad"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-9">
                            <label for="unitFaseId">Fase</label>
                            <select name="unitFaseId" id="unitFaseId" wire:model="unitFaseId" class="form-control">
                                @forelse($fases as $fase)
                                    <option
                                        value="{{$fase->id}}">{{$fase->denominacion.' ('.$fase->fecha_inicio.' / '.$fase->fecha_fin.' )'}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <label for="unitDenomination">Denominación</label>
                            <textarea name="unitDenomination" id="unitDenomination" rows="1"
                                      wire:model="unitDenomination" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 col-md-4">
                            <label for="unitFechaInicio">Fecha Inicio</label>
                            <input type="date" name="unitFechaInicio" id="unitFechaInicio"
                                   wire:model="unitFechaInicio"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="unitFechaFin">Fecha fin</label>
                            <input type="date" name="unitFechaFin" id="unitFechaFin" wire:model="unitFechaFin"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="unitDuracion">duración</label>
                            <input type="text" name="unitDuracion" id="unitDuracion" wire:model="unitDuracion"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-primary legitRipple" wire:click="saveUnit" data-dismiss="modal">
                        Save changes
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
