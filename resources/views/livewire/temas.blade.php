<div>
    <div class="card">
        <div class="card-header">
            <h6>Unidad: {{$unidad->denominacion}} ... Fase: {{$unidad->fase->denominacion}}</h6>
        </div>
        <div class="card-body">
            <h5>Datos Generales</h5>
            <div class="row form-group">
                <div class="col-12">
                    {{--                <div class="col-12 col-md-6">--}}
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="nombre" id="nombre" wire:model="nombre"
                           class="form-control">
                    @error('nombre')<label class="badge badge-danger">{{ $message }}</label>@enderror
                </div>
                <div class="col-12">
                    @if($tema)
                        @forelse($tema->materialesApoyo as $material)
                            @if($material->url_doc)
                                <a target="_blank" class="ml-3"
                                   href="{{asset($material->url_doc)}}">{{$material->nombre.' Documento'}}
                                    <i class="fa fa-file"></i></a> /
                            @endif
                            @if($material->link)
                                <a target="_blank" class="ml-3"
                                   href="{{asset($material->link)}}">{{$material->nombre.' Link'}}
                                    <i class="fa fa-link"></i></a>
                            @endif
                            @if($material->pin)
                                /<span>{{$material->pin}}</span>
                            @endif
                                <i class="fa fa-trash ml-3 text-danger" style="cursor: pointer"
                                   wire:click="removeMatApoyo({{$material->id}})"></i>
                                <br>
                            <span class="ml-4"
                                  style="color: grey">{{$material->startDate}} - {{$material->endDate}}</span>
                            <br>
                        @empty
                        @endforelse
                    @endif
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="d-block font-weight-semibold">Seleccione Materiales de Apoyo</label>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="1" wire:model="chkL"  value="1" checked="">
                            <label class="custom-control-label" for="1">Lectura</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" wire:model="chkV" value="2" id="2">
                            <label class="custom-control-label" for="2">Videos</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" wire:model="chkQ" value="3" id="3">
                            <label class="custom-control-label" for="3">Lección Quizizz</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" wire:model="chkT"  value="4" id="4">
                            <label class="custom-control-label" for="4">Trabajo Colaborativo</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <button class="btn btn-success mt-4" wire:click="add({{$iC}})">Mat. Compl.</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <h5>Materiales de apoyo</h5>
            <div class="row" style="background-color: #d4e6f1; display: {{$chkL==1?'':'none'}}">
                <h4 class="table font-weight-semibold">Lectura</h4><br>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Nombre Documento:</label>
                    <input type="text" wire:model="documentNames.0" class="form-control">
                    @error('documentNames.0')<label class="badge badge-danger">{{ $message }}</label>@enderror
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Documento:</label>
                    <input type="file" wire:model="documents.0"
                           class="form-control">
                    <div wire:loading wire:target="documents.0">Espere. Cargando...</div>
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Link:</label>
                    <input type="text" wire:model="documentLinks.0"
                           class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">Desde:</label>
                            <input type="date" name="startDate0" id="startDate0" wire:model="startDate.0"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">hora</label>
                            <input type="text" name="startTime0" id="startTime0" wire:model="startTime.0"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endDate0">Hasta:</label>
                            <input type="date" name="endDate0" id="endDate0" wire:model="endDate.0"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endTime0">hora</label>
                            <input type="text" name="endTime0" id="endTime0" wire:model="endTime.0"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="background-color: #ebf5fb; display: {{$chkV==2?'':'none'}}">
                <h4 class="table font-weight-semibold">Video</h4>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Nombre Video:</label>
                    <input type="text" wire:model="documentNames.1" class="form-control">
                    @error('documentNames.1')<label class="badge badge-danger">{{ $message }}</label>@enderror
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Documento:</label>
                    <input type="file" wire:model="documents.1"
                           class="form-control">
                    <div wire:loading wire:target="documents.1">Espere. Cargando...</div>
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Link:</label>
                    <input type="text" wire:model="documentLinks.1"
                           class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">Desde:</label>
                            <input type="date" name="startDate0" id="startDate0" wire:model="startDate.1"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">hora</label>
                            <input type="text" name="startTime0" id="startTime0" wire:model="startTime.1"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endDate0">Hasta:</label>
                            <input type="date" name="endDate0" id="endDate0" wire:model="endDate.1"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endTime0">hora</label>
                            <input type="text" name="endTime0" id="endTime0" wire:model="endTime.1"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="background-color:  #d4efdf; display: {{$chkQ==3?'':'none'}}">
                <h6 class="table font-weight-semibold">Lección Quizizz</h6>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Nombre Documento:</label>
                    <input type="text" wire:model="documentNames.2" class="form-control">
                    @error('documentNames.2')<label class="badge badge-danger">{{ $message }}</label>@enderror
                </div>
                {{--                <div class="col-12 col-md-3 form-group">--}}
                {{--                    <label for="document">Documento:</label>--}}
                {{--                    <input type="file" wire:model="documents.2"--}}
                {{--                           class="form-control">--}}
                {{--                </div>--}}
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Link:</label>
                    <input type="text" wire:model="documentLinks.2"
                           class="form-control">
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">PIN:</label>
                    <input type="text" wire:model="documentPins.2"
                           class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">Desde:</label>
                            <input type="date" name="startDate0" id="startDate0" wire:model="startDate.2"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">hora</label>
                            <input type="text" name="startTime0" id="startTime0" wire:model="startTime.2"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endDate0">Hasta:</label>
                            <input type="date" name="endDate0" id="endDate0" wire:model="endDate.2"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endTime0">hora</label>
                            <input type="text" name="endTime0" id="endTime0" wire:model="endTime.2"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="background-color:  #eaeded; display: {{$chkT==4?'':'none'}}">
                <h6 class="table font-weight-semibold">Trabajo Colaborativo</h6>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Nombre Documento:</label>
                    <input type="text" wire:model="documentNames.3" class="form-control">
                    @error('documentNames.3')<label class="badge badge-danger">{{ $message }}</label>@enderror
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Documento:</label>
                    <input type="file" wire:model="documents.3"
                           class="form-control">
                    <div wire:loading wire:target="documents.3">Espere. Cargando...</div>
                </div>
                <div class="col-12 col-md-6 form-group">
                    <label for="document">Link:</label>
                    <input type="text" wire:model="documentLinks.3"
                           class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">Desde:</label>
                            <input type="date" name="startDate0" id="startDate0" wire:model="startDate.3"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="startDate0">hora</label>
                            <input type="text" name="startTime0" id="startTime0" wire:model="startTime.3"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endDate0">Hasta:</label>
                            <input type="date" name="endDate0" id="endDate0" wire:model="endDate.3"
                                   class="form-control"
                                   placeholder="{{now()}}">
                        </div>
                        <div class="col-12 col-md-3 form-group">
                            <label for="endTime0">hora</label>
                            <input type="text" name="endTime0" id="endTime0" wire:model="endTime.3"
                                   class="form-control"
                                   placeholder="{{date('H:i')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
{{--                <button class="btn btn-success mt-4" wire:click="add({{$i}})">Mat. Compl.</button>--}}
            </div>
            @foreach($inputsC as $key=>$value)
                <div class="row">
                    <div class="col-12 col-md-6 form-group">
                        <label for="document">Nombre Documento:</label>
                        <input type="text" wire:model="documentNamesC.{{$value}}" class="form-control">
                    </div>
                    <div class="col-12 col-md-6 form-group">
                        <label for="document">Documento:</label>
                        <input type="file" wire:model="documentsC.{{$value}}"
                               class="form-control">
                        <div wire:loading wire:target="documentsC.{{$value}}">Espere. Cargando...</div>
                    </div>
                    <div class="col-12 col-md-6 form-group">
                        <label for="document">Link:</label>
                        <input type="text" wire:model="documentLinksC.{{$value}}"
                               class="form-control">
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-12 col-md-3 form-group">
                                <label for="startDate0">Desde:</label>
                                <input type="date" name="startDate0" id="startDate0" wire:model="startDateC.{{$value}}"
                                       class="form-control"
                                       placeholder="{{now()}}">
                            </div>
                            <div class="col-12 col-md-3 form-group">
                                <label for="startDate0">hora</label>
                                <input type="text" name="startTime0" id="startTime0" wire:model="startTimeC.{{$value}}"
                                       class="form-control"
                                       placeholder="{{date('H:i')}}">
                            </div>
                            <div class="col-12 col-md-3 form-group">
                                <label for="endDate0">Hasta:</label>
                                <input type="date" name="endDate0" id="endDate0" wire:model="endDateC.{{$value}}"
                                       class="form-control"
                                       placeholder="{{now()}}">
                            </div>
                            <div class="col-12 col-md-3 form-group">
                                <label for="endTime0">hora</label>
                                <input type="text" name="endTime0" id="endTime0" wire:model="endTimeC.{{$value}}"
                                       class="form-control"
                                       placeholder="{{date('H:i')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-danger btn-sm" wire:click.prevent="remove({{$key}})"><i
                                class="fa fa-trash"></i></button>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary" wire:click="store">Guardar Datos</button>
        </div>
    </div>
</div>
