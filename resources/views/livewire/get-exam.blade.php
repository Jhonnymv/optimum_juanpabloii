<div>
    <div class="card">
        <div class="card-header">
            <h6>Vista previa del simulacro de exámen</h6>
        </div>
        <div class="card-body">
            @if(\Illuminate\Support\Facades\Auth::user()->usuario=='26706925')
                <div class="row">
                    {{--                <div class="col-12 col-md-4">--}}
                    {{--                    <label for="periodId">Periodo</label>--}}
                    {{--                    <select name="periodId" id="periodId" wire:model="periodId" class="form-control">--}}
                    {{--                        @foreach($periods as $period)--}}
                    {{--                            <option value="{{$period->id}}">{{$period->descripcion}}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                </div>--}}
                    <div class="col-12 col-md-4">
                        <label for="month">Mes:</label>
                        <select name="month" id="month" wire:model="month" class="form-control">
                            @foreach($months as $month)
                                <option
                                    value="{{$month['month'].'-'.$month['year']}}">{{$month['monthName'].' - '.$month['year']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-md-2">
                        <button class="btn btn-info mt-4" wire:click="searchExam"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                @if($exam)
                    @forelse($exam->preguntas->sortBy('curso_id') as $pregunta)
                        <h6><strong>{{$pregunta->curso->nombre}}</strong></h6>
                        <div class="row">
                            <div class="col-12">
                                {{$loop->index+1 .") ".$pregunta->pregunta}}
                                @if($pregunta->url_imagen)
                                    <br>
                                    <img height="300px" width="300px" src="{{asset($pregunta->url_imagen)}}">
                                @endif
                                <br>
                                <ol type="a">
                                    @foreach($pregunta->bpAlternativas as $alternativa)
                                        <li style="color: {{$alternativa->valor==1?'blue':''}}">{{$alternativa->alternativa}}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    @empty
                    @endforelse
                @endif
            @endif
        </div>
    </div>
</div>
