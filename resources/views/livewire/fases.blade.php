<div>
    <div class="card">
        <div class="card-header">
            <p><strong>Fases del periodo acádemico </strong> tenga en cuenta que esto será tomado en cuenta al momento
                de registrar la programción del curso y contenidos</p>
            <button class="btn btn-success" data-toggle="modal" data-target="#modal_fase" wire:click="create"><i class="fa fa-plus"></i> Nueva Fase</button>
        </div>
        <div class="card-body">
            <h6>lista de periodos lectivos</h6>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Número</th>
                        <th>Denominación</th>
                        <th>Inicio/Fin</th>
                        <th>Duración</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($fases as $loop=>$fase)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$fase->num_fase}}</td>
                            <td>{{$fase->denominacion}}</td>
                            <td>{{$fase->fecha_inicio.' / '.$fase->fecha_fin}}</td>
                            <td>{{$fase->duracion}}</td>
                            <td>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#modal_fase" wire:click="edit({{$fase->id}})">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-warning" wire:click="delete({{$fase->id}})">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center;background-color: grey"> Sin datos</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div wire:ignore.self id="modal_fase" class="modal fade" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Fase</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12 col-md-4">
                            <label for="numFase">Número de fase</label>
                            <input type="text" name="numFase" id="numFase" wire:model="numFase"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-8">
                            <label for="denominacion">Denominación</label>
                            <input type="denominacion" name="denominacion" id="denominacion" wire:model="denominacion"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 col-md-4">
                            <label for="fechaInicio">Fecha Inicio</label>
                            <input type="date" name="fechaInicio" id="fechaInicio"
                                   wire:model="fechaInicio"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="fechaFin">Fecha fin</label>
                            <input type="date" name="fechaFin" id="fechaFin" wire:model="fechaFin"
                                   class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="duracion">duración</label>
                            <input type="text" name="duracion" id="duracion" wire:model="duracion"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple">Close</button>
                    <button type="button" class="btn bg-primary legitRipple"
                            wire:click="{{$faseId==null?'savefase':'update'}}" data-dismiss="modal">Save changes
                    </button>
                </div>
            </div>
        </div>
    </div>
{{--        @section('script')--}}
{{--            <script type="text/javascript">--}}
{{--                // window.livewire.on('modal_fase', () => {--}}
{{--                //     alert('hola')--}}
{{--                // });--}}
{{--                function alert(){--}}
{{--                    alert('hola')--}}
{{--                }--}}
{{--            </script>--}}
{{--        @endsection--}}
</div>
