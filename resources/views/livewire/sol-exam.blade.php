<div>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label>Periodo:</label>
                    <select name="examId" id="examId" wire:model="periodId" class="form-control">
                        <option>Seleccione un periodo</option>
                        @forelse($periods as $period)
                            <option value="{{$period->id}}">{{$period->descripcion}}</option>
                        @empty
                            <option>No hay periodos que mostrar</option>
                        @endforelse
                    </select>
                </div>
                <div class="col-12 col-md-4">
                    <label>Exámen:</label>
                    <select name="examId" id="examId" wire:model="examId" class="form-control">
                        <option>Seleccione Un exámen para evaluar</option>
                        @forelse($exams as $exam)
                            <option value="{{$exam->id}}">{{$exam->mes_ano}}</option>
                        @empty
                            <option>No hay exàmenes que mostrar</option>
                        @endforelse
                    </select>
                </div>
                @if($habEvaluar==1)
                    <div class="col-12 col-md-1">
                        <button class="btn btn-info mt-4" wire:click="evaluar">Evaluar</button>
                    </div>
                @endif
                @if($results)
                    <div class="col-12 col-md-1">
                        <button class="btn btn-info mt-4" wire:click="export">Descargar</button>
                    </div>
                @endif
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                @if($results)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Estudiante</th>
                                <th>Sección</th>
                                <th>Mes</th>
                                <th>Puntaje</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($results as $loop=> $result)
                                <tr>
                                    <td>{{$loop->index +1}}</td>
                                    <td>{{$result->alumno->persona->DNI.' - '.$result->alumno->persona->paterno.' '.$result->alumno->persona->materno.' '.$result->alumno->persona->nombres}}</td>
                                    <td>{{$result->alumno->alumnocarreras->last()->grupo}}</td>
                                    <td>{{$result->examen->mes_ano}}</td>
                                    <td>{{number_format($result->nota,0)}}</td>
                                <!--<td>{{round($result->nota, 0)}}</td>-->
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
