<div>
    <div class="card">
        <div class="card-header">
            <h4>Mis cursos</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-5">
                    <label for="periodoId">Periodo</label>
                    <select name="periodoId" id="periodoId" wire:model="periodoId" class="form-control">
                        <option value="">Seleccione un periodo</option>
                        @foreach($periodos as $periodo)
                            <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                @forelse($myCourses as $myCourse)
                    <div class="col-12 col-md-6">
                        <a href="{{route('CourseAlumno',['seccionId'=>$myCourse->seccion->id])}}">
                            <div class="card" style="background-color: #c5d8ea">
                                <div class="card-header" >
                                    <span style="color: #0a6ebd">{{$myCourse->seccion->pe_curso->curso->nombre.' - '.$myCourse->seccion->grupo}}</span>
                                    <br>
                                </div>
                               <div class="card-body">
                                   <div class="row">
                                       <div class="col-12 col-md-8">
                                           <label>{{$myCourse->seccion->pe_curso->curso->carrera->nombre}}</label>
                                       </div>
                                       <div class="col-12 col-md-4">
                                           <i class="fa fa-book fa-3x"></i>
                                       </div>
                                   </div>
                               </div>
                            </div>
                        </a>
                    </div>
                @empty
                @endforelse
                {{--                {{$myCourses->links()}}--}}
            </div>
        </div>
    </div>
</div>
