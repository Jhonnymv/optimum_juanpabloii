<div>
    <div class="card">
        <div class="card-header">
            Registrar Preguntas para simulacro
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-2">
                    <label for="period">Periodo:</label>
                    <input type="text" wire:model="periodDes" class="form-control text-center" readonly>
                </div>
                <div class="col-12 col-md-2">
                    <label for="month">Mes:</label>
                    <select name="month" id="month" wire:model="month"
                            class="form-control" {{$examId!=null?'disabled':''}}>
                        @forelse($months as $month)
                            <option value="{{$month['value']}}">{{$month['text']}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>

                <div class="col-12 col-md-5">
                    <label for="period">Fecha / hora programada / Duración:</label>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <input type="date" wire:model="date"
                                   class="form-control text-center" {{$examId!=null?'readonly':''}}>
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="text" wire:model="hour" class="form-control text-center"
                                   placeholder="{{\Carbon\Carbon::now()->format('H:i')}}" {{$examId!=null?'readonly':''}}>
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="text" wire:model="duration" class="form-control text-center"
                                   placeholder="{{'h:m'}}" {{$examId!=null?'readonly':''}}>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <button type="button" class="btn btn-primary mt-4"
                            wire:click="storeExam" {{$examId!=null?'disabled':''}}>Registrar Preguntas
                    </button>
                </div>
            </div>
            @if($examId)
                <div class="row">
                    <div class="col-12 col-md-4">
                        <label for="peCourseId">Curso/Categoría</label>
                        <select name="peCourseId" id="peCourseId" class="form-control" wire:model="cursoId">
                            <option>Seleccione un área</option>
                            @forelse($cursos as $curso)
                                <option value="{{$curso->id}}">{{$curso->nombre}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
                <h5>Nueva Pregunta</h5>
                <hr>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if($cursoId)
                    @if($cursoQuestions>$questionNumber)
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <label for="questionName">Pregunta</label>
                                <textarea name="questionName" id="questionName" rows="1" wire:model="questionName"
                                          class="form-control"></textarea>
                                @error('questionName')<strong style="color: red">{{ $message }}</strong>@enderror
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="questionImage">Imagen</label>
                                <input type="file" name="questionImage" id="questionImage" wire:model="questionImage"
                                       class="form-control">
                                @if ($questionImage)
                                    <img height="300px" width="300px" src="{{ $questionImage->temporaryUrl() }}">
                                @else
                                    <div wire:loading wire:target="questionImage">Espere. Cargando...</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-7 form-group">
                                <label for="alterName">Alternativa:</label>
                                <input type="text" id="alterName" wire:model="alterName.0" class="form-control">
                                @error('alterName.0')<strong style="color: red">{{ $message }}</strong>@enderror
                            </div>
                            <div class="col-12 col-md-2 form-group">
                                <label for="alterImg">img alternativa:</label>
                                <input type="file" id="alterImg" wire:model="alterImg.0" class="form-control">
                                @if (isset($alterImg[0]))
                                    <img height="100px" width="100px" src="{{ $alterImg[0]->temporaryUrl() }}">
                                @else
                                    <div wire:loading wire:target="alterImg.0">Espere. Cargando...</div>
                                @endif
                            </div>
                            <div class="col-12 col-md-2 form-group">
                                <input type="radio" name="valor" id="alterValor0" wire:model="alterValor" value="0"
                                       class="mt-4"> Verdadero
                                @error('alterValor')<strong style="color: red">{{ $message }}</strong>@enderror
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-success mt-4" wire:click="add({{$i}})">+</button>
                            </div>
                        </div>
                        @foreach($inputs as $key=>$value)
                            <div class="row">
                                <div class="col-12 col-md-7 form-group">
                                    <label for="alterName">Aeternativa:</label>
                                    <input type="text" id="alterName{{$value}}" wire:model="alterName.{{$value}}"
                                           class="form-control" placeholder="Este campo debe obligatóriamente ser llenado">
                                    @error("alterName.'.$value'")<strong style="color: red">{{ $message }}</strong>@enderror
                                </div>
                                <div class="col-12 col-md-2 form-group">
                                    <label for="alterImg{{$value}}">img alternativa:</label>
                                    <input type="file" id="alterImg{{$value}}" wire:model="alterImg.{{$value}}"
                                           class="form-control">
                                    @if (isset($alterImg[$value]))
                                        <img height="100px" width="100px" src="{{ $alterImg[$value]->temporaryUrl() }}">
                                    @else
                                        <div wire:loading wire:target="alterImg.{{$value}}">Espere. Cargando...</div>
                                    @endif
                                </div>
                                <div class="col-12 col-md-2 form-group">
                                    <input type="radio" name="valor" id="alterValor{{$value}}" wire:model="alterValor"
                                           value="{{$value}}" class="mt-4"> Verdadero
                                    @error('alterValor')<strong style="color: red">{{ $message }}</strong>@enderror
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-danger btn-sm" wire:click.prevent="remove({{$key}})">-
                                    </button>
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-primary" wire:click="storeQuestion">Guardar
                                    Pregunta
                                </button>
                            </div>
                        </div>
                    @endif
                    <hr>
                    @forelse($preguntas as $loop=>$pregunta)
                        <div class="row">
                            <div class="col-12">
                                {{$loop->index+1 .") ".$pregunta->pregunta}}
                                <i class="fa fa-edit" style="color: orange; cursor: pointer"
                                   wire:click="edit({{$pregunta->id}},'question')"
                                   data-toggle="modal" data-target="#large_modal"
                                   title="Editar"></i>
                                <i class="fa fa-trash" style="color: red; cursor: pointer"
                                   wire:click="deleteQuestion({{$pregunta->id}})" title="eliminar pregunta"></i>
                                @if($pregunta->url_imagen)
                                    <br>
                                    <img height="300px" width="300px" src="{{asset($pregunta->url_imagen)}}">
                                @endif
                                <br>
                                <ol type="a">
                                    @foreach($pregunta->bpAlternativas->sortBy('id') as $alternativa)
                                        <li style="color: {{$alternativa->valor=='1'?'blue':'black'}}" class="mt-1">
                                            {{$alternativa->alternativa}}
                                            @if($alternativa->url_img)
                                                <img height="100px" width="100px"
                                                     src="{{asset($alternativa->url_img)}}">
                                            @endif
                                            <i class="fa fa-edit" style="color: orange; cursor: pointer"
                                               wire:click="edit({{$alternativa->id}},'alter')" data-toggle="modal"
                                               data-target="#large_modal" title="Editar"></i>

                                            <i style="cursor: pointer" class="fa fa-check-circle"
                                               wire:click="checkTrue({{$pregunta->id}},{{$alternativa->id}})"
                                               title="Marcar como verdadera"></i>
                                            <i class="fa fa-trash" style="color: red; cursor: pointer"
                                               wire:click="deleteAlter({{$alternativa->id}})"
                                               title="eliminar Alternativa"></i>
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    @empty
                        <span>Aún no registra preguntas</span>
                    @endforelse
                @endif
            @endif
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="large_modal" tabindex="-1" role="dialog"
         style="z-index: 1050; display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <label for="textEdit">Campo a editar</label>
                            <textarea type="text" name="textEdit" id="textEdit" wire:model="textEdit"
                                      class="form-control" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="saveEdit" data-dismiss="modal">
                        guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
