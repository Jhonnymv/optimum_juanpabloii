<div>
    <div class="card">
        <div class="card-header" style="background-color: #006298; color: white">
            Asignaciones y Tareas
            <label>Courso: </label><strong> {{$seccion->pe_curso->curso->nombre.' - '.$seccion->pe_curso->pe_carrera->carrera->nombre}}</strong>
        </div>
        <div class="card-body">
            @forelse($fases as $loop=>$fase)
                <h4>{{$fase->denominacion}}</h4>
                @foreach($units->where('fase_id',$fase->id) as $unit)
                    <div class="col-12">
                        <div class="card card-collapsed">
                            <div class="card-header text-white header-elements-inline "
                                 style="background-color: #006298">
                                <h6 class="card-title">Unidad: {{$unit->denominacion}}</h6>
                                <button type="button" class="btn btn-info" data-toggle="modal"
                                        data-target="#large_modal"
                                        wire:click="getUnitId({{$unit->id}})">Nueva
                                    Asignación
                                </button>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body" style="display: none">
                                <div class="row mt-2">
                                    @foreach($unit->asignaciones as $asignacion)
                                        <div class="col-12 col-md-4">
                                            <div class="card">
                                                <div class="card-header bg-info text-white header-elements-inline">
                                                    <h6 class="card-title">{{$asignacion->nombre}}</h6>
                                                    <div class="header-elements">
                                                        <div class="list-icons">
                                                            <a class="list-icons-item" data-action="collapse"></a>
                                                            <a class="list-icons-item" data-action="reload"></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-body">
                                                    <p>{{$asignacion->descripcion}}</p>
                                                    @if($asignacion->url_doc)
                                                        <a target="_blank" href="{{asset($asignacion->url_doc)}}">Documento</a>
                                                        <br>
                                                    @endif
                                                    @if($asignacion->link)
                                                        <a target="_blank" href="{{asset($asignacion->link)}}">link</a>
                                                        <br>
                                                    @endif
                                                    @if(count($asignacion->alumno)>0)
                                                        <a href="{{route('showAssign',['assignId'=>$asignacion->id])}}"
                                                           class="btn btn-primary">Ver Respuestas</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @empty
            @endforelse
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="large_modal" tabindex="-1" role="dialog"
         style="z-index: 1050; display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ingreso de Categorías</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12 mb-2">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" wire:model="nombre"
                                   class="form-control">
                        </div>
                        <div class="col-12">
                            <label for="checkInstrumento">Instrumento</label>
                            <input type="checkbox" name="checkInstrumento" id="checkInstrumento"
                                   wire:model="checkInstrumento"
                                   value="1">
                        </div>
                        @if($checkInstrumento=='1')
                            <div class="col-12">
                                <label for="instrumento">Instrumento</label>
                                <input type="text" name="instrumento" id="instrumento" wire:model="instrumento"
                                       class="form-control">
                            </div>
                        @endif
                        <div class="col-12">
                            <label for="descripcion">Descripción</label>
                            <input type="text" name="descripcion" id="descripcion" wire:model="descripcion"
                                   class="form-control">
                        </div>
                        <div class="col-12">
                            <label for="url_doc">Documento</label>
                            <input type="file" name="url_doc" id="url_doc" wire:model="url_doc"
                                   class="form-control">
                        </div>
                        <div class="col-12">
                            <label for="link">Link</label>
                            <input type="text" name="link" id="link" wire:model="link" class="form-control">
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12 col-md-6 form-group">
                                    <label for="startDate0">Desde:</label>
                                    <input type="date" name="startDate0" id="startDate0" wire:model="start_date"
                                           class="form-control"
                                           placeholder="{{now()}}">
                                </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for="startDate0">hora</label>
                                    <input type="text" name="startTime0" id="startTime0" wire:model="startTime"
                                           class="form-control"
                                           placeholder="{{date('H:i')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12 col-md-6 form-group">
                                    <label for="endDate0">Hasta:</label>
                                    <input type="date" name="endDate0" id="endDate0" wire:model="end_date"
                                           class="form-control"
                                           placeholder="{{now()}}">
                                </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for="endTime0">hora</label>
                                    <input type="text" name="endTime0" id="endTime0" wire:model="endTime"
                                           class="form-control"
                                           placeholder="{{date('H:i')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="storeAssignation" data-dismiss="modal">
                        guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
