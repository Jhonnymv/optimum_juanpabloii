<div>
     <div class="card">
         <div class="card-header">
             <h6>Asignacion: <strong>{{$assignation->descripcion}}</strong></h6>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table class="table table-striped">
                     <thead>
                     <tr>
                         <th>#</th>
                         <th>DNI</th>
                         <th>Alumno</th>
                         <th>Respuesta</th>
                         <th>Documento</th>
                         <th>Nota</th>
                         <th>Acción</th>
                     </tr>
                     </thead>
                     <tbody>
                     @forelse($assignation->alumno as $loop=>$alumno)
                         <tr>
                             <td>{{$loop->index +1}}</td>
                             <td>{{$alumno->persona->DNI}}</td>
                             <td>{{$alumno->persona->paterno.' '.$alumno->persona->materno.' '.$alumno->persona->nombres}}</td>
                             <td>{{$alumno->pivot->respuesta}}</td>
                             <td><a target="_blank" href="{{asset($alumno->pivot->doc_respuesta)}}">Documento</a></td>
                             <td>
                                 @if($alumno->pivot->nota===null)
                                     <input type="text" class="form-control" wire:model="nota.{{$alumno->id}}">
                                 @else
                                     <label>{{$alumno->pivot->nota}}</label>
                                 @endif
                             </td>
                             <td>
                                 @if($alumno->pivot->nota===null)
                                 <button class="fa fa-save" wire:click="storeNote({{$alumno->id}})"></button>
                                 @endif
                             </td>
                         </tr>
                     @empty
                         <tr>
                             <td colspan="5">Sin Datos</td>
                         </tr>
                     @endforelse
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
</div>
