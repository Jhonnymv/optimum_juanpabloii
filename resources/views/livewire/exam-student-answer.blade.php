<div>
    @if($levelGrade=='3-5')
        <div class="card">
            <div class="card-header">
                <h4>
                    Estudiante:<strong>{{$student->persona->paterno.' '.$student->persona->materno.', '.$student->persona->nombres}}</strong>
                </h4>
                <h6>Resultado de examen simulacro del mes de: <strong>{{$fecha->monthName}}</strong></h6>
            </div>
            <div class="card-body overflow-auto" wire:poll.1000ms style="height: 500px; overflow-y: scroll">
{{--                <span style="color: blue;">Hora actual: <strong>{{$fecha->format('h:i:s a')}}</strong></span><br>--}}
                @if($exam)
{{--                    <span style="color: red;">Fin del exámen: <strong>{{$finish->format('h:i:s a')}}</strong></span>--}}
{{--                    <span--}}
{{--                        class="text-warning">!!Momento en el cual el formulario del exámen se ocultará y solo las respuestas marcadas se contabilizarán¡¡</span>--}}
                    <hr>
                @php
                $correctas=0;
                $sw=0;
                @endphp
                    @forelse($exam->preguntas->sortBy('curso_id') as $loop=>$pregunta)
                        <div class="row">
                            <div class="col-12">
                                curso::{{$pregunta->curso->nombre}}<br>
                                {{$loop->index+1 .") ".$pregunta->pregunta}}
                                @if($pregunta->url_imagen)
                                    <br>
                                    <img height="300px" width="300px" src="{{asset($pregunta->url_imagen)}}">
                                @endif
                                <br>
                                <ol type="a">
                                    @foreach($pregunta->bpAlternativas as $alternativa)
                                        <li style="cursor: pointer">
                                            <input type="radio" name="alternativa[{{$pregunta->id}}]" disabled
{{--                                            @if($examAl)--}}
{{--                                                {{\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()!=null?(\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()->alternativa_id==$alternativa->id?'checked':''):''}}--}}
{{--                                                @endif--}}
                                            @if($examAl)
                                                {{\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()!=null?(\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()->alternativa_id==$alternativa->id?'checked':''):''}}
                                                {{\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()!=null?(\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()->alternativa_id==$alternativa->id?$sw=\App\EalDetalles::where([['examen_alumno_id',$examAl],['pregunta_id',$pregunta->id]])->first()->alternativa_id:''):''}}
                                                @endif
                                            >
{{--                                            {{$sw}}--}}
                                            @if($alternativa->valor==1)
                                                @if($alternativa->id==$sw)
                                                    @php
                                                        $correctas++;
                                                    @endphp
                                                @endif
                                                <FONT COLOR="blue">{{$alternativa->alternativa}} (Alternativa Correcta) </FONT>
                                            @else
                                                {{$alternativa->alternativa}}
                                            @endif


                                            @if($alternativa->url_img)
                                                <img height="100px" width="100px"
                                                     src="{{asset($alternativa->url_img)}}">
                                            @endif
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
{{--                        <div wire:loading.grid wire:target="saveResponse" style="color: #0aa7ef">Guardando respuestas--}}
{{--                        </div>--}}
                    @empty
                    @endforelse

                <hr>
                <h8><strong>Resultado Final:</strong></h8>
                <br>
                Respuestas Correctas = {{$correctas}}
                <br>
                Respuestas en blanco o incorrectas = {{60 - $correctas}}
                @endif
            </div>

        </div>
    @else
        <div class="card bg bg-warning">
            <div class="card-body">
                No pueder rendir un exámen simulacro si no estás en el quinto grado de secundaria
            </div>
        </div>
    @endif
</div>
