<div>
    <div class="card">
        <div class="card-header" style="background-color: #0aa7ef">
            <h2>Curso: {{$seccion->pe_curso->curso->nombre}}</h2>
        </div>
        <div class="card-body mt-2">
            <a class="btn btn-primary"
               href="{{route('CourseDocenteProgramation',['seccionId'=>$seccion->id])}}">{{$programation==null?'Programación':'Editar Programación'}}</a>
            @if($programation!=null)
                <a class="btn btn-primary"
                   href="{{route('assignationDocent',['seccionId'=>$seccion->id])}}">Asignaciones</a>
            @endif
            <div class="row">
                <div class="col-12 col-md-6">
                    {{--                    <h6>Avisos</h6>--}}
                    {{--                    <hr>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-1" style="text-align: right"><i class="fa fa-file-archive-o"></i></div>--}}
                    {{--                        <div class="col-10"><a href="">Materiales complementarios no vistos</a></div>--}}
                    {{--                        <div class="col-1"><span class="badge rounded-pill bg-warning">10</span></div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-1" style="text-align: right"><i class="fa fa-file-archive-o"></i></div>--}}
                    {{--                        <div class="col-10"><a href="">Preguntas frecuentes no leidas</a></div>--}}
                    {{--                        <div class="col-1"><span class="badge rounded-pill bg-success">0</span></div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-1" style="text-align: right"><i class="fa fa-file-archive-o"></i></div>--}}
                    {{--                        <div class="col-10"><a href="">Eventos de Calendario para hoy</a></div>--}}
                    {{--                        <div class="col-1"><span class="badge rounded-pill bg-warning">5</span></div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-1" style="text-align: right"><i class="fa fa-file-archive-o"></i></div>--}}
                    {{--                        <div class="col-10"><a href="">Mensajes no leidos</a></div>--}}
                    {{--                        <div class="col-1"><span class="badge rounded-pill bg-success">0</span></div>--}}
                    {{--                    </div>--}}
                    {{--                    <br>--}}
                    {{--                    <br>--}}
                    {{--                    <h6>Progreso</h6>--}}
                    {{--                    <hr>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-10">Lecciones Completadas</div>--}}
                    {{--                        <div class="col-2">(10/50)</div>--}}
                    {{--                        <div class="progress mb-3" style="height: 0.375rem;">--}}
                    {{--                            <div class="progress-bar progress-bar-striped bg-success" style="width: 72%">--}}
                    {{--                                <span class="sr-only">72% Complete</span>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-10">Exámenes Entregados</div>--}}
                    {{--                        <div class="col-2">(10/50)</div>--}}
                    {{--                        <div class="progress mb-3" style="height: 0.375rem;">--}}
                    {{--                            <div class="progress-bar progress-bar-striped bg-success" style="width: 72%">--}}
                    {{--                                <span class="sr-only">72% Complete</span>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-10">Encuestas Entregadas</div>--}}
                    {{--                        <div class="col-2">(10/50)</div>--}}
                    {{--                        <div class="progress mb-3" style="height: 0.375rem;">--}}
                    {{--                            <div class="progress-bar progress-bar-striped bg-success" style="width: 72%">--}}
                    {{--                                <span class="sr-only">72% Complete</span>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <br><br>
                    <h6>Documentos para el Alumno</h6>
                    <hr>
                    @if($programation)
                        <a href="{{$programation->link_clase}}" target="_blank">{{$programation->nombre_clase}}</a>
                        <hr>
                        @forelse($programation->documentos as $documento)
                            @if($documento->url)
                                <a target="_blank" href="{{asset($documento->link)}}">{{$documento->nombre}} <i
                                        class="fa fa-file"></i></a> //
                            @endif
                            @if($documento->link)
                                <a target="_blank" href="{{$documento->link}}">{{$documento->nombre}} <i
                                        class="fa fa-link"></i></a> <br>
                            @endif

                        @empty
                        @endforelse
                    @endif

                </div>
                <div class="col-12 col-md-6">
                    {{--                    <h6>Próximos eventos</h6>--}}
                    {{--                    <hr>--}}
                    {{--                    <span>No tiene eventos próximos</span>--}}
                    {{--                    <br>--}}
                    {{--                    <a href="" class="btn btn-warning"><i class="fa fa-calendar"></i> Calendario</a>--}}
                    {{--                    <br>--}}
                    {{--                    <br>--}}
                    {{--                    <h6>Mensajes</h6>--}}
                    {{--                    <hr>--}}
                    {{--                    <span>Mensajes no leidos</span>--}}
                    {{--                    <br>--}}
                    {{--                    <a href="" class="btn btn-warning"><i class="fa fa-mail-forward"></i>Mensajes</a>--}}
                    {{--                    <br>--}}
                </div>
            </div>
        </div>
    </div>
</div>
