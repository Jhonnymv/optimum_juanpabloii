<div>
    <div class="card">
        <div class="card-header">
            <h6>Reporte de deudas</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-2">
                    <label for="">Periodo</label>
                    <select name="periodId" id="periodId" wire:model="periodId" class="form-control">
                        @forelse($periods as $period)
                            <option value="{{$period->id}}">{{$period->descripcion}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-12 col-md-2">
                    <label for="">Carrera</label>
                    <select name="levelId" id="levelId" wire:model="levelId" class="form-control">
                        @forelse($levels as $level)
                            <option value="{{$level->id}}">{{$level->nombre}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-12 col-md-2">
                    <label for="">Grado</label>
                    <select name="gradeId" id="gradeId" wire:model="gradeId" class="form-control">
                        @forelse($grades as $grade)
                            <option value="{{$grade->grade}}">{{$grade->grade}}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="col-12 col-md-2">
                    <label for="">Sección</label>
                    <select name="section" id="section" wire:model="section" class="form-control">
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                    </select>
                </div>
                <div class="col-12 col-sm-1">
                    <button class="btn btn-info mt-4" wire:click="getData"><i class="fa fa-search"></i></button>
                </div>
                <div class="col-12 col-sm-2">
                    <a target="_blank" href="{{route('reportDeudaPdf',['periodoId'=>$periodId])}}" class="btn btn-info mt-4"><i class="fa fa-download"></i> Reporte Completo</a>
                </div>
                <div class="col-12 col-sm-2">
                    <a target="_blank" href="{{route('reportDeudasExcel')}}" class="btn btn-info mt-4"><i class="fa fa-download"></i> Reporte Excel</a>
                </div>
                {{--                Para Reporte por Carreras--}}
                <div class="col-12 col-sm-2">
                    <a target="_blank" href="{{route('reportDeudasCarrerasExcel',['periodo_id'=>$periodId,'carrera_id'=>$levelId])}}" class="btn btn-info mt-4"><i class="fa fa-download"></i> Reporte Inicial</a>
                    <label style="color: red"> Selecione Periodo y Nivel</label>
                </div>
                {{--                Para Reporte por Carreras--}}
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ALUMNO</th>
                            <th>MAT</th>
                            <th>MAR</th>
                            <th>ABR</th>
                            <th>MAY</th>
                            <th>JUN</th>
                            <th>JUL</th>
                            <th>AGO</th>
                            <th>SEP</th>
                            <th>OCT</th>
                            <th>NOV</th>
                            <th>DIC</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $loop=>$item)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{$item['alumno']}}</td>
                                <td>{{$item['matr']}}</td>
                                <td>{{$item['mar']}}</td>
                                <td>{{$item['abr']}}</td>
                                <td>{{$item['may']}}</td>
                                <td>{{$item['jun']}}</td>
                                <td>{{$item['jul']}}</td>
                                <td>{{$item['ago']}}</td>
                                <td>{{$item['sep']}}</td>
                                <td>{{$item['oct']}}</td>
                                <td>{{$item['nov']}}</td>
                                <td>{{$item['dic']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
