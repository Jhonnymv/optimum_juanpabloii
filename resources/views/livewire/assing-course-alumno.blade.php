<div>
    <div class="card">
        <div class="card-header" style="background-color: #0aa7ef">
            Asignaciones y Tareas
            <label>Courso: </label><strong> {{$seccion->pe_curso->curso->nombre.' - '.$seccion->pe_curso->pe_carrera->carrera->nombre}}</strong>
        </div>
        <div class="card-body" wire:poll>
            @forelse($fases as $loop=>$fase)
                <h4>{{$fase->denominacion}}</h4>
                @foreach($units->where('fase_id',$fase->id) as $unit)
                    <span class="ml-2">Unidad: <b>{{$unit->denominacion}}</b></span>
                    <ul>
                        @foreach($unit->asignaciones->where('start_date','<=',$now)->where('end_date','>=',$now) as $asignacion)
                            <li>
                                <u>{{$asignacion->nombre}}</u>
                                <p>{{$asignacion->descripcion}}</p>

                                @if($asignacion->url_doc)
                                     <a target="_blank" href="{{asset($asignacion->url_doc)}}">Documento</a><i class="fa fa-file ml-1"></i>
                                @endif
                                @if($asignacion->link)
                                    <br> <a target="_blank" href="{{asset($asignacion->link)}}">link</a><i class="fa fa-link ml-1"></i>
                                @endif
                                @if(count($alumno->asignaciones)===0)
                                    <br> <a href="" data-toggle="modal" data-target="#large_modal"
                                       wire:click="getAssignId({{$asignacion->id}})" style="color: #4ca950">Respuesta
                                    </a>
                                @else
                                    <br> <span style="color: {{$asignacion->alumno[0]->pivot->nota>10?'blue':'red'}}">Nota: {{$asignacion->alumno[0]->pivot->nota}}</span>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endforeach
            @empty
            @endforelse
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="large_modal" tabindex="-1" role="dialog"
         style="z-index: 1050; display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ingreso de Categorías</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            <label for="respuesta">Respuesta</label>
                            <textarea type="text" name="respuesta" id="respuesta" wire:model="respuesta"
                                      class="form-control" rows="1"></textarea>
                        </div>
                        <div class="col-12">
                            <label for="docRespuesta">Documento</label>
                            <input type="file" name="docRespuesta" id="docRespuesta" wire:model="docRespuesta"
                                   class="form-control">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="storeResp" data-dismiss="modal">
                        guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

