<div>
    <div class="row">
        <div class="col-md-12">

            <!-- Static mode -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Ver Datos Alumnos</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Selecione Tipo de Busqueda:</label>
                                <select class="form-control" wire:model="tsearch">
                                    <option value="">Seleccione una Opción</option>
                                    <option value="dni">DNI</option>
                                    <option value="nombre">NOMBRES</option>
                                    <option value="apellidos">APELLIDOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" STYLE="display: {{$tsearch==='dni'?'':'none'}}">
                            <div class="form-group">
                                <label>Ingrese Dni a Buscar:</label>
                                <input type="text" class="form-control" wire:model="dniSearch">
                            </div>
                        </div>
                        <div class="col-md-3" STYLE="display: {{$tsearch==='nombre'?'':'none'}}">
                            <div class="form-group">
                                <label>Ingrese Nombres:</label>
                                <input type="text" class="form-control" wire:model="nombresSearch">
                            </div>
                        </div>
                        <div class="col-md-3" STYLE="display: {{$tsearch==='apellidos'?'':'none'}}">
                            <div class="form-group">
                                <label>Ingrese Apellido Paterno:</label>
                                <input type="text" class="form-control" wire:model="apPsearch" required>
                            </div>
                            <div class="form-group">
                                <label>Ingrese Apellido Materno:</label>
                                <input type="text" class="form-control" wire:model="apMsearch" required>
                            </div>
                        </div>

                        <div class="col-md-3" STYLE="display: {{$tsearch!=''?'':'none'}}">
                            <button class="btn btn-success" wire:click="search()">Buscar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table" id="table_padres">
                                <thead>
                                <tr>
                                    {{--                                                                    <th>#</th>--}}
                                    <th>DNI</th>
                                    <th>Nombre</th>
                                    <th>Teléfono</th>
                                    {{--                                    <th>Nivel</th>--}}
                                    {{--                                    <th>Grado</th>--}}
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--                                {{$alumnos}}--}}
                                @forelse($alumnos as $alumno)
                                    <tr>
                                        <td>{{$alumno->persona->DNI??''}}</td>
                                        <td>{{$alumno->persona->paterno??''}} {{$alumno->persona->materno??''}} {{$alumno->persona->nombres??''}}</td>
                                        <td>{{$alumno->persona->telefono??''}}</td>
                                        {{--                                        <td>{{$alumno->alumno->alumnocarreras->first()->carrera->nombre??''}}</td>--}}
                                        {{--                                        <td>{{$alumno->alumno->alumnocarreras->first()->ciclo}}</td>--}}
                                        <td>
                                            {{--                                            <button class="btn btn-primary" wire:click="getDatos({{$alumno->id}})">Ver Datos--}}
                                            {{--                                            </button>--}}

                                            <button type="button" wire:click="getDatos({{$alumno->id}})"
                                                    class="btn btn-light legitRipple" data-toggle="modal"
                                                    data-target="#modal_large">Ver Detalle <i class="icon-play3 ml-2"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4"> ¡¡¡¡No hay datos!!!!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /static mode -->
        </div>

    </div>


    <div wire:ignore.self id="modal_large" class="modal" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Datos Generales del Alumnos</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <form action="#">
                    <div class="modal-body">
                        <label class="title"><b>Datos Alumnos</b></label>
                        <div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>DNI</label>
                                    <input type="text" placeholder="dni" wire:model="dni" class="form-control" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label>Nombres</label>
                                    <input type="text" placeholder="nombres" wire:model="nombres" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Paterno</label>
                                    <input type="text" placeholder="paterno" wire:model="paterno" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Materno</label>
                                    <input type="text" placeholder="materno" wire:model="materno" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Telefono</label>
                                    <input type="text" placeholder="telefono" wire:model="telefono"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Direccion</label>
                                    <input type="text" placeholder="direccion" wire:model="direccion"
                                           class="form-control">
                                </div>
                                <div class="col-sm-1">
                                    <label>Nivel</label>
                                    <input type="text" placeholder="grado" wire:model="nivel" class="form-control">
                                </div>
                                <div class="col-sm-1">
                                    <label>Grado</label>
                                    <input type="text" placeholder="grado" wire:model="grado" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Fecha Nacimiento</label>
                                    <input type="date" placeholder="fecha nacimiento" wire:model="fecha_nacimiento" class="form-control" required>
{{--                                    <input type="hidden" placeholder="grado" wire:model="idAlumno" class="form-control">--}}
                                </div>
                                <div class="col-sm-2">
                                    <label>Sexo</label>
                                    <select wire:model="sexo" class="form-control" required>
                                        <option value="">Seleccione un valor</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                    </select>
                                    {{--                                    <input type="hidden" placeholder="grado" wire:model="idAlumno" class="form-control">--}}
                                </div>
                                <div class="col-sm-1">
                                    <label>Pension</label>
                                    <input type="text" placeholder="pension" wire:model="pension" class="form-control">
                                    <input type="hidden" placeholder="grado" wire:model="idAlumno" class="form-control">
                                </div>
                                <div class="col-sm-4"
                                     style="display: flex; justify-content: flex-end; flex-direction: row; align-items: center">
                                    <button class="btn btn-info" wire:click="editAlumno()">Editar Alumno</button>
                                </div>
                            </div>
                        </div>

                        <label class="title"><b>Datos Padre</b></label>
                        <div style="background-color: #EFEDD8">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>DNI</label>
                                    <input type="text" placeholder="dniP" wire:model="dniP" class="form-control"
                                           readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label>Nombres</label>
                                    <input type="text" placeholder="nombresP" wire:model="nombresP"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Paterno</label>
                                    <input type="text" placeholder="paternoP" wire:model="paternoP"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Materno</label>
                                    <input type="text" placeholder="maternoP" wire:model="maternoP"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Telefono</label>
                                    <input type="text" placeholder="telefonoP" wire:model="telefonoP"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dirección</label>
                                    <input type="text" placeholder="direccionP" wire:model="direccionP"
                                           class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <label>Email</label>
                                    <input type="text" placeholder="emailP" wire:model="emailP" class="form-control">
                                    <input type="hidden" placeholder="grado" wire:model="idPadre" class="form-control">
                                </div>
                                <div class="col-sm-3" style="display: flex;align-items: center">
                                    <label for="apoderadoP">Apoderado</label>
{{--                                    <input type="checkbox" id="apoderadoP" wire:model="rp"  name="apoderado" value="1">--}}
{{--                                    <input type="checkbox" id="apoderadoP" wire:model="rp" name="apoderadoP" value="1" checked required>--}}
                                    <select class="form-control" wire:model="apoderadoP" wire:click="changeEventP($event.target.value)">
                                        <option value="si">Si</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                                <div class="col-sm-3" style="display: flex; justify-content: flex-end;flex-direction: row; align-items: center">
                                    <button class="btn btn-info" wire:click="editPadre()">Editar Padre</button>

                                </div>
                            </div>

                        </div>

                        <label class="title"><b>Datos Madre</b></label>
                        <div style="background-color: #E5D8EF">
                            <div class="row">

                                <div class="col-sm-2">
                                    <label>DNI</label>
                                    <input type="text" placeholder="dniM" wire:model="dniM" class="form-control"
                                           readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label>Nombres</label>
                                    <input type="text" placeholder="nombresM" wire:model="nombresM"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Paterno</label>
                                    <input type="text" placeholder="paternoM" wire:model="paternoM"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Apellido Materno</label>
                                    <input type="text" placeholder="maternoM" wire:model="maternoM"
                                           class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <label>Teléfono</label>
                                    <input type="text" placeholder="telefonoM" wire:model="telefonoM"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dirección</label>
                                    <input type="text" placeholder="direccionM" wire:model="direccionM"
                                           class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <label>Email</label>
                                    <input type="text" placeholder="emailM" wire:model="emailM" class="form-control">
                                    <input type="hidden" placeholder="grado" wire:model="idMadre" class="form-control">
                                </div>
                                <div class="col-sm-3" style="display: flex;align-items: center">
                                    <label for="apoderadoP">Apoderado</label>
{{--                                    <input type="checkbox" id="apoderadoM" wire:model="rm" name="apoderado" value="2">--}}
{{--                                     <input type="checkbox" id="apoderadoM" wire:model="rm" name="apoderadoM" value="1">--}}
                                    <select class="form-control" wire:model="apoderadoM" wire:click="changeEvent($event.target.value)">
                                        <option value="si">Si</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                                <div class="col-sm-3" style="display: flex; justify-content: flex-end;flex-direction: row; align-items: center">
                                    <button class="btn btn-info" wire:click="editMadre()">Editar Madre</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
