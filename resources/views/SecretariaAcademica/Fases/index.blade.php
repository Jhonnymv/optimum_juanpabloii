@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Periodo Académico')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Fases de un Periodo Académico</h4>
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Fases</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <livewire:fases :periodoId="$periodoId"/>
@endsection
