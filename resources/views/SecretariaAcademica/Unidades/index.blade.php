@extends('Layouts.main')
@section('title','silabos')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Unidades</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nueva Unidad</span>
    </a>
</div>
@endsection
@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Unidades</span>
@endsection
@section('header_subbuttons')

@endsection
@section('content')
@if (Session::has('Mensaje'))
{{
    Session::get('Mensaje')
}}
@endif

<table class="table table-light">
    <thead  class="table-light">
        <tr>
            <th>#</th>
            <th>Sílabo id</th>
            <th>Unidad</th>
            <th>Fecha inicio</th>
            <th>Fecha fin</th>
            <th>Duración</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @forelse($unidad as $item)
        <tr>
            <td scope="row">{{$loop -> iteration}}</td>
            <td>{{$item -> silabo_id}}</td>
            <td>{{$item -> unidad}}</td>

            <td>{{$item -> fecha_inicio}}</td>
            <td>{{$item -> fecha_fin}}</td>
            <td>{{$item -> duracion}}</td>


            <td>


             <a href="{{url('/unidades/'.$item -> silabo_id)}}">Ver Unidades</a>
            |
            <form method="post" action="{{url('/docentes/'.$item -> silabo_id)}}">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" onclick="return confirm('¿Desea borrar el registro?');">Borrar</button>


            <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id }})"><i class="fas fa-trash"></i></a>
            <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})"><i class="fas fa-pen"></i></a>

            </form>
            </td>
        </tr>
    @endforeach
        <tr>

        </tr>
    </tbody>
</table>
@endsection

@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/unidades">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo Unidad</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Sílabo:</label>
                    <div class="col-lg-9">
                        <select name="silabo" class="form-control">

                            @forelse ($silabo as $item)
                            <option value="{{$item->id}}">
                                {{$item->nombre}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Unidad:</label>
                    <div class="col-lg-9">
                        <input type="text" name="unidad" class="form-control" placeholder="Número de Unidad..." required>
                    </div>
                {{--  <div class="form-group row">
					<label class="col-lg-3 col-form-label">Curso:</label>
						<div class="col-md-10">
							 <select name="curso" class="form-control">

                            //@forelse ($cursos as $item)
                            //<option value="{{$item->id}}">
                            //    {{$item->nombre}}
                            //</option>
                            //@empty

                            //@endforelse

                        </select>
						</div>
				</div>
                  --}}
                 {{--  <div class="form-group row">
					<label class="col-lg-3 col-form-label">Periodo Académico:</label>
                    <br>
						<div class="col-md-10">
							 <select name="periodoacademicos" class="form-control">

                            @forelse ($periodoacademicos as $item)
                            <option value="{{$item->id}}">
                                {{$item->descripcion}}
                            </option>
                            @empty

                            @endforelse

                        </select>
						</div>
				</div>  --}}
                       <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Inicio</label>
						<div class="col-md-10">
							<input class="form-control" type="date" name="fecha_inicio">
						</div>
				</div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Fin</label>
						<div class="col-md-10">
							<input class="form-control" type="date" name="fecha_fin">
				        </div>
				</div>

                    <div class="form-group row">
						<label class="col-lg-3 col-form-label">Duración:</label>
					    	<div class="col-lg-10">
										<textarea rows="3" cols="3" name="duracion" class="form-control" placeholder="Duración...."></textarea>
						    </div>

                <div class="form-group row">
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/periodosacademicos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Periodo AAcadémico?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Periodo Académico</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<!-- editar modal -->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/periodosacademicos">
            @method('PUT');
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Actualizar periodo académico</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" id="descripcionEdit" name="descripcionEdit" class="form-control" placeholder="Descripción..." >
                    </div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Inicio</label>
						<div class="col-md-10">
							<input class="form-control" type="date" id="fecha_inicioEdit" name="fecha_inicioEdit">
						</div>
				</div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Fin</label>
						<div class="col-md-10">
							<input class="form-control" type="date"  id="fecha_finEdit" name="fecha_finEdit">
				        </div>
				</div>
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>

@endsection


@section('script')
<script>
    function eliminar(id)
    {
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/periodosacademicos/remove/'+id);
    }

    function editar(id)
    {
        console.log(id);
        $("#formEditar").attr('action','/periodosacademicos/'+id);
        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traerperiodoacademico',
                data: data,
                success: function(data) {
                    $('#descripcionEdit').val(data['periodoacademico']['descripcion']);
                    $('#fecha_inicioEdit').val(data['periodoacademico']['fecha_inicio']);
                    $('#fecha_finEdit').val(data['periodoacademico']['fecha_fin']);
                   // $('#telefonoEdit').val(data['alumno']['telefono']);



                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>

@endsection
