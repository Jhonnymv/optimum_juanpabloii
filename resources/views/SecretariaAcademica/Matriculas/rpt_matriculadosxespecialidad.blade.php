@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Reporte Matriculados por
        especialidad
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Reporte matriculados por especialidad</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-form-label ">Period académico:</label>
                        <select id="semestre" class="col-2 form-control">
                            <option>Seleccione un periodo de estudios</option>
                            @foreach($periodos as $periodo)
                                <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                            @endforeach
                        </select>
                        <label class="col-form-label  carrera fade">Programa:</label>
                        <select id="carrera" class="col-3 carrera fade form-control">
                            <option>Seleccione un programa</option>
                            @foreach($carreras as $carrera)
                                <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                            @endforeach
                        </select>
                        <a id="pdf_generate" target="_blank" class="btn btn-primary pull-right" disabled><i
                                class="icon-file-pdf"></i>Descargar</a>
                    </div>
                    Esta es una pantalla para verificar la pre-matricula de los alumnos <br>

                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>Alumno</th>
                                <th>Teléfono</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>fecha de registro</th>
                                {{--                                <th>Opciones</th>--}}
                            </tr>
                            </thead>
                            <tbody id="tbody_matriculas">

                            </tbody>
                        </table>
                        <span class="t_alumno fade">Número total de alumnos en el curso:  <strong class="t_alumno fade"
                                                                                                  id="total_alumnos">0</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_rptxespecialidad" value="{{route('matricula.view_rptxespecialidad')}}">
    <input type="hidden" id="url_pdf_rptxespecialidad" value="{{route('matricula.rptxespecialidad')}}/">
    <input type="hidden" name="" id="url_get_boletaxmatricula" value="{{route('vouchers.getboletaxmatricula')}}">
    <input type="hidden" name="" id="url_get_detx_matricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">
@endsection

@section('modals')
    <div id="modal_img" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disable keyboard interaction</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <span>Número de boleta: <strong id="num_boleta"></strong></span><br>
                    <span>Fecha: <strong id="fecha_boleta"></strong></span><br>
                    <span>Monto: <strong id="monto_boleta"></strong></span><br>

                    <iframe id="img_boleta" style="width: 100%;height: 50em"></iframe>
                </div>

            </div>
        </div>
    </div>
    <div id="modal_detalles" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lista de Cursos matriculados</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                  <table id="t_detalles" class="table table-striped">
                      <thead>
                      <th>Curso</th>
                      <th>Ciclo</th>
                      <th>Tipo</th>
                      </thead>
                      <tbody id="t_body_detalles">
                      </tbody>
                  </table>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>
    <script>
        moment.locale('es')
        $(function () {
            $('#semestre').change(function () {
                $('.carrera').removeClass('fade')
                $('#tbody_matriculas').html('')
            })
            $('#carrera').change(function () {
                var url_pdf = $('#url_pdf_rptxespecialidad').val() + $('#semestre').val() + '/' + $(this).val()
                $('#tbody_matriculas').html('')
                var url = $('#url_rptxespecialidad').val()
                var data = {'periodo_id': $('#semestre').val(), 'carrera_id': $(this).val()}
                // console.log(url_pdf)
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data.matriculas)
                        data.matriculas.forEach(element => {
                            var date = moment(element.created_at)
                            var alumno = element.alumnocarrera.alumno.persona
                            var template = "<tr>" +
                                "<td>" + alumno.paterno + " " + alumno.materno + ", " + alumno.nombres + "</td>" +
                                "<td>" + alumno.telefono + "</td>" +
                                "<td>" + element.alumnocarrera.carrera.nombre + "</td>" +
                                "<td>" + element.alumnocarrera.ciclo + '°'+"</td>" +
                                "<td>" + date.format('L') + "" +
                                // "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_detalles(" + element.id + ")' title='Ver detalles'><i class='icon-eye' ></i>Ver Detalles</button>"+
                                // "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_img(" + element.id + ")' title='Ver boleta'><i class='icon-eye' ></i>Ver Boleta</button>"+
                                "</td>"+
                                "</tr>"

                            $('#tbody_matriculas').append(template)
                        })
                        $('#table_matriculas').DataTable({
                            "language": {
                                "url": "{{asset('assets/DataTables/Spanish.json')}}"
                            }
                        })
                        $('#pdf_generate').attr('href', url_pdf).removeAttr('disabled')
                    }
                })
            })
        });

        function get_img(matricula_id) {
            var url=$('#url_get_boletaxmatricula').val();
            var data={'mat_id':matricula_id}
            $.ajax({
                url:url,
                type: 'GET',
                data:data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    $('#num_boleta').text(data.voucher.num_operacion)
                    $('#fecha_boleta').text(data.voucher.fecha)
                    $('#monto_boleta').text(data.voucher.monto)
                    $('#img_boleta').attr('src',data.urlimg)
                    $('#modal_img').modal("show");
                },
                error: function (){
                    console.log('error')
                }
            })
        }
        function get_detalles(matricula_id){
            var url=$('#url_get_detx_matricula').val()+matricula_id
            $.ajax({
                url:url,
                type:'GET',
                dataType:'json',
                success: function (data) {
                    $('#t_body_detalles').html('')
                    data.detalles.forEach(element=>{
                        var template="<tr>" +
                            "<td>"+element.seccion.pe_curso.curso.nombre+"</td>"+
                            "<td>"+element.seccion.pe_curso.semestre+"</td>"+
                            "<td>"+element.tipo+"</td>"+
                            "</tr>"

                        $('#t_body_detalles').append(template)
                    });
                    $('#modal_detalles').modal('show')
                }
            })
            $('#t_detalles').DataTable({
                "language": {
                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                }
            })
        }
    </script>
@endsection


