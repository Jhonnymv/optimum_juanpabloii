@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Pre-matriculas
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar matriculas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar la pre-matricula de los alumnos
                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>Alumno</th>
                                <th>DNI</th>
                                <th>Nivel</th>
                                <td>Ciclo</td>
                                <th>fecha de registro</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            @foreach($matriculas as $matricula)
                                <tr>
                                    <td>{{($matricula->alumno->persona->paterno).' '.($matricula->alumno->persona->materno??' ').', '.($matricula->alumno->persona->nombres??' ')}}</td>
                                    <td>{{$matricula->alumno->persona->DNI}}</td>
                                    <td>{{$matricula->carrera->nombre}}</td>
                                    <td>{{$matricula->ciclo}}</td>
                                    <td>{{date('d-m-Y',strtotime($matricula->created_at))}}</td>
                                    <td>{{$matricula->grupo}}</td>
                                    <td>

                                        <a type="button"
                                           {{--                                           data-toggle='modal' data-target='#modal-detalle'--}}
                                           {{--                                                onclick="get_detalles({{$matricula->id}})"--}}
                                           href="{{route('detalle_matricula.getxmatricula',['matricula_id'=>$matricula->id])}}"
                                           class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                           title="Revisar Matrícula">
                                            <i class="icon-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <input type="hidden" id="url_getxmatricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">--}}
@endsection

@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Basic modal</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <th>Curso</th>
                        <th>Horario</th>
                        </thead>
                        <tbody id="t_body_detalles">

                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="matricula_id">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar()">Aprobar</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_matriculas').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_detalles(id) {
            var url = $('#url_getxmatricula').val() + id
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#t_body_detalles').html('')
                    data.detalles.forEach(element => {
                        var horarios = "";
                        $.each(element.seccion.horarios, function (index, value) {
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + ' --- ' + value.aula['aula'] +"<br>";
                        });
                        var template = "<tr>"
                            + "<td>"+element.seccion.pe_curso.curso.nombre+"</td>"
                            + "<td>"+horarios+"</td>"
                            + "</tr>"

                        $('#t_body_detalles').append(template)
                    })
                    $('#matricula_id').val(id)
                }
            })
        }
        function aprobar() {
            var url=$('#url_actualizar_matricula').val()
            var id=$('#matricula_id').val()
            var data={'id':id,'estado':'POR-PAGAR'}
            $.ajax({
                url:url,
                type: 'GET',
                data:data,
                dataType: 'json',
                success: function (data){
                    // console.log(data)
                        location.reload()
                }
            })
        }

    </script>
@endsection
