@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar
        Pre-matriculascon cursos de subsanación
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar matriculas con cursos de subsanación </span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">
                        <div class="header-elements">
                            <button
                                onclick="get_cursos({{$detalles->first()->matricula->alumnocarrera->carrera_id}})"
                                type="button" class="btn btn-success btn-sm pull-right"
                                data-toggle="modal" data-target="#modal-agregar-curso">Agregar curso
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar la pre-matricula de los alumnos
                    <div class="table-responsive">
                        <table class="table" id="table_detalles">
                            <thead>
                            <tr>
                                <th>Area</th>
                                <th>Ciclo</th>
                                <th>Créditos</th>
                                <th>N° horas</th>
                                <th>Matriculados en el curso</th>
                                <th>horario</th>
                                <th>tipo</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_detalles">
                            @foreach($detalles as $detalle)
                                <tr>
                                    <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                                    <td class="nivel-{{$detalle->tipo=='R'?'regular':'subsanacion'}}">{{$detalle->seccion->pe_curso->semestre}}</td>
                                    <td class="creditos">{{$detalle->seccion->pe_curso->creditos}}</td>
                                    <td class="horas">{{$detalle->seccion->pe_curso->horas_teoria+$detalle->seccion->pe_curso->horas_practica}}
                                    </td>
                                    <td class="n_matriculados">{{$detalle->seccion->n_matriculados}}</td>
                                    <td>
                                        @foreach($detalle->seccion->horarios as $horario)
                                            <span>{{$horario->hora_inicio.' - '.$horario->hora_fin.' -- '.$horario->dia}}</span>
                                            <br>
                                        @endforeach
                                    </td>
                                    <td>{{$detalle->tipo=='R'?'Regular':'Subsanación'}}</td>
                                    <td>
                                        <button onclick="remove_curso({{$detalle->id}},{{$detalle->seccion->id}})"
                                                class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple'
                                                type='button'>
                                            <i class='icon-cross2'></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <div class="form-group row">
                        <label class="col-form-label ">Observaciones: </label>
                        <div class="col-lg-3">
                            <textarea class="form-control"
                                      id="observaciones">{{$detalles->first()->matricula->observaciones??''}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label ">Monto de pago: S/ </label>
                        <div class="col-lg-3">
                            <input class="form-control decimales" type="text" id="monto"
                                   value="{{$detalles->first()->matricula->monto??''}}"
                                   onkeypress="return filterFloat(event,this);">
                        </div>
                    </div>
                    @if($detalles->first()->matricula->estado=='OBSERVADA')
                        <button type="button" class="btn bg-primary legitRipple" onclick="aprobar('POR-CONFIRMAR')">
                            Aprobar
                        </button>
                    @else
                        <button type="button" class="btn bg-primary legitRipple" onclick="aprobar('POR-PAGAR')">
                            Aprobar
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="matricula_id" value="{{$detalles->first()->matricula_id}}">
    {{--    <input type="hidden" id="url_getxmatricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">--}}
    <input type="hidden" id="url_listxpecarrera" value="{{route('pecursos.listxpecarrera',[''])}}/">
    <input type="hidden" id="Url_Get_Curso" value="{{route('pecursos.get_pecurso')}}">
    <input type="hidden" id="Url_remove_detalle" value="{{route('matriculadetalle.remove')}}">
    <input type="hidden" id="Url_Get_SeccionesxCurso" value="{{route('seccion.getSecciondexCurso')}}">
    <input type="hidden" id="Url_addmatriculadetalle" value="{{route('matriculadetalle.store')}}">
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
@endsection
@section('modals')
    <div id="modal-agregar-curso" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Cursos adicionales</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6>Seleccione el cursos adicional en lo que se desee matricular</h6>
                    <div class="table-responsive">
                        <table class="table" id="tb_cursos">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Nivel</th>
                                <th>Créditos</th>
                                <th>H. teoría/H. practica</th>
                                <th>Elegir</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-horario" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Scrollable modal</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6>Seleccione el grupo que desea registrar su horario, luego presione el botón elegir</h6>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Grupo</th>
                                <th>Día/hora</th>
                                <th>Horário</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_horarios">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_detalles').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_detalles(id) {
            var url = $('#url_getxmatricula').val() + id
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#t_body_detalles').html('')
                    data.detalles.forEach(element => {
                        var horarios = "";
                        $.each(element.seccion.horarios, function (index, value) {
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + ' --- ' + value.aula['aula'] + "<br>";
                        });
                        var template = "<tr>"
                            + "<td>" + element.seccion.pe_curso.curso.nombre + "</td>"
                            + "<td>" + horarios + "</td>"
                            + "</tr>"

                        $('#t_body_detalles').append(template)
                    })
                }
            })
        }

        function get_cursos(carrera_id) {
            var id = carrera_id
            var url = $('#url_listxpecarrera').val() + id;
            // var data = {'id': id}
            $.ajax({
                type: 'GET',
                url: url,
                // data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    $('#tbody_cursos').html('')
                    data.pe_cursos.forEach(element => {
                        var template = "<tr>"
                            + "<td>" + element.curso.nombre + "</td>"
                            + "<td>" + element.semestre + "</td>"
                            + "<td><span id='creditos" + element.id + "'>" + element.creditos + "</span></td>"
                            + "<td>" + element.horas_teoria + " / " + element.horas_practica + "</td>"
                            + "<td><button class='btn btn-light' onclick='add_curso(" + element.id + ")' data-dismiss='modal'><i class='icon-plus-circle2'></i>Agregar</button></td>"
                            + "</tr>"
                        $('#tbody_cursos').append(template)
                    })
                    $('#tb_cursos').DataTable({
                        "language": {
                            "url": "{{asset('assets/DataTables/Spanish.json')}}"
                        }
                    })
                }
            })
        }

        function filterFloat(evt, input) {
            // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
            var key = window.Event ? evt.which : evt.keyCode;
            var chark = String.fromCharCode(key);
            var tempValue = input.value + chark;
            if (key >= 48 && key <= 57) {
                if (filter(tempValue) === false) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if (key == 8 || key == 13 || key == 0) {
                    return true;
                } else if (key == 46) {
                    if (filter(tempValue) === false) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }

        function filter(__val__) {
            var preg = /^([0-9]+\.?[0-9]{0,2})$/;
            if (preg.test(__val__) === true) {
                return true;
            } else {
                return false;
            }

        }

        function add_curso(id) {
            var url = $('#Url_Get_Curso').val();
            var data = {'id': id}
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    var template = "<tr>"
                        + "<td>" + data.pe_curso.curso.nombre + "</td>"
                        + "<td>" + data.pe_curso.semestre + "</td>"
                        + "<td><select id='tipo" + data.pe_curso.id + "' class='form-control'>" +
                        "<option  value='R'>Regular</option>" +
                        "<option  value='S'>Subsanación</option>" +
                        "</select></td>"
                        + "<td><button onclick='get_secciones(" + data.pe_curso.id + ")' type='button' class='btn btn-warning legitRipple' data-toggle='modal' data-target='#modal-horario'>Matricular</button></td>"
                        + "</tr>"

                    $('#tbody_detalles').append(template)
                }
            })
        }

        function remove_curso(id,seccion_id) {
            var url = $('#Url_remove_detalle').val()
            var data = {'id': id,'seccion_id':seccion_id}

            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    alert('Curso Eliminado')
                    location.reload()
                }
            })
        }

        function get_secciones(id) {
            var url = $('#Url_Get_SeccionesxCurso').val();
            var data = {'id': id,'tipo':$('#tipo'+id).val()}
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data.secciones)
                    $('#tbody_horarios').html('');
                    data.secciones.forEach(element => {

                        var horarios = "";
                        $.each(element.horarios, function (index, value) {
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                        });

                        var template = "<tr>"
                            + "<td>" + element.pe_curso.curso.nombre + "</td>"
                            + "<td>" + element.seccion + "</td>"
                            + "<td>" +
                            horarios
                            + "</td>"
                            + "<td><button type='button' class='btn btn-light legitRipple' onclick='matricular(" + element.id + "," + id + ")' data-dismiss='modal'>Seleccionar</button></td>"
                            + "</tr>"

                        $('#tbody_horarios').append(template)
                    })
                }
            })
        }

        function matricular(seccion_id, select_id) {

            var url = $('#Url_addmatriculadetalle').val()
            var data = {
                'seccion_id': seccion_id,
                'matricula_id': $('#matricula_id').val(),
                'tipo': $('#tipo' + select_id).val().toString()
            }
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    alert('Detalle agregado')
                    location.reload()
                }
            })
        }

        function aprobar(estado) {
            var url = $('#url_actualizar_matricula').val()
            var id = $('#matricula_id').val()
            var data = {
                'id': id,
                'estado': estado,
                'monto': $('#monto').val(),
                'observaciones': $('#observaciones').val()
            }
            // console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    alert('Matricula aprobada')
                    window.location.href = '/Secretaria_Academica/validar_pre_matricula_sub'
                }
            })
        }

        $(document).ready(function () {
            calcular_pago()
        })

        function calcular_pago() {
            var fijo_matricula = 120
            var total = fijo_matricula


            $('.nivel-subsanacion').each(function () {
                var nivel = parseFloat($(this).text())
                var creditos = parseFloat($(this).siblings('.creditos').text())
                var n_horas = parseFloat($(this).siblings('.horas').text())
                var n_matriculados = parseFloat($(this).siblings('.n_matriculados').text())
                var n_semanas = 16

                total += creditos * 19.5
                if (nivel >= '5') {
                    total += (5 * n_horas * n_semanas) / n_matriculados
                }
            })
            $('#monto').val(total.toFixed(2))
        }
    </script>
@endsection
