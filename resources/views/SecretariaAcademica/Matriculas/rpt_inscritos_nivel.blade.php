@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Reporte Matriculados por
        Nivel
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Reporte matriculados por Nivel</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-form-label ">Period académico:</label>
                        <select id="semestre" class="col-4 form-control">
                            <option>Seleccione un periodo de estudios</option>
                            @foreach($periodos as $periodo)
                                <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                            @endforeach
                        </select>
                        <label class="col-form-label  carrera fade">Nivel:</label>
                        <select id="carrera" class="col-4 carrera fade form-control">
                            <option>Seleccione un nive</option>
                            @foreach($carreras as $carrera)
                                <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                            @endforeach
                        </select>
{{--                        <a id="pdf_generate" target="_blank" class="btn btn-primary pull-right" disabled><i--}}
{{--                                class="icon-file-pdf"></i>Descargar</a>--}}
                    </div>
                    Esta es una pantalla para verificar la pre-matricula de los alumnos <br>

                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>DNI</th>
                                <th>Alumno</th>
                                <th>Teléfono</th>
                                <th>Niv.</th>
                                <th>G.</th>
                                <th>Sec.</th>
                                <th>Apoderado</th>
                                <th>Cat.</th>
                                <th>Monto</th>
                                {{--                                <th>Opciones</th>--}}
                            </tr>
                            </thead>
                            <tbody id="tbody_matriculas">

                            </tbody>
                        </table>
                        <span class="t_alumno fade">Número total de alumnos en el curso:  <strong class="t_alumno fade"
                                                                                                  id="total_alumnos">0</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_rptxnivel" value="{{route('matricula.view_rptxnivel')}}">
    <input type="hidden" id="url_pdf_rptxnivel" value="{{route('matricula.rptxnivel')}}/">
    <input type="hidden" name="" id="url_get_boletaxmatricula" value="{{route('vouchers.getboletaxmatricula')}}">
    <input type="hidden" name="" id="url_get_detx_matricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>
    <script>
        moment.locale('es')
        $(function () {
            $('#semestre').change(function () {
                $('.carrera').removeClass('fade')
                $('#tbody_matriculas').html('')
            })
            $('#carrera').change(function () {
                $('#tbody_matriculas').html('')
                $('#nivel').html('').append(new Option('Seleccione un Grado', ''))
                $('.nivel').removeClass('fade')
                $('#tbody_matriculas').html('')
                $.ajax({
                    url: 'rpt_inscritos_nivel/' + $('#semestre').val() + '/' + $(this).val(),
                    type: 'GET',
                    datatype: 'json',
                    success: function (data) {
                        data.alumnos.forEach(element => {
                            console.log(element.id)
                            if (element.padre_id !== 0)
                                if (element.padre.apoderado === 'si')
                                    var Apo = element.padre.persona.DNI+' - '+ element.padre.persona.paterno + ' ' + element.padre.persona.materno + ' - ' + element.padre.persona.nombres+ ' ' + element.padre.persona.telefono
                            if (element.madre_id !== 0)
                                if (element.madre.apoderado === 'si')
                                    var Apo =element.madre.persona.DNI+' - '+ element.madre.persona.paterno + ' ' + element.madre.persona.materno + ' - ' + element.madre.persona.nombres+ ' ' + element.madre.persona.telefono
                            var template = `<tr>
                                                <td>${element.persona.DNI}</td>
                                                <td>${element.persona.paterno} ${element.persona.materno} ${element.persona.nombres}</td>
                                                <td>${element.persona.telefono??'-'}</td>
                                                <td>${element.alumnocarreras[element.alumnocarreras.length - 1].carrera.nombre}</td>
                                                <td>${element.alumnocarreras[element.alumnocarreras.length - 1].ciclo + '°'}</td>
                                                <td>${element.alumnocarreras[element.alumnocarreras.length - 1].grupo}</td>
                                                <td>${Apo}</td>
                                                <td>${element.categorias[element.categorias.length - 1].nombre}</td>
                                                <td>${element.categorias[element.categorias.length - 1].monto}</td>
                                            </tr>`
                            $('#tbody_matriculas').append(template)
                        })
                        if (data.alumnos.length > 0) {
                            $('#table_matriculas').DataTable({
                                "language": {
                                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                                }
                            })
                        }
                    }
                })

            })
        })
    </script>
@endsection


