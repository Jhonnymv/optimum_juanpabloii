<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document sections</title>

    <style>

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
             border: 2px solid black;
        }

        table {
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">
<table style="margin-top: -20px">
    <tr>
        <td style="text-align: center; width: 15%">
            <img src="{{public_path()}}/assets/img/circular/2.png" style="width: 10%;padding-bottom: -25px" alt="">
{{--            <img src="{{asset('assets/img/circular/2.png')}}" style="width: 10%;padding-bottom: -25px" alt="">--}}
        </td><p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCATIVA PARTICULAR
            “JUAN PABLO II”</p>
        <td style="text-align: center; width: 70%">
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE
                ADMISIÓN Y REGISTRO ACADÉMICO</p>
            <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">NIVEL {{strtoupper($carrera->nombre)}}</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">REPORTE DE ALUMNOS POR SECCIÓN</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">"JUAN PABLO II"</p>
        </td>

    </tr>
</table>

<span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>

<table>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{strtoupper($carrera->nombre)}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Grado
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{strtoupper($grado)}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCIÓN
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{strtoupper($grupo)}}
        </td>
    </tr>

</table>
<span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>

<br>
<div class="row mt-0 font-size-sm">
    <table class="table table-sm table-striped" id="t_regulares">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial; color: white">Nº</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial; color: white">Código</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 30%; font-family:arial;color: white">Alumno</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">Nivel</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">Grado</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">Sección</th>
        </tr>
        </thead>

        <tbody>
        @foreach($alumnos as $item)
                <tr>
                    <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;"> {{$loop->index + 1}}</td>
                    <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;"> {{$item->alumno->persona->DNI}}</td>
                    <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;">{{strtoupper($item->alumno->persona->paterno.' '.$item->alumno->persona->materno.' '.$item->alumno->persona->nombres)}}</td>
                    <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;">{{strtoupper($carrera->nombre)}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {{$item->ciclo}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$item->grupo}}
                </tr>
        @endforeach

        <tr>

        </tr>
        </tbody>
    </table>

</div>
<br>

<div id="footer" class="page-break">
    <div class="page-number"></div>
</div>

</body>
</html>
