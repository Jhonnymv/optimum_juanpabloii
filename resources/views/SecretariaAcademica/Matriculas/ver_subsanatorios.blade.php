@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Pre-matriculas
        con cursos de subsanación
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar matriculas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar la pre-matricula de los alumnos <br>
                    <div class="row">
                        <label class="col-form-label ">Curso de subsanación:</label>
                        <select id="pe_curso_id" class="col-10 form-control">
                            <option>Seleccione un Curso</option>
                            @foreach($pe_cursos as $pe_curso)
                                <option
                                    value="{{$pe_curso->id}}">{{$pe_curso->curso->nombre.' - '.$pe_curso->semestre.' Ciclo'}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>Alumno</th>
                                <th>Carrera</th>
                                <th>Ciclo</th>
                                <th>fecha de registro</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            </tbody>
                        </table>
                        <span class="t_alumno fade">Número total de alumnos en el curso:  <strong class="t_alumno fade"
                                                                                                  id="total_alumnos">0</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_listmatricula" value="{{route('matricula.prematriculas_sub')}}/">
    <input type="hidden" id="url_gedetallematricula" value="{{route('detalle_matricula.getxmatricula_sub',[''])}}/">
@endsection

@section('modals')
    {{--    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">--}}
    {{--        <div class="modal-dialog modal-lg">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h5 class="modal-title">Basic modal</h5>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal">×</button>--}}
    {{--                </div>--}}

    {{--                <div class="modal-body">--}}
    {{--                    <table class="table">--}}
    {{--                        <thead>--}}
    {{--                        <th>Curso</th>--}}
    {{--                        <th>Horario</th>--}}
    {{--                        </thead>--}}
    {{--                        <tbody id="t_body_detalles">--}}

    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}

    {{--                <div class="modal-footer">--}}
    {{--                    <input type="hidden" id="matricula_id">--}}
    {{--                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>--}}
    {{--                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar()">Aprobar</button>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">--}}
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>
    <script>
        $('#pe_curso_id').change(function () {
            var url = $('#url_listmatricula').val()
            var data = {'pe_curso_id': $(this).val()}
            // console.log(url)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#tbody_cursos').html('')
                    data.matriculas.forEach(element => {
                        var date = moment(element.created_at)
                        var url_matricula =$('#url_gedetallematricula').val()+element.id
                        var template = "<tr>" +
                            "<td>" + element.alumnocarrera.alumno.persona.paterno + " " + element.alumnocarrera.alumno.persona.materno + ", " + element.alumnocarrera.alumno.persona.nombres + "</td>"
                            + "<td>" + element.alumnocarrera.carrera.nombre + "</td>"
                            +"<td>"+element.matriculadetalles[0].seccion.pe_curso.semestre+"</td>"
                            + "<td>" + date.format('l') + "</td>"
                            + "<td>" +
                            "<a href='"+url_matricula+"' class='btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple' title='Ver detalles'><i class='icon-eye'></i></a>" +
                            "</td>"
                            + "</tr>"
                        $('#tbody_cursos').append(template)
                    })
                    $('.t_alumno').removeClass('fade')
                    $('#total_alumnos').text(data.matriculas.length)
                }
            })
        })
    </script>
@endsection
