@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Licencias')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Aprobar Licencias
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/media/fancybox.min.js"></script>

    <script src="/assets/global_assets/js/demo_pages/gallery.js"></script>
    <!-- /theme JS files -->
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Aprobar Licencias de estudios</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de licencias por aprobar</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los pagos realizados por Licencia de Estudios
                    <div class="table-responsive">
                        <table class="table" id="table_licencias">
                            <thead>
                            <tr>

                                <th>Alumno</th>
                                <th>Correo</th>
                                <th>Carrera</th>
                                <th>Fecha de registro</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            @foreach($licencias as $licencia)
                                <tr>

                                    <td>{{ $licencia->matricula->alumnocarrera->alumno->persona->paterno.' '.$licencia->matricula->alumnocarrera->alumno->persona->materno.', '.$licencia->matricula->alumnocarrera->alumno->persona->nombres}}</td>

                                    <td>{{ $licencia->matricula->alumnocarrera->alumno->persona->email}}</td>
                                    <td>{{ $licencia->matricula->alumnocarrera->carrera->nombre}}</td>
                                    <td>{{date('d-m-Y',strtotime( $licencia->created_at))}}</td>
                                    <td>{{ $licencia->estado}}</td>
                                    <td>
                                      <button type="button"
                                                 onclick="get_licencia({{ $licencia->id}})"

                                                class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                title="Revisar Licencia">
                                            <i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_getlicencia" value="{{route('licencia.getxlicencia',[''])}}/">
@endsection

@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalla de Licencia</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">

                            <fieldset>
                                <legend class="font-weight-semibold">Detalle de Licencia
                                </legend>
                                <div class="form-group">
                                    <label>Código de Licencia:</label>
                                    <span id="id" name="id"></span>
                                </div>
                                <input type="hidden" id="lic">
                                <div class="form-group">
                                    <label>Fecha:</label>
                                    <span id="fecha"></span>
                                </div>
                                <div class="form-group">
                                    <label>Número de periodos solicitados:</label>
                                     <span id="duracion"></span>
                                </div>
                                <div class="form-group">
                                    <label>Descripción: </label>
                                    <span id="descripcion"></span>
                                </div>
                                <div class="form-group">

                            </div>

                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"> Resolución:
                                </legend>
                                <label>Número de Resolución: </label>
                                <input type="text" id="resolucion">

                                <br>
                                <label>Observaciones: </label>
                                <textarea  rows="10" cols="40" id="observacion">

                                </textarea>
{{--                                <img id="img_comprobante" style="height: 20em;width: 20em">--}}
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar()">Aprobar</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_actualizar_licencia" value="{{route('licencia.actualizarestadob')}}">
    <div id="modal-detalle_activada" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalla de Licencia Activa</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">

                            <fieldset>
                                <legend class="font-weight-semibold"> Detalle de Licencia
                                </legend>
                                <div class="form-group">
                                    <label>Código de Licencia:</label>
                                    <span id="idb" name="id"></span>
                                </div>
                                <input type="hidden" id="lic">
                                <div class="form-group">
                                    <label>Fecha:</label>
                                    <span id="fechab"></span>
                                </div>
                                <div class="form-group">
                                    <label>Número de periodos solicitados:</label>
                                     <span id="duracionb"></span>
                                </div>
                                <div class="form-group">
                                    <label>Descripción: </label>
                                    <span id="descripcionb"></span>
                                </div>
                                <div class="form-group">

                            </div>

                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"> Resolución:
                                </legend>
                                <label>Número de Resolución: </label>
                                <span id="resolucionb"></span>
                                <br>
                                <label>Fecha de registro de resolución: </label>
                                <span id="fecha_cierre"></span>
                                <br>
                                <label>Observaciones: </label>
                                <span  rows="10" cols="40" id="observacionb">

                                </span>
{{--                                <img id="img_comprobante" style="height: 20em;width: 20em">--}}
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_licencias').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_licencia(id) {
            var url = $('#url_getlicencia').val() + id

             console.log(url)
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                     //console.log(data)
                     if(data.licencia.estado == 'ACTIVA')
                     {
                    $('#idb').text(data.licencia.id)
                    $('#licb').val(data.licencia.id)
                    $('#fechab').text(data.licencia.fecha_registro)
                    $('#duracionb').text(data.licencia.duracion)
                    $('#descripcionb').text(data.licencia.descripcion)
                    $('#resolucionb').text(data.licencia.resolucion)
                    $('#observacionb').text(data.licencia.observaciones)
                    $('#fecha_cierre').text(data.licencia.fecha_cierre)
                    $("#modal-detalle_activada").modal('show');
                    }
                    else
                    {
                    $('#id').text(data.licencia.id)
                    $('#lic').val(data.licencia.id)
                    $('#fecha').text(data.licencia.fecha_registro)
                    $('#duracion').text(data.licencia.duracion)
                    $('#descripcion').val(data.licencia.descripcion)
                    $('#resolucion').val(data.licencia.resolucion)
                    $('#observacion').val(data.licencia.observaciones)
                    $("#modal-detalle").modal('show');
                    }


                }
            })

        }


        function aprobar() {
            var url = $('#url_actualizar_licencia').val()
            var id = $('#lic').val()
            var f = new Date();
            var observaciones = $('#observacion').val()
            var resolucion = $('#resolucion').val()

            var data = {'id': id, 'estado': 'ACTIVA', 'observaciones' : observaciones, 'resolucion' : resolucion, 'fecha_cierre' : f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate()}
            console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                     console.log("succes")
                    location.reload()
                }
            })
        }

    </script>
@endsection
