@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Registro de Padres de
        familia y estudiantes
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <style>
        label.error {
            color: red;
            font-size: 1rem;
            display: block;
            margin-top: 5px;
        }
    </style>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Registro de Padres de familia y estudiantes</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Registro de Padres de familia y estudiantes</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <form action="{{route('saveSinceSecretaria')}}" method="POST" id="generalForm">
                    @csrf
                    <div class="card-body">
                        <h5>Datos del padre</h5>
                        <div class="row form-group">
                            <div class="col-12 col-md-4">
                                <label for="dnip" class="col-12">DNI:</label>
                                <input type="text" id="dnip" name="dnip" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="paternop" class="col-12">Apellido Paterno:</label>
                                <input type="text" id="paternop" name="paternop" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="maternop" class="col-12">Apellido Materno:</label>
                                <input type="text" id="maternop" name="maternop" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 col-md-4">
                                <label for="nombresp" class="col-12">Nombres:</label>
                                <input type="text" id="nombresp" name="nombresp" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="telefonop" class="col-12">Teléfono:</label>
                                <input type="text" id="telefonop" name="telefonop" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-1">
                                <label for="apoderadoP">Apoderado</label>
                                <input type="radio" id="apoderadoP" name="apoderado" value="1" checked required>
                            </div>
                        </div>
                        <hr>
                        <h5>Datos de la madre</h5>
                        <div class="row form-group">
                            <div class="col-12 col-md-4">
                                <label for="dnim" class="col-12">DNI:</label>
                                <input type="text" id="dnim" name="dnim" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="paternop" class="col-12">Apellido Paterno:</label>
                                <input type="text" id="paternom" name="paternom" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="maternom" class="col-12">Apellido Materno:</label>
                                <input type="text" id="maternom" name="maternom" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 col-md-4">
                                <label for="nombresm" class="col-12">Nombres:</label>
                                <input type="text" id="nombresm" name="nombresm" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="telefonom" class="col-12">Teléfono:</label>
                                <input type="text" id="telefonom" name="telefonom" class="form-control" required>
                            </div>
                            <div class="col-12 col-md-1">
                                <label for="apoderadoM">Apoderado</label>
                                <input type="radio" id="apoderadoM" name="apoderado" value="0">
                            </div>
                        </div>
                        <hr>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal_addStudent">Agregar
                            Estudiante
                        </button>
                        <h5>Datos de los estudiantes</h5>
                        <div class="row">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    {{--                                <th>#</th>--}}
                                    <th>DNI</th>
                                    <th>Nombre</th>
                                    <th>Teléfono</th>
                                    <th>Nivel</th>
                                    <th>Grado</th>
                                    <th>Sección</th>
                                </tr>
                                </thead>
                                <tbody id="dataAlumnos">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <div class="row">
                            <div class="col-12" style="text-align: center">
                                <button class="btn btn-primary" type="button" onclick="saveForm()">Guardar datos
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('modals')
    <div id="modal_addStudent" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Cursos adicionales</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body py-0">
                    <div class="row form-group">
                        <div class="col-12 col-md-2">
                            <label for="dniStudent" class="col-12">DNI:</label>
                            <input type="text" id="dniStudent" name="dniStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="paternoStudent" class="col-12">Apellido Paterno:</label>
                            <input type="text" id="paternoStudent" name="paternoStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="maternoStudent" class="col-12">Apellido Materno:</label>
                            <input type="text" id="maternoStudent" name="maternoStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <label for="nombresStudent" class="col-12">Nombres:</label>
                            <input type="text" id="nombresStudent" name="nombresStudent" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 col-md-4">
                            <label for="telefonoStudent" class="col-12">Teléfono:</label>
                            <input type="text" id="telefonoStudent" name="telefonoStudent" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <label for="nuevoStudent">Nuevo</label>
                            <select name="nuevoStudent" id="nuevoStudent" class="form-control">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="nuevoStudent">Coolegio de Procedencia</label>
                            <input type="text" name="originStudent" id="originStudent" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-5">
                            <label for="carreraIdStudent" style="font-weight: bold; margin: 0">Nivel: (<label
                                    for="" style="color: red;">*</label>)</label></label>
                            <select name="carreraIdStudent" id="carreraIdStudent" class="form-control">
                                <option value="">Seleccione un Nivel</option>
                                @foreach($carreras as $carrera)
                                    <option
                                        value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label for="ciclo" style="font-weight: bold; margin: 0">Grado: (<label
                                    for="" style="color: red;">*</label>)</label></label></label>
                            <select name="ciclo" id="ciclo" class="form-control">
                                <option>Seleccione Grado</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="seccionStudent">Seccion</label>
                            <select name="seccionStudent" id="seccionStudent" class="form-control">
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12" style="text-align: center">
                            <button type="button" class="btn btn-info" id="addStudent" onclick="addStudent()">Agregar
                                Estudiante
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        function getDatos(valor, ap, am, nom) {
            $.ajax({
                url: '/searchPerson/' + valor,
                type: 'GET',
                datatype: 'json',
                success: function (data) {
                    $('#' + ap).val(data.apellidoPaterno)
                    $('#' + am).val(data.apellidoMaterno)
                    $('#' + nom).val(data.nombres)
                }
            })
        }

        $(document).ready(function () {
            $("#dnip").keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    getDatos($(this).val(), 'paternop', 'maternop', 'nombresp')
                }
            });
            $("#dnim").keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    getDatos($(this).val(), 'paternom', 'maternom', 'nombresm')
                }
            });
            $("#dniStudent").keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    getDatos($(this).val(), 'paternoStudent', 'maternoStudent', 'nombresStudent')
                }
            });
            $('#carreraIdStudent').change(function () {
                $('#ciclo').html('').append(new Option('Seleccione un Grado', ''))
                $.ajax({
                    url: '/getgrades/' + $(this).val(),
                    type: 'GET',
                    datatype: 'json',
                    success: function (data) {
                        data.grades.forEach(element => {
                            $('#ciclo').append(new Option(element.grade + ' - ' + element.description, element.grade))
                        })
                    }
                })
            })
            $('#generalForm').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action')
                var data = $(this).serialize();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    datatype: 'josn',
                    success: function (data) {
                        alert(data)
                        window.location.reload()
                    }
                })
            })
        });

        function addStudent() {
            var template = "<tr>" +
                "<td style='width: 10%'><input class='dniStudent form-control' type='text' name='sDni[]' style='width: 100%' value='" + $('#dniStudent').val() + "' readonly></td>" +
                "<td style='width: 45%'>" +
                "<input type='hidden' name='sPaterno[]' value='" + $('#paternoStudent').val() + "'><input type='hidden' name='sMaterno[]' value='" + $('#maternoStudent').val() + "'><input type='hidden' name='sNombres[]' value='" + $('#nombresStudent').val() + "'>" +
                "<input type='text' class='form-control' style='width: 100%' value='" + $('#paternoStudent').val() + ' ' + $('#maternoStudent').val() + ' ' + $('#nombresStudent').val() + "' readonly>" +
                "</td>" +
                "<td style='width: 15%'><input type='text' class='form-control' name='sTelefono[]' style='width: 100%' value='"+$('#telefonoStudent').val()+"' readonly></td>"+
                "<td style='width: 10%'><input type='text' class='form-control' name='sNivel[]' style='width: 100%' value='" + $('#carreraIdStudent').val() + "' readonly></td>" +
                "<td style='width: 10%'><input type='text' class='form-control' name='sCiclo[]' style='width: 100%' value='" + $('#ciclo').val() + "' readonly>" +
                "<td style='width: 10%'><input type='text' class='form-control' name='sSeccion[]' style='width: 100%' value='" + $('#seccionStudent').val() + "' readonly>" +
                "<input type='hidden' value='"+$('#nuevoStudent').val()+"' name='sNuevo[]'>"+
                "<input type='hidden' value='"+$('#originStudent').val()+"' name='sOrigin[]'>"+
                "</td>" +
                "</tr>"

            $('#dataAlumnos').append(template)
            $('#modal_addStudent').modal('hide')
            $('#dniStudent').val('')
            $('#paternoStudent').val('')
            $('#maternoStudent').val('')
            $('#nombresStudent').val('')
            $('#telefonoStudent').val('')
            $('#carreraIdStudent').val('')
            $('#seccionStudent').val('')
            $('#ciclo').val('')
            $('#nuevoStudent').val('')
            $('#originStudent').val('')
        }

        function saveForm() {
            if ($("#generalForm").validate())
                $('#generalForm').submit()
        }

        // $(document).ready(function() {
        //     $("#generalForm").validate();
        // });
    </script>
@endsection
