@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Pre-matriculas
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar matriculas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    Esta es una pantalla para verificar la matricula de:
                    <em><strong>{{strtoupper($alumno->first()->alumno->persona->nombres)}}   {{strtoupper($alumno->first()->alumno->persona->paterno)}}  {{strtoupper($alumno->first()->alumno->persona->materno)}}</strong></em>
                    <div class="header-elements">
                        @if(isset($contrato))
                            @if(!isset($detalles))
                                {{--                        @if(count($detalles) == 0)--}}
                                <div class="header-elements-inline">
                                    <a href="{{route('contrato.getcontrato',[$contrato->first()->id])}}"
                                       class="btn btn-large pull-right"><i class="icon-download-alt"></i>Descargar
                                        Convenio</a>

                                    <a href="{{route('voucher.getvoucher',[$alumno->first()->id])}}"
                                       class="btn btn-large pull-right"><i class="icon-download-alt"></i>Descargar
                                        Voucher</a>

                                    <button onclick="generar_matricula({{$alumno->first()->id}})"
                                            type="button" class="btn btn-success btn-sm pull-right">Generar Matrícula
                                    </button>
                                </div>
{{--                            @else--}}
{{--                                <div class="header-elements">--}}
{{--                                    <button--}}
{{--                                        onclick="get_cursos({{$detalles->first()->matricula->alumnocarrera->carrera_id}})"--}}
{{--                                        type="button" class="btn btn-success btn-sm pull-right"--}}
{{--                                        data-toggle="modal" data-target="#modal-agregar-curso">Agregar curso--}}
{{--                                    </button>--}}
{{--                                </div>--}}
                    </div>
                </div>

                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table" id="table_detalles">
                            <thead>
                            <tr>
                                <th>Curso</th>
                                <th>Grado</th>
                                <th>horario</th>
                                <th>tipo</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_detalles">
                            @foreach($detalles as $detalle)
                                <tr>
                                    <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                                    <td class="nivel-{{$detalle->tipo=='R'?'regular':'subsanacion'}}">{{$detalle->seccion->pe_curso->semestre}}</td>
                                    </td>
                                    <td>
                                        @foreach($detalle->seccion->horarios as $horario)
                                            <span>{{$horario->hora_inicio.' - '.$horario->hora_fin.' -- '.$horario->dia}}</span>
                                            <br>
                                        @endforeach
                                    </td>
                                    <td>{{$detalle->tipo=='R'?'Regular':'Subsanación'}}</td>
                                    <td>
                                        <button onclick="remove_curso({{$detalle->id}})"
                                                class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple'
                                                type='button'>
                                            <i class='icon-cross2'></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-center">
                    @if($detalles->first()->matricula->estado=='OBSERVADA')
                        <div class="form-group row">
                            <label class="col-form-label ">Observaciones: </label>
                            <div class="col-lg-3">
                                <textarea
                                    class="form-control">{{$detalles->first()->matricula->observaciones}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label ">Monto de pago: S/ </label>
                            <div class="col-lg-3">
                                <input class="form-control decimales" type="text" id="monto"
                                       value="0.00"
                                       onkeypress="return filterFloat(event,this);">
                            </div>
                        </div>
                    @else
                        <div class="form-group row">
                            <label class="col-form-label ">Monto de pago:
                                {{--                                S/ {{$alumno_pago->first()->fe_precio_und}}</label>--}}
                            </label>


                        </div>
                    @endif
                    <a type="button" target="_blank" class="btn bg-primary legitRipple"
                       href="{{route('matriculas.ReporteMatricula',['alumno_id'=>$alumno->first()->alumno->id,'matricula_id'=>$detalle->matricula_id])}}">
                        <i class="icon-printer2"></i> Ficha Matricula
                    </a>
                    {{--                    --}}{{--                    @if($detalles->first()->matricula->estado=='OBSERVADA')--}}
                    {{--                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar('POR-CONFIRMAR')">--}}
                    {{--                        Para Confirmar--}}
                    {{--                    </button>--}}
                    {{--                    --}}{{--                    @else--}}
                    {{--                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar('POR-PAGAR')">--}}
                    {{--                        Para pagar--}}
                    {{--                    </button>--}}
                    {{--                    --}}{{--                    @endif--}}
                    {{--                    <button type="button" class="btn bg-primary-300" onclick="aprobar('PAGADA')">Enviar a contabilidad--}}
                    {{--                    </button>--}}

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="matricula_id" value="{{$detalles->first()->matricula_id}}">
    <input type="hidden" id="carrera_id" value="{{$detalles->first()->matricula->alumnocarrera->carrera_id}}">
    <input type="hidden" id="alumno_id" value="{{$detalle->first()->matricula->alumnocarrera->alumno_id}}">
    <input type="hidden" id="alumno_grupo" value="{{$detalle->first()->matricula->alumnocarrera->grupo}}">
    {{--    <input type="hidden" id="url_getxmatricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">--}}
    <input type="hidden" id="url_listxpecarrera" value="{{route('pecursos.sec.listxpecarrera',[''])}}/">

    <input type="hidden" id="Url_Get_Curso" value="{{route('pecursos.get_pecurso')}}">
    <input type="hidden" id="Url_calcular_pago" value="{{route('matriculadetalle.calcular_pago')}}">
    <input type="hidden" id="Url_remove_detalle" value="{{route('matriculadetalle.remove')}}">
    <input type="hidden" id="Url_Get_SeccionesxCurso" value="{{route('seccion.getSecciondexCurso')}}">
    <input type="hidden" id="Url_addmatriculadetalle" value="{{route('matriculadetalle.storeb')}}">
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
    @endif
    @endif
    {{--    @endif--}}
    <input type="hidden" id="url_gen_matricula" value="{{route('matricula.getxmatricula_sec',[''])}}/">
    <input type="hidden" id="url_getcontrato" value="{{route('contrato.getcontrato',[''])}}/">
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
@section('modals')
    <div id="modal-agregar-curso" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Agregar Cursos</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6>Seleccione el cursos que desea agregar</h6>
                    <div class="table-responsive">
                        <table class="table" id="tb_cursos">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Grado</th>
                                <th>Elegir</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-horario" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Scrollable modal</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6>Seleccione el grupo que desea registrar su horario, luego presione el botón elegir</h6>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Grupo</th>
                                <th>Día/hora</th>
                                <th>Horário</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_horarios">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Detalle de contrato</h3>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">


                    <div class="col-md-6">

                        <legend class="font-weight-semibold"><i class="icon-image2 mr-2"></i> Imagen del
                            Voucher
                        </legend>
                        <div class="card">
                            <div class="card-img-actions m-1">
                                <iframe style="width: 100%; height: 300px" id="img_voucher" frameborder="0"></iframe>
                            </div>
                        </div>
                        {{--                                <img id="img_comprobante" style="height: 20em;width: 20em">--}}

                    </div>

                </div>

                <div class="modal-footer">
                    <input type="hidden" id="postulacion_id">
                    <input type="hidden" id="postulante_id">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    {{--                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar()">Enviar</button>--}}
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_actualizar_postulante" value="{{route('postulante.actualizarestado')}}">
    <div id="modal-preview" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo-modal-contenidos"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>


                <div class="modal-body">
                    <iframe id="pre_view" frameborder="0" height="500em" width="100%"></iframe>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_detalles').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function generar_matricula(id_alumno) {
            var url = $('#url_gen_matricula').val() + id_alumno
            //console.log(url)
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    //console.log(data)
                    if (data == 'error')
                        alert('El alumno no registra pago por matrícula')
                    else
                        alert("El registro fue generado exitosamente")
                    location.reload()
                }
            })

        }

        function get_detalles(id) {
            var url = $('#url_getxmatricula').val() + id
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#t_body_detalles').html('')
                    data.detalles.forEach(element => {
                        var horarios = "";
                        $.each(element.seccion.horarios, function (index, value) {
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + ' --- ' + value.aula['aula'] + "<br>";
                        });
                        var template = "<tr>"
                            + "<td>" + element.seccion.pe_curso.curso.nombre + "</td>"
                            + "<td>" + horarios + "</td>"
                            + "</tr>"

                        $('#t_body_detalles').append(template)
                    })
                }
            })
        }

        function get_cursos(carrera_id) {
            var id = carrera_id
            var url = $('#url_listxpecarrera').val() + id;
            // var data = {'id': id}
            //console.log(url)
            $.ajax({
                type: 'GET',
                url: url,
                // data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#tbody_cursos').html('')
                    data.pe_cursos.forEach(element => {
                        var template = "<tr>"
                            + "<td>" + element.curso.nombre + "</td>"
                            + "<td>" + element.semestre + "</td>"
                            + "<td><button class='btn btn-light' onclick='add_curso(" + element.id + ")' data-dismiss='modal'><i class='icon-plus-circle2'></i>Agregar</button></td>"
                            + "</tr>"
                        $('#tbody_cursos').append(template)
                    })
                    $('#tb_cursos').DataTable({
                        "language": {
                            "url": "{{asset('assets/DataTables/Spanish.json')}}"
                        }
                    })
                }
            })
        }

        function filterFloat(evt, input) {
            // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
            var key = window.Event ? evt.which : evt.keyCode;
            var chark = String.fromCharCode(key);
            var tempValue = input.value + chark;
            if (key >= 48 && key <= 57) {
                if (filter(tempValue) === false) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if (key == 8 || key == 13 || key == 0) {
                    return true;
                } else if (key == 46) {
                    if (filter(tempValue) === false) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }

        function filter(__val__) {
            var preg = /^([0-9]+\.?[0-9]{0,2})$/;
            if (preg.test(__val__) === true) {
                return true;
            } else {
                return false;
            }

        }

        function add_curso(id) {
            var url = $('#Url_Get_Curso').val();
            var data = {'id': id}
            //console.log(data);
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    var template = "<tr>"
                        + "<td>" + data.pe_curso.curso.nombre + "</td>"
                        + "<td id='semestre" + data.pe_curso.id + "'>" + data.pe_curso.semestre + "</td>"
                        + "<td><select id='tipo" + data.pe_curso.id + "' class='form-control'>" +
                        "<option  value='R'>Regular</option>" +
                        "<option  value='S'>Subsanación</option>" +
                        "</select></td>"
                        + "<td><button onclick='get_secciones(" + data.pe_curso.id + ")' type='button' class='btn btn-warning legitRipple' data-toggle='modal' data-target='#modal-horario'>Matricular</button></td>"
                        + "</tr>"

                    $('#tbody_detalles').append(template)
                }
            })
        }

        function remove_curso(id) {
            var url = $('#Url_remove_detalle').val()
            var data = {'id': id}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    alert('Curso Eliminado')
                    location.reload()
                    // calcular_pago()
                }
            })
        }

        function get_secciones(id) {
            var url = $('#Url_Get_SeccionesxCurso').val();
            var data = {
                'id': id,
                'tipo': $('#tipo' + id).val(),
                'semestre': $('#semestre' + id).text(),
                'carrera_id': $('#carrera_id').val(),
                'alumno_id': $('#alumno_id').val(),
                'grupo': $('#alumno_grupo').val()
            }
            //console.log(data)
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    //console.log(data)
                    $('#tbody_horarios').html('');
                    data.secciones.forEach(element => {

                        var horarios = "";
                        $.each(element.horarios, function (index, value) {
                            // console.log(value.dia+' / '+value.hora_inicio+' - '+value.hora_fin)
                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                        });

                        var template = "<tr>"
                            + "<td>" + element.pe_curso.curso.nombre + "</td>"
                            + "<td>" + element.seccion + "</td>"
                            + "<td>" +
                            horarios
                            + "</td>"
                            + "<td><button type='button' class='btn btn-light legitRipple' onclick='matricular(" + element.id + "," + id + ")' data-dismiss='modal'>Seleccionar</button></td>"
                            + "</tr>"

                        $('#tbody_horarios').append(template)
                    })
                }
            })
        }

        function matricular(seccion_id, select_id) {

            var url = $('#Url_addmatriculadetalle').val()
            var data = {
                'seccion_id': seccion_id,
                'matricula_id': $('#matricula_id').val(),
                'tipo': $('#tipo' + select_id).val().toString()
            }
            //console.log("datasec"+data)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log("data" + data)
                    if (data == 'error')
                        alert('Registro duplicado')
                    else
                        alert('Detalle agregado')
                    // calcular_pago()
                    location.reload()

                },
            })
        }

        function aprobar(estado) {
            var url = $('#url_actualizar_matricula').val()
            var id = $('#matricula_id').val()
            var data = {'id': id, 'estado': estado, 'monto': $('#monto').val()}
            // console.log($('#monto').val())
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    alert('Matricula aprobada')
                    window.location.href = '/Secretaria_Academica/validar_pre_matricula'
                }
            })
        }


        // $(document).ready(function () {
        //     calcular_pago()
        // })

        // function calcular_pago() {
        //
        //     var url = $('#Url_calcular_pago').val()
        //     var data = { 'matricula_id': $('#matricula_id').val(),'alumno_id':$('#alumno_id').val()}
        //     console.log("data"+data)
        //     $.ajax({
        //         url: url,
        //         type: 'GET',
        //         data: data,
        //         dataType: 'json',
        //         success: function (data) {
        //             //console.log(data)
        //         },
        //     })
        //     //$('#monto').val(data.monto.toFixed(2))
        // }
    </script>
@endsection
