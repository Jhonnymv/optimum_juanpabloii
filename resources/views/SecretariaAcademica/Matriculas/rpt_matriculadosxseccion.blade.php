@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Reporte Matriculados por
        sección
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Reporte matriculados por seccion</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <label class="col-form-label ">Period académico:</label>
                            <select id="periodo_id" class="form-control">
                                <option>Seleccione un periodo de estudios</option>
                                @foreach($periodos as $periodo)
                                    <option value="{{$periodo->id}}">{{$periodo->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <label class="col-form-label ">Nivel:</label>
                            <select id="nivel_id" class="select-sm form-control">
                                <option>Seleccione un programa de estudios</option>
                                @foreach($carreras as $carrera)
                                    <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-md-2">
                            <label class="col-form-label ">Grado:</label>
                            <select name="grado" id="grado" class="form-control">
                                <option>Seleccione Grado</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-2">
                            <label class="col-form-label ">Sección:</label>
                            <select id="grupo" class="select-sm form-control">
                                <option value="0">Seleccione una seccion</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                            </select>
                        </div>

                        <div class="col-12 col-md-2" style="display: none" id="descargar">
                            <a id="pdf_generate" target="_blank" class="btn btn-primary pull-right" ><i
                                    class="icon-file-pdf" disabled></i>Descargar</a>
                        </div>
                    </div>

                    Esta es una pantalla para verificar la pre-matricula de los alumnos <br>

                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Alumno</th>
                                <th>DNI</th>
                                <th>Teléfono</th>
                                <th>Carrera</th>
                                <th>fecha de registro</th>
                                {{--                                <th>Opciones</th>--}}
                            </tr>
                            </thead>
                            <tbody id="tbody_matriculas">

                            </tbody>
                        </table>
                        <span class="t_alumno fade">Número total de alumnos en el curso:  <strong class="t_alumno fade"
                                                                                                  id="total_alumnos">0</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_listxseccion" value="{{route('matricula.view_rptxseccion')}}">
    <input type="hidden" id="url_listxseccionpdf" value="{{route('matricula.rptxseccionpdf')}}/">
    <input type="hidden" name="" id="url_get_boletaxmatricula" value="{{route('vouchers.getboletaxmatricula')}}">
    <input type="hidden" name="" id="url_get_detx_matricula" value="{{route('detalle_matricula.getxmatricula',[''])}}/">
@endsection
@section('modals')
    <div id="modal_img" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Disable keyboard interaction</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <span>Número de boleta: <strong id="num_boleta"></strong></span><br>
                    <span>Fecha: <strong id="fecha_boleta"></strong></span><br>
                    <span>Monto: <strong id="monto_boleta"></strong></span><br>

                    <iframe id="img_boleta" style="width: 100%;height: 50em"></iframe>
                </div>

            </div>
        </div>
    </div>
    <div id="modal_detalles" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lista de Cursos matriculados</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <table id="t_detalles" class="table table-striped">
                        <thead>
                        <th>Curso</th>
                        <th>Ciclo</th>
                        <th>Tipo</th>
                        </thead>
                        <tbody id="t_body_detalles">
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>
    <script>
        moment.locale('es')

        // function descargar (){
        //         var periodo_id = $('#periodo_id').val()
        //         var nivel_id = $('#nivel_id').val()
        //         var grado = $('#grado').val()
        //         var grupo = $('#grupo').val()
        //         var url = $('#url_listxseccionpdf').val()
        //
        // }

        $(function () {
            $('#grupo').change(function () {
                var periodo_id = $('#periodo_id').val()
                var nivel_id = $('#nivel_id').val()
                var grado = $('#grado').val()
                var grupo = $('#grupo').val()
                var url = $('#url_listxseccion').val()
                var urlpdf= $('#url_listxseccionpdf').val()+ periodo_id+'/'+nivel_id+'/'+grado+'/'+grupo;
                console.log(urlpdf)
                var datos = {
                    'periodo_id': periodo_id,
                    'nivel_id': nivel_id,
                    'grado': grado,
                    'grupo': grupo
                }
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: datos,
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data)
                        $('#tbody_matriculas').html('')
                        var num = 1;
                        data.registrados.forEach(element => {
                            var date = moment(element.created_at)
                            var alumno = element.alumno.persona
                            var template = "<tr>" +
                                "<td>" + num + "</td>" +
                                "<td>" + alumno.paterno + " " + alumno.materno + ", " + alumno.nombres + "</td>" +
                                "<td>" + alumno.DNI +  "</td>" +
                                "<td>" + alumno.telefono + "</td>" +
                                "<td>" + element.carrera.nombre + "</td>" +
                                // "<td>" + element.created_at + "" +
                                "<td>" + date.format('L') + "" +
                                // "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_detalles(" + element.id + ")' title='Ver detalles'><i class='icon-eye' ></i>Ver Detalles</button>" +
                                // "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_img(" + element.id + ")' title='Ver boleta'><i class='icon-eye' ></i>Ver Boleta</button>" +
                                "</td>" +
                                "</tr>"
                            $('#tbody_matriculas').append(template)
                            num += 1
                            $('#descargar').show();
                            $('#pdf_generate').attr('href', urlpdf).removeAttr('disabled');
                        })
                        {{--$('#tbody_matriculas').html('')--}}
                            {{--data.matriculas.forEach(element => {--}}
                            {{--    var date = moment(element.created_at)--}}
                            {{--    var alumno = element.alumnocarrera.alumno.persona--}}
                            {{--    var template = "<tr>" +--}}
                            {{--        "<td>" + alumno.paterno + " " + alumno.materno + ", " + alumno.nombres + "</td>" +--}}
                            {{--        "<td>" + element.alumnocarrera.carrera.nombre + "</td>" +--}}
                            {{--        "<td>" + date.format('l') + "" +--}}
                            {{--        "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_detalles(" + element.id + ")' title='Ver detalles'><i class='icon-eye' ></i>Ver Detalles</button>" +--}}
                            {{--        "<button type='button' class='btn btn-info btn-sm ml-4' onclick='get_img(" + element.id + ")' title='Ver boleta'><i class='icon-eye' ></i>Ver Boleta</button>" +--}}
                            {{--        "</td>" +--}}
                            {{--        "</tr>"--}}
                            {{--    $('#tbody_matriculas').append(template)--}}
                            {{--})--}}
                        if (data.length > 0) {
                            $('#table_matriculas').DataTable({
                                "language": {
                                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                                }
                            })
                        }
                    }
                })
            })
            $('#nivel_id').change(function () {
                $('#grado').html('').append(new Option('Seleccione un Grado', ''))
                $.ajax({
                    url: '/getgrades/' + $(this).val(),
                    type: 'GET',
                    datatype: 'json',
                    success: function (data) {
                        data.grades.forEach(element => {
                            $('#grado').append(new Option(element.grade + ' - ' + element.description, element.grade))
                        })
                    }
                })
            })
            $('#grado').change(function (){
                $('#grupo option[value="0"]').prop("selected",true);
            })
        })

        // function get_img(matricula_id) {
        //     var url = $('#url_get_boletaxmatricula').val();
        //     var data = {'mat_id': matricula_id}
        //     $.ajax({
        //         url: url,
        //         type: 'GET',
        //         data: data,
        //         dataType: 'json',
        //         success: function (data) {
        //             // console.log(data)
        //             $('#num_boleta').text(data.voucher.num_operacion)
        //             $('#fecha_boleta').text(data.voucher.fecha)
        //             $('#monto_boleta').text(data.voucher.monto)
        //             $('#img_boleta').attr('src', data.urlimg)
        //             $('#modal_img').modal("show");
        //         },
        //         error: function () {
        //             console.log('error')
        //         }
        //     })
        // }

        {{--function get_detalles(matricula_id) {--}}
        {{--    var url = $('#url_get_detx_matricula').val() + matricula_id--}}
        {{--    $('#t_body_detalles').html('')--}}
        {{--    $.ajax({--}}
        {{--        url: url,--}}
        {{--        type: 'GET',--}}
        {{--        dataType: 'json',--}}
        {{--        success: function (data) {--}}
        {{--            data.detalles.forEach(element => {--}}
        {{--                var template = "<tr>" +--}}
        {{--                    "<td>" + element.seccion.pe_curso.curso.nombre + "</td>" +--}}
        {{--                    "<td>" + element.seccion.pe_curso.semestre + "</td>" +--}}
        {{--                    "<td>" + element.tipo + "</td>" +--}}
        {{--                    "</tr>"--}}

        {{--                $('#t_body_detalles').append(template)--}}
        {{--            });--}}
        {{--            $('#modal_detalles').modal('show')--}}
        {{--        }--}}
        {{--    })--}}
        {{--    $('#t_detalles').DataTable({--}}
        {{--        "language": {--}}
        {{--            "url": "{{asset('assets/DataTables/Spanish.json')}}"--}}
        {{--        }--}}
        {{--    })--}}
        {{--}--}}
    </script>
@endsection
