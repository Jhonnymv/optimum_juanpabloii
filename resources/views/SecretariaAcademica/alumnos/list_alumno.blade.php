@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Lista de Alumnos')


@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Alumnos</span> - Lista de Alumnos
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Alumnos</a>
    <span class="breadcrumb-item active">Lista de Alumnos</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Alumnos</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-md-1 col-form-label text-center">Carreras</label>
                        <div class="col-md-3">
                        <select id="carrera" class="form-control">
                            <option>Seleccione una carrera</option>
                            @foreach($carreras as $carrera)
                                <option
                                    value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Alumnos</h5>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="table_alumnos">
                                    <thead>
                                    <tr>
                                        <th>DNI</th>
                                        <th>Alumno</th>
                                        <th>Email</th>
                                        <th>Estado</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody_alumnos">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_getalumnosxcarrera" value="{{route('getalumnosxcarrera')}}">
    <input type="hidden" id="url_updateEstadoAlumno" value="{{route('updateEstadoAlumno', [''])}}/">
    <input type="hidden" name="carrera" id="icarrera_id">
@endsection

@section('script')
    <script>
        $(function () {
            $('#carrera').change(function () {
                var url = $('#url_getalumnosxcarrera').val();
                var id = $(this).val();
                var data = {'carrera_id': id};
                var est;
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#tbody_alumnos').html('');
                        data.alumnos.forEach(element => {
                            if (element.alumno.estado === "1") {
                                est = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Habilitado</font></font></span>"
                            } else {
                                est = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Inhabilitado</font></font></span>"
                            }

                            var template = "<tr>"
                                // +"<td>"+element.index+"</td>"
                                + "<td>" + element.alumno.persona.DNI + "</td>"
                                + "<td>" + element.alumno.persona.paterno + ' ' + element.alumno.persona.materno + ' ' + element.alumno.persona.nombres + "</td>"
                                + "<td>" + element.alumno.persona.email + "</td>"
                                + "<td id='estado" + element.alumno.id + "'>" + est + "</td>"
                                + "<td><button class='btn btn-warning' onclick='cambiar(" + element.alumno.id + ")'><i class='fas fa-pen'></i></button></td>"
                                + "</tr>"
                            $('#tbody_alumnos').append(template)
                        });
                        $('#table_alumnos').dataTable({
                            "language": {
                                "url": "{{asset('assets/DataTables/Spanish.json')}}"
                            }
                        });
                    }
                });
                $('#icarrera_id').val(id)
            })
        });

        function cambiar(id) {
            var url = $('#url_updateEstadoAlumno').val() + id;
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#estado'+id).html('');
                    if (data.alumno.estado === "1") {
                        var est = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Habilitado</font></font></span>"
                    } else {
                        var est = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Inhabilitado</font></font></span>"
                    }

                    $('#estado'+id).html(est);
                }
            })
        }
    </script>
@endsection
