@extends('IntranetDocente.Layouts.mainDocente')
@section('title','Asistencia')

@section('header_title')
    {{-- <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4> --}}

    @include('IntranetDocente.vistasParciales.filtroPeriodo')



@endsection


@section('filtroSeccion')



@endsection


@section('header_buttons')
    <style>

        .table-hover tbody tr:hover {
            background-color: rgba(90, 119, 129, 0.7);
            color: rgb(10, 10, 10));
        }

        input[type=checkbox] {
            display: none;
        }

        input[type=checkbox] + label {
            cursor: pointer;
        }

        .asistencia:before {
            content: "";
            background: transparent;
            border: 2.3px solid #88c64b;
            text-shadow: 4px -2px 3px gray;
            border-radius: 5px;
            display: inline-block;
            height: 20px;
            margin-right: 20px;
            text-align: center;
            vertical-align: middle;
            width: 20px;
        }

        input[type=checkbox]:checked + label:before {
            content: '✓';
            font-size: 14px;
            color: #88c64b;
            font-family: "Times New Roman";
        }


    </style>
    <div class="d-flex justify-content-center">

    </div>
@endsection


@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Aula</a>
    <span class="breadcrumb-item active">Asistencia</span>
@endsection
@section('header_subbuttons')
    <div class="breadcrumb justify-content-center">
        <a href="#" class="breadcrumb-elements-item">
            <i class="icon-comment-discussion mr-2"></i>
            Support
        </a>

        <div class="breadcrumb-elements-item dropdown p-0">
            <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                <i class="icon-gear mr-2"></i>
                Settings
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
            </div>
        </div>
    </div>
@endsection



@section('content')
    <input type="hidden" id="url_report" value="{{route('reporteAsistenciasAlumnoSecretaria.pdf')}}">
    <div class="row">

        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="" method="" id="">
                @csrf
                <input type="hidden" id="seccion_id" name="seccion_id" value="0">
                <input type="hidden" id="alumno_id" name="alumno_id" value="0">
                <input type="hidden" id="contador" name="contador" value="0">


                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Asistencias</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" id="tabOpciones">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#paso1" id="paso1-tab" class="nav-link active"
                                                    data-toggle="tab">Lista de Alumnos</a></li>
                            <li class="nav-item" id="paso2-tab-container"><a href="#paso2" id="paso2-tab"
                                                                             class="nav-link" data-toggle="tab">Detalle
                                    de Asistencia</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="paso1">
                                <div class="card-body" id="">
                                    <div id="listaAlumnos" class="table-responsive">
                                        <table id="tblistaAlumnos" class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nro</th>
                                                <th>Codigo</th>
                                                <th>Alumno</th>
                                                <th>Ver Asistencias</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyListaAlumnos">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="paso2">
                                <div>
                                    <br>
                                    <a id="btnReporte" target="_blank"
                                       size="10"
                                       class="btn btn-success"
                                       style="font-size:8pt">
                                        <b><i class="icon-copy  mr-1 icon-1x"></i> Reporte</a>
                                </div>
                                <div id="sesionesAcademicas" class="table-responsive">
                                    <table id="tbAsistencia" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nro</th>
                                            <th>Fecha</th>
                                            <th>Día</th>
                                            <th>Turno</th>
                                            <th>Hora</th>
                                            <th>Asistencia</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyAsistencia">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>


    </div>
@endsection

@section('script')
    <script>

        document.addEventListener('DOMContentLoaded', function () {

            $('.form-control-select2').select2();

            $('#selectPeriodo').change(function () {
                var id = $(this).val();
                var url = $('#url_carreras').val() + id;
                // irAsistencia('', '', '', '', '', '', '');
                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/CarrerasPorPeriodo",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            $("#selectCarrera").html('');
                            $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#tbSesionClase tr").remove();

                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectCarrera").append(new Option(data[i].nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {
                    $("#selectCarrera").html('');
                    $("#selectCarrera").append(new Option('Seleccione una carrera', ''));

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione un grupo', ''));

                    $("#tbSesionClase tr").remove();
                }


            });


            $('#selectCarrera').change(function () {
                var id = $(this).val()
                // irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/AreasPorCarrera",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);

                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione un area', ''));

                            $("#selectArea").html('');
                            $("#selectArea").append(new Option('Seleccione un area', ''));

                            $("#tbSesionClase tr").remove();
                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectArea").append(new Option(data[i].curso.nombre, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');
                        }
                    });

                } else {

                    $("#selectArea").html('');
                    $("#selectArea").append(new Option('Seleccione un area', ''));

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));

                    $("#tbSesionClase tr").remove();
                }


            });


            $('#selectArea').change(function () {
                var id = $(this).val()

                // irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                    var data = {'id': id};
                    $.ajax({
                        type: "GET",
                        url: "/IntranetDocente/SeccionesPorArea",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            //console.log(data);
                            $("#selectGrupo").html('');
                            $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                            $("#tbSesionClase tr").remove();
                            for (var i in data) {
                                // console.log(data[i]);
                                $("#selectGrupo").append(new Option(data[i].seccion, data[i].id));
                            }
                            // $('.ibox').children('.ibox-content').toggleClass('sk-loading');

                        }
                    });

                } else {

                    $("#selectGrupo").html('');
                    $("#selectGrupo").append(new Option('Seleccione una seccion', ''));
                    $("#tbSesionClase tr").remove();
                }


            });

            $('#selectGrupo').change(function () {

                var id = $(this).val()

                // irAsistencia('', '', '', '', '', '', '');

                if (id != "") {
                    var seccion_id = $('#selectGrupo').val();
                    var periodo_id = $('#selectPeriodo').val();

                    var data = {'seccion_id': seccion_id, 'periodo_id': periodo_id};

                    $.ajax({
                        type: "GET",
                        url: "getlistaAsistenciaSecretaria",
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            $('#tbodyListaAlumnos').html('')
                            $('#contador').val(0)
                            console.log(data)
                            data.listaAlumnos.forEach(element => {

                                var nro = parseInt($('#contador').val()) + 1;
                                $('#contador').val(nro);
                                var asistencia = "<button id='btnAsistencia' onclick='irAsistencia(" + element.matricula.alumnocarrera.alumno.id + ", \"" + seccion_id + "\" )' type='button' class='btn bg-blue-400 btn-labeled btn-labeled-left' size='6' style='font-size:7pt' ><b><i class='icon-checkmark-circle2 mr-1 icon-0.1x'></i></b>Ver Asistencia</button>"

                                var template = "<tr>"
                                    + "<input type='hidden' id='alumno_id' value='" + element.matricula.alumnocarrera.alumno.id + "' name='alumno_id[]'>"
                                    + "<td>" + nro + "</td>"
                                    + "<td>" + element.matricula.alumnocarrera.alumno.persona.DNI + "</td>"
                                    + "<td>" + element.matricula.alumnocarrera.alumno.persona.paterno + " " + element.matricula.alumnocarrera.alumno.persona.materno + " " + element.matricula.alumnocarrera.alumno.persona.nombres + "</td>"
                                    + "<td>" + asistencia + "</td>"
                                    + "</tr>"
                                $('#tbodyListaAlumnos').append(template)

                            })
                        }
                    });

                } else {

                    $("#tbSesionClase tr").remove();

                }


            });


        });

        function irAsistencia(alumno_id, sec_id) {

            $('#alumno_id').val(alumno_id)
            $('#seccion_id').val(sec_id)

            $("#paso2-tab-container").show();
            $("#paso2-tab").tab("show");


            getRegistroAsistencia();

            var seccion_id = $('#selectGrupo').val()
            var alumno_id = $('#alumno_id').val()

            var url_pdf = $('#url_report').val() + '/' + seccion_id + '/' + alumno_id
            $('#btnReporte').attr('href', url_pdf)

        }

        function getRegistroAsistencia() {
            var seccion_id = $('#selectGrupo').val()
            var alumno_id = $('#alumno_id').val()

            var data = {'seccion_id': seccion_id, 'alumno_id': alumno_id}

            $.ajax({
                url: 'getAsistenciasAlumnoSecretaria',
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    $('#tbodyAsistencia').html('')
                    $('#contador').val(0)

                    data.registroAsistencia.forEach(element => {
                        var nro = parseInt($('#contador').val()) + 1;
                        $('#contador').val(nro);

                        if (element.asistencia === "1") {
                            var asistencia = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>A</font></font></span>"
                        } else {
                            var asistencia = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>F</font></font></span>"

                        }
                        var template = "<tr>"
                            + "<td>" + nro + "</td>"
                            + "<td>" + element.fecha_sesion + "</td>"
                            + "<td>" + element.horario.dia + "</td>"
                            + "<td>" + element.horario.turno + "</td>"
                            + "<td>" + element.horario.hora_inicio + " - " + element.horario.hora_fin + "</td>"
                            + "<td>" + asistencia + "</td>"
                            + "</tr>"
                        $('#tbodyAsistencia').append(template)
                    })
                    //console.log(data.registroAsistencia)
                    //$('#t_notas').DataTable()
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }
    </script>

@endsection
