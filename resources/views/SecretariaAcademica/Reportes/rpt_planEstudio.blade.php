@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Planes de estudios')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Reporte de Planes de Estudio</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-3">Carrera: </label>
                            <select id="idCarrera" class="col-md-6 form-control">
                                <option>Seleccione Carrera</option>
                                @foreach($carreras as $item)
                                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label class="col-form-label ">Planes de Estudios:</label>
                            <select id="idPlanEstudios" class="col-md-6 form-control">
                                {{--                                <option>Seleccione Plan de Estudios</option>--}}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="button"
                                    class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round legitRipple">
                                <b><i class="icon-file-download"></i></b> Descargar
                                <div class="legitRipple-ripple"
                                     style="left: 50.399%; top: 50%; transform: translate3d(-50%, -50%, 0px); width: 208.227%; opacity: 0;"></div>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table" id="table_planEstudios">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Semestre
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Curso
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Horas Totales
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Créditos
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">Horas Teoría
                                    </th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending">horas Práctica
                                    </th>
                                    <th class="text-center sorting_disabled" rowspan="1" colspan="1"
                                        aria-label="Actions" style="width: 100px;">Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="t_body_detalles">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_plan_x_carrera" value="{{route('PlanEstudios.getPlanEstCarrera')}}">
    <input type="hidden" id="url_plan_x_pecarrera" value="{{route('PlanEstudios.list_pe_carrera')}}">
@endsection

@section('script')
    <script>
        $(function () {
            $('#idCarrera').change(function () {
                var url = $('#url_plan_x_carrera').val();
                var data = {'carrera_id': $(this).val()}
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#idPlanEstudios').html('').append(new Option('Seleciona Opción', 0));
                        data.planes_estudio.forEach(element => {
                            $('#idPlanEstudios').append(new Option(element.plan, element.id))
                        })
                    }
                })
            })
        })

        $(function () {
            $('#idPlanEstudios').change(function () {
                console.log('hola')
                var url = $('#url_plan_x_pecarrera').val();
                var data = {'plan_estudio_id': $(this).val(), 'carrera_id': $('#idCarrera').val(),}
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#t_body_detalles').html('')
                        $('#idCarrera').val(0)
                        console.log(data)
                        data.pe_cursos.forEach(element => {
                            var template = "<tr>" +
                                "<td>" + element.semestre + "</td>" +
                                "<td>" + element.curso.nombre + "</td>" +
                                "<td>" + (parseInt(element.horas_teoria) + parseInt(element.horas_practica)) + "</td>" +
                                "<td>" + element.creditos + "</td>" +
                                "<td>" + element.horas_teoria + "</td>" +
                                "<td>" + element.horas_practica + "</td>" +
                                "</tr>"
                            $('#t_body_detalles').append(template)
                        });
                    }
                })
            })
        })


    </script>
@endsection
