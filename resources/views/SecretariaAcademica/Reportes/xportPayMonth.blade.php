<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Apoderado</th>
        <th>Comprobante</th>
        <th>Concepto</th>
{{--        <th>Detalle</th>--}}
        <th>Monto</th>
        <th>Fecha</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pagos as $loop=>$pago)
        <tr>
            <td>{{$loop->index+1}}</td>
            <td>{{$pago->persona->DNI.' - '.$pago->persona->paterno.' '.$pago->persona->materno.' '.$pago->persona->nombres}}</td>
            <td>{{$pago->serie.'-'.$pago->numero_doc}}</td>
            <td>{{$pago->alPagos->first()->cronograma->concepto->nombre}}</td>
{{--            <td>--}}
{{--                <ul>--}}
{{--                    @foreach($pago->alPagos as $alPago)--}}
{{--                        <li>{{$alPago->fe_descripcion}}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </td>--}}
            <td>S/ {{$pago->fe_total_price}}</td>
            <td>{{$pago->created_at->format('d-m-Y')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
