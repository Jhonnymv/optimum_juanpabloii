@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Lista de padres
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Lista de padres de familia</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    Esta es una pantalla para vel la lista de Padres de familia <br>
                    <div class="table-responsive">
                        <table class="table" id="table_padres">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>DNI</th>
                                <th>Nombre</th>
                                <th>Teléfono</th>
                                <th>N° hijos</th>
                                <th>Acciones</th>
                                {{--                                <th>Opciones</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($padres as $padre)
                                <tr>
                                    <td>{{$padre->persona->DNI??''}}</td>
                                    <td>{{$padre->persona->paterno??''}} {{$padre->persona->materno??''}} {{$padre->persona->nombres??''}}</td>
                                    <td>{{$padre->persona->telefono??''}}</td>
                                    <td>{{count($padre->alumnop)+count($padre->alumnom)??''}}</td>
                                    <td>
                                        <button class="btn btn-primary" onclick="getAlumnos({{$padre->id}})">Ver Hijos
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4"> ¡¡¡¡No hay datos!!!!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
    <div id="modal_detalles" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lista de Cursos matriculados</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="t_detalles" class="table table-striped">
                            <thead>
                            <th>DNI</th>
                            <th>Nombre</th>
                            <th>Tel.</th>
                            <th>Nivel</th>
                            <th>Grado</th>
                            <th>Acciones</th>
                            </thead>
                            <tbody id="t_body_detalles">
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="modal_detalles_test" class="modal fade" data-keyboard="false" tabindex="-1" style="display: none;"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lista de Cursos matriculados</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <form action="{{route('DocumentoStore')}}" method="post" id="FormSaveCompromiso"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="padre_id" id="modal_padre_id" class="padre_id">
                        <input type="hidden" name="alumno_id" id="modal_alumno_id" class="alumno_id">
                        <div class="row">
                            <div class="col-md-12">
                                <label style="font-weight: bold; border-style: none">Documento de Conformidad: (<label
                                        for="file" style="color: red; margin: 0">*</label>).</label>
                                <input type="file" name="file" id="file" class="form-control"
                                       placeholder="Documento" onkeyup="valid(this);">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-success" type="submit"> Grabar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{--    <script src="{{asset('assets/js/moment.min.js')}}"></script>--}}
    {{--    <script src="{{asset('assets/js/moment-with-locales.js')}}"></script>--}}
    <script>
        $('#table_padres').DataTable();

        // moment.locale('es')

        function getAlumnos(padre_id) {
            var data = {'padre_id': padre_id}
            $.ajax({
                url: '/Secretaria_Academica/getAlumnoByPadre/',
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    // console.log(data)
                    $('#t_body_detalles').html('');
                    data.alumnos.forEach(element => {
                        if (element.habPago === 1) {
                            var btnTest = "<button type='button' class='btn btn-info' onclick='openModal(" + padre_id + "," + element.id + ")'>Registrar contrato.</button>"
                        } else
                            var btnTest = ""

                        var temp = "<tr>" +
                            "<td>" + element.dni + "</td>" +
                            "<td>" + element.nombre + "</td>" +
                            "<td>" + element.telefono + "</td>" +
                            "<td>" + element.nivel + "</td>" +
                            "<td>" + element.grado + "</td>" +
                            "<td><a target='_blank' href='" + element.url + "'><i class='fa fa-download'></i>Contrato</a></td>" +
                            "<td>" + btnTest + "</td>" +
                            "</tr>"
                        $('#t_body_detalles').append(temp)
                    })
                    $('#modal_detalles').modal('show')
                }
            })
        }

        function addPago(padre_id, alumno_id) {
            // console.log(padre_id,alumno_id)
            var data = {'papa_id': padre_id, 'alumnito_id': alumno_id}
            $.ajax({
                url: '/register_pago_secretaria',
                type: 'GET',
                data: data,
                datatype: 'json',
                success: function (data) {
                    console.log('ok')
                }
            })
        }

        function openModal(padre_id, alumno_id) {
            $('#modal_alumno_id').val(alumno_id);
            $('#modal_padre_id').val(padre_id);
            $('#modal_detalles_test').modal('show')
        }

        $(function () {
            $('#FormSaveCompromiso').submit(function (e) {
                e.preventDefault()
                addPago($('#modal_padre_id').val(), $('#modal_alumno_id').val())
                var url = $(this).attr('action')
                var formData = new FormData($('#FormSaveCompromiso')[0]);
                formData.append('file', $('#FormSaveCompromiso input[type=file]')[0].files[0]);
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data === 'success') {
                            alert('La documentacíón fue registrada correctamente la secretaria validará la documentación y de no haber observaciones el estudiante será matriculado')
                            location.reload()
                        }
                    }
                })
                $('#modal_alumno_id').val('');
                $('#modal_padre_id').val('');
                $('#file').val('');
                $('#modal_detalles_test').modal('hide')
            })
        })
    </script>
@endsection


{{--<a type="button" class="btn btn-info" target="_blank"--}}
{{--   href="{{route('terminos',['alumno_id'=>$alumno_id,'padre_id'=>$padre_id,'carrera_id'=>$alumno->alumnocarreras->last()->carrera_id,'grado'=>$alumno->first()->alumnocarreras->first()->ciclo])}}"><i--}}
{{--        class="icon-books"></i>Contrato</a>--}}
