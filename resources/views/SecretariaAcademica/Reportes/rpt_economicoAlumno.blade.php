@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Unidades Administrativas
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Unidad Administrativa</span>
        </a>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Unidades administrativas</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de unidades administrativas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para ver la lsita de Unidades Administrativas
                    <div class="table-responsive">
                        <table class="table table-striped" id="alumnos_reporte_economico">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Responsable</th>
                                <th>Ver Trámites de Unidad Administrativa</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($alumnos as $item)
                                <tr>
                                    <td>{{$item->persona->DNI}}</td>
                                    <td>{{$item->persona->paterno. ' '.$item->persona->materno. ', '.$item->persona->nombres}} </td>
                                    <td><button type="button" class="btn btn-outline-primary"><i class="icon-file-eye2"></i> Ver Reporte Ecónomico</button></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <input type="hidden" id="url_get_undTra" value="{{route('UnidadAdministrativaController.getUndAdmin',[''])}}/">--}}
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#alumnos_reporte_economico').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
@endsection
