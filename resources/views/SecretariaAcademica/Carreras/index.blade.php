@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Alumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Nivel</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Nivel</span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Nivel</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Nivel</h5>
                <div class="header-elements">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

            <div class="card-body">
                Este es una pantalla para la edicion y mantenimiento de Niveles
            </div>

            <div class="table-responsive">
                <table class="table" id="MyTabla">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Resolución</th>
{{--                            <th>Responsable</th>--}}
                            <th>Estado</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($carreras as $item)
                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>
                            <td>
                                {{ $item->nombre }}
                            </td>
                            <td>
                                {{ $item->resolucion }}
                            </td>
{{--                            <td>--}}
{{--                                {{ $item->responsable != null? $item->responsable->nombre:'' }}--}}
{{--                            </td>--}}
                            <td>
                                {{ $item->estado=1?'Activo':'Inactivo'}}
                            </td>
                            <td class="text-right">
                                <a href="{{route('pecarrera.listxcarrera',['carrera_id'=>$item->id])}}" type="button" class="btn btn-success btn-sm legitRipple"><i
                                        class="icon-clipboard3 mr-2"></i> Ver planes de estudio
                                </a>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})"><i
                                        class="fas fa-pen"></i></a>
                                <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id }})"><i
                                        class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection


@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('carreras.store')}}">
            <!--Importante-->
            @csrf
            <!--Importante-->
            <div class="modal-header">
                <h5 class="modal-title">Crear nueva Nivel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombre:</label>
                    <div class="col-lg-9">
                        <input type="text" name="nombre" class="form-control" placeholder="Carrera Ejemplo..." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Resolucion:</label>
                    <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="Resolución..." required>
                    </div>
                </div>
{{--                <div class="form-group row">--}}
{{--                    <label class="col-lg-3 col-form-label">Responsable:</label>--}}
{{--                    <div class="col-lg-9">--}}
{{--                        <select name="responsable" class="form-control">--}}

{{--                            @forelse ($responsables as $item)--}}
{{--                            <option value="{{$item->id}}">--}}
{{--                                {{$item->nombre}}--}}
{{--                            </option>--}}
{{--                            @empty--}}

{{--                            @endforelse--}}

{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->


<!-- crear modal -->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/carreras">

            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Nivel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nombre:</label>
                    <div class="col-lg-9">
                        <input type="text" id="nombreEdit" name="nombreEdit" class="form-control" placeholder="Carrera Ejemplo..."
                            required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Resolucion:</label>
                    <div class="col-lg-9">
                        <input type="text" id="resolucionEdit" name="resolucionEdit" class="form-control" placeholder="Resolución..."
                            required>
                    </div>
                </div>
{{--                <div class="form-group row">--}}
{{--                    <label class="col-lg-3 col-form-label">Responsable:</label>--}}
{{--                    <div class="col-lg-9">--}}
{{--                        <select id="responsableEdit" name="responsableEdit" class="form-control">--}}

{{--                            @forelse ($responsables as $item)--}}
{{--                            <option value="{{$item->id}}">--}}
{{--                                {{$item->nombre}}--}}
{{--                            </option>--}}
{{--                            @empty--}}

{{--                            @endforelse--}}

{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->





<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/carreras/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar Carrera?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Carrera</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

@endsection


@section('script')
<script>
    $(function () {
        $('#MyTabla').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    })
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/carreras/remove/'+id);
    }
    function editar(id){
        $("#formEditar").attr('action','/carreras/'+id);


        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traercarrera',
                data: data,
                success: function(data) {
                    $('#nombreEdit').val(data['carrera']['nombre']);
                    $('#resolucionEdit').val(data['carrera']['resolucion']);
                    $('#responsableEdit').val(data['carrera']['responsable_id']);

                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>

@endsection
