@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Carga Horaria')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cargar Horaria
    </h4>
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Carga Horaria</span>
        </a>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Carga Horaria</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <!-- Basic table -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Carga Horaria por Carreras</h5>
                        <div class="header-elements">

                        </div>
                    </div>
                    <div class="col-md-6 card-body">
                        Esta es una pantalla para ver la carga horaria por carrera
                        <select id="carrera_id" name="carrera_id" class="col-md-6 form-control">
                            <option>Seleccione Carrera</option>
                            @foreach($carreras as $item)
                                <option value="{{$item->id}}">{{$item->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-striped" id="tableCargaHoraria">
                                <thead>
                                <tr>
                                    <th>Carrera</th>
                                    <th>Docente</th>
                                    <th>Curso</th>
                                    <th>Ciclo</th>
                                    <th>Grupo</th>
                                    <th>Dia</th>
                                    <th>Horas</th>
                                </tr>
                                </thead>
                                <tbody id="t_body_detalles">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <input type="hidden" id="url_get_carga" value="{{route('SeccionController@buscarCargaHoraria')}}/">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $(function () {

            $('#carrera_id').change(function () {
                var url = $('#url_get_carga').val();
                var data = {'carrera_id': $(this).val()}
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#t_body_detalles').html('');
                        // console.log(data.secciones);
                        data.secciones.forEach(element => {
                            var Hora = "";
                            var dia = "";
                            $.each(element.horarios, function (index, value) {
                                dia += value.dia + "<br>";
                                Hora += value.hora_inicio + "<br>";
                            });
                            var p = "";
                            $.each(element.docentes, function (index, value) {
                                p += value.persona.paterno + ' ' + value.persona.materno + ' ' + value.persona.nombres + "<br>";
                            });
                            var template = "<tr>" +
                                "<td>" + element.pe_curso.pecarrera.carrera.nombre + "</td>" +
                                "<td>" + p + "</td>" +
                                "<td>" + element.pe_curso.curso.nombre + "</td>" +
                                "<td>" + element.pe_curso.semestre + "</td>" +
                                "<td>" + element.grupo + "</td>" +
                                "<td>" + dia + "</td>" +
                                "<td>" + Hora + "</td>" +
                                "</tr>"
                            $('#t_body_detalles').append(template)
                        });
                        $('#tableCargaHoraria').DataTable({
                            "paging": true,
                            "searching": true,
                            "destroy": true,
                            "language": {
                                "url": "{{asset('assets/DataTables/Spanish.json')}}"
                            }
                        })

                    }
                })
            })
        })
    </script>

@endsection

