@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','CredencialesAlumno')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Periodos Académicos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Periodo Académico</span>
    </a>
</div>
@endsection
@section('header_subtitle')
<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Periodos Académicos</span>
@endsection
@section('header_subbuttons')

@endsection
@section('content')
@if (Session::has('Mensaje'))
{{
    Session::get('Mensaje')
}}
@endif

<table class="table table-light">
    <thead  class="table-light">
        <tr>
            <th>#</th>
            <th>Descripción</th>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($periodosacademicos as $periodosacademicos)
        <tr>
            <td scope="row">{{$loop -> iteration}}</td>
            <td>{{$periodosacademicos -> descripcion}}</td>
            <td>{{$periodosacademicos -> fecha_inicio}}</td>
            <td>{{$periodosacademicos -> fecha_fin}}</td>
            <td>{{$periodosacademicos -> estado}}</td>
            <td>
            {{--  <a href="{{url('/docentes/'.$docentes -> id.'/edit')}}">Editar</a>
            |
            <form method="post" action="{{url('/docentes/'.$docentes -> id)}}">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" onclick="return confirm('¿Desea borrar el registro?');">Borrar</button>  --}}

            <a href="#" class="btn btn-danger" onclick="eliminar({{ $periodosacademicos->id }})"><i class="fas fa-trash"></i></a>
            <a href="#" class="btn btn-warning" onclick="editar({{ $periodosacademicos->id }})"><i class="fas fa-pen"></i></a>
            </td>
        </tr>
    @endforeach
        <tr>

        </tr>
    </tbody>
</table>
@endsection

@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/periodosacademicos">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo periodo académico</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
              {{--  <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Id:</label>
                    <div class="col-lg-9">
                        <input type="text" name="id" class="form-control" placeholder="Id..." required>
                    </div>  --}}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" name="descripcion" class="form-control" placeholder="Descripción..." required>
                    </div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Inicio</label>
						<div class="col-md-10">
							<input class="form-control" type="date" name="fecha_inicio">
						</div>
				</div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Fin</label>
						<div class="col-md-10">
							<input class="form-control" type="date" name="fecha_fin">
				        </div>
				</div>
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/periodosacademicos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Periodo AAcadémico?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Periodo Académico</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->

<!-- editar modal -->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/periodosacademicos">
            @method('PUT');
            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Actualizar periodo académico</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Descripción:</label>
                    <div class="col-lg-9">
                        <input type="text" id="descripcionEdit" name="descripcionEdit" class="form-control" placeholder="Descripción..." >
                    </div>

                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Inicio</label>
						<div class="col-md-10">
							<input class="form-control" type="date" id="fecha_inicioEdit" name="fecha_inicioEdit">
						</div>
				</div>
                <div class="form-group row">
					<label class="col-form-label col-md-2">Fecha de Fin</label>
						<div class="col-md-10">
							<input class="form-control" type="date"  id="fecha_finEdit" name="fecha_finEdit">
				        </div>
				</div>
                    <div class="form-group row">
                    <label class="col-lg-3 col-form-label"></label>
                    <div class="col-lg-9">
                        <input type="hidden" name="estado" class="form-control" placeholder="" value='1'>
                    </div>
                </div>

            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>

@endsection


@section('script')
<script>
    function eliminar(id)
    {
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/periodosacademicos/remove/'+id);
    }

    function editar(id)
    {

        $("#formEditar").attr('action','/periodosacademicos/'+id);
        var data = {'id':id};
        $.ajax({
                type: "GET",
                url: '/traerperiodoacademico',
                data: data,
                success: function(data) {
                    $('#descripcionEdit').val(data['periodoacademico']['descripcion']);
                    $('#fecha_inicioEdit').val(data['periodoacademico']['fecha_inicio']);
                    $('#fecha_finEdit').val(data['periodoacademico']['fecha_fin']);

                    $("#modalEditar").modal('show');
                    console.log(data['periodoacademico']['descripcion']);
                },
                error: function() {
                    console.log("ERROR");
                }

            });
    }
</script>

@endsection
