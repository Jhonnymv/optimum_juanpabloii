@extends('SecretariaAcademica.Layouts.main_secretaria')

@section('title','Alumnos Cronograma')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cronograma pagos Alumnos</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')

@endsection

@section('header_subtitle')
    <a href="{{url('/secretaria')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cronograma pagos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Alumnos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para a ver la lista de todos los alumnos y ver los pagos pendientes.

                    <div class="table-responsive">
                        <table class="table table-striped" id="table_alumnos">
                            <thead>
                            <tr>
                                <th>N°</th>
                                <th>DNI°</th>
                                <th>Alumno</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>Categoria descuento</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($alumnos as $item)
                                @php
                                    $i=0;
                                @endphp
                                <tr>
                                    <td>{{$i=$i+1}}</td>
                                    <td>{{$item->persona->DNI}}</td>
                                    <td>{{$item->persona->paterno.' '.$item->persona->materno.', '.$item->persona->nombres}}</td>
                                    <td>{{$item->carreras->first()->nombre}}</td>
                                    <td>{{$item->alumnocarreras->first()->ciclo}}</td>
                                    <td>{{$item->categorias->first()->nombre. ' - '.$item->categorias->first()->observacion}}</td>
                                    <td>
                                        <a type="button" href="{{route('vercronograma',['alumno_id'=>$item->id])}}" class="btn btn-outline-primary"><i class="icon-calendar2"></i>Cronograma</a>
                                        <a type="button" href="{{route('fichaAlumno',['alumno_id'=>$item->id])}}" class="btn btn-outline-primary"><i class="icon-shredder"></i>Ficha</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="url_get_card" value="{{route('showAlumnosCard',[''])}}/">
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
@endsection
