@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cronograma Pagos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cronograma Pagos</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')

@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cronograma Pagos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Cronograma Pagos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body ">
                    Esta es una pantalla para ver el cronograma de pagos
                    <br>
                    <br>
                    <div class="row justify-content-center">
                        @foreach($cronograma as $item)
                            {{--                            <div class="col-md-4" style="margin: 0px">--}}
                            @if($carrera->id===$item->concepto->carrera_id || $item->concepto->nombre=='Pensión')
                                <div class="card col-md-3"
                                     style="margin: 10px ; padding: 0px;padding: 2px ; border-radius: 15px">
                                    <div class="card-header justify-content-center flex-row"
                                         style="color: white;background-color: #0a6ebd; border-top-left-radius: 15px; border-top-right-radius: 15px">
                                        <h2>{{$item->concepto->nombre.'  -  '. $item->num_couta}}</h2>
                                    </div>
                                    <div class="card-body" style="padding:0px; padding: 20px; padding-top: 20px">
                                        <div class="row justify-content-center">
                                            @php
                                                $datenow=\Carbon\Carbon::now()->locale('es');
                                                $date = \Carbon\Carbon::parse($item->fech_vencimiento);
                                            @endphp
                                            <label class="text-uppercase text-center"
                                                   style="margin: 0px">{{$date->isoFormat('MMMM')}}</label>
                                        </div>
                                        <div class="row justify-content-center">
                                            @foreach($alumnoPago as $pago)
                                            @if($pago)
                                               <label class="text-uppercase text-center" style="margin: 0px">{{$item->id==$pago->cronograma_id && $pago->fe_precio_und!=0 ?'Pagado':'Pendiente de Pago'}}</label>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    @if($alumnoPago->first())
                                    <div class="card-footer justify-content-center flex-row"
                                         style="color: white;background-color: {{$datenow>$date && $item->id==$pago->cronograma_id && $pago->fe_precio_und==0?'red':($item->id==$pago->cronograma_id && $pago->fe_precio_und!=0?'green':'#ffc107')}}; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px">
                                        <div class="row justify-content-center">
                                            <label class="text-uppercase text-center" style="margin: 0px">Fecha de
                                                vencimiento</label>

                                        </div>
                                        <div class="row justify-content-center">
                                            <label class="text-uppercase text-center"
                                                   style="margin: 0px">{{$date->isoFormat('YYYY/MM/DD')}}</label>
                                        </div>
                                        <div class="row justify-content-center">
                                            <label class="text-uppercase text-center"
                                                   style="margin: 0px">{{$datenow->isoFormat('YYYY/MM/DD')}}</label>
                                        </div>
                                    </div>
                                    @else
                                        <div class="card-footer justify-content-center flex-row"
                                             style="color: white;background-color: #ffc107; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px">
                                            <div class="row justify-content-center">
                                                <label class="text-uppercase text-center" style="margin: 0px">Fecha de
                                                    vencimiento</label>

                                            </div>
                                            <div class="row justify-content-center">
                                                <label class="text-uppercase text-center"
                                                       style="margin: 0px">{{$date->isoFormat('YYYY/MM/DD')}}</label>
                                            </div>
                                            <div class="row justify-content-center">
                                                <label class="text-uppercase text-center"
                                                       style="margin: 0px">{{$datenow->isoFormat('YYYY/MM/DD')}}</label>
                                            </div>
                                        </div>
                                    @endif
                                    {{--                            {{$item->alumno->first()->pago->where('id',$item->alumno->first()->pivot->pago_id)}}--}}
                                </div>
                                {{--                            </div>--}}
                            @endif
                        @endforeach

                    </div>

                </div>

            </div>
            <!-- /basic table -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{(asset('assets/js/moment-with-locales.js'))}}"></script>
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        moment.locale('es')
    </script>
@endsection
