@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','ConceptoPagos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cronograma</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir un Nuevo</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cronograma</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Cronograma</h5>
                    <div class="header-elements">

                    </div>
                </div>
                <form class="modal-content" method="POST" action="{{route('storeCronograma')}}">

                    <!--Importante-->
                    @csrf
                    <div class="card-body">
                        Esta es una pantalla para ver el cronograma de pagos
                        <br>
                        <br>
                        <div class="tab-pane active show" id="basic-pill1">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="departamento" style="font-weight: bold; margin: 0">Periodo
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label>
                                        <select class="form-control" id="show_periodo_id" name="show_periodo_id">
                                            <option>Seleccione periodo</option>
                                            @foreach($periodos as $item)
                                                <option
                                                    value="{{$item->id}}">{{$item->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="departamento" style="font-weight: bold; margin: 0">Concepto de Pago
                                            (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label>
                                        <select class="form-control" id="show_concepto_id" name="show_concepto_id">
                                            <option>Seleccione Concepto</option>
                                            @foreach($conceptos as $item)
                                                <option
                                                    value="{{$item->id}}">{{$item->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" onclick="show()">Ver
                                            Cronograma
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center" id="cronograma">

                            </div>

                        </div>

                    </div>
                </form>
            </div>
            <!-- /basic table -->
        </div>
    </div>
    <input type="hidden" id="url_cronogramas" value="{{route('ShowCronograma', [''])}}/">
@endsection

@section('modals')

    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <!-- Basic table -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Cronograma</h5>
                            <div class="header-elements">

                            </div>
                        </div>
                        <form class="modal-content" method="POST" action="{{route('storeCronograma')}}">

                            <!--Importante-->
                            @csrf
                            <div class="card-body">
                                Esta es una pantalla para ver el cronograma de pagos
                                <br>
                                <br>
                                <div class="tab-pane active show" id="basic-pill1">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="departamento" style="font-weight: bold; margin: 0">Periodo
                                                    (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <select class="form-control" id="periodo_id" name="periodo_id">
                                                    <option>Seleccione periodo</option>
                                                    @foreach($periodos as $item)
                                                        <option
                                                            value="{{$item->id}}">{{$item->descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="departamento" style="font-weight: bold; margin: 0">Concepto
                                                    de Pago
                                                    (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <select class="form-control" id="concepto_id" name="concepto_id">
                                                    <option>Seleccione Concepto</option>
                                                    @foreach($conceptos as $item)
                                                        <option
                                                            value="{{$item->id}}">{{$item->nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="departamento" style="font-weight: bold; margin: 0">Mes
                                                    Inicio
                                                    (<label
                                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                                <input type="month" name="mesInicio" id="mesInicio"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="departamento" style="font-weight: bold; margin: 0">Numero de
                                                    Cuotas
                                                    (<label for=""
                                                            style="color: red; margin: 0">*</label>)</label></label>
                                                <input type="number" class="form-control" name="cuotas" id="cuotas">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-success" onclick="generate()">
                                                    Generar
                                                    Cronograma
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center" id="cronog">

                                    </div>
                                    <div class="row justify-content-center">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <!-- /basic table -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{(asset('assets/js/moment-with-locales.js'))}}"></script>
    <script>
        moment.locale('es')

        function generate() {
            var inicio = parseInt($('#mesInicio').val()) - 1;
            var inicio1 = $('#mesInicio').val();
            var coutoas = parseInt($('#cuotas').val());
            var m = moment($('#mesInicio').val());
            var mes = m.months();
            var ano = m.year();
            console.log(m);
            console.log(mes);
            console.log(ano);
            $('#cronog').html(' ');
            for (var i = 0; i < coutoas; i++) {
                console.log(m.months((i + mes)).format('MMMM'));

                var pension = i + 1;
                var template = "<div class='card col-md-3' style='margin: 10px ; padding: 0px;padding: 2px ; border-radius: 15px'>" +
                    "<div class='card-header justify-content-center flex-row' style='color: white;background-color: #0a6ebd; border-top-left-radius: 15px; border-top-right-radius: 15px'>" +
                    "<h2  class='text-center'>" + 'Pensión' + ' ' + pension + "</h2>" +
                    "<input type='hidden' name='num_couta[]' id='num_couta[]' value='" + pension + "'>" +
                    "</div>" +
                    "<div class='card-body' style='padding:0px; padding: 20px; padding-top: 20px'>" +
                    "<div class='row justify-content-center'>" +
                    "<label class='text-uppercase text-center' style='margin: 0px'>" + m.months((i + mes)).format('MMMM') + "</label>" +
                    "</div>" +
                    "</div>" +
                    "<div class='card-footer justify-content-center flex-row' style='color: white;background-color: #ffc107; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px'>" +
                    "<div class='row justify-content-center'>" +
                    "<label class='text-uppercase text-center' style='margin: 0px'>" + 'Fecha de Vencimiento' + "</label>" +
                    "</div>" +
                    "<div class='row justify-content-center'>" +
                    "<label class='text-uppercase text-center' style='margin: 0px'>" + m.year(ano).months((i + mes)).endOf('month').format('YYYY-MM-DD') + "</label>" +
                    "<input type='hidden' name='fechaVencimiento[]' id='fechaVencimiento[]' value='" + m.year(ano).months((i + mes)).endOf('month').format('YYYY-MM-DD') + "'>" +
                    "</div>" +
                    "</div>"
                    + "</div>"
                $('#cronog').append(template)
            }
        }


    </script>
    <script>
        function show() {
            var periodo_id = $('#show_periodo_id').val();
            var concepto_id = $('#show_concepto_id').val();
            var url = $('#url_cronogramas').val();
            if (concepto_id.length > 3) {
                var data = {'periodo_id': periodo_id};
            } else {
                var data = {'periodo_id': periodo_id, 'concepto_id': concepto_id};
            }
            console.log(data);
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('#cronograma').html('');

                    data.cronograma.forEach(element => {
                        console.log(element.concepto)

                        var template = "<div class='card col-md-3' style='margin: 10px ; padding: 0px;padding: 2px ; border-radius: 15px'>" +
                            "<div class='card-header justify-content-center flex-row' style='color: white;background-color: #0a6ebd; border-top-left-radius: 15px; border-top-right-radius: 15px'>" +
                            "<h2  class='text-center'>" + element.concepto.nombre + ' ' + element.num_couta+"</h2>" +
                            "</div>" +
                            "<div class='card-body' style='padding:0px; padding: 20px; padding-top: 20px'>" +
                            "<div class='row justify-content-center'>" +
                            "<label class='text-uppercase text-center' style='margin: 0px'>" + moment(element.fech_vencimiento).format('MMMM') + "</label>" +
                            "</div>" +
                            "</div>" +
                            "<div class='card-footer justify-content-center flex-row' style='color: white;background-color: #ffc107; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px'>" +
                            "<div class='row justify-content-center'>" +
                            "<label class='text-uppercase text-center' style='margin: 0px'>" + 'Fecha de Vencimiento' + "</label>" +
                            "</div>" +
                            "<div class='row justify-content-center'>" +
                            "<label class='text-uppercase text-center' style='margin: 0px'>" + element.fech_vencimiento + "</label>" +
                            "</div>" +
                            "</div>"
                            + "</div>"
                        $('#cronograma').append(template)
                    })
                    console.log(data);
                }
            })
        }
    </script>
@endsection()
