<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

        .labels {
            font-size: 16px;
        }

        .datos {
            font-size: 17px;
            font-weight: bold;
            margin-right: 20px;
        }

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            justify-content: center;
            align-items: center;
            width: 100%;
        }

        .left {
            float: left;
            width: 50%;
            text-align: right;
            margin: 2px 10px;
            display: inline;
            justify-content: right;
        }

        .right {
            float: left;
            text-align: left;
            margin: 2px 10px;
            display: inline;
            justify-content: left;
        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">
<div >
    <div class="row">
        <table style="margin-top: -20px">
            <tr>
                <td style="text-align: center; width: 15%">
                    <img src="{{public_path()}}/assets/img/logo_raimondi.png"
                         style="width: 100px;padding-bottom: -25px; float: right; margin-top: 20px" alt="">
                </td>
                <td style="text-align: center; width: 100%">
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: 30px">INSTITUCION
                        EDUCATIVA PARTICULAR </p>
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: -15px">“ANTONIO
                        RAIMONDI”</p><br>
                    <p style="font-size:15px;font-weight: bold; font-family:arial; margin-top: -15px; margin-left: -30px">
                        FICHA INTEGRAL DEL ALUMNO</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>DATOS PERSONALES DEL
                ALUMNO</u></h2>
        <div class="">

            {{--            <p style="justify-content: right; font-size:14px;font-weight: bold; font-family:arial">Apellido Paterno:</p>--}}
            <table>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APELLIDO PATERNO:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->paterno}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APELLIDO MATERNO:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->materno}}</label>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">NOMBRES:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->nombres}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DNI:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->DNI}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">SEXO:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->sexo}}</label>

                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">EDAD:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{\Carbon\Carbon::parse($alumno->persona->fecha_nacimiento)->age. ' '. 'años'}}</label>

                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">FECHA DE
                            NACIMIENTO:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->fecha_nacimiento}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">NUMERO DE HERMANOS:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->num_hermanos}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">LUGAR QUE OCUPA:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->num_ocupa}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DIRECCION DE CASA:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->persona->direccion}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">RELIGION:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->religion}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">GRUPO SANUINEO:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->gru_sanginio}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">PARTO(NOMAR O
                            CESÁREA):</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->parto==1?'Normal':'Cesárea'}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">ENFERMEDADES:</label>
                        <label style="margin-left: 5px; font-size: 14px">{{$alumno->enfermedades}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">ALERGIAS:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->parto==1?'Normal':'Cesárea'}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">ACCIDENTES GRAVES:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->accidentes}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">EXPERIENCIAS
                            TRAUMATICAS:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->traumas}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">LIMITACIONES
                            FISICAS:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->lim_fisicas}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">PESO:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->peso}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">TALLA:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->talla}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">VACUNAS COMPLETAS:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->vacunas}}</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">SEGURO:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->t_seguro}}</label>
                    </td>
                    <td>
                        <label style="margin-left: 20px; font-weight: bold; font-size: 14px">N° SEGURO:</label>
                        <label
                            style="margin-left: 5px; font-size: 14px">{{$alumno->n_seguro}}</label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="row">
            <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u><u>DATOS PERSONALES DE LOS
                        PADRES DE FAMILIA</u></u></h2>
            <h3 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>PADRE</u></h3>

            <div class="">
                <table>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APODERADO:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->apoderado}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">VIVE:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->vive}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APELLIDOS Y
                                NOMBRES:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->persona->paterno.' '.$padre->persona->materno.', '.$padre->persona->nombres}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DNI:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->persona->DNI}}</label>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">EDAD:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{\Carbon\Carbon::parse($padre->persona->fecha_nacimiento)->age. ' '. 'años'}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">FECHA DE
                                NACIMIENTO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->persona->fecha_nacimiento}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">LUGAR DE
                                NACIMIENTO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->persona->distrito->nombre}}</label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">VIVE CON EL
                                ALUMNO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->vive_alumno}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">ESTADO CIVIL:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->estado_civil}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DIRECCIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->persona->direccion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">NIVEL DE
                                INSTRUCCIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->nivel_instruccion}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">RELIGION:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->religion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">PROFESIÓN:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->profesion}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">OCUPACIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$padre->ocupacion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">CENTRO DE
                                TRABAJO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->centro_trabajo}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">CORREO
                                ELECTRÓNICO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->persona->email_personal}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">TELEFONO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$padre->persona->telefono}}</label>
                        </td>
                    </tr>
                </table>
            </div>


            <h3 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>MADRE</u></h3>

            <div class="">
                <table>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APODERADO:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->apoderado}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">VIVE:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->vive}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">APELLIDOS Y
                                NOMBRES:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->persona->paterno.' '.$madre->persona->materno.', '.$madre->persona->nombres}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DNI:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->persona->DNI}}</label>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">EDAD:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{\Carbon\Carbon::parse($madre->persona->fecha_nacimiento)->age. ' '. 'años'}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">FECHA DE
                                NACIMIENTO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->persona->fecha_nacimiento}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">LUGAR DE
                                NACIMIENTO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->persona->distrito->nombre}}</label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">VIVE CON EL
                                ALUMNO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->vive_alumno}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">ESTADO CIVIL:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->estado_civil}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">DIRECCIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->persona->direccion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">NIVEL DE
                                INSTRUCCIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->nivel_instruccion}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">RELIGION:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->religion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">PROFESIÓN:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->profesion}}</label>
                        </td>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">OCUPACIÓN:</label>
                            <label style="margin-left: 5px; font-size: 14px">{{$madre->ocupacion}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">CENTRO DE
                                TRABAJO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->centro_trabajo}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 20px; font-weight: bold; font-size: 14px">CORREO
                                ELECTRÓNICO:</label>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->persona->email_personal}}</label>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="row">
                <table>
                    <tr>
                        <td>
                            <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>ESTADO CIVIL DE LOS
                                    PADRES:</u></h2>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$madre->estado_civil==1?'Soltero':($madre->estado_civil==2?'Casados':($madre->estado_civil==3?'Convivientes':($madre->estado_civil==4?'Separados':'Divorciados')))}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>RELACIÓN PADRES:</u></h2>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$alumno->relacion==1?'Muy buena':($alumno->relacion==2?'Buena':($alumno->relacion==3?'Regular':'Mala'))}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>APARTE DE LOS PADRES¿QUÉ OTRA PERSONA PARA MÁS TIEMPO CON EL NIÑO(A)?:</u></h2>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$alumno->m_tiempo}}</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 style="margin-left: 10px;font-weight: bold;font-size:16px;font-family:arial"><u>¿EN CASO DE EMERGECIA A QUIEN PODEMOS RECURRIR?:</u></h2>
                            <label
                                style="margin-left: 5px; font-size: 14px">{{$alumno->num_emergencia}}</label>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
    </div>
</div>
</body>
</html>
