@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','PE Niveles')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - PE Niveles</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir PE Nivel</span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Plan Estudio - Nivel</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Plan de Estudio por Nivel</h5>
                <div class="header-elements">

                </div>
            </div>

            <div class="card-body">
                Este es una pantalla para la edicion y mantenimiento de PE Nivel
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Plan Estudio</th>
                            <th>Nivel</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($pecarreras as $item)

                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>
                            <td>
                                {{ $item->planestudio != null? $item->planestudio->plan:'' }}
                            </td>
                            <td>
                                {{ $item->carrera != null? $item->carrera->nombre:'' }}
                            </td>
                            <td>
                                {{ $item->estado }}
                            </td>
                            <td>

                                <a href="/pecursos/{{$item->carrera->id}}/{{$item->planestudio->id}}" class="btn btn-primary" onclick="">
                                CURSOS<i class=""></i></a>
                                <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id}})"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection


@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/pecarreras">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo PE Nivel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Plan de Estudio:</label>
                    <div class="col-lg-9">
                        <select name="planestudio" class="form-control">

                            @forelse ($planestudios as $item)
                            <option value="{{$item->id}}">
                                {{$item->plan}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nivel:</label>
                    <div class="col-lg-9">
                        <select name="carrera" class="form-control">

                            @forelse ($carreras as $item)
                            <option value="{{$item->id}}">
                                {{$item->nombre}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/pecarreras/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el plan de estudios - Carrera?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Carrera</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->



@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/pecarreras/remove/'+id);
    }

</script>

@endsection
