@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','PE Niveles')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Plan de estudios por
        Nivel</h4>
@endsection

@section('header_buttons')
    <div class="d-flex justify-content-center">
        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Agregar Plan de estudios</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Niveles</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Niveles</h5>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    Este es una pantalla para la edicion y mantenimiento de Niveles
                </div>

                <div class="table-responsive">
                    <table class="table" id="MyTabla">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Fecha Inicio/Fecha Fin</th>
                            {{--                            <th>Responsable</th>--}}
                            <th>Estado</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pe_carreras as $pe_carrera)
                            <tr>
                                <td>
                                    {{$loop->index+1}}
                                </td>
                                <td>{{$pe_carrera->planestudio->plan}}</td>
                                <td>{{date('d-m-Y',strtotime($pe_carrera->planestudio->fecha_inicio))}}
                                    / {{date('d-m-Y',strtotime($pe_carrera->planestudio->fecha_fin))}}</td>
                                <td>{{$pe_carrera->estado==1?'Activo':'nactivo'}}</td>
                                <td class="text-right">
                                    <a href="{{route('pecursos.listxpecarrera',['carrera_id'=>$pe_carrera->id])}}" type="button" class="btn btn-success btn-sm legitRipple"><i
                                            class="icon-clipboard3 mr-2"></i> Ver cursos
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No hay elementos</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
@endsection
@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="/pecarreras">

                <!--Importante-->
            @csrf
            <!--Importante-->

                <div class="modal-header">
                    <h5 class="modal-title">Crear nuevo PE Nivel</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="vista" value="listxcarrera">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Plan de Estudio:</label>
                        <div class="col-lg-9">
                            <select name="planestudio" class="form-control">
                                @forelse ($planestudios as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->plan}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="carrera" value="{{$carrera->id}}">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->




{{--    <!-- Danger modal -->--}}
{{--    <div id="modalEliminar" class="modal fade" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <form id="formEliminar" class="modal-content" action="/pe/remove/" method="POST">--}}

{{--                @method('DELETE');--}}

{{--                <!--Importante-->--}}
{{--            @csrf--}}
{{--            <!--Importante-->--}}


{{--                <div class="modal-header bg-danger">--}}
{{--                    <h6 class="modal-title">Desea Eliminar el plan de estudios - Carrera?</h6>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}

{{--                <div class="modal-body">--}}

{{--                </div>--}}

{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>--}}
{{--                    <button type="submit" class="btn bg-danger">Eliminar Carrera</button>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
@section('script')
    <script>
        $(function () {
            $('#MyTabla').DataTable({
                "language": {
                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                }
            })
        })
    </script>
    @endsection
