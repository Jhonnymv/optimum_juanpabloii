@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('content')
    <form id="formCategoriaAlumno" name="formCategoriaAlumno" method="POST" action="{{route('storeAlumnoCategoria')}}">

        <!--Importante-->
    @csrf
    <!--Importante-->
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style=" font-weight: bold" class="card-title text-center">Editar Categoría de
                                Alumno</h5>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-3">
                                <label style="font-weight: bold; border-style: none">DNI:</label>
                                <input type="text" name="DNI" id="DNI" class="form-control" placeholder="Número de DNI"
                                       style="margin-top: 0">
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="btnBuscar">Comprobar</button>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-12 col-md-4 align-items-center">
                                <div class="form-group">
                                    <div class="">
                                        <strong for="" id="nombre" style="font-size: 20px"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="nivel" style="font-weight: bold; margin: 0"> Nivel: (<label
                                            for="nivel" style="color: red; margin: 0">*</label>)</label></label>
                                        <select id="carrera_id" name="carrera_id" class="form-control">
                                            <option>Seleccione el Nivel</option>
                                            @forelse ($carreras as $item)
                                                <option value="{{$item->id}}">
                                                    {{$item->nombre}}
                                                </option>
                                            @empty
                                            @endforelse
                                        </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="periodo" style="font-weight: bold; margin: 0">Periodo: (<label
                                            for="" style="color: red; margin: 0">*</label>)</label></label>
                                    <select id="periodo" name="periodo" class="form-control">
                                        <option>Seleccione el Periodo</option>
                                        @forelse ($periodos as $item)
                                            <option value="{{$item->id}}">
                                                {{$item->descripcion}}
                                            </option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="periodo" style="font-weight: bold; margin: 0">Categoría: (<label
                                            for="" style="color: red; margin: 0">*</label>)</label></label>
                                    <select id="categoria" name="categoria" class="form-control">
                                        <option value="Seleccionar">Seleccione una Categoría</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <div>
                                        <label for="periodo" style="font-weight: bold; margin: 0">Monto de Pensión: (<label
                                                for="" style="color: red; margin: 0">*</label>)</label></label>
                                    </div>
                                    <div>
                                        <strong for="" id="monto" style="font-size: 25px"></strong>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="">
                                <button type="button" id="btn_enviardatos"
                                        class="btn btn-primary" onclick="guardardatos()">Guardar Datos alumno
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /basic layout -->
            <input type="hidden" id="url_buscarxdni" value="{{route('buscarAlumnoxDni', [''])}}/">
            <input type="hidden" id="url_categoriasxcarrera" value="{{route('categoriaCarrera', [''])}}/">
            <input type="hidden" id="url_montoxcategoria" value="{{route('montoCategoria', [''])}}/">
            <input type="hidden" id="idAlumno" name="idAlumno">
            <input type="hidden" id="idCategoria" name="idCategoria">
        </div>
    </form>
@endsection

@section('script')
    <script>
        $(function () {
            $('#btnBuscar').click(function () {
                var dniBuscar = $('#DNI').val();
                var url = $('#url_buscarxdni').val() + dniBuscar;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data)
                        if (data) {
                            $('#idAlumno').val(data.alumno.id);
                            $('#nombre').html('Alumno: ' + data.alumno.persona.nombres + ' ' + data.alumno.persona.paterno + ' ' + data.alumno.persona.materno);
                            $('#carrera_id').val(data.alumno_categoria.carrera_id).attr('selected', true);
                            $('#carrera_id').change();
                            $('#periodo').val(data.alumno_categoria.periodo_id).prop('selected', true);
                            $('#idCategoria').val(data.alumno_categoria.categoria_id);
                            // $('#categoria option[value=' + data.alumno_categoria.categoria_id + ']').prop('selected', true);
                            if($('#categoria').change()){
                                $('#monto').html('S/. ' + data.alumno_categoria.monto);
                            }

                        } else {
                            $('#nombre').html("El Usuario no Existe");
                        }
                    }
                })
            })
        });

        $(function () {
            $('#carrera_id').change(function () {
                $('#monto').html('Seleccione una Categoría');
                $('#categoria').html('');
                var carrera_id = $(this).val();
                var url = $('#url_categoriasxcarrera').val() + carrera_id;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('#categoria').append(new Option('Seleccionar Categoría')).prop('selected', true);
                        $.each(data, function (index, value) {
                            $('#categoria').append(new Option(value.nombre+' - '+value.observacion, value.id));
                        });
                        $('#categoria').val($('#idCategoria').val());
                        $('#categoria').change();

                        if($('#categoria').val() === null){
                            $('#categoria').val('Seleccionar Categoría');
                        }
                    }
                })
            });
        });

        $(function () {
            $('#categoria').change(function () {
                $('#monto').html('S/. ');
                var categoria_id = $(this).val();
                var url = $('#url_montoxcategoria').val() + categoria_id;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('#monto').html('S/. '+data.monto);
                    }
                })
            });
        });

        function guardardatos() {
            if($('#categoria').val() === 'Seleccionar Categoría'){
                alert('Seleccione una Categoría')
            }
            else {
                $('#formCategoriaAlumno').submit();
            }
        }
    </script>
@endsection
