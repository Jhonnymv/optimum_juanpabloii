@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Periodo Académico')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Periodo Académico</h4>
@endsection

@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Un Nuevo Periodo</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Periodos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Periodos Académicos</h5>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    Este es una pantalla para la edicion y mantenimiento de carreras
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Estado</th>
                            <th>Fases</th>
                            <th>Acciones</th>
                        </tr>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($periodos as $item)
                            <tr>
                                <td scope="row">{{$loop -> iteration}}</td>
                                <td>{{$item -> descripcion}}</td>
                                <td>{{$item -> fecha_inicio}}</td>
                                <td>{{$item -> fecha_fin}}</td>
                                <td>
                                    {{ $item->estado=='1'?'Activo':'Inactivo'}}
                                </td>
                                <td>
                                    <a class="btn btn-info btn-sm" href="{{route('fasesPeriodo',['periodoId'=>$item->id])}}">Ver Fases</a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id }})"><i
                                            class="fas fa-trash"></i></a>
                                    <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})"><i
                                            class="fas fa-pen"></i></a>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No hay elementos</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
    <input type="hidden" id="url_update" value="{{route('periodos.update',[''])}}/">
    <input type="hidden" id="url_edit" value="{{route('periodos.edit',[''])}}/">
    <input type="hidden" id="url_eliminar" value="{{route('periodos.delete',[''])}}/">
@endsection


@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('periodos.store')}}">
                <!--Importante-->
            @csrf
            <!--Importante-->
                <div class="modal-header">
                    <h5 class="modal-title">Crear Periodo</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="descripcion" class="form-control" placeholder="Descripción..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Fecha de Inicio</label>
                        <div class="col-md-9">
                            <input class="form-control" type="date" name="fecha_inicio">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Fecha de Fin</label>
                        <div class="col-md-9">
                            <input class="form-control" type="date" name="fecha_fin">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->


    <!-- crear modal -->
    <div id="modalEditar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEditar" class="modal-content" method="POST">
            <!--Importante-->
            @csrf
            <!--Importante-->

                <div class="modal-header">
                    <h5 class="modal-title">Editar carrera</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" id="editdescriocion" name="descripcion" class="form-control" placeholder="Descripción..."
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Fecha de Inicio</label>
                        <div class="col-md-9">
                            <input id="editfechainicio" class="form-control" type="date" name="fecha_inicio">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Fecha de Fin</label>
                        <div class="col-md-9">
                            <input id="editfechafin" class="form-control" type="date" name="fecha_fin">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /crear modal -->

    <!-- Danger modal -->
    <div id="modalEliminar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEliminar" class="modal-content" method="POST">
                <!--Importante-->
            @csrf
            <!--Importante-->


                <div class="modal-header bg-danger">
                    <h6 class="modal-title">Desea Periodo Académico?</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-danger">Eliminar Periodo</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /default modal -->

@endsection


@section('script')
    <script>
        function eliminar(id) {
            $("#modalEliminar").modal('show');
            var url=$('#url_eliminar').val()+id
            $("#formEliminar").attr('action', url);
        }

        function editar(id) {
            var url=$('#url_update').val()+id
            $("#formEditar").attr('action', url);

            var url_edit = $('#url_edit').val()+id;
            $.ajax({
                type: "GET",
                url: url_edit,
                dataType:'json',
                success: function (data) {

                    $('#editdescriocion').val(data.periodo.descripcion);
                    $('#editfechainicio').val(data.periodo.fecha_inicio);
                    $('#editfechafin').val(data.periodo.fecha_fin);

                    $("#modalEditar").modal('show');
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }
    </script>

@endsection
