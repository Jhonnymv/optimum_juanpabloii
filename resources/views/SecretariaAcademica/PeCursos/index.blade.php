@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','PE Cursos')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - PE Cursos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir PE - Cursos </span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Plan Estudio - Cursos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
 <div class="row justify-content-center" align="right">
    <div class="col-md-12">
         <h7 class="card-title">
          {{ $pecursos->first() != null? $pecursos->first()->carrera->nombre:'' }}
           - {{ $pecursos->first() != null? $pecursos->first()->planestudio->plan:'' }}
          </h7>
    </div>
</div>

<div class="row justify-content-center">

    <div class="col-md-12">
        <!-- Basic table -->

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Cursos por Plan de Estudio</h5>

                <div class="header-elements">
                </div>
             </div>



            <div class="card-body">
                Este es una pantalla para la edicion y mantenimiento de PE Cursos
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Curso</th>
                            <th>Horas Teoria</th>
                            <th>Horas Practica</th>
                            <th>Creditos</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($pecursos as $item)

                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>

                            <td>
                                {{ $item->curso != null? $item->curso->nombre:'' }}
                            </td>
                            <td>
                                {{ $item->horas_teoria }}
                            </td>
                            <td>
                                {{ $item->horas_practica }}
                            </td>
                            <td>
                                {{ $item->creditos }}
                            </td>
                            <td>
                                {{ $item->estado }}
                            </td>
                            <td>
                                 <a href="/prerrequisitos/{{$item->id}}/{{$item->curso->id}}/{{$item->carrera->id}}/{{$item->planestudio->id}}"
                                 class="btn btn-primary" onclick="">Pre<i ></i></a>
                                  <a href="/equivalencias/{{$item->id}}/{{$item->curso->id}}/{{$item->carrera->id}}/{{$item->planestudio->id}}"
                                 class="btn btn-primary" onclick="">Equiv<i ></i></a>
                                <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id}})"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection



@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/pecursos">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo PE - Curso</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

               <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Carrera:</label>
                    <div class="col-lg-9">
                    <label class="col-lg-3 col-form-label">
                        {{$carreras->first()->nombre}}</label>
                    <input id="carreraid" name="carreraid" type="hidden"
                       value={{$carreras->first()->id}}>

                    </div>
                </div>

                 <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Plan de Estudio:</label>
                    <div class="col-lg-9">
                    <label class="col-lg-3 col-form-label">
                        {{$planestudios->first()->plan}}</label>
                    <input id="peid" name="peid" type="hidden"
                        value={{$planestudios->first()->id}}>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Curso:</label>
                    <div class="col-lg-9">
                        <select name="curso" class="form-control">

                            @forelse ($cursos as $item)
                            <option value="{{$item->id}}">
                                {{$item->nombre}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Horas Teoría:</label>
                    <div class="col-lg-9">
                        <input type="text" name="horasteoria" class="form-control" placeholder="Horas Teoría Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Horas Práctia:</label>
                    <div class="col-lg-9">
                        <input type="text" name="horaspractica" class="form-control" placeholder="Horas Práctica Ejemplo..." required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Créditos:</label>
                    <div class="col-lg-9">
                        <input type="text" name="creditos" class="form-control" placeholder="Créditos Ejemplo..." required>
                    </div>
                </div>



            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/pecursos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el plan de estudios - Curso?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Carrera</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->



@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/pecursos/remove/'+id+'/'+{{$carreras->first()->id}}
        +'/'+{{$planestudios->first()->id}});
    }
</script>

@endsection
