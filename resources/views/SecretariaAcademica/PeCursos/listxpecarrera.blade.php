@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','PE Niveles')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Plan de estudios por
        Nivel</h4>
@endsection

@section('header_buttons')
    <div class="d-flex justify-content-center">
        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Agregar Curso</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Niveles</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Niveles</h5>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    Este es una pantalla para la edición y mantenimiento de niveles.
                </div>

                <div class="table-responsive">
                    <table class="table" id="MyTabla">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
{{--                            <th>H. Teoría|H. Practica</th>--}}
                            <th>Grado</th>
{{--                            <th>Créditos</th>--}}
                            <th>Estado</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pe_cursos as $pe_curso)
                            <tr>
                                <td>
                                    {{$loop->index+1}}
                                </td>
                                <td>{{$pe_curso->curso->nombre}}</td>
{{--                                <td>{{$pe_curso->horas_teoria}} | {{$pe_curso->horas_practica}}</td>--}}
                                <td>{{$pe_curso->semestre}}</td>
{{--                                <td>{{$pe_curso->creditos}}</td>--}}
                                <td>{{$pe_curso->estado==1?'Activo':'Inactivo'}}</td>
                                <td></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No hay elementos</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
@endsection
@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('pecursos.store')}}">

                <!--Importante-->
            @csrf
            <!--Importante-->

                <div class="modal-header">
                    <h5 class="modal-title">Crear nuevo PE Nivel</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="vista" value="listxpecarrera">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Curso</label>
                        <div class="col-lg-9">
                            <select name="curso_id" class="form-control">
                                @foreach($cursos as $curso)
                                    <option value="{{$curso->id}}">
                                        {{$curso->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
{{--                    <div class="form-group row">--}}
{{--                        <label class="col-lg-3 col-form-label">Horas Teoría</label>--}}
{{--                        <div class="col-lg-2">--}}
{{--                            <input type="number" name="horas_teoria" id="" class="form-control" min="1" step="1">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group row">--}}
{{--                        <label class="col-lg-3 col-form-label">Horas Prácticas</label>--}}
{{--                        <div class="col-lg-2">--}}
{{--                            <input type="number" name="horas_practica" id="" class="form-control" min="1" step="1">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Grado</label>
                        <div class="col-lg-2">
                            <input type="number" name="semestre" id="" class="form-control" min="1" step="1">
                        </div>
                    </div>
{{--                    <div class="form-group row">--}}
{{--                        <label class="col-lg-3 col-form-label">Créditos</label>--}}
{{--                        <div class="col-lg-2">--}}
{{--                            <input type="number" name="creditos" id="" class="form-control" min="1" step="1" >--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <input type="hidden" name="pe_carrera_id" value="{{$pe_carrera->id}}">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->

{{--    <!-- Danger modal -->--}}
{{--    <div id="modalEliminar" class="modal fade" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <form id="formEliminar" class="modal-content" action="/pecarreras/remove/" method="POST">--}}

{{--                @method('DELETE');--}}

{{--                <!--Importante-->--}}
{{--            @csrf--}}
{{--            <!--Importante-->--}}


{{--                <div class="modal-header bg-danger">--}}
{{--                    <h6 class="modal-title">Desea Eliminar el plan de estudios - Carrera?</h6>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}

{{--                <div class="modal-body">--}}

{{--                </div>--}}

{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>--}}
{{--                    <button type="submit" class="btn bg-danger">Eliminar Carrera</button>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#MyTabla').DataTable({
                "language": {
                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                }
            })
        })
    </script>
@endsection
