@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cursos</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalCrear">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Añadir Nuevo Curso</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cursos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Cursos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Este es una pantalla para la edicion y mantenimiento de cursos
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Nivel:</label>
                        <div class="col-lg-4">
                            <select id="carrera" class="form-control">
                                <option>Seleccione una Nivel</option>
                                @forelse ($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @empty

                                @endforelse

                            </select>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table" id="table_cursos">
                            <thead>
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>Nombre</th>
                                <th>Nivel</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /basic table -->
        </div>
    </div>
    <input type="hidden" id="url_getcursosxcarrera" value="{{route('cursos.getcursosxcarrera')}}">
@endsection


@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="/cursos">

                <!--Importante-->
            @csrf
            <!--Importante-->

                <div class="modal-header">
                    <h5 class="modal-title">Crear nuevo curso</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nombre:</label>
                        <div class="col-lg-9">
                            <input type="text" name="nombre" class="form-control" placeholder="Curso Ejemplo..."
                                   required>
                        </div>
                    </div>

                    <input type="hidden" name="carrera" id="icarrera_id">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Descripción:</label>
                        <div class="col-lg-9">
                            <input type="text" name="descripcion" class="form-control" placeholder="Descripcion..."
                                   required>
                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->


    {{--    <!-- editar modal -->--}}
        <div id="modalEditar" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <form id="formEditar" class="modal-content" method="POST" action="/cursos">


                @method('PUT')

                <!--Importante-->
                @csrf
                <!--Importante-->

                    <div class="modal-header">
                        <h5 class="modal-title">Editar Cursos</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nombre:</label>
                            <div class="col-lg-9">
                                <input type="text" id="nombreEdit" name="nombreEdit" class="form-control"
                                       placeholder="Curso Ejemplo..."
                                       required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Carrera:</label>
                            <div class="col-lg-9">
                                <select id="carreraidEdit" name="carreraidEdit" class="form-control">

                                    @forelse ($carreras as $item)
                                        <option value="{{$item->id}}">
                                            {{$item->nombre}}
                                        </option>
                                    @empty

                                    @endforelse

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Descripción:</label>
                            <div class="col-lg-9">
                                <input type="text" id="descripcionEdit" name="descripcionEdit" class="form-control"
                                       placeholder="Descripcion..."
                                       required>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /editar modal -->


        <!-- Danger modal -->
        <div id="modalEliminar" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <form id="formEliminar" class="modal-content" action="/cursos/remove/" method="POST">

                    @method('DELETE');

                    <!--Importante-->
                @csrf
                <!--Importante-->


                    <div class="modal-header bg-danger">
                        <h6 class="modal-title">Desea Eliminar Curso?</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn bg-danger">Eliminar curso</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /default modal -->


@endsection



@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        function eliminar(id) {
            $("#modalEliminar").modal('show');
            $("#formEliminar").attr('action', '/cursos/remove/' + id);
        }

        function editar(id) {
            // console.log(id);
            $("#formEditar").attr('action', '/cursos/' + id);


            var data = {'id': id};

            $.ajax({
                type: "GET",
                url: '/traercurso',
                data: data,
                success: function (data) {
                    $('#nombreEdit').val(data['curso']['nombre']);
                    $('#descripcionEdit').val(data['curso']['descripcion']);
                    $('#carreraidEdit').val(data['curso']['carrera_id']);

                    $("#modalEditar").modal('show');
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }

        $(function () {
            $('#carrera').change(function () {
                var url = $('#url_getcursosxcarrera').val()
                var id=$(this).val()
                var data = {'id': id}
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        $('#tbody_cursos').html('')
                        data.cursos.forEach(element => {

                            if (element.estado === 1) {
                                var est = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Activo</font></font></span>"
                            } else {
                                var est = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Inactivo</font></font></span>"

                            }
                            var template = "<tr>"
                                // +"<td>"+element.index+"</td>"
                                + "<td>" + element.nombre + "</td>"
                                + "<td>" + element.carrera.nombre + "</td>"
                                + "<td>" + element.descripcion + "</td>"
                                + "<td>" + est + "</td>"
                                + "<td><button class='btn btn-warning' onclick='editar("+element.id+")'><i class='fas fa-pen'></i></button><button class='btn btn-danger' onclick='eliminar("+element.id+")'><i class='fas fa-trash'></i></button></td>"
                                + "</tr>"
                            $('#tbody_cursos').append(template)
                        })
                        $('#table_cursos').DataTable({
                            "language": {
                                "url": "{{asset('assets/DataTables/Spanish.json')}}"
                            }
                        });
                    }
                })
                $('#icarrera_id').val(id)
            })
        })
    </script>

@endsection
