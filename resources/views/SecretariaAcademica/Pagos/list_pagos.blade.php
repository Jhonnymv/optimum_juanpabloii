@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Lista de pagos <br>
                    <a class="btn btn-warning" href="{{route('baja_facturas')}}">Baja Facturas</a>
                    <a class="btn btn-warning" href="{{route('AnularBoletas')}}">Anular Boletas</a>
                </div>
                <div class="card-body" id="section_pago">
                    <div class="table-responsive">
                        <table class="table table-striped" id="myTable">
                            <thead>
                            <tr style="background-color: #4dd0e1">
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Responsable</th>
                                <th>Identificación</th>
                                <th>Tipo Doc.</th>
                                <th>Serie-Número</th>
                                <th>Sub-total</th>
                                <th>Igv</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pagos->sortBy('updated_at') as $loop=>$pago)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{date('d-m-Y',strtotime($pago->created_at))}}</td>
                                    <td>{{strtoupper($pago->persona->paterno.' '.$pago->persona->matermo.' '.$pago->persona->nombres)??''}}</td>
                                    <td>{{strtoupper($pago->persona->DNI)??$pago->ruc}}</td>
                                    @if($pago->tipo_doc==3)
                                        <td>Boleta</td>
                                    @elseif($pago->tipo_doc==7)
                                        <td>Nota de Crédito</td>
                                    @else
                                        <td>Factura</td>
                                    @endif
                                    <td>{{$pago->serie.'-'.$pago->numero_doc}}</td>
                                    <td>S/. {{$pago->fe_sub_total}}</td>
                                    <td>{{$pago->fe_igv}}</td>
                                    <td>S/. {{$pago->fe_total_price}}</td>
                                    <td><span class="badge badge-{{$pago->fe_estado=='0'?'success':($pago->fe_estado=='EnRevision'?'warning':($pago->fe_estado=='Anulado'?'danger':'info'))}}">{{$pago->fe_estado=='0'?'Facturado':($pago->fe_estado=='EnRevision'?'En revisión':($pago->fe_estado=='Anulado'?'Anulada':'Pagada'))}}</span></td>
                                    <td>
                                        <a target="_blank" href="{{route('get_pdf_factura',['pago_id'=>$pago->id])}}"><i
                                                class="fa fa-print"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#myTable').DataTable()
    </script>
@endsection
