@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Esta acción se puede realizar Una vez al día
                </div>
                <div class="card-body" id="section_pago">
                    <form id="serch_factura" action="{{route('search_factura')}}" method="post">
                        @csrf
                        <div class="row form-group">
                            <div class="col-12 col-md-3">
                                <label class="col-form-label" for="serie_numero">Factura</label>
                                <input type="text" name="serie_numero" id="serie_numero" required class="form-control">
                            </div>
                            <div class="col-12 col-md-2">
                                <button type="submit" class="btn btn-info mt-4">Buscar</button>
                            </div>
                        </div>
                    </form>

                    <form id="form_send_bajas" action="{{route('pago.anular_factura')}}" method="POST">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr style="background-color: #4dd0e1">
                                    {{--                                <th>#</th>--}}
                                    <th>Fecha</th>
                                    <th>Responsable</th>
                                    <th>Identificación</th>
                                    <th>Tipo Doc.</th>
                                    <th>Serie-Número</th>
                                    <th>Sub-total</th>
                                    <th>Igv</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="dat_facturas">
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{(asset('assets/js/moment-with-locales.js'))}}"></script>
    <script>
        moment.locale('en')
        $(function () {
            $('#serch_factura').submit(function (e) {
                e.preventDefault()
                var url = $(this).attr('action')
                var data = $(this).serialize()
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    datatype: 'json',
                    success: function (data) {
                        var template = "<tr>" +
                            "<td>" +
                            "<input type='hidden'name='pago_id[]' value='" + data.id + "'>" +
                            moment(data.updated_at).format('DD-MM-YYYY HH:mm') +
                            "</td>" +
                            "<td>" + data.persona.paterno + " " + data.persona.materno + ", " + data.persona.nombres + "</td>" +
                            "<td>" + (data.ruc == null ? data.persona.DNI : data.ruc) + "</td>" +
                            "<td>" + (data.tipo_doc == '01' ? 'Factura' : 'Boleta') + "</td>" +
                            "<td>" + data.serie + "-" + data.numero_doc + "</td>" +
                            "<td>S/ " + data.fe_sub_total + "</td>" +
                            "<td>S/ " + data.fe_igv + "</td>" +
                            "<td>S/ " + data.fe_total_price + "</td>" +
                            "<td></td>" +
                            "</tr>"
                        $('#dat_facturas').append(template)
                        $('#serie_numero').val('')
                    }
                })
            })
            $('#form_send_bajas').submit(function (e){
                e.preventDefault()
                var url = $(this).attr('action')
                var data = $(this).serialize()
                $("body").block({
                    message: '<div class="loader"></div> <p><br />Guardando los cambios...</p>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    datatype: 'json',
                    success: function (data) {
                        if (data==='success'){
                            alert('Facturas Anuladas')
                            location.reload();
                        }
                    }
                })
            })
        })
    </script>
@endsection
