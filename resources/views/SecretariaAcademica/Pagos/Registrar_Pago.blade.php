@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form action="{{route('pago_matricula.store')}}" method="post" id="p_form">
                @csrf
                <div class="card">
                    <div class="card-header">
                        Registrar pago de matrícula
                    </div>
                    <div class="card-body" id="section_pago">
                        <strong>Datos generales</strong>
                        <div class="row form-group">
                            <div class="col-12 col-md-3">
                                <label class="col-form-label" for="fecha">Concepto</label>
                                <select name="concepto_id" id="concepto_id" class="form-control" required>
                                    <option value="">Seleccione un concepto de pago</option>
                                    @foreach($conceptos as $concepto)
                                        <option value="{{$concepto->id}}">{{$concepto->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-2">
                                <label class="col-form-label" for="fecha">Fecha</label>
                                <input class="form-control" type="date" name="fecha" id="fecha" readonly
                                       value="{{date('Y-m-d')}}">
                            </div>
                            <div class="col-12 col-md-3">
                                <label class="d-block">Tipo Comprobante:</label>
                                <label style=" border-style: none">
                                    Boleta:
                                    <input type="radio" name="tipo_doc"
                                           id="tipodoc_03"
                                           class="form-control"
                                           value="03" checked></label>
                                <label class="ml-4" style=" border-style: none">
                                    Factura:
                                    <input
                                        type="radio"
                                        name="tipo_doc"
                                        id="tipodoc_01"
                                        class="form-control ml-4"
                                        value="01"></label>
                            </div>
                            <div class="col-12 col-md-2">
                                <label class="col-form-label" for="serie">Serie Boleta</label>
                                <input class="form-control" type="text" name="serie" id="serie" readonly>
                            </div>
                            <div class="col-12 col-md-2">
                                <label class="col-form-label" for="numero">Número de Documento</label>
                                <input class="form-control" type="text" name="numero" id="numero" readonly>
                            </div>
                        </div>
                        <strong>Datos del padre / empresa cliente</strong>
                        <div class="row form-group" id="sec_dni" style="display: none">
                            <div class="col-12 col-md-3">
                                <label class="col-form-label" for="fecha">DNI</label>
                                <input type="hidden" name="id_cliente" id="id_cliente">
                                <input class="form-control" type="text" name="doc_cliente" id="doc_cliente">
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="col-form-label" for="nombre_cliente">Nombre</label>
                                <input class="form-control" type="text" name="nombre_cliente" id="nombre_cliente"
                                       readonly>
                            </div>
                        </div>
                        <div class="row form-group" id="sec_ruc" style="display: none">
                            <div class="col-12 col-md-3">
                                <label class="col-form-label" for="doc_ruc">RUC</label>
                                <input class="form-control" type="text" name="doc_ruc" id="doc_ruc">
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="col-form-label" for="nombre_empresa">Nombre empresa</label>
                                <input class="form-control" type="text" name="nombre_empresa" id="nombre_empresa"
                                       readonly>
                            </div>
                        </div>
                        <strong>Detalle de pago</strong>
                        <div class="row form-group" id="sec_add_alumno" style="display: none">
                            <div class="col-12 col-md-2">
                                <label for="dni_alumno" class="col-12">DNI Alumno</label>
                                <select name="dni_alumno" id="dni_alumno" class="form-control">
                                    <option>Seleccione un estudiante</option>
                                </select>
{{--                                <input type="text" id="dni_alumno" class="form-control">--}}
                            </div>
                            <div class="col-12 col-md-10">
                                <label for="pension" class="col-12 col-md-3">Pensión</label>
                                <div id="pensiones">
                                    <table class="table table-striped table-sm">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Alumno</th>
                                            <th>Monto</th>
                                            <th>Mes</th>
                                            <th>F. vencimiento</th>
                                            <th>Acción</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table_penciones">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group" id="sec_add_detalle" style="display: none">
                            <div class="col-12 col-md-5">
                                <label for="Servivio" class="col-12">Servicio</label>
                                <input type="text" id="servicio" class="form-control">
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="monto_servicio" class="col-12">Monto</label>
                                <input type="text" id="monto_servicio" class="form-control">
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="cantidad_servicio" class="col-12">Cantidad</label>
                                <input type="text" id="cantidad_servicio" class="form-control">
                            </div>
                            <div class="col-12 col-md-1">
                                <button type="button" class="btn btn-info" onclick="add_detalle()">Agregar</button>
                            </div>
                        </div>
                        <div>
                            <table class="table table-striped" id="pago_alumnos" style="display:none">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Estudiante</th>
                                    <th style="width: 30px">Nivel</th>
                                    <th style="width: 30px">Grado</th>
                                    <th style="width: 30px">Categoría</th>
                                    <th style="width: 30px">Monto</th>
                                    <th style="width: 30px"></th>
                                </tr>
                                </thead>
                                <tbody id="data_detalle">
                                </tbody>
                            </table>
                            <table class="table table-striped" id="pago_servicio" style="display:none">
                                <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>Servicio</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>Sub. total</th>
                                    <th>Igv</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_servicio">

                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="total" id="total">
                        <div class="row text-center" id="card_total" style="display: none">
                            <div class="col-lg-4">
                                <!-- Today's revenue -->
                                <div class="card bg-blue-400" style="zoom: 1;">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <h3 class="font-weight-semibold mb-0 total"></h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- /today's revenue -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-success" type="button"
                                onclick="enviar()">
{{--    >--}}
                            Enviar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <input type="hidden" name="nhijo" id="nhijo">
    </div>
@endsection
@section('script')
    <script>
        function enviar() {
            var tipo = $('input[name="tipo_doc"]').val()
            if (navigator.onLine) {
                $("body").block({
                    message: '<div class="loader"></div> <p><br />Guardando los cambios...</p>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
                var url = $('#p_form').attr('action')
                var data = $('#p_form').serialize()
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data)
                        $("body").unblock();
                        if (data === 'success') {
                            alert('Comprobante y pago generados correctamente')
                            location.reload()
                        } else
                            alert('Ocurrió un error al realizar la facturación, pero los datos de pago fueron guardados');
                    }
                })
            } else {
                alert('Para realizar esta operación debe estar conectado a internet')
            }
        }
        function total() {
            var tot = 0;
            let sum = 0;
            $('.monto').each(function () {
                sum += parseFloat($(this).val());
            });
            tot = sum;
            $('#total').val(tot)
            $('.total').html('S/ ' + tot)
            $('#card_total').show()
        }
        // var totalicimo;//variable para controlar el precio total de la matrícula en el caso de remover un item
        $(function () {
            $('input[name="tipo_doc"]').change(function () {
                var text = $('#concepto_id option:selected').text().toUpperCase()
                var checkMt = text.indexOf('MATR')
                var checkPen = text.indexOf('PENSI')
                var data = {'tipo_doc': $(this).val()}
                $.ajax({
                    url: '/Secretaria_Academica/get_sequence_num',
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        var  num=parseInt(data.sequence.current)
                        $('#serie').val(data.sequence.serie)
                        $('#numero').val(('00000' + (num += 1)).slice(-6))
                    }
                })
                if ($(this).val() === '01') {
                    $('#sec_dni').show().val('')
                    if (checkMt !== -1 || checkPen !== -1)
                        $('#sec_ruc').show().val('');
                    else
                        $('#sec_ruc').hide().val('');
                    $('#nombre_cliente').siblings('label').text('Razón Social:')
                    $('#detalle_servicio').html('')
                }
                if ($(this).val() === '03') {
                    $('#sec_dni').show()
                    $('#sec_ruc').hide().val('');
                    $('#nombre_cliente').siblings('label').text('Nombre:')
                    $('#detalle_servicio').html('')

                }
            })

            $('#doc_cliente').change(function (e) {
                $('#data_detalle').html('')
                if ($(this).val().length === 8) {
                    if ($("#p_form input[name='tipo_doc']:radio").is(':checked')) {
                        var text = $('#concepto_id option:selected').text().toUpperCase()
                        var checkMt = text.indexOf('MATR')
                        var checkPen = text.indexOf('PENSI')
                        if (checkMt !== -1) {
                            $.ajax({
                                url: '/admin/search_person/' + $(this).val() + '/' + $('#concepto_id').val(),
                                type: 'GET',
                                dataType: 'json',
                                success: function (data) {
                                    // console.log(data)
                                    $('#id_cliente').val(data.persona.id)
                                    $('#nombre_cliente').val(data.persona.paterno + ' ' + data.persona.materno + ', ' + data.persona.nombres)
                                    var c = 0
                                    data.alumnos.forEach(element => {
                                        var template = "<tr>" +
                                            "<td>" + (c + 1) + "</td>" +
                                            "<td>" +
                                            "<input type='hidden' name='alumno_id[]' value='" + element.alumno_id + "'>" +
                                            "<input type='text' readonly style='border: none; width: 100%' name='descripcion[]' value='PAGO MATRÍCULA ALUMNO " + element.alumno + "'>" +
                                            "</td>" +
                                            "<td>" + element.nivel + "</td>" +
                                            "<td>" + element.grado + "</td>" +
                                            "<td>" + element.categoria + "</td>" +
                                            "<td><input class='monto' type='text' style='border: none'  name='monto[]' value='" + element.monto + "'></td>" +
                                            "<td><button class='btn btn-danger borrar'><i class='fa fa-trash'></i></button></td>" +
                                            "</tr>"
                                        $('#data_detalle').append(template)
                                        c += 1
                                        $('#nhijo').val(c);
                                    })
                                    total()
                                }
                            })
                        }
                        if (checkPen !== -1) {
                            $.ajax({
                                url: '/admin/search_person_pension/' + $(this).val(),
                                type: 'GET',
                                dataType: 'json',
                                success: function (data) {
                                    $('#id_cliente').val(data.persona.id)
                                    $('#nombre_cliente').val(data.persona.paterno + ' ' + data.persona.materno + ', ' + data.persona.nombres)
                                    var data = {'padre_id': data.persona.padre.id}
                                    $.ajax({
                                        url:'/Secretaria_Academica/getAlumnoByPadre/',
                                        type: 'GET',
                                        data: data,
                                        datatype: 'json',
                                        success: function (data) {
                                            data.alumnos.forEach(element =>{
                                                $("#dni_alumno").append(new Option(element.dni+' '+element.nombre+' '+element.nivel, element.dni));
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    } else {
                        alert("Seleccione un tipo de comprobante");
                    }
                }
            })

            $('#doc_ruc').change(function (e) {
                var ruc = $(this).val()
                if ($(this).val().length === 11) {
                    $("body").block({
                        message: '<div class="loader"></div> <p><br />Buscando Ruc en sunat...</p>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    $.ajax({
                        url: '/Secretaria_Academica/search_ruc/' + $(this).val(),
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            $("body").unblock();
                            console.log(data)
                            if (data.res === 'Not found') {
                                alert('No se econtró una compañía con el ruc ' + ruc)
                            } else {
                                $('#nombre_empresa').val(data.razonSocial)
                            }
                        }
                    })
                }
            })

            $('#concepto_id').change(function () {
                var text = $('#concepto_id option:selected').text().toUpperCase()
                var checkMt = text.indexOf('MATR')
                var checkPen = text.indexOf('PENSI')
                if (checkMt === -1 && checkPen === -1) {
                    // $('#tipodoc_01').parent().show()
                    $('#pago_alumnos').hide()
                    $('#pago_servicio').show()
                    $('#sec_add_detalle').show()
                    $('#sec_add_alumno').hide()
                    $('#sec_dni').show()
                } else {
                    // $('#tipodoc_01').prop('checked', false).parent().hide()
                    // $('#tipodoc_03').prop('checked', true).change()
                    $('#pago_alumnos').show()
                    $('#pago_servicio').hide()
                    $('#sec_add_detalle').hide()
                    $('#sec_dni').show()
                    if (checkPen !== -1)
                        $('#sec_add_alumno').show()
                    else
                        $('#sec_add_alumno').hide()
                }
                $('#tipodoc_03').change()
                $('#data_detalle').html('')
                total()
            })

        })

        function add_detalle() {
            var pric = $('#monto_servicio').val()
            var cant = $('#cantidad_servicio').val()
            var msi = pric / 1.18
            var sub = msi * cant
            var ig = (pric - msi) * cant
            var tot = sub + ig
            var template =
                "<tr>" +
                //    "<td>"+1+"</td>"+
                "<td><input class='form-control' name='service_name[]' value='" + $('#servicio').val() + "' readonly></td>" +
                "<td>" + msi.toFixed(2) + "</td>" +
                "<td><input class='form-control' name='quantity[]' value='" + cant + "' readonly></td>" +
                "<td>" + sub.toFixed(2) + "</td>" +
                "<td>" + ig.toFixed(2) + "</td>" +
                "<td><input class='monto' type='text' style='border: none' name='monto[]' value='" + tot.toFixed(2) + "' readonly>" +
                "<input type='hidden' name='price[]' value='" + pric + "' readonly>" +
                "</td>" +
                "<td><button class='btn btn-danger borrar'><i class='fa fa-trash'></i></button></td>" +
                "</tr>"
            $('#detalle_servicio').append(template)
            total()
            $('#monto_servicio').val('')
            $('#cantidad_servicio').val('')
            $('#servicio').val('')
        }

        $('#dni_alumno').change(function () {
            $('#data_detalle').html('')
            $('#table_penciones').html('')
            total()
            // if ($(this).val().length == 8) {
                $.ajax({
                    url: '/Secretaria_Academica/get_alumnoPagoPenciones/' + $(this).val(),
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data)
                        if (data === 'sinMatricula')
                            alert('el estudiante no se ncuentra matriculado')
                        else {
                            if (data.alumnopagos.length) {
                                $.each(data.alumnopagos, function (index, element) {
                                    var template = "<tr>" +
                                        "<td>" + (index + 1) + "</td>" +
                                        "<td><input readonly style='border: none; width: 100%' id='name_al_" + element.id + "' value='" + element.alumno + "'>" +
                                        "<input type='hidden' id='alumno_id_" + element.id + "' value='" + element.alumno_id + "'>" +
                                        "<input type='hidden' id='nivel_" + element.id + "' value='" + element.nivel + "'>" +
                                        "<input type='hidden' id='cronograma_id_" + element.id + "' value='" + element.cronograma_id + "'>" +
                                        "<input type='hidden' id='grado_" + element.id + "' value='" + element.grado + "'>" +
                                        "<input type='hidden' id='categoria_" + element.id + "' value='" + element.categoria + "'>" +
                                        "</td>" +
                                        "<td><input style='border: none' id='monto_al_" + element.id + "' value='" + element.monto + "'></td>" +
                                        "<td><input readonly style='border: none' id='mes_al_" + element.id + "' value='" + element.mes + "'></td>" +
                                        "<td>" + element.fecha_vencimiento + "</td>" +
                                        "<td><button type='button' class='btn btn-primary btn-sm borrar' onclick='add_al(" + element.id + ")'>Seleccionar</button></td>" +
                                        "</tr>"

                                    $('#table_penciones').append(template)
                                })
                            } else alert('El estudiante no tiene pensiones pendientes')
                        }
                    }
                })
            // }
        })

        function add_al(id) {
            var pric = parseFloat($('#monto_al_' + id).val())

            var template =
                "<tr>" +
                "<td></td>" +
                "<td><input class='form-control' name='descripcion[]' value='PAGO PENSIÓN ALUMNO: " + $('#name_al_' + id).val() + ". MES DE: "+$('#mes_al_' + id).val()+"' readonly>" +
                "<input type='hidden' name='alumno_id[]' value='" + $('#alumno_id_' + id).val() + "'>" +
                "<input type='hidden' name='cronograma_id[]' value='" + $('#cronograma_id_' + id).val() + "'>" +
                "</td>" +
                "<td>" + $('#nivel_' + id).val() + "</td>" +
                "<td>" + $('#grado_' + id).val() + "</td>" +
                "<td>" + $('#categoria_' + id).val() + "</td>" +
                "<td><input class='monto' type='text' style='border: none' name='monto[]' value='" + pric.toFixed(2) + "'></td>" +
                "<td><button class='btn btn-danger borrar'><i class='fa fa-trash'></i></button></td>" +
                "</tr>"
            $('#data_detalle').append(template)
            total()
        }
        $(document).on('click', '.borrar', function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
            total()
        });
        $(document).on('keyup','.monto',function (event){
            event.preventDefault()
            total()
        })
    </script>
@endsection
