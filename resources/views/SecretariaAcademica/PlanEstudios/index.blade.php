@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','PlanEstudio')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Plan de Estudio</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Nuevo Plan de Estudio</span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Plan Estudio</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Planes de Estudio</h5>
                <div class="header-elements">

                </div>
            </div>

            <div class="card-body">
                Este es una pantalla para la edicion y mantenimiento de planes de estudios
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Plan</th>
                            <th>fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($planestudios as $item)
                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>
                            <td>
                                {{ $item->plan }}
                            </td>
                            <td>
                                {{ $item->fecha_inicio }}
                            </td>
                            <td>
                                {{ $item->fecha_fin }}
                            </td>
                            <td>
                                {{ $item->estado==1?'Activo':'Inactivo' }}
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})">
                                  <i class="fas fa-pen"></i></a>
                                <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id }})"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection


@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/planestudios">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo plan de estudio</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

        <div class="modal-body">

            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Plan Estudio:</label>
                <div class="col-lg-9">
                <input type="text" name="plan" class="form-control" placeholder="Plan Estudio Ejemplo..." required>
                </div>
            </div>

            <div class="form-group row">
				<label class="col-form-label col-md-2">Fecha Inicio:</label>
					<div class="col-md-10">
					<input class="form-control" type="date" name="fecha_inicio" required>
                    </div>
			</div>

            <div class="form-group row">
				<label class="col-form-label col-md-2">Fecha Fin:</label>
					<div class="col-md-10">
					  <input class="form-control" type="date" name="fecha_fin" required>
                    </div>
			</div>
         </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->

<!-- crear modal -->
<div id="modalEditar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEditar" class="modal-content" method="POST" action="/cursos">


            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Editar Plan de Estudio</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Plan Estudio:</label>
                <div class="col-lg-9">
                <input type="text" id="planEdit" name="planEdit" class="form-control" placeholder="Plan Estudio Ejemplo..." required>
                </div>
            </div>

            <div class="form-group row">
				<label class="col-form-label col-md-2">Fecha Inicio:</label>
					<div class="col-md-10">
					<input class="form-control" type="date" id="fechaInicioEdit" name="fechaInicioEdit" required>
                    </div>
			</div>

            <div class="form-group row">
				<label class="col-form-label col-md-2">Fecha Fin:</label>
					<div class="col-md-10">
					  <input class="form-control" type="date" id="fechaFinEdit" name="fechaFinEdit" required>
                    </div>
			</div>
         </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->


<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/planestudios/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Plan de Estudio?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar curso</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->



@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/planestudios/remove/'+id);
    }

    function editar(id){
        //dd(id);
        $("#formEditar").attr('action','/planestudios/'+id);


        var data = {'id':id};

        $.ajax({
                type: "GET",
                url: '/traerplanestudio',
                data: data,
                success: function(data) {
                    $('#planEdit').val(data['planestudio']['plan']);
                    $('#fechaInicioEdit').val(data['planestudio']['fecha_inicio']);
                    $('#fechaFinEdit').val(data['planestudio']['fecha_fin']);

                    $("#modalEditar").modal('show');
                },
                error: function() {
                    console.log("ERROR");
                }
            });
    }
</script>

@endsection
