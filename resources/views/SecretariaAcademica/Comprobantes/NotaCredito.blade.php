<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota de Crédito</title>
    <style>
        table {
            width: 100%;
        }
    </style>
</head>
<body>
<div style="justify-content: left; margin-left: 0;">
{{--    <img src="{{asset('assets/img/logo-juanPabloII.png')}}"--}}
{{--         style="width: 10%;padding-bottom: -15px; float: left; margin-top: 10px" alt="">--}}
    <img src="{{public_path()}}/assets/img/logo-juanPabloII.png"
         style="width: 10%;padding-bottom: -15px; float: left; margin-top: 10px" alt="">
</div>
<table>
    <tr>
        <td style="text-align: left; width: 60%">
            <p><h5>INSTITUCION EDUCATIVA PARTICULAR BEATO JUAN PABLO II S.R.L</h5></p>
            <p>JR. EL INCA N° 524</p>
            <p>CAJAMARCA - CAJAMARCA - CAJAMARCA - BAR. LA COLMENA</p>
        </td>
        <td style="text-align: center; width: 30%; border: 1px solid black;">
            <div>
                <strong>NOTA DE CRÉDITO</strong><br>
                <p>RUC:20607116114 </p>
                <p>{{$pago->serie.'-'.$pago->numero_doc}}</p>
{{--                <p style="color: red">{{$pago->fe_estado=='Anulado'?'ANULADO':''}}</p>--}}
            </div>
        </td>
    </tr>
</table>
<br>
<div style="border: 1px solid black;">
    <table>
        <tr>
            <td style="width: 10%;text-align: left; font-size: 10px">
                <strong>{{$pago->tipo_doc=='01'?'Razón Social':'Señor(es)'}}</strong><br>
                <strong>{{$pago->tipo_doc=='01'?'RUC':'DNI'}}</strong><br>
                <strong>DIRECCIÓN</strong><br>
            </td>
            <td style="width: 40%;text-align: left; font-size: 10px">
                <p>
                    : {{$pago->tipo_doc=='01'? $pago->social_reason:strtoupper($pago->persona->paterno.' '.$pago->persona->materno.' '.$pago->persona->nombres)}}</p>
                <p>: {{$pago->tipo_doc=='01'? $pago->ruc:$pago->persona->DNI}}</p>
                <p>: {{strtoupper($pago->persona->direccion)}}</p>
            </td>
            <td style="width: 20%; font-size: 10px; text-align: end">
                <strong>FECHA DE EMISIÓN</strong><br>
                <strong>FECHA DE VENCIMIENTO</strong><br>
                <strong>MONEDA</strong>
            </td>
            <td style="width: 30%; font-size: 10px ">
                <P>: {{date('d-m-Y',strtotime($pago->created_at))}}</P>
                <P>: </P>
                <P>: Sol</P>
            </td>
        </tr>
    </table>
</div>
<br>
<div style="border: 1px solid black;">
    <table>
        <tr>
            <td style="width: 20%;text-align: left; font-size: 10px">
                <strong>Documento referencia</strong><br>
                <strong>N° documento referencia</strong><br>
                <strong>F. documento referencia</strong><br>

            </td>
            <td style="width: 40%;text-align: left; font-size: 10px">
                <p>
                    : {{$pago->serie=='BC01'? 'BOLETA':'FACTURA'}}</p>
                <p>: {{$pago_ref->serie}} - {{$pago_ref->numero_doc}}</p>
                <p>: {{$pago_ref->created_at}}</p>
            </td>
            <td style="width: 25%; font-size: 10px; text-align: end">
                <strong>Sustento</strong><br>
                <strong>Cod. Tipo de nota de crédito</strong><br>
                <strong>Tipo de ntoa de crédito</strong>
            </td>
            <td style="width: 30%; font-size: 10px ">
                <p>: Al sustento de la nota de crédito</p>
                <P>: {{$pago->serie=='BC01'? '03':'01'}}</P>
                <P>: Anulación de operación</P>
            </td>
        </tr>
    </table>
</div>
<br>
<div style="border: 1px solid black;">
    <table>
        <thead>
        <tr>
            <th style="font-size: 10px">CANTIDAD</th>
            <th style="font-size: 10px">DESCRIPCIÓN</th>
            <th style="font-size: 10px">PRECIO UNITARIO</th>
            <th style="font-size: 10px">IGV</th>
            <th style="font-size: 10px">IMPORTE</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pago->alumnos as $alumno)
            <tr>
                <td style="font-size: 10px; text-align: center">{{$alumno->pivot->cantidad}}</td>
                <td style="font-size: 10px;">{{strtoupper($alumno->pivot->fe_descripcion)}}</td>
                <td style="font-size: 10px; text-align: center">{{number_format((-1.00*$alumno->pivot->fe_valor_venta),2)}}</td>
                <td style="font-size: 10px; text-align: center">0.00</td>
                <td style="font-size: 10px; text-align: center">{{number_format((-1.00*$alumno->pivot->fe_valor_venta),2)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div style=" width: 50%; float: right; float: right">
    <table style="border-collapse: collapse; border: 0.5px solid black">
        <tr>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black;">Total del Valor
                de Venta:
            </td>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black; text-align: right">
                {{number_format((-1*$pago->fe_sub_total),2)}}
            </td>
        </tr>
        <tr>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black;">IGV:</td>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black; text-align: right">
                {{number_format((-1*$pago->fe_igv),2)}}
            </td>
        </tr>
        <tr>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black;">Importe Total:
            </td>
            <td style="font-size: 10px; border-collapse: collapse; border: 0.5px solid black; text-align: right">
                {{number_format((-1*$pago->fe_total_price),2)}}
            </td>
        </tr>
    </table>
</div>
<br>
<table>
    <tr>
        <td style="width: 50%">
            <label style="font-size: 10px">http://juanpabloii.optimumlab.io</label> <br>
{{--            <img style="width: 15%; margin-left: 10px" alt="" src="{{asset('qrcodes/qrcodecomprobante.svg')}}"><br>--}}
            <img style="width: 15%; margin-left: 10px" alt="" src="{{public_path()}}/qrcodes/qrcodecomprobante.svg"><br>
            <label style="font-size: 10px">Representación Impresa de
                la                                     @if($pago->tipo_doc==3)
            Boleta.
        @elseif($pago->tipo_doc==7)
           Nota de Crédito.
        @else
            Factura.
        @endif</label>
        </td>
    </tr>
</table>

</body>
</html>
