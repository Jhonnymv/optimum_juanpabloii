@extends('Layouts.main')
@section('title','Prerrequisitos')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Prerrequisitos</h4>
@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

    <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
        data-target="#modalCrear">
        <i class="icon-calendar5 text-pink-300"></i>
        <span>Añadir Prerrequisito </span>
    </a>
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Prerrequisito</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
 <div class="row justify-content-center" align="right">
    <div class="col-md-12">
         <h7 class="card-title">
          {{ $pecursos->first() != null? $pecursos->first()->carrera->nombre:'' }}
          - {{ $pecursos->first() != null? $pecursos->first()->planestudio->plan:'' }}
          - {{ $pecursos->first() != null? $pecursos->first()->curso->nombre:'' }}


          </h7>
    </div>
</div>

<div class="row justify-content-center">

    <div class="col-md-12">
        <!-- Basic table -->

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Prerrequisitos</h5>

                <div class="header-elements">
                </div>
             </div>



            <div class="card-body">
                Este es una pantalla para la edicion y mantenimiento de Prerrequisitos
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Prerrequisitos</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($prerrequisitos as $item)

                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>

                            <td>
                                {{ $item->cursopre != null? $item->cursopre->nombre:'' }}
                            </td>
                            <td>
                                {{ $item->estado }}
                            </td>
                            <td>
                                <a href="#" class="btn btn-danger" onclick="eliminar({{ $item->id}})"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection



@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="/prerrequisitos">

            <!--Importante-->
            @csrf
            <!--Importante-->

            <div class="modal-header">
                <h5 class="modal-title">Crear Prerrequisito</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Prerrequisito:</label>
                    <div class="col-lg-9">
                        <select name="cursospre" class="form-control">

                            @forelse ($cursospre as $item)
                            <option value="{{$item->id}}">
                                {{$item->nombre}}
                            </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                </div>

                <div>
                   <input id="pecursosid" name="pecursosid" type="hidden"
                        value={{$pecursos->first()->id}}>
                    <input id="carreraid" name="carreraid" type="hidden"
                        value={{$pecursos->first()->carrera_id}}>
                    <input id="cursoid" name="cursoid" type="hidden"
                        value={{$pecursos->first()->curso_id}}>
                    <input id="peid" name="peid" type="hidden"
                        value={{$pecursos->first()->plan_estudio_id}}>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->




<!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/prerrequisitos/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar el Curso Prerrequisito?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Prerrequisito</button>
            </div>
        </form>
    </div>
</div>
<!-- /default modal -->



@endsection


@section('script')
<script>
    function eliminar(id){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/prerrequisitos/remove/'+id
        +'/'+{{$pecursos->first()->id}}
        +'/'+{{$pecursos->first()->curso_id}}
        +'/'+{{$pecursos->first()->carrera_id}}
        +'/'+{{$pecursos->first()->plan_estudio_id}});
    }
</script>

@endsection
