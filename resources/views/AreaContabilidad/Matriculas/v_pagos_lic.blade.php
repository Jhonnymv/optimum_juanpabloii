@extends('AreaContabilidad.Layouts.main_Conta')
@section('title','Cursos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Pagos
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/media/fancybox.min.js"></script>

    <script src="/assets/global_assets/js/demo_pages/gallery.js"></script>
    <!-- /theme JS files -->
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar Pagos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de matriculas</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los pagos realizados por Licencia de Estudios
                    <div class="table-responsive">
                        <table class="table" id="table_licencias">
                            <thead>
                            <tr>

                                <th>Alumno</th>
                                <th>Correo</th>
                                <th>Nivel</th>
                                <th>Fecha de Registro</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            @foreach($licencias as $licencia)
                                <tr>

                                    <td>{{ $licencia->matricula->alumnocarrera->alumno->persona->paterno.' '.$licencia->matricula->alumnocarrera->alumno->persona->materno.', '.$licencia->matricula->alumnocarrera->alumno->persona->nombres}}</td>

                                    <td>{{ $licencia->matricula->alumnocarrera->alumno->persona->email}}</td>
                                    <td>{{ $licencia->matricula->alumnocarrera->carrera->nombre}}</td>
                                    <td>{{date('d-m-Y',strtotime( $licencia->created_at))}}</td>
                                    <td>{{ $licencia->estado}}</td>
                                    <td>
                                      <button type="button" data-toggle='modal' data-target='#modal-detalle'
                                                onclick="get_voucher({{ $licencia->id}})"
                                                class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                title="Revisar Voucher">
                                            <i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_getvoucher" value="{{route('vouchers.getxlicencia',[''])}}/">
@endsection

@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Basic modal</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-cash mr-2"></i> Detalle de pago
                                </legend>

                                <div class="form-group">
                                    <label>Número de operación:</label>
                                    <span id="num_operacion"></span>
                                </div>
                                <div class="form-group">
                                    <label>Fecha:</label>
                                    <span id="fecha"></span>
                                </div>
                                <div class="form-group">
                                    <label>Monto:</label>
                                     S/<span id="monto"></span>
                                </div>
                                <div class="form-group">
                                    <label>Obsercación: </label>
                                    <span id="observacion_pago"></span>
                                </div>

                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-image2 mr-2"></i> Imagen del
                                    comprobante
                                </legend>
                                <div class="card">
                                    <div class="card-img-actions m-1">
                                        <img class="card-img img-fluid" id="img_voucher" alt="">
                                        <div class="card-img-actions-overlay card-img">
                                            <a id="a_voucher"  class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round legitRipple" data-popup="lightbox" rel="group">
                                                <i class="icon-plus3"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
{{--                                <img id="img_comprobante" style="height: 20em;width: 20em">--}}
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="licencia_id">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-primary legitRipple" onclick="aprobar()">Aprobar</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_actualizar_licencia" value="{{route('licencia.actualizarestado')}}">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_licencias').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_voucher(id) {
            var url = $('#url_getvoucher').val() + id

             console.log(url)
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                     console.log(data)
                    $('#num_operacion').text(data.voucher.num_operacion)
                    $('#fecha').text(data.voucher.fecha)
                    $('#monto').text(data.voucher.monto)
                    $('#licencia_id').val(data.voucher.licencia_id)
                    // $('#img_comprobante').attr('src',data.urlimg)
                     $('#img_voucher').attr('src',data.urlimg)
                     $('#a_voucher').attr('href',data.urlimg)
                     $('#observacion_pago').text(data.voucher.licencia.observaciones)

                }
            })
        }

        function aprobar() {
            var url = $('#url_actualizar_licencia').val()
            var id = $('#licencia_id').val()
            var data = {'id': id, 'estado': 'CONFIRMADA'}
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    location.reload()
                }
            })
        }

    </script>
@endsection
