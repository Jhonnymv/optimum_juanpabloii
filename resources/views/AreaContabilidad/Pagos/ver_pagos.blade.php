@extends('AreaContabilidad.Layouts.main_Conta')
@section('title','Pagos')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Ver Pagos
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/media/fancybox.min.js"></script>

    <script src="/assets/global_assets/js/demo_pages/gallery.js"></script>
    <!-- /theme JS files -->

@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Ver Pagos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Pagos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para los pagos de los alumnos
                    <div class="table-responsive">
                        <table class="table" id="table_matriculas">
                            <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>DNI</th>
                                <th>Correo</th>
                                <th>Nivel</th>
                                <th>Fecha de Registro</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            @foreach($matriculas as $matricula)
                                <tr>
                                    <td>{{$matricula->alumnocarrera->alumno->persona->paterno.' '.$matricula->alumnocarrera->alumno->persona->materno.', '.$matricula->alumnocarrera->alumno->persona->nombres}}</td>
                                    <td>{{$matricula->alumnocarrera->alumno->persona->DNI}}</td>
                                    <td>{{$matricula->alumnocarrera->alumno->persona->email}}</td>
                                    <td>{{$matricula->alumnocarrera->carrera->nombre}}</td>
                                    <td>{{date('d-m-Y',strtotime($matricula->created_at))}}</td>
                                    <td>
                                        <button type="button" data-toggle='modal' data-target='#modal-detalle'
                                                onclick="get_voucher({{$matricula->id}})"
                                                class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                title="Revisar Matrícula">
                                            <i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_getvoucher" value="{{route('vouchers.ver_voucher',[''])}}/">
@endsection

@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header col-md-12" style="text-align: center">
                    <h5 class="modal-title">Detalle de Pago</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body" id="modal_detalle">

                </div>

                <div class="modal-footer">
                    <input type="hidden" id="matricula_id" readonly>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
    <input type="text" id="storagep" value="{{asset('storage/')}}">
@endsection



@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_matriculas').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_voucher(id) {
            $('#modal_detalle').html('')
            var url = $('#url_getvoucher').val() + id
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    var st = $('#storagep').val()
                    data.voucher.forEach(element => {
                        console.log(st+element.urlimg)
                        var template = "<div class='row'>" +
                            "<div class='col-md-4'>" +
                            "<fieldset>" +
                            "<legend class='font-weight-semibold'><i class='icon-cash mr-2'></i> Detalle de Pago</legend> " +
                            "<div class='form-group'> <label>Número de operación:</label> <span>"+element.num_operacion+"</span> </div> " +
                            "<div class='form-group'> <label>Fecha:</label> <span>"+element.fecha+"</span> </div> " +
                            "<div class='form-group'> <label>Monto:</label>S/<span>"+element.monto+"</span> </div> " +
                            "<div class='form-group'> <label>Obsercación: </label> <textarea rows='2' class='form-control' readonly>"+element.matricula.observaciones+"</textarea> </div> </fieldset> </div> " +
                            "<div class='col-md-8'> " +
                            "<fieldset> " +
                            "<legend class='font-weight-semibold'><i class='icon-image2 mr-2'></i> Imagen del Comprobante </legend> " +
                            "<div class='card'> " +
                            "<div class='card-img-actions m-1'> " +
                            "<iframe class='card-img img-fluid'  scrolling='auto' src='"+st+"/"+element.urlimg+"' style='width: 50em;height: 30em; overflow:scroll' allowfullscreen></iframe> " +
                            "</div> " +
                            "</div> "
                        $('#modal_detalle').append(template)
                    })
                }
            })
        }

    </script>
@endsection
