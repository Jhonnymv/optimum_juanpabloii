@extends('AreaContabilidad.Layouts.main_Conta')
@section('title','Pagos Postulantes')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Revisar Pagos
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/media/fancybox.min.js"></script>

    <script src="/assets/global_assets/js/demo_pages/gallery.js"></script>
    <!-- /theme JS files -->
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Revisar Pagos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Postulantes</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para verificar los pagos realizados por Postulantes
                    <div class="table-responsive">
                        <table class="table" id="table_licencias">
                            <thead>
                            <tr>

                                <th>Postulante</th>
                                <th>DNI</th>
                                <th>Programa</th>
                                <th>Fecha de solicitud</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_cursos">
                            @foreach($postulaciones as $postulacion)
                                <tr>

{{--                                    <td>{{ $postulacion->postulante->persona->paterno.' '.$postulacion->postulante->persona->materno.', '.$postulacion->postulante->persona->nombres}}</td>--}}
{{----}}
                                    <td>{{ $postulacion->postulante->persona->DNI}}</td>
                                    <td>{{ $postulacion->pe_carrera->carrera->nombre}}</td>
                                    <td>{{date('d-m-Y',strtotime( $postulacion->created_at))}}</td>
                                    <td>{{ $postulacion->estado}}</td>
                                    <td>
                                      <button type="button" data-toggle='modal' data-target='#modal-detalle'
                                                onclick="get_voucher({{ $postulacion->id}})"
                                                class="btn btn-outline bg-info border-info text-info btn-icon rounded-round legitRipple"
                                                title="Revisar Voucher">
                                            <i class="icon-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_getvoucher" value="{{route('vouchers.getxpostulacion',[''])}}/">
@endsection

@section('modals')
    <div id="modal-detalle" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <form action="{{route('postulante.actualizarestado')}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalle de Postulante</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-cash mr-2"></i> Detalle de Pago
                                </legend>

                                <div class="form-group">
                                    <label>Postulante:</label>
                                    <span id="postulante"></span>
                                </div>
                                <div class="form-group">
                                    <label>DNI:</label>
                                    <span id="dni" name="dni"></span>
                                </div>
                                <div class="form-group">
                                    <label>E-mail:</label>
                                    <span id="email"></span>
                                </div>
                                <div class="form-group">
                                    <label>Fecha:</label>
                                     <span id="fecha"></span>
                                </div>
                                <div class="form-group">
                                    <label>Observaciones: </label>
                                    <br>
                                    <textarea  rows="5" cols="40" id="observaciones" name="observaciones">

                                    </textarea>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Boleta: </label>
                                    <div class="col-lg-10">
                                        <input class="" type="file" name="boletaimg" required>
                                    </div>
                                </div>

                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-image2 mr-2"></i> Imagen del
                                    Voucher
                                </legend>
                                <div class="card">
                                    <div class="card-img-actions m-1">
                                        <iframe style="width: 100%; height: 300px" id="img_voucher" frameborder="0"></iframe>
                                    </div>
                                </div>
{{--                                
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="postulacion_id"name="postulacion_id">
                    <input type="hidden" id="postulante_id" name="postulante_id">
                    <input type="hidden" id="dni_val" name="dni_val">
                    <input type="hidden" id="estado" name="estado" value="Por-Confirmar">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary legitRipple" >Enviar</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <input type="hidden" id="url_actualizar_postulante" value="{{route('postulante.actualizarestado')}}">
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_licencias').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function get_voucher(id) {
            var url = $('#url_getvoucher').val() + id
            // console.log(url)
            $.ajax({
                 url: url,
                 type: 'GET',
                 dataType: 'json',
                 success: function (data) {
                     //console.log(data)
                     $('#postulante').text(data.voucher.postulante.persona.nombres+" "+data.voucher.postulante.persona.paterno+" "+data.voucher.postulante.persona.materno)
                     $('#dni').text(data.voucher.postulante.persona.DNI)
                     $('#email').text(data.voucher.postulante.persona.email_personal)
                     $('#fecha').text(data.voucher.fecha_pos)
                     $('#postulacion_id').val(data.anexo.postulacion_id)
                     $('#postulante_id').val(data.voucher.postulante.id)
                     $('#observaciones').val(data.voucher.postulante.observaciones)
                     $('#dni_val').val(data.voucher.postulante.persona.DNI)
                     // $('#img_comprobante').attr('src',data.urlimg)
                      $('#img_voucher').attr('src',data.anexo.url)
                      $('#a_voucher').attr('href',data.anexo.urlimg)
                      //$('#observacion_pago').text(data.voucher.licencia.observaciones)

                 }
             })
        }

        // function aprobar() {

        //     var url = $('#url_actualizar_postulante').val()
        //     var id = $('#postulacion_id').val()
        //     var pos_id = $('#postulante_id').val()
        //     var obs = $('#observaciones').val()
        //     var dni = $('#dni').text()
        //     var boletaimg = $('#boletaimg').
        //     var data = {'id': id, 'dni':dni, 'estado': 'Por-Confirmar', 'observaciones': obs, 'pos_id' : pos_id, 'boletaimg' : boletaimg}
        //     console.log(data)
        //     $.ajax({
        //         url: url,
        //         type: 'GET',
        //         data: data,
        //         dataType: 'json',
        //         success: function (data) {
        //             alert('aprobado')
        //             location.reload()
        //         }
        //     })
        // }

    </script>
@endsection
