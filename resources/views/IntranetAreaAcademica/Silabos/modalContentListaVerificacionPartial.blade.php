<div class="modal-header">
    <h5 class="modal-title" id="titulo-modal-contenidos">LISTA DE VERIFICACION</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

    <form action="{{route('IntranetAreaAcademica.lista_verificacion.update')}}" method="POST">
        @csrf
        <input type="hidden" name="silabo_id" value="{{$silabo->id}}">
        <div class="modal-body">

            @foreach ($silabo->lista_verificacion as $indicador)

                <div class="form-group row">
                    <label class="col-form-label col-sm-5">{{$indicador->descripcion}}</label>
                    <div class="col-sm-2">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" name="indicador_{{$indicador->id}}" value="si" class="form-check-input-styled-primary" {{$indicador->pivot->escala==1?'checked':''}} data-fouc>
                                SI
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" name="indicador_{{$indicador->id}}" value="no" class="form-check-input-styled-primary" {{$indicador->pivot->escala==0?'checked':''}} data-fouc>
                                NO
                            </label>
                        </div>

                    </div>
                    <div class="col-md-5">
                        <textarea id="detalles_{{$indicador->id}}" name="detalles_{{$indicador->id}}" rows="3" cols="5" class="form-control" placeholder="">{{$indicador->pivot->detalles}}</textarea>
                    </div>
                </div>
                <br>
            @endforeach

        </div>

        <div class="modal-footer">
            @if($silabo->estado==3)
            <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn bg-primary">Guardar cambios</button>
            @endif
        </div>

    </form>
