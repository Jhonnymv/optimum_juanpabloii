@extends('Layouts.main_intranet_area_academica')
@section('title','Area Academica - Silabos')


@section('header_title')




@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

	{{-- <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
		data-target="#modalCrear">
		<i class="icon-calendar5 text-pink-300"></i>
		<span>Añadir Nuevo Silabo</span>
	</a> --}}
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Intranet Area Academica</span>
<span class="breadcrumb-item active">Silabos</span>
@endsection

@section('header_subbuttons')

@endsection

@section('style')
<style>
    .select2-selection__rendered ,.select2-selection__arrow{
        color: white
    }
</style>

@endsection

@section('content')




	<div class="row justify-content-center">
		<div class="col-md-12">
			<!-- Basic table -->
			<div class="card justify-content-center text-center" id="SilaboIndexCard">

				{{-- <div class="card-header header-elements-inline">
					<legend id="carrera-label" class="text-uppercase font-size-sm font-weight-bold">

					</legend>

                </div> --}}

                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Silabos</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            {{-- <a class="list-icons-item" data-action="collapse"></a> --}}
                            {{-- <a class="list-icons-item" data-action="reload"></a> --}}
                        </div>
                    </div>
                </div>

				<div class="card-body">

                    <form id="form-conulta" action="/IntranetAreaAcademica/SeccionesRespAreaPorCarrera" method="POST">
                        @csrf
					<div class="row">


                            <label class="col-lg-1 col-form-label">Periodo:</label>
                            <div class="col-lg-2">
                                <select id="periodo_id" name="periodo_id" data-placeholder="Seleccionar un periodo" class="form-control form-control-select2" data-fouc="" required>
                                    @foreach ($periodos as $periodo)
                                    <option value="{{$periodo->id}}">
                                        {{$periodo->descripcion}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-lg-1 col-form-label">Carrera:</label>
                            <div class="col-lg-4">
                                <select id="carrera_id" name="carrera_id" data-placeholder="Seleccionar una carrera" class="form-control form-control-select2" data-fouc="" required>
                                    @foreach ($carreras as $carrera)
                                    <option value="{{$carrera->id}}">
                                        {{$carrera->nombre}}
                                    </option>
                                    @endforeach 
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-primary legitRipple"><i class="icon-arrow-right5 mr-2"></i>Consultar</button>
                            </div>

                    </div>

                    </form>

                        <br><br>
                        <div class="row">
                            <div id="silabos-carrera" class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Semestre</th>
                                            <th>Area</th>
                                            <th>Grupo</th>
                                            <th>Docente</th>
                                            <th>Creado</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7">
                                                NO SE HAN ENCONTRADO SILABOS.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

				</div>


			</div>
			<!-- /basic table -->

		</div>
	</div>



@endsection


@section('modals')




@endsection


@section('script')
<script>

document.addEventListener('DOMContentLoaded', function() {


    $('.form-control-select2').select2();

    $('#form-conulta').submit(function(e){
        e.preventDefault();

        // var data={'id':id};
        var form_data = $(this).serialize();
        var url = $(this).attr('action');

        // console.log(form_data);

        $("#SilaboIndexCard").block({
            message: '<div class="loader"></div> <p><br />Enviando data, espera un momento!...</p>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

		$.ajax({
            type: "GET",
            url: url,
            data: form_data,
            dataType:'json',
            success: function(data){

                $("#SilaboIndexCard").unblock();
                console.log(data);

                if(data.length>0){

                    $('#silabos-carrera').find('table').find('tbody').html('');

                    data.forEach(element => {

                        var download_icon='';

                        if (element.silabo.estado==2){
                            var estado='<span class="badge badge-warning" style="font-size: 1em">En revisión</span>';
                        }
                        if (element.silabo.estado==3){
                            var estado='<span class="badge badge-success" style="font-size: 1em">Publicado</span>';
                            download_icon= "<a href='"+"{{route('silabos.pdf.download')}}?id="+element.silabo.id+"' title='Descargar silabo'> <i class='icon-file-download'></i> </a> | "
                        }



                        var template = "<tr>"
                                        + "<td>" + element.pe_curso.semestre + "</td>"
                                        + "<td>" + element.pe_curso.curso.nombre + "</td>"
                                        + "<td>" + element.seccion + "</td>"

                                        + "<td style='white-space: pre-wrap;'>" + element.docentes[0].persona.nombres + ' '+ element.docentes[0].persona.paterno + '' + element.docentes[0].persona.materno+ "</td>"
                                        + "<td style='white-space: pre-wrap;'>" + element.created_at.split("T")[0] + "</td>"
                                        + "<td style='white-space: pre-wrap;'>"

                                            + estado

                                        + "</td>"


                                        + "<td>"

                                        +download_icon


                                        +"<a href='/IntranetAreaAcademica/silabos/detalle/"+element.silabo.id+"'>"
                                        +"<i class='icon-file-eye'></i>"
                                        +"</a>"



                                        +"</td>"


                                        + "</tr>";

                        $('#silabos-carrera').find('table').find('tbody').append(template)
                    });

                }else{
                    $('#silabos-carrera').find('table').find('tbody').html('<tr><td colspan="7">NO SE HAN ENCONTRADO SILABOS.</td></tr>');
                }



            }
        });
    })
});


</script>


@endsection
