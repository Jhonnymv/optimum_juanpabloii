@extends('Layouts.main_intranet_area_academica')
@section('title','Area Academica - Silabos')


@section('header_title')



@endsection

@section('header_buttons')
<div class="d-flex justify-content-center">

	{{-- <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
		data-target="#modalCrear">
		<i class="icon-calendar5 text-pink-300"></i>
		<span>Añadir Nuevo Silabo</span>
	</a> --}}
</div>
@endsection

@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Intranet Area Academica</span>
<span class="breadcrumb-item active">Silabos</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')





	<div class="row justify-content-center" style="display: block" id="SilaboPanel1">
		<div class="col-md-12">
			<!-- Basic table -->
			<div class="card">

				<div class="card-header header-elements-inline">
					<legend id="carrera-label" class="text-uppercase font-size-sm font-weight-bold">
						{{$silabo->seccion->pe_curso->curso->carrera->nombre}}
					</legend>

				</div>

				<div class="card-body">

					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Periodo:</label>
								<div class="form-control-plaintext" id="periodo-label">{{$silabo->seccion->periodo->descripcion}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Duración:</label>
								<div class="form-control-plaintext" id="duracion-label">5 semanas</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Inicio:</label>
								<div class="form-control-plaintext" id="inicio-label">{{$silabo->seccion->periodo->fecha_inicio}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Fin:</label>
								<div class="form-control-plaintext" id="fin-label">{{$silabo->seccion->periodo->fecha_fin}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Curso:</label>
								<div class="form-control-plaintext" id="curso-label">{{$silabo->seccion->pe_curso->curso->nombre}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Sección:</label>
								<div class="form-control-plaintext" id="seccion-label">{{$silabo->seccion->seccion}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Horas Teoría:</label>
								<div class="form-control-plaintext" id="hteoria-label">{{$silabo->seccion->pe_curso->horas_teoria}}</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Horas Práctica:</label>
								<div class="form-control-plaintext" id="hpractica-label">{{$silabo->seccion->pe_curso->horas_practica}}</div>
							</div>
                        </div>

                        <div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Semestre:</label>
								<div class="form-control-plaintext" id="semestre-label">{{$silabo->seccion->pe_curso->semestre}}</div>
							</div>
                        </div>

                        <div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Docente:</label>
								<div class="form-control-plaintext" id="semestre-label">{{$silabo->seccion->docentes->first()->persona->fullName()}}</div>
							</div>
                        </div>

                        <div class="col-md-2">
							<div class="form-group">
								<label class="font-weight-semibold">Estado:</label><br>
								@if ($silabo->estado==2)
                                <span class="badge badge-warning" style="font-size: 1em">En revisión</span>
                                @endif
                                @if ($silabo->estado==3)
                                <span class="badge badge-success" style="font-size: 1em">Publicado</span>
                                @endif
							</div>
                        </div>





					</div>


				</div>


			</div>
			<!-- /basic table -->

		</div>
	</div>



	<div class="row" style="display: block" id="SilaboPanel2">
		<div class="col-md-12">
			<div class="card" id="SilaboCard" style="display: block">






					<div class="card-header header-elements-inline">
						<legend class="text-uppercase font-size-sm font-weight-bold">
                            Silabo de la seccion

                            @if ($silabo->estado==2)
                                <a onclick="publicarSilabo({{$silabo->id}})" style="float: right;margin-right: 1%;" class="btn bg-success-400 btn-icon rounded-round legitRipple" title="Publicar silabo"><i class="icon-sphere"></i></a>
                            @endif

                            <button type="button" style="float: right" data-toggle="modal"
							data-target="#modal_lista_verificacion" class="btn btn-info btn-sm rounded-round legitRipple"><i class="icon-check mr-2"></i>Lista de verificación</button>
							<button type="button" style="float: right;margin-right: 1%;"  onclick="vista_previa({{$silabo->id}})" class="btn btn-info ">Vista previa</button>


						</legend>

					</div>


<!--


					<div id="section-silabo" style="display: block" class="card-body">

						<div class="col-md-12">

							<label class="d-block font-weight-semibold">Estructura de silabo</label>

							<input type="hidden" id="tipo" name="tipo" value="{{$silabo->tipo}}">

							<div class="form-check form-check-inline disabled">
								<label class="form-check-label">
                                    <div class="uniform-choice disabled">
                                        <span class="{{$silabo->tipo=='2019'?'checked':''}}">
                                            <input type="radio" name="radio_tipo" value="2019" disabled="" class="form-input-styled" data-fouc>
                                        </span>
                                    </div>
                                    FORMATO ANTIGUO (2010)
								</label>
							</div>

							<div class="form-check form-check-inline disabled">
								<label class="form-check-label">
                                    <div class="uniform-choice disabled">
                                        <span class="{{$silabo->tipo=='2020'?'checked':''}}">
                                            <input type="radio" name="radio_tipo" value="2020" disabled="" class="form-input-styled" data-fouc>
                                        </span>
                                    </div>
                                    FORMATO NUEVO
								</label>
                            </div>

                            {{-- <div class="form-check form-check-inline disabled">
                                <label class="form-check-label">
                                    <div class="uniform-choice disabled">
                                        <span class="checked">
                                            <input type="radio" class="form-input-styled" name="gender" disabled="" checked="" data-fouc="">
                                        </span>
                                    </div>
                                    Male
                                </label>
                            </div> --}}

						</div>

						<br><br>

						<div class="col-md-12">
							<div class="form-group form-group-float">
								<label class="form-group-float-label is-visible" style="font-weight: bold" id="fundamentacion-sumilla-label">Fundamentación</label>
								<textarea readonly id="fundamentacion" name="fundamentacion" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->fundamentacion}}</textarea>
							</div>

							<div class="form-group form-group-float">
								<label class="form-group-float-label is-visible" style="font-weight: bold" id="proyecto-label">Proyecto institucional</label>
								<textarea readonly id="proyecto_institucional" name="proyecto_institucional" rows="4" cols="5" class="form-control" placeholder="">{{$silabo->proyecto_institucional}}</textarea>
							</div>

							<div class="form-group form-group-float" id="temas-transversales-seccion">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Temas transversales</label>
								<textarea readonly id="temas_transversales" name="temas_transversales" rows="4" cols="5" class="form-control" placeholder="">{{$silabo->temas_transversales}}</textarea>
							</div>



							<div class="form-group form-group-float" id="enfoque-derecho-seccion" style="display: none">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Enfoque de derecho</label>
								<div class="row">
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">Enfoque</label>
										<textarea readonly id="enfoque_derecho_1" name="enfoque_derecho_1" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_derecho_1}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">¿Cuándo son observables?</label>
										<textarea readonly id="enfoque_derecho_2" name="enfoque_derecho_2" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_derecho_2}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">¿En qué acciones concretas se observa?</label>
										<textarea readonly id="enfoque_derecho_3" name="enfoque_derecho_3" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_derecho_3}}</textarea>
									</div>
								</div>

							</div>

							<div class="form-group form-group-float" id="enfoque-intercultural-seccion" style="display: none">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Enfoque de intercultural</label>
								<div class="row">
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">Enfoque</label>
										<textarea readonly id="enfoque_intercultural_1" name="enfoque_intercultural_1" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_intercultural_1}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">¿Cuándo son observables?</label>
										<textarea readonly id="enfoque_intercultural_2" name="enfoque_intercultural_2" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_intercultural_2}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">¿En qué acciones concretas se observa?</label>
										<textarea readonly id="enfoque_intercultural_3" name="enfoque_intercultural_3" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->enfoque_intercultural_3}}</textarea>
									</div>
								</div>

							</div>



							<div class="form-group form-group-float" id="competencias-seccion" style="display: none">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Articulación entre competencias del perfil y estándares de la FID</label>

								<div class="row">
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">Competencias</label>
										<textarea readonly id="competencias" name="competencias" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->competencias}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">Capacidades</label>
										<textarea readonly id="capacidades" name="capacidades" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->capacidades}}</textarea>
									</div>
									<div class="col-md-4">
										<label class="form-group-float-label is-visible">Estandar</label>
										<textarea readonly id="estandar" name="estandar" rows="5" cols="5" class="form-control" placeholder="">{{$silabo->estandar}}</textarea>
									</div>
								</div>

							</div>

							<hr>

							<div class="form-group form-group-float">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Organización y evaluación de los aprendizajes</label>



								<div class="row" id="unidades-seccion" style="display: block">


                                    <br>


                                    <ul class="nav nav-tabs nav-tabs-solid border-0">

                                        @foreach ($silabo->unidades as $key => $unidad)
                                        <li class="nav-item">
                                            <a href="#solid-tab{{$key}}" class="nav-link {{$key==0?'active':''}}" data-toggle="tab">Unidad {{$unidad->unidad}}</a>
                                        </li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">

                                        @foreach ($silabo->unidades as $key => $unidad)
                                        <div class="tab-pane fade {{$key==0?'show active':''}}" id="solid-tab{{$key}}">
                                            <div class="row">

                                                {{-- <div class="form-group row"> --}}
                                                    <label class="col-md-1 col-form-label" style="font-weight: bold;">Unidad Nro:</label>
                                                    <div class="col-md-1">
                                                        <div class="form-control-plaintext">{{$unidad->unidad}}</div>
                                                    </div>

                                                    <label class="col-md-2 col-form-label" style="font-weight: bold;">Denominacion:</label>
                                                    <div class="col-md-2">
                                                        <div class="form-control-plaintext">{{$unidad->denominacion}}</div>
                                                    </div>



                                                    <label class="col-md-1 col-form-label" style="font-weight: bold;">Fecha inicio:</label>
                                                    <div class="col-md-1">
                                                        <div class="form-control-plaintext">{{$unidad->fecha_inicio}}</div>
                                                    </div>

                                                    <label class="col-md-1 col-form-label" style="font-weight: bold;">Fecha Fin:</label>
                                                    <div class="col-md-1">
                                                        <div class="form-control-plaintext">{{$unidad->fecha_fin}}</div>
                                                    </div>

                                                    <div class="col-md-1">
                                                    </div>
                                                {{-- </div> --}}



                                            </div>

                                            <br>
                                            <br>

                                            <div class="row">


                                                <div class="col-md-4">
                                                    <div id="desempeno-criterios-seccion-{{$key}}" class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <th>Titulo</th>
                                                                <th></th>
                                                            </thead>
                                                            <tbody>
                                                            @if ($unidad->desempeno_criterios->isNotEmpty())

                                                                @foreach ($unidad->desempeno_criterios as $key_dc => $desempeno_criterio)
                                                                <tr>
                                                                    <td>
                                                                        {{$desempeno_criterio->titulo}}
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0)" onclick="open_modal_desempeno_criterio_edit({{$desempeno_criterio->id}})"> <i class="icon-eye"></i> </a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach


                                                            @else

                                                                <tr>
                                                                    <td colspan="2">
                                                                        <p style="font-size: 0.9em;">AUN NO SE HAN REGISTRADO CRITERIOS DE DESEMPEÑO.</p>
                                                                    </td>
                                                                </tr>

                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="col-md-1">
                                                </div>

                                                <div class="col-md-7">
                                                    <div id="contenidos-seccion-{{$key}}" class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Semana</th>
                                                                    <th>Fecha inicio</th>
                                                                    <th>Fecha fin</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if ($unidad->contenidos->isNotEmpty())

                                                                    @foreach ($unidad->contenidos as $key_c => $semana)
                                                                    <tr>
                                                                        <td>
                                                                            {{$semana->nro_semana}}
                                                                        </td>
                                                                        <td>
                                                                            {{$semana->fecha_inicio}}
                                                                        </td>
                                                                        <td>
                                                                            {{$semana->fecha_fin}}
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)" onclick="open_modal_contenidos({{$semana->id}})"> <i class="icon-eye"></i> </a>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach


                                                                @else

                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <p style="margin-left: 35%;">AUN NO SE HAN REGISTRADO SEMANAS.</p>
                                                                        </td>
                                                                    </tr>

                                                                @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach


                                    </div>


								</div>

							</div>

							<hr>

							<div class="form-group form-group-float" id="metodologias-seccion" style="display: none">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Metodologia</label>
								<textarea readonly id="metodologia" name="metodologia" rows="3" cols="5" class="form-control" placeholder="">{{$silabo->metodologia}}</textarea>
							</div>

							<div class="form-group form-group-float">
								<label class="form-group-float-label is-visible" style="font-weight: bold">Bibliografía</label>
								<textarea readonly id="bibliografia" name="bibliografia" rows="4" cols="5" class="form-control" placeholder="">{{$silabo->bibliografia}}</textarea>
							</div>

						</div>

					</div> -->

			</div>
		</div>

	</div>
    <input type="hidden" id="tipo" name="tipo" value="{{$silabo->tipo}}">
    <input type="hidden" id="url" name="url" value="{{$silabo->url_silabo}}">
@endsection


@section('modals')
    <!-- Large modal -->
    <div id="modal-preview" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo-modal-contenidos"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="pre_view" frameborder="0" height="500em" width="100%"></iframe>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                </div>

                </form>
            </div>
        </div>
    </div>
    <!-- /large modal -->

<!-- Large modal -->
<div id="modal_contenidos" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="titulo-modal-contenidos">SEMANAS</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

				<div class="modal-body">



					<div class="form-group row">
						<label class="col-form-label col-sm-2">Nro semana</label>
						<div class="col-sm-1">
							<input readonly type="number" min="1" required id="nro_semana" name="nro_semana" class="form-control">
						</div>

						<label class="col-form-label col-sm-2">Fecha inicio</label>
						<div class="col-sm-3">
							<input readonly type="date" required id="fecha_inicio_semana" name="fecha_inicio" class="form-control">
						</div>

						<label class="col-form-label col-sm-1">Fecha fin</label>
						<div class="col-sm-3">
							<input readonly type="date" required id="fecha_fin_semana" name="fecha_fin" class="form-control">
						</div>
					</div>


					<div class="form-group row">
						<label class="col-form-label col-sm-3">Indicadores / Desempeño</label>
						<div class="col-sm-9">
							<textarea readonly id="indicadores_desempeno" name="indicadores_desempeno" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Capacidades</label>
						<div class="col-sm-9">
							<textarea readonly id="capacidad_semana" name="capacidades" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Estrategias</label>
						<div class="col-sm-9">
							<textarea readonly id="estrategias" name="estrategias" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Instrumentos</label>
						<div class="col-sm-9">
							<textarea readonly id="instrumentos" name="instrumentos" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Conocimientos</label>
						<div class="col-sm-9">
							<textarea readonly id="conocimientos" name="conocimientos" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Producto o evidencias</label>
						<div class="col-sm-9">
							<textarea readonly id="producto_evidencias" name="producto_evidencias" rows="5" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>
				</div>

		</div>
	</div>
</div>
<!-- /large modal -->


<!-- Large modal -->
<div id="modal_lista_verificacion" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="titulo-modal-contenidos">LISTA DE VERIFICACION</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

				<form action="{{route('IntranetAreaAcademica.lista_verificacion.update')}}" method="POST">
					@csrf
					<input type="hidden" name="silabo_id" value="{{$silabo->id}}">
					<div class="modal-body">

						@foreach ($silabo->lista_verificacion as $indicador)

							<div class="form-group row">
								<label class="col-form-label col-sm-5">{{$indicador->descripcion}}</label>
								<div class="col-sm-2">
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="radio" name="indicador_{{$indicador->id}}" value="si" class="form-check-input-styled-primary" {{$indicador->pivot->escala==1?'checked':''}} data-fouc>
											SI
										</label>
									</div>

									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="radio" name="indicador_{{$indicador->id}}" value="no" class="form-check-input-styled-primary" {{$indicador->pivot->escala==0?'checked':''}} data-fouc>
											NO
										</label>
									</div>

								</div>
								<div class="col-md-5">
									<textarea id="detalles_{{$indicador->id}}" name="detalles_{{$indicador->id}}" rows="3" cols="5" class="form-control" placeholder="">{{$indicador->pivot->detalles}}</textarea>
								</div>
							</div>
							<br>
						@endforeach

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn bg-primary">Guardar cambios</button>
					</div>

				</form>

		</div>
	</div>
</div>
<!-- /large modal -->

<!-- Basic modal -->
<div id="modal_desempeno_criterios" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">

				<div class="modal-header">
					<h5 class="modal-title" id="titulo-modal-depempeno_criterios">CRITERIOS DE DESEMPEÑO</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">
					<input type="hidden" id="desempeno_criterio_id" name="desempeno_criterio_id" value="0">
					<input type="hidden" id="unidades_id_cd" name="unidades_id" value="0">

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Titulo</label>
						<div class="col-sm-9">
							<input readonly type="text" required id="titulo" name="titulo" placeholder="" class="form-control">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-sm-3">Descripción</label>
						<div class="col-sm-9">
							<textarea readonly id="descripcion_desempeno_criterio" required name="descripcion" rows="4" cols="5" class="form-control" placeholder=""></textarea>
						</div>
					</div>

				</div>


		</div>
	</div>
</div>
<!-- /basic modal -->


@endsection


@section('script')
<script>
     function vista_previa(id) {
			// console.log(id)
         if ($('#tipo').val()=== 'archivo') {
             $('#pre_view').prop('src','https://docs.google.com/viewer?url='+window.location.origin+$('#url').val());
         }else {
			$('#pre_view').prop('src', "{{route('IntranetAreaAcademica.silabos.pdf')}}?id=" + id);
         }

            $('#modal-preview').modal('show')
        }
	$('.form-check-input-styled-primary').uniform({
        wrapperClass: 'border-primary text-primary'
	});

	function select_tipo_silabo(){

        var tipo=$('#tipo').val();

        if(tipo=='2020'){
			$('#fundamentacion-sumilla-label').html('Sumilla');
			$('#proyecto-label').html('Vinculación con proyecto integrador');


			$('#temas-transversales-seccion').css('display','none');
			$('#enfoque-derecho-seccion').css('display','block');
			$('#enfoque-intercultural-seccion').css('display','block');
			$('#competencias-seccion').css('display','block');
			$('#metodologias-seccion').css('display','block');


		}

		if(tipo=='2019'){
			$('#fundamentacion-sumilla-label').html('Fundamentación');
			$('#proyecto-label').html('Proyecto institucional');

			$('#temas-transversales-seccion').css('display','block');
			$('#enfoque-derecho-seccion').css('display','none');
			$('#enfoque-intercultural-seccion').css('display','none');
			$('#competencias-seccion').css('display','none');
			$('#metodologias-seccion').css('display','none');
		}

    }

	function open_modal_contenidos(semana_id){

		var data={'semana_id':semana_id};

		$.ajax({
            type: "GET",
            url: '/contenidos/show',
            dataType:'json',
            data    : data,
            success: function(data){
				console.log(data);

				$('#modal_contenidos').modal({
					backdrop: 'static',
					keyboard: false
				});



				// $('.unidades_id').val(data.unidades_id);
				// $('#semana_id').val(data.id);
				$('#nro_semana').val(data.nro_semana);
				$('#fecha_inicio_semana').val(data.fecha_inicio);
				$('#fecha_fin_semana').val(data.fecha_fin);

				$('#indicadores_desempeno').val(data.indicadores_desempeno);
				$('#capacidad_semana').val(data.capacidades);
				$('#estrategias').val(data.estrategias);
				$('#instrumentos').val(data.instrumentos);
				$('#conocimientos').val(data.conocimientos);
				$('#producto_evidencias').val(data.producto_evidencias);

			}
		});

	}

	function publicarSilabo(silabo_id){
		var r= confirm('¿Está seguro de publicar el presente silabo?');

		if(r){
			var data={'silabo_id':silabo_id};

			$.ajax({
				type: "GET",
				url: '/IntranetAreaAcademica/silabos/publicar',
				dataType:'json',
				data    : data,
				success: function(data){
					console.log(data);
					location.reload();
				}
			});
		}
	}



    document.addEventListener('DOMContentLoaded', function() {


        // $('.form-control-select2').select2();
        select_tipo_silabo();

    });


</script>


@endsection
