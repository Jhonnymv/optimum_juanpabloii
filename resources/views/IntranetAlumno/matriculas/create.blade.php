@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pre-Matrícula')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title" style="font-weight: bold">Matrícula</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        @if($matricula??'')
                            <p>Ya te encuentras matrículado en el período actual</p>
{{--                            <button onclick="get_subsanaciones({{$matricula->alumno_carrera_id}})"--}}
{{--                                    type="button" class="btn btn-success btn-sm pull-right pull-right ml-4">Agregar--}}
{{--                                cursos Adicionales--}}
{{--                            </button>--}}
                        @else
                            <label class="col-lg-1 col-form-label" style="font-weight: bold">Nivel:</label>
                            <div class="col-lg-4">
                                <select id="carrera" class="form-control">
                                    <option>Seleccione un Nivel</option>
                                    @foreach ($carreras as $item)
                                        <option value="{{$item->id}}">
                                            {{$item->nombre}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="col-lg-1 col-form-label" style="font-weight: bold">Grado:</label>
                            <div class="col-lg-4">
                                <select id="grado" class="form-control">
                                    <option>Seleccione un Grado</option>
                                    @foreach ($grado as $item)
                                        <option value="{{$item->ciclo}}">
                                            {{$item->ciclo}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="header-elements add_curso fade">
                                <button onclick="get_adicionales()" type="button"
                                        class="btn btn-success btn-sm pull-right"
                                        data-toggle="modal" data-target="#modal-agregar-curso">Agregar Taller Extra-Curricular
                                </button>
                            </div>
                        @endif
                    </div>

                    <ul id="caja_tabs" class="nav nav-tabs nav-tabs-solid border-0">
                        <li class="nav-item" id="tab_pre_matricula"><a href="#pre-matricula" class="nav-link active"
                                                                       data-toggle="tab">Pre-Matrícula</a></li>
                        @if($matricula??'')
                            @if($matricula->estado=='POR-PAGAR')
                                <li id="tab_por_pagar" class="nav-item"><a href="#registrar-pago" class="nav-link"
                                                                           data-toggle="tab">Registrar Pago</a>
                                </li>
                            @endif
                            @if($matricula->estado=='POR-CONFIRMAR')
                                <li id="tab_por_confirmar" class="nav-item"><a href="#confirmar-matricula"
                                                                               class="nav-link"
                                                                               data-toggle="tab">Confirmar Matrícula</a>
                                </li>
                            @endif
                            @if($matricula->estado=='POR-PAGAR-SUBSANACION')
                                <li id="tab_por_pagar_subsanación" class="nav-item"><a href="#voucher_subsanacion"
                                                                                       class="nav-link"
                                                                                       data-toggle="tab">Voucher
                                        Subsanación</a></li>
                            @endif
                        @endif

                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane show active" id="pre-matricula">
                            <form action="{{route('matricula.store')}}" method="post" id="form_store_matricula">
                                @csrf
                                <input type="hidden" name="carrera_id" id="carrera_id">
                                <div class="card">
                                    <div class="table-responsive table-striped">
                                        <table class="table" id="t_cursos">
                                            <thead>
                                            <tr>
                                                <th>Horario</th>
                                                <th>Nombre del curso</th>
                                                <th>Nivel</th>
                                                <th>Grado</th>
{{--                                                <th>Créditos</th>--}}
{{--                                                <th>H. teoría/H. practica</th>--}}
{{--                                                <th></th>--}}
                                            </tr>
                                            </thead>
                                            <tbody id="body_cursos">
                                            @if($matricula??'')
                                                @foreach($matricula->matriculadetalles as $detalle)
                                                    <tr>
                                                        <td>
                                                            @foreach($detalle->seccion->horarios as $horario)
                                                                {{$horario->hora_inicio.' - '.$horario->hora_fin.' '.$horario->dia}}
                                                                <br>
                                                            @endforeach
                                                        </td>
                                                        <td>{{$detalle->seccion->pe_curso->curso->nombre}}
                                                            ({{$detalle->tipo=='R'?'Regular':'Adicional'}})
                                                            <input type="hidden"
                                                                   class="{{$detalle->tipo=='R'?'c_regular':'c_adicional'}}"
                                                                   value="1">
{{--                                                                   value="{{$detalle->seccion->pe_curso->creditos}}">--}}
                                                        </td>
                                                        <td>{{$detalle->seccion->pe_curso->pe_carrera->carrera->nombre}}</td>
                                                        <td>{{$detalle->seccion->pe_curso->semestre}}</td>
{{--                                                        <td>{{$detalle->seccion->pe_curso->creditos}}</td>--}}
{{--                                                        <td>{{$detalle->seccion->pe_curso->horas_teoria.' / '.$detalle->seccion->pe_curso->horas_practica}}</td>--}}
                                                    </tr>
                                                @endforeach
                                            @else
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
{{--                                        @if($matricula??'')--}}
{{--                                            <p>Número de créditos totales registrados: <strong id="to_tcr">0</strong>--}}
{{--                                            </p>--}}
{{--                                        @else--}}
{{--                                            <p>Número de créditos totales registrados: <strong id="tcr">0</strong></p>--}}
{{--                                        @endif--}}
{{--                                        <p>Número de créditos adicionales permitidos: <span id="cap"></span></p>--}}
{{--                                        <p>Número de créditos regulares totales permitidos: <span id="tcrp"></span></p>--}}
{{--                                        @if($matricula??'')--}}
{{--                                            <p>Número de créditos adicionales totales registrados: <strong--}}
{{--                                                    id="to_tcsr">0</strong></p>--}}
{{--                                        @else--}}
{{--                                            <p>Número de créditos adicionales totales registrados: <strong--}}
{{--                                                    id="tcsr"></strong>--}}
{{--                                            </p>--}}
{{--                                        @endif--}}
                                        @if($matricula??'')
                                            <div class="card-footer text-center">
                                                <a style="display: none" target="_blank" id="btn_ficha_matricula"
                                                   class="btn btn-info"><i class="icon-download"></i>
                                                    Ficha de Matrícula
                                                </a>
                                                <a style="display: none" target="_blank" id="btn_horario"
                                                   class="btn btn-info">
                                                    <i class="icon-download"></i>
                                                    Horario
                                                </a>
                                            </div>
                                        @else
                                            <div class="card-footer text-center">
                                                <button type="submit" id="btn_save_matricula" class="btn btn-primary">
                                                    Guardar Datos
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        @if($matricula??'')
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-3">Observaciones: </label>
                                                <div class="col-lg-9">
                                                    <textarea class="form-control" type="text"
                                                              readonly>{{$matricula->observaciones}}</textarea>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="registrar-pago">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Registrar mi pago</h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('voucher.store')}}" method="post"
                                          enctype="multipart/form-data">
                                        @csrf
                                        @if($matricula??'')
                                            <input type="hidden" id="matricula_id" name="matricula_id"
                                                   value="{{$matricula->id}}">
                                        @endif
                                        <fieldset class="mb-3">
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Número de operación: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="num_operacion"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Monto de pago: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="monto" required
                                                           value="{{$matricula->monto??''}}"
                                                           onkeypress="return filterFloat(event,this);">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Fecha de pago: </label>
                                                <div class="col-lg-3">
                                                    <input class="form-control" type="date" name="fecha" min=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Imagen de comprobante: </label>
                                                <div class="col-lg-10">
                                                    <input class="" type="file" name="img" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <textarea id="textarea_observation" class="form-control"
                                                              name="observaciones" cols="30" rows="10"
                                                              placeholder="Observación">{{$matricula->observaciones??''}}</textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="card-footer text-center">
                                            <button type="submit" class="btn btn-primary">Guardar Datos</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="confirmar-matricula">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Cursos</h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            {{--                                                <th><input type="checkbox" name="all_select" id="all_select"></th>--}}
                                            <th>Horario</th>
                                            <th>Nombre del curso</th>
                                            <th>Nivel</th>
{{--                                            <th>Créditos</th>--}}
                                            {{--                                                <th>Estado</th>--}}
                                            {{--                                                <th>Observación</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($matricula??'')
                                            @foreach($matricula->matriculadetalles as $detalle)
                                                <tr>
                                                    <td>
                                                        @foreach($detalle->seccion->horarios as $horario)
                                                            {{$horario->hora_inicio.' - '.$horario->hora_fin.' '.$horario->dia}}
                                                            <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                                                    <td>{{$detalle->seccion->pe_curso->semestre}}</td>
{{--                                                    <td>{{$detalle->seccion->pe_curso->creditos}}</td>--}}
                                                    {{--                                                    <td></td>--}}
                                                    {{--                                                    <td></td>--}}
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <p><strong>Nota: </strong> El estado de la matrícula se encontrará en pendiente de
                                confirmación hasta que el alumno la haya confirmado</p>
                            <p><strong>Resumen de matrícula:</strong></p>
                            <form action="{{route('matricula.confirmar')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if($matricula??'')
                                    <input type="hidden" id="matricula_id_conf" name="matricula_id"
                                           value="{{$matricula->id}}">
                                @endif
                                <fieldset class="mb-3">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-4">Fecha de pago: </label>
                                                <div class="col-lg-8">
                                                    <input class="form-control" type="date" name="fecha" min=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-4">Monto de pago: </label>
                                                <div class="col-lg-8">
                                                    <input class="form-control" type="text" name="monto" required
                                                           value="{{$matricula->monto??''}}"
                                                           onkeypress="return filterFloat(event,this);">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Número de Boleta: </label>
                                        <div class="col-lg-6">
                                            <input class="form-control" type="text" name="num_operacion"
                                                   required
                                                   placeholder="Coloque el número de boleta emitida a su correo corporativo por el area de contabilidad">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Boleta</label>
                                        <div class="col-lg-10">
                                            <div class="uniform-uploader">
                                                <input type="file" name="img" class="form-control-uniform" data-fouc=""
                                                       readonly>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="card-footer text-center">
                                    <button type="submit" class="btn btn-primary">Confirmar
                                        Matrícula
                                    </button>
                                    <button type="button" class="btn btn-warning" data-toggle="modal"
                                            data-target="#modal-observar-matricula">Observar Matrícula
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="voucher_subsanacion">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Registrar mi pago de subsanación</h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('voucher.store')}}" method="post"
                                          enctype="multipart/form-data">
                                        @csrf
                                        @if($matricula??'')
                                            <input type="hidden" id="matricula_id_sub" name="matricula_id"
                                                   value="{{$matricula->id}}">
                                        @endif
                                        <input type="hidden" name="tipo" value="subsanacion">
                                        <fieldset class="mb-3">
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Número de operación: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="num_operacion"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Monto de pago: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="monto" required
                                                           onkeypress="return filterFloat(event,this);">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Fecha de pago: </label>
                                                <div class="col-lg-3">
                                                    <input class="form-control" type="date" name="fecha" min=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Imagen de comprobante: </label>
                                                <div class="col-lg-10">
                                                    <input class="" type="file" name="img" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <textarea class="form-control"
                                                              name="observaciones" cols="30" rows="10"
                                                              placeholder="Observación"></textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="card-footer text-center">
                                            <button type="submit" class="btn btn-primary">Guardar Datos</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="tipo_periodo" value="{{$periodo->tipo??''}}">
    <input type="hidden" id="Url_Get_Curso" value="{{route('pecursos.get_pecurso')}}">
    <input type="hidden" id="Url_Get_adicionales" value="{{route('pecursos.get_adicionales')}}">
    <input type="hidden" id="Url_Get_SeccionesxCurso" value="{{route('seccion.getSecciondexCurso')}}">
    <input type="hidden" id="url_listxpecarrera" value="{{route('pecursos.listxpecarrera',[''])}}/">
    <input type="hidden" id="url_actualizar_matricula" value="{{route('matricula.actualizarestado')}}">
    <input type="hidden" id="url_listpecusrosxcarrera" value="{{route('peCursos.get_curso_subsanacion')}}">
    <input type="hidden" id="Url_Get_Curso" value="{{route('pecursos.get_pecurso')}}">
    <input type="hidden" id="Url_addmatriculadetalle" value="{{route('matriculadetalle.store')}}">
    <input type="hidden" id="Url_rpt_horario" value="{{route('matriculas.rpt_horario')}}/">
    <input type="hidden" id="Url_rpt_ficha_mat" value="{{route('matriculas.ReporteMat')}}/">
@endsection
@section('modals')
    <div id="modal-horario" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title" style="font-weight: bold">Seleccionar Horario</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6 style="font-weight: bold; color: #ffc107">Seleccione el grupo en el que desea registrar su horario, luego presione el botón Seleccionar</h6>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Grupo</th>
                                <th>Día/hora</th>
                                <th>Matriculados en el curso</th>
                                <th>Horario</th>
                            </tr>
                            </thead>
                            <tbody id="tbody_horarios">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-agregar-curso" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title" style="font-weight: bold;">Taller Extra Curriculares</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body py-0">
                    <h6 style="font-weight: bold">Elija el Taller Extra Curricular que desee matricular y luego Agregar </h6>
                    <div class="table-responsive">
                        <table id="t_cursos_adicionales" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nombre del curso</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>Opción</th>
{{--                                <th>H. teoría/H. practica</th>--}}
{{--                                <th>Elegir</th>--}}
                            </tr>
                            </thead>
                            <tbody id="tbody_add_cursos">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-observar-matricula" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Observar Matrícula</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body py-0">
                    <p>Describa detalladamente su observación acerca de su ficha de matrícula envida:</p>
                    <div class="form-group row">
                        <div class="col-12">
                                <textarea id="text_observation" class="form-control" name="" cols="30" rows="10"
                                          placeholder="Observación"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="observar()" class="btn btn-primary">Enviar</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        var tcr = 0;
        var tca = 0;

        function filterFloat(evt, input) {
            // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
            var key = window.Event ? evt.which : evt.keyCode;
            var chark = String.fromCharCode(key);
            var tempValue = input.value + chark;
            if (key >= 48 && key <= 57) {
                if (filter(tempValue) === false) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if (key == 8 || key == 13 || key == 0) {
                    return true;
                } else if (key == 46) {
                    if (filter(tempValue) === false) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }

        function filter(__val__) {
            var preg = /^([0-9]+\.?[0-9]{0,2})$/;
            if (preg.test(__val__) === true) {
                return true;
            } else {
                return false;
            }

        }

        function sumar(id) {
            var creditos = parseFloat($('#creditos' + id).text())
            var creditos_adicionales = parseFloat($('#creditos_ad' + id).text())

            if (creditos) {
                tcr += creditos;
                $('#tcr').text(tcr)
            } else {
                tca += creditos_adicionales;
                $('#tcsr').text(tca)
            }
        }

        function quitar(id) {
            var creditos = parseFloat($('#creditos' + id).text())
            var creditos_adicionales = parseFloat($('#creditos_ad' + id).text())

            if (creditos) {
                tcr -= creditos;
                $('#tcr').text(tcr)
            } else {
                tca -= creditos_adicionales;
                $('#tcsr').text(tca)
            }

            $('#seccion_id' + id).val('').attr('disabled', '')
            $('#semestre' + id).attr('disabled', '')
            $('#cred' + id).attr('disabled', '')
            $('#tipo' + id).attr('disabled', '')
            $('#elegir_horario' + id).removeClass('btn-success').addClass('btn-warning').html('Matricular')
            $('#quitar' + id).attr('hidden', '')
        }

        function get_adicionales() {
            var url = $('#Url_Get_adicionales').val();
            var data = {'carrera_id': $('#carrera').val(), 'semestre': $('#semestre').val()}
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    $('#tbody_add_cursos').html('')
                    var car=data.carrera.nombre
                    data.pe_cursos.forEach(element => {
                        var template = "<tr>"
                            + "<td>" + element.curso.nombre + "</td>"
                            + "<td>" + car+"</td>"
                            + "<td>" + element.semestre + "</td>"
                            // + "<td>" + element.creditos + "</td>"
                            // + "<td>" + element.horas_teoria / element.horas_practica + "</td>"
                            + "<td><button class='btn btn-light' onclick='add_curso_adicional(" + element.id + ")'>Agregar</button></td>"
                            + "</tr>"

                        $('#tbody_add_cursos').append(template)
                    })
                    $('#t_cursos_adicionales').DataTable({
                        "language": {
                            "url": "{{asset('assets/DataTables/Spanish.json')}}"
                        }
                    })
                }
            })
        }

        function add_curso_adicional(id) {
            if ($('#seccion_id' + id).length > 0) {
                alert('ya tienes este curso en la tabla')
            } else {
                var url = $('#Url_Get_Curso').val()
                var data = {'id': id}
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        var car=data.carrera.nombre
                        var template = "<tr>"
                            + "<td><input class='seccion_id' disabled type='hidden' id='seccion_id" + data.pe_curso.id + "' name='seccion_id[]'><button onclick='get_secciones(" + data.pe_curso.id + ")' type='button' class='btn btn-warning legitRipple' id='elegir_horario" + data.pe_curso.id + "'>Matricular</button></td>"
                            + "<td>" + data.pe_curso.curso.nombre + "</td>"
                            + "<td>" + car + "</td>"
                            + "<td><input id='semestre" + data.pe_curso.id + "' type='text' name='ciclo[]' value='" + data.pe_curso.semestre + "' disabled readonly style='text-align: center; width: 50px;border: 0;' ></td>"
                            // + "<td><span id='creditos_ad" + data.pe_curso.id + "'>" + data.pe_curso.creditos + "</span><input id='cred" + data.pe_curso.id + "' disabled readonly type='hidden' name='creditos[]' value='" + data.pe_curso.creditos + "'></td>"
                            // + "<td>" + data.pe_curso.horas_teoria + " / " + data.pe_curso.horas_practica + "</td>"
                            + "<td><input id='tipo" + data.pe_curso.id + "' disabled type='hidden' name='tipo[]' value='S'><button id='quitar" + data.pe_curso.id + "' class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple' onclick='quitar(" + data.pe_curso.id + ")' type='button' hidden><i class='icon-cross2'></i></button></td>"
                            + "</tr>"
                        $('#body_cursos').append(template)
                    }
                })
                $('#modal-agregar-curso').modal('hide')
            }
        }

        function get_subsanaciones(alumno_carrera_id) {
            alert('Tenga en cuenta que al registrar un taller Extra curricular luego, deberá registrar nuevamente su pago seleccinonando la opcion de "PAGO POR TALLER EXTRACURRICULAR"')
            var url = $('#url_listpecusrosxcarrera').val()
            var data = {'alumno_carrera_id': alumno_carrera_id}
            // console.log(data)
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    $('#tbody_add_cursos').html('')
                    data.pe_cursos.forEach(element => {
                        var template = "<tr>"
                            + "<td>" + element.curso.nombre + "</td>"
                            + "<td>" + element.semestre + "</td>"
                            // + "<td><span id='creditos" + element.id + "'>" + element.creditos + "</span></td>"
                            // + "<td>" + element.horas_teoria + " / " + element.horas_practica + "</td>"
                            + "<td><button class='btn btn-light' onclick='add_curso(" + element.id + ")' data-dismiss='modal'><i class='icon-plus-circle2'></i>Agregar</button></td>"
                            + "</tr>"
                        $('#tbody_add_cursos').append(template)
                    })
                    $('#t_cursos_adicionales').DataTable({
                        "language": {
                            "url": "{{asset('assets/DataTables/Spanish.json')}}"
                        }
                    })
                    $('#modal-agregar-curso').modal('show')

                }
            })
        }

        function add_curso(id) {
            var url = $('#Url_Get_Curso').val();
            var data = {'id': id}
            var accion = 1
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    var template = "<tr>"
                        + "<td></td>"
                        + "<td>" + data.pe_curso.curso.nombre + "</td>"
                        + "<td><input id='semestre" + data.pe_curso.id + "' type='text' name='ciclo[]' value='" + data.pe_curso.semestre + "' disabled readonly style='text-align: center; width: 50px;border: 0;' ></td>"
                        // + "<td><span id='creditos_ad" + data.pe_curso.id + "'>" + data.pe_curso.creditos + "</span><input id='cred" + data.pe_curso.id + "' disabled readonly type='hidden' name='creditos[]' value='" + data.pe_curso.creditos + "'></td>"
                        + "<td><input type='text' readonly id='tipo" + data.pe_curso.id + "' class='form-control' value='S'></td>"
                        + "<td><button onclick='get_secciones(" + data.pe_curso.id + "," + accion + ")' type='button' class='btn btn-warning legitRipple' data-toggle='modal' data-target='#modal-horario'>Matricular</button></td>"
                        + "</tr>"
                    $('#body_cursos').append(template)
                }
            })
        }

        function get_secciones(id, accion) {
            var tipo = $('#tipo' + id).val()
            var cred = 0
            if (tipo == 'R')
                cred = parseFloat($('#creditos' + id).text())
            else {
                cred = parseFloat($('#creditos_ad' + id).text())
            }
            if ($('#tipo_periodo').val() === 'R') {
                // if (tca + cred <= 12 || tipo !== 'S') {
                    get_data_secciones(id, accion)
                // // } else {
                //     alert('En un periodo regular no te puedes matricular en más de 3 Talleres Extra Curriculares')
                // }
            } else {
                // if (tca + cred <= 12|| tipo !== 'S') {
                    get_data_secciones(id, accion)
                // } else {
                //     alert('En un periodo vacacional no te puedes matricular en más de 3 Talleres Extra Curriculares')
                // }
            }
        }

        function get_data_secciones(id, accion) {
            // console.log(id)
            $('#tbody_horarios').html('');
            $("#modal-horario").modal("show");
            var url = $('#Url_Get_SeccionesxCurso').val();
            var semestre = $('#semestre' + id).val()

            if (semestre >= '5')
                var data = {
                    'id': id,
                    'tipo': $('#tipo' + id).val(),
                    'carrera_id': $('#carrera').val(),
                    'semestre': semestre
                }
            else
                var data = {
                    'id': id,
                    'tipo': $('#tipo' + id).val(),
                    'carrera_id': $('#carrera').val()
                }
            $.ajax({
                type: 'GET',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    data.secciones.forEach(element => {
                        if (element.horarios.length > 0) {
                            var horarios = "";
                            $.each(element.horarios, function (index, value) {
                                horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                            });
                        } else {
                            var horarios = 'El horario será definido con el docente'
                        }
                        var template = "<tr>"
                            + "<td>" + element.pe_curso.curso.nombre + "</td>"
                            + "<td>" + element.seccion + "</td>"
                            + "<td>" + horarios + "</td>"
                            + "<td class='text-center'>" + element.n_matriculados + "</td>"
                            + "<td><button type='button' class='btn btn-light legitRipple' onclick='seleccionar_horario(" + element.id + "," + id + "," + accion + ")' data-dismiss='modal'>Seleccionar</button></td>"
                            + "</tr>"

                        $('#tbody_horarios').append(template)
                    })
                }
            })
        }

        function seleccionar_horario(seccion_id, input_curso_id, accion) {
            if (accion !== 1) {
                $('#elegir_horario' + input_curso_id).removeClass('btn-warning').addClass('btn-success').html('Cambiar Horario')
                $('#quitar' + input_curso_id).removeAttr('hidden')
                sumar(input_curso_id)
                $('#seccion_id' + input_curso_id).val(seccion_id).removeAttr('disabled')
                $('#semestre' + input_curso_id).removeAttr('disabled')
                $('#cred' + input_curso_id).removeAttr('disabled')
                $('#tipo' + input_curso_id).removeAttr('disabled')
            } else {
                var url = $('#Url_addmatriculadetalle').val()
                var data = {
                    'seccion_id': seccion_id,
                    'matricula_id': $('#matricula_id').val(),
                    'tipo': $('#tipo' + input_curso_id).val().toString(),
                    'accion': 1
                }
                // console.log(url)
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        location.reload()
                    }
                })
            }
        }

        $(function () {
            $('#grado').change(function () {
                if ($(this) !== '') {
                    // $('.semestre').addClass('show')
                    $('.add_curso').addClass('show')
                    var id = $('#carrera').val()
                    var url = $('#url_listxpecarrera').val() + id;
                    var data = {'tipo_peticion': 'mis_cursos'}
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            var carrera = data.carrera
                            $('#body_cursos').html('')
                            tcr = 0
                            if (data.matricula) {
                                $('#tcr').val(0)
                                data.matricula.matriculadetalles.forEach(element => {

                                    if (element.seccion.horarios.length > 0) {
                                        var horarios = "";
                                        $.each(element.seccion.horarios, function (index, value) {
                                            horarios += value.dia + ' / ' + value.hora_inicio + ' - ' + value.hora_fin + "<br>";
                                        });
                                    } else {
                                        var horarios = 'El horario será definido con el docente'
                                    }
                                    var template = "<tr>"
                                        + "<td>" + horarios + "</td>"
                                        + "<td>" + element.seccion.pe_curso.curso.nombre + " (" + element.tipo + ")" + "</td>"
                                        + "<td>" + carrera+ "</td>"
                                        + "<td>" + element.seccion.pe_curso.semestre + "</td>"
                                        // + "<td><span id='creditos" + element.seccion.pe_curso.id + "'>" + element.seccion.pe_curso.creditos + "</span><input id='cred" + element.seccion.pe_curso.id + "' disabled readonly type='hidden' name='creditos[]' value='" + element.seccion.pe_curso.creditos + "'></td>"
                                        // + "<td>" + element.seccion.pe_curso.horas_teoria + " / " + element.seccion.pe_curso.horas_practica + "</td>"
                                        + "<td></td>"
                                        + "</tr>"
                                    $('#body_cursos').append(template)
                                })
                                $('#matricula_id').val(data.matricula.id)
                                $('#matricula_id_conf').val(data.matricula.id)
                                $('#matricula_id_sub').val(data.matricula.id)
                                $('#btn_save_matricula').hide();
                                $('#caja_tabs').html('')
                                $('#pre-matricula').addClass('show active')
                                switch (data.matricula.estado) {
                                    case "POR-PAGAR":
                                        $('#caja_tabs').html("<li class='nav-item' id='tab_pre_matricula'><a href='#pre-matricula' class='nav-link active' data-toggle='tab'>Pre-Matrícula</a></li><li class='nav-item'><a href='#registrar-pago' class='nav-link' data-toggle='tab'>Registrar Pago</a></li>");
                                        break;
                                    case "POR-CONFIRMAR":
                                        $('#caja_tabs').html("<li class='nav-item' id='tab_pre_matricula'><a href='#pre-matricula' class='nav-link active' data-toggle='tab'>Pre-Matrícula</a></li><li id='tab_por_confirmar' class='nav-item'><a href='#confirmar-matricula' class='nav-link'  data-toggle='tab'>Confirmar Matrícula</a></li>");
                                        break;
                                    case "POR-PAGAR-SUBSANACION":
                                        $('#caja_tabs').html("<li class='nav-item' id='tab_pre_matricula'><a href='#pre-matricula' class='nav-link active' data-toggle='tab'>Pre-Matrícula</a></li><li id='tab_por_pagar_subsanación' class='nav-item'><a href='#voucher_subsanacion' class='nav-link' data-toggle='tab'>Voucher Subsanación</a></li>");
                                        break;
                                    case "CONFIRMADA":
                                        $('#btn_horario').show().prop('href', $('#Url_rpt_horario').val() + data.matricula.id);
                                        $('#btn_ficha_matricula').show().prop('href', $('#Url_rpt_ficha_mat').val() + data.matricula.id);
                                        break;
                                }
                            } else {
                                $('#tcr').val(0)
                                data.pe_cursos.forEach(element => {
                                    var template = "<tr>"
                                        + "<td><input class='seccion_id' disabled type='hidden' id='seccion_id" + element.id + "' name='seccion_id[]'><button onclick='get_secciones(" + element.id + ")' type='button' class='btn btn-warning legitRipple' id='elegir_horario" + element.id + "'>Matricular</button></td>"
                                        + "<td>" + element.curso.nombre + "</td>"
                                        + "<td>" + carrera + "</td>"
                                        + "<td><input id='semestre" + element.id + "' type='text' name='ciclo[]' value='" + element.semestre + "' disabled readonly style='text-align: center; width: 50px;border: 0;' ></td>"
                                        // + "<td><span id='creditos" + element.id + "'>" + element.creditos + "</span><input id='cred" + element.id + "' disabled readonly type='hidden' name='creditos[]' value='" + element.creditos + "'></td>"
                                        // + "<td>" + element.horas_teoria + " / " + element.horas_practica + "</td>"
                                        + "<td><input id='tipo" + element.id + "'  disabled type='hidden' name='tipo[]' value='R'><button id='quitar" + element.id + "' class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple' onclick='quitar(" + element.id + ")' type='button' hidden><i class='icon-cross2'></i></button></td>"
                                        + "</tr>"
                                    $('#body_cursos').append(template)
                                })
                                $('#btn_save_matricula').show();
                            }
                        }
                    })
                    $('#carrera_id').val(id)
                }
            })
        })

        function update_estado(adddata) {
            var url = $('#url_actualizar_matricula').val()
            // console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: adddata,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    location.reload()
                }
            })
        }

        function observar() {
            var observaciones = $('#text_observation').val()
            if (observaciones.length >= 1) {
                var id = $('#matricula_id').val()
                var data = {'id': id, 'estado': 'OBSERVADA', 'observaciones': observaciones}
                update_estado(data)
            } else {
                alert('escriba una observación')
            }
        }

        $('#form_store_matricula').on("submit", function (e) {
            // if (tcr >= 1) {
                $(this).submit();
            // } else {
            //     e.preventDefault();
            //     alert('Matriculate en al menos un curso')
            // }
        })

        // $(document).ready(function () {
        //     if ($('.c_regular')) {
        //         var sum = 0;
        //         $('.c_regular').each(function () {
        //             sum += parseFloat($(this).val());
        //         });
        //         $('#to_tcr').text(sum)
        //     }
        //     if ($('.c_adicional')) {
        //         var sum = 0;
        //         $('.c_adicional').each(function () {
        //             sum += parseFloat($(this).val());
        //         });
        //         $('#to_tcsr').text(sum)
        //     }
        // })

    </script>
@endsection
