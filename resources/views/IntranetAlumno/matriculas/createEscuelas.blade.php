@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pre-Matrícula')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"> Matrícula</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label> Nivel: </label>
                            <select id="carrera_id" class="form-control">
                                <option>Seleccione un Nivel</option>
                                @foreach($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane show active" id="pre-matricula">
                            <div class="card">
                                <h5 class="card-title">Cursos</h5>
                                <div class="table-responsive table-striped">
                                    <table class="table" id="t_cursos">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Curso</th>
                                            <th>Nivel</th>
                                            <th>N° de Matriculados</th>
                                        </tr>
                                        </thead>
                                        <tbody id="body_cursos">
                                        @if($matricula??'')
                                            @php
                                            $count=0;
                                            @endphp
                                            @foreach($matricula as $detalle)
                                                <tr>
                                                    @if($detalle->seccion->grupo !='Taller')
                                                        <td>{{$count++}}</td>
                                                        <td>
                                                            {{$detalle->seccion->pe_curso->curso->nombre}}
                                                        </td>
                                                        <td>{{$detalle->seccion->pe_curso->semestre}}</td>
                                                        <td>{{$detalle->seccion->n_matriculados}}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="url_listxpecarrera" value="{{route('pecursos.listxpecarrera',[''])}}/">
    </div>
@endsection

@section('modals')
@endsection

@section('script')
    <script>
        $(function(){
            $('#carrera_id').change(function () {
                var id = $('#carrera_id').val()
                var url = $('#url_listxpecarrera').val() + id;
                $.ajax({
                    type:'GET',
                    url:url,
                    dataType:'json',
                    data:id,
                    success: function (data){
                        console.log(id)
                    }
                })

            })
        })
    </script>
@endsection
