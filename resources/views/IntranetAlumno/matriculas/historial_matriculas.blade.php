@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pre-Matrícula')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Matrículas</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped" id="My_table">
                        <thead>
                        <tr>
                            <th>Periodo</th>
                            <th>Carrera</th>
                            <th>Fecha</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($matriculas as $item)
                            <tr>
                                <td>{{$item->periodo->descripcion}}</td>
                                <td>{{$item->alumnocarrera->carrera->nombre}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>
                                    <a class="btn btn-info" target="_blank" href="{{route('matriculas.rpt_horario',['matricula_id'=>$item->id])}}">Horario</a>
                                    <a class="btn btn-info" target="_blank" href="{{route('matriculas.ReporteMat',['matricula_id'=>$item->id])}}">Ficha de Matrícula</a>
                                    <a class="btn btn-info" target="_blank" href="{{route('matricula.plan_estudio',['matricula_id'=>$item->id])}}">Plan de estudios</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="Url_rpt_horario" value="{{route('matriculas.rpt_horario')}}/">
    <input type="hidden" id="Url_rpt_ficha_mat" value="{{route('matriculas.ReporteMat')}}/">
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#My_table').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
@endsection
