<!-- Global stylesheets -->
{{--  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
    type="text/css">
<link href="/assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/layout.css" rel="stylesheet" type="text/css">
<link href="/assets/css/components.css" rel="stylesheet" type="text/css">
<link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">    --}}
@yield('style')
<!-- /global stylesheets -->

<style>

    .table {
        border-collapse: collapse !important;
    }

    .table td,
    .table th {
        background-color: #fff !important;
    }

    .table-bordered th,
    .table-bordered td {
        border: 1px solid #eee !important;
    }

    .table-dark {
        color: inherit;
    }

    .table-dark th,
    .table-dark td,
    .table-dark thead th,
    .table-dark tbody + tbody {
        border-color: #ddd;
    }

    .table .thead-dark th {
        color: inherit;
        border-color: #ddd;
    }


    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 1.25rem;
        background-color: transparent;
    }

    .table th,
    .table td {
        padding: 0.75rem 1.25rem;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
    }

    .table tbody + tbody {
        border-top: 2px solid #ddd;
    }

    .table .table {
        background-color: #eeeded;
    }

    .table-sm th,
    .table-sm td {
        padding: 0.625rem 1.25rem;
    }

    .table-bordered {
        border: 1px solid #ddd;
    }

    .table-bordered th,
    .table-bordered td {
        border: 1px solid #ddd;
    }

    .table-bordered thead th,
    .table-bordered thead td {
        border-bottom-width: 2px;
    }

    .table-borderless th,
    .table-borderless td,
    .table-borderless thead th,
    .table-borderless tbody + tbody {
        border: 0;
    }

    .table-striped tbody tr:nth-of-type(odd) {
        background-color: rgba(0, 0, 0, 0.02);
    }

    .table-hover tbody tr:hover {
        background-color: rgba(0, 0, 0, 0.03);
    }

    .table-primary,
    .table-primary > th,
    .table-primary > td {
        background-color: #c1e2fc;
    }


    @media (max-width: 575.98px) {
        .table-responsive-sm {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table-responsive-sm > .table-bordered {
            border: 0;
        }
    }

    @media (max-width: 767.98px) {
        .table-responsive-md {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table-responsive-md > .table-bordered {
            border: 0;
        }
    }

    @media (max-width: 991.98px) {
        .table-responsive-lg {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table-responsive-lg > .table-bordered {
            border: 0;
        }
    }

    @media (max-width: 1199.98px) {
        .table-responsive-xl {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table-responsive-xl > .table-bordered {
            border: 0;
        }
    }

    .table-responsive {
        display: block;
        width: 100%;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }

    .table-responsive > .table-bordered {
        border: 0;
    }

    p {
        margin-bottom: 0px;
        margin-top: 0px;
    }


</style>
<script src="/assets/js/app.js"></script>


<!-- Account settings -->
<div class="card">
    <div class="card-header">
        <h5 class="card-title">Ficha de matrícula</h5>
    </div>
    <img src="{{asset('assets/img/circular/1.png')}}"
         class="img" width="100" height="100" alt="">
    <div class="card-body">
        <form action="#">
            <div class="form-group">

                <div class="row">
                    <div class="col-md-6">
                        <label>Nombres y Apellidos:</label>
                        <label>VALENCIA BRIONES, MARIA FRANCISCA</label>
                    </div>
                    <div class="col-md-6">
                        <label>Ciclo - Sección</label>
                        <label>VI - A</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Carrera:</label>
                        <label>MATEMÁTICA:</label>

                    </div>
                    <div class="col-md-6">
                        <label>Periodo académico</label>
                        <label>2020-II</label>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- /account settings -->

    <table class="table">
        <thead>
        <tr>
            <th>
                Matrícula Regular
            </th>
        </tr>
        <tr>
            <th>#</th>

            <th>Nombre del curso</th>
            <th>Nivel</th>
            <th>Créditos</th>
            <th>Horas</th>
            <th>Estado</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Matemáticas</td>
            <td>1</td>
            <td>8</td>
            <td>5</td>
            <td>Aprobado</td>

        </tr>
        <tr>
            <td>2</td>
            <td>Literatura</td>
            <td>1</td>
            <td>8</td>
            <td>6</td>
            <td>Aprobado</td>

        </tr>
        <tr>
            <td>3</td>
            <td>Física I</td>
            <td>1</td>
            <td>8</td>
            <td>10</td>
            <td>Aprobado</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Totales</td>
            <td>23</td>
            <td>23</td>
            <td></td>

        </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
        <tr>
            <th>
                Subsanaciones:
            </th>
        </tr>
        <tr>
            <th>#</th>

            <th>Nombre del curso</th>
            <th>Nivel</th>
            <th>Créditos</th>
            <th>Horas</th>
            <th>Estado</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Matemáticas</td>
            <td>1</td>
            <td>8</td>
            <td>5</td>
            <td>Aprobado</td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Totales</td>
            <td>23</td>
            <td>23</td>
            <td></td>

        </tr>
        </tbody>
    </table>
