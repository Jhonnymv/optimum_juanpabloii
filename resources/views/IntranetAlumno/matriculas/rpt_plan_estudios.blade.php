<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {background-color: #f2f2f2;}

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">
<table style="margin-top: -20px">
    <tr>
        <td style="text-align: center; width: 15%">
            <img src="{{public_path()}}/assets/img/circular/2.png" style="width: 10%;padding-bottom: -25px" alt="">
{{--            <img src="{{asset('assets/img/circular/2.png')}}" style="width: 50%;padding-bottom: -25px" alt="">--}}
        </td><p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCATIVA PARTICULAR
            “Juan Pablo II”</p>
        <td style="text-align: center; width: 70%">
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE
                ADMISIÓN Y REGISTRO ACADÉMICO</p>
            <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">NIVEL {{strtoupper($alumno->carreras->first()->nombre)}}</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">FICHA DE MATRÍCULA</p>
        </td>

    </tr>
</table>

<span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>

<table>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}
        </td>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            CARRERA
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{$pe_carrera->carrera->nombre}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALUMNO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$alumno->persona->paterno.' '.$alumno->persona->materno.', '.$alumno->persona->nombres}}
        </td>

        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            {{--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCION--}}
            FECHA Y HORA DE CONSULTA:
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{-- :&nbsp;&nbsp;&nbsp;{{$curso->seccion}} --}}
            @php
                use Carbon\Carbon;
                //date_default_timezone_set('Perú')
                //format("F")
                $date = Carbon::now();
                //$date = $date->toDateTimeString('y-m-dTH:M:S.s');
                //$date = Carbon::now()->formatLocalized('%B');
            @endphp
            {{$date}}
        </td>
    </tr>

    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            {{-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOCENTE--}}
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{-- :&nbsp;&nbsp;&nbsp;{{$docente->paterno}} {{$docente->materno}} {{$docente->nombres}}--}}
        </td>
    </tr>

</table>
<span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>
<br>
<div class="row mt-0 font-size-sm">
    <table style="border: #0c0c0c; font-size: 10px">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
            <th>Ciclo</th>
            <th>Curso</th>
            <th>H.t</th>
            <th>H.p</th>
            <th>Creditos</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pe_carrera->pe_curso->sortBy('semestre')  as $cur)
            <tr>
                <td style="text-align: center">{{$cur->semestre}}</td>
                <td>{{$cur->curso->nombre}}</td>
                <td style="text-align: center">{{$cur->horas_teoria}}</td>
                <td style="text-align: center">{{$cur->horas_practica}}</td>
                <td style="text-align: center">{{$cur->creditos}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <br>
</div>
<br>

<div id="footer" class="page-break">
    <div class="page-number"></div>
</div>

</body>
</html>
