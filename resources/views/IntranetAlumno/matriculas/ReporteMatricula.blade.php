@extends('Layouts.main_intranet_alumno')
@section('title','Reporte de Matrícula')


@section('header_title')



@endsection



@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Intranet Alumno</span>
<span class="breadcrumb-item active">Ficha de Matrícula</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')

<label class="col-lg-1 col-form-label">Carrera:</label>
                            <div class="col-lg-4">
                                <select id="carrera" class="form-control">
                                    <option>Seleccione una carrera</option>
                                    @forelse ($carreras as $item)
                                        <option value="{{$item->id}}">
                                            {{$item->nombre}}
                                        </option>
                                    @empty

                                    @endforelse

                                </select>
                            </div>
							<label class="col-lg-1 col-form-label">Periodo Académico:</label>
                            <div class="col-lg-4">
                                <select id="carrera" class="form-control">
                                    <option>Seleccione un Periodo Académico</option>
                                    {{-- @forelse ($periodosAcademicos as $item)
                                        <option value="{{$item->first()->id}}">
                                            {{$item->first()->descripcion}}
                                        </option>
                                    @empty

                                    @endforelse --}}

                                </select>
                            </div>
<table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>

                                                <th>Nombre del curso</th>
                                                <th>Nivel</th>
                                                <th>Créditos</th>
                                                <th>Horas</th>
                                                <th>Estado</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Matemáticas</td>
                                                <td>1</td>
                                                <td>8</td>
                                                <td>5</td>
                                                <td><span class="badge badge-success">Aprobado</span></td>

                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Literatura</td>
                                                <td>1</td>
                                                <td>8</td>
                                                <td>6</td>
                                                <td><span class="badge badge-success">Aprobado</span></td>

                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Física I</td>
                                                <td>1</td>
                                                <td>8</td>
                                                <td>10</td>
                                                <td><span class="badge badge-success">Aprobado</span></td>

                                            </tr>
                                            </tbody>
                                        </table>
									 <a href="{{ route('matriculas.ReporteMat')}}">

                                       <button type="submit" class="btn btn-primary" onclick="{{ route('matriculas.ReporteMat')}}">Imprimir Ficha</button>
										</a>
@endsection
