@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pre-Matrícula')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Matrícula</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="alert alert-warning" role="alert">
                       ¡¡El Periodo de Matrícula ha Concluido!!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
