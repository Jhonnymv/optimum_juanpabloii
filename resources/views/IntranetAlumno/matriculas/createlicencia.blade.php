@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Licencia de Estudios')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Solicitud de Licencia de Estudios</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        @if($matricula??'')

                            @if($licencia->count()>0)
                            <p>Tu solicitud de Licencia está en estado PENDIENTE DE RESOLUCIÓN</p>

                            @endif
                        @else
                            <label class="col-lg-1 col-form-label"></label>
                            <div class="col-lg-4">
                                Para poder solicitar Licencia de Estudios, tu matrícula debería estar en estado "CONFIRMADA"
                            </div>


                        @endif
                    </div>

                    <ul class="nav nav-tabs nav-tabs-solid border-0">
                        <li class="nav-item"><a href="#pre-matricula" class="nav-link active" data-toggle="tab">Matrícula</a>
                        </li>
                       @if($matricula??'')
                            @if($matricula->first()->estado=='CONFIRMADA' && $licencia->count()==0)
                                <li class="nav-item"><a href="#registrar-sol" class="nav-link"
                                                        data-toggle="tab">Registrar Solicitud</a>
                                </li>
                            @endif
                            @if($licencia->count() > 0)
                                @if($licencia->first()->estado=='POR-CONFIRMAR')
                                    <li class="nav-item"><a href="#confirmar-matricula" class="nav-link"
                                                        data-toggle="tab">Confirmar Matrícula</a>
                                    </li>
                                @endif
                            @endif
                        @endif
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane show active" id="pre-matricula">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Cursos</h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            {{--                                                <th><input type="checkbox" name="all_select" id="all_select"></th>--}}
                                            <th>Horario</th>
                                            <th>Nombre del curso</th>
                                            <th>Nivel</th>
                                            <th>Créditos</th>
                                            {{--                                                <th>Estado</th>--}}
                                            {{--                                                <th>Observación</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($matricula??'')
                                            @foreach($matricula->matriculadetalles as $detalle)
                                                <tr>
                                                    <td>
                                                        @foreach($detalle->seccion->horarios as $horario)
                                                            {{$horario->hora_inicio.' - '.$horario->hora_fin.' '.$horario->dia}}
                                                            <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                                                    <td>{{$detalle->seccion->pe_curso->semestre}}</td>
                                                    <td>{{$detalle->seccion->pe_curso->creditos}}</td>
                                                    {{--                                                    <td></td>--}}
                                                    {{--                                                    <td></td>--}}
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    
                        <div class="tab-pane fade" id="registrar-sol">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Registrar mi pago</h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('voucher.store_lic')}}" method="post"
                                          enctype="multipart/form-data">
                                        @csrf
                                        @if($matricula??'')
                                            <input type="hidden" id="matricula_id" name="matricula_id"
                                                   value="{{$matricula->id}}">
                                        @endif
                                        <fieldset class="mb-3">
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Número de operación: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="num_operacion"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Monto de pago: </label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" type="text" name="monto" readonly
                                                           value="16.00">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Fecha de pago: </label>
                                                <div class="col-lg-3">
                                                    <input class="form-control" type="date" name="fecha" min=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Imagen de comprobante: </label>
                                                <div class="col-lg-10">
                                                    <input class="" type="file" name="img" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                            <select id="sel_duracion" name="sel_duracion" class="form-control">
                                                <option>Seleccionar el número de periodos de duración de la Licencia</option>

                                                    <option value="1">
                                                       1 Periodo
                                                    </option>
                                                    <option value="2">
                                                        2 Periodos
                                                     </option>
                                                     <option value="3">
                                                        3 Periodos
                                                     </option>
                                                     <option value="4">
                                                         4 Periodos
                                                      </option>

                                            </select>
                                        </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-lg-2">Sumilla: </label>
                                                <div class="col-12">
                                                    <textarea id="sumilla" class="form-control" name="sumilla" cols="30" rows="5" placeholder="sumilla"></textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="card-footer text-center">
                                            <button type="submit" class="btn btn-primary">Enviar Solicitud</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                      
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        var tcr = 0;
        var tca = 0;
        var idioma={
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        }



        $(function () {
            $('#carrera').change(function () {
                if ($(this !== '')) {
                    // $('.semestre').addClass('show')
                    $('.add_curso').addClass('show')
                    var id = $('#carrera').val()
                    var url = $('#url_listxpecarrera').val() + id;
                    var data = {'tipo_peticion': 'mis_cursos'}
                    $.ajax({
                        type: 'GET',
                        url: url,
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data)
                            tcr = 0
                            $('#body_cursos').html('')
                            $('#tcr').val(0)
                            data.pe_cursos.forEach(element => {
                                var template = "<tr>"
                                    + "<td><input class='seccion_id' disabled type='hidden' id='seccion_id" + element.id + "' name='seccion_id[]'><button onclick='get_secciones(" + element.id + ")' type='button' class='btn btn-warning legitRipple' id='elegir_horario" + element.id + "'>Matricular</button></td>"
                                    + "<td>" + element.curso.nombre + "</td>"
                                    + "<td><input id='semestre" + element.id + "' type='text' name='ciclo[]' value='" + element.semestre + "' disabled readonly style='text-align: center; width: 50px;border: 0;' ></td>"
                                    + "<td><span id='creditos" + element.id + "'>" + element.creditos + "</span><input id='cred" + element.id + "' disabled readonly type='hidden' name='creditos[]' value='" + element.creditos + "'></td>"
                                    + "<td>" + element.horas_teoria + " / " + element.horas_practica + "</td>"
                                    + "<td><input id='tipo" + element.id + "'  disabled type='hidden' name='tipo[]' value='R'><button id='quitar" + element.id + "' class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple' onclick='quitar(" + element.id + ")' type='button' hidden><i class='icon-cross2'></i></button></td>"
                                    + "</tr>"
                                $('#body_cursos').append(template)
                            })
                            $('#t_cursos').DataTable({
                                "language": {
                                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                                }
                            })
                        }
                    })
                    $('#carrera_id').val(id)
                }
            })
        })
        $(document).ready(function () {
            $('.solo-numero').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });
        });

        function update_estado(adddata) {
            var url = $('#url_actualizar_matricula').val()
            // console.log(data)
            $.ajax({
                url: url,
                type: 'GET',
                data: adddata,
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    location.reload()
                }
            })
        }

        function observar() {
            var observaciones = $('#text_observation').val()
            if (observaciones.length >= 1) {
                var id = $('#matricula_id').val()
                var data = {'id': id, 'estado': 'OBSERVADA', 'observaciones': observaciones}
                update_estado(data)
            } else {
                alert('escriba una observación')
            }
        }

        // function confirmar() {
        //     var id = $('#matricula_id').val()
        //     var data = {'id': id, 'estado': 'CONFIRMADA'}
        //     update_estado(data)
        // }

        $('#form_store_matricula').on("submit", function (e) {
            if (tcr >= 1) {
                $(this).submit();
            } else {
                e.preventDefault();
                alert('matriculate en al menos un curso')
            }
        })

        $(document).ready(function () {
            if ($('.c_regular')) {
                var sum = 0;
                $('.c_regular').each(function () {
                    sum += parseFloat($(this).val());
                });
                $('#to_tcr').text(sum)
            }
            if ($('.c_adicional')) {
                var sum = 0;
                $('.c_adicional').each(function () {
                    sum += parseFloat($(this).val());
                });
                $('#to_tcsr').text(sum)
            }
        })
    </script>
@endsection
{{--onchange='sum(" + element.id + ")'--}}
{{--<input class='check' type='checkbox' id='check" + element.id + "' >--}}
