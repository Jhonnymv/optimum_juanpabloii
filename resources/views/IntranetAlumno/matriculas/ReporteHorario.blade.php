<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>


        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }
        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }
        .page-number{
            margin-top: 10px;
            font-weight: bold
        }
        .page-number:before {
            content: "" counter(page);
        }


        .contenidos tr{
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td{
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table{
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">

<table style="margin-top: -20px">
    <tr>
        <td style="text-align: center; width: 15%">
{{--            <img src="{{public_path()}}/assets/img/circular/2.png" style="width: 10%;padding-bottom: -25px" alt=""> <img src="{{public_path()}}/assets/img/circular/2.png" style="width: 10%;padding-bottom: -25px" alt="">--}}

            <img src="{{asset('assets/img/circular/1.png')}}" style="width: 50%;padding-bottom: -25px" alt="">
        </td><p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCATIVA PARTICULAR
            “Juan Pablo II”</p>
        <td style="text-align: center; width: 70%">
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE
                ADMISIÓN Y REGISTRO ACADÉMICO</p>
            <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">NIVEL {{strtoupper($alumno->carreras->first()->nombre)}}</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">FICHA DE MATRÍCULA</p>
        </td>

    </tr>
</table>

<span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>

<table>
    <tr>
        <td style="font-weight: bold; text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$matricula->periodo->descripcion}}
        </td>
        <td style="font-weight: bold; text-align: right;font-size: 11px;width: 27%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIVEL:
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{$matricula->alumnocarrera->carrera->nombre}}
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALUMNO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$persona->paterno.' '.$persona->materno.', '.$persona->nombres}}
        </td>

        <td style="font-weight: bold; text-align: left;font-size: 11px;width: 27%; font-family:arial;">
            {{--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCION--}}
            FECHA Y HORA DE CONSULTA:
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{-- :&nbsp;&nbsp;&nbsp;{{$curso->seccion}} --}}
            @php
                use Carbon\Carbon;
                //date_default_timezone_set('Perú')
                //format("F")
                $date = Carbon::now();
                //$date = $date->toDateTimeString('y-m-dTH:M:S.s');
                //$date = Carbon::now()->formatLocalized('%B');
            @endphp
            {{$date}}
        </td>
    </tr>
</table>
<span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>
<br>
<div class="row mt-0 font-size-sm">
    <table class="table table-sm table-striped" id="t_regulares">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
{{--            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial;">Nº</th>--}}
            <th scope="col" style="text-align: center;font-size: 10px;width: 25%; font-family:arial;">CURSO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;">NIVEL</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;">DIA</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;">H. INICIO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;">H. FIN</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 10%; font-family:arial;">TIPO</th>
        </tr>
        </thead>

        <tbody>
        @foreach($matricula->matriculadetalles as $detalle)
            <tr>
                <td>{{$detalle->seccion->pe_curso->curso->nombre}}</td>
                <td style="text-align: center">{{$detalle->seccion->pe_curso->semestre}}</td>
                <td style="text-align: center">
                    @foreach($detalle->seccion->horarios as $horario)
                        <span>{{$horario->dia}}</span><br>
                    @endforeach
                </td >
                <td style="text-align: center">@foreach($detalle->seccion->horarios as $horario)
                        <span>{{$horario->hora_inicio}}</span><br>
                    @endforeach
                </td>
                <td style="text-align: center">
                    @foreach($detalle->seccion->horarios as $horario)
                        <span>{{$horario->hora_fin}}</span><br>
                    @endforeach
                </td>
                <td style="text-align: center">
                    {{$detalle->tipo=='R'?'Regular':'Taller'}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <br>
</div>
<br>





<div id="footer" class="page-break">
    <div  class="page-number"></div>
</div>






</body>
</html>
