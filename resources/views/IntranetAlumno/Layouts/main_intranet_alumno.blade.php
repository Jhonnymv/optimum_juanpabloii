<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Optimum Lab - @yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet"
          type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets/global_assets/js/main/jquery.min.js"></script>
    <script src="/assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/global_assets/js/plugins/ui/perfect_scrollbar.min.js"></script>
    <script src="/assets/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/assets/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/assets/js/app.js"></script>
    @yield('style')

    <style>
        .foto a {
            display: inline-block;
            position: relative;
        }

        .foto a label {
            position: absolute;
            text-align: center;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            display: none;
        }

        .foto a:hover label {
            color: white;
            display: block;
            cursor: pointer;
        }

        .foto a:hover img {
            filter: brightness(0.5);
        }
    </style>

    <script>
        var FixedSidebarCustomScroll = function () {


            //
            // Setup module components
            //

            // Perfect scrollbar
            var _componentPerfectScrollbar = function () {
                if (typeof PerfectScrollbar == 'undefined') {
                    console.warn('Warning - perfect_scrollbar.min.js is not loaded.');
                    return;
                }

                // Initialize
                var ps = new PerfectScrollbar('.sidebar-fixed .sidebar-content', {
                    wheelSpeed: 2,
                    wheelPropagation: true
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function () {
                    _componentPerfectScrollbar();
                }
            }
        }();

        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function () {
            FixedSidebarCustomScroll.init();
        });
    </script>
    <!-- /theme JS files -->

</head>

<body class="navbar-top">

<div style="position: fixed;
bottom: 0;
right: 0;
width: 80px;
display: block;
height: 160px;
font-size: 10px;
z-index: 1000;
margin-right: 50px;text-align:center;color:#fff">
    <img src="/assets/img/circular/2.png" alt="" style="width: 125%;">
    <br>
</div>
<!-- Main navbar -->

<div class="navbar navbar-expand-md navbar-dark bg-slate-800 fixed-top">
    <div class="navbar-brand">
        <a href="{{url('/')}}" class="d-inline-block">
            <img src="/assets/img/optimum-logo-blanco.svg" alt="" style="width:100%">
        </a>
    </div>
    @php
        $personUsuario=Auth::user()->persona;
    @endphp
    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="navbar-text ml-md-3">
                <span class="badge badge-mark border-orange-300 mr-2"></span>
                Bienvenido/a {{Auth::user()->fullName()}}
            </span>

        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item" style="padding-top: 15px">
                Intranet Alumno
            </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                   class="navbar-nav-link">
                    <i class="icon-switch2"></i>
                    <span class="d-md-none ml-2">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->
        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user-material">
                <div class="sidebar-user-material-body">
                    <div class="card-body text-center">
                        @if (Auth::user()->persona->urlimg)
                            <div class="foto">
                                <a href="{{route('indexPerfil')}}">
                                    <img src="{{asset(Auth::user()->persona->urlimg)}}"
                                         class="img-fluid rounded-circle shadow-1 mb-3"
                                         style="width: 80px; height: 80px"
                                         width="80" height="80" alt="">
                                    <label style="font-weight: bold; font-size: smaller">Mi Perfil</label>
                                </a>
                            </div>
                            <a href="{{route('indexPerfil')}}">
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @else
                            <a href="{{route('indexPerfil')}}">
                                {{--                                <img src="{{asset('assets/img/optimum-logo.svg')}}"--}}
                                {{--                                     class="img-fluid rounded-circle shadow-1 mb-3" style="width: 80px; height: 80px"--}}
                                {{--                                     width="80" height="80" alt="">--}}
                                <h6 class="mb-0 text-black-100 text-shadow-dark">{{Auth::user()->fullName()}}</h6>
                            </a>
                        @endif
                    </div>

                    <div class="sidebar-user-material-footer">
                        <a href="#user-nav"
                           class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                           data-toggle="collapse"><span>Mi cuenta</span></a>
                    </div>
                </div>

                <div class="collapse" id="user-nav">
                    <ul class="nav nav-sidebar">
                        <li class="nav-item">
                            <a href="{{route('indexPerfil')}}" class="nav-link">
                                <i class="icon-user-plus"></i>
                                <span>Mi perfil</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-comment-discussion"></i>
                                <span>Mensajes</span>
                                <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">
                                <i class="icon-key"></i>
                                <span>Cambiar de Módulo</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i>
                                <span>Cerrar Sesión</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /user menu -->

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">


                    <!-- Layout -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Intranet</div>
                        <i class="icon-menu"
                           title="Layout options"></i>
                    </li>
                    {{--                    <li class="nav-item nav-item-submenu">--}}
                    {{--                        <a href="#" class="nav-link"><i class="fas fa-clipboard-list"></i> <span>Información Personal</span></a>--}}
                    {{--                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">--}}
                    {{--                            <li class="nav-item"><a href="{{route('mantenimientodatos.index')}}" class="nav-link"><i--}}
                    {{--                                        class=""></i> Editar información personal</a></li>--}}
                    {{--                            <li class="nav-item"><a href="{{route('mantenimientoclave.index')}}" class="nav-link"><i--}}
                    {{--                                        class=""></i> Actualizar Clave</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}

                    <li class="nav-item nav-item-submenu">
                        <a href="{{route('MyCoursesAlumno')}}" class="nav-link"><i class="fa fa-chalkboard-teacher"></i> <span>Aula Virtual</span></a>

                    </li>

{{--                    <li class="nav-item nav-item-submenu">--}}
{{--                        <a href="#" class="nav-link"><i class="fas fa-clipboard-list"></i> <span>LMS</span></a>--}}

{{--                    </li>--}}

                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="fas fa-clipboard-list"></i> <span>Matrícula</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
                            <li class="nav-item"><a href="{{route('matricula_create')}}" class="nav-link"><i
                                        class=""></i> Registrar Matrícula</a></li>
                            <li class="nav-item"><a href="{{route('licencia_create')}}" class="nav-link"><i
                                        class=""></i> Registrar Licencia de Estudios</a></li>
                            <li class="nav-item"><a href="{{route('matriculas.my_historial')}}" class="nav-link"><i
                                        class=""></i>Mis matrículas</a></li>
                            {{--                            <li class="nav-item"><a href="{{route('matriculas.ReporteMat')}}" target="_blank" class="nav-link"><i class=""></i> Ver ficha de--}}
                            {{--                                    matrícula</a></li>--}}
                            {{--                            <li class="nav-item"><a target="_blank" href="{{route('matriculas.rpt_horario')}}" class="nav-link"><i class=""></i> Horario</a></li>--}}
                            <li class="nav-item"><a href="{{route('matriculaEscuela_create')}}" class="nav-link"><i
                                        class=""></i>Matrícula Escuela</a></li>
                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="fas fa-graduation-cap"></i> <span>Académico</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
{{--                            <li class="nav-item"><a href="{{route('indexAsistenciaAlumno')}}" class="nav-link"><i--}}
{{--                                        class=""></i> Asistencias</a></li>--}}
{{--                            --}}{{--                            <li class="nav-item"><a href="{{route('notas.show_notas_alumno')}}" class="nav-link"><i class=""></i> Notas</a></li>--}}
{{--                            <li class="nav-item"><a href="{{route('nota.getRecorNotas')}}" class="nav-link"><i--}}
{{--                                        class=""></i> Record de Notas</a></li>--}}
                            <li class="nav-item"><a href="{{route('intranetalumno.notas.index')}}" class="nav-link"><i class=""></i>
                                    Notas</a></li>
                            {{--                            <li class="nav-item"><a href="#" class="nav-link"><i class=""></i> Ver Plan de estudios</a>--}}
                            {{--                            </li>--}}
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="fas fa-book"></i><span>Aula Virtual</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
                            <li class="nav-item"><a
                                    href="https://classroom.google.com/a/?authuser={{$personUsuario->email}}"
                                    target="_blank" class="nav-link">Aula virtual</a></li>
                            <li class="nav-item"><a href="#" class="nav-link">Bliblioteca Virtual</a></li>
                            <li class="nav-item"><a
                                    href="https://mail.google.com/mail/a/?authuser={{$personUsuario->email}}"
                                    target="_blank" class="nav-link">Correo electrónico</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i
                                class="fas fa-money-check-alt"></i><span>Informe Económico</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
{{--                            <li class="nav-item"><a href="{{route('vercronogramapago',['alumno_id'=>$personUsuario->alumno->id])}}" class="nav-link"><i class=""></i> Ver Pagos</a></li>--}}
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-collaboration"></i> <span>Trámites</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Menu levels">
                            <li class="nav-item"><a href="{{route('solicitudTramite.create')}}" class="nav-link"><i
                                        class=""></i> Nuevo Trámite</a></li>
                            <li class="nav-item"><a href="{{route('solicitudes.enviadas')}}" class="nav-link"><i
                                        class=""></i> Mis Trámites</a></li>
                        </ul>
                    </li>
                    {{--                    <!-- /layout -->--}}

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    @yield('header_title')
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_buttons')

                </div>
            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        @yield('header_subtitle')
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>

                <div class="header-elements d-none">
                    @yield('header_subbuttons')
                </div>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            @if (session('status'))
                <div class="alert alert-warning">
                    {{ session('status') }}
                </div>
            @endif
            @yield('content')


        </div>
        <!-- /content area -->

    @yield('modals')

    <!-- Footer -->
    @include('Layouts.VistasParciales.footer')
    <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
<script src="{{asset('assets/js/sweetalert2.all.js')}}"></script>
@yield('script')

</html>
