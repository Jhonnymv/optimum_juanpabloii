@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Pagos ')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Cronograma</h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection
@section('header_buttons')
    <div class="d-flex justify-content-center">

    </div>
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cronograma</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Cronograma</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body ">
                    Esta es una pantalla para ver el cronograma de pagos
                    <br>
                    <br>
                    <div class="row justify-content-center">
                        @foreach($cronograma as $item)
                            {{--                            <div class="col-md-4" style="margin: 0px">--}}
                            <div class="card col-md-3" style="margin: 10px ; padding: 0px;padding: 2px ; border-radius: 15px">
                                <div class="card-header justify-content-center flex-row"
                                     style="color: white;background-color: #0a6ebd; border-top-left-radius: 15px; border-top-right-radius: 15px">
                                    <h2>{{$item->concepto->nombre.'  -  '. $item->num_couta}}</h2>
                                </div>
                                <div class="card-body" style="padding:0px; padding: 20px; padding-top: 20px">
                                    <div class="row justify-content-center">
                                        @php
                                            $datenow=\Carbon\Carbon::now()->locale('es');
                                            $date = \Carbon\Carbon::parse($item->fech_vencimiento);
                                        @endphp
                                        <label class="text-uppercase text-center" style="margin: 0px">{{$date->isoFormat('MMMM')}}</label>
                                    </div><div class="row justify-content-center">
                                        <label class="text-uppercase text-center" style="margin: 0px">{{$item->alumno->first()->pivot->estado=='Pendiente'?'Pendiente de Pago':'Pagado'}}</label>
                                    </div>
                                </div>
                                <div class="card-footer justify-content-center flex-row"
                                     style="color: white;background-color: {{$datenow>$date && $item->alumno->first()->pivot->estado==='Pendiente'?'red':($item->alumno->first()->pivot->estado==='Pagado'?'green':'#ffc107')}}; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px">
                                    <div class="row justify-content-center">
                                        <label class="text-uppercase text-center" style="margin: 0px">Fecha de
                                            vencimiento</label>

                                    </div>
                                    <div class="row justify-content-center">
                                        <label class="text-uppercase text-center" style="margin: 0px">{{$date->isoFormat('YYYY/MM/DD')}}</label>
                                    </div><div class="row justify-content-center">
                                        <label class="text-uppercase text-center" style="margin: 0px">{{$datenow->isoFormat('YYYY/MM/DD')}}</label>
                                    </div>
                                </div>
                                {{--                            {{$item->alumno->first()->pago->where('id',$item->alumno->first()->pivot->pago_id)}}--}}
                            </div>
                            {{--                            </div>--}}
                        @endforeach

                    </div>

                </div>

            </div>
            <!-- /basic table -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{(asset('assets/js/moment-with-locales.js'))}}"></script>
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        moment.locale('es')
    </script>
@endsection

