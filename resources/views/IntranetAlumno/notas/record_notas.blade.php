@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Record de Notas')

@section('style')

    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>

    <style>
        thead {
            background: #6c757d;
            color: white;
        }

        .validate {
            text-align: center;
        }
    </style>

@endsection



@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Record de Notas</h5>
                    <a target="_blank" href="{{route('nota.getnota',['action'=>'pdf'])}}"
                       class="btn btn-info">Descargar</a>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="misNotas" border="1">
                            <thead>
                            <tr>
                                <th style="text-align:center; font-size:12px; font-weight:bold">N°</th>
                                <th style="text-align:center; font-size:12px; font-weight:bold">Curso</th>
                                <th style="text-align:center; font-size:12px; font-weight:bold">Semestre</th>
                                <th style="text-align:center; font-size:12px; font-weight:bold">Calificación</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $cont=0;
                            @endphp
                            @foreach($cursos as $curso)
                                <tr>

                                    @foreach($curso->carrera->planes_estudio as $planes)
                                        @if($planes->id == $plan->first()->id)
                                            @php
                                                $cont++;
                                            @endphp
                                            <td>
                                                {{$cont}}
                                            </td>
                                            <td>
                                                {{$curso->nombre }}
                                                <input type="hidden" name="id_curso" id="id_curso"
                                                       value="{{$curso->id}}">
                                            </td>
                                            <td>
                                                @foreach($curso->pe_cursos as $item)
                                                    {{$item->semestre}}
                                                @endforeach
                                                {{--                                                {{$curso->pe_cursos->semestre}}--}}
                                            </td>
                                            @if($curso->id)
                                                @foreach($matricula_detalle as $detalle)
                                                    @if($curso->id==$detalle->seccion->pe_curso->curso->id)

                                                        @php
                                                            $cont=0;$cn=0;
                                                        @endphp
                                                        <input type="hidden" value="{{ $val = 0}}">
                                                        <input type="hidden" value="{{ $nota1 = 0}}">
                                                        @foreach  ($detalle->seccion->productos as $producto)
                                                            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                                                        @php
                                                                            $cont++;
                                                                            $cn++;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @foreach($producto->notas as $nota)
                                                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)

                                                                            <input type="hidden"
                                                                                   value="{{ $val += $nota->nota}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                                <input type="hidden"
                                                                       value="{{$nota1=$val/$cn}}">
                                                                @php
                                                                    $cont=0;
                                                                @endphp
                                                            @endif
                                                        @endforeach

                                                        @php
                                                            $cont=0;$cn2=0;
                                                        @endphp
                                                        <input type="hidden" value="{{ $val2 = 0}}">
                                                        <input type="hidden" value="{{ $nota2 = 0}}">
                                                        @foreach  ($detalle->seccion->productos as $producto)
                                                            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                                                        @php
                                                                            $cont++;
                                                                            $cn2++;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach

                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @foreach($producto->notas as $nota)
                                                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)

                                                                            <input type="hidden"
                                                                                   value="{{ $val2 += $nota->nota}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach

                                                                <input type="hidden"
                                                                       value="{{$nota2=$val2/$cn2}}">
                                                                @php
                                                                    $cont=0;
                                                                @endphp
                                                            @endif
                                                        @endforeach


                                                        @php
                                                            $cont=0;$cn3=0;
                                                        @endphp
                                                        <input type="hidden" value="{{ $val3 = 0}}">
                                                        <input type="hidden" value="{{ $nota3 = 0}}">
                                                        @foreach  ($detalle->seccion->productos as $producto)
                                                            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                                                        @php
                                                                            $cont++;
                                                                            $cn3++;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach

                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @foreach($producto->notas as $nota)
                                                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)

                                                                            <input type="hidden"
                                                                                   value="{{ $val3 += $nota->nota}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach

                                                                <input type="hidden"
                                                                       value="{{$nota3=$val3/$cn3}}">
                                                                @php
                                                                    $cont=0;
                                                                @endphp
                                                            @endif
                                                        @endforeach

                                                        @php
                                                            $cont=0;$cn4=0;
                                                        @endphp
                                                        <input type="hidden" value="{{ $val4 = 0}}">
                                                        <input type="hidden" value="{{ $nota4 = 0}}">
                                                        @foreach  ($detalle->seccion->productos as $producto)
                                                            @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                                                        @php
                                                                            $cont++;
                                                                            $cn4++;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @foreach  ($producto->producto_instrumentos as $p_i)
                                                                    @foreach($producto->notas as $nota)
                                                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)

                                                                            <input type="hidden"
                                                                                   value="{{ $val4 += $nota->nota}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                                <input type="hidden"
                                                                       value="{{$nota4=$val4/$cn4}}">
                                                                @php
                                                                    $cont=0;
                                                                @endphp
                                                            @endif
                                                        @endforeach

                                                        @php
                                                            $notafinal=0;
                                                        @endphp
                                                        <td class="validate">
                                                            {{$notafinal= $nota1*0.25 + $nota2*0.15 + $nota3*0.35 + $nota4*0.25??'0'}}
                                                        </td>

                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    {{--    <script>--}}
    {{--        $(function (){--}}
    {{--            $('.validate').each(function (){--}}
    {{--                var a =parseFloat($(this).text())--}}

    {{--                if (a>=10.5){--}}
    {{--                    $(this).css('color','blue')--}}
    {{--                }else--}}
    {{--                    $(this).css('color','red')--}}
    {{--                $(this).val(a.toFixed(2))--}}
    {{--            })--}}
    {{--        })--}}

    {{--    </script>--}}
@endsection

