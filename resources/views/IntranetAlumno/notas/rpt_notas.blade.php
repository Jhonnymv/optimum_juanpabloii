<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Reporte Notas</title>

    <style>

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            width: 100%;
            border-collapse: collapse;

        }

        thead {
            background: #6c757d;
            color: white;
        }

        .validate {
            text-align: center;
        }
    </style>
    <script src="/assets/global_assets/js/main/jquery.min.js"></script>
</head>
<body style="">
<table>
    <tr>
        <td style="text-align: center; width: 15%">
            <img src="{{asset('assets/img/escudo_raimondi.png')}}" style="width: 50%;padding-bottom: -25px" alt="">
        </td><p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUTO DE EDUCATIVA PARTICULAR
            “ANTONIO RAIMONDI”</p>
        <td style="text-align: center; width: 70%">
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE
                ADMISIÓN Y REGISTRO ACADÉMICO</p>
            <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">NIVEL {{strtoupper($alumno->carreras->first()->nombre)}}</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">FICHA DE MATRÍCULA</p>
        </td>

    </tr>
</table>
<br>

<span style="color: rgb(141, 132, 132); width: 10%">_________________________________________________________________________________________________________________________________</span>

<table style="margin-top: 10px;">
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;font-weight:bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO:
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial; font-weight:bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALUMNO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$alumno->persona->paterno . ' ' .$alumno->persona->materno. ' '. $alumno->persona->nombres  }}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial; font-weight:bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$carrera[0]->nombre}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial; font-weight:bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;@php echo date('d/m/Y H:i:s');
            @endphp
        </td>
    </tr>
</table>
<span style="color: rgb(141, 132, 132); width: 10%">_________________________________________________________________________________________________________________________________</span>


<div class="row">
    <div class="col-md-12" style="margin-left: 10px">
        @foreach($matricula_detalles as $detalle)
            <h3 id="cursoid">{{$detalle->seccion->pe_curso->curso->nombre}}</h3>
            {{--                                    <input type="hidden">--}}
            <table id="misNotas" style="width:100%">
                <thead>
                <tr>
                    @php
                        $cont=1;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 1)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <td colspan="{{$cont}}"
                        style="text-align:center; font-size:12px; font-weight:bold">
                        PRODUCTOS DE PROCESOS 25%
                    </td>

                    @php
                        $cont=1;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 2)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <td colspan="{{$cont}}"
                        style="text-align:center; font-size:12px; font-weight:bold">
                        AUTOEVALUACIÓN Y COEVALUACIÓN 15%
                    </td>

                    @php
                        $cont=1;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 3)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <td colspan="{{$cont}}"
                        style="text-align:center; font-size:12px; font-weight:bold">
                        PRODUCTO FINAL 35%
                    </td>

                    @php
                        $cont=1;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 4)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <td colspan="{{$cont}}"
                        style="text-align:center; font-size:12px; font-weight:bold">
                        PORTAFOLIO 25%
                    </td>
                    <th rowspan="2">Nota Final</th>

                </tr>
                <tr>
                    {{--                                            <th rowspan="2">Tabla Avanzada</th>--}}
                    {{--                                            <th colspan="2">Cabecera Múltiples Columnas</th>--}}
                    @php
                        $cont=0;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                            {{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
                            <th style="font-size: 10px; text-align: center" colspan={{$cont}} >
                                {{$producto->producto}}
                            </th>

                        @endif
                        @php
                            $cont=0;
                        @endphp
                    @endforeach
                    <th style="font-size: 10px; text-align: center" colspan>
                        Nota 1
                    </th>
                    @php
                        $cont=0;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                            {{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
                            <th style="font-size: 10px; text-align: center" colspan={{$cont}} >
                                {{$producto->producto}}
                            </th>

                        @endif
                        @php
                            $cont=0;
                        @endphp
                    @endforeach
                    <th style="font-size: 10px; text-align: center" colspan>
                        Nota 2
                    </th>

                    @php
                        $cont=0;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                            {{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
                            <th style="font-size: 10px; text-align: center" colspan={{$cont}} >
                                {{$producto->producto}}
                            </th>
                        @endif
                        @php
                            $cont=0;
                        @endphp
                    @endforeach
                    <th style="font-size: 10px; text-align: center" colspan>
                        Nota 3
                    </th>

                    @php
                        $cont=0;
                    @endphp
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                    @endphp
                                @endif
                            @endforeach
                            {{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
                            <th style="font-size: 10px; text-align: center" colspan={{$cont}} >
                                {{$producto->producto}}
                            </th>
                        @endif
                        @php
                            $cont=0;
                        @endphp
                    @endforeach
                    <th style="font-size: 10px; text-align: center" colspan>
                        Nota 4
                    </th>

                </tr>
                {{--                                        <tr>--}}
                {{--                                            <th>Primera Col. Cab.</th>--}}
                {{--                                            <th>Segunda Col. Cab.</th>--}}
                {{--                                        </tr>--}}
                </thead>
                <tbody>


                <tr>
                    @php
                        $cont=0;$cn=0;
                    @endphp
                    <input type="hidden" value="{{ $val = 0}}">
                    <input type="hidden" value="{{ $nota1 = 0}}">
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                        $cn++;
                                    @endphp
                                @endif
                            @endforeach
                            <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >

                                @foreach  ($producto->producto_instrumentos as $p_i)
                                    @foreach($producto->notas as $nota)
                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                            {{$nota->nota}}
                                            <input type="hidden"
                                                   value="{{ $val += $nota->nota}}">
                                        @endif
                                    @endforeach
                                @endforeach
                            </th>
                            <input type="hidden"
                                   value="{{$nota1=$val/$cn}}">
                            @php
                                $cont=0;
                            @endphp
                        @endif
                    @endforeach
                    <th class="validate">
                        {{$nota1}}
                    </th>


                    @php
                        $cont=0;$cn2=0;
                    @endphp
                    <input type="hidden" value="{{ $val2 = 0}}">
                    <input type="hidden" value="{{ $nota2 = 0}}">
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                        $cn2++;
                                    @endphp
                                @endif
                            @endforeach
                            <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >

                                @foreach  ($producto->producto_instrumentos as $p_i)
                                    @foreach($producto->notas as $nota)
                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                            {{$nota->nota}}
                                            <input type="hidden"
                                                   value="{{ $val2 += $nota->nota}}">
                                        @endif
                                    @endforeach
                                @endforeach
                            </th>
                            <input type="hidden"
                                   value="{{$nota2=$val2/$cn2}}">
                            @php
                                $cont=0;
                            @endphp
                        @endif
                    @endforeach
                    <th class="validate">
                        {{$nota2}}
                    </th>


                    @php
                        $cont=0;$cn3=0;
                    @endphp
                    <input type="hidden" value="{{ $val3 = 0}}">
                    <input type="hidden" value="{{ $nota3 = 0}}">
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                        $cn3++;
                                    @endphp
                                @endif
                            @endforeach
                            <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >

                                @foreach  ($producto->producto_instrumentos as $p_i)
                                    @foreach($producto->notas as $nota)
                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                            {{$nota->nota}}
                                            <input type="hidden"
                                                   value="{{ $val3 += $nota->nota}}">
                                        @endif
                                    @endforeach
                                @endforeach
                            </th>
                            <input type="hidden"
                                   value="{{$nota3=$val3/$cn3}}">
                            @php
                                $cont=0;
                            @endphp
                        @endif
                    @endforeach
                    <th class="validate">
                        {{$nota3}}
                    </th>


                    @php
                        $cont=0;$cn4=0;
                    @endphp
                    <input type="hidden" value="{{ $val4 = 0}}">
                    <input type="hidden" value="{{ $nota4 = 0}}">
                    @foreach  ($detalle->seccion->productos as $producto)
                        @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)
                            @foreach  ($producto->producto_instrumentos as $p_i)
                                @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)
                                    @php
                                        $cont++;
                                        $cn4++;
                                    @endphp
                                @endif
                            @endforeach
                            <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >

                                @foreach  ($producto->producto_instrumentos as $p_i)
                                    @foreach($producto->notas as $nota)
                                        @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                            {{$nota->nota}}
                                            <input type="hidden"
                                                   value="{{ $val4 += $nota->nota}}">
                                        @endif
                                    @endforeach
                                @endforeach
                            </th>
                            <input type="hidden"
                                   value="{{$nota4=$val4/$cn4}}">
                            @php
                                $cont=0;
                            @endphp
                        @endif
                    @endforeach
                    <th class="validate">
                        {{$nota4}}
                    </th>
                    @php
                        $notafinal=0;
                    @endphp
                    <th class="validate">
                        {{$notafinal= $nota1*0.25 + $nota2*0.15 + $nota3*0.35 + $nota4*0.25}}
                    </th>

                </tr>
                </tbody>
            </table>
        @endforeach
    </div>
</div>
<script>
    // $(document).ready(function (){
    //     document.getElementsByClassName('validate').each(function (){
    //         var a =parseFloat($(this).text())
    //         if (a>=10.5){
    //             $(this).css('color','blue')
    //         }else
    //             $(this).css('color','red')
    //         $(this).val(a.toFixed(2))
    //     })
    // })
    $(function (){
        $('.validate').each(function (){
            var a =parseFloat($(this).text())
            if (a>=10.5){
                $(this).css('color','blue')
            }else
                $(this).css('color','red')
            $(this).val(a.toFixed(2))
        })
    })

</script>
</body>
</html>


