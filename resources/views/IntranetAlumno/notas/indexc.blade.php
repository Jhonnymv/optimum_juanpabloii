@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Notas del CredencialesAlumno')

@section('style')

    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_inputs.js')}}"></script>

    <style>
        thead {
            background: #6c757d;
            color: white;
        }
        .validate {
            text-align: center;
        }
    </style>

@endsection



@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-left: 10px">
            <div class="card">
                <div class="tab-content">
                    <div class="tab-pane show active" id="pre-matricula">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Registro de notas de Áreas Curriculares por bimestre</h5>
                                <hr>
                                Selecciones un bimestre para visualizar sus notas:
                                <select name="bimestre" id="bimestre">
                                    <option value="0">Selecciones un bimestre</option>
                                    <option value="1" selected>Primer Bimestre</option>
                                    <option value="2">Segundo Bimestre</option>
                                    <option value="3">Tercer Bimestre</option>
                                    <option value="4">Cuarto Bimestre</option>
                                </select>
{{--                                <a target="_blank" href="{{route('nota.getnota',['action'=>'pdf'])}}" class="btn btn-info">Descargar</a>--}}
                            </div>
                            <div id="reporte" class="table-responsive" >
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Matemática</h3></td></tr>
                                    <tr>
                                        <td>
                                <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @if($instrumentos)
                                @foreach($instrumentos as $instrumento)

                                    @if($instrumento->description == '1')
{{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                                    <tr>
                                                        <td>
{{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                        @foreach($matricula_detalles as $detalle)
                                            @foreach($detalle->seccion->productos as $producto)
{{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                @if($producto->instrumento_id==$instrumento->id)
                                                    <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
{{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                    @foreach($producto->notas as $nota)
                                                        @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                        @endif
                                                    @endforeach
                                                @endif

                                            @endforeach
                                        @endforeach
                                                </tr>
{{--                                            </table>--}}
                                    @elseif($instrumento->description == '4')
{{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                                    <tr>
                                                        <td>
                                        @foreach($matricula_detalles as $detalle)
                                            @foreach($detalle->seccion->productos as $producto)
                                                {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                @if($producto->instrumento_id==$instrumento->id)
                                                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
{{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                    @foreach($producto->notas as $nota)
                                                        @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                            @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endforeach
                                                    </tr>
{{--                                                </table>--}}
                                    @elseif($instrumento->description == '5')
                                            <tr>
                                                <td>
                                        @foreach($matricula_detalles as $detalle)
                                            @foreach($detalle->seccion->productos as $producto)
                                                {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
{{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                    @foreach($producto->notas as $nota)
                                                        @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endforeach
                                            </tr>
                                    @elseif($instrumento->description == '6')
                                            <tr>
                                                <td>
                                        @foreach($matricula_detalles as $detalle)
                                            @foreach($detalle->seccion->productos as $producto)
                                                {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
{{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                    @foreach($producto->notas as $nota)
                                                        @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endforeach
                                            </tr>
                                    @endif
                                @endforeach
                                    @endif

                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Comunicación</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '5')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '6')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '7')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Desarrollo personal, ciudadanía y ética</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '24')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
{{--                                        @elseif($instrumento->id == '25')--}}
{{--                                            --}}{{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
{{--                                            <tr>--}}
{{--                                                <td>--}}
{{--                                                    @foreach($matricula_detalles as $detalle)--}}
{{--                                                        @foreach($detalle->seccion->productos as $producto)--}}
{{--                                                            --}}{{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
{{--                                                            @if($producto->instrumento_id==$instrumento->id)--}}
{{--                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>--}}
{{--                                                --}}{{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
{{--                                                @foreach($producto->notas as $nota)--}}
{{--                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)--}}
{{--                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>--}}
{{--                                                    @endif--}}
{{--                                                @endforeach--}}
{{--                                                @endif--}}
{{--                                                @endforeach--}}
{{--                                                @endforeach--}}
{{--                                            </tr>--}}
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '26')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{($nota->nota)?:$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Educación Física</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '10')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '11')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '12')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{($nota->nota)?:$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Ciencias Sociales</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '21')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '22')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '23')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{($nota->nota)?:$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Ingles</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '18')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '19')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '20')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{($nota->nota)?:$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Educación Religiosa</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '16')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '17')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}

                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Ciencia y tecnología</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '28')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '29')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}
                                        @elseif($instrumento->id == '30')
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td><h3 id="cursoid">{{($nota->nota)?:$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach


                                </table>
                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Arte y cultura</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '13')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}
                                        @elseif($instrumento->id == '14')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3> </td>
                                                    @endif
                                                @endforeach
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                                </table>--}}

                                        @endif
                                    @endforeach


                                </table>

                                <table id="misNotas" border="1" style="width:100%">
                                    <tr><td colspan="3"><h3 id="cursoid">Área Curricular: Educación para el trabajo</h3></td></tr>
                                    <tr>
                                        <td>
                                            <h3 id="cursoid">Competencias</h3>
                                        </td>
                                        <td colspan="2"><h3 id="cursoid">Nota</h3></td>
                                    @foreach($instrumentos as $instrumento)

                                        @if($instrumento->id == '15')
                                            {{--                                                <table id="misNotas2" border="1" style="width:100%">--}}
                                            <tr>
                                                <td>
                                                    {{--                                        <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                    @foreach($matricula_detalles as $detalle)
                                                        @foreach($detalle->seccion->productos as $producto)
                                                            {{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
                                                            @if($producto->instrumento_id==$instrumento->id)
                                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3></td>
                                                {{--                                                    <h3 id="cursoid">--{{$producto->producto_instrumentos->first()->id}}</h3>--}}
                                                @foreach($producto->notas as $nota)
                                                    @if($producto->producto_instrumentos->first()->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)
                                                        <td>   <h3 id="cursoid">{{$nota->nota}}</h3></td>
                                                    @endif
                                                @endforeach
                                                @endif

                                                @endforeach
                                                @endforeach
                                            </tr>
                                            {{--                                            </table>--}}

                                        @endif
                                    @endforeach


                                </table>
{{--                                @foreach($matricula_detalles as $detalle)--}}
{{--                                    @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                        <h3 id="cursoid">{{$producto->instrumento_id}}</h3>--}}
{{--                                    @endforeach--}}
{{--                                @endforeach--}}
{{--                                @foreach($matricula_detalles as $detalle)--}}
{{--                                    <h3 id="cursoid">{{$detalle->seccion->pe_curso->curso->nombre}}-{{$detalle->seccion->pe_curso->creditos}}</h3>--}}
{{--                                        @foreach($instrumentos as $instrumento)--}}
{{--                                            @if($instrumento->description==$detalle->seccion->pe_curso->creditos)--}}
{{--                                                @if($instrumento->description == '1' && $instrumento->description == '4' && $instrumento->description == '5')--}}
{{--                                                <h3 id="cursoid">{{$instrumento->nombre}}</h3>--}}
{{--                                                @endif--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    <input type="hidden">--}}
{{--                                    <table id="misNotas" border="1" style="width:100%">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            @php--}}
{{--                                                $cont=1;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 1)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <td colspan="{{$cont}}"--}}
{{--                                                style="text-align:center; font-size:12px; font-weight:bold">--}}
{{--                                                PRODUCTOS DE PROCESOS 25%--}}
{{--                                            </td>--}}

{{--                                            @php--}}
{{--                                                $cont=1;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 2)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <td colspan="{{$cont}}"--}}
{{--                                                style="text-align:center; font-size:12px; font-weight:bold">--}}
{{--                                                AUTOEVALUACIÓN Y COEVALUACIÓN 15%--}}
{{--                                            </td>--}}

{{--                                            @php--}}
{{--                                                $cont=1;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 3)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <td colspan="{{$cont}}"--}}
{{--                                                style="text-align:center; font-size:12px; font-weight:bold">--}}
{{--                                                PRODUCTO FINAL 35%--}}
{{--                                            </td>--}}

{{--                                            @php--}}
{{--                                                $cont=1;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 &&$producto->evaluacion_categoria_id == 4)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <td colspan="{{$cont}}"--}}
{{--                                                style="text-align:center; font-size:12px; font-weight:bold">--}}
{{--                                                PORTAFOLIO 25%--}}
{{--                                            </td>--}}
{{--                                            <th rowspan="2">Nota Final</th>--}}

{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--                                            <th rowspan="2">Tabla Avanzada</th>--}}
{{--                                            --}}{{--                                            <th colspan="2">Cabecera Múltiples Columnas</th>--}}
{{--                                            @php--}}
{{--                                                $cont=0;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    --}}{{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
{{--                                                    <th style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}
{{--                                                        {{$producto->producto}}--}}
{{--                                                    </th>--}}

{{--                                                @endif--}}
{{--                                                    @php--}}
{{--                                                        $cont=0;--}}
{{--                                                    @endphp--}}
{{--                                            @endforeach--}}
{{--                                            <th style="font-size: 10px; text-align: center" colspan>--}}
{{--                                                Nota 1--}}
{{--                                            </th>--}}
{{--                                            @php--}}
{{--                                                $cont=0;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    --}}{{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
{{--                                                    <th style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}
{{--                                                        {{$producto->producto}}--}}
{{--                                                    </th>--}}

{{--                                                @endif--}}
{{--                                                    @php--}}
{{--                                                        $cont=0;--}}
{{--                                                    @endphp--}}
{{--                                            @endforeach--}}
{{--                                            <th style="font-size: 10px; text-align: center" colspan>--}}
{{--                                                Nota 2--}}
{{--                                            </th>--}}

{{--                                            @php--}}
{{--                                                $cont=0;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    --}}{{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
{{--                                                    <th style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}
{{--                                                        {{$producto->producto}}--}}
{{--                                                    </th>--}}
{{--                                                @endif--}}
{{--                                                    @php--}}
{{--                                                        $cont=0;--}}
{{--                                                    @endphp--}}
{{--                                            @endforeach--}}
{{--                                            <th style="font-size: 10px; text-align: center" colspan>--}}
{{--                                                Nota 3--}}
{{--                                            </th>--}}

{{--                                            @php--}}
{{--                                                $cont=0;--}}
{{--                                            @endphp--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    --}}{{-- @foreach  ($silabo->seccion->productos->producto_instumentos as $p_i) --}}
{{--                                                    <th style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}
{{--                                                        {{$producto->producto}}--}}
{{--                                                    </th>--}}
{{--                                                @endif--}}
{{--                                                    @php--}}
{{--                                                        $cont=0;--}}
{{--                                                    @endphp--}}
{{--                                            @endforeach--}}
{{--                                            <th style="font-size: 10px; text-align: center" colspan>--}}
{{--                                                Nota 4--}}
{{--                                            </th>--}}

{{--                                        </tr>--}}
{{--                                        --}}{{--                                        <tr>--}}
{{--                                        --}}{{--                                            <th>Primera Col. Cab.</th>--}}
{{--                                        --}}{{--                                            <th>Segunda Col. Cab.</th>--}}
{{--                                        --}}{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}


{{--                                        <tr>--}}
{{--                                            @php--}}
{{--                                                $cont=0;$cn=0;--}}
{{--                                            @endphp--}}
{{--                                            <input type="hidden" value="{{ $val = 0}}">--}}
{{--                                            <input type="hidden" value="{{ $nota1 = 0}}">--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 1)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                                $cn++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}

{{--                                                        @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                            @foreach($producto->notas as $nota)--}}
{{--                                                                @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)--}}
{{--                                                                    {{$nota->nota}}--}}
{{--                                                                    <input type="hidden"--}}
{{--                                                                           value="{{ $val += $nota->nota}}">--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        @endforeach--}}
{{--                                                    </th>--}}
{{--                                                    <input type="hidden"--}}
{{--                                                           value="{{$nota1=$val/$cn}}">--}}
{{--                                                        @php--}}
{{--                                                            $cont=0;--}}
{{--                                                        @endphp--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <th class="validate">--}}
{{--                                                {{$nota1}}--}}
{{--                                            </th>--}}


{{--                                            @php--}}
{{--                                                $cont=0;$cn2=0;--}}
{{--                                            @endphp--}}
{{--                                            <input type="hidden" value="{{ $val2 = 0}}">--}}
{{--                                            <input type="hidden" value="{{ $nota2 = 0}}">--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 2)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                                $cn2++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}

{{--                                                        @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                            @foreach($producto->notas as $nota)--}}
{{--                                                                @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)--}}
{{--                                                                    {{$nota->nota}}--}}
{{--                                                                    <input type="hidden"--}}
{{--                                                                           value="{{ $val2 += $nota->nota}}">--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        @endforeach--}}
{{--                                                    </th>--}}
{{--                                                    <input type="hidden"--}}
{{--                                                           value="{{$nota2=$val2/$cn2}}">--}}
{{--                                                        @php--}}
{{--                                                            $cont=0;--}}
{{--                                                        @endphp--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <th class="validate">--}}
{{--                                                {{$nota2}}--}}
{{--                                            </th>--}}


{{--                                            @php--}}
{{--                                                $cont=0;$cn3=0;--}}
{{--                                            @endphp--}}
{{--                                            <input type="hidden" value="{{ $val3 = 0}}">--}}
{{--                                            <input type="hidden" value="{{ $nota3 = 0}}">--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 3)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                                $cn3++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}

{{--                                                        @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                            @foreach($producto->notas as $nota)--}}
{{--                                                                @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)--}}
{{--                                                                    {{$nota->nota}}--}}
{{--                                                                    <input type="hidden"--}}
{{--                                                                           value="{{ $val3 += $nota->nota}}">--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        @endforeach--}}
{{--                                                    </th>--}}
{{--                                                    <input type="hidden"--}}
{{--                                                           value="{{$nota3=$val3/$cn3}}">--}}
{{--                                                        @php--}}
{{--                                                            $cont=0;--}}
{{--                                                        @endphp--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <th class="validate">--}}
{{--                                                {{$nota3}}--}}
{{--                                            </th>--}}


{{--                                            @php--}}
{{--                                                $cont=0;$cn4=0;--}}
{{--                                            @endphp--}}
{{--                                            <input type="hidden" value="{{ $val4 = 0}}">--}}
{{--                                            <input type="hidden" value="{{ $nota4 = 0}}">--}}
{{--                                            @foreach  ($detalle->seccion->productos as $producto)--}}
{{--                                                @if ($producto->estado == 1 && $producto->evaluacion_categoria_id == 4)--}}
{{--                                                    @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                        @if ($p_i->estado ==1 && $p_i->producto_id == $producto->id)--}}
{{--                                                            @php--}}
{{--                                                                $cont++;--}}
{{--                                                                $cn4++;--}}
{{--                                                            @endphp--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                    <th class="validate" style="font-size: 10px; text-align: center" colspan={{$cont}} >--}}

{{--                                                        @foreach  ($producto->producto_instrumentos as $p_i)--}}
{{--                                                            @foreach($producto->notas as $nota)--}}
{{--                                                                @if($p_i->id == $nota->producto_instrumento_id && $nota->alumno_id == Auth::user()->persona->alumno->id)--}}
{{--                                                                    {{$nota->nota}}--}}
{{--                                                                    <input type="hidden"--}}
{{--                                                                           value="{{ $val4 += $nota->nota}}">--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        @endforeach--}}
{{--                                                    </th>--}}
{{--                                                    <input type="hidden"--}}
{{--                                                           value="{{$nota4=$val4/$cn4}}">--}}
{{--                                                        @php--}}
{{--                                                            $cont=0;--}}
{{--                                                        @endphp--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                            <th class="validate">--}}
{{--                                                {{$nota4}}--}}
{{--                                            </th>--}}
{{--                                            @php--}}
{{--                                                $notafinal=0;--}}
{{--                                            @endphp--}}
{{--                                            <th class="validate">--}}
{{--                                                {{$notafinal= $nota1*0.25 + $nota2*0.15 + $nota3*0.35 + $nota4*0.25}}--}}
{{--                                            </th>--}}

{{--                                        </tr>--}}
{{--                                        --}}{{--                                        <tr>--}}
{{--                                        --}}{{--                                            <th>Fila 2</th>--}}
{{--                                        --}}{{--                                            <td>Fila 2 Columna 1</td>--}}
{{--                                        --}}{{--                                            <td>Fila 2 Columna 2</td>--}}
{{--                                        --}}{{--                                        </tr>--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                    <br>--}}
{{--                                    <br>--}}
{{--                                @endforeach--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            $("#reporte").hide();
        });

        $(function (){
            $('.validate').each(function (){
            var a =parseFloat($(this).text())

                if (a>=10.5){
                    $(this).css('color','blue')
                }else
                    $(this).css('color','red')
                $(this).val(a.toFixed(2))
            })
        })
        $('#bimestre').change(function () {
            var bimestre_id = $('#bimestre').val()
        alert("hola"+bimestre_id);
            $('#reporte').show();
        })

    </script>
@endsection

