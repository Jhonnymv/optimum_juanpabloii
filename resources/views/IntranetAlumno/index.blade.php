@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('content')
    <div class="card">
        <div class="card-header"><h6>Deudas</h6></div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>MAT</th>
                        <th>MAR</th>
                        <th>ABR</th>
                        <th>MAY</th>
                        <th>JUN</th>
                        <th>JUL</th>
                        <th>AGO</th>
                        <th>SEP</th>
                        <th>OCT</th>
                        <th>NOV</th>
                        <th>DIC</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$alumnoPago >= 1 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 2 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 3 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 4 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 5 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 6 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 7 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 8 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 9 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 10 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                        <td>{{$alumnoPago >= 11 ? '0' : $alumno->categoriaActiva->last()->monto}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--@endsection--}}
    @if(count($noPago)>0)
        {{--@section('modals')--}}
        <div id="modal_theme_danger" class="modal show " tabindex="-1" style="display: block; padding-right: 17px;"
             aria-modal="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h6 class="modal-title">Deuda</h6>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <div class="modal-body">
                        <h6 class="font-weight-semibold">¡¡Importante!!</h6>
                        <p>Estimado estudiante se le hace de conocimiento que cuenta con deuda pendiente de :</p>
                        <br>
                        <ol>
                            @foreach($noPago as $pago)
                                <li>
                                    <strong>{{$pago->fech_vencimiento->monthName.' - '.$pago->fech_vencimiento->year}}</strong>
                                    por pago de: <strong>{{$pago->concepto->nombre}}</strong>
                                </li>
                            @endforeach
                        </ol>
                        <p>Por tal motivo en los próximos días el acceso a la plataforma, así como a los correos
                            corporativos será restringido</p>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link legitRipple close" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script>
        $(function (){
            $('.close').click(function (){
                $('#modal_theme_danger').hide()
            })
        })
    </script>
@endsection
