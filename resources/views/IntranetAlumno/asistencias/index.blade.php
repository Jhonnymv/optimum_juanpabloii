@extends('IntranetAlumno.Layouts.main_intranet_alumno')
@section('title','Asistencias')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Asistencias</h6>
                </div>

                <div class="card-body">
                    <div class="col-12 col-md-4">
                        <label class="form-control" for="">Curso: </label>
                        <select id="selectCurso" class="form-control" name="cursos" id="">
                            @foreach($detalles as $detalle)
                            <option value="{{$detalle->seccion->id}}">{{$detalle->seccion->pe_curso->curso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <a id="btnReporte" target="_blank"
                       size="10"
                       class="btn btn-success"
                       style="font-size:8pt">
                        <b><i class="icon-copy  mr-1 icon-1x"></i> Reporte</a>
                    <br>
                    <br>
                    <div class="tab-pane fade show active" id="paso1">

                        <div id="sesionesAcademicas" class="table-responsive">
                            <table id="tbSesionClase" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Fecha</th>
                                    <th>Día</th>
                                    <th>Turno</th>
                                    <th>Hora</th>
                                    <th>Asistencia</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="tbodySesionClase">

                                <tr>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="contador" name="contador" value="0">
    <input type="hidden" id="url_seccion" value="{{route('getAsistenciasAlumno', [''])}}/">
    <input type="hidden" id="url_report" value="{{route('reporteAsistenciasAlumno.pdf')}}">
@endsection
@section('script')
    <script>
        $(function () {
            $('#selectCurso').change(function () {
                var seccion = $('#selectCurso').val();
                var url = $('#url_seccion').val() + seccion;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data)
                        $('#tbodySesionClase').html('')
                        $('#contador').val(0)

                        data.registroAsistencia.forEach(element => {
                            var nro = parseInt($('#contador').val()) + 1;
                            $('#contador').val(nro);

                            if (element.asistencia == 1) {
                                var asistencia = "<span class='badge badge-success'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>A</font></font></span>"
                            } else {
                                var asistencia = "<span class='badge badge-danger'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>F</font></font></span>"

                            }

                            var template = "<tr>"
                                + "<td>" + nro + "</td>"
                                + "<td>" + element.fecha_sesion + "</td>"
                                + "<td>" + element.horario.dia + "</td>"
                                + "<td>" + element.horario.turno + "</td>"
                                + "<td>" + element.horario.hora_inicio + " - " + element.horario.hora_fin + "</td>"
                                + "<td>" + asistencia + "</td>"
                                // + "<td>  <input type='radio' name='rbtSesion' id='rbtSesion' onclick='irAsistencia(" + element.horario_id + ", \"" + element.fecha + "\" , \"" + element.dia + "\" , \"" + element.turno + "\" , \"" + element.hora_inicio + "\" , \"" + element.hora_fin + "\" , \"" + element.aula + "\"  )' /></td>"
                                + "</tr>"
                            $('#tbodySesionClase').append(template)
                        })
                    }
                })
            })
        });

        $(function () {
            $('#selectCurso').change(function () {
                var seccion_id = $('#selectCurso').val();
                var url_pdf = $('#url_report').val() + '/' + seccion_id;
                // var url_pdf = $('#url_report').val() + '/' + seccion_id + '/' + horario_id;

                $('#btnReporte').attr('href', url_pdf)
            })
        })
    </script>
@endsection
