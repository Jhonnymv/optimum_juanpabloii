@extends('Layouts.main_intranet_alumno')
@section('title','Silabus')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Silabus
    </h4>
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/admin')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Silabus</span>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Silabus</h5>
                    <div class="header-elements">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-form-label ">Curso:</label>
                        <select id="seccion_id" class="col-2 form-control">
                            @foreach($detalles as $detalle)
                                <option
                                    value="{{$detalle->seccion->id}}">{{$detalle->seccion->pe_curso->curso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    Esta es una pantalla para ver mis silabus <br>

                    <div class="">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function () {
            $('#seccion_id').change(function () {
                console.log($(this))
            })
        });
    </script>
@endsection
