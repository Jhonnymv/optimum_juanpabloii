@extends('Visitas.Layouts.main_visitas')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="formDetalle" method="POST" class="form-group">
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title" style="font-weight: bold">Detalle Postulante</h5>
                        <div class="header-elements-toggle">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row"
                             style="display: flex; flex-direction: row; justify-content: space-between; align-items: center">
                            <div class="col-12 col-md-6">
                                <div class="form-control" style="border-style: none">
                                    <h5 class="font-weight-semibold pt-1">Postulante: </h5>
                                    <h4> {{$postulante->persona->paterno. ' '. $postulante->persona->materno. ', '. $postulante->persona->nombres}}</h4>
                                    <h5 class="font-weight-semibold pt-1">Condición del Postulante:</h5>
                                    <h4 id="condicion"> {{$postulante->condicion}}</h4>
                                    <h4 id="tipoExoneraxion"> {{$postulante->tipo_exoneracion??''}}</h4>
                                </div>
                            </div>
                            <div class="row" style="margin-right: 30px; margin-left: 0">
                                <!-- Mostrar Foto-->
                                <div class="col-12 col-md-4"
                                     style="display: flex; flex-direction: row; align-items: center">
                                    <div class="row" style="margin-right: 30px">
                                        <div style="width: 300px">
                                            <img
                                                src="{{$postulante->postulaciones->last()->postulacion_anexos[1]->url??''}}"
                                                alt="" style="width:45%; height: auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>


                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title" style="font-weight: bold">Datos Personales del Postulante</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="row card-body">
                                <div class="row col-md-12">
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">DNI:</label>
                                        <input type="text" class="form-control"
                                               value="{{$postulante->persona->DNI}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Fecha de Nacimiento::</label>
                                        <input type="date" class="form-control"
                                               value="{{$postulante->persona->fecha_nacimiento}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Email:</label>
                                        <input type="text" class="form-control"
                                               value="{{$postulante->persona->email_personal}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Telefono:</label>
                                        <input type="text" class="form-control"
                                               value="{{$postulante->persona->telefono}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Sexo:</label>
                                        <input type="text" class="form-control"
                                               value="{{($postulante->sexo='M')? 'Masculino': 'Femenino'}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Direción:</label>
                                        <input type="text" class="form-control"
                                               value="{{$postulante->persona->direccion}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Departamento:</label>
                                        <input type="text" class="form-control"
                                               value="{{$departamento[0]->provincia->departamento->nombre}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Provincia:</label>
                                        <input type="text" class="form-control"
                                               value="{{$departamento[0]->provincia->nombre}}" disabled>
                                    </div>
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Distrito:</label>
                                        <input type="text" class="form-control" value="{{$departamento[0]->nombre}}"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DATOS DE CONTACTO-->
                        <div class="card card-collapsed">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title" style="font-weight: bold">Datos Contacto del Postulante</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="row card-body">
                                <div class="col-md-5">
                                    <label style="font-weight: bold; margin: 0">Nombre de la Persona de Contácto</label>
                                    <input type="text" class="form-control" value="{{$postulante->datos_contacto}}"
                                           disabled>
                                </div>

                                <div class="col-md-5">
                                    <label style="font-weight: bold; margin: 0">Teléfono:</label>
                                    <input type="text" class="form-control"
                                           value="{{$postulante->telefono_contacto}}" disabled>
                                </div>
                            </div>
                        </div>
                        <!-- DATOS DE ESTUDIO-->
                        {{--                        <div class="card card-collapsed">--}}
                        <div class="card card-collapsed">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title" style="font-weight: bold">Datos de Estudio</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="row card-body">
                                <div class="col-md-5">
                                    <label style="font-weight: bold; margin: 0">Colegio</label>
                                    <input type="text" class="form-control" value="{{$postulante->colegio}}"
                                           disabled>
                                </div>

                                <div class="col-md-5">
                                    <label style="font-weight: bold; margin: 0">Año de Egreso:</label>
                                    <input type="text" class="form-control" value="{{$postulante->año_culmino}}"
                                           disabled>
                                </div>
                            </div>
                        </div>

                        <!-- DATOS DE LA INSCRIPCION- card-collapsed-->
                        <div class="card  ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title" style="font-weight: bold">Datos de la Inscripción</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="row" >
                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Programa de Estudio</label>
                                        <input type="text" class="form-control"
                                               value="{{$postulante->postulaciones[0]->pe_carrera->carrera->nombre}}"
                                               disabled>
                                    </div>

                                    <div class="col-md-5">
                                        <label style="font-weight: bold; margin: 0">Periodo:</label>
                                        <input type="text" class="form-control" value="{{$periodo->descripcion}}"
                                               disabled>
                                        <br>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-check" style="padding-bottom: 0.5em">
                                            <label class="form-check-label" style="font-weight: bold">
                                                Ver Comprobante de Emitido:
                                            </label>
                                            <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                               data-target="#verComprobanteEmitido" style="margin-left: 2px"><i
                                                    class="icon-file-eye"></i><span></span></a>
                                        </div>
                                        <br>
                                    </div>
                                </div>

                                <div class="row">
                                    @foreach($anexos as $anexo)
                                        @switch($anexo->tipo)
                                            @case('1')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input" id="checkDni">--}}
                                                        Dni:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verDni" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span></a>
                                                </div>

                                            </div>
                                            @break
                                            @case('2')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input" id="checkFoto">--}}
                                                        Foto:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verFoto" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                            @break
                                            @case('3')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input"--}}
{{--                                                               id="checkVoucher">--}}
                                                        Voucher:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verVoucher" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                            </div>
                                            @break
                                            @case('4')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input"--}}
{{--                                                               id="checkCertificado">--}}
                                                        Certificado de Estudios:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verCertificado" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                            @break
                                            @case('5')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input"--}}
{{--                                                               id="checkPartida">--}}
                                                        Partida de Nacimiento:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verPartida" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                            </div>
                                            @break
                                            @case('6')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input"--}}
{{--                                                               id="checkExoneracion">--}}
                                                        Constancia de Exoneracion:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verExoneracion" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                            @break
                                            @case('7')
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <label class="form-check-label" style="font-weight: bold">
{{--                                                        <input type="checkbox" class="form-check-input"--}}
{{--                                                               id="checkSolicitud">--}}
                                                        Solicitud:
                                                    </label>
                                                    <a href="#" class="btn btn-outline-info" data-toggle="modal"
                                                       data-target="#verSolicitud" style="margin-left: 2px"><i
                                                            class="icon-file-eye"></i><span></span>
                                                    </a>
                                                </div>
                                                <br>
                                            </div>
                                            @break
                                        @endswitch
                                    @endforeach
                                </div>
                                <br>
                                <div class="row">
                                    <div class=" col-md-6">
                                        <label style="font-weight: bold; margin-outside: 10px; margin-left: 30px">
                                            Observaciones
                                        </label>
                                        <textarea rows="3" cols="5" class="form-control" name="observaciones" style="margin-left:30px;" required
                                                  id="observaciones"
                                                  placeholder="Ingrese Observaciones" disabled>{{$postulante->postulaciones[0]->observaciones}}</textarea>
                                        <br>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="" style="display: flex; flex-direction: row;justify-content: left; align-items: center; margin-bottom: 25px">
                                            <label id="estado_postulacion"
                                                   style="font-weight: bold;margin-left:30px">
                                                Estado : {{$postulante->postulaciones[0]->estado}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{route('verPostulantes')}}" class="btn btn-success"  style="margin-left: 45%">Retornar</a>

                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="idPostulante" id="idPostulante" value="{{$postulante->id}}">
                <input type="hidden" name="condicionPos" id="condicionPos" value="{{$postulante->condicion}}">
            </form>
            <!-- /basic layout -->
        </div>
    </div>
@endsection
@section('modals')
    <div id="verDni" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dni</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Dni</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'dni')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="verFoto" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dni</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Dni</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'foto_name')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="verVoucher" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Voucher</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Voucher</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'voucher')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="verCertificado" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Certificado</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Certificado</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'img_certificado')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>
                            @endif
                        @endforeach
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="verPartida" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Partida de Nacimieto</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Partida de Nacimieto</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'img_part_nacimiento')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div id="verSolicitud" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Solicitud</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'solicitud')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="verExoneracion" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Solicitud</h6>
                    <div>
                        @foreach($anexos as $anexo)
                            @if($anexo->nombre == 'const_exonerac')
                                <iframe src="{{$anexo->url}}" alt="" style="width: 850px;height: 600px"></iframe>

                            @endif
                        @endforeach
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="verComprobanteEmitido" class="modal fade " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Comprobante de Pago</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h6 class="font-weight-semibold">Comprobante de Pago</h6>
                    <div>
                        <iframe src="{{$anexo->urlimg}}" alt="" style="width: 850px;height: 600px"></iframe>
                        {{--                        <label>{{$comprobante[0]->urlimg}}</label>--}}
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

