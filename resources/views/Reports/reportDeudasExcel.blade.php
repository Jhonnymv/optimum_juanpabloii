<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
        <style>
            table {
                border: 2px solid black;
                width: 100%

            }
            th {
                border-bottom: 2px solid black;
                border-collapse: collapse;
                font-size: 10px;
            }

            .contenidos {
                border: 0.5px solid black;
                border-collapse: collapse;
                table-layout: fixed;
            }

            .contenidos tr {
                border: 0.5px solid black;
                border-collapse: collapse;
            }

            td {
                text-align: center;
                font-size: 9px;
                /*border: 0.5px solid black;*/
                /*border-collapse: collapse;*/
            }

            tfoot td {
                border-top: 2px solid black;
                border-collapse: collapse;
            }
        </style>
</head>
<body>
    @foreach($data as $keyLevel=>$level)
        @foreach($level as $keyGrado=>$grado)
            @foreach($grado as $keyGrupo=>$grupo)
                <strong>Educación:</strong> {{$keyLevel}} - <strong>Grado:</strong> {{$keyGrado}}°
                <strong>Sección:</strong> {{$keyGrupo}}<br>
                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>DNI</th>
                        <th>ALUMNO</th>
                        <th>TELÉFONO</th>
                        <th>TELÉFONO PADRE</th>
                        <th>TELÉFONO MADRE</th>
                        <th>MAT</th>
                        <th>MAR</th>
                        <th>ABR</th>
                        <th>MAY</th>
                        <th>JUN</th>
                        <th>JUL</th>
                        <th>AGO</th>
                        <th>SEP</th>
                        <th>OCT</th>
                        <th>NOV</th>
                        <th>DIC</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($grupo as $loop=>$alumno)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td style="text-align: left !important;">{{$alumno['DNI']}}</td>
                            <td style="text-align: left !important;">{{$alumno['telefono']}}</td>
                            <td style="text-align: left !important;">{{$alumno['alumno']}}</td>
                            <td style="text-align: left !important;">{{$alumno['tpadre']}}</td>
                            <td style="text-align: left !important;">{{$alumno['tmadre']}}</td>
                            <td style="color: {{$alumno['matr']>0?'red':'blue'}}">{{$alumno['matr']}}</td>
                            <td style="color: {{$alumno['mar']>0?'red':'blue'}}">{{$alumno['mar']}}</td>
                            <td style="color: {{$alumno['abr']>0?'red':'blue'}}">{{$alumno['abr']}}</td>
                            <td style="color: {{$alumno['may']>0?'red':'blue'}}">{{$alumno['may']}}</td>
                            <td style="color: {{$alumno['jun']>0?'red':'blue'}}">{{$alumno['jun']}}</td>
                            <td style="color: {{$alumno['jul']>0?'red':'blue'}}">{{$alumno['jul']}}</td>
                            <td style="color: {{$alumno['ago']>0?'red':'blue'}}">{{$alumno['ago']}}</td>
                            <td style="color: {{$alumno['sep']>0?'red':'blue'}}">{{$alumno['sep']}}</td>
                            <td style="color: {{$alumno['oct']>0?'red':'blue'}}">{{$alumno['oct']}}</td>
                            <td style="color: {{$alumno['nov']>0?'red':'blue'}}">{{$alumno['nov']}}</td>
                            <td style="color: {{$alumno['dic']>0?'red':'blue'}}">{{$alumno['dic']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3" style="justify-content: center">Total:</td>
                        <td style="color: {{$grupo->sum('matr')>0?'red':'blue'}}">{{$grupo->sum('matr')}}</td>
                        <td style="color: {{$grupo->sum('mar')>0?'red':'blue'}}">{{$grupo->sum('mar')}}</td>
                        <td style="color: {{$grupo->sum('abr')>0?'red':'blue'}}">{{$grupo->sum('abr')}}</td>
                        <td style="color: {{$grupo->sum('may')>0?'red':'blue'}}">{{$grupo->sum('may')}}</td>
                        <td style="color: {{$grupo->sum('jun')>0?'red':'blue'}}">{{$grupo->sum('jun')}}</td>
                        <td style="color: {{$grupo->sum('jul')>0?'red':'blue'}}">{{$grupo->sum('jul')}}</td>
                        <td style="color: {{$grupo->sum('ago')>0?'red':'blue'}}">{{$grupo->sum('ago')}}</td>
                        <td style="color: {{$grupo->sum('sep')>0?'red':'blue'}}">{{$grupo->sum('sep')}}</td>
                        <td style="color: {{$grupo->sum('oct')>0?'red':'blue'}}">{{$grupo->sum('oct')}}</td>
                        <td style="color: {{$grupo->sum('nov')>0?'red':'blue'}}">{{$grupo->sum('nov')}}</td>
                        <td style="color: {{$grupo->sum('dic')>0?'red':'blue'}}">{{$grupo->sum('dic')}}</td>
                    </tr>
                    </tfoot>
                </table>
                <br>
            @endforeach
        @endforeach
    @endforeach

</body>
</html>
