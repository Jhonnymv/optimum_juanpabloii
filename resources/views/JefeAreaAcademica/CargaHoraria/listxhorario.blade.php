@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Horarios')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Horarios por
        Secciones</h4>
@endsection

@section('header_buttons')
    <div class="d-flex justify-content-center">
        <a href="#" class="btn btn-link btn-float font-size-sm font-weight-semibold text-default" data-toggle="modal"
           data-target="#modalHorario">
            <i class="icon-calendar5 text-pink-300"></i>
            <span>Agregar Horario</span>
        </a>
    </div>
@endsection

@section('header_subtitle')
    <a href="{{route('JefeAreaAcedemica.cargarhoraria.index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Horarios</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Horarios</h5>
                    <div class="header-elements">
                    </div>
                </div>

                <div class="card-body">
                    Este es una pantalla para la edición y mantenimiento de horarios para:
                    <strong>{{ $secciones->first()->pe_curso->curso->nombre }} del grado {{$secciones->first()->pe_curso->semestre}}
                        de nivel {{ $secciones->first()->pe_curso->curso->carrera->nombre }}</strong>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Aula</th>
                            <th>Día</th>
                            <th>Hora Inicio</th>
                            <th>Hora Fin</th>
                            <th>Turno</th>
                            <th>Opciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @forelse($horarios as $item)
                            <tr>
                                <td>
                                    {{$loop->index+1}}
                                </td>
                                <td>{{$item->aula->aula}}</td>
                                <td>{{$item->dia}}</td>
                                <td>{{$item->hora_inicio}}</td>
                                <td>{{$item->hora_fin}}</td>
                                <td>{{$item->turno}}</td>
                                <td>

                                        <button onclick="remove_horario({{$item->id}})"
                                                class='btn btn-outline bg-danger border-danger btn-icon rounded-round legitRipple'
                                                type='button'>
                                            <i class='icon-cross2'></i></button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No hay elementos</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /basic table -->

        </div>
    </div>
    <input type="hidden" id="Url_remove_detalle" value="{{route('horario.remove')}}">
@endsection
@section('modals')

    <!-- crear modal -->
    <!-- crear Horario -->
<div id="modalHorario" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('cargarhoraria.storehorario')}}">
            <!--Importante-->
            @csrf
            <!--Importante-->
            <div class="modal-header">
                <h5 class="modal-title">Horario</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                <label class="col-lg-3 col-form-label">Día:</label>
                <select id="sel_dia" name="sel_dia" class="form-control">
                            <option>Seleccionar un día</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miércoles">Miércoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                            <option value="Sábado">Sábado</option>
                            <option value="Domingo">Domingo</option>
                </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Turno:</label>
                    <select id="sel_turno" name="sel_turno" class="form-control">
                            <option>Seleccionar un turno</option>
                            <option value="Mañana">Mañana</option>
                            <option value="Tarde">Tarde</option>
                            <option value="Noche">Noche</option>

                </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Hora Inicio:</label>
                    <div class="col-lg-9">
                    <input class="form-control" type="time" name="horainicio">
					<span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Hora Fin:</label>
                    <div class="col-lg-9">
                    <input class="form-control" type="time" name="horafin">
					<span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Aula:</label>
                    <select id="sel_aula" nombre="sel_aula" class="form-control">
                            <option>Seleccionar un aula</option>
                            @forelse ($aulas as $item)
                                <option value="{{$item->id}}">
                                    {{$item->aula}}
                                </option>
                            @empty

                            @endforelse

                        </select>
                </div>

</div>
            <input type="hidden" id="aula_id" name="aula_id" value="0">
            <input type="hidden" id="turno_id" name="turno_id" value="0">
            <input type="hidden" id="dia" name="dia" value="0">

            <input type="hidden" id="seccion_id" name="seccion_id" value="{{$secciones->first()->id}}">

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear Horario -->
    <!-- Danger modal -->
<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form id="formEliminar" class="modal-content" action="/horarios/remove/" method="POST">

            @method('DELETE');

            <!--Importante-->
            @csrf
            <!--Importante-->


            <div class="modal-header bg-danger">
                <h6 class="modal-title">Desea Eliminar Carrera?</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-danger">Eliminar Carrera</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>


        function eliminar(id,sec){
        $("#modalEliminar").modal('show');
        $("#formEliminar").attr('action','/horrarios/remove/'+id+'/'+sec);
    }
    function remove_horario(id) {
        console.log("remove")
            var url = $('#Url_remove_detalle').val()
            var data = {'id': id}
            console.log(url)
            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log("remove2")
                    alert('Horario Eliminado')
                    location.reload()
                }
            })
        }
        $('#sel_aula').change(function(){
            var id=$(this).val();
            console.log(id);
            $('#aula_id').val(id);
            //$('#sel_periodo2').val(id);
//            console.log($('#periodo_id').val(id));
            //document.getElementById("sel_periodo2").value=id;
            //listarCursos();
            //recibir();
        });

        $('#sel_turno').change(function(){
            var id=$(this).val();
            console.log(id);
            $('#turno_id').val(id);
        });


        $('#sel_dia').change(function(){
            var id=$(this).val();
            console.log(id);
            $('#dia').val(id);
        });
//});





</script>
@endsection
