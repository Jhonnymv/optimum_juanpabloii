@extends('SecretariaAcademica.Layouts.main_secretaria')
@section('title','Carga Horaria')

@section('header_title')
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Carga Horaria</h4>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('assets/DataTables/datatables.min.css')}}">
@endsection

@section('header_subtitle')
    <a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
    <span class="breadcrumb-item active">Cargar Horaria</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
    <div class="row justify-content-center" align="right">
        <div class="col-md-12">
            <h7 class="card-title">


            </h7>
        </div>
    </div>

    <div class="row justify-content-center">

        <div class="col-md-12">
            <!-- Basic table -->

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Secciones por Área</h5>

                    <div class="header-elements">

                    </div>
                </div>

                <input type="hidden" id="carrera_id" name="carrera_id" value="0">
                <input type="hidden" id="planestudio_id" name="planestudio_id" value="0">
                <input type="hidden" id="periodo_id" name="periodo_id" value="0">

                <div class="card-body">
                    <div>
                        <button href="#" type="button" onclick="cargardata()" id="btnAgregar" name="btnAgregar"
                                class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                style="font-size:8pt" data-toggle="modal" data-target="#modalCrear">
                            <b><i class="icon-add mr-1 icon-1x"></i>
                            </b> Agregar Sección
                        </button>
                        <br></br>
                        <div class="table-responsive">
                            <table class="table" id="table_secciones">
                                <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Cod. Curso</th>
                                    <th>Curso</th>
                                    <th>Nivel</th>
                                    <th>Grado</th>
                                    <th>Docente</th>
                                    <th>Sección</th>
                                    <th>Cupos</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody id="tbody_secciones">
                                @forelse ($secciones as $item)
                                    <tr>
                                        <td>
                                            {{ $item->pe_curso->semestre }}
                                        </td>
                                        <td>
                                            {{ $item->pe_curso->curso->id }}
                                        </td>
                                        <td>
                                            {{ $item->pe_curso->curso->nombre }}
                                        </td>
                                        <td>
                                            {{ $item->pe_curso->curso->carrera->nombre }}
                                        </td>
                                        <td>
                                            {{ $item->pe_curso->semestre }}
                                        </td>
{{--                                        <td>--}}
{{--                                            {{ $item->pe_curso->curso->carrera->nombre }}--}}
{{--                                        </td>--}}

                                        {{--                                            {{ $item->docentes->first()->id}}--}}

                                        <td>
                                            @foreach($item->docentes as $docente)
                                                {{$docente->persona->paterno.' '.$docente->persona->materno.' '.$docente->persona->nombres}}
                                            @endforeach

                                        </td>


                                        <td>
                                            {{ $item->seccion}}
                                        </td>
                                        <td>
                                            {{ $item->cupos}}
                                        </td>

                                        <td>
                                            <a href="{{route('cargarhoraria.listxseccion',['seccion_id'=>$item->id])}}"
                                               type="button" class="btn btn-success btn-sm legitRipple"><i
                                                    class="icon-clipboard3 mr-2"></i> Ver Horarios
                                            </a>
                                            <a href="#" class="btn btn-warning" onclick="editar({{ $item->id }})"><i
                                                    class="fas fa-pen"></i></a>

                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="6">No se ha registrado Carga Horaria</td>
                                    </tr>
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /basic table -->
                </div>
            </div>


        </div>
    </div>
@endsection



@section('modals')

    <!-- crear modal -->
    <div id="modalCrear" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{route('cargarhoraria.store')}}">
                <!--Importante-->
            @csrf
            <!--Importante-->


                <div class="modal-header">
                    <h5 class="modal-title">Registrar Sección</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Nivel:</label>
                        <div class="col-lg-9">
                            <select id="sel_carrera" class="form-control">
                                <option>Seleccionar una Nivel</option>
                                @forelse ($carreras as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nombre}}
                                    </option>
                                @empty

                                @endforelse

                            </select>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Planes de Estudios:</label>
                        <div class="col-lg-9">
                            <select id="sel_planestudio" class="form-control">
                                <option>Seleccionar un plan de estudios</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <select id="sel_periodo" nombre="sel_periodo" class="form-control">
                                <option>Seleccionar un periodo</option>
                                @forelse ($periodos as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->descripcion}}
                                    </option>
                                @empty

                                @endforelse

                            </select>
                        </div>

                    </div>
                    <input type="hidden" id="carrera_id" name="carrera_id" value="0">
                    <input type="hidden" id="planestudio_id" name="planestudio_id" value="0">
                    <input type="hidden" id="periodo_id" name="periodo_id" value="0">
                    <input type="hidden" id="sel_periodo2" name="sel_periodo2" value="0">

                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Curso</label>
                        <div class="col-lg-9">
                            <select id="sel_curso" name="sel_curso" data-placeholder="Seleccionar un curso"
                                    class="form-control form-control-select2" data-fouc="">


                            </select>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-lg-9 col-form-label">Docente:</label>
                        <!-- <div class="col-lg-9">
                            <input type="text" name="resolucion" class="form-control" placeholder="" required>
                        </div> -->
                        <select id="sel_docente" class="form-control">
                            <option>Seleccionar un docente</option>
                            @forelse ($docentes as $item)
                                <option value="{{$item->id}}">
                                    {{$item->persona->paterno}} {{$item->persona->materno}} {{$item->persona->nombres}}
                                </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Sección:</label>
                        <div class="col-lg-9">
                            <select id="sel_grupo" name="sel_grupo" class="form-control">
                                <option>Seleccionar una grupo</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="TALLER">Taller</option>
                            </select>
                        </div>

                    </div>
                    <input type="hidden" id="grupo_id" name="grupo_id" value="0">
                    <input type="hidden" id="docente_id" name="docente_id" value="0">
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Cupos:</label>
                        <div class="col-lg-4">
                            <input type="text" name="cupos" id="cupos" class="form-control" placeholder="" required>
                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn bg-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /crear modal -->
    <div id="modalEditar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <form id="formEditar" class="modal-content" method="POST" action="/secciones">

            @method('PUT')

            <!--Importante-->
            @csrf
            <!--Importante-->
                <input type="hidden" id="id" name="id" value="0">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Carga Horaria</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Nivel:</label>
                        <div class="col-lg-9">
                            <input type="text" name="carrera" id="carrera" class="form-control" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Plan de Estudio:</label>
                        <div class="col-lg-9">
                            <input type="text" name="plan" id="plan" class="form-control" placeholder="" readonly>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Periodo:</label>
                        <div class="col-lg-9">
                            <input type="text" name="periodo" id="periodo" class="form-control" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Curso:</label>
                        <div class="col-lg-9">
                            <input type="text" name="curso" id="curso" class="form-control" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Sección:</label>
                        <div class="col-lg-9">
                            <input type="text" name="grupo" id="grupo" class="form-control" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Cupos:</label>
                        <div class="col-lg-9">
                            <input type="text" name="cuposEdit" id="cuposEdit" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Docente:</label>
                        <div class="col-lg-9">
                            <select id="sel_docenteEdit" name="sel_docenteEdit" class="form-control">
                                <option value="0">Seleccionar un Docente</option>
                                @forelse ($docentes as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->persona->paterno}} {{$item->persona->materno}} {{$item->persona->nombres}}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                        <button type="submit" onclick="validar(sel_docenteEdit.value)" class="btn bg-primary">Guardar
                            Cambios
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection



@section('script')


    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#table_secciones').DataTable({
                "language": {
                    "url": "{{asset('assets/DataTables/Spanish.json')}}"
                }
            });
        });

        function cargardata() {

            $('#sel_carrera').change(function () {
                var id = $(this).val();
                console.log(id);
                //var url = $('#url_get_planescarrera').val()
                var url = "/cargarPlanesCarrera/"
                // $('#carrera_id').val(id);
                //console.log(url);

                var grupos = {'A':'A', 'B':'B','C':'C','D':'D','E':'E'}
                if (id != "") {
                    var data = {'carrera_id': id};


                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            $("#sel_planestudio").html('');
                            $("#sel_planestudio").append(new Option('Seleccionar un plan de estudio', ''));
                            //
                            for (var i in data) {
                                $("#sel_planestudio").append(new Option(data[i].planestudio.plan, data[i].id));
                            }

                        }
                    });

                    $("#sel_grupo").show();


                    // $.ajax({
                    //     type: "GET",
                    //     url: "/cargarGrupos",
                    //     data: data,
                    //     dataType: 'json',
                    //     success: function (data) {
                    //         //   console.log(data);
                    //         $("#sel_grupo").html('');
                    //         $("#sel_grupo").append(new Option('Seleccionar grupo', ''));
                    //
                    //
                    //         for (var i in data) {
                    //             $("#sel_grupo").append(new Option(data[i].grupo, data[i].grupo));
                    //         }
                    //
                    //
                    //     }
                    // });

                } else {
                    $("#sel_planestudio").html('');
                    $("#sel_planestudio").append(new Option('Seleccionar un plan de estudio', ''));
                }
            });

            $('#sel_planestudio').change(function () {
                var id = $(this).val();

                $('#planestudio_id').val(id);
            });

            $('#sel_periodo').change(function () {
                var id = $(this).val();
                console.log(id);
                $('#periodo_id').val(id);
                $('#sel_periodo2').val(id);
//            console.log($('#periodo_id').val(id));
                document.getElementById("sel_periodo2").value = id;
                listarCursos();
                //recibir();
            });

            $('#sel_grupo').change(function () {
                var id = $(this).val();
                console.log(id);
                $('#grupo_id').val(id);
            });


            $('#sel_docente').change(function () {
                var id = $(this).val();
                console.log(id);
                $('#docente_id').val(id);
            });
//});


            $('#sel_curso').select2({
                dropdownParent: $('#modalCrear')

            });

            // function recibir()
            // {
            // var valor = document.getElementById("sel_periodo2").value;
            // document.getElementById("per").value=valor;

            // }
            function listarCursos() {
                var carrera_id = $('#carrera_id').val()
                var planestudio_id = $('#planestudio_id').val()
                var data = {'pecarrera_id': planestudio_id}

                $.ajax({
                    url: '/obtenerCursos',
                    type: 'GET',
                    data: data,
                    datatype: 'json',
                    success: function (data) {
                        {{--  $('#tbody_cursos').html('')  --}}
                        $('#sel_curso').html('')
                        $('#sel_curso').append(new Option('Seleccionar curso', ''))
                        data.pe_cursos.forEach(element => {
                            var template = "<tr>"
                                + "<input type='hidden' id='pe_curso_id' value='" + element.id + "' name='pe_curso_id[]'>"
                            // + "<td>" + element.semestre  + "</td>"
                            // + "<td>" + element.id+" " + "</td>"
                            // + "<td>" + element.curso.nombre+ "</td>"
                            // + "<td>" +  element.creditos + "</td>"
                            // + "<td>" + element.horas_teoria + "</td>"
                            // + "<td>" + element.horas_practica + "</td>"
                            // +"<td> <button href='#' type='button' onclick='' id='btnSeleccionarCurso' name='btnSeleccionarCurso' class='btn bg-teal-400 btn-labeled btn-labeled-left' size='10' style='font-size:8pt' ><b><i class='icon-add mr-1 icon-1x'  ></i></b> Seleccionar</button>"
                            // +"</td>"

                            // + "</tr>"
                            $('#tbody_cursos').append(template)

                            $('#sel_curso').append("<option value=" + element.id + ">" + element.curso.nombre + " Grado - " + element.semestre + "</option>");

                        })
                        console.log(data)
                        // $('#t_cursos').DataTable()
                    },
                    error: function () {
                        console.log("ERROR");
                    }
                });
            }
        }

        function eliminar(id) {
            $("#modalEliminar").modal('show');
            $("#formEliminar").attr('action', '/carreras/remove/' + id);
        }

        function editar(id) {

            $("#formEditar").attr('action', '/secciones/' + id);

            var data = {'id': id};
            $.ajax({
                type: "GET",
                url: '/traerseccion',
                data: data,
                success: function (data) {
                    console.log(data)
                    //$('#CarreraEdit').val(data['seccion']['nombre']);
                    $('#carrera').val(data.seccion.pe_curso.pecarrera.carrera.nombre)
                    $('#plan').val(data.seccion.pe_curso.pecarrera.planestudio.plan)
                    $('#periodo').val(data.seccion.periodo.descripcion)
                    $('#curso').val(data.seccion.pe_curso.curso.nombre)
                    $('#grupo').val(data.seccion.grupo)
                    $('#cuposEdit').val(data.seccion.cupos)
                    $('#id').val(data.seccion.id)
                    //$('#sel_docenteEdit').val(data.seccion.docentes.id)
                    //$('#selcarrera').val(data.seccion.pe_curso.curso.id)
                    if (data.seccion.docente_seccionb == null) {
                        //$('#sel_curso').html('')
                        //$('#sel_curso').append(new Option('Seleccionar curso', ''))
                        $('#sel_docenteEdit option[value="0"]').prop("selected", true);
                        $('#sel_docenteEdit').attr("required", true);
                        //alert("null");
                    } else
                        $('#sel_docenteEdit option[value="' + data.seccion.docente_seccionb.docente.id + '"]').prop("selected", true);
                    //$('#Plan').val(data['carrera']['resolucion']);
                    //$('#responsableEdit').val(data['carrera']['responsable_id']);

                    $("#modalEditar").modal('show');
                },
                error: function () {
                    console.log("ERROR");
                }
            });
        }

        function validar(id) {
            //alert(id);
            if (id == 0) {
                alert('Antes de enviar, seleccione un docente');
                event.preventDefault();
                //event.stopPropagation();
            }

        }
    </script>
@endsection
