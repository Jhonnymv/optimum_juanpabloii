@extends('Layouts.main')
@section('title','Carga Horaria')

@section('header_title')
<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Carga Horaria</h4>
@endsection


@section('header_subtitle')
<a href="{{url('/')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<span class="breadcrumb-item active">Cargar Horaria</span>
@endsection
@section('header_subbuttons')

@endsection

@section('content')
 <div class="row justify-content-center" align="right">
    <div class="col-md-12">
         <h7 class="card-title">


          </h7>
    </div>
</div>

<div class="row justify-content-center">

    <div class="col-md-12">
        <!-- Basic table -->

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lista de Cursos</h5>

                <div class="header-elements">
                </div>
             </div>

             <input type="hidden" id="carrera_id" name="carrera_id" value="0">
             <input type="hidden" id="planestudio_id" name="planestudio_id" value="0">
             <input type="hidden" id="periodo_id" name="periodo_id" value="0">
             <input type="hidden" id="grupo_id" name="grupo_id" value="0">


            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Carrera:</label>
                    <div class="col-lg-4">
                        <select id="sel_carrera" class="form-control">
                            <option>Seleccionar una carrera</option>
                            @forelse ($carreras as $item)
                                <option value="{{$item->id}}">
                                    {{$item->nombre}}
                                </option>
                            @empty

                            @endforelse

                        </select>
                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Planes de Estudios:</label>
                    <div class="col-lg-4">
                        <select id="sel_planestudio" class="form-control">
                            <option>Seleccionar un plan de estudios</option>
                        </select>
                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Periodo:</label>
                    <div class="col-lg-4">
                        <select id="sel_periodo" class="form-control">
                            <option>Seleccionar un periodo</option>
                            @forelse ($periodos as $item)
                                <option value="{{$item->id}}">
                                    {{$item->descripcion}}
                                </option>
                            @empty

                            @endforelse

                        </select>
                    </div>
                    <input type="text" id="sel_periodo2" name="sel_periodo2">
                </div>


                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Grupos:</label>
                    <div class="col-lg-4">
                        <select id="sel_grupo" class="form-control">
                            <option>Seleccionar una grupo</option>
                        </select>
                    </div>

                </div>

            </div>

            <div class="card-body">
                <div>
                    <button href="#" type="button" onclick="listarCursos()" id="btnAgregar" name="btnAgregar"
                        class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                        style="font-size:8pt"  data-toggle="modal" data-target="#modalCrear">
                        <b><i class="icon-add mr-1 icon-1x"  ></i>
                    </b> Agregar Sección</button>


                </div>


            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nivel</th>
                            <th>Cod. Curso</th>
                            <th>Curso</th>
                            <th>Cod. Docente</th>
                            <th>Docente</th>
                            <th>Sección</th>
                            <th>Cupos</th>
                        </tr>
                    </thead>
                    <tbody>



                        <tr>
                            <td>
                               1
                            </td>
                            <td>
                               000001
                            </td>
                            <td>
                                Matematica 1
                            </td>
                            <td>
                                44440101
                            </td>
                            <td>
                                Julio Roman
                            </td>
                            <td>
                                A
                            </td>
                            <td>
                                60
                            </td>
                            <td>
                                <button href="#" type="button" onclick=""
                                class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                                style="font-size:8pt" data-toggle="modal"
                                data-target="#modalHorario" >
                                <b><i class="icon-add mr-1 icon-1x"  ></i>
                                </b> HORARIO</button>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="6">No hay elementos</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
</div>
@endsection



@section('modals')

<!-- crear modal -->
<div id="modalCrear" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('cargarhoraria.store')}}">
            <!--Importante-->
            @csrf
            <!--Importante-->
            <input type="hidden" id="carrera_id" name="carrera_id" value="0">
             <input type="hidden" id="planestudio_id" name="planestudio_id" value="0">

             <input type="hidden" id="per" name="per">

             <input type="hidden" id="grupo_id" name="grupo_id" value="0">

            <div class="modal-header">
                <h5 class="modal-title">Registrar Sección</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group row">
                        <label class="col-lg-3 col-form-label" >Curso</label>
                        <div class="col-lg-4">
                           <select id="sel_curso" name="sel_curso"  data-placeholder="Seleccionar un curso" class="form-control form-control-select2" data-fouc=""  >


                            </select>
                        </div>
                        <!-- <button href="#" type="button" onclick="listarCursos()"
                        class="btn bg-teal-400 btn-labeled btn-labeled-left" size="10"
                        style="font-size:8pt"  data-toggle="modal" data-target="#modalCursos">
                        <b><i class="icon-add mr-1 icon-1x"  ></i>
                    </b> Buscar Curso</button>                    -->
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Docente:</label>
                    <!-- <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="" required>
                    </div> -->
                    <select id="docente" class="form-control">
                            <option>Seleccionar un docente</option>
                            @forelse ($docentes as $item)
                                <option value="{{$item->id}}">
                                    {{$item->persona->paterno}} {{$item->persona->materno}} {{$item->persona->nombres}}
                                </option>
                            @empty

                            @endforelse

                        </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Sección:</label>
                    <select id="grupo" name="grupo" class="form-control">
                            <option>Seleccionar una Sección</option>

                                <option value="A">A

                                </option>
                                <option value="B">B

                                </option>


                        </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Cupos:</label>
                    <div class="col-lg-9">
                        <input type="text" name="cupos" id="cupos" class="form-control" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-1 col-form-label">Grupos:</label>
                    <div class="col-lg-4">
                        <select id="sel_grupo" class="form-control">
                            <option>Seleccionar una grupo</option>
                        </select>
                    </div>

                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear modal -->



<!-- crear Horario -->
<div id="modalHorario" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="">
            <!--Importante-->
            @csrf
            <!--Importante-->
            <div class="modal-header">
                <h5 class="modal-title">Horario</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Dia:</label>
                    <div class="col-lg-9">
                        <input type="text" name="nombre" class="form-control" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Turno:</label>
                    <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Hora Inicio:</label>
                    <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Hora Fin:</label>
                    <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Aula:</label>
                    <div class="col-lg-9">
                        <input type="text" name="resolucion" class="form-control" placeholder="" required>
                    </div>
                </div>



                <div id="sesionesAcademicas" class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Día</th>
                                <th>Turno</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>LUNES</td>
                                <td>MAÑANA</td>
                                <td>10:00 am</td>
                                <td>11:00 am</td>
                                <td>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn bg-primary">Guardar Cambios</button>
            </div>
        </form>
    </div>
</div>
<!-- /crear Horario -->

@endsection



@section('script')
<script>
    document.addEventListener('DOMContentLoaded', function() {


       // $('.form-control-select2').select2();


        $('#sel_carrera').change(function(){
            var id=$(this).val();

            $('#carrera_id').val(id);
            // console.log(id);


            if(id != ""){

                var data = {'carrera_id':id};

                $.ajax({
                    type: "GET",
                    url: "/cargarPlanesCarrera",
                    data: data,
                    dataType:'json',
                    success: function(data){
                        // console.log(data);
                        $("#sel_planestudio").html('');
                        $("#sel_planestudio").append(new Option('Seleccionar un plan de estudio',''));

                        for(var i in data) {
                            $("#sel_planestudio").append(new Option(data[i].planestudio.plan,data[i].id));
                        }


                    }
                });

                $.ajax({
                    type: "GET",
                    url: "/cargarGrupos",
                    data: data,
                    dataType:'json',
                    success: function(data){
                      //   console.log(data);
                        $("#sel_grupo").html('');
                        $("#sel_grupo").append(new Option('Seleccionar grupo',''));

                        for(var i in data) {
                            $("#sel_grupo").append(new Option(data[i].grupo,data[i].grupo));
                        }


                    }
                });

            }else{
                $("#sel_planestudio").html('');
                $("#sel_planestudio").append(new Option('Seleccionar un plan de estudio',''));
            }


        });

        $('#sel_planestudio').change(function(){
            var id=$(this).val();

            $('#planestudio_id').val(id);
        });

        $('#sel_periodo').change(function(){
            var id=$(this).val();

            $('#periodo_id').val(id);
            $('#sel_periodo2').val(id) ;
            recibir();
        });

        $('#sel_grupo').change(function(){
            var id=$(this).val();

            $('#grupo_id').val(id);
        });


    });


    $('#sel_curso').select2({
        dropdownParent: $('#modalCrear')

    });

    function recibir()
    {
    var valor = document.getElementById("sel_periodo2").value;
    document.getElementById("per").value=valor;

    }
    function listarCursos(){
        var carrera_id=$('#carrera_id').val()
        var planestudio_id=$('#planestudio_id').val()
        var data={'pecarrera_id':planestudio_id}

        $.ajax({
            url:'/obtenerCursos',
            type:'GET',
            data:data,
            datatype:'json',
            success: function(data){
                {{--  $('#tbody_cursos').html('')  --}}
                $('#sel_curso').html('')
                $('#sel_curso').append(new Option('Seleccionar curso',''))
                data.pe_cursos.forEach(element => {
                      var template = "<tr>"
                        +"<input type='hidden' id='pe_curso_id' value='" + element.id+ "' name='pe_curso_id[]'>"
                        // + "<td>" + element.semestre  + "</td>"
                        // + "<td>" + element.id+" " + "</td>"
                        // + "<td>" + element.curso.nombre+ "</td>"
                        // + "<td>" +  element.creditos + "</td>"
                        // + "<td>" + element.horas_teoria + "</td>"
                        // + "<td>" + element.horas_practica + "</td>"
                        // +"<td> <button href='#' type='button' onclick='' id='btnSeleccionarCurso' name='btnSeleccionarCurso' class='btn bg-teal-400 btn-labeled btn-labeled-left' size='10' style='font-size:8pt' ><b><i class='icon-add mr-1 icon-1x'  ></i></b> Seleccionar</button>"
                        // +"</td>"

                        // + "</tr>"
                    $('#tbody_cursos').append(template)

                    $('#sel_curso').append("<option value=" + element.id + ">" + element.curso.nombre + "semestre - " + element.semestre + "</option>");

                })
                  console.log(data)
                  // $('#t_cursos').DataTable()
            },
            error: function() {
                console.log("ERROR");
            }
        });
    }






</script>
@endsection
