<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Iniciar Sesion en OptimumLab</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    {{--    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">--}}

    <style>
        html, body {
            margin: 0px;
            height: 100%;
            background: #fff;
        }

        .imgBackground {
            position: absolute;
            object-fit: cover;
        }

        .MainContent {
            height: 70%;
            background: url("{{asset('assets/img/back-login.png')}}");
            background-repeat: no-repeat;
            background-size: cover;
            background-position-y: bottom;
            background-position-x: center;
        }

        .btn-primary {
            background-color: #006298;
        }

        .btn-primary:hover {
            background-color: #ffffff;
            color: #006298;
        }

        .form {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
            margin-left:calc(50% - 150px);
            padding: 25px;
        }

        .logo {
            position: absolute;
            bottom: 0;
            right: 0;
            margin-bottom: 40px;
            margin-right: 20px;
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
            align-items: flex-end;

        }

        .imgLogo {
            width: 200px;
            padding-right: 25px;
            padding-bottom: 25px;
        }

        .content {

            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }

        .colum {
            display: flex;
            flex-direction: column;
        }

        .row {
            display: flex;
            flex-direction: row;
        }

        .arrow {
            padding: 5px;
        }

        .buttonGoogle {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        #title{
            position: absolute;
            z-index: 0;
            font-size: 450%;
            color: #ffffff;
            margin-top: 50px;
            margin-left: 50px;
        }

        @font-face {
            font-family: "Commercial Script MN Regular";
            /*src: url("public/assets/fonts/Commercial Script MN Regular.ttf");*/
        }

        .logo-jpii {
            font-family: "Commercial Script MN Regular";
        }

        @media (min-width: 300px) and (min-height: 1000px) and (max-width: 1000px){
            .MainContent {
                height: 50%; !important;
            }

            .form {
                display: none;
            }

            .content {
                height: 50%;
                flex-direction: column;
                justify-content: space-evenly;
            }

            .form2 {
                font-size: 50px;
                display: flex;
                justify-content: center;
                align-items: center;
            }
        }

        @media (max-width: 900px){
            .logo {
                display: none;
            }
        }

        @media (max-width: 500px){
            #title {
                font-size: 250%;
            }
        }
    </style>

</head>
<body>
    <div style="position: fixed;
    bottom: 0;
    left: 0;
    width: 160px;
    display: block;
    height: 240px;
    z-index: 1000;
    margin-left: 50px;
    text-align:center;
    color:#ffffff" class="logo">
        <img id="logo" src="/assets/img/circular/2.png" alt="" class="logo logo-jpii" style="width: 100%">
        <br>
    </div>
    <span id="title" style="display: block">
        <strong>Iniciando la <br>
        transformación <br>
        digital...</strong>
    </span>

    <div class="logo row">
        <img src="{{asset('assets/img/optimum-logo.svg')}}" alt="" class="imgLogo">
    </div>
<div class="MainContent">
</div>

<div class="content">
    <div class="form">
        <form class="login-form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row">
                <div class="col-12 colum">
                    <div class="row align-items-center justify-content-center">
                        <div class="colum" style="margin-left: 10px; margin-right: 10px">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" name="usuario" class="form-control @error('usuario') border-danger @enderror"
                                       value="{{ old('usuario') }}" placeholder="Usuario" autofocus required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                @error('usuario')
                                <span class="form-text text-danger">Credenciales incorrectas.</span>
                                @enderror
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" name="password"
                                       class="form-control @error('usuario') border-danger @enderror" placeholder="Contraseña"
                                       required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @error('usuario')
                                <span class="form-text text-danger">Credenciales incorrectas.</span>
                                @enderror
                            </div>
                        </div>
                        <div class="arrow" style="margin-left: 10px; margin-right: 10px">
                            <button type="submit" class="btn btn-primary btn-block">Iniciar sesión <i
                                    class="icon-circle-right2 ml-2"></i></button>
                            <a href="{{route('Register_Padre')}}" class="btn btn-info btn-block">Registrate <i
                                    class="icon-circle-right2 ml-2"></i></a>
                        </div>
                    </div>
                    <div class="buttonGoogle">
                        <a href="http://localhost:8000/auth/google" class="btn btn-outline btn-icon rounded-round border-2 legitRipple" style="
                                        border-color: #F44336;
                                        color: #F44336;
                                        background-color: #F44336;
                                    "><i class="icon-google"></i> Google
                        </a>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>

</body>
</html>
