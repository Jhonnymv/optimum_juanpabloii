@extends('IntranetPadre.Layouts.main_padres')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Alumnos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para el control de asistencias y notas de sus hijos ("Si tuviese más de dos hijos contactese con la Administración
                    para el cambio de categoría")
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_alumnos">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>Categoria descuento</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hijos as $item)
                                <tr>
                                    <td>{{$item->persona->paterno.' '.$item->persona->materno.', '.$item->persona->nombres}}</td>
                                    <td>{{$item->carreras->first()->nombre}}</td>
                                    <td>{{$item->alumnocarreras->first()->ciclo}}</td>
                                    <td>{{$item->categorias->first()->nombre. ' - '.$item->categorias->first()->observacion}}</td>
                                    <td class="row justify-content-center" style="padding: 0">
                                        <a type="button" href="{{route('indexAsistenciaAlumnoPadre', [$item->id])}}" class="btn btn-outline-primary" style="margin-right: 5px">Asistencias</a>
                                        <a type="button" href="#" class="btn btn-outline-secondary" style="margin-left: 5px">Notas</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="url_get_card" value="{{route('showAlumnosCard',[''])}}/">
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })
    </script>
@endsection
