@extends('IntranetPadre.Layouts.main_padres')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Lista de Alumnos</h5>
                    <div class="header-elements">

                    </div>
                </div>

                <div class="card-body">
                    Esta es una pantalla para a sus hijos ("Si tuviese más de dos hijos contactese con la Administración
                    para el cambio de categoría")
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_alumnos">
                            <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>Nivel</th>
                                <th>Grado</th>
                                <th>Categoria descuento</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hijos as $item)
                                <tr>
                                    <td>{{$item->persona->paterno.' '.$item->persona->materno.', '.$item->persona->nombres}}</td>
                                    <td>{{$item->carreras->first()->nombre??''}}</td>
                                    <td>{{$item->alumnocarreras->first()->ciclo??''}}</td>
                                    <td>{{$item->categorias->where('estado',1)->first()->nombre.' - '.$item->categorias->where('estado',1)->first()->observacion}}</td>
                                    <td>
                                        <button class="btn btn-warning" type="button"
                                                onclick="getDataStudent({{$item->id}})">Editar datos
                                        </button>
                                        {{--                                        <a type="button" class="btn btn-info" target="_blank"--}}
                                        {{--                                           href="{{route('terminos',['alumno_id'=>$item->id,'padre_id'=>Auth::user()->persona->padre->id,'carrera_id'=>$item->carreras->first()->id,'grado'=>$item->alumnocarreras->first()->ciclo])}}"><i class="icon-books"></i>Contrato</a>--}}
                                        <a type="button" class="btn btn-info"
                                           href="{{route('PadreCrearMatrícula',['alumno_id'=>$item->id])}}"><i
                                                class="icon-books"></i>Matrícula</a>
                                        <a type="button" href="{{route('vercronogram',['alumno_id'=>$item->id])}}"
                                           class="btn btn-outline-primary"><i class="icon-calendar"></i>Ver
                                            Cronograma</a>
                                        @if($item->matricula->last())
                                            <a type="button" target="_blank"
                                               href="{{route('matriculas.RptMatri',['alumno_id'=>$item->id])}}"
                                               class="btn btn-outline-primary"><i class="icon-stack-down"></i>ver Ficha</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="url_get_card" value="{{route('showAlumnosCard',[''])}}/">
        <input type="hidden" id="url_distritos_prov" value="{{route('distrito_prov', [''])}}/">
        <input type="hidden" id="url_provincias_dep" value="{{route('provincia_dep', [''])}}/">
    </div>
@endsection
@section('modals')
    <div id="modal_editStudent" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header pb-3">
                    <h5 class="modal-title">Actualizacion de datos de Estudiante</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <form action="{{route('updateOnlyAlumno')}}" method="POST" id="form_update_only">
                    @csrf
                <div class="modal-body py-0">
                    <div class="row form-group">
                        <input type="hidden" name="alumno_id" id="alumno_id">
                        <div class="col-12 col-md-3">
                            <label for="dniStudent" class="col-12">DNI:</label>
                            <input type="text" id="dniStudent" name="dniStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="paternoStudent" class="col-12">Apellido Paterno:</label>
                            <input type="text" id="paternoStudent" name="paternoStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="maternoStudent" class="col-12">Apellido Materno:</label>
                            <input type="text" id="maternoStudent" name="maternoStudent" class="form-control">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="nombresStudent" class="col-12">Nombres:</label>
                            <input type="text" id="nombresStudent" name="nombresStudent" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="ciclo" style="font-weight: bold; margin: 0">Sexo: (<label
                                    for="" style="color: red; margin: 0">*</label>)</label></label></label>
                            <select name="sexo" id="sexo" class="form-control">
                                <option>Seleccione Sexo</option>
                                <option value="F"> Femenino</option>
                                <option value="M"> Masculino</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Fecha de Nacimiento: (<label
                                    for="" style="color: red; margin: 0">*</label>)</label></label></label>
                            <input class="form-control" type="date" name="fecha_nacimiento"
                                   id="fecha_nacimiento"
                            >
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Teléfono:</label>
                            <input type="text" name="telefono" id="telefono" class="form-control"
                                   placeholder="Teléfono...">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Email Personal:</label>
                            <input type="text" name="email_personal" id="email_personal"
                                   class="form-control" placeholder="Email...">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Dirección:</label>
                            <input type="text" name="direccion" id="direccion" class="form-control"
                                   placeholder="Dirección">
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                    (<label
                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                <select class="form-control" id="departamento" name="departamento_id">
                                    <option>Seleccione un Departamento</option>
                                    @foreach($departamentos as $departamento)
                                        <option
                                            value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                    (<label
                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                <select class="form-control" id="provincia" name="provincia_id">
                                    <option selected>Seleccione una Provincia</option>
                                    @foreach($provincias as $provincia)
                                        <option
                                            value="{{$provincia->id}}">{{$provincia->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                        for="" style="color: red; margin: 0">*</label>)</label></label>
                                <select class="form-control" id="distrito" name="distrito_id">
                                    <option value="" selected>Seleccione un Distrito</option>
                                    @foreach($distritos as $distrito)
                                        <option
                                            value="{{$distrito->id}}">{{$distrito->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <label style="font-weight: bold; margin: 0">N° de Hermanos:</label>
                            <input type="number" name="num_hermanos" id="num_hermanos" class="form-control"
                                   placeholder="Hermanos...">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Lugar que Ocupa: </label>
                            <input type="number" name="num_ocupa" id="num_ocupa" class="form-control"
                                   placeholder="Lugar que ocupa...">
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Religion: </label>
                            <input type="text" name="religion" id="religion" class="form-control"
                                   placeholder="Lugar que ocupa...">
                        </div>
                        <div class="col-md-3">
                            <label for="ciclo" style="font-weight: bold; margin: 0">Grupo Sanguinio: (<label
                                    for="" style="color: red; margin: 0">*</label>)</label></label></label>
                            <select name="gru_sanginio" id="gru_sanginio" class="form-control">
                                <option value="O+"> O+</option>
                                <option value="A+"> A+</option>
                                <option value="A-"> A-</option>
                                <option value="B+"> B+</option>
                                <option value="B-"> B-</option>
                                <option value="AB+"> AB</option>
                                <option value="AB-"> AB</option>
                                <option value="O-"> O-</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Parto: </label>
                            <select name="parto" id="parto" class="form-control">
                                <option value="1"> Normal</option>
                                <option value="2"> Cesárea</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Enfermedades:</label>
                            <textarea type="text" name="enfermedades" id="enfermedades" class="form-control"
                                      placeholder="Enfermedades..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>
                        <br>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Alergias:</label>
                            <textarea type="text" name="alergias" id="alergias" class="form-control"
                                      placeholder="Alergias..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Accidentes Graves:</label>
                            <textarea type="text" name="accidentes" id="accidentes" class="form-control"
                                      placeholder="Accidentes..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Exp. Traumaticas:</label>
                            <textarea type="text" name="traumas" id="traumas" class="form-control"
                                      placeholder="Traumas..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Lim. Fisicas</label>
                            <textarea type="text" name="lim_fisicas" id="lim_fisicas" class="form-control"
                                      placeholder="Fiscas..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Peso:</label>
                            <input type="text" name="peso" id="peso" class="form-control"
                                   placeholder="Peso...">
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Talla:</label>
                            <input type="text" name="talla" id="talla" class="form-control"
                                   placeholder="Talla...">
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Vacunas:</label>
                            <textarea type="text" name="vacunas" id="vacunas" class="form-control"
                                      placeholder="Vacunas..."
                                      style="margin-top: 0px; margin-bottom: 0px; height: 38px"></textarea>
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Tipo de Seguro:</label>
                            <select name="t_seguro" id="t_seguro" class="form-control">
                                <option value="">Ninguno</option>
                                <option value="1"> SIS</option>
                                <option value="2"> EsSalud</option>
                                <option value="3"> Rimac</option>
                                <option value="4"> Mapfre</option>
                                <option value="5"> Sanitas</option>
                                <option value="6"> Pacifico</option>
                            </select>
                        </div>
                        <br>
                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">N° Seguro:</label>
                            <input type="text" name="n_seguro" id="n_seguro" class="form-control"
                                   placeholder="N° de Seguro...">
                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: bold; margin: 0">Teléfono Emergencia:</label>
                            <input type="text" name="telefonoe" id="telefonoe" class="form-control"
                                   placeholder="Teléfono...">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12" style="text-align: center">
                            <button type="submit" class="btn btn-info" id="addStudent">Guardar datos
                            </button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('assets/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#table_alumnos').DataTable({
            "language": {
                "url": "{{asset('assets/DataTables/Spanish.json')}}"
            }
        })

        function getDataStudent(studentId) {
            $.ajax({
                url: '/Padres/alumnoEdit/' + studentId,
                type: 'GET',
                datatype: 'json',
                success: function (data) {
                    // console.log(data)
                    $('#alumno_id').val(data.id)
                    $('#dniStudent').val(data.persona.DNI)
                    $('#paternoStudent').val(data.persona.paterno)
                    $('#maternoStudent').val(data.persona.materno)
                    $('#nombresStudent').val(data.persona.nombres)
                    $('#sexo option[value='+data.persona.sexo+']').prop("selected",true);
                    $('#fecha_nacimiento').val(data.persona.fecha_nacimiento)
                    $('#telefono').val(data.persona.telefono)
                    $('#email_personal').val(data.persona.email_personal)
                    $('#direccion').val(data.persona.direccion)
                    $('#departamento option[value='+data.persona.distrito.provincia.departamento_id+']').prop("selected",true);
                    $('#provincia option[value='+data.persona.distrito.provincia_id+']').prop("selected",true);
                    $('#distrito option[value='+data.persona.distrito_id+']').prop("selected",true);
                    $('#num_hermanos').val(data.num_hermanos)
                    $('#religion').val(data.religion)
                    $('#gru_sanginio option[value='+data.gru_sanginio+']').prop("selected",true);
                    $('#parto option[value='+data.parto+']').prop("selected",true);

                    $('#enfermedades').val(data.enfermedades)
                    $('#accidentes').val(data.accidentes)
                    $('#traumas').val(data.traumas)
                    $('#lim_fisicas').val(data.lim_fisicas)
                    $('#peso').val(data.peso)
                    $('#talla').val(data.talla)
                    $('#vacunas').val(data.vacunas)
                    $('#t_seguro option[value='+data.t_seguro+']').prop("selected",true);
                    $('#n_seguro').val(data.n_seguro)
                    $('#telefono').val(data.telefono)

                }
            })
            $('#modal_editStudent').modal('show')
        }

        $(function () {
            $('#departamento').change(function () {
                $('#distrito').html('');
                $('#distrito').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('#provincia').html('');
                        $('#provincia').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            $('#distrito').html('');
                            $('#distrito').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
            $('#form_update_only').submit(function (e){
                e.preventDefault()
                var url=$(this).attr('action')
                var data=$(this).serialize()
                $.ajax({
                    url:url,
                    type:'POST',
                    data:data,
                    datatype:'json',
                    success: function (data){
                        // console.log(data)
                        alert('alumno '+data.alumno.nombres+' Actualizado')
                    }
                })
            })
        });
    </script>
@endsection
