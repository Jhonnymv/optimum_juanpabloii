<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Compromiso Padres</title>

    <style>

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
            font-size: 10px;
        }

        .contenidos th {
            border: 0.5px solid black;
            border-collapse: collapse;
            font-size: 10px;
            text-align:center;
        }

        /*tr, td {*/
        /*     !*border: 1px solid black;*!*/
        /*}*/

        /*table {*/
        /*     border: 2px solid black;*/
        /*    width: 100%*/

        /*}*/

        h1 {
            font-size: 1.5rem;
        }

        h2 {
            font-size: 1.25rem;
        }

        p {
            font-family: "Times New Roman";
            text-align: justify;
            font-size: 12px;
            line-height: 180%; /*esta es la propiedad para el interlineado*/
            color: #000;
        }

        strong {
            font-size: 11px;
        }

        .padre {
            background-color: #fafafa;
            margin: 1rem;
            padding: 1rem;
            border: 2px solid #ccc;
            /* IMPORTANTE */
            text-align: center;
        }

        @page {
            header: page-header;
            footer: page-footer;
            margin-top: 25mm;
            margin-bottom: 25mm;
            margin-left: 25mm;
            margin-right: 25mm;
        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>

<body style="">

<htmlpageheader name="page-header"><br>
    <div style="text-align: left; font-weight: bold">
        <img src="{{asset('/assets/img/circular/2.png')}}" style="width: 10%;" alt="">
{{--        <img src="{{public_path()}}/assets/img/logo-juanPabloII.png" style="width: 10%;" alt="">--}}
    </div>
</htmlpageheader>

<htmlpagefooter name="page-footer">
    <div style="text-align: center; margin-bottom: 40px">
        <span style="color: rgb(141, 132, 132)">__________________________________________________________________________________</span>
        <table style="width: 100%; text-align: center">
            <tr>
                <td style="text-align: center"> <p style="font-size: 8px">Jr. El Inca N° 524 <br> Teléfono (076) 637221</p></td>
                <td style="text-align: center; margin-left: 50px"> <p style="font-size: 8px; margin-left: 50px">Cajamarca</p></td>
                <td style="text-align: center;margin-left: 50px"> <p style="font-size: 8px; margin-left: 50px">Correo: informes@juanpabloii.edu.pe<br>
                    Psje. Cumulca N° 154 Cel. (076) 637221
                    </p>
                </td>
            </tr>
        </table>
    </div>
</htmlpagefooter>

<table style="margin-top: 0px">
    <tr>
        <td style="text-align: center; width: 40%">

            <br>
            <p style="text-align: center;font-weight: bold;font-size: 16px">COMPROMISO QUE ASUME EL PADRE DE FAMILIA</p>
            <p style="text-align: center;font-weight: bold;font-size: 16px">FRENTE A LA INSTITUCIÓN EDUCATIVA</p>
            <p style="text-align: center;font-weight: bold;font-size: 16px">PARA AÑO
                ESCOLAR {{$periodo->descripcion}}</p>
        </td>
    </tr>
</table>


{{--<table>--}}
{{--    <tr>--}}
{{--        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">--}}
{{--            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO--}}
{{--        </td>--}}
{{--        <td style="text-align: left;font-size: 11px; font-family:arial;">--}}
{{--            :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">--}}
{{--            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALUMNO--}}
{{--        </td>--}}
{{--        <td style="text-align: left;font-size: 11px; font-family:arial;">--}}
{{--            :&nbsp;&nbsp;&nbsp;{{$alumno->persona->paterno.' '.$alumno->persona->materno.', '.$alumno->persona->nombres}}--}}
{{--        </td>--}}

{{--        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">--}}
{{--            --}}{{--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCION--}}
{{--            FECHA Y HORA DE CONSULTA:--}}
{{--        </td>--}}
{{--        <td style="text-align: left;font-size: 11px; font-family:arial;">--}}
{{--            --}}{{-- :&nbsp;&nbsp;&nbsp;{{$curso->seccion}} --}}
{{--            @php--}}
{{--                use Carbon\Carbon;--}}
{{--                //date_default_timezone_set('Perú')--}}
{{--                //format("F")--}}
{{--                $date = Carbon::now();--}}
{{--                //$date = $date->toDateTimeString('y-m-dTH:M:S.s');--}}
{{--                //$date = Carbon::now()->formatLocalized('%B');--}}
{{--            @endphp--}}
{{--            {{$date}}--}}
{{--        </td>--}}
{{--    </tr>--}}

{{--    <tr>--}}
{{--        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">--}}
{{--            --}}{{-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOCENTE--}}
{{--        </td>--}}
{{--        <td style="text-align: left;font-size: 11px; font-family:arial;">--}}
{{--            --}}{{-- :&nbsp;&nbsp;&nbsp;{{$docente->paterno}} {{$docente->materno}} {{$docente->nombres}}--}}
{{--        </td>--}}
{{--    </tr>--}}

{{--</table>--}}
{{--<span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>--}}

<div>
    <p style="text-align: justify;font-size: 10px">
        El Padre de Familia o Apoderado que suscribe el presente documento, de acuerdo a lo establecido en el Artículo
        3°
        de la Ley Nº 26549 de Centros Educativos Privados, concordante con el Art. 5 del Decreto Supremo N° 011-98-ED.
        <strong><U>DECLARA</U>:</strong> Haber recibido por escrito y conocer la información relacionada con el costo
        del servicio educativo, mediante comunicado de octubre y noviembre, así como el marco doctrinal, axiología y la
        fe católica que sustenta la educación que brinda la Institución Educativa y por lo tanto, sus fines
        y objetivos establecidos en su Reglamento, y expresan su compromiso de observar y respetar dicho marco
        doctrinal, axiología, ideario, estilo educativo, fe católica y Reglamento, así como respetar al personal
        que labora en la Institución, fortaleciendo el principio de autoridad ante su hijo(a).
    </p>
</div>
<div>
    <p style="text-align: justify;font-size: 10px">
        El Padre de Familia o Apoderado que suscribe el presente documento, de acuerdo a lo establecido en el Artículo
        3°
        de la Ley Nº 26549 de Centros Educativos Privados, concordante con el Art. 5 del Decreto Supremo N° 011-98-ED.
        <strong><U>DECLARA</U>:</strong> Haber recibido por escrito y conocer la información relacionada con el costo
        del servicio educativo, mediante comunicado de octubre y noviembre, así como el marco doctrinal, axiología y la
        fe católica que sustenta la educación que brinda la Institución Educativa y por lo tanto, sus fines
        y objetivos establecidos en su Reglamento, y expresan su compromiso de observar y respetar dicho marco
        doctrinal, axiología, ideario, estilo educativo, fe católica y Reglamento, así como respetar al personal
        que labora en la Institución, fortaleciendo el principio de autoridad ante su hijo(a).
    </p>
</div>

<div style="margin-left: 20px">
    <h2 style="text-align: justify;font-size: 10px"><U>SE COMPROMETE:</U></h2>
    <p style="margin-left:30px">
        a. A cumplir, puntualmente, con los pagos de pensiones de enseñanza, de acuerdo al cronograma especificado en el
        presente ya que a su vez el colegio tiene que solventar el pago de remuneraciones del personal, así como de
        servicios de agua, luz, teléfono, etc. y otros.<br>
        b. A que, si se atrasa una cuota, se presentará inmediatamente, a la oficina de la Dirección para coordinar la
        fecha de pago.<br>
        c. A velar y garantizar la asistencia y puntualidad de su hijo(a) o pupilo, a sus clases y actividades
        escolares, evitando tardanzas o faltas injustificadas.<br>
        d. A velar y garantizar que su hijo(a) o pupilo demuestre en todo momento, dentro y fuera del Plantel una
        conducta correcta, haciendo uso de un lenguaje adecuado, vistiendo correctamente el uniforme escolar y cuidando
        su buena presentación personal.<br>
        e. Aceptar el protocolo de convivencia escolar establecido en el Reglamento Interno y los acuerdos que de ello
        se derivan, por las faltas que cometa su hijo(a); para así contribuir con el orden y buena convivencia en la
        Institución.<br>
    </p>
</div>

<div style="margin-left: 20px">
    <h2 style="text-align: justify;font-size: 10px"><U>ACEPTA QUE:</U></h2>
    <p style="margin-left:30px">
        a. La Institución no está obligada a devolver gastos de matrícula u otros aportes, por retiro o traslado del
        menor en cualquier época del año.<br>
        b. Para realizar algún trámite administrativo: información de evaluaciones, traslados, constancias,
        certificados, etc. debe estar al día en el pago de pensiones.<br>
        c. Las inasistencias a clases de manera continua del estudiante, no implica descuento en la pensión.<br>
        d. Asume la responsabilidad económica en caso de que su hijo(a) participe o ejecute acciones que deteriore la
        infraestructura, mobiliario, instalaciones, equipos de laboratorio, etc.<br>
        e. Que, de acuerdo a las disposiciones legales vigentes, Ley de Instituciones Educativas Privadas, la
        Institución tiene la facultad de retener los documentos de evaluaciones correspondientes a periodos no pagados
        Art. 16 Ley N° 26549.<br>
        f. Que cuando tenga deuda de pensión debe apersonarse al plantel y firmar un acta de compromiso, indicando la
        fecha en que cumplirá con el pago atrasado; si no lo hace, se le enviará un comunicado, manifestándole que tiene
        pendiente una deuda, si al segundo comunicado no se apersona, su deuda se dará a conocer a la UGEL, Fiscalía y
        se lo inscribirá en INFOCORP.<br>
        g. La responsabilidad es de los padres, evite incomodidad en su familia, siendo puntual en sus pagos.<br>
        h. Cuando se presente en tesorería o Dirección y tenga deuda de pensiones, evite insultar y tratar mal al
        personal encargado.<br>
        i. Buscar el diálogo y el buen entendimiento para resolver cualquier problema relacionado con su menor o menores
        hijos.<br>
    </p>
</div>

<div style="margin-left: 20px">
    <h2 style="text-align: justify;font-size: 10px"><U>DECLARA: </U></h2>
    <p style="margin-left:30px">
        Conocer que la participación de los padres de familia es fundamental para el logro de los objetivos
        educacionales
        y formativos, por lo que, <strong>acepta participar activamente en el proceso educativo de su menor
            hijo(a)</strong> como: a) Actividades
        académicas, estar presente en el momento de la matrícula, en la entrega de libreta de información del
        estudiante, asistir
        a las reuniones convocadas por los Directivos, Profesores, Psicóloga y/o tutor y seguir las recomendaciones
        dadas por las
        autoridades de la Institución.<br>
        Confiar en que la Institución brindará a sus hijos(as) la educación que su familia espera; y que autoriza al
        Director(a),
        a elegir las metodologías y políticas para cumplir con los principios contenidos en su marco doctrinal y la Ley
        General de
        Educación.
    </p>
    <h2 style="text-align: center;font-size: 10px"><U>COSTOS DE LOS SERVIVIOS PARA EL 2021: </U></h2>
    <p style="margin-left:30px">
        <strong>Matrícula Inicial</strong>: S/ 240.00<br>
        <strong>Pensión Inicial</strong> : S/ 240.00 (Servicio Educativo Virtual todo el año Escolar)
        En total del costo del servicio es S/ 2,400.00 distribuido en 10 cuotas fijas al año de marzo a diciembre.<br>
        <strong>MATRÍCULA:</strong> S/. 280.00 Primaria y Secundaria (por uno o más hermanos)
    </p>
    <table class="contenidos" style="width: 80%;margin-left: 30px">
        <tr>
            <td><strong>PENSIÓN PRIMARIA</strong></td>
            <td>
                <p>S/. 300.00 presencial</p>
            </td>
            <td>
                S/. 280.00 virtual
            </td>
        </tr>
        <tr>
            <td><strong>PENSIÓN SECUNDARIA</strong></td>
            <td>
                <p>S/. 320.00 presencial</p>
            </td>
            <td>
                S/. 280.00 virtual
            </td>
        </tr>
    </table>

    <p style="margin-left:30px">
        En todos los casos el total de pensión del año, distribuido en 10 cuotas fijas de marzo a diciembre, pagadas
        cada
        fin de mes después de brindado el servicio.
    </p>

    <h2 style="text-align: center;font-size: 10px"><U>CRONOGRAMA DE PAGOS PARA EL 2021: </U></h2>
    <div class="col-md-6">
        <table class="contenidos" style="width: 80%; margin-left: 30px">
            <thead class="contenidos">
            <tr>
                <th><p>N° CUOTA</p></th>
                <th><p>MES</p></th>
                <th><p>VENCIMIENTO</p></th>
                <th><p>N° CUOTA</p></th>
                <th><p>MES</p></th>
                <th><p>VENCIMIENTO</p></th>
            </tr>
            </thead>
            <tbody class="contenidos">
            <tr>
                <td>1</td>
                <td>MARZO</td>
                <td>Hasta el 31 de marzo.</td>
                <td>6</td>
                <td>AGOSTO</td>
                <td>Hasta el 31 de agosto.</td>
            </tr>
            <tr>
                <td>2</td>
                <td>ABRIL</td>
                <td>Hasta el 30 de abril.</td>
                <td>7</td>
                <td>SEPTIEMBRE</td>
                <td>Hasta el 30 de septiembre.</td>
            </tr>
            <tr>
                <td>3</td>
                <td>MAYO</td>
                <td>Hasta el 31 de mayo.</td>
                <td>8</td>
                <td>OCTUBRE</td>
                <td>Hasta el 02 de noviembre.</td>
            </tr>
            <tr>
                <td>4</td>
                <td>JUNIO</td>
                <td>Hasta el 30 de junio.</td>
                <td>9</td>
                <td>NOVIEMBRE</td>
                <td>Hasta el 30 de noviembre</td>
            </tr>
            <tr>
                <td>5</td>
                <td>JULIO</td>
                <td>Hasta el 31 de julio.</td>
                <td>10</td>
                <td>DICIEMBRE</td>
                <td>Hasta el 17 de diciembre.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <p style="margin-left:30px">
        Yo,
        <strong>{{strtoupper($apoderado->persona->nombres.' '. $apoderado->persona->paterno.' '. $apoderado->persona->materno)}} </strong>
        Padre/madre o apoderado del(la) estudiante
        <strong>{{strtoupper($alumno->persona->nombres.' '.$alumno->persona->paterno.' '.$alumno->persona->materno)}}</strong>
        del {{$grado}} {{strtoupper($carrera->nombre)=='INICIAL'?'años':'grado'}}, nivel {{$carrera->nombre}}

        En mi calidad de responsable legal, una vez que he recibido por escrito y tomado conocimiento de las Políticas
        Institucionales,
        costos de los servicios, materiales educativos y algunas normas del Reglamento Interno, expreso, <strong>MI
            COMPROMISO DE CUMPLIR</strong> con
        lo que me corresponde como padre/madre o apoderado del(la) estudiante antes registrado(a).<br>

        <strong>DECLARO HABER RECIBIDO EN SU MOMENTO, tres (3) comunicados informativos:</strong> uno en octubre sobre
        costos de los servicios
        para el 2021, otro la primera semana de noviembre sobre finalización de año escolar y por segunda vez costos de
        los
        servicios y el 12 de noviembre sobre la propuesta pedagógica.<br>
        En este momento que firmo el presente recibo copia del mismo, parte del reglamento sobre normas y atención para
        lograr
        una buena convivencia escolar y Boletín Informativo ¿Cómo debemos tener una sana convivencia escolar?
        Preciso que soy el(la) persona responsable del pago de las pensiones de enseñanza, en caso de no cumplir
        puntualmente, acepto la mora del 0.015% a partir del primer día de vencida la fecha del cronograma.
    </p>
    <br>
    <br>
    <br>
    <br>
</div>

<div class="row">

    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
{{--                <img src="{{asset('assets/img/firmaDigital.png')}}" style="width: 14%;" alt="">--}}
                <img src="{{asset('assets/img/firmaDigital.png')}}" style="width: 14%;" alt="">
                <p style="text-align: center;margin-top: 300px">
                Magdalena Francisca Pérez Rojas<br>
                Promotora
                </p>
            </td>
            <td>
                <p style="text-align: left">
                    Firma:____________________________________________________<br>
                    Padre o Apoderado DNI N°: {{$apoderado->persona->DNI}}<br>
                    Domicilio: {{$apoderado->persona->direccion}}<br>
{{--                    Teléfono fijo: ___________________________________________<br>--}}
                    Celular:{{$apoderado->persona->telefono}}<br>
                    Centro de trabajo:{{$apoderado->centro_trabajo}}<br>
                    @php
                        $datenow=\Carbon\Carbon::now()->locale('es');
                    @endphp
                    Cajamarca, {{$datenow->format('d')}} de {{$datenow->isoFormat('MMMM')}} de {{$datenow->format('yy')}}.
                </p>
            </td>
        </tr>
    </table>
</div>



</body>
</html>
