<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> --}}
    <title>Document</title>

    <style>

        /* .td-v{
            width: 3px !important
        } */

        .rotate {
            /* writing-mode: vertical-lr; */
            /* -ms-writing-mode: tb-rl; */
            white-space: nowrap;
            transform: rotate(270deg);
            margin-right: -100%;
            margin-left: -100%;

        }

        #header,
        #footer {
            position: fixed;
            /* left: 0;
            right: 0; */

            text-align: center;
            margin-left: auto;
            margin-right: auto;

            color: #aaa;
            font-size: 0.9em;
        }

        /* #header {
            top: 0;
            border-bottom: 0.1PROMEDIO TOTAL solid #aaa;
        } */
        #footer {

            /* bottom: 0;
            border-top: 0.1PROMEDIO TOTAL solid #aaa; */

            bottom: -25px;
            background-image: url('image.png');
            background-repeat: no-repeat;
            background-position: center;
            height: 35px;

        }

        .page-number {
            margin-top: 10px;
            font-weight: bold
        }

        .page-number:before {
            content: "" counter(page);
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            /* table-layout: fixed;  */
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        .contenidos td {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        tr, td {
            /* border: 2px solid black; */
        }

        table {
            /* border: 2px solid black; */
            width: 100%

        }

        /* .page-break {
            page-break-after: always;
        } */
    </style>
</head>
<body style="">
<table style="margin-top: -20px">
    <tr>
        <td style="text-align: center; width: 15%">
            <img src="{{asset('assets/img/circular/2.png')}}" style="width: 50%;padding-bottom: -25px" alt="">
        </td><p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">INSTITUCIÓN DE EDUCATIVA PARTICULAR
            “JUAN PABLO II”</p>
        <td style="text-align: center; width: 70%">
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">DEPARTAMENTO DE
                ADMISIÓN Y REGISTRO ACADÉMICO</p>
            <p style="padding-bottom: -20px;font-size:10px;font-weight: bold; font-family:arial;">NIVEL {{strtoupper($alumno->carreras->first()->nombre)}}</p>
            <p style="padding-bottom: -15px;font-size:10px;font-weight: bold; font-family:arial;">FICHA DE MATRÍCULA</p>
        </td>

    </tr>
</table>

<span style="color: rgb(141, 132, 132); width: 10%">_______________________________________________________________________________________</span>

<table>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$periodo->descripcion}}
        </td>
    </tr>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ALUMNO
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            :&nbsp;&nbsp;&nbsp;{{$alumno->persona->paterno.' '.$alumno->persona->materno.', '.$alumno->persona->nombres}}
        </td>

        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            {{--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SECCION--}}
            FECHA Y HORA DE CONSULTA:
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{-- :&nbsp;&nbsp;&nbsp;{{$curso->seccion}} --}}
            @php
                use Carbon\Carbon;
                //date_default_timezone_set('Perú')
                //format("F")
                $date = Carbon::now();
                //$date = $date->toDateTimeString('y-m-dTH:M:S.s');
                //$date = Carbon::now()->formatLocalized('%B');
            @endphp
            {{$date}}
        </td>
    </tr>

    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            {{-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOCENTE--}}
        </td>
        <td style="text-align: left;font-size: 11px; font-family:arial;">
            {{-- :&nbsp;&nbsp;&nbsp;{{$docente->paterno}} {{$docente->materno}} {{$docente->nombres}}--}}
        </td>
    </tr>

</table>
<span style="color: rgb(141, 132, 132)">_______________________________________________________________________________________</span>
<table>
    <tr>
        <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CURSOS REGULARES:
        </td>
    </tr>
</table>
<br>
<div class="row mt-0 font-size-sm">
    <table class="table table-sm table-striped" id="t_regulares">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial;color: white">Nº</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 30%; font-family:arial;color: white">CURSO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">NIVEL</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">CRÉDITOS</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white"># HORAS</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 10%; font-family:arial;color: white">ESTADO</th>
        </tr>
        </thead>

        <tbody>
        @php
            $s_creditos = 0;
            $s_horas = 0;
        @endphp
        @foreach($detalles as $items)
            @if($items->tipo == 'R')

                @php
                    $s_creditos += $items->seccion->pe_curso->creditos;
                    $s_horas += $items->seccion->pe_curso->horas_teoria + $items->seccion->pe_curso->horas_practica;
                @endphp
                <tr>

                    <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;"> {{$loop->index + 1}}</td>
                    <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;">{{$items->seccion->pe_curso->curso->nombre}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {{$items->seccion->pe_curso->semestre}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$items->seccion->pe_curso->creditos}}
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$items->seccion->pe_curso->horas_teoria + $items->seccion->pe_curso->horas_practica}}
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$matricula->estado}}
                    </td>

                </tr>
            @endif
        @endforeach

        <tr>

            <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;">Totales:</td>
            <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;"></td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;"> {{ $s_creditos}}</td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{ $s_horas}}</td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">
            </td>

        </tr>
        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <td style="text-align: left;font-size: 11px;width: 14%; font-family:arial;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CURSOS DE SUBSANACIÓN:
            </td>
        </tr>
    </table>
    <br>
    <table class="table table-sm table-striped" id="t_sub">
        <thead>
        <tr style="color: rgb(250, 244, 244); background-color:rgb(25, 41, 85)">
            <th scope="col" style="text-align: center;font-size: 10px;width: 3%; font-family:arial;color: white">Nº</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 30%; font-family:arial;color: white">CURSO</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">NIVEL</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white">CRÉDITOS</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 5%; font-family:arial;color: white"># HORAS</th>
            <th scope="col" style="text-align: center;font-size: 10px;width: 10%; font-family:arial;color: white">ESTADO</th>
        </tr>
        </thead>
        <tbody>
        @php
            $s_creditos = 0;
            $s_horas = 0;
        @endphp
        @foreach($detalles as $itemsb)
            @if($itemsb->tipo == 'S')

                @php
                    $s_creditos += $itemsb->seccion->pe_curso->creditos;
                    $s_horas += $itemsb->seccion->pe_curso->horas_teoria + $items->seccion->pe_curso->horas_practica;
                @endphp
                <tr>

                    <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;"> {{$loop->index + 1}}</td>
                    <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;">{{$itemsb->seccion->pe_curso->curso->nombre}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {{$itemsb->seccion->pe_curso->semestre}}</td>
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$itemsb->seccion->pe_curso->creditos}}
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$itemsb->seccion->pe_curso->horas_teoria + $itemsb->seccion->pe_curso->horas_practica}}
                    <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{$matricula->estado}}
                    </td>

                </tr>
            @endif
        @endforeach

        <tr>

            <td style="text-align: center;font-size: 11px;width: 3%; font-family:arial;">Totales:</td>
            <td style="text-align: left;font-size: 11px;width: 3%; font-family:arial;"></td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;"> {{ $s_creditos}}</td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">{{ $s_horas}}</td>
            <td style="text-align: center;font-size: 11px;width: 10%; font-family:arial;">
            </td>

        </tr>
        </tbody>
    </table>
</div>
<br>

<div id="footer" class="page-break">
    <div class="page-number"></div>
</div>

</body>
</html>
