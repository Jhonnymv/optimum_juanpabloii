@extends('IntranetPadre.Layouts.main_padres')
@section('content')
    <!--Importante-->
    <!--Importante-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline justify-content-center">
                    <div>
                        <h5 style=" font-weight: bold" class="card-title text-center">Registro
                            Matrícula Alumnos</h5>

                        NOTA IMPORTANTE: Descargue todos los documentos y lea detenidamente, si está de acuerdo con los términos y condiciones puede realizar el pago
                        respectivo, luego imprima y firme el documento "CARTA DE COMPROMISO" para que lo cargue en el formulario siguiente.
                        <br>
                        <a href="/info.pdf" class="btn btn-info" target="_blank"><i class="icon-books"> </i> Costos y Servicios 2021</a>
                        <br>
                        <a href="/infob.pdf" class="btn btn-info" target="_blank"><i class="icon-books"> </i> Normas de buena convivencia 2021</a>
                        <br>
                        <a href="/infoc.pdf" class="btn btn-info" target="_blank"><i class="icon-books"> </i> Boletín informativo ¿Cómo debemos tener una sana convivencia escolar?</a>
                        <br>
                       <a type="button" class="btn btn-info" target="_blank"
                         href="{{route('terminos',['alumno_id'=>$alumno_id,'padre_id'=>$padre_id,'carrera_id'=>$alumno->alumnocarreras->last()->carrera_id,'grado'=>$alumno->first()->alumnocarreras->first()->ciclo])}}"><i class="icon-books"></i>Contrato</a>
{{--                         href="{{route('terminos',['alumno_id'=>$alumno_id,'padre_id'=>$padre_id,'carrera_id'=>$alumno->carreras->first()->id,'grado'=>$alumno->alumnocarreras->first()->ciclo])}}"><i class="icon-books"></i>Contrato</a>--}}

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12" style="display: flex; flex-direction: row; align-items: center">
                        </div>
                    </div>

                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a href="#basic-pill1" id="tab_pago" class="nav-link legitRipple active"
                               data-toggle="tab"
                               style="font-weight: bold">Registrar Pago</a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill2" class="nav-link legitRipple" id="tab_conformidad"
                               data-toggle="tab"
                               style="font-weight: bold; display: none">Constancia de Conformidad</a>
                        </li>
                    </ul>
                    Cargue el Voucher o Recibo de pago, luego hacer click en el botón siguiente para poder cargar la carta de compromiso firmada
                    <div class="tab-content">
                        <div class="tab-pane active show" id="basic-pill1">
{{--                        <div class="tab-pane active show" id="basic-pill1">--}}
                            <form id="FormRegisterPago" action="{{route('RegistrarPago')}}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="alumno_id" value="{{$alumno_id}}" class="alumno_id" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <label style="font-weight: bold; border-style: none" for=num_voucher">Número
                                            de Voucher: (<label
                                                style="color: red; margin: 0">*</label>)</label>
                                        <input type="text" name="num_voucher" id="num_voucher" class="form-control"
                                               placeholder="Número de voucher" >
                                    </div>
                                    <div class="col-md-6">
                                        <label style="font-weight: bold; border-style: none">Foto voucher: (<label
                                                for="file" style="color: red; margin: 0">*</label>)</label>

                                        <input type="file" name="file" id="file" class="form-control"
                                               placeholder="" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="button" onclick="siguiente()" class="btn btn-success">Siguiente<i
{{--                                        <button type="submit" class="btn btn-success">Siguiente<i--}}
                                                class="fa fa-arrow-circle-o-right"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="basic-pill2">
                            <form action="{{route('DocumentoStore')}}" method="post" id="FormSaveCompromiso"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="alumno_id" value="{{$alumno_id}}" class="alumno_id">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label style="font-weight: bold; border-style: none">Documento de Conformidad: (<label
                                                for="file" style="color: red; margin: 0">*</label>). <strong>En este
                                                apartado se subirá la carta de compromiso descargada anteriormente,
                                                debidamente firmada en señal de conformidad por la prestación del
                                                servicio</strong></label>
                                        <input type="file" name="file" id="file" class="form-control"
                                               placeholder="DNI..." onkeyup="valid(this);">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success" type="submit"> Grabar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /basic layout -->
    </div>
@endsection

@section('script')

    <script>
        function siguiente() {
            if ($('#num_voucher').val().length > 0) {
                if ($('#file').val().length > 0) {
                    $('#tab_pago').removeClass('active')
                    $('#basic-pill1').removeClass('active').removeClass('show')
                    $('#tab_conformidad').addClass('active').show()
                    $('#basic-pill2').addClass('active').addClass('show')
                } else alert('suba su comprobante de pago')
            } else alert('ingrese su número de voucher')
        }

        $(function () {
            $('#cronograma_id').change(function () {
                $.ajax({
                    url: '/Padres/GatCronoGramaPago/' + $(this).val(),
                    type: 'GET',
                    datatype: 'json',
                    success: function (data) {
                        $('#monto').val(data.concepto.precio)
                    }
                })
            })
            $('#FormRegisterPago').submit(function (e) {
                e.preventDefault()
                var url = $(this).attr('action')
                var formData = new FormData($('#FormRegisterPago')[0]);
                formData.append('file', $('#FormRegisterPago input[type=file]')[0].files[0]);
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: formData,
                    processData: false,
                    contentType: false,
                })
            })
            $('#FormSaveCompromiso').submit(function (e) {
                e.preventDefault()
                $('#FormRegisterPago').submit()
                var url = $(this).attr('action')
                var formData = new FormData($('#FormSaveCompromiso')[0]);
                formData.append('file', $('#FormSaveCompromiso input[type=file]')[0].files[0]);
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data === 'success') {
                            alert('La documentacíón fue registrada correctamente la secretaria validará la documentación y de no haber observaciones el estudiante será matriculado')
                            $(location).attr('href', '/Padres/padres/hijos')
                        }
                    }
                })
            })
        })
    </script>
@endsection
