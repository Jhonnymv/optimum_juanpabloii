@extends('Admisiones.Layouts.main_admision')
@section('content')
    <form action="{{route('PadreRegister')}}" method="POST" class="form-group"
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline justify-content-center">
                        <div>
                            <h5 style="padding-top: 25px; font-weight: bold" class="card-title text-center">PROCESO DE REGISTRO DE PADRES DE FAMILIA I.E. JUAN PABLO II”</h5>
                        </div>
                    </div>
                    <hr>

                    <div class="card-body">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="#basic-pill1" id="li_datos_personales" class="nav-link legitRipple active"
                                   data-toggle="tab"
                                   style="font-weight: bold">Datos Personales</a>
                            </li>
                        </ul>
                        <br>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="basic-pill1">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="DNI" style="font-weight: bold; margin: 0">DNI: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text"  minlength="8" maxlength="8"
                                                   name="dni" id="dni" class="form-control" required>
                                            @if($errors->first('dni'))
                                                <label class="badge badge-danger">{{ $errors->first('dni')}}</label>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="paterno" style="font-weight: bold; margin: 0">Apellido Paterno:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="paterno" id="paterno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="materno" style="font-weight: bold; margin: 0">Apellido Materno:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="materno" id="materno" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="nombres" style="font-weight: bold; margin: 0">Nombre: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label>
                                            <input type="text" name="nombres" id="nombres" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento" style="font-weight: bold; margin: 0">Fecha de
                                                Nacimiento: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="date" name="fecha_nacimiento" id="fecha_nacimiento"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="email_personal" style="font-weight: bold; margin: 0">Email:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="email_personal" id="email_personal"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="celular-postulante" style="font-weight: bold; margin: 0">Número
                                                de
                                                Celular: (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" minlength="9"
                                                   maxlength="9" name="telefono" id="celular-postulante"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="sexo-postulante" style="font-weight: bold; margin: 0">Sexo:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" name="sexo" id="sexo-postulante" required>
                                                <option value="">Seleccionar</option>
                                                <option value="M">Masculino</option>
                                                <option value="F">Femenino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="form-group">
                                            <label for="direccion" style="font-weight: bold; margin: 0">Dirección:
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <input type="text" name="direccion" id="direccion"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="departamento" style="font-weight: bold; margin: 0">Departamento
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="departamento" name="departamento_id">
                                                <option>Seleccione un Departamento</option>
                                                @foreach($departamentos as $departamento)
                                                    <option
                                                        value="{{$departamento->id}}">{{$departamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="provincia" style="font-weight: bold; margin: 0">Provincia
                                                (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="provincia" name="provincia_id">
                                                <option selected>Seleccione una Provincia</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="distrito" style="font-weight: bold; margin: 0">Distrito (<label
                                                    for="" style="color: red; margin: 0">*</label>)</label></label>
                                            <select class="form-control" id="distrito" name="distrito_id" required>
                                                <option value="" selected>Seleccione un Distrito</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit"
                                                class="btn btn-success"
                                                style="margin: 15px">
                                            Registrar <i class="icon-arrow-right6"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /basic layout -->

        </div>
    </form>

    <input type="hidden" id="url_distritos_prov" value="{{route('distrito_prov', [''])}}/">
    <input type="hidden" id="url_provincias_dep" value="{{route('provincia_dep', [''])}}/">
@endsection
@section('script')
    <script>
        $(function () {
            $('#departamento').change(function () {
                $('#distrito').html('');
                $('#distrito').append(new Option("Seleccione un Distrito"));
                var id_departamento = $(this).val();
                var url = $('#url_provincias_dep').val() + id_departamento;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // load_data('#provincia', data)
                        $('#provincia').html('');
                        $('#provincia').append(new Option("Seleccione una Provincia"));
                        $.each(data, function (index, value) {
                            $('#provincia').append(new Option(value.nombre, value.id));
                        })
                    }
                })
            });
            $('#provincia').change(function () {
                var id_provincia = $(this).val();
                var url = $('#url_distritos_prov').val() + id_provincia;
                $.ajax({
                        url: url,
                        method: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            // load_data('#distrito', data)
                            $('#distrito').html('');
                            $('#distrito').append(new Option("Seleccione un Distrito"));
                            $.each(data, function (index, value) {
                                $('#distrito').append(new Option(value.nombre, value.id));
                            })
                        }
                    }
                )
            });
            $('#dni').change(function (){
                if ($(this).val().length==8){
                    $.ajax({
                        url: '/searchPerson/' + $(this).val(),
                        type: 'GET',
                        datatype: 'json',
                        success: function (data) {
                            $('#paterno').val(data.apellidoPaterno)
                            $('#materno').val(data.apellidoMaterno)
                            $('#nombres').val(data.nombres)
                        }
                    })
                }
            })
        });
    </script>
@endsection
