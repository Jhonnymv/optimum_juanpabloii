<div>
    @extends('LMS.Alumno.Layout.mainLmsAlumno')
    @section('content')
        <livewire:exam-student />
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        {{--        <script src="sweetalert2.all.min.js"></script>--}}
        @yield('scriptsIn')
    @endsection
{{--    @section('script')--}}
{{--        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>--}}
{{--        <script src="sweetalert2.all.min.js"></script>--}}
{{--        @yield('scriptsIn')--}}
{{--    @endsection--}}
</div>
