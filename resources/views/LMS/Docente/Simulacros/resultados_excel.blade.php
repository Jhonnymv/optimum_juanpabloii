<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table {
            border: 2px solid black;
            width: 100%

        }

        th {
            border-bottom: 2px solid black;
            border-collapse: collapse;
            font-size: 10px;
        }

        .contenidos {
            border: 0.5px solid black;
            border-collapse: collapse;
            table-layout: fixed;
        }

        .contenidos tr {
            border: 0.5px solid black;
            border-collapse: collapse;
        }

        td {
            text-align: center;
            font-size: 9px;
            /*border: 0.5px solid black;*/
            /*border-collapse: collapse;*/
        }

        tfoot td {
            border-top: 2px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Estudiante</th>
        <th>Sección</th>
        <th>Mes</th>
        <th>Puntaje</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $loop=>$item)
        <tr>
            <td>{{$loop->index + 1 }}</td>
            <td>{{$item['alumno']}}</td>
            <td>{{$item['seccion']}}</td>
            <td>{{$item['examen']}}</td>
            <td>{{$item['nota']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>
<br>
</body>
</html>
