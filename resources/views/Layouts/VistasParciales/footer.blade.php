<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
            data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
        <span class="navbar-text">
                        &copy; 2015 - 2020. <a href="#">OptimumLab powered by <b>WA</b> &nbsp;&nbsp; rights of use assigned to I.E.P JUAN PABLO II.. </a>
        </span>
    </div>
</div>
