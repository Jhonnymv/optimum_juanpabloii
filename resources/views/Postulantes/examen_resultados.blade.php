@extends('Postulantes.Layouts.main_postulante')
@section('content')
    <div class="card" id="formulario">
        <div class="card-header">
            <h5>Examen</h5>
            @if(Session::has('msj'))
                <div class="alert alert-warning" role="alert">
                    {{Session::get('msj')}}
                </div>
            @endif
            <p><u>Observaciones:</u> Lea atentamente las alternativas planteadas a continuación y marque la alternativa,
                que según usted sea la correcta.</p>
            <p><u>Recuerde que:</u></p>
            <ul>
                <li>Debe estar en la sala de google meet creada para este propósito.</li>
                <li>Su camera de video debe estar activa.</li>
                <li>Tiene solo un intento.</li>
                <li>Todo intento de plagio u actitud sospechosa, derivará en la anulación de su examen.</li>
            </ul>
            <button id="btn_inicio" style="display: none" type="button" onclick="inicio()" class="btn btn-success">
                Iniciar examen
            </button>
        </div>
        <div id="form_examen">
            <form id="f_examen" action="{{route('examen_postulante.store')}}" method="POST">
                <input type="hidden" name="examen_id" value="{{$examen->id}}">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-12 mb-2 file-repeater">
                            @foreach( $examen->preguntas->shuffle() as  $pregunta)
                                <input type="hidden" name="pregunta[]" value="{{$pregunta->id}}">
                                <pre>{{$pregunta->pregunta}}</pre><br>
                                @if($pregunta->url_imagen)
                                    <img class="ml-4" style="height: 35em; width: 35em"
                                         src="{{asset($pregunta->url_imagen)}}">
                                @endif
                                <ol type="a">
                                    @foreach($pregunta->bpAlternativas->shuffle() as $alternativa)
                                        <li><input type="radio" name="alternativa[{{$pregunta->id}}]"
                                                   value="{{$alternativa->id}}">
                                            {{$alternativa->alternativa}}</li>
                                    @endforeach
                                </ol>
                            <h1>----------------------------------------------{{$pregunta->categoria->categoria}}--------------------------------------------</h1>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" onclick="enviar()" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    <input type="hidden" id="hora_inicio" value="{{$examen->fecha_hora}}">
    <input type="hidden" id="hora_fin" value="{{$fecha_fin}}">
    <input type="hidden" id="alerta" value="{{$alerta}}">
    <input type="hidden" id="alerta_2" value="{{$alerta_2}}">
@endsection
@section('script')
    <script src="{{asset('assets/js/jquery.repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form-repeater.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/sweetalert2.all.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{(asset('assets/js/moment-with-locales.js'))}}"></script>
    <script>
        moment.locale('en')

        function inicio() {
            $('#form_examen').show()
        }

        function enviar() {
            var opcion = confirm('¿desea guardar sus respuestas?');

            if (opcion) {
                $('#f_examen').submit();
            }
        }

        var interval = null
        var fin = null
        var validar = function () {
            var a = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
            var b = moment($('#hora_inicio').val()).format('YYYY-MM-DD HH:mm:ss');
            var c = moment($('#hora_fin').val()).format('YYYY-MM-DD HH:mm:ss');

            // console.log(a,b,c)
            if (a >= b && a <= c) {
                $('#btn_inicio').show()
                clearInterval(interval);
            }
        }

        var terminar = function () {
            var fa = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
            var fb = moment($('#alerta').val()).format('YYYY-MM-DD HH:mm:ss');
            var fb_2=moment($('#alerta_2').val()).format('YYYY-MM-DD HH:mm:ss');
            var fc = moment($('#hora_fin').val()).format('YYYY-MM-DD HH:mm:ss');


            if (fa === fb) {
                alert('te quedan 20 min')
            }
            else if (fa===fb_2){
                alert('te quedan 10 min')
            }
            else if (fa === fc) {
                $('#f_examen').submit();
                alert('tiempo agotado')
                $('#formulario').hide();
                clearInterval(fin);
            }

        }

        $(document).ready(function () {
            interval = setInterval(validar, 1000)
            fin = setInterval(terminar, 1000)
        })

    </script>
@endsection
