<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    // 'google' => [
    //     'client_id' => env('GOOGLE_ID'),
    //     'client_secret' => env('GOOGLE_SECRET'),
    //     'redirect' => env('GOOGLE_URL'),
    // ],
    'google' => [
        'client_id' => '395981490544-gch6vhp873qsothoq0r9eeduq1r55nqm.apps.googleusercontent.com',
        'client_secret' => 'SNmlMV2Hu8PvgNH1bdmOiuZm',
        'redirect' => '/auth/google/callback',
    ],

    'google_classroom' => [
        'client_id' => '387015856411-sgopfrsjgq1ul6d9hu6sqsf307ghits3.apps.googleusercontent.com',
        'project_id' => 'optimumlab',
        'auth_uri' => 'ttps://accounts.google.com/o/oauth2/auth',
        'token_uri' => 'https://oauth2.googleapis.com/token',
        'auth_provider_x509_cert_url' => 'https://www.googleapis.com/oauth2/v1/certs',
        'client_secret' => 'AclCyAXr6JZ-6P4YgEYmM38r',
        'redirect_uris' => ['http://localhost:8000/ver_cursos'],
        'app_name' => 'optimumlab'
    ]


    

];

