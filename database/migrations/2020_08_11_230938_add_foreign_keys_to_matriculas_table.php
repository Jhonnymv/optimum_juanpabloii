<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMatriculasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('matriculas', function(Blueprint $table)
		{
			$table->foreign('alumno_carrera_id', 'fk_matriculas_alumno_carreras1')->references('id')->on('alumno_carreras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('periodo_id', 'fk_matriculas_periodos1')->references('id')->on('periodos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('matriculas', function(Blueprint $table)
		{
			$table->dropForeign('fk_matriculas_alumno_carreras1');
			$table->dropForeign('fk_matriculas_periodos1');
		});
	}

}
