<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlumnoCarrerasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alumno_carreras', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('carrera_id')->index('fk_alumno_carreras_carreras1_idx');
			$table->integer('alumno_id')->index('fk_alumno_carreras_alumnos1_idx');
			$table->integer('periodo_id')->nullable();
			$table->integer('pe_carrera_id')->nullable();
			$table->integer('ciclo');
			$table->string('grupo')->nullable();
			$table->integer('estado')->default(1);
			$table->dateTime('fecha_ingreso')->nullable();
			$table->dateTime('fecha_egreso')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alumno_carreras');
	}

}
