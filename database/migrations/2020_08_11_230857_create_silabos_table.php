<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSilabosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('silabos', function(Blueprint $table)
		{
			$table->integer('id', true);
			// $table->text('url_document');
			$table->enum('tipo',['2019','2020','archivo']);
			$table->text('fundamentacion')->nullable();
			$table->text('proyecto_institucional')->nullable();
			$table->text('temas_transversales')->nullable();

			$table->text('enfoque_derecho_1')->nullable();
			$table->text('enfoque_derecho_2')->nullable();
			$table->text('enfoque_derecho_3')->nullable();

			$table->text('enfoque_intercultural_1')->nullable();
			$table->text('enfoque_intercultural_2')->nullable();
			$table->text('enfoque_intercultural_3')->nullable();

			$table->text('competencias')->nullable();
			$table->text('capacidades')->nullable();
			$table->text('estandar')->nullable();

			$table->text('metodologia')->nullable();
			$table->text('bibliografia')->nullable();
			$table->string('estado', 45)->nullable();

			// $table->integer('pe_curso_id')->index('fk_pe_cursos_silabos1_idx');
			$table->integer('seccion_id')->index('fk_seccion_silabos1_idx');
			// $table->integer('periodo_id')->index('fk_periodos_silabos1_idx');
            $table->string('url_silabo')->nullable();

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('silabos');
	}

}
