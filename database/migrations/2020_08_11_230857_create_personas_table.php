<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personas', function(Blueprint $table)
		{
            $table->integer('id',true);
            $table->string('paterno', 45)->nullable();
            $table->string('materno', 45)->nullable();
            $table->string('nombres', 45)->nullable();
            $table->string('DNI', 45)->nullable();
            $table->char('sexo', 1)->nullable();
            $table->string('fecha_nacimiento', 45)->nullable();
            $table->string('telefono', 45)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('email_personal', 200)->nullable();
            $table->string('direccion')->nullable();
            $table->string('urlimg', 45)->nullable();
            $table->string('estado')->nullable();
            $table->longText('img_identificacion')->nullable();
            $table->foreignId('distrito_id')->nullable()->constrained();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personas');
	}

}
