<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContenidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contenidos', function(Blueprint $table)
		{
			
			$table->foreign('unidades_id', 'fk_contenidos_unidades1')->references('id')->on('unidades')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contenidos', function(Blueprint $table)
		{
			
			$table->dropForeign('fk_contenidos_unidades1');
		});
	}

}
