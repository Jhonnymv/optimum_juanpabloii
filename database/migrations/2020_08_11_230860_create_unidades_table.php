<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unidades', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('silabo_id')->nullable()->index('fk_silabos_unidades1_idx');
			$table->foreignId('programation_id')->constrained();
			$table->foreignId('fase_id')->constrained();
			$table->string('denominacion', 255)->nullable();
			// $table->string('capacidades', 4000)->nullable();
			$table->string('unidad', 45)->nullable();
			$table->date('fecha_inicio')->nullable();
			$table->date('fecha_fin')->nullable();
			$table->string('duracion', 45)->nullable();
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unidades');
	}

}
