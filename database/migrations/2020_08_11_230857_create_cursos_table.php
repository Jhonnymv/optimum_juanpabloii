<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCursosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('carrera_id')->index('fk_cursos_carreras1_idx');
            $table->string('nombre', 250)->nullable();
            $table->text('descripcion', 65535)->nullable();
            $table->integer('estado')->nullable();
            $table->integer('preg_simulacro')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cursos');
    }

}
