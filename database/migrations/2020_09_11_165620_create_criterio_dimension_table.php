<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriterioDimensionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criterio_dimension', function (Blueprint $table) {
            $table->id();
            $table->text('criterio');
            $table->unsignedBigInteger('dimension_id');
            $table->integer('carrera_id')->nullable();
            $table->foreign('dimension_id')->references('id')->on('dimensiones');
            $table->foreign('carrera_id')->references('id')->on('carreras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterio_dimension');
    }
}
