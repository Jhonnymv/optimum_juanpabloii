<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHorarioDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('horario_detalles', function(Blueprint $table)
		{
			$table->foreign('aula_id', 'fk_horario_detalles_aulas1')->references('id')->on('aulas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('seccion_id', 'fk_horario_detalles_secciones1')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('horario_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_horario_detalles_aulas1');
			$table->dropForeign('fk_horario_detalles_secciones1');
		});
	}

}
