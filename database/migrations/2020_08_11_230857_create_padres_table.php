<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePadresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('padres', function (Blueprint $table) {
            $table->id();
            $table->integer('persona_id')->index('fk_padres_personas1_idx');
            $table->string('apoderado')->nullable();
            $table->enum('tipo',['P','M'])->default('P');
            $table->string('vive')->nullable();
            $table->string('vive_alumno')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('nivel_instruccion')->nullable();
            $table->string('religion')->nullable();
            $table->string('profesion')->nullable();
            $table->string('ocupacion')->nullable();
            $table->string('centro_trabajo')->nullable();
            $table->timestamps();
//            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('padres');
    }
}
