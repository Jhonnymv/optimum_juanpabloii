<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBpAlternativasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_alternativas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bp_pregunta_id')->constrained();
            $table->text('alternativa');
            $table->text('url_img')->nullable();
            $table->enum('valor',['1','0']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_alternativas');
    }
}
