<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListaVerificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_verificacion', function (Blueprint $table) {
            $table->id();
            $table->integer('silabo_id')->index('fk_silabos_lista_verificacion_idx');
            $table->integer('indicadores_id')->index('fk_indicadores_lista_verificacion_idx');
            $table->integer('escala');                  
            $table->text('detalles')->nullable();     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lista_verificacion');
    }
}
