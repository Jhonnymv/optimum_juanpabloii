<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPeCarrerasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pe_carreras', function(Blueprint $table)
		{
			$table->foreign('carrera_id', 'fk_pe_carreras_carreras1')->references('id')->on('carreras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('plan_estudio_id', 'fk_pe_carreras_plan_estudios1')->references('id')->on('plan_estudios')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pe_carreras', function(Blueprint $table)
		{
			$table->dropForeign('fk_pe_carreras_carreras1');
			$table->dropForeign('fk_pe_carreras_plan_estudios1');
		});
	}

}
