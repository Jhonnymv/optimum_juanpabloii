<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table)
		{
            $table->integer('id',true);
            $table->dateTime('fecha')->nullable();
            $table->string('num_operacion', 45)->nullable();
            $table->float('monto', 10, 0)->nullable();
            $table->string('estado', 45)->default('1');
            $table->enum('tipo',['voucher','boleta','subsanacion'])->default('voucher');
            $table->string('urlimg', 45)->nullable();
            $table->integer('postulacion_id')->nullable();
            $table->integer('matricula_id')->index('fk_vouchers_matriculas1_idx')->nullable();
            $table->integer('licencia_id')->index('fk_vouchers_licencias1_idx')->nullable();
            $table->timestamps();

//			$table->integer('postulacion_id')->index('fk_vouchers_postulaciones1_idx')->nullable();


		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vouchers');
	}

}
