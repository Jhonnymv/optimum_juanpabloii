<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrerrequisitosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prerrequisitos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('curso_pre_id')->index('fk_prerrequisitos_cursos2_idx');
			$table->integer('pe_curso_id')->index('fk_prerrequisitos_pe_cursos1_idx');
			$table->integer('estado')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prerrequisitos');
	}

}
