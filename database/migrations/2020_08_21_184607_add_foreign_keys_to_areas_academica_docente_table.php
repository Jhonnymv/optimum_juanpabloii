<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAreasAcademicaDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_academica_docente', function (Blueprint $table) {
            //
            $table->foreign('docente_id', 'fk_docentes_areas_academica_docente')->references('id')->on('docentes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            // $table->foreign('areas_academica_id', 'fk_areas_academicas_areas_academica_docente')->references('id')->on('areas_academicas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas_academica_docente', function (Blueprint $table) {
            //
            $table->dropForeign('fk_docentes_areas_academica_docente');
            // $table->dropForeign('fk_areas_academicas_areas_academica_docente');

        });
    }
}
