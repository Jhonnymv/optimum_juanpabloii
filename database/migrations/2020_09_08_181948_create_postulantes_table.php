<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulantes', function (Blueprint $table) {
            $table->id();
            $table->integer('persona_id');
            $table->foreignId('colegio_id')->nullable()->constrained();
            $table->foreignId('distrito_id')->constrained();
            $table->string('condicion');
            $table->string('tipo_exoneracion')->nullable();
            $table->string('sexo');
            $table->string('colegio');
            $table->date('año_culmino');
            $table->string('datos_contacto');
            $table->string('telefono_contacto');
            $table->timestamps();
            $table->foreign('persona_id')->references('id')->on('personas');
            //$table->foreign('periodo_id')->references('id')->on('periodos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulantes');
    }
}
