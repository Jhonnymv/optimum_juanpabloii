<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDesempenoCriteriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('desempeno_criterios', function (Blueprint $table) {
            
            $table->foreign('unidades_id', 'fk_desempeno_criterios_unidades')->references('id')->on('unidades')->onUpdate('NO ACTION')->onDelete('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('desempeno_criterios', function (Blueprint $table) {
            $table->dropForeign('fk_desempeno_criterios_unidades');
        });
    }
}
