<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContenidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contenidos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('unidades_id')->index('fk_contenidos_unidades1_idx');
			$table->string('nro_semana', 2)->nullable();			
			$table->string('fecha_inicio', 45)->nullable();
			$table->string('fecha_fin', 45)->nullable();

			$table->text('indicadores_desempeno')->nullable();
			$table->text('capacidades')->nullable();
			$table->text('estrategias')->nullable();
			$table->text('instrumentos')->nullable();
			$table->text('conocimientos')->nullable();
			$table->text('producto_evidencias')->nullable();
			
			$table->timestamps();
			
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contenidos');
	}

}
