<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToListaVerificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lista_verificacion', function (Blueprint $table) {
            //
            $table->foreign('silabo_id', 'fk_silabos_lista_verificacion')->references('id')->on('silabos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            // $table->foreign('indicadores_id', 'fk_indicadores_lista_verificacion')->references('id')->on('indicadores')->onUpdate('NO ACTION')->onDelete('NO ACTION');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lista_verificacion', function (Blueprint $table) {
            //
            $table->dropForeign('fk_silabos_lista_verificacion');
            // $table->dropForeign('fk_indicadores_lista_verificacion');
        });
    }
}
