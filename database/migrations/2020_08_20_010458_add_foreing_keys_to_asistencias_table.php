<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeysToAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asistencias', function (Blueprint $table) {
            $table->foreign('seccion_id', 'fk_asistencias_secciones1')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('alumno_id', 'fk_asistencias_alumnos1')->references('id')->on('alumnos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('horario_detalle_id', 'fk_asistencias_horario_detalles1')->references('id')->on('horario_detalles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asistencias', function (Blueprint $table) {
            $table->dropForeign('fk_asistencias_secciones1');
            $table->dropForeign('fk_asistencias_alumnos1');
            $table->dropForeign('fk_asistencias_horario_detalles1');

        });
    }
}
