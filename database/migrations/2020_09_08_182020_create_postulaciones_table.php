<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulaciones', function (Blueprint $table) {
            $table->id();
            $table->integer('pe_carrera_id');
            $table->integer('periodo_id');
            $table->foreignId('postulante_id')->constrained();
            $table->date('fecha_pos')->default(date('Y-m-d'));
            $table->enum('estado',['Solicitado','Por-Confirmar','Confirmado','Observado','Rechazado'])->default('Solicitado');
            $table->decimal('puntaje_mate','10','2')->default(0)->nullable();
            $table->decimal('puntaje_comu','10','2')->default(0)->nullable();
            $table->decimal('puntaje_cultura','10','2')->default(0)->nullable();
            $table->decimal('puntaje_entrevista','10','2')->default(0)->nullable();
            $table->longText('observaciones')->nullable();
            $table->timestamps();
            $table->foreign('pe_carrera_id')->references('id')->on('pe_carreras');
            $table->foreign('periodo_id')->references('id')->on('periodos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulaciones');
    }
}
