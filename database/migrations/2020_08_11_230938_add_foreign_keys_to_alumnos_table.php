<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('alumnos', function(Blueprint $table)
		{
			$table->foreign('persona_id', 'fk_alumnos_personas1')->references('id')->on('personas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('alumnos', function(Blueprint $table)
		{
			$table->dropForeign('fk_alumnos_personas1');
		});
	}

}
