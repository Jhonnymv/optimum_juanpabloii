<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarrerasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carreras', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 150)->nullable();
			$table->string('resolucion', 200)->nullable();
//			$table->string('responsable_id', 45)->nullable();
			$table->integer('estado')->nullable();
			$table->integer('areas_academica_id')->nullable()->index('fk_areas_academicas_carreras1_idx');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carreras');
	}

}
