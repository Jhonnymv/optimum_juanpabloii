<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSilabosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('silabos', function(Blueprint $table)
		{
			$table->foreign('seccion_id', 'fk_secciones_silabos1')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			// $table->foreign('periodo_id', 'fk_periodos_silabos1')->references('id')->on('periodos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('silabos', function(Blueprint $table)
		{
			$table->dropForeign('fk_secciones_silabos1');
			// $table->dropForeign('fk_periodos_silabos1');
		});
	}

}
