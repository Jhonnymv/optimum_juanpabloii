<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->decimal('nota', 10)->nullable();
			$table->string('estado', 5)->nullable();
			$table->dateTime('fecha')->nullable();
			$table->string('condicion', 5)->nullable();
			$table->string('observacion', 100)->nullable();
			$table->integer('alumno_id')->index('fk_notas_alumnos1_idx');
			$table->integer('producto_instrumento_id')->index('fk_notas_productoinstrumentos1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notas');
	}

}
