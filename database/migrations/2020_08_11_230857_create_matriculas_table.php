<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatriculasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matriculas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->integer('alumno_carrera_id')->index('fk_matriculas_alumno_carreras1_idx');
			$table->integer('periodo_id')->index('fk_matriculas_periodos1_idx');
			$table->enum('estado', array('PRE-MATRICULA','POR-PAGAR','PAGADA','POR-CONFIRMAR','OBSERVADA','CONFIRMADA','POR-PAGAR-SUBSANACION'))->nullable();
			$table->float('monto', 10, 0)->nullable();
			$table->text('observaciones', 65535)->nullable();
			$table->integer('categoria_id')->index('fk_matricula_categoria1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matriculas');
	}

}
