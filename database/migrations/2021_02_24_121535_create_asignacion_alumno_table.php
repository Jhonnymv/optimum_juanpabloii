<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsignacionAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignacion_alumno', function (Blueprint $table) {
            $table->id();
            $table->integer('alumno_id');
            $table->unsignedBigInteger('asignacion_id');
            $table->text('respuesta')->nullable();
            $table->text('doc_respuesta')->nullable();
            $table->string('nota')->nullable();
            $table->timestamps();
            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->foreign('asignacion_id')->references('id')->on('asignaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignacion_alumno');
    }
}
