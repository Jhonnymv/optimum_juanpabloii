<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPrerrequisitosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prerrequisitos', function(Blueprint $table)
		{
			$table->foreign('curso_pre_id', 'fk_prerrequisitos_cursos2')->references('id')->on('cursos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pe_curso_id', 'fk_prerrequisitos_pe_cursos1')->references('id')->on('pe_cursos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prerrequisitos', function(Blueprint $table)
		{
			$table->dropForeign('fk_prerrequisitos_cursos2');
			$table->dropForeign('fk_prerrequisitos_pe_cursos1');
		});
	}

}
