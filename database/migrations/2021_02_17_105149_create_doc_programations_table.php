<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocProgramationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_programations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('programation_id')->constrained();
            $table->text('nombre');
            $table->text('url')->nullable();
            $table->text('link')->nullable();
            $table->enum('estatus',['1','0'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_programations');
    }
}
