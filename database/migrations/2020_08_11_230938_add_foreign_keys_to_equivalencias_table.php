<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEquivalenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('equivalencias', function(Blueprint $table)
		{
			$table->foreign('curso_equiv_id', 'fk_equivalencias_cursos2')->references('id')->on('cursos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pe_curso_id', 'fk_equivalencias_pe_cursos1')->references('id')->on('pe_cursos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('equivalencias', function(Blueprint $table)
		{
			$table->dropForeign('fk_equivalencias_cursos2');
			$table->dropForeign('fk_equivalencias_pe_cursos1');
		});
	}

}
