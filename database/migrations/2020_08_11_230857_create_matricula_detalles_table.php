<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatriculaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matricula_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('matricula_id')->index('fk_matricula_detalles_matriculas1_idx');
			$table->integer('seccion_id')->index('fk_matricula_detalles_secciones1_idx')->nullable();
			$table->enum('tipo',['R','S'])->default('R')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matricula_detalles');
	}

}
