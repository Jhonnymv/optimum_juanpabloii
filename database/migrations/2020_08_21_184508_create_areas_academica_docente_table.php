<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasAcademicaDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_academica_docente', function (Blueprint $table) {
            $table->id();
            $table->integer('docente_id')->index('fk_docentes_areas_academica_docente_idx');
            $table->integer('areas_academica_id')->index('fk_areas_academicas_areas_academica_docente_idx');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->integer('estado_gestion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_academica_docente');
    }
}
