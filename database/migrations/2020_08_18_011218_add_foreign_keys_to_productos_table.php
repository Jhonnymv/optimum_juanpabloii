<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     
        Schema::table('productos', function (Blueprint $table) {
            $table->foreign('seccion_id', 'fk_productos_secciones1')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('evaluacion_categoria_id', 'fk_productos_evaluacion_categorias1')->references('id')->on('evaluacion_categorias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
		Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign('fk_productos_secciones1');
            $table->dropForeign('fk_productos_evaluacion_categorias1');
        });
	}

        
	
    
}
