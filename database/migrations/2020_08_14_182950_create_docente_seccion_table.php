<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteSeccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('docente_seccion', function (Blueprint $table) {
            $table->id();
            $table->integer('docente_id')->nullable()->index('fk_docentes_docente_seccion_idx');
            $table->integer('seccion_id')->index('fk_secciones_docente_seccion_idx');
            $table->enum('rol',['principal','practicante','investigador']);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente_seccion');
    }
}
