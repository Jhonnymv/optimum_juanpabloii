<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialesApoyoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiales_apoyo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tema_id')->constrained();
            $table->string('nombre')->nullable();
            $table->text('url_doc')->nullable();
            $table->text('link')->nullable();
            $table->string('pin')->nullable();
            $table->dateTime('startDate')->nullable();
            $table->dateTime('endDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiales_apoyo');
    }
}
