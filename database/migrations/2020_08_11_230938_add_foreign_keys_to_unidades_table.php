<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUnidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('unidades', function(Blueprint $table)
		{
			$table->foreign('silabo_id', 'fk_silabos_unidades1_idx')->references('id')->on('silabos')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('unidades', function(Blueprint $table)
		{
			$table->dropForeign('fk_silabos_unidades1_idx');
		});
	}

}
