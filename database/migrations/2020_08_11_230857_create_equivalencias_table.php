<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEquivalenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equivalencias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('curso_equiv_id')->index('fk_equivalencias_cursos2_idx');
			$table->integer('estado')->nullable();
			$table->timestamps();
			$table->integer('pe_curso_id')->index('fk_equivalencias_pe_cursos1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equivalencias');
	}

}
