<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMatriculaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('matricula_detalles', function(Blueprint $table)
		{
			$table->foreign('matricula_id', 'fk_matricula_detalles_matriculas1')->references('id')->on('matriculas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('seccion_id', 'fk_matricula_detalles_secciones1')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('matricula_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_matricula_detalles_matriculas1');
			$table->dropForeign('fk_matricula_detalles_secciones1');
		});
	}

}
