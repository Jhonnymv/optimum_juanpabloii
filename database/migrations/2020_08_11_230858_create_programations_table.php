<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programations', function (Blueprint $table) {
            $table->id();
            $table->integer('seccion_id');
//            $table->enum('topo',[''])
            $table->string('metodologia')->nullable();
            $table->text('descripcion')->nullable();
            $table->text('nombre_clase')->nullable();
            $table->text('link_clase')->nullable();
            $table->text('bibliografia')->nullable();
            $table->string('estado')->nullable();
            $table->timestamps();
            $table->foreign('seccion_id')->references('id')->on('secciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programations');
    }
}
