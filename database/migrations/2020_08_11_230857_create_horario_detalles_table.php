<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorarioDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('horario_detalles', function(Blueprint $table)
		{
			$table->integer('id',true);
//			$table->integer('horario_id');
			$table->integer('aula_id')->index('fk_horario_detalles_aulas1_idx');
			$table->string('dia', 45)->nullable();
			$table->string('hora_inicio', 45)->nullable();
			$table->string('hora_fin', 45)->nullable();
			$table->timestamps();
			$table->integer('seccion_id')->index('fk_horario_detalles_secciones1_idx');
			$table->string('turno', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('horario_detalles');
	}

}
