<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->integer('id',true);
			$table->string('remember_token')->nullable();
			$table->string('usuario', 45)->nullable();
			$table->string('password');
			$table->string('estado', 45)->nullable();
			$table->enum('tipo', array('alumno','docente','administrador','secretaria_academica','jefe_area_academica','padre'))->nullable();
			$table->integer('persona_id')->index('fk_personas_usuarios_idx');
			$table->string('google_id', 45)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
