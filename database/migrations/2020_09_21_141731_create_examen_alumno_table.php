<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamenAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examen_alumno', function (Blueprint $table) {
            $table->id();
            $table->integer('alumno_id');
            $table->unsignedBigInteger('examen_id');
            $table->decimal('nota');
            $table->timestamps();
            $table->foreign('examen_id')->references('id')->on('examenes');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examen_alumno');
    }
}
