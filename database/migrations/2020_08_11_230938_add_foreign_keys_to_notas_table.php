<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notas', function(Blueprint $table)
		{
			$table->foreign('alumno_id', 'fk_notas_alumnos1')->references('id')->on('alumnos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('producto_instrumento_id', 'fk_notas_productoinstrumentos1')->references('id')->on('producto_instrumentos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notas', function(Blueprint $table)
		{
			$table->dropForeign('fk_notas_alumnos1');
			$table->dropForeign('fk_notas_productoinstrumentos1');
		});
	}

}
