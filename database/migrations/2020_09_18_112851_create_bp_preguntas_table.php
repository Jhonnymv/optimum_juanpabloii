<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBpPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_preguntas', function (Blueprint $table) {
            $table->id();
//            $table->foreignId('bp_categoria_id')->constrained();
            $table->integer('curso_id');
            $table->text('pregunta');
            $table->string('url_imagen')->nullable();
            $table->timestamps();
            $table->foreign('curso_id')->references('id')->on('cursos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_preguntas');
    }
}
