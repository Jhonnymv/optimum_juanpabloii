<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pe_cursos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('horas_teoria', 8)->nullable();
			$table->string('horas_practica', 8)->nullable();
			$table->integer('semestre')->nullable();
			$table->string('creditos', 2)->nullable();
			$table->integer('curso_id')->index('fk_pe_cursos_cursos1_idx');
            $table->integer('pe_carrera_id')->index('fk_pe_cursos_pe_carreras1_idx');
			$table->integer('estado')->nullable();
			$table->timestamps();
//			$table->integer('plan_estudio_id')->index('fk_pe_cursos_plan_estudios1_idx');
			// $table->integer('carrera_id')->index('fk_pe_cursos_carreras1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pe_cursos');
	}

}
