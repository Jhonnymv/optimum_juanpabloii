<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesempenoCriteriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desempeno_criterios', function (Blueprint $table) {
            $table->id();
            $table->string('titulo',255);
            $table->text('descripcion');
            $table->integer('unidades_id')->index('fk_desempeno_criterios_unidades_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desempeno_criterios');
    }
}
