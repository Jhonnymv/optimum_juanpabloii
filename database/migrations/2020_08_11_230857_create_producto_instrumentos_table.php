<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_instrumentos', function (Blueprint $table) {
            $table->integer('id', true);			                      
			$table->integer('estado')->nullable();			
            $table->integer('producto_id')->index('fk_productoinstrumentos_productos1_idx');
            $table->integer('instrumento_id')->index('fk_productoinstrumentos_instrumentos1_idx');
            $table->integer('contenido_id')->index('fk_productoinstrumentos_contenidos1_idx');
            $table->timestamps();
            $table->date('fecha')->nullable();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_instrumentos');

        
    }


}
