<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->id();
            $table->integer('persona_id')->nullable();
            $table->char('ruc',11)->nullable();
            $table->text('social_reason')->nullable();
            $table->enum('tipo_doc',['01','03','07','08'])->nullable();
            $table->string('serie')->nullable();
            $table->string('numero_doc')->nullable();
            $table->string('doc_afec')->nullable();
            $table->string('num_voucher')->nullable();
            $table->integer('estado')->nullable();
            $table->string('fe_ticket')->nullable();
            $table->string('fe_cdr_name')->nullable();
            $table->decimal('fe_sub_total')->default(0);
            $table->decimal('fe_igv')->default(0);
            $table->decimal('fe_total_price')->default(0);
            $table->string('fe_estado')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('url_voucher')->nullable();
            $table->timestamps();
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
