<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeccionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('secciones', function(Blueprint $table)
		{
			$table->foreign('periodo_id', 'fk_grupos_periodos1')->references('id')->on('periodos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			// $table->foreign('docente_id', 'fk_secciones_docentes1')->references('id')->on('docentes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pe_curso_id', 'fk_secciones_pe_cursos1')->references('id')->on('pe_cursos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			// $table->foreign('silabo_id', 'fk_secciones_silabos1')->references('id')->on('silabos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('secciones', function(Blueprint $table)
		{
			$table->dropForeign('fk_grupos_periodos1');
			// $table->dropForeign('fk_secciones_docentes1');
			$table->dropForeign('fk_secciones_pe_cursos1');
			// $table->dropForeign('fk_secciones_silabos1');
		});
	}

}
