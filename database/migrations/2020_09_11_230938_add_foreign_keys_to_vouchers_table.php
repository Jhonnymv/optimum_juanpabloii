<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVouchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->foreign('matricula_id', 'fk_vouchers_matriculas1')->references('id')->on('matriculas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('licencia_id', 'fk_vouchers_licencias1')->references('id')->on('licencias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->dropForeign('fk_vouchers_matriculas1');
			$table->dropForeign('fk_vouchers_licencias1');
		});
	}

}
