<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlujosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flujos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tarea_id')->nullable()->constrained();
            $table->unsignedBigInteger('solicitud_id');
            $table->integer('revisado_por')->nullable();
            $table->date('fecha');
            $table->text('observacion')->nullable();
            $table->text('reenviado_a')->nullable();
            $table->enum('estado',['inicio','en_proceso','finalizado']);
            $table->timestamps();
            $table->foreign('solicitud_id')->references('id')->on('solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flujos');
    }
}
