<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEalDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eal_detalles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('examen_alumno_id');
            $table->integer('pregunta_id');
            $table->integer('alternativa_id');
            $table->timestamps();
            $table->foreign('examen_alumno_id')->references('id')->on('examen_alumno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eal_detalles');
    }
}
