<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->integer('id', true);
            $table->timestamps();
            $table->integer('seccion_id')->index('fk_asistencias_secciones1_idx');
            $table->integer('alumno_id')->index('fk_asistencias_alumnos1_idx');
            $table->integer('horario_detalle_id')->index('fk_asistencias_horario_detalles1_idx');
            $table->date('fecha_sesion')->nullable();
            $table->string('asistencia', 1)->nullable();
            $table->date('fecha_create')->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
}
