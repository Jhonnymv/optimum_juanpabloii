<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licencias', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('matricula_id')->index('fk_matricula_detalles_matriculas1_idx');
            $table->string('descripcion', 600)->nullable();
			$table->integer('estado')->nullable();
			$table->date('fecha_registro')->nullable();
			$table->date('fecha_cierre')->nullable();
            $table->string('resolucion', 600)->nullable();
            //duracion en ciclos hasta max de 4
            $table->integer('duracion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licencias');
    }
}
