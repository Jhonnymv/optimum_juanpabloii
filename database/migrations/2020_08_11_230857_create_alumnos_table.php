<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alumnos', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->integer('persona_id')->index('fk_alumnos_personas1_idx');
//            $table->integer('categoria_id')->index('fk_alumnos_categoria1_idx');
            $table->integer('padre_id');
            $table->integer('madre_id');
            $table->enum('nuevo',['0','1'])->default('0');
            $table->text('origin')->nullable();
			$table->string('estado', 11)->nullable();
            $table->integer('num_hermanos')->nullable();
            $table->integer('num_ocupa')->nullable();
            $table->string('religion')->nullable();
            $table->string('gru_sanginio', 45)->nullable();
            $table->integer('parto')->nullable();
            $table->string('enfermedades', 45)->nullable();
            $table->string('alergias', 45)->nullable();
            $table->string('accidentes', 45)->nullable();
            $table->string('traumas', 45)->nullable();
            $table->string('lim_fisicas', 200)->nullable();
            $table->string('peso')->nullable();
            $table->string('talla')->nullable();
            $table->string('vacunas')->nullable();
            $table->string('t_seguro')->nullable();
            $table->string('n_seguro')->nullable();
			$table->string('num_emergencia')->nullable();
			$table->string('relacion')->nullable();
			$table->string('m_tiempo')->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alumnos');
	}

}
