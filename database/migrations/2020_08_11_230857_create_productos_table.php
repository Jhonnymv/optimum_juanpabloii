<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
			$table->integer('id', true);			
			$table->string('producto', 300)->nullable();			
			$table->integer('estado')->nullable();			
			$table->date('fecha')->nullable();
			$table->date('fecha_cierre')->nullable();			
			$table->integer('seccion_id')->index('fk_productos_secciones1_idx');
			$table->integer('evaluacion_categoria_id')->index('fk_productos_evaluacion_categorias1_idx');
			$table->timestamps();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productos');

	}
}
