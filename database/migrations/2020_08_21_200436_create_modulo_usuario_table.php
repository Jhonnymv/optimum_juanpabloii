<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuloUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulo_usuario', function (Blueprint $table) {
            $table->id();
           $table->unsignedBigInteger('modulo_id');
//            $table->integer('usuario_id')->index('fk_usuarios_modulo_usuario_idx');
           $table->integer('usuario_id');
           $table->timestamps();
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
            //$table->foreign('modulo_id')->references('id')->on('modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulo_usuario');
    }
}
