<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToModuloUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modulo_usuario', function (Blueprint $table) {
            //

            // $table->foreign('modulo_id', 'fk_modulos_modulo_usuario')->references('id')->on('modulos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
//            $table->foreign('usuario_id', 'fk_usuarios_modulo_usuario')->references('id')->on('usuarios')->onUpdate('NO ACTION')->onDelete('NO ACTION');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modulo_usuario', function (Blueprint $table) {
            //
            // $table->dropForeign('fk_modulos_modulo_usuario');
//            $table->dropForeign('fk_usuarios_modulo_usuario');
        });
    }
}
