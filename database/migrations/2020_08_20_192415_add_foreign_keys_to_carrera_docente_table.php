<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCarreraDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carrera_docente', function (Blueprint $table) {
            //
            $table->foreign('docente_id', 'fk_docentes_docente_carrera')->references('id')->on('docentes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('carrera_id', 'fk_carreras_docente_carrera')->references('id')->on('carreras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carrera_docente', function (Blueprint $table) {
//          /  //
            $table->dropForeign('fk_docentes_docente_carrera');
            $table->dropForeign('fk_carreras_docente_carrera');
        });
    }
}
