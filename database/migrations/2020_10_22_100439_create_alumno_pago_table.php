<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnoPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno_pago', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pago_id')->constrained();
            $table->foreignId('cronograma_id')->nullable()->constrained();
            $table->integer('alumno_id')->nullable();
            $table->integer('cantidad')->nullable();
            $table->text('fe_descripcion')->nullable();
            $table->string('fe_unidad')->nullable();
            $table->decimal('fe_precio_und')->nullable();
            $table->decimal('fe_igv')->nullable();
            $table->decimal('fe_valor_venta')->nullable();
            $table->enum('estado',['Pendiente','Pagado','Anulado'])->default('Pendiente');
            $table->timestamps();
            $table->foreign('alumno_id')->references('id')->on('alumnos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno_pago');
    }
}
