<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAlumnoCarrerasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('alumno_carreras', function(Blueprint $table)
		{
			$table->foreign('alumno_id', 'fk_alumno_carreras_alumnos1')->references('id')->on('alumnos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('carrera_id', 'fk_alumno_carreras_carreras1')->references('id')->on('carreras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('alumno_carreras', function(Blueprint $table)
		{
			$table->dropForeign('fk_alumno_carreras_alumnos1');
			$table->dropForeign('fk_alumno_carreras_carreras1');
		});
	}

}
