<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocenteSeccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('docente_seccion', function (Blueprint $table) {
            $table->foreign('docente_id', 'fk_docentes_docente_seccion')->references('id')->on('docentes')->onUpdate('NO ACTION')->onDelete('NO ACTION');

            $table->foreign('seccion_id', 'fk_secciones_docente_seccion')->references('id')->on('secciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('docente_seccion', function (Blueprint $table) {
            $table->dropForeign('fk_docentes_docente_seccion');
            $table->dropForeign('fk_secciones_docente_seccion');
        });
    }
}
