<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluacionCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_categorias', function (Blueprint $table) {            
            
            $table->integer('id', true);
            $table->timestamps();
            $table->string('categoria',300)->nullable();            
            $table->decimal('peso',10)->nullable();
            $table->integer('estado')->nullable();
            $table->integer('min_prod')->nullable();
            $table->integer('siges_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_categorias');
    }
}
