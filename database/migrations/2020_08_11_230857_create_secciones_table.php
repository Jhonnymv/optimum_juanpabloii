<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeccionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secciones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('seccion', 20)->nullable();
			$table->string('grupo')->nullable();
			//$table->integer('cupos')->nullable();
			$table->string('google_classroom_course_id',255)->nullable();
			$table->integer('cupos')->nullable()->default('0');
			$table->integer('n_matriculados')->default('0')->nullable();
			$table->integer('periodo_id')->index('fk_grupos_periodos1_idx');
			$table->integer('pe_curso_id')->index('fk_secciones_pe_cursos1_idx');
			$table->timestamps();
			// $table->integer('docente_id')->index('fk_secciones_docentes1_idx');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secciones');
	}

}
