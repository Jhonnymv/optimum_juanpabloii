<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeCarrerasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pe_carreras', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_estudio_id')->index('fk_pe_carreras_plan_estudios1_idx');
			$table->integer('carrera_id')->index('fk_pe_carreras_carreras1_idx');
			$table->integer('estado')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pe_carreras');
	}

}
