<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentosEvaluacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrumentos_evaluacion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tema_id')->constrained();
            $table->foreignId('instrument_id')->constrained();
            $table->unsignedBigInteger('capacidad_id');
            $table->text('nombre');
            $table->text('decripcion');
            $table->date('fecha');
            $table->string('nota');
            $table->timestamps();
            $table->foreign('capacidad_id')->references('id')->on('capacidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrumentos_evaluacion');
    }
}
