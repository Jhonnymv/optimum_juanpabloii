<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProductoInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_instrumentos', function (Blueprint $table) {
            $table->foreign('producto_id', 'fk_productoinstrumentos_productos1')->references('id')->on('productos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('instrumento_id', 'fk_productoinstrumentos_instrumentos1')->references('id')->on('instrumentos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('contenido_id', 'fk_productoinstrumentos_contenidos1')->references('id')->on('contenidos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_instrumentos', function (Blueprint $table) {
            $table->dropForeign('fk_productoinstrumentos_productos1');
            $table->dropForeign('fk_productoinstrumentos_instrumentos1');
            $table->dropForeign('fk_productoinstrumentos_contenidos1');
        });
    }
}
