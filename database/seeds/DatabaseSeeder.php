<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            PersonaSeeder::class,
//            UsuarioSeeder::class,
//
//            AlumnosSeeder::class,
//            AreaAcademicaSeeder::class,
//            DocenteSeeder::class,
//            Areas_academicas_docenteSeeder::class,
//            IndicadoresSeeder::class,
//            InstrumentosSeeder::class,
//            CarreraSeeder::class,
//            CarreraDocenteSeeder::class,
//            DimensionSeeder::class,
//            CriterioDimensionSeeder::class,
//            AlumnoCarreraSeeder::class,
//            PeriodoSeeder::class,
//            PlanEstudiosSeeder::class,
//            PeCarrerasSeeder::class,
//            CursoSeeder::class,
//            Pe_cursoSeeder::class,
//            SeccionSeeder::class,
//            DocenteSeccionSeeder::class,
            DepartamentoSeeder::class,
            ProvinciaSeeder::class,
            DistritoSeeder::class,
            //BpCategoriasSeeder::class,
            ModuloSeeder::class,
//            ModuloUsuarioSeeder::class,

        ]);

    }

    public function truncateTables(array $tables)
    {
    }
}

