<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
           ['nombre' => 'Intranet CredencialesAlumno','code' => 'intranet_alumno','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Intranet Docente','code' => 'intranet_docente','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Intranet Secretaria Academica','code' => 'intranet_secretaria','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Intranet Area Academica','code' => 'intranet_area_academica','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Contabilidad','code' => 'contabilidad','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Administrador','code' => 'administrador','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Admision','code' => 'admision','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Postulante','code' => 'postulante','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Visitas','code' => 'visita','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
            ['nombre' => 'Padres','code' => 'padres','created_at' => '2020-08-27','updated_at' => '2020-08-27'],
        ];
        DB::table('modulos')->insert($data);
    }
}
