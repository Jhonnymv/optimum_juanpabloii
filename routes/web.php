<?php

//use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**=========================== ADMIN RUTAS ===========================*/

//Route::get('users/export/', 'UsuarioController@export');


Auth::routes();


Route::middleware(['auth'])->group(function () {

    Route::get('/', 'AdministratorController@module_chooser')->name('module_chooser');
    Route::get('/modulo/select', 'AdministratorController@select_module')->name('Administrador.seleccionar_modulo');


//
//    Route::get('matricular', 'PruebaController@index')->name('Matricular.index');
//    Route::post('matricular', 'PruebaController@guardar')->name('Matricular.guardar');


    /**======================================MANTENIMIENTO CARRERAS====================================== */
    /**======================================MANTENIMIENTO CARRERAS====================================== */
    /**======================================MANTENIMIENTO CARRERAS====================================== */

    Route::get('carreras', 'CarreraController@index')->name('carreras.index');
    Route::post('carreras', 'CarreraController@store')->name('carreras.store');

    Route::put('carreras/{id}', 'CarreraController@update')->name('carreras.update');
    Route::get('traercarrera', 'CarreraController@getCarrera')->name('carreras.getCarrera');

    Route::delete('carreras/remove/{id}', 'CarreraController@delete')->name('carreras.delete');

    Route::get('ejemplo', 'ControllerGeneral@index')->name('ejemplo');

    Route::get('ejemplo', 'ControllerGeneral@index')->name('ejemplo');

    Route::get('elementos', 'ControllerGeneral@elementos')->name('ejemplo.elemento');

    /**======================================MANTENIMIENTO CURSOS====================================== */
    /**======================================MANTENIMIENTO CURSOS====================================== */
    /**======================================MANTENIMIENTO CURSOS====================================== */
    Route::get('cursos', 'CursoController@index')->name('cursos.index');
    Route::post('cursos', 'CursoController@store')->name('cursos.store');
    Route::put('cursos/{id}', 'CursoController@update')->name('cursos.update');
    Route::get('traercurso', 'CursoController@getCurso')->name('cursos.getCurso');
    Route::delete('cursos/remove/{id}', 'CursoController@delete')->name('cursos.delete');
    Route::get('cursos/getcursosxcarrera', 'CursoController@getcursosxcarrera')->name('cursos.getcursosxcarrera');


    /**================================MANTENIMIENTO PLAN ESTUDIO====================================== */
    /**================================MANTENIMIENTO PLAN ESTUDIO====================================== */
    /**================================MANTENIMIENTO PLAN ESTUDIO====================================== */
    Route::get('planestudios', 'PlanEstudioController@index')->name('planestudios.index');
    Route::post('planestudios', 'PlanEstudioController@store')->name('planestudios.store');
    Route::delete('planestudios/remove/{id}', 'PlanEstudioController@delete')->name('planestudios.delete');
    Route::put('planestudios/{id}', 'PlanEstudioController@update')->name('planestudios.update');
    Route::get('traerplanestudio', 'PlanEstudioController@getPlanEstudio')->name('planestudios.getPlanEstudio');


    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    Route::get('pecarreras', 'PeCarreraController@index')->name('pecarreras.index');
    Route::post('pecarreras', 'PeCarreraController@store')->name('pecarreras.store');
    Route::delete('pecarreras/remove/{id}', 'PeCarreraController@delete')->name('pecarreras.delete');
    Route::get('pecarreras/listxcarreras/{carrera_id}', 'PeCarreraController@listxcarrera')->name('pecarrera.listxcarrera');


    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CARRERAS================================== */
    Route::get('pecarreras', 'PeCarreraController@index')->name('pecarreras.index');
    Route::post('pecarreras', 'PeCarreraController@store')->name('pecarreras.store');
    Route::delete('pecarreras/remove/{id}', 'PeCarreraController@delete')->name('pecarreras.delete');


    /**=========================MANTENIMIENTO PLAN ESTUDIO - CURSO================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CURSO================================== */
    /**=========================MANTENIMIENTO PLAN ESTUDIO - CURSO================================== */
    Route::get('pecursos', 'PeCursoController@index')->name('pecursos.index');
    Route::get('pecursos/{envidcarrera}/{envidpe}', 'PeCursoController@index')->name('pecursos.index');
    Route::post('pecursos', 'PeCursoController@store')->name('pecursos.store');
    Route::delete('pecursos/remove/{id}/{carreraid}/{peid}', 'PeCursoController@delete')->name('pecursos.delete');
    Route::get('traer_pecurso', 'PeCursoController@get_pecurso')->name('pecursos.get_pecurso');
    Route::get('pecurso/listxpecarrera/{carrera_id?}', 'PeCursoController@listxpecarrera')->name('pecursos.listxpecarrera');
    Route::get('pecurso/listxpecarrerasec/{carrera_id?}', 'PeCursoController@listxpecarrerasec')->name('pecursos.sec.listxpecarrera');

    Route::get('pecursos', 'PeCursoController@index')->name('pecursos.index');
    Route::get('pecursos/{envidcarrera}/{envidpe}', 'PeCursoController@index')->name('pecursos.index');
    Route::post('pecursos', 'PeCursoController@store')->name('pecursos.store');
    Route::delete('pecursos/remove/{id}/{carreraid}/{peid}', 'PeCursoController@delete')->name('pecursos.delete');
//    Route::get('traer_pecurso', 'PeCursoController@get_pecurso')->name('pecursos.get_pecurso');


    /**============================MANTENIMIENTO PRERREQUISITOS===================================== */
    /**============================MANTENIMIENTO PRERREQUISITOS===================================== */
    /**============================MANTENIMIENTO PRERREQUISITOS===================================== */

    Route::get('prerrequisitos', 'PrerrequisitoController@index')->name('prerrequisitos.index');
    Route::get('prerrequisitos/{envpecursoid}/{envcursoid}/{envcarreraid}/{envpeid}', 'PrerrequisitoController@index')->name('prerrequisitos.index');
    Route::post('prerrequisitos', 'PrerrequisitoController@store')->name('prerrequisitos.store');
    Route::delete('prerrequisitos/remove/{id}/{envpecursoid}/{envcursoid}/{envcarreraid}/{envpeid}', 'PrerrequisitoController@delete')->name('prerrequisitos.delete');

    /**============================MANTENIMIENTO EQUIVALENECIAS===================================== */
    /**============================MANTENIMIENTO EQUIVALENECIAS===================================== */
    /**============================MANTENIMIENTO EQUIVALENECIAS===================================== */

    Route::get('equivalencias', 'EquivalenciaController@index')->name('equivalencias.index');
    Route::get('equivalencias/{envpecursoid}/{envcursoid}/{envcarreraid}/{envpeid}', 'EquivalenciaController@index')->name('equivalencias.index');
    Route::post('equivalencias', 'EquivalenciaController@store')->name('equivalencias.store');
    Route::delete('equivalencias/remove/{id}/{envpecursoid}/{envcursoid}/{envcarreraid}/{envpeid}', 'EquivalenciaController@delete')->name('equivalencias.delete');

    /**======================================MANTENIMIENTO DOCENTES====================================== */
    /**======================================MANTENIMIENTO DOCENTES====================================== */
    /**======================================MANTENIMIENTO DOCENTES====================================== */


    //Route::get('/docentes', 'DocenteController@index');
    //Route::get('/docentes/create', 'DocenteController@create');
    //Route::resource('docentes', 'DocenteController');

    Route::get('docentes', 'DocenteController@index')->name('docentes.index');
    Route::post('docentes', 'DocenteController@store')->name('docentes.store');
    Route::delete('docentes/remove/{id}', 'DocenteController@delete')->name('docentes.delete');
    Route::get('traerdocente', 'DocenteController@getDocente')->name('docentes.getDocente');
    Route::put('docentes/{id}', 'DocenteController@update')->name('docentes.update');


    /**======================================MANTENIMIENTO ALUMNOS====================================== */
    /**======================================MANTENIMIENTO ALUMNOS====================================== */
    /**======================================MANTENIMIENTO ALUMNOS====================================== */

    Route::get('alumnos', 'AlumnoController@index')->name('alumno.index');
    Route::post('alumnos', 'AlumnoController@store')->name('alumno.store');
    Route::delete('alumnos/remove/{id}', 'AlumnoController@delete')->name('alumno.delete');
    Route::get('traeralumno', 'AlumnoController@getAlumno')->name('alumno.getAlumno');
    Route::put('alumnos/{id}', 'AlumnoController@update')->name('alumno.update');


    /**======================================MANTENIMIENTO PERIODOS ACADEMICOS====================================== */
    /**======================================MANTENIMIENTO PERIODOS ACADEMICOS====================================== */
    /**======================================MANTENIMIENTO PERIODOS ACADEMICOS====================================== */


//    Route::get('periodosacademicos', 'PeriodoController@index')->name('periodoacademico.index');
//    Route::post('periodosacademicos', 'PeriodoController@store')->name('periodoacademico.store');
//    Route::delete('periodosacademicos/remove/{id}', 'PeriodoController@delete')->name('periodoacademico.delete');
//    Route::get('traerperiodoacademico', 'PeriodoController@getPeriodoacademico')->name('periodoacademico.getPeriodoacademico');
//    Route::put('periodosacademicos/{id}', 'PeriodoController@update')->name('periodoacademico.update');


    /**======================================MANTENIMIENTO SÍLABO====================================== */
    /**======================================MANTENIMIENTO SÍLABO====================================== */
    /**======================================MANTENIMIENTO SÍLABO====================================== */

    //Route::get('silabos','SilabosController@indexb')->name('silabos.indexb');
    Route::get('SilaboDocentes', 'SilaboDocentesController@index')->name('SilaboDocentes.index');

    Route::post('SilaboDocentes', 'SilaboDocentesController@store')->name('SilaboDocentes.store');
    Route::get('silabos/{silabo_id}', 'SilabosController@index')->name('silabos.index');
    Route::get('silabosb/pdf', 'SilabosController@silabo_pdf')->name('IntranetAreaAcademica.silabos.pdf');
    // Route::get('unidades/{silabo_id}', 'UnidadesController@index')->name('unidades.index');
    // Route::get('unidades', 'UnidadesController@indexb')->name('unidades.indexb');
    Route::get('traersilabo', 'SilabosController@getSilabo')->name('silabos.getSilabo');
    Route::put('silabos/{id}', 'SilabosController@update')->name('silabos.update');
    //Route::delete('periodosacademicos/remove/{id}','PeriodoAcademicoController@delete')->name('periodoacademico.delete');
    Route::get('traerperiodoacademico', 'PeriodoController@getPeriodoacademico')->name('periodoacademico.getPeriodoacademico');
    //Route::put('periodosacademicos/{id}','PeriodoAcademicoController@update')->name('periodoacademico.update');

    /**======================================INTRANET ALUMNO====================================== */
    Route::get('/matricula_actualizar_estado', 'MatriculaController@actualizarestado')->name('matricula.actualizarestado');

    Route::get('/get_seccionesxcurso', 'SeccionController@getSeccionxCurso')->name('seccion.getSecciondexCurso');
    Route::get('/getseccXcur/', 'SeccionController@getseccXcur')->name('seccion.getseccXcur');

    /**==================================== FREE ROUTES FACTURATION ==============================*/
    Route::get('/admin/search_person/{dni}/{concepto_id?}', 'PersonaController@search_person')->name('search_person');
    Route::get('/admin/search_person_pension/{dni}', 'PersonaController@search_person_pension')->name('search_person_pension');
    Route::get('streem_rpt_factura/{pago_id}', 'PagoController@get_factura')->name('get_pdf_factura');

//    Route::get('/get_alumnoPagoPenciones/{dni}','AlumnoController@get_x_conograma')->name('get_alumno_pago_x_alumno');


    Route::get('/getgrades/{carrera_id?}', 'GradeController@getGradexcarrera')->name('getGradexcarrera');


    Route::prefix('Secretaria_Academica')->group(function () {
        Route::middleware(['verificar.secretaria_academica'])->group(function () {


            Route::get('/secretaria', function () {
                return view('SecretariaAcademica.index');
            })->name('secretaria.index');


            /**==========================REPORT BANK =========================*/
            Route::get('reportBank', 'PagoController@repotBank')->name('reportBank');

            /**==========================REPORT DEUDAS EXCEL =========================*/
            Route::get('reportDeudasExcel', 'PagoController@reportDeudasExcel')->name('reportDeudasExcel');
            Route::get('reportDeudasCarrerasExcel/{periodo_id?}/{carrera_id?}', 'PagoController@reportDeudasCarrerasExcel')->name('reportDeudasCarrerasExcel');



            Route::get('fasesPeriodo/{periodoId?}', function ($periodoId) {
                return view('SecretariaAcademica.Fases.index', ['periodoId' => $periodoId]);
            })->name('fasesPeriodo');

            /**================================= REGISTRAR PADRES DE FAMILIA ==================================**/
            Route::get('parentsRegister', 'PadreController@create')->name('registerParents');

            /**======================================= REPORTE DEUDAS ==================================**/
            Route::get('reportDeudas', function (){
                return view('SecretariaAcademica.Reportes.ReportDeudas');
            })->name('reportDeudas');

            /**=======================Report-Pagos-por mes ================*/

            Route::get('rptMonthPago', function (){
                return view('SecretariaAcademica.Reportes.rptMonthPago');
            })->name('rptMonthPago');

            Route::get('reportDeudaPdf/{periodoId?}','PagoController@reportDeudasPdf')->name('reportDeudaPdf');

            Route::get('changeSection', function (){
                return view('SecretariaAcademica.alumnos.changeSection');
            })->name('changeSection');
            /**========================================== FACTURACIÓN =================================================*/
            Route::get('/pago_matrícula', 'PagoController@Pago_matricula')->name('pago_matricula');
            Route::get('/get_sequence_num', 'SequenceController@get_sequence')->name('get_sequence');
            Route::post('/pago_matrícula/store', 'PagoController@store')->name('pago_matricula.store');
            Route::get('/search_ruc/{ruc?}', 'PeruConsultingController@search_ruc')->name('search_ruc');
            Route::get('/get_alumnoPagoPenciones/{dni}', 'AlumnoController@get_x_conograma')->name('get_alumno_pago_x_alumno');
            Route::get('/all_pagos', 'PagoController@AllPpagos')->name('SecretariaAllPagos');
            Route::get('view_facturas', function () {
                return view('SecretariaAcademica.Pagos.baja_facturas');
            })->name('baja_facturas');
            Route::post('search_factura', 'PagoController@search_factura')->name('search_factura');
            Route::post('/anular_factura', 'PagoController@anular_factura')->name('pago.anular_factura');
            Route::get('view_boletas', function () {
                return view('SecretariaAcademica.Pagos.AnularBoletas');
            })->name('AnularBoletas');
            Route::post('/searchBoleta', 'PagoController@searchBoleta')->name('searchBoleta');
            Route::post('/anularBoleta', 'PagoController@anularBoleta')->name('pago.anularBoleta');
            Route::get('notasSunat',function (){
                return view('SecretariaAcademica.Pagos.notesCreDev');
            })->name('notasSunat');

            Route::get('/validar_pre_matricula', 'MatriculaController@validprematricula')->name('matricula.prematriculas');
            Route::get('/validar_pre_matricula_sub', 'MatriculaController@validar_sub_5')->name('matricula.prematriculas_sub');
            Route::get('/validar_licencia_sub', 'LicenciaController@validar_sub_5')->name('matricula.licencias_sub');

            Route::get('vista_rpt_matriciladosxseccion', 'MatriculaController@view_rptxseccion')->name('matricula.view_rptxseccion');
            Route::get('vista_rpt_matriciladosxseccionpdf/{periodo_id?}/{nivel_id?}/{grado?}/{grupo?}', 'MatriculaController@rptxseccionpdf')->name('matricula.rptxseccionpdf');

            Route::get('vista_rpt_matriciladosxperiodo', 'MatriculaController@view_rptxperiodo')->name('matricula.view_rptxperiodo');
            Route::get('rpt_matriciladosxperiodo/{periodo_id}', 'MatriculaController@rptxperiodo')->name('matricula.rptxperiodo');
            Route::get('vista_rpt_matriciladosxespecialidad', 'MatriculaController@view_rptxespecialidad')->name('matricula.view_rptxespecialidad');
            Route::get('rpt_matriciladosxespecialidad/{periodo_id?}/{carrera_id?}', 'MatriculaController@rptxespecialidad')->name('matricula.rptxespecialidad');
            Route::get('vista_rpt_matriciladosxnivel', 'MatriculaController@view_rptxnivel')->name('matricula.view_rptxnivel');
            Route::get('rpt_matriciladosxnivel/{periodo_id?}/{carrera_id?}/{nivel?}', 'MatriculaController@rptxnivel')->name('matricula.rptxnivel');


            Route::get('/licencia_actualizar_estado', 'LicenciaController@actualizarestado')->name('licencia.actualizarestadob');

//            Route::get('/validar_pagos', 'MatriculaController@validarpago')->name('matricula.validarpago');
            Route::get('/get_vouchr/{alumno_id?}', 'PagoController@get_vouchr')->name('voucher.getvoucher');
            Route::get('/get_contrato2/{alumno_id?}', 'DocumentoController@get_contrato')->name('contrato.getcontrato');
            //Route::get('/get_contrato/{alumno_carrera_id?}', 'DocumentoController@reporte_contrato')->name('detalle_contrato.getcontrato');
            Route::get('/detallexmatricula/{matricula_id}', 'MatriculaDetalleController@getxmatricula')->name('detalle_matricula.getxmatricula');
            Route::get('/detallexmatriculan/{matricula_id}', 'MatriculaDetalleController@getxmatriculan')->name('detalle_matricula.getxmatriculan');
            Route::get('/detallexmatriculasec/{alumno_id}', 'MatriculaDetalleController@matriculasec')->name('matricula.getxmatricula_sec');
            Route::get('/detallexmatricula_sub/{matricula_id}', 'MatriculaDetalleController@getxmatricula_sub')->name('detalle_matricula.getxmatricula_sub');
            Route::get('/detallexmatricula', 'MatriculaDetalleController@calcular_pago')->name('matriculadetalle.calcular_pago');
            Route::get('/get_voucherxmatricula', 'VoucherController@getxmatricula')->name('vouchers.getboletaxmatricula');
            Route::get('/get_licencia/{licencia_id?}', 'LicenciaController@getxlicencia')->name('licencia.getxlicencia');

            Route::get('/rpt_matricula/{alumno_id?}/{matricula_id?}', 'MatriculaController@reporteb')->name('matriculas.ReporteMatricula');

            Route::get('vrp_inscritos_nivel', 'AlumnoController@getInscritosNivel')->name('getInscritosNivel');
            Route::get('rpt_inscritos_nivel/{periodo_id?}/{carrera_id}', 'AlumnoController@getInscritosNivelData')->name('getInscritosNivelData');

            Route::get('vrp_inscritos_nuevos_nivel', 'AlumnoController@getInscritosNuevosNivel')->name('getInscritosNuevosNivel');
            Route::get('rpt_inscritos_nuevos_nivel/{periodo_id?}/{carrera_id}', 'AlumnoController@getInscritosNuevosNivelData')->name('getInscritosNuevosNivelData');

            Route::get('/remove_curso', 'MatriculaDetalleController@removedetalle')->name('matriculadetalle.remove');

            Route::get('/agregar_detalle', 'MatriculaDetalleController@store')->name('matriculadetalle.storeb');

            Route::get('/periodos', 'PeriodoController@index')->name('periodos.index');
            Route::post('/periodos/store', 'PeriodoController@store')->name('periodos.store');
            Route::get('/periodo/edit/{id}', 'PeriodoController@edit')->name('periodos.edit');
            Route::post('/periodo/update/{id}', 'PeriodoController@update')->name('periodos.update');
            Route::post('/periodo/eliminar/{id}', 'PeriodoController@delete')->name('periodos.delete');
//            Route::get('/periodos','PeriodoController@index')->name('periodos.index');

            //Reportes planes de estudio
            Route::get('/rpt_planEstudios', 'PlanEstudioController@getRptPlanEstudios')->name('planEstudios.getRptPlanEstudios');
            Route::get('/rpt_planEstudiosCarrera', 'PlanEstudioController@getPlanEstCarrera')->name('PlanEstudios.getPlanEstCarrera');
            Route::get('/rpt_planEstudiosxCarrera', 'PeCursoController@list_pe_carrera')->name('PlanEstudios.list_pe_carrera');

            //Reportes Carreras por especialidad

            /** ===================================================MANTENIMIENTO Unidad Administrativa=====================================*/
            Route::get('/unidad_admministrativas', 'UnidadAdministrativaController@create')->name('unidadAdministrativa.create');
            Route::post('/unidad_admministrativas/store', 'UnidadAdministrativaController@store')->name('unidadAdministrativa.store');
            Route::get('/unidad_admnistrativos/buscar/{id}', 'UnidadAdministrativaController@getUndAdmin')->name('UnidadAdministrativaController.getUndAdmin');
            Route::post('/unidad_admnistrativa/update', 'UnidadAdministrativaController@update')->name('UnidadAdministrativaController.update');
            /** ===================================================MANTENIMIENTO TRÁMITES=====================================*/
            Route::get('/tramite_admnistrativos/{id}', 'TramiteController@getTramitesxUnidad')->name('tramiteController.getTramitesxUnidad');
            Route::post('/tramite_admnistrativos/store', 'TramiteController@store')->name('tramiteController.store');
            Route::get('/tramite_admnistrativos/buscar/{id}', 'TramiteController@getTramite')->name('tramiteController.getTramite');
            Route::post('/tramite_admnistrativos/update', 'TramiteController@update')->name('tramiteController.update');

            //Reporte economico Alumnos-Secretaria
            Route::get('/rpt_economico_alumno', 'AlumnoController@rptEconomicoCreate')->name('alumnoController.rptEconomicoCreate');


            //Carga Horiria
            Route::get('cargaHoraria', 'SeccionController@cargaHoraria')->name('SeccionController@cargaHoraria');
            Route::get('cargaHoraria/buscar', 'SeccionController@buscarCargaHoraria')->name('SeccionController@buscarCargaHoraria');


            Route::get('/tramite_admnistrativos/{id}', 'TramiteController@getTramitesxUnidad')->name('tramiteController.getTramitesxUnidad');
            Route::post('/tramite_admnistrativos/store', 'TramiteController@store')->name('tramiteController.store');
            Route::get('/tramite_admnistrativos/buscar/{id}', 'TramiteController@getTramite')->name('tramiteController.getTramite');
            Route::post('/tramite_admnistrativos/update', 'TramiteController@update')->name('tramiteController.update');

            /** ===================================================Cronograma=====================================*/

            Route::get('/cronograma', 'CronogramaController@index')->name('cronograma.index');
            Route::get('/cronograma/buscar', 'CronogramaController@show')->name('ShowCronograma');
            Route::post('/cronograma', 'CronogramaController@store')->name('storeCronograma');
            Route::get('/cronograma/pagos', 'CronogramaController@verAlumnosPagos')->name('verAlumnosPagos');
            Route::get('/cronograma/paago/alumno/{alumno_id}', 'CronogramaController@verCronograma')->name('vercronograma');
            Route::get('/ficha/{alumno_id}', 'AlumnoController@verFicha')->name('fichaAlumno');

            /** ================================ Report Parents ==================================== */
            Route::get('/getParents', 'PadreController@getParents')->name('getParents');
            Route::get('/getAlumnoByPadre', 'AlumnoController@getAlumnoByPadre')->name('getAlumnoByPadre');
            /** ================================ Report see Data Alumno ==================================== */
            Route::get('mydataAlumno', function () {
                return view('SecretariaAcademica.Reportes.SeeDataAlumno');
            })->name('mydataAlumno');

        });
    });
    Route::post('saveSinceSecretaria', 'PadreController@saveSinceSecretaria')->name('saveSinceSecretaria');
    Route::get('/agregar_detalle_matricula', 'MatriculaDetalleController@store')->name('matriculadetalle.store');


    Route::prefix('OptmunTramites')->group(function () {
        Route::get('/optimiun', function () {
            return view('OptimunTramites.index');
        });
        Route::get('tramites/list', 'SolicitudController@list')->name('solicitudes.list');
        Route::get('tramites/mis_tramites', 'SolicitudController@send_solicitudes')->name('solicitudes.enviadas');
        Route::get('/tramites/flujos', 'FlujoController@getXSolicitud')->name('solicitudes.flujoxsolicitud');

        Route::get('detalletramite/{solicitud_id?}', 'SolicitudController@get_detalle')->name('solicitud.detalle');

        Route::post('/guardar_glujo_trámite', 'FlujoController@store')->name('flujo.store');
        Route::get('/nuevaSolicitudDeTramite', 'SolicitudController@create')->name('solicitudTramite.create');
        Route::post('/saveSolucitudDeTramite', 'SolicitudController@store')->name('solicitudTramite.store');

    });


    Route::prefix('CONTABILIDAD')->group(function () {
        Route::middleware(['verificar.contabilidad'])->group(function () {
            Route::get('/Contabilidad', function () {
                return view('AreaContabilidad.index');
            })->name('contabilidad.index');
            Route::get('/validar_pagos', 'MatriculaController@validarpago')->name('matricula.validarpago');
            Route::get('/validar_pagos_lic', 'LicenciaController@validarpago_lic')->name('licencia.validarpagolic');
            Route::get('/validar_pagos_pos', 'PostulacionController@validarpago_pos')->name('postulacion.validarpago');
            Route::get('/get_voucherxmatricula/{matricula_id?}', 'VoucherController@getxmatricula')->name('vouchers.getxmatricula');
            Route::get('/get_voucherxlicencia/{licencia_id?}', 'VoucherController@getxlicencia')->name('vouchers.getxlicencia');
            Route::get('/get_voucherxpostulacion/{postulacion_id?}', 'VoucherController@getxpostulacion')->name('vouchers.getxpostulacion');
            Route::get('/licencia_actualizar_estado', 'LicenciaController@actualizarestado')->name('licencia.actualizarestado');

            //Route::put('postulante', 'PostulanteController@actualizarestado')->name('postulante.actualizarestado');

            Route::get('/postulante_actualizar_estado', 'PostulanteController@actualizarestado')->name('postulante.actualizarestado');

            // Ver Pagos Matricula
            Route::get('/ver_pagos_matricula', 'MatriculaController@Ver_Pagos_Alumnos')->name('matricula.ver_pago_matricula');
            Route::get('/ver_voucher_matricula/{matricula_id}', 'VoucherController@ver_voucher_matricula')->name('vouchers.ver_voucher');


        });
    });

    Route::prefix('Admin')->group(function () {
        Route::middleware(['verificar.administrador'])->group(function () {
            Route::get('/', function () {
                return view('Admin.index');
            })->name('admin.index');

            /** ===================================================MANTENIMIENTO USUARIOS=====================================*/
            Route::get('/usuario/create', 'UsuarioController@create')->name('usuario.create');


            Route::get('/adminUsuarios_modulo', 'ModuloController@index')->name('adminUsuarios_modulo.index');
            Route::get('/adminUsuarios_modulo/buscar/{id}', 'ModuloController@buscarUsuario')->name('adminUsuarios_modulo.buscarUsuario');
            Route::put('/adminUsuarios_modulo/update', 'ModuloController@updatemodulos')->name('adminUsuarios_modulo.updatemodulos');
            Route::post('/adminUsuarios_modulo/store', 'UsuarioController@storeAdminUsuaios')->name('adminUsuarios_modulo.storeAdminUsuaios');

            Route::get('/adminUsuarios/contraseñas', 'UsuarioController@createUsuariosContraseña')->name('createUsuariosContraseña');
            Route::get('/adminUsuarios/buscarUsuario/{dni}', 'UsuarioController@buscarUsuarioxDni')->name('buscarUsuarioxDni');
            Route::put('/adminUsuarios/contraseñas/update', 'UsuarioController@updateContraseña')->name('updateUsuariosContraseña');
            Route::post('/adminUsuarios/contraseñas/store', 'UsuarioController@storeUsuariosContraseña')->name('storeUsuariosContraseña');

            Route::get('/adminUsuarios/editarAlumno', 'AlumnoController@editAlumno')->name('editAlumno');
            Route::get('/adminUsuarios/buscarAlumno/{dni}', 'AlumnoController@buscarAlumnoxDni')->name('busAlumnoxDni');
            Route::put('/adminUsuarios/alumno/update', 'AlumnoController@updateAlumno')->name('updateAlumno');

            /**================================Administracion de Jefes de Areas==========================================**/
            Route::get('/admiAreaAcademicaDocentes', 'DocenteController@jefesAreas')->name('listarJefeArea');
            Route::get('/admiAreaAcademicaDocentes/buscar/{id}', 'DocenteController@buscarDocente')->name('buscarDocente');
            Route::put('/admiAreaAcademicaDocentes/asignar', 'DocenteController@asignarJefesAreas')->name('asignarJefeArea');/**================================APAGOS==========================================**/;
            Route::get('/admin/conceptos', 'ConceptoController@index')->name('conceptosindex');
            Route::post('/admin/conceptos/store', 'ConceptoController@store')->name('storeConcepto');
            Route::get('/admin/conceptos/edit/{id}', 'ConceptoController@edit')->name('editConceptos');
            Route::put('/admin/conceptos/pago/update', 'ConceptoController@update')->name('updateConceptos');
        });
    });

    Route::prefix('/Padres')->group(function () {
        Route::middleware(['verificar.padres'])->group(function () {
            Route::get('/padreIndex', function () {
                return view('IntranetPadre.index');
            })->name('padre.index');
            /** ================================== NUEVO REGISTRO DE ALUMNOS ======================================*/
            Route::get('newRegisterStudents', 'PadreController@newRegister')->name('newRegister');
            /** =========================EDITAR ESTUDIANTE ================================ */
            Route::get('/alumnoEdit/{id}', 'AlumnoController@edit');
            Route::post('/updateOnlyAlumno', 'AlumnoController@updateOnlyAlumno')->name('updateOnlyAlumno');


            Route::get('/padres/alumnos', 'AlumnoController@CredencialesAlumnosAdmin')->name('index_registrar_alumno_padre');
            Route::get('/padres/registraralumno', 'AlumnoController@CrearCredencialesAlumno')->name('registrar_credenciales_alumno_padre');
            Route::post('/padres/storealumno', 'AlumnoController@StoreCredencialesAlumnoPadre')->name('store_credenciales_alumno_padre');
            Route::get('/padres/hijos/', 'PadreController@showHijos')->name('showHijos');
            Route::get('/padres/hijosAreaAcademica/', 'PadreController@showHijosAcademica')->name('showHijosAcademica');
            Route::get('/padres/hijosAreaAcademicaNotas/', 'PadreController@showHijosAcademica_notas')->name('showHijosAcademicaNotas');
            Route::get('/padres/cronogramapagos/padre/{alumno_id}', 'CronogramaController@verCronograma')->name('vercronogram');
            Route::get('/crearMatrícula/{alumno_id}', 'MatriculaController@crear')->name('PadreCrearMatrícula');
            Route::get('/GatCronoGramaPago/{cronograma_id}', 'CronogramaController@GetCronograma')->name('PadreGetCronograma');
            Route::get('/padres/hijosreposteMat/{alumno_id?}', 'MatriculaController@reporteb')->name('matriculas.RptMatri');
        });
    });
    Route::post('/RegisterCompromiso', 'DocumentoController@store')->name('DocumentoStore');
    Route::post('/register_pago', 'PagoController@RegistrarPago')->name('RegistrarPago');
    Route::get('/register_pago_secretaria', 'PagoController@RegistrarPago')->name('RegistrarPagoSecretaria');
    Route::get('/padres/contrato/{alumno_id?}/{padre_id?}/{carrera_id?}/{grado?}', 'AlumnoController@terminos')->name('terminos');

    Route::prefix('/Admisiones')->group(function () {
        Route::middleware(['verificar.admisiones'])->group(function () {
            Route::get('/', function () {
                return view('Admisiones.index');
            })->name('admision.index');

            /**======================================ADMINISTRACION DE POSTULACIONES====================================== */
            Route::get('admisiones_all_postulantes', 'PostulanteController@list_all')->name('admisiones_all_postulantes');

            Route::get('admisiones_postulantes', 'PostulanteController@listPostulantes')->name('listaPostulantes.listPostulantes');
            Route::get('admisiones_postulantes_carrera', 'PostulanteController@listPostulantesCarrera')->name('listaPostulantesCarrera.listPostulantesCarrera');
            Route::get('admisiones_postulantes/detalles/{postulante_id}', 'PostulanteController@detalles')->name('detallePostulante.detalles');

            Route::get('admisiones_postulantes_exonerados', 'PostulanteController@listPostulantesExonerado')->name('listPostulantesExonerado.listPostulantes');
            Route::get('admi_post_exo_carrera', 'PostulanteController@listPostulantesExoneradoCarrera')->name('listPostulantesExoneradoCarrera.listPostulantes');

            Route::put('admisiones_validacion', 'PostulanteController@validarPostulante')->name('validacionPostulante.validarPostulante');
            Route::get('admision_resporte', 'PostulanteController@reportes')->name('postulantesReportes.reportes');


            Route::get('admision/rpt_postulantes_carrera', 'PostulanteController@rptPostulatesxCarrera_pdf')->name('rpt_postulantesCarreras.pdf');
            Route::get('admision/rpt_postulantes_carrera_exonerados', 'PostulanteController@rptPostulatesxCarreraExo_pdf')->name('rpt_postulantesCarrerasExonerados.pdf');


            /**======================================ADMINISTRACION BANCO DE PREGUNTAS====================================== */
            Route::get('BpPreguntas/', 'BpPreguntaController@create')->name('BpPregunta.create');
            Route::post('BpPreguntas/store', 'BpPreguntaController@store')->name('BpPregunta.store');
            Route::get('BpPreguntas/index', 'BpPreguntaController@index')->name('BpPreguntas.index');
            Route::get('BpPreguntas/detalles', 'BpPreguntaController@get_detalle')->name('BpPreguntas.get_detalle');
            Route::get('BpPreguntas/update', 'BpPreguntaController@selectCorrecta')->name('BpPreguntas.selectCorrecta');

            Route::get('Examen/create', 'ExamenController@create')->name('examen.create');
            Route::get('Examen/generate', 'ExamenController@generar')->name('examen.generar');
            Route::post('Examen/store', 'ExamenController@store')->name('examen.store');

            /**======================================EXAMEN DE ADMISION====================================== */
            Route::get('Examen/view_evaluar', 'ExamenController@v_index')->name('examen.v_evaluar');
            Route::post('examen/evaluar', 'ExamenController@evaluar')->name('examen.evaluar');

            /**======================================ENTREVISTA====================================== */
            Route::get('Examen/Entrevistas', 'EntrevistaPostulanteController@index')->name('entrevista_postulante.index');
            Route::get('get_auto_postulante', 'PostulanteController@auto_get_postulante')->name('auto_get_postulante');
            Route::post('Entrevista_postulante/store', 'EntrevistaPostulanteController@store')->name('entrevista_postulante.estore');
            /**======================================INGRESANTES====================================== */
            Route::get('Admision/alumno', 'AlumnoController@postulanteAlumno')->name('admisionAlumno');
            Route::get('Admision/alumno/buscar', 'AlumnoController@buscarPostulanteAlumno')->name('buscarPostulante');
            Route::post('Admision/alumno/agregar', 'AlumnoController@addPostulanteAlumno')->name('agregarPostulanteAlumno');


        });
    });

    Route::prefix('Postulante')->group(function () {
        Route::middleware(['verificar.postulante'])->group(function () {
//            Route::get('P_index',function (){
//                return view('Postulantes.index');
//            });
            /**======================================EXAMEN DE ADMISION====================================== */
            Route::get('Examen/index', 'ExamenController@index')->name('examen.index');
            Route::post('PostulanteExamen', 'ExamenPostulanteController@store')->name('examen_postulante.store');
        });
    });
    /**======================================Admision contancia para postulante====================================== */
    Route::get('admision/constancia_pdf/{id}', 'PostulanteController@constancia_pdf')->name('constancia.pdf');

    Route::prefix('IntranetVisita')->group(function () {
        Route::middleware(['verificar.visita'])->group(function () {
            Route::get('/visitas', function () {
                return view('Visitas.index');
            })->name('visitas.index');
            /**======================================Admision visitantes====================================== */
            Route::get('admision/verPostulantes', 'PostulanteController@verListPostulantes')->name('verPostulantes');
            Route::get('admision/verPostulantes/carrera', 'PostulanteController@verListPostulantesCarrera')->name('verPostulantesCarrera');
            Route::get('admision/verPostulantes/detalles/{postulante_id}', 'PostulanteController@verListPostulantesDetalle')->name('verPostulantesDetalles');
        });
    });


    Route::prefix('IntranetAlumno')->group(function () {
        Route::middleware(['verificar.alumno'])->group(function () {

            /**================ LMS ALUMNO ======================*/
            Route::get('layoutLmsAlumnos', function () {
                return view('LMS.Alumno.Layout.mainLmsAlumno');
            })->name('lmsAlumno');
            Route::view('myCoursesAlumno', 'LMS.Alumno.Courses.myCourses')->name('MyCoursesAlumno');

            Route::get('CourseAlumno/{seccionId}', function ($seccionId) {
                return view('LMS.Alumno.Courses.courseAlumno', ['seccionId' => $seccionId]);
            })->name('CourseAlumno');

            Route::get('contCourseAlumno/{seccionId?}', function ($seccionId) {
                return view('LMS.Alumno.Courses.contendCourseAlumno', ['seccionId' => $seccionId]);
            })->name('contCourseAlumno');

            Route::get('frameContents/{materialId?}/{type?}', function ($materialId = null, $type = null) {
                return view('LMS.Alumno.Courses.frame_contenidos', ['materialId' => $materialId, 'type' => $type]);
            })->name('frame_contends');

            /**================== Simulacro ====================**/
            Route::get('ExamStudent',function (){
                return view('LMS.Alumno.Simulacros.Exam');
            })->name('ExamStudent');
            Route::get('ExamStudentAnswer',function (){
                return view('LMS.Alumno.Simulacros.Exam_Answer');
            })->name('ExamStudentAnswer');

            /**================== Assignations =================**/

            Route::get('assignaciones/{seccionId}', function ($seccionId) {
                return view('LMS.Alumno.Asignaciones.view', ['seccionId' => $seccionId]);
            })->name('assignacionesCourseAlumno');

            /**==================================================**/

            Route::get('intranet_alumno', function () {
                return view('IntranetAlumno.index');
            });
            Route::get('/', 'AlumnoController@started')->name('Intranet_Alumno.index');

            Route::get('/matricula', 'MatriculaController@create')->name('matricula_create');


            Route::post('/matricula/store', 'MatriculaController@store')->name('matricula.store');

            Route::post('/confirmar_matricula', 'MatriculaController@confirmar_matricula')->name('matricula.confirmar');
//
            Route::get('/agregar_detalle_matricula', 'MatriculaDetalleController@store')->name('matriculadetalle.store');

//            Route::get('/get_seccionesxcurso', 'SeccionController@getSeccionxCurso')->name('seccion.getSecciondexCurso');

            Route::post('/registrar_pago', 'VoucherController@store')->name('voucher.store');

            Route::get('/get_pecursos_adicionales', 'PeCursoController@get_adicionales')->name('pecursos.get_adicionales');

//Escuelas
            Route::get('/matricula/escuela', 'MatriculaController@createEscuela')->name('matriculaEscuela_create');


            Route::get('/licencia', 'LicenciaController@create')->name('licencia_create');
            Route::get('/mantenimientodatos', 'PersonaController@index')->name('mantenimientodatos.index');
            Route::put('/mantenimientodatos/id', 'PersonaController@update')->name('mantenimientodatos.update');
            Route::get('/mantenimientoclave', 'UsuarioController@index')->name('mantenimientoclave.index');
            Route::put('/mantenimientoclave/id', 'UsuarioController@update')->name('mantenimientoclave.update');
            Route::post('/registrar_pago_lic', 'VoucherController@store_lic')->name('voucher.store_lic');
            Route::get('/rpt_ficha_mat/{matricula_id?}', 'MatriculaController@reporteb')->name('matriculas.ReporteMat');
            Route::get('/rpt_horario/{matricula_id?}', 'MatriculaController@RptHorario')->name('matriculas.rpt_horario');
            Route::get('/get_pecursp_carreras', 'PeCursoController@get_curso_subsanacion')->name('peCursos.get_curso_subsanacion');
            Route::get('/historial_matriculas', 'MatriculaController@my_historial')->name('matriculas.my_historial');
            Route::get('/rpt_plan_estudios/{matricula_id?}', 'MatriculaController@report_plan_studios')->name('matricula.plan_estudio');
//            Route::get('ver_notas_alumno', function () {
//                return view('IntranetAlumno.notas.index',['instrumentos'=>$instrumentos[]={}]);
//            })->name('nota.getnota');
            // reportes
            Route::get('notas', 'NotaController@index_al')->name('intranetalumno.notas.index');
            Route::get('/ver_notas_alumno/{action?}', 'NophptaController@ver_notas_alumnos')->name('nota.getnotab');
            Route::get('/ver_record_notas_alumno/{action?}', 'NotaController@ver_RecordNotas')->name('nota.getRecorNotas');
            //******************************Pagos Ver************
            Route::get('/veralumno/alumno/{alumno_id}', 'CronogramaController@verCronograma')->name('vercronogramapago');
            Route::get('nota_alumno/{periodo?}/{area?}/{bimestra?}','NotaController@ver_notas_alumnos_b')->name('intranet.nota');

        });
    });

    /**======================================INTRANET DOCENTE====================================== */

    Route::prefix('IntranetDocente')->group(function () {

        Route::middleware(['verificar.docente'])->group(function () {

            /**================ LMS DOCENTE ======================*/
            Route::get('layoutLms', function () {
                return view('LMS.Docente.Layout.mainLmsDocente');
            })->name('lmsDocente');
            Route::view('myCoursesDocente', 'LMS.Docente.Courses.myCourses')->name('MyCoursesDocente');
            Route::get('CourseDocente/{seccionId}', function ($seccionId) {
                return view('LMS.Docente.Courses.courseDocente', ['seccionId' => $seccionId]);
            })->name('CourseDocente');

            Route::get('CourseDocenteProgramation/{seccionId}', function ($seccionId = null) {
                $seccion = \App\Seccion::find($seccionId);
                return view('LMS.Docente.Programations.Programation', ['seccion' => $seccion]);
            })->name('CourseDocenteProgramation');

            Route::get('temaDocente/{unidadId}/{temaId?}', function ($unidadId, $temaId = null) {
                return view('LMS.Docente.Programations.tema', ['unidadId' => $unidadId, 'temaId' => $temaId]);
            })->name('temaDocente');

            /**================= Asignations Docent ==========================*/

            Route::get('AssignationDocent/{seccionId?}', function ($seccionId = null) {
                return view('LMS.Docente.Asignations.create', ['seccionId' => $seccionId]);
            })->name('assignationDocent');

            Route::get('showAssign/{assignId}', function ($assignId = null) {
                return view('LMS.Docente.Asignations.showAssign', ['assignId' => $assignId]);
            })->name('showAssign');


            /**=======================================================================*/
            /**==================== LISTA DE ESTUDIANTES ============================*/
            Route::get('listEstudentSection', function (){
                return view('IntranetDocente.Alumnos.listStudentSection');
            })->name('listStudentSection');

            /**=============================SIMULACROS=============================*/
            Route::get('questionRegister',function (){
                return view('LMS.Docente.Simulacros.question-register');
            })->name('questionRegister');

            Route::get('getExam',function (){
                return view('LMS.Docente.Simulacros.examen');
            })->name('getExam');

            Route::get('solExam',function (){
                return view('LMS.Docente.Simulacros.SolExam');
            })->name('solExam');


            Route::post('seccion/updateGoogleClassroomCourseId', 'SeccionController@updateGoogleClassroomCourseId')->name('IntranetDocente.seccion.updateGoogleClassroomCourseId');
            Route::get('horarios/sesionesApredizaje', 'HorarioController@sesionesApredizaje')->name('IntranetDocente.horario.sesionesApredizaje');


            Route::get('secciones/alumnos', 'SeccionController@alumnos')->name('IntranetDocente.seccion.alumnos');


            Route::post('silabos/store', 'SilabosController@store')->name('IntranetDocente.silabos.store');
            Route::post('silabos/update', 'SilabosController@update')->name('IntranetDocente.silabos.update');

            // Route::post('silabos/store', 'SilabosController@store')->name('IntranetDocente.silabos.store');

            // Route::post('silabos/update', 'SilabosController@update')->name('IntranetDocente.silabos.update');
            // Route::post('silabo/store','SilabosController@store')->name('IntranetDocente.silabos.store');


            Route::get('silabos', 'SilabosController@create')->name('IntranetDocente.silabos.create');

            Route::post('silabos/copy', 'SilabosController@copiar_silabo')->name('IntranetDocente.silabos.copiar');

            Route::get('silabos/pdf', 'SilabosController@silabo_pdf')->name('IntranetDocente.silabos.pdf');


//            Route::get('silabos', 'SilabosController@create')->name('IntranetDocente.silabos.create');
            // Route::get('silabos/copiar', 'SilabosController@copiar_silabo')->name('IntranetDocente.silabos.copiar');


            Route::get('unidades/parcial', 'UnidadesController@renderUnidadesCrud')->name('IntranetDocente.unidades.parcial');

            Route::post('unidades/store', 'UnidadesController@store')->name('IntranetDocente.unidades.store');
            Route::post('unidades/update', 'UnidadesController@update')->name('IntranetDocente.unidades.update');
            Route::get('unidades/eliminar', 'UnidadesController@destroy')->name('IntranetDocente.unidades.eliminar');


            Route::post('contenidos/store', 'ContenidoController@store')->name('IntranetDocente.contenidos.store');
            Route::post('contenidos/update', 'ContenidoController@update')->name('IntranetDocente.contenidos.update');
            Route::get('contenidos/eliminar', 'ContenidoController@destroy')->name('IntranetDocente.contenidos.eliminar');


            Route::get('criterio-desempeno/show', 'DesempenoCriterioController@show')->name('IntranetDocente.criterio-desempeno.show');
            Route::post('criterio-desempeno/store', 'DesempenoCriterioController@store')->name('IntranetDocente.criterio-desempeno.store');
            Route::post('criterio-desempeno/update', 'DesempenoCriterioController@update')->name('IntranetDocente.criterio-desempeno.update');
            Route::get('criterio-desempeno/eliminar', 'DesempenoCriterioController@destroy')->name('IntranetDocente.criterio-desempeno.eliminar');

            Route::get('criterio-desempeno/show', 'DesempenoCriterioController@show')->name('IntranetDocente.criterio-desempeno.show');
            Route::post('criterio-desempeno/store', 'DesempenoCriterioController@store')->name('IntranetDocente.criterio-desempeno.store');
            Route::post('criterio-desempeno/update', 'DesempenoCriterioController@update')->name('IntranetDocente.criterio-desempeno.update');
            Route::get('criterio-desempeno/eliminar', 'DesempenoCriterioController@destroy')->name('IntranetDocente.criterio-desempeno.eliminar');

            /**======================================  SISTEMA EVALUACIONES  ====================================== */
            Route::get('SistemaEvaluacion', 'ProductoController@index')->name('intranetdocente.productos.index');
            Route::post('SistemaEvaluacion/store', 'ProductoController@store')->name('productos.store');
            Route::get('getProductosCategoria', 'ProductoController@getProductosCategoria')->name('url_getProductosCategoria');
            Route::get('traerproducto', 'ProductoController@getProducto')->name('producto.getProducto');
            Route::put('productos/{id}', 'ProductoController@update')->name('productos.update');
            Route::delete('productos/remove/{id}', 'ProductoController@delete')->name('productos.delete');

            Route::post('SistemaEvaluacion/inststore', 'ProductoController@prodinstStore')->name('produtoInstrumento.store');
            Route::get('getInstrumentosProducto', 'ProductoController@getInstrumentosProducto')->name('url_getInstrumentosProducto');
            Route::get('traerprodinstrumento', 'ProductoController@getProdInstrumento')->name('prodInstrumento.getProdInstrumento');
            Route::put('prodInstrumentos/{id}', 'ProductoController@updateProdInstrumento')->name('prodInstrumento.update');
            Route::delete('prodInstrumentos/remove/{id}', 'ProductoController@deleteProdInstrumento')->name('prodInstrumento.delete');


            Route::get('productos/getUnidades/{seccion_id}', 'ProductoController@getUnidades')->name('url_getUnidades');
            Route::get('productos/getContenidos/{unidad_id}', 'ProductoController@getContenidos')->name('url_getContenidos');
            Route::get('productos/findContenido/{contenido_id}', 'ProductoController@findContenido')->name('url_findContenido');


            /**======================================  REGISTRO DE NOTAS  ====================================== */
            Route::get('notas', 'NotaController@index')->name('intranetdocente.notas.index');
            Route::get('get_evaluacionesxseccion', 'EvaluacionController@get_evaluacionesxseccion')->name('url_getevaluacionesxseccion');
            Route::get('get_listaAlumnosNota', 'MatriculaDetalleController@get_listaAlumnosNota')->name('url_getlistaAlumnosNota');
            Route::post('notas', 'NotaController@store')->name('notas.store');
            Route::get('get_notasAlumnos', 'NotaController@get_notasAlumnos')->name('url_getlistaNotas');

            Route::delete('notas/remove/{id?}', 'NotaController@delete')->name('notas.delete');
            Route::get('notas/reporteNI/{seccion_id?}/{prod_inst_id?}/{periodo_id?}/{carrera_id?}/{area_id?}/{cat_id?}/{prod_id?}', 'NotaController@reporteNotasInstrumneto')->name('notas.reporteNotasInstrumneto.pdf');

            Route::get('intranetdocente/notas', function () {
                return view('IntranetDocente.Nota.index');
            });


            /**======================================CONTROL ASISTENCIA====================================== */
            Route::get('Asistencia', 'AsistenciaController@index')->name('IntranetDocente.asistencia.index');


            Route::get('getSesionClase', 'AsistenciaController@getSesionClase')->name('asistencia.getSesionClase');


            Route::get('getListaAsistencia', 'AsistenciaController@getListaAsistencia')->name('asistencias.getListaAsistencia');
            Route::post('asistencia.store', 'AsistenciaController@store')->name('asistencia.store');
            Route::get('getRegistroAsistencia', 'AsistenciaController@getRegistroAsistencia')->name('asistencias.getRegistroAsistencia');
            Route::delete('asistencia/remove/{seccion_id}/{horario_id}', 'AsistenciaController@delete')->name('asistencia.delete');

            Route::get('asistencia/reporteSesion/{seccion_id?}/{horario_id?}/{fecha_sesion?}', 'AsistenciaController@reporteSesion')->name('asistencia.reporteSesion.pdf');


            /**======================================FILTRO PARCIAL====================================== */

            Route::get('CarrerasDocentePorPeriodo', 'DocenteController@carrerasDocentePorPeriodo');
            Route::get('AreasDocentePorCarrera', 'DocenteController@areasDocentePorCarrera')->name('docente.areaDocenteXCarrera');
            Route::get('SeccionesDocentePorArea', 'DocenteController@seccionesDocentePorArea');

            Route::get('VerificarSilaboCurso', 'SilabosController@verificarSilaboCurso');

            Route::get('CarrerasPorPeriodo', 'DocenteController@carrerasPorPeriodo');
            Route::get('AreasPorCarrera', 'DocenteController@areasPorCarrera');
            Route::get('SeccionesPorArea', 'DocenteController@seccionesPorArea');


            /**======================================AREAS POR DOCENTE====================================== */
            Route::get('/areas', 'DocenteController@mis_areas')->name('Idocente.areas');


            /**======================================HORARIOS POR DOCENTE====================================== */
            Route::get('/My_horario', 'DocenteController@mis_horarios')->name('IDocente.horarios');
            Route::get('/My_horario_get', 'DocenteController@get_horarios')->name('IDocente.get_mis_horarios');


            /**===============================================DIMENSIONES===============================================*/
            Route::get('/get_criterio_dimenciones', 'CriterioDimensionController@get_dimension')->name('criterioDimension.get_dimensiones');
        });

    });


    Route::prefix('IntranetAreaAcademica')->group(function () {

        // Route::middleware(['verificar.jefe_area_academica'])->group(function () {

        Route::get('silabos', 'SilabosController@index')->name('IntranetAreaAcademica.silabos.index');
        Route::get('silabos/detalle/{id}', 'SilabosController@show')->name('IntranetAreaAcademica.silabos.show');
        Route::get('silabos/publicar', 'SilabosController@publicar')->name('IntranetAreaAcademica.silabos.publicar');
        // Route::get('silabos', 'SilabosController@index');
//        Route::get('silabos/detalle/{id}', 'SilabosController@show')->name('IntranetAreaAcademica.silabos.show');
        Route::get('SeccionesRespAreaPorCarrera', 'SeccionController@seccionesRespAreaPorCarrera');


        Route::post('lista_verificacion/update', 'SilabosController@update_lista_verificacion')->name('IntranetAreaAcademica.lista_verificacion.update');
        Route::get('/silabo/estado/change', 'SilabosController@cambiar_estado')->name('silabo.cambiar_estado');

        // });

    });

    Route::get('silabos/pdf/download', 'SilabosController@silabo_pdf_download')->name('silabos.pdf.download');

    Route::get('modalContentListaVerificacion', 'SilabosController@modalContentListaVerificacion');

    Route::get('contenidos/show', 'ContenidoController@show')->name('contenidos.show');
    Route::get('unidades/show', 'UnidadesController@show')->name('unidades.show');
    Route::get('modalContentListaVerificacion', 'SilabosController@modalContentListaVerificacion');

    Route::get('contenidos/show', 'ContenidoController@show')->name('contenidos.show');
    Route::get('unidades/show', 'UnidadesController@show')->name('unidades.show');

    Route::get('GetFullSeccion', 'SeccionController@getFullSeccion');

    Route::get('ContenidosPorUnidad', 'UnidadesController@contenidosPorUnidad');

    Route::get('classroom', 'SeccionController@classroom')->name('classroom');

    Route::get('ver_cursos', 'SeccionController@ver_cursos')->name('ver_cursos');


    Route::get('CreateGoogleClassroomCourseAndTopics', 'SeccionController@createGoogleClassroomCourseAndTopics');


});


/**======================================CARGAR HORARIA====================================== */
Route::get('/cargahoraria', 'SeccionController@index')->name('JefeAreaAcedemica.cargarhoraria.index');
Route::post('/cargahoraria', 'SeccionController@store')->name('cargarhoraria.store');
Route::get('/traerseccion', 'SeccionController@getseccion')->name('secciones.getseccion');
Route::post('/cargahoraria/horario', 'SeccionController@storehorario')->name('cargarhoraria.storehorario');
Route::get('/cargarPlanesCarrera/', 'SeccionController@getCargarPlanesCarrera')->name('url_cargarPlanesCarrera');
Route::get('/cargarGrupos', 'SeccionController@getcargarGrupos')->name('url_cargarGrupos');
Route::get('/obtenerCursos', 'SeccionController@getCursosPe')->name('url_getCursosPe');
Route::get('seccion/horarios/{seccion_id}', 'SeccionController@listxseccion')->name('cargarhoraria.listxseccion');
Route::get('/remove_horario', 'HorarioController@removehorario')->name('horario.remove');
Route::put('secciones/{id}', 'SeccionController@update')->name('secciones.update');
//Route::delete('horarios/remove/{id}/{sec}', 'HorarioController@delete')->name('horarios.delete');

// Route::get('/attach_inidcadores', 'SilabosController@attach_inidcadores');
/**======================================GOOGLE AUTHENTICATION====================================== */
Route::get('google', function () {
    return view('googleAuth');
});
Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('intranetalumno/reportematricula', 'MatriculaController@reporte')->name('IntranetAlumno.matriculas.ReporteMatricula');
Route::get('intranetalumno/reportemat', 'MatriculaController@reporteb')->name('IntranetAlumno.matriculas.ReporteMat');

Route::get('download', function () {
    $pdf = PDF::loadView('IntranetAlumno.matriculas.ReporteMat');
    return $pdf->download();
})->name('download');

/**======================================POSTULACIONES====================================== */
Route::get('/colegio', 'ColegioController@getUbigeo')->name('colegio.getubigeo');
Route::get('/admisiones_optimum', 'PostulacionController@index')->name('admisiones');
Route::get('/admisiones/provincias_dep/{id_dep}', 'ProvinciaController@show')->name('provincia_dep');
Route::get('/admisiones/distritos_prov/{id_prov}', 'DistritoController@show')->name('distrito_prov');
Route::post('/admision/store', 'PostulacionController@store')->name('postulacion.store');
Route::get('/solicitud_pdf/{id?}', 'PostulacionController@download_pdf')->name('postulante_solicitud_pdf');
Route::post('/postulacion_update', 'PostulacionController@update')->name('postulacion.update');
Route::get('/admisiones/consultar_postulacion', 'PostulacionController@indexConsultarPostulacion')->name('admision.indexConsultarPostulacion');
//Route::get('/admisiones', 'PostulacionController@indexConsultarPostulacion')->name('admision.indexConsultarPostulacion');
Route::get('/admisiones/consultar_postulacion/buscardni/{dni}', 'PostulacionController@buscarxdni')->name('admision.buscarUsuarioxDni');

/**====================================== REGISTRO DE ALUMNOS ====================================== */
Route::get('/admin/alumnos', 'AlumnoController@CredencialesAlumnosAdmin')->name('index_registrar_credenciales_alumno');
Route::get('/admin/registraralumno', 'AlumnoController@CrearCredencialesAlumno')->name('registrar_credenciales_alumno');
Route::post('/admin/storealumno', 'AlumnoController@StoreCredencialesAlumno')->name('store_credenciales_alumno');
Route::get('/admin/buscarPadre/{dni}', 'PersonaController@buscarPadre')->name('padre_buscar');
Route::get('/admin/alumnos/card', 'AlumnoController@showAlumnos')->name('showAlumnos');
Route::get('/admin/alumnos/card/{id?}', 'AlumnoController@get_detalleAlumno')->name('showAlumnosCard');
Route::get('/admin/alumnoCategoria/{id}', 'CategoriaController@showCategorias')->name('categoriasCarrera');
/**====================================== Perfil ====================================== */
Route::get('/perfil', 'UsuarioController@indexPerfil')->name('indexPerfil');
Route::put('/updatefotoperfil', 'UsuarioController@updateFotoPerfil')->name('updateFotoPerfil');
Route::put('/updatedatospersonales', 'UsuarioController@updateDatosPersonales')->name('updateDatosPersonales');
Route::put('/updatecontraseña', 'UsuarioController@updateContraseñaPerfil')->name('updateContraseña');

/**====================================== Asistencias Intranet Alumno ====================================== */
Route::get('/asistencias', 'AsistenciaController@indexAsistenciaAlumno')->name('indexAsistenciaAlumno');
Route::get('/asistencias/{seccion}', 'AsistenciaController@getAsistenciasAlumno')->name('getAsistenciasAlumno');
Route::get('/asistencias/reporte/{seccion_id?}', 'AsistenciaController@reporteAsistenciasAlumno')->name('reporteAsistenciasAlumno.pdf');

/**====================================== Asistencias Intranet Secretaría Académica ====================================== */
Route::get('secretaria/asistencias', 'AsistenciaController@indexAsistenciaSecretaria')->name('indexAsistenciaSecretaria');
Route::get('secretaria/getlistaAsistenciaSecretaria', 'AsistenciaController@getlistaAsistenciaSecretaria')->name('getlistaAsistenciaSecretaria');
Route::get('secretaria/getAsistenciasAlumnoSecretaria', 'AsistenciaController@getAsistenciasAlumnoSecretaria')->name('getAsistenciasAlumnoSecretaria');
Route::get('secretaria/reporteAsistenciasAlumnoSecretaria/{seccion_id?}/{alumno_id?}', 'AsistenciaController@reporteAsistenciasAlumnoSecretaria')->name('reporteAsistenciasAlumnoSecretaria.pdf');

/**====================================== Categorías Módulo Administrador ====================================== */
Route::get('Admin/categorias', 'CategoriaController@index')->name('indexCategoria');
Route::get('Admin/getcategoriaxcarrera/{id}', 'CategoriaController@getcategoriaxcarrera')->name('getcategoriaxcarrera');
Route::get('Admin/edit/{id}', 'CategoriaController@edit')->name('editCategoria');
Route::post('Admin/storecateoria/', 'CategoriaController@store')->name('storeCategoria');
Route::put('Admin/updatecategoria/', 'CategoriaController@update')->name('updateCategoria');
Route::delete('Admin/eliminarcategoria/{id}', 'CategoriaController@delete')->name('eliminarCategoria');

/**====================================== Asignar Alumno-Categorías Módulo Administrador ====================================== */
Route::get('Admin/alumno-categorias', 'CategoriaController@indexAlumnoCategorias')->name('indexAlumnoCategorias');
Route::get('/Admin/buscarAlumnoCategoria/{dni}', 'CategoriaController@buscarAlumnoxDni')->name('buscarAlumnoxDni');
Route::put('/Admin/storeAlumnoCategoria', 'CategoriaController@storeAlumnoCategoria')->name('storeAlumnoCategoria');
Route::get('/Admin/categoriaCarrera/{id}', 'CategoriaController@categoriaCarrera')->name('categoriaCarrera');
Route::get('/Admin/montoCategoria/{id}', 'CategoriaController@montoCategoria')->name('montoCategoria');

/**====================================== Asistencias Alumno MÓDULO PADRE ====================================== */
Route::get('academica/asistencias/{id}', 'AsistenciaController@indexAsistenciaAlumnoPadre')->name('indexAsistenciaAlumnoPadre');
Route::get('academica/notas/{id}', 'PadreController@ver_RecordNotas_hijo')->name('indexNotasAlumnoPadre');
Route::get('academica/asistencias/{seccion?}/{alumno_id?}', 'AsistenciaController@getAsistenciasAlumnoPadre')->name('getAsistenciasAlumnoPadre');
Route::get('academica/asistencias/reporte/{seccion_id?}/{alumno_id?}', 'AsistenciaController@reporteAsistenciasAlumnoPadre')->name('reporteAsistenciasAlumnoPadre.pdf');

/**================================= LMS =============================================*/

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});

Route::get('/migrate', function () {
    Artisan::call('migrate:fresh');
    return "migrate success";
});

Route::get('test_gd', function () {
    return view('test_2');
});
Route::get('prueba', function () {
    return view('prueba_gd');
});
Route::get('searchPerson/{dni}', 'PeruConsultingController@searchPerson')->name('searchPerson');
Route::get('register_padre', function () {
    $departamentos = \App\Departamento::all();
    return view('IntranetPadre.Registros.create', ['departamentos' => $departamentos]);
})->name('Register_Padre');
Route::post('guardar_padre', 'PadreController@register')->name('PadreRegister');

//Route::get('xxxRe', function () {
//    $alumnos = \App\Alumno::with('persona', 'alumnocarreras')
//        ->whereHas('alumnocarreras', function ($q) {
//            $q->where('carrera_id', 1);
//        })->get();
//
//    foreach ($alumnos as $loop => $alumno) {
//        if ($alumno->padre_id != 0)
//            $padre = \App\Padre::with('persona')->find($alumno->padre_id);
//        if ($alumno->madre_id != 0)
//            $madre = \App\Padre::with('persona')->find($alumno->madre_id);
//        echo $loop + 1 . '|' . $alumno->id . '| ' .
//            $alumno->alumnocarreras->first()->ciclo . '°|' .
//            $alumno->alumnocarreras->first()->grupo . '|' .
//            $alumno->persona->DNI . '|' .
//            $alumno->persona->paterno . ' ' .
//            $alumno->persona->materno . ', ' .
//            $alumno->persona->nombres . '|' .
//            $alumno->persona->telefono . '|' .
//            $padre->persona->paterno. ' ' .
//            $padre->persona->materno . ' ' .
//            $padre->persona->nombres  . '|' .
//            $padre->persona->telefono . '|' .
//            $madre->persona->paterno . ' ' .
//            $madre->persona->materno . ' ' .
//            $madre->persona->nombres . '|' .
//            $madre->persona->telefono. '|' .
////            $alumno->padre_id . ',' .
////            $alumno->madre_id .
//            "</br>";
////        echo $alumno->padre_id . ',' . $alumno->madre_id . ',';
////        echo $alumno->id . ',';
////        dd($alumno=\App\Alumno::with('persona')->whereDoesntHave('alumnocarreras')->get());
//    }
//})->name('PadRporte');

//Route::get('banco', function () {
////    $datos = collect();
////    $padres = \App\Padre::with('persona')
////        ->where('apoderado', 'si')
////        ->get();
////    foreach ($padres as $padre) {
////        $sumamonto = 0;
////        if (count($padre->alumnop) > 0) {
////            foreach ($padre->alumnop as $item) {
////                $sumamonto += $item->categorias->last()->monto;
////                $alumno = $padre->alumnop->first();
////            }
////        }
////        if (count($padre->alumnom) > 0) {
////            foreach ($padre->alumnom as $item) {
////                $sumamonto += $item->categorias->last()->monto;
////                $alumno = $padre->alumnom->first();
////            }
////        }
////
////        $cronogramas = \App\Cronograma::with('concepto')
////            ->whereHas('concepto', function ($q) {
////                $q->where([['nombre', 'like', 'Pens%'], ['periodo_id', \App\Periodo::where('estado', '1')->first()->id]]);
////            })
////            ->get();
////        $pagosCronId = $alumno->alumnoPago->pluck('cronograma_id');
////
////        foreach ($cronogramas as $cronograma) {
////            if (!$pagosCronId->contains($cronograma->id)) {
////                $data = [
////                    'padre' => strtoupper($padre->persona->paterno . ' ' . $padre->persona->materno . ' ' . $padre->persona->nombres),
////                    'DNI' => $padre->persona->DNI,
////                    'concepto' => strtoupper(\Carbon\Carbon::parse($cronograma->fech_vencimiento)->monthName),
////                    'vencimiento' => \Carbon\Carbon::now()->format('Y-m-d'),
////                    'bloqueo' => \Carbon\Carbon::now()->format('Y-m-d'),
////                    'importeMax' => $sumamonto,
////                    'importeMin' => $sumamonto,
////                ];
//////                dd($data['padre'] . '|' . $data['DNI'] . '|' . $data['concepto'] );
////                echo $data['padre'] . '|' . $data['DNI'] . '|' . $data['concepto'] . "<br>";
////            }
////        }
////
////
//////        $datos->push($data);
////    }
////
//////    dd($datos);
//});

Route::get('siag', function () {
    $datos = collect();
    $periodo_id = \App\Periodo::where('estado', '1')->first()->id;
    $alumnos = \App\Alumno::with('persona', 'matricula')
        ->whereHas('alumnocarreras.matriculas', function ($q) use ($periodo_id) {
            $q->where('periodo_id', $periodo_id);
        })
        ->whereHas('alumnocarreras', function ($q) {
            $q->where('carrera_id', 1);
        })
        ->get();
    foreach ($alumnos as $loop => $alumno) {
        if ($alumno->padre_id != 0) {
            $padre = \App\Padre::with('persona')->find($alumno->padre_id);
            if ($padre->apoderado == 'si') {
                $ap = $padre;
            }
        }
        if ($alumno->madre_id != 0) {
            $madre = \App\Padre::with('persona')->find($alumno->madre_id);
            if ($madre->apoderado == 'si') {
                $ap = $madre;
            }
        }
        $al_carrera = $alumno->alumnocarreras->last();
        if ($al_carrera->estado !== 0) {
            $data = [
                'N' => $loop + 1,
                'ESTUDIANTE' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ' . $alumno->persona->nombres,
                'DNI_ESTUDIANTE' => strtoupper($alumno->persona->DNI),
                'APODERADO' => $ap->persona->paterno . ' ' . $ap->persona->materno . ' ' . $ap->persona->nombres,
                'DNI_APODERADO' => $ap->persona->DNI,
                'NIVEL' => $al_carrera->carrera->nombre,
                'GRADO' => $al_carrera->ciclo,
                'SECCION' => $al_carrera->grupo,
                'DIRECCION' => $ap->persona->direccion,
                'TELEFONO' => $ap->persona->telefono,
            ];
            $datos->push($data);
        }
    }
    echo 'N°;' . 'APELLIDOS Y NOMBRES DE ESTUDIANTES;' . 'DNI ESTUDIANTE;' . 'APODERADO;' . 'DNI_APODERADO;' . 'NIVEL;' . 'GRADO;' . 'SECCION;' . 'DIRECCIÓN;' . 'TELÉFONO;' . "<br>";
    foreach ($datos as $dato) {
        echo $dato['N'] . ';' . $dato['ESTUDIANTE'] . ';' . $dato['DNI_ESTUDIANTE'] . ';' . $dato['APODERADO'] . ';' . $dato['DNI_APODERADO'] . ';' . $dato['NIVEL'] . ';' . $dato['GRADO'] . ';' . $dato['SECCION']. ';' . $dato['DIRECCION'] . ';' . $dato['TELEFONO']  . "<br>";
    }
});

//Route::get('simM',function (){
//    $datos = collect();
//    $periodo_id = \App\Periodo::where('estado', '1')->first()->id;
//    $alumnos = \App\Alumno::with('persona', 'matricula')
//        ->whereDoesntHave('alumnocarreras.matriculas', function ($q) use ($periodo_id) {
//            $q->where('periodo_id', $periodo_id);
//        })->get();
//    foreach ($alumnos as $loop => $alumno) {
//        $data = [
//            'N' => $loop + 1,
//            'DNI_ESTUDIANTE' => strtoupper($alumno->persona->DNI),
//            'ESTUDIANTE' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ' . $alumno->persona->nombres,
//            ];
//        $datos->push($data);
//    }
//    echo 'N°;'.'DNI ESTUDIANTE;'.'APELLIDOS Y NOMBRES DE ESTUDIANTES;'."<br>";
//    foreach ($datos as $dato){
//        echo $dato['N'].';'.$dato['DNI_ESTUDIANTE'].';'.$dato['ESTUDIANTE']."<br>";
//    }
//});

//Route::get('matricula_all', function () {
//    $periodoId = \App\Periodo::where('estado', 1)->first()->id;
//    $cronogramas = \App\Cronograma::with('periodo')
//        ->where('periodo_id', $periodoId)
//        ->pluck('id');
//
//    $alumnos = \App\Alumno::with('persona')->whereHas('pago', function ($q) use ($cronogramas) {
//        $q->whereIn('cronograma_id', $cronogramas)->where('fe_total_price', '<>', 0);
//    })->get();
//
//
//    foreach ($alumnos as  $alumno) {
//        $al_carrera = $alumno->alumnocarreras->last();
//        $al_categ = $alumno->categorias->last();
//        if ($al_carrera->estado !== 0 && $al_categ->estado !== 0 && $al_carrera->carrera_id==3) {
//            \Illuminate\Support\Facades\DB::beginTransaction();
//            $matricula=new \App\Matricula();
//            $matricula->alumno_carrera_id=$al_carrera->id;
//            $matricula->periodo_id=$periodoId;
//            $matricula->estado='CONFIRMADA';
//            $matricula->categoria_id=$al_categ->id;
//            $matricula->save();
//            $secciones = \App\Seccion::with('periodo')
//                ->whereHas('pe_curso', function ($q) use ($al_carrera) {
//                    $q->where('semestre', $al_carrera->ciclo);
//                })
//                ->whereHas('pe_curso.pe_carrera', function ($q) use ($al_carrera) {
//                    $q->where('carrera_id', $al_carrera->carrera_id);
//                })
//                ->where('periodo_id', $periodoId)
//                ->where('grupo','like','%'.$al_carrera->grupo.'%')
//                ->get();
//            foreach ($secciones as $loop=>$seccion) {
//             $det_mat=new \App\MatriculaDetalle();
//             $det_mat->matricula_id=$matricula->id;
//             $det_mat->seccion_id=$seccion->id;
//             $det_mat->tipo='R';
//             $det_mat->save();
//            }
//            \Illuminate\Support\Facades\DB::commit();
//        }
//    }
//    echo 'todo ok';
//});
//
//Route::get('alNoPago', function () {
//    $alumnos = \App\Alumno::with('persona')
//        ->whereHas('alumnoPago', function ($q) {
//            $q->where('fe_valor_venta', 0);
//        })
//        ->get();
//    $datos = collect();
//    foreach ($alumnos as $loop => $alumno) {
//        if ($alumno->padre_id != 0) {
//            $padre = \App\Padre::with('persona')->find($alumno->padre_id);
//            if ($padre->apoderado == 'si') {
//                $ap = $padre;
//            }
//        }
//        if ($alumno->madre_id != 0) {
//            $madre = \App\Padre::with('persona')->find($alumno->madre_id);
//            if ($madre->apoderado == 'si') {
//                $ap = $madre;
//            }
//        }
//        $al_carrera = $alumno->alumnocarreras->last();
//        if ($al_carrera->estado !== 0) {
//            $data = [
//                'N' => $loop + 1,
//                'DNI_ESTUDIANTE' => strtoupper($alumno->persona->DNI),
//                'ESTUDIANTE' => $alumno->persona->paterno . ' ' . $alumno->persona->materno . ' ' . $alumno->persona->nombres,
//                'TEL'=>$alumno->persona->telefono,
//                'NIVEL'=>$al_carrera->carrera->nombre,
//                'GRADO' => $al_carrera->ciclo,
//                'SECCION' => $al_carrera->grupo,
//                'APODERADO' => $ap->persona->paterno . ' ' . $ap->persona->materno . ' ' . $ap->persona->nombres,
//                'TELEFONO' => $ap->persona->telefono,
//                'DIRECCION' => $ap->persona->direccion,
//            ];
//            $datos->push($data);
//        }
//    }
//    echo 'N°;' . 'DNI ESTUDIANTE;' . 'APELLIDOS Y NOMBRES DE ESTUDIANTES;'.'TELÉFONO;'.'NIVEL;'. 'GRADO;' . 'SECCION;' . 'APODERADO;' . 'TELÉFONO;' . 'DIRECCIÓN;' . "<br>";
//    foreach ($datos as $dato) {
//        echo $dato['N'] . ';' . $dato['DNI_ESTUDIANTE'] . ';' . $dato['ESTUDIANTE'] . ';' .$dato['TEL'] . ';' .$dato['NIVEL'] . ';' . $dato['GRADO'] . ';' . $dato['SECCION'] . ';' . $dato['APODERADO'] . ';' . $dato['TELEFONO'] . ';' . $dato['DIRECCION'] . "<br>";
//    }
//});
